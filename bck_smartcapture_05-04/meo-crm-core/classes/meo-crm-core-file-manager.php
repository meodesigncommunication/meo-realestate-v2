<?php

class MeoCrmCoreFileManager
{
    public static $mime_types = array(

            'txt' => array(
                'icon' => '<i class="fa fa-file-text-o" aria-hidden="true"></i>', 
                'mime_type' => 'text/plain'
            ),
            'htm' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'text/html'
            ),
            'html' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'text/html'
            ),
            'php' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'text/html'
            ),
            'css' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'text/css'
            ),
            'js' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/javascript'
            ),
            'json' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/json'
            ),
            'xml' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/xml'
            ),
            'swf' => array(
                'icon' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/x-shockwave-flash'
            ),
            'flv' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/x-flv'
            ),
            'png' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/png'
            ),
            'jpe' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/jpeg'
            ),
            'jpeg' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/jpeg'
            ),
            'jpg' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/jpeg'
            ),
            'gif' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/gif'
            ),
            'bmp' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/bmp'
            ),
            'ico' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/vnd.microsoft.icon'
            ),
            'tiff' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/tiff'
            ),
            'tif' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/tiff'
            ),
            'svg' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/svg+xml'
            ),
            'svgz' => array(
                'icon' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/svg+xml'
            ),
            'zip' => array(
                'icon' => '<i class="fa fa-file-archive-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/zip'
            ),
            'rar' => array(
                'icon' => '<i class="fa fa-file-archive-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/x-rar-compressed'
            ),
            'exe' => array(
                'icon' => '<i class="fa fa-file-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/x-msdownload'
            ),
            'msi' => array(
                'icon' => '<i class="fa fa-file-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/x-msdownload'
            ),
            'cab' => array(
                'icon' => '<i class="fa fa-file-archive-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/vnd.ms-cab-compressed'
            ),
            'mp3' => array(
                'icon' => '<i class="fa fa-file-audio-o" aria-hidden="true"></i>', 
                'mime_type' => 'audio/mp3'
            ),
            'mp3' => array(
                'icon' => '<i class="fa fa-file-audio-o" aria-hidden="true"></i>', 
                'mime_type' => 'audio/wav'
            ),
            'mp4' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/mp4'
            ),
            'ogv' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/ogg'
            ),
            'webm' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/webm'
            ),
            'mov' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/quicktime'
            ),
            'qt' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/quicktime'
            ),
            'mov' => array(
                'icon' => '<i class="fa fa-file-video-o" aria-hidden="true"></i>', 
                'mime_type' => 'video/quicktime'
            ),
            'pdf' => array(
                'icon' => '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/pdf'
            ),
            'psd' => array(
                'icon' => '<i class="fa fa-file-o" aria-hidden="true"></i>', 
                'mime_type' => 'image/vnd.adobe.photoshop'
            ),
            'ai' => array(
                'icon' => '<i class="fa fa-file-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/postscript'
            ),
            'eps' => array(
                'icon' => '<i class="fa fa-file-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/postscript'
            ),
            'ps' => array(
                'icon' => '<i class="fa fa-file-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/postscript'
            ),
            'doc' => array(
                'icon' => '<i class="fa fa-file-word-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/msword'
            ),
            'rtf' => array(
                'icon' => '<i class="fa fa-file-word-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/rtf'
            ),
            'xls' => array(
                'icon' => '<i class="fa fa-file-excel-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/vnd.ms-excel'
            ),
            'ppt' => array(
                'icon' => '<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>', 
                'mime_type' => 'application/vnd.ms-powerpoint'
            ),
    );
    
    /*
     * Test si un dossier exist ou pas 
     */
    public static function ifFolderExist($url)
    {
        if(file_exists($url))
        {
            return true;
        }else{
            return false;
        }        
    }
    
    /*
     * Test si un fichier exist ou pas 
     */
    public static function ifFileExist($url)
    {
        if(self::ifFolderExist($url))
        {
            return true;
        }else{
            return false;
        }        
    }
    
    /*
     * Create a new folder 
     * 
     * @param $name(string), $url(string), $chmod(int), $recursive(boolean)
     * @return boolean
     */
    public static function createFolder($name, $url, $chmod, $recursive)
    {
        if(!self::ifFolderExist($url.$name))
        {
            if(!mkdir($url.$name, $chmod, $recursive))
            {
                return false;
            }else{
                chmod($url.$name, $chmod);
            }
        }
        return true;
    }
    
    /*
     * 
     */
    public static function dropFolder($url)
    {

    }
    
    /*
     * 
     */
    public static function uploadMedia($file,$destination)
    {
        $message = '';
        $check_upload = true;
        $file_upload_result = array();

        print_r($file['tmp_name']);
        
        if(isset($file['tmp_name']) && !empty($file['tmp_name']))
        {
            foreach($file['tmp_name'] as $key => $tmp_file)
            {
                $filename = self::cleanNameFileOrFolder($file['name'][$key]);

                if(!move_uploaded_file($tmp_file, $destination.$filename))
                {

                    $file_upload_result[] = array(
                        'success' => false,
                        'file_name' => $filename,
                        'path_file' => $destination.$filename,
                        'mime_type' => $file['type'][$key]
                    );

                    $check_upload = false;
                    $message = 'Une erreur est arrivé durant l\'upload des médias';

                }else{

                    $file_upload_result[] = array(
                        'success' => true,
                        'file_name' => $filename,
                        'path_file' => $destination.$filename,
                        'mime_type' => $file['type'][$key]
                    );

                }
            }
        }else{
            $check_upload = false;
            $message = 'Une erreur est arriv&eacute;e durant l\'upload des médias';
        }
            
        
        return array('success' => $check_upload, 'file_upload_result' => $file_upload_result, 'message' => $message);
    }
    
    /*
     * Deleting a special letter on a file name or folder name 
     */
    private static function cleanNameFileOrFolder($filename)
    {
        $except = array('é','è','ê','ë','à','â','ä','ï','î','ô','ö','û','ü','ç','¦','@','*','#','"','%','&','|','\\','<','>','{','}','[',']','(',')','^','?','!','°','=','~',' ');        
        $except_replace = array('e','e','e','e','a','a','a','i','i','o','o','u','u','c','','','','','','','','','','','','','','','','','','','','','','','','_');      
        $final_filename = str_replace($except,$except_replace,$filename);
        return $final_filename;
    }
    
    /*
     * Create a media in html
     */
    public static function iconTypeMedia($mime_type_file)
    {
        foreach(static::$mime_types as $key => $mime_type)
        {
            if($mime_type['mime_type'] == $mime_type_file)
            {
                return $mime_type['icon'];
            }
        }        
        return '<i class="fa fa-file-o" aria-hidden="true"></i>';
    }
    
    /*
     * Create a media in html
     */
    public static function showMediaElement($media,$id,$action_delete='all',$selectable=false,$is_selected=false,$onclick='')
    {
        $html = '';
        $path_file = '';
        $class = 'preview-image-upload';
        $link_admin = get_admin_url().'admin.php';
        $link_add_media = site_url('meo-crm-medias-form');
        $count = 0;
        
        if(is_array($media['post_metas']) && !empty($media['post_metas']))
        {        
            foreach($media['post_metas'] as $meta)
            {
                if($meta->meta_key == 'path_file')
                {
                    $path_file = $meta->meta_value;
                }
            }
        }
        
        $mime_type = $media['post_mime_type'];  
        
        $list_mime_type = explode('/', $mime_type);
        
        $icon = self::iconTypeMedia($mime_type);
        
        $is_image = ($list_mime_type[0] == 'image') ? true : false;
        
        $style = ($is_image) ? 'background: url('.$media['guid'].') no-repeat;' : 'background-color: #dedede;';
        
        $class = ($is_selected) ? 'selected-media' : '' ;
        
        $html .= '<div id="media_'.$id.'" class="preview-image-upload '.$class.'" style="'.$style.'" onclick="'.$onclick.'">';
        $html .=    (!$is_image)? $icon : '';
        $html .=    '<div class="label-file-upload">'.$media['post_name'].'</div>';
        
        // If selected media don't show a edit and delete button
        if(!$selectable)
        {
            $html .=    '<div title="Editer" class="edit-file-upload"><a href="'.$link_add_media.'?id='.$id.'"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>';
            $html .=    '<div title="Supprimer" class="remove-file-upload" onclick=\'deleteMedia('.$id.',"'.$path_file.'","'.$action_delete.'")\'><i class="fa fa-trash" aria-hidden="true"></i></div>';
            $html .= '<input type="hidden" id="media_'.$id.'_checkbox" class="checkbox_media" name="medias[]" value="'.$id.'" />';
            
        }else{
            
            $checked = ($is_selected) ? 'checked="true"' : '' ;
            
            $html .= '<div class="checked-media"><i class="fa fa-check" aria-hidden="true"></i></div>';
            $html .= '<input type="checkbox" '.$checked.' id="media_'.$id.'_checkbox" class="checkbox_media" name="medias[]" value="'.$id.'" />';
        }
        
        $html .= '</div>';
        
        return $html;
    }
    
    public static function showMedias($medias,$selected=false,$action_delete='all',$is_selected=false)
    {
        $html = '';  
        if(is_array($medias) && !empty($medias))
        {
            foreach($medias as $key => $media)
            {
                $html .= self::showMediaElement($media,$key,$action_delete,$selected,$is_selected);
            }
        }else{
            $html .= '<p>Aucun média existant</p>';
        }
        return $html;
    }
    
    // Generate a Javascript Array for spinner preview (360 rotate images)
    public static function createJSArrayMedia($medias)
    {
        $html  = '<script type="text/javascript">';
        $html .= 'var images = [];';
        if(is_array($medias) && !empty($medias))
        {
            foreach($medias as $key => $media)
            {
                $html .= 'images.push("'.$media['guid'].'");';
            }
        }
        $html .= '</script>';
        
        return $html;
    }
    
    // Check and create folder in external FTP
    public static function externalFTPcheckAndCreateFolder($ftp,$content_dir_path,$folderNameCreated)
    {
        // Init varables
        $check = false;
        $check_create_folder = true;
        $list_file_folder = ftp_nlist($ftp, $content_dir_path);
        
        // Check if folder exists
        foreach($list_file_folder as $file_folder)
        {
            if($file_folder == $content_dir_path.$folderNameCreated)
            {
                $check = true;
            }
        }
        
        // if folder doesn't exists create it
        if(!$check)
        {
            if(!ftp_mkdir($ftp, $content_dir_path.$folderNameCreated.'/'))
            {
                $check_create_folder = false;
            }
        }
        
        return $check_create_folder;
    }
}

