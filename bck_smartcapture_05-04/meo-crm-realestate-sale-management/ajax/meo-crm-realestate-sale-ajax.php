<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
add_action( 'wp_ajax_nopriv_getSaleListByProjectId', 'getSaleListByProjectId' );  
add_action( 'wp_ajax_getSaleListByProjectId', 'getSaleListByProjectId' );
function getSaleListByProjectId()
{
    //INIT VARIABLES
    $html = '';
    global $wpdb;
    $objectTab = array();
    $total_price = 0;
    $project_id = (isset($_POST['project_id'])) ? $_POST['project_id'] : '';
    $contact_id = (isset($_POST['contact_id'])) ? $_POST['contact_id'] : '';
    $where = '';
    $lotExist = false;
    $objectExist = false;
    $noContactToShow = (isset($_POST['show_contact'])) ? $_POST['show_contact'] : false;
        
    // GET ACCESS PROJECT
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    if(!isset($_SESSION['project_id']) || $_SESSION['project_id'] != $project_id)
    {
        $where = 'WHERE project_id = '.$project_id.' ';
        $contacts = meo_crm_contacts_getContactsByProjectID($project_id);        
        $_SESSION['project_id'] = $project_id;
        $_SESSION['contacts'] = $contacts;
        
    }else{
        $contacts = $_SESSION['contacts'];
    }
    
    
    if($project){
        
        // INIT CLASS
        $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);    
    
        // INIT VARIABLE TO CONNECT TO EXTERNAL DB
        if(isset($access->host_db) && isset($access->name_db) && isset($access->login_db) && isset($access->password_db))
        {
            $hostDB     = $meoCrmCoreCryptData->decode($access->host_db);
            $nameDB     = $meoCrmCoreCryptData->decode($access->name_db);
            $loginDB    = $meoCrmCoreCryptData->decode($access->login_db);
            $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
        }else{
            $hostDB     = '';
            $nameDB     = '';
            $loginDB    = '';
            $passwordDB = '';
        }

        // CONNECT TO THE EXTERNAL DB
        $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);

        $lots = LotModel::getAllLot($external_wpdb);
        $status = StatusModel::getAllStatus($external_wpdb);
        $objects = ObjectModel::selectObjectByProjectId($project_id);
        
        $where = (isset($contact_id) && !empty($contact_id)) ? array(array('index' => 's.contact_id', 'compare' => '=', 'value' => $contact_id)) : '';
        $sales = SaleManagementModel::selectSaleContactLotObject($external_wpdb,$where); 
        
        $html .= '<table class="table wp-list-table widefat fixed striped posts">';
        $html .=        '<thead> ';
        if(!$noContactToShow)
        {
            $html .=            '<th>Contact</ht>';
        }
        $html .=            '<th>Lot</ht>';
        $html .=            '<th>Status du lot</ht>';
        $html .=            '<th>Objets</th>';
        $html .=            '<th>Prix</th>';    
        $html .=            '<th></th>';    
        $html .=        '</thead> ';
        $html .=        '<tbody> ';
        $html .=            '<tr id="add_new_entry">';  
        if(!$noContactToShow)
        {
            $html .=                '<td>';            
            $html .=                    '<select name="contact" class="contact_list">'; 
            $html .=                        '<option value="0">Choose a contact</option>';

            if(isset($contacts) && !empty($contacts))
            {
                foreach($contacts as $contact)
                {
                    // Filtre uniquement accès
                    $contact = meo_crm_content_access_getContactData($contact);

                    if(isset($contact) && !empty($contact)){
                        $html .=  '<option value="'.$contact['id'].'">'.$contact['first_name'].' '.$contact['last_name'].'</option>';
                    }
                }
            }

            $html .=                    '</select>';            
            $html .=                '</td>';  
        }
        $html .=                '<td>';     
        $html .=                    '<select name="lot" class="list_lot" onchange="change_status_add(this)">'; 
        $html .=                        '<option value="0">Choose a lot</option>'; 

        if(isset($lots) && !empty($lots))
        {
            foreach($lots as $lot)
            {

                foreach($sales as $sale)
                {
                    if($lot->id == $sale['lots']['id'])
                    {
                        $lotExist = true;
                    }
                }

                $html .=  (!$lotExist) ? '<option value="'.$lot->id.'">'.$lot->title.'</option>' : ''; 

                $lotExist = false;
            }
        }

        $html .=                    '</select>';  
        $html .=                '</td>'; 
        
        $html .=                '<td>';     
        $html .=                    '<select name="status" class="list_status">'; 
        $html .=                        '<option value="0">Choose a status</option>'; 

        if(isset($status) && !empty($status))
        {
            foreach($status as $value)
            {
                $html .=  '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }

        $html .=                    '</select>';  
        $html .=                '</td>';

        $html .=                '<td>';  

        $html .=                    '<section id="bloc-object">';  
        $html .=                        '<select name="objects[]" class="select-object-list list_objects" onchange="addObjectList(this)">'; 
        $html .=                            '<option value="0">Choose a object</option>'; 
        foreach($objects as $object)
        {
            foreach($sales as $sale)
            {
                if(isset($sale['objects']))
                {
                    foreach($sale['objects'] as $objectSale)
                    {
                        if($object->id == $objectSale['id'])
                        {
                            $objectExist = true;
                        }            
                    }    
                }
            }
            if(!$objectExist)
            {
                $objectTab[] = $object->id;
                $html .=  '<option class="value_'.$object->id.'" value="'.$object->id.'">'.$object->title.'</option>'; 
            }else{
                $html .=  '';     
            }
            $objectExist = false;
        }
        $html .=                        '</select>'; 
        $html .=                    '</section>';  
        $html .=                '</td>';  
        $html .=                '<td>&nbsp;</td>';  
        $html .=                '<td><input type="button" name="add-sale" id="add-sale" value="save" onclick="saveSale()"></td>';            
        $html .=            '</tr>'; 

        foreach($sales as $sale)
        {
            $sale_id = $sale['id'];
            $status_lot = 0;
            if(isset($contacts) && !empty($contacts))
            {
                foreach($contacts as $value)
                {
                    if($sale['contact_id'] == $value['id']){
                        $contact = meo_crm_content_access_getContactData($value);
                        $check = true;
                    }else{
                        $check = false;
                    }
                }
            }
            if(isset($contact) && !empty($contact)){
                if(isset($sale['lots']['title']) && isset($sale['lots']['price'])){

                    $total_price = $sale['lots']['price'];           

                    $html .=            '<tr class="update_entry">';
                    if(!$noContactToShow)
                    {
                        $html .=                '<td class="updated_list editable">';
                        $html .=                    '<input type="hidden" id="sale_id" name="sale_id" value="'.$sale['id'].'" />';
                        $html .=                    '<p>'.$sale['contact_name'].'</p>';
                        $html .=                    '<div class="form-input-list"><select name="contact" class="contact_list">'; 
                        foreach($contacts as $contact)
                        {
                            $contact = meo_crm_content_access_getContactData($contact);
                            if($contact){
                                if($sale['contact_id'] == $contact['id'])
                                {
                                    $html .=  '<option value="'.$contact['id'].'" selected=true>'.$contact['first_name'].' '.$contact['last_name'].'</option>';  
                                } else{
                                    $html .=  '<option value="'.$contact['id'].'">'.$contact['first_name'].' '.$contact['last_name'].'</option>';  
                                }
                            }
                        }
                        $html .=                    '</select>';  
                        $html .=                    '<input type="button" class="btn-update btn-update-contacts" name="update-contacts" value="save" onclick="updateSaleContact(this)" /></div>';
                        $html .=                '</td>';     
                    }
                    $html .=                '<td class="updated_list editable">';
                    if($noContactToShow)
                    {
                        $html .=                '<input type="hidden" id="sale_id" name="sale_id" value="'.$sale['id'].'" />';
                    }
                    $html .=                    '<p>' . $sale['lots']['title'] . '</p>';
                    $html .=                    '<div class="form-input-list"><select name="lot" class="list_lot" onchange="change_status_update_lot(this)">'; 
                    if(isset($lots) && !empty($lots))
                    {
                        foreach($lots as $lot)
                        {
                            foreach($sales as $saleExist)
                            {
                                if($lot->id == $saleExist['lots']['id'] && $lot->id  != $sale['lots']['id'])
                                {
                                    $lotExist = true;
                                }

                                if($lot->id  == $sale['lots']['id'])
                                {
                                    $selected = 'selected=true';
                                }else{
                                    $selected = '';
                                }
                            }
                            $html .=  (!$lotExist) ? '<option value="'.$lot->id.'" '.$selected.'>'.$lot->title.'</option>' : '';
                            $lotExist = false;
                        }
                    }
                    $html .=                    '</select>';    
                    $html .=                    '<input type="button" class="btn-update btn-update-lots" name="update-lots" value="save"  onclick="updateSaleLot(this)" /></div>';
                    $html .=                '</td>'; 
                    
                    $html .=                '<td  class="updated_list editable">'; 
                    foreach($status as $status_val)
                    {
                        if(isset($lots) && !empty($lots))
                        {
                            foreach($lots as $lot)
                            {
                                if($lot->id == $sale['lots']['id'])
                                {
                                    if($lot->status_id == $status_val->id)
                                    {
                                        
                                        $status_lot = $status_val->id;
                                        $html .=  '<p>'.$status_val->name.'</p>'; 
                                    }
                                }
                            }
                        }
                    }
                    $html .=                    '<div class="form-input-list"><select name="sale_lot_status" id="sale_lot_status" class="list_status">';
                    foreach($status as $status_val)
                    {
                        if($status_lot == $status_val->id)
                        {

                            $html .=  '<option value="'.$status_val->id.'" selected="true">'.$status_val->name.'</option>'; 
                        }else{
                            $html .=  '<option value="'.$status_val->id.'">'.$status_val->name.'</option>'; 
                        }
                    }
                    $html .=                    '</select>';
                    $html .=                    '<i class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateSaleLotStatus(this)" aria-hidden="true"></i>';
                    $html .=                    '<i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelEditableField(this)" aria-hidden="true"></i>';
                    $html .=                    '</div>';
                    $html .=                '</td>';
                    
                    $html .=                '<td class="updated_list editable">'; 
                    if(isset($sale['objects']))
                    {
                        $html .=                    '<ul class="show-data">'; 
                        foreach($sale['objects'] as $object)
                        {
                            $html .=  '<li>'.$object['title'].'</li>';   
                        }
                        $html .=                    '</ul>';            
                        $html .=                    '<div class="form-input-list"><ul>';            
                        foreach($sale['objects'] as $object)
                        {
                            if(!empty($object['id']))
                            {
                                $html .= '<li class="updated_list">';
                                $html .=    '<select name="list_objects" class="list_objects" onchange="changeSelectOption(this)">';
                                $html .=        '<option value="'.$object['id'].'">'.$object['title'].'</option>';
                                foreach($objects as $object)
                                {
                                    foreach($sales as $sale)
                                    {
                                        if(isset($sale['objects']))
                                        {
                                            foreach($sale['objects'] as $objectSale)
                                            {
                                                if($object->id == $objectSale['id'])
                                                {
                                                    $objectExist = true;
                                                }            
                                            }   
                                        }
                                    }
                                    if(!$objectExist)
                                    {
                                        $objectTab[] = $object->id;
                                        $html .=  '<option class="value_'.$object->id.'" value="'.$object->id.'">'.$object->title.'</option>';
                                    }else{
                                        $html .=  '';     
                                    }
                                    $objectExist = false;
                                }
                                $html .=    '</select>';
                                $html .=    '<input type="button" class="btn-remove" name="remove-objects" value="remove" onclick="removeObjectList(this)" />';  
                                $html .= '</li>';
                                if(isset($object->price) && !empty($object->price))
                                {
                                    $total_price += $object->price;
                                }
                            }
                        }

                        $html .=               '</ul>';
                        $html .=               '<br/>';
                        $html .=               '<input type="button" class="btn-update btn-update-objects" name="update-objects" value="save" onclick="updateObjects(this)" />';  
                        $html .=               '<input type="button" class="btn-add btn-add-objects" name="add-objects" value="add" onclick="addObjectUpdateList(this)" />';
                        $html .=               '</td>';
                    }
                    $html .=               '<td>'.number_format($total_price,0 ,".","'").' '.DEVISE.'</td>';
                    $html .=                '<td><input type="button" name="delete-sale" id="delete-sale" value="delete" onclick="deleteSale('.$sale_id.')"></td>';
                    $html .=           '</tr>';
                }
            }
        }

        $html .=    '</tbody> ';
        $html .= '</table>';
    }
    
    echo $html;
    
    die();
}

/*
 * 
 * 
 */
add_action( 'wp_ajax_nopriv_selectListObject', 'selectListObject' );  
add_action( 'wp_ajax_selectListObject', 'selectListObject' );
function selectListObject()
{
    // INIT VALUE
    $objectExist = false;
    $objects_id = $_POST['object_id'];
    $project_id = $_POST['project_id'];
    
    // INIT CLASS
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    if(isset($access->host_db) && isset($access->name_db) && isset($access->login_db) && isset($access->password_db))
    {
        $hostDB     = $meoCrmCoreCryptData->decode($access->host_db);
        $nameDB     = $meoCrmCoreCryptData->decode($access->name_db);
        $loginDB    = $meoCrmCoreCryptData->decode($access->login_db);
        $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    }else{
        $hostDB     = '';
        $nameDB     = '';
        $loginDB    = '';
        $passwordDB = '';
    }
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $sales = SaleManagementModel::selectSaleContactLotObject($external_wpdb);
    
    // GET OBJECTS
    $objects = ObjectModel::selectObjectByProjectId($project_id);
    
    $html  = '';
    $html .= '<select name="objects[]" class="select-object-list" onchange="addObjectList(this)">'; 
    $html .=    '<option value="0">Choose a object</option>'; 
    foreach($objects as $object)
    {
        foreach($sales as $sale)
        {
            foreach($sale['objects'] as $objectSale)
            {
                if($object->id == $objectSale['id'])
                {
                    $objectExist = true;
                }            
            }            
        }
        
        foreach($objects_id as $object_id)
        {
            if($object_id == $object->id)
            {
                $objectExist = true;
            }
        }
        
        if(!$objectExist)
        {
            $objectTab[] = $object->id;
            $html .=  '<option value="'.$object->id.'">'.$object->title.'</option>'; 
        }else{
            $html .=  '';     
        }
        
        $objectExist = false;
    }
    $html .= '</select>'; 
    
    echo $html;
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_saveSale', 'saveSale' );  
add_action( 'wp_ajax_saveSale', 'saveSale' );
function saveSale()
{
    // INIT VALUE
    $check = true;
    $datas = array();
    $lot_id = $_POST['lot_id'];
    $status_id = $_POST['status_id'];
    $project_id = $_POST['project_id']; 
    $contact_id = $_POST['contact_id'];
    $objects_id = $_POST['objects_id'];
    
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    if($project){
        
        // INIT CLASS
        $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);    
    
        // INIT VARIABLE TO CONNECT TO EXTERNAL DB
        if(isset($access->host_db) && isset($access->name_db) && isset($access->login_db) && isset($access->password_db))
        {
            $hostDB     = $meoCrmCoreCryptData->decode($access->host_db);
            $nameDB     = $meoCrmCoreCryptData->decode($access->name_db);
            $loginDB    = $meoCrmCoreCryptData->decode($access->login_db);
            $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
        }else{
            $hostDB     = '';
            $nameDB     = '';
            $loginDB    = '';
            $passwordDB = '';
        }

        // CONNECT TO THE EXTERNAL DB
        $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
        $datas['status_id'] = $status_id;
        $where = array('id' => $lot_id);
        $result = LotModel::updateLot($external_wpdb,$datas,$where);
        do_action('meo_crm_realestate_create_price_list',$project_id);
    }    
    
    $datas = array();
    $datas['project_id'] = $project_id;
    $datas['contact_id'] = $contact_id;
    $datas['created_at'] = date('Y-m-d H:m:i');
    $datas['updated_at'] = date('Y-m-d H:m:i');
    $datas['lot_id'] = $lot_id;
    
    $result = SaleManagementModel::insert_sale_management($datas); 
    
    if($result['valid'])
    {
        $datas = array();
        foreach($objects_id as $object_id)
        {
            $datas['sale_id'] = $result['id'];
            $datas['object_id'] = $object_id;
            $datas['created_at'] = date('Y-m-d H:m:i');
            $datas['updated_at'] = date('Y-m-d H:m:i');
            
            $result_object = SaleManagementModel::insert_sale_management_object($datas);
            
            print_r($result_object);
            
            if(!$result_object)
            {
                $check = false;
            }
        }
    }  
    
    echo $check;    
    die();    
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deleteObjectBySaleId', 'deleteObjectBySaleId' );  
add_action( 'wp_ajax_deleteObjectBySaleId', 'deleteObjectBySaleId' );
function deleteObjectBySaleId()
{
    // INIT VALUE
    $sale_id = $_POST['sale_id'];
    $object_id = $_POST['object_id'];
    // WHERE CLAUSE FOR SQL REQUEST
    $where = array(
        'sale_id' => $sale_id,
        'object_id' => $object_id
    );    
    // EXECUTE SQL REQUEST
    echo SaleManagementModel::deleteObjectSale($where);
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_updateSale', 'updateSale' );  
add_action( 'wp_ajax_updateSale', 'updateSale' );
function updateSale()
{
    // INIT VALUE
    $check = true;
    $datas = array();
    $sale_id = $_POST['sale_id'];
    $dataToUpdate = $_POST['dataUpdate'];
    $value = $_POST['value'];
    
    $datas[$dataToUpdate] = $value;
    
    $where = array('id' => $sale_id);
            
    $result = SaleManagementModel::update_sale_management($datas,$where);
    if(!$result['valid'])
    {
        $check = false;
    }
    
    echo $check;
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_updateObjectsBySaleId', 'updateObjectsBySaleId' );  
add_action( 'wp_ajax_updateObjectsBySaleId', 'updateObjectsBySaleId' );
function updateObjectsBySaleId()
{
    // INIT VALUE
    $check = true;
    $datas = array();
    $sale_id = $_POST['sale_id'];
    $objects = $_POST['objects'];
    
    $where = array(
        'sale_id' => $sale_id
    ); 
    
    SaleManagementModel::deleteObjectSale($where);
            
    foreach($objects as $object)
    {
        if(!empty($object))
        {
            $datas['sale_id'] = $sale_id;
            $datas['object_id'] = $object;
            $datas['created_at'] = date('Y-m-d H:m:i');
            $datas['updated_at'] = date('Y-m-d H:m:i');
            $result = SaleManagementModel::insert_sale_management_object($datas);
            if(!$result['valid'])
            {
                $check = false;
            }
        }
    }
    
    echo $check;
    
    die();
}

/*
 * 
 * 
 */
add_action( 'wp_ajax_nopriv_deleteSaleBySaleId', 'deleteSaleBySaleId' );  
add_action( 'wp_ajax_deleteSaleBySaleId', 'deleteSaleBySaleId' );
function deleteSaleBySaleId()
{
    global $wpdb;
    $sale_id = $_POST['sale_id'];
    
    $where = array('id' => $sale_id);
    if(SaleManagementModel::deleteSale($where))
    {   
        $where = array('sale_id' => $sale_id);
        if(SaleManagementModel::deleteObjectSale($where))
        {
            echo true;
        }else{
            echo false;
        }
    }
    
    die();
}

/*
 * 
 * 
 */
add_action( 'wp_ajax_nopriv_updateSaleLotStatus', 'updateSaleLotStatus' );  
add_action( 'wp_ajax_updateSaleLotStatus', 'updateSaleLotStatus' );
function updateSaleLotStatus()
{
    // INIT POST VALUE
    $result = array('success' => false);
    $project_id = $_POST['project_id'];    
    $status_id = $_POST['status_id'];
    $lot_id = $_POST['lot_id'];
    
    // GET ACCESS PROJECT
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    if($project){
        
        // INIT CLASS
        $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);    
    
        // INIT VARIABLE TO CONNECT TO EXTERNAL DB
        if(isset($access->host_db) && isset($access->name_db) && isset($access->login_db) && isset($access->password_db))
        {
            $hostDB     = $meoCrmCoreCryptData->decode($access->host_db);
            $nameDB     = $meoCrmCoreCryptData->decode($access->name_db);
            $loginDB    = $meoCrmCoreCryptData->decode($access->login_db);
            $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
        }else{
            $hostDB     = '';
            $nameDB     = '';
            $loginDB    = '';
            $passwordDB = '';
        }

        // CONNECT TO THE EXTERNAL DB
        $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
        $datas['status_id'] = $status_id;
        $where = array('id' => $lot_id);
        $result = LotModel::updateLot($external_wpdb,$datas,$where);
        do_action('meo_crm_realestate_create_price_list',$project_id);
    }
    
    echo $result['success'];
    
    die();
}

/*
 * 
 * 
 */
add_action( 'wp_ajax_nopriv_statusLot', 'statusLot' );  
add_action( 'wp_ajax_statusLot', 'statusLot' );
function statusLot()
{
    // INIT POST VALUE
    $status_id = 0;
    $result = array('success' => false);
    $project_id = $_POST['project_id']; 
    $lot_id = $_POST['lot_id'];
    
    // GET ACCESS PROJECT
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    if($project){
        
        // INIT CLASS
        $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);    
    
        // INIT VARIABLE TO CONNECT TO EXTERNAL DB
        if(isset($access->host_db) && isset($access->name_db) && isset($access->login_db) && isset($access->password_db))
        {
            $hostDB     = $meoCrmCoreCryptData->decode($access->host_db);
            $nameDB     = $meoCrmCoreCryptData->decode($access->name_db);
            $loginDB    = $meoCrmCoreCryptData->decode($access->login_db);
            $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
        }else{
            $hostDB     = '';
            $nameDB     = '';
            $loginDB    = '';
            $passwordDB = '';
        }

        // CONNECT TO THE EXTERNAL DB
        $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
        $lot = LotModel::getLotById($external_wpdb,$lot_id);        
        $status_id = $lot->status_id;
    }
    
    echo $status_id;
    
    die();
}