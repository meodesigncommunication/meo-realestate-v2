<?php

add_action( 'wp_ajax_nopriv_getMetaFormObject', 'getMetaFormObject' );  
add_action( 'wp_ajax_getMetaFormObject', 'getMetaFormObject' );
function getMetaFormObject()
{
    $html = '';
    $id_type = $_POST['type_id'];
    $id_object = $_POST['id_object'];
    
    $metas = MetaObjectModel::selectMetaObjectByTypeObjectId($id_type);
    $meta_datas = ObjectModel::selectMetaValueByObjectId($id_object);
    
    if(!empty($meta_datas)){
        foreach($metas as $meta){
            foreach($meta_datas as $meta_data){
                if($meta_data->meta_id == $meta->id){
                    $html .= '<tr class="form-field form-required">';
                        $html .= '<th>';
                            $html .= $meta->meta_key; 
                            $html .= '<span class="description">&nbsp;(obligatoire)</span>';
                        $html .= '</th>';
                        $html .= '<td>';
                            $html .= '<input type="text" name="'.$meta->meta_slug.'" id="'.$meta->meta_slug.'" placeholder="'.$meta->meta_key.'" value="'.$meta_data->value.'" />';
                        $html .= '</td>';
                    $html .= '</tr>';
                }
            }
        }
    }else{
        foreach($metas as $meta){
            $html .= '<tr class="form-field form-required">';
                $html .= '<th>';
                    $html .= $meta->meta_key; 
                    $html .= '<span class="description">&nbsp;(obligatoire)</span>';
                $html .= '</th>';
                $html .= '<td>';
                    $html .= '<input type="text" name="'.$meta->meta_slug.'" id="'.$meta->meta_slug.'" placeholder="'.$meta->meta_key.'" value="" />';
                $html .= '</td>';
            $html .= '</tr>';
        }
    }
    
    echo $html;
    die();
}