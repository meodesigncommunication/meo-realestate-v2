<?php

class SaleManagementModel
{
    // Init Table
    public static $table_contact = 'wp_meo_crm_contacts';
    public static $table_project = 'wp_meo_crm_projects';
    public static $table_object = 'wp_meo_crm_realestate_sale_objects';
    public static $table_sale = 'wp_meo_crm_realestate_sale_management';
    public static $table_sale_object = 'wp_meo_crm_realestate_sale_management_object';    
    public static $table_sale_type_object = 'wp_meo_crm_realestate_sale_type_object';    
    public static $table_sale_object_meta = 'wp_meo_crm_realestate_sale_object_metas';    
    public static $table_sale_object_meta_value = 'wp_meo_crm_realestate_sale_object_meta_values';  
    
    /*
     *
     */
    public static function selectSaleContactLotObject($wpdb_external = '',$where = '')
    {
        global $wpdb;
        $datas = array();
        
        $dataToShow  = 's.id, s.contact_id, s.lot_id, ';
        $dataToShow .= 'c.first_name, c.last_name, c.email, ';
        $dataToShow .= 'p.name AS project_name, p.url AS project_url, ';
        $dataToShow .= 'o.id AS object_id, o.title AS object_title, o.description AS object_description, o.price, ';
        $dataToShow .= 'mo.meta_key, mov.value AS meta_value, tob.type AS object_type ';
        
        $query  = 'SELECT '.$dataToShow.' ';
        $query .= 'FROM '.self::$table_sale.' AS s ';
        $query .= 'LEFT JOIN '.self::$table_sale_object.' AS so ON so.sale_id = s.id ';
        $query .= 'LEFT JOIN '.self::$table_contact.' AS c ON c.id = s.contact_id  ';
        $query .= 'LEFT JOIN '.self::$table_project.' AS p ON p.id = s.project_id  ';
        $query .= 'LEFT JOIN '.self::$table_object.' AS o ON o.id = so.object_id   ';        
        $query .= 'LEFT JOIN '.self::$table_sale_type_object.' AS tob ON tob.id = o.object_type_id   ';
        $query .= 'LEFT JOIN '.self::$table_sale_object_meta_value.' AS mov ON mov.object_id = o.id   ';
        $query .= 'LEFT JOIN '.self::$table_sale_object_meta.' AS mo ON mo.id = mov.meta_id   ';
                
        if(!empty($where))
        {
            $count = 0;
            $query .= 'WHERE ';
            foreach($where as $value)
            {
                $query .= ($count > 0)? 'AND ' : '';
                $query .= $value['index'].$value['compare'].$value['value'].' ';
                $count++;
            }
        } 
        
        $results_sale = $wpdb->get_results( $query ); 
        
        foreach($results_sale as $result)
        {            
            $datas[$result->id]['id'] = $result->id;
            $datas[$result->id]['contact_id'] = $result->contact_id;
            $datas[$result->id]['contact_name'] = $result->first_name.' '.$result->last_name;
            $datas[$result->id]['contact_email'] = $result->email;
            $datas[$result->id]['lots']['id'] = $result->lot_id;
            
            if(!empty($wpdb_external)){
                $lots = LotModel::getLotById($wpdb_external, $result->lot_id);
                $lot = (isset($lots[0]) && !empty($lots[0])) ? $lots[0] : array();
                if(!empty($lot)){
                    $datas[$result->id]['lots']['id'] = (!empty($lot->id)) ? $lot->id : 0;
                    $datas[$result->id]['lots']['title'] = (!empty($lot->title)) ? $lot->title : '';
                    $datas[$result->id]['lots']['description'] = (!empty($lot->description)) ? $lot->description : '';
                    $datas[$result->id]['lots']['price'] = (!empty($lot->price)) ? $lot->price : '';
                }
            }
            
            if(!empty($result->object_id)){
                $datas[$result->id]['objects'][$result->object_id]['id'] = $result->object_id;
                $datas[$result->id]['objects'][$result->object_id]['title'] = $result->object_title;
                $datas[$result->id]['objects'][$result->object_id]['description'] = $result->object_description;
                $datas[$result->id]['objects'][$result->object_id]['price'] = $result->price;
            }
        }

        return $datas;
    }
    
    
    /*
     * 
     */
    public static function insert_sale_management($datas)
    {
        global $wpdb; 
        
        if($wpdb->insert(static::$table_sale, $datas))
        {
            return array('valid' => true, 'id' => $wpdb->insert_id);
        }else{
            return array('valid' => false, 'id' => 0);    
        }        
    }
    
    /*
     * 
     */
    public static function update_sale_management($datas,$where)
    {
        global $wpdb; 
        
        if($wpdb->update(static::$table_sale, $datas, $where))
        {
            return array('valid' => true);
        }else{
            return array('valid' => false);    
        }        
    }
    
    /*
     * 
     */
    public static function deleteSale($where)
    {
        global $wpdb;
        
        if($wpdb->delete(static::$table_sale,$where))
        {
            return true;
        }else{
            return false;
        }
    }
    
    
    /*
     * 
     */
    public static function insert_sale_management_object($datas)
    {
        global $wpdb; 
        
        if($wpdb->insert(static::$table_sale_object, $datas))
        {
            return array('valid' => true, 'id' => $wpdb->insert_id);
        }else{
            return array('valid' => false, 'id' => 0);    
        }        
    }
    
    /*
     * 
     */
    public static function deleteObjectSale($where)
    {
        global $wpdb;
        
        if($wpdb->delete(static::$table_sale_object,$where))
        {
            return true;
        }else{
            return false;
        }
    }
}