<?php

class MetaObjectModel
{
    /*
     * Make header of type object list
     */
    public static function getHeaderTable()
    {
        return array(
            array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'object_type_id', 'name' => 'Type', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'meta_key', 'name' => 'Key', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'meta_slug', 'name' => 'Slug', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'meta_order', 'name' => 'Ordre', 'type_data' => 'base', 'class' => 'w40px center-text')
        );
    }  
    
    /*
     * Make action cell of type object list
     */
    public static function getActionTable()
    {
        $link_admin = get_admin_url().'admin.php';
        $link_add_project = $link_admin.'?page=add_meta_object';
        $edit_url = $link_add_project.'&id=@id';

        return array(
            array('url_action' => $edit_url, 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id') 
        );
    }
    
    public static function selectAllMetaObjects()
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE.' AS t ';
        $query .= 'ORDER BY id ASC ';
        
        return $wpdb->get_results($query, ARRAY_A);
    }

    public static function getListMetaTypeObjects()
    {
        global $wpdb;

        $query  = 'SELECT m.id, t.type, m.meta_key, m.meta_slug ';
        $query .= 'FROM '.getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE.' AS m ';
        $query .= 'LEFT JOIN '.getExternalPrefix().MEO_REALESTATE_SALE_TYPE_OBJECT_TABLE.' AS t  ON t.id = m.object_type_id ';
        $query .= 'ORDER BY t.type, m.meta_order ASC ';

        return $wpdb->get_results($query, ARRAY_A);
    }
    
    public static function selectMetaObjectById($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE.' AS t ';
        $query .= 'WHERE id = '.$id.' ';
        
        return $wpdb->get_results($query);
    }

    public static function getMetaObjectById($id)
    {
        global $wpdb;

        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE.' AS t ';
        $query .= 'WHERE id = '.$id.' ';

        return $wpdb->get_results($query);
    }
    
    public static function selectMetaObjectByTypeObjectId($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE.' AS t ';
        $query .= 'WHERE object_type_id = '.$id.' ';
        
        return $wpdb->get_results($query);
    }
    
    public static function insertMeta($datas)
    {
        global $wpdb;   
        $datas['created_at'] = date('Y-m-d H:m:i');
        if($wpdb->insert(getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE, $datas))
        { 
            $id = $wpdb->insert_id;
            return array('valid' => true, 'id' => $id);            
        }else{
            return array('valid' => false, 'id' => 0);
        }
    }
    
    public static function updateMeta($where, $datas)
    {
        global $wpdb;
        $datas['updated_at'] = date('Y-m-d H:m:i');
        if($wpdb->update( getExternalPrefix().MEO_REALESTATE_SALE_META_OBJECT_TABLE, $datas, $where ))
        { 
            return array('valid' => true, 'id' => $id);            
        }else{
            return array('valid' => false, 'id' => $id);
        }
    }
}