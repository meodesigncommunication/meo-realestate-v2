<?php
global $current_user;
$check_data = true;
$errors = array();
$input_class_name = '';
$input_class_desc = '';
$meta_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];

$project = meo_crm_projects_getProjectInfo($project_id);
$object_types = TypeObjectModel::getAllTypeObjects();

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(!empty($meta_id))
    {
        $datas = array();
        $datas['object_type_id'] = $_POST['object_type_id'];
        $datas['meta_key'] = $_POST['meta_key'];
        $datas['meta_slug'] = $_POST['meta_slug'];
        $datas['meta_order'] = $_POST['meta_order'];

        if(empty($datas['meta_key'])){ $check_data = false; $input_class_meta_key = 'warningField'; }
        if(empty($datas['meta_slug'])){ $check_data = false; $input_class_meta_slug = 'warningField'; }
        if(empty($datas['object_type_id'])){ $check_data = false; $input_class_meta_slug = 'warningField'; }

        if(!empty($datas) && $check_data)
        {
            $where = array('id' => $meta_id);
            $result = MetaObjectModel::updateMeta($where,$datas);

            if($result['valid'])
            {
                header('Location:/meo-crm-realestate-object-meta-list/?project='.$project_id.'&id='.$meta_id);
            }
        }

    }else{

        $datas = array();
        $datas['object_type_id'] = $_POST['object_type_id'];
        $datas['meta_key'] = $_POST['meta_key'];
        $datas['meta_slug'] = $_POST['meta_slug'];
        $datas['meta_order'] = $_POST['meta_order'];

        if(empty($datas['meta_key'])){ $check_data = false; $input_class_meta_key = 'warningField'; }
        if(empty($datas['meta_slug'])){ $check_data = false; $input_class_meta_slug = 'warningField'; }
        if(empty($datas['object_type_id'])){ $check_data = false; $input_class_meta_slug = 'warningField'; }

        if($check_data)
        {
            $resultInsert = MetaObjectModel::insertMeta($datas);
        }else $resultInsert = array('valid' => 0);

        if($resultInsert['valid'])
        {
            $insert_id = $resultInsert['id'];

            if(isset($insert_id) && !empty($insert_id))
            {
                header('Location:/meo-crm-realestate-object-meta-form/?project='.$project_id.'&id='.$insert_id);
            }

        }else{
            $errors[] = 'Erreur durant l\'insertion';
        }
    }

    if(is_array($errors) && !empty($errors))
    {
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($meta_id))
{
    $meta = MetaObjectModel::getMetaObjectById($meta_id);

    $old_meta_key = (!empty($meta[0]->meta_key)) ? $meta[0]->meta_key : '';
    $old_meta_slug = (!empty($meta[0]->meta_slug)) ? $meta[0]->meta_slug : '';
    $old_meta_order = (!empty($meta[0]->meta_order)) ? $meta[0]->meta_order : 0;
    $old_object_type = (!empty($meta[0]->object_type_id)) ? $meta[0]->object_type_id : 0;
}else{
    $old_meta_key = '';
    $old_meta_slug = '';
    $old_meta_order = '';
    $old_object_type = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    ?>
    <form method="post" name="add_status" id="form_status">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>Metadonnées d'objet</h2>
        <div class="input-group">
            <label>Meta</label>
            <input type="text" name="meta_key" id="meta_key" value="<?php echo $old_meta_key ?>" placeholder="Meta" class="<?php echo (!empty($input_class_meta_key))? $input_class_meta_key : '' ?>" />
        </div>
        <div class="input-group">
            <label>Meta slug</label>
            <input type="text" name="meta_slug" id="meta_slug" value="<?php echo $old_meta_slug ?>" placeholder="Slug" class="<?php echo (!empty($input_class_meta_slug))? $input_class_meta_slug : '' ?>" />
        </div>
        <div class="input-group">
            <label>Object type</label>
            <select name="object_type_id" id="object_type_id" class="<?php echo (!empty($input_class_meta_type))? $input_class_meta_type : '' ?>">
                <option value="0"<?php echo (empty($old_object_type)) ? ' selected ' : '' ?>>Choisir un type</option>
                <?php foreach($object_types as $key => $object_type): ?>
                    <option value="<?php echo $object_type['id'] ?>"<?php echo ($old_object_type == $object_type['id']) ? ' selected ' : '' ?>><?php echo $object_type['type'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="input-group">
            <label>Meta ordre</label>
            <input type="text" name="meta_order" id="meta_order" value="<?php echo $old_meta_order ?>" placeholder="Ordre" class="" />
        </div>
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-object-meta-list/?project='.$project_id ?>" name=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
    <?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?>
    <script type="text/javascript">
        window.$ = jQuery;
        $(document).ready(function(){
        });
    </script>
<?php get_footer(); ?>