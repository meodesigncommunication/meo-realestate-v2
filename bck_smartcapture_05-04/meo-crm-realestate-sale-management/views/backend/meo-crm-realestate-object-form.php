<?php 

/*
 * TODO :: Si enregistrement (insert) est ok passer en mode update (ajouter ?id=x dans url)
 */

// Init variables
$count = 0;
$datas = array();
$meta_datas = array();
$id = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id']  : 0 ;
$page_title = (isset($_GET['id']) && !empty($_GET['id'])) ? 'Modifier un objet' : 'Ajouter un objet';
$link_add_type = '';
$flash_message = '';

$helper = new MeoCrmCoreHelper();
$validation = new MeoCrmCoreValidationForm();

$types = TypeObjectModel::selectAllTypeObjects();
$projects = ProjectModel::selectAllProject();

$link_admin = get_admin_url().'admin.php';
$link_list_type_object = $link_admin.'?page=manage_object';

// Init classes
$helperList = new MeoCrmCoreListHelper();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    
    // Init value
    $metas = (!empty($_POST['object_type_id']))? MetaObjectModel::selectMetaObjectByTypeObjectId($_POST['object_type_id']) : array();
    
    // Declare a validation rules
    $rules = array(
        'object_type_id' => 'required|numeric',
        'project_id' => 'required|numeric',
        'title' => 'required|min:5|max:100',
        'description' => 'required|text',
        'price' => 'required|float'
    );
    
    $results_validation = $validation->validationDatas($rules, $_POST);        
    $results = $validation->checkResultValidation($results_validation);
    $validate = $results['result'];
    
    if($validate){
        
        $datas['object_type_id'] = $_POST['object_type_id'];
        $datas['project_id'] = $_POST['project_id'];
        $datas['title'] = $_POST['title'];
        $datas['description'] = $_POST['description'];
        $datas['price'] = $_POST['price'];
        
        foreach($metas as $meta)
        {
            if(!empty($id)){
                $meta_datas[$count]['object_id'] = $id;
            }
            $meta_datas[$count]['value'] = $_POST[$meta->meta_slug];
            $meta_datas[$count]['meta_id'] = $meta->id;
            $count++;
        }
        
        if(isset($id) && !empty($id)){ // Update type
            $results = ObjectModel::updateObject($id, $datas, $meta_datas);
            if($results['valid']){
                $message = 'Update success';
                $flash_message = $helper->getFlashMessageAdmin($message, 1);
            }else{
                $message = 'Update error';
                $flash_message = $helper->getFlashMessageAdmin($message, 0);
            }
        }else{ // Insert type
            $results = ObjectModel::insertObject($datas, $meta_datas);
            if($results['valid']){
                $message = 'Insert success';
                $flash_message = $helper->getFlashMessageAdmin($message, 1);
            }else{
                $message = 'Insert error';
                $flash_message = $helper->getFlashMessageAdmin($message, 0);
            }
        }
    }
    
    // Update data in get methode 
    // Select data to show in form    
    $object = (isset($results['id']) && !empty($results['id'])) ? ObjectModel::selectObjectById($results['id']) : array();
    $type_id = (isset($object[0]->object_type_id) && !empty($object[0]->object_type_id)) ? $object[0]->object_type_id : 0;
    $project_id = (isset($object[0]->project_id) && !empty($object[0]->project_id)) ? $object[0]->project_id : 0;
    $title = (isset($object[0]->title) && !empty($object[0]->title)) ? $object[0]->title : '';
    $description = (isset($object[0]->description) && !empty($object[0]->description)) ? $object[0]->description : '';
    $price = (isset($object[0]->price) && ($object[0]->price < 0 || $object[0]->price <> '') ) ? $object[0]->price : '';
    
    if(isset($object[0]->object_type_id) && !empty($object[0]->object_type_id)){
        $metas = MetaObjectModel::selectMetaObjectByTypeObjectId($object[0]->object_type_id);
        $meta_values = ObjectModel::selectMetaValueByObjectId($results['id']);
    }else{
        $metas = array();
        $meta_values = array();
    }
    
}else{
    
    // Update data in get methode 
    // Select data to show in form    
    $object = (isset($_GET['id']) && !empty($_GET['id'])) ? ObjectModel::selectObjectById($_GET['id']) : array();
    $type_id = (isset($object[0]->object_type_id) && !empty($object[0]->object_type_id)) ? $object[0]->object_type_id : 0;
    $project_id = (isset($object[0]->project_id) && !empty($object[0]->project_id)) ? $object[0]->project_id : 0;
    $title = (isset($object[0]->title) && !empty($object[0]->title)) ? $object[0]->title : '';
    $description = (isset($object[0]->description) && !empty($object[0]->description)) ? $object[0]->description : '';
    $price = (isset($object[0]->price) && ($object[0]->price < 0 || $object[0]->price <> '') ) ? $object[0]->price : '';
    
    if(isset($object[0]->object_type_id) && !empty($object[0]->object_type_id)){
        $metas = MetaObjectModel::selectMetaObjectByTypeObjectId($object[0]->object_type_id);
        $meta_values = ObjectModel::selectMetaValueByObjectId($_GET['id']);
    }else{
        $metas = array();
        $meta_values = array();
    }
}
?>
<div class="wrap meo-crm-object-list">
    <?php 
        if(!empty($flash_message))
        {
            echo $flash_message;    
        }    
    ?>
    <h1><?php echo $page_title; ?><a class="page-title-action" href="<?php echo $link_list_type_object; ?>">Retour</a></h1>
    <form name="from_meta_object" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_object" id="id_object" value="<?php echo $id ?>" />
        <table class="form-table meo-form-table">
            <tr class="form-field form-required">
                <th>
                    <label for="type_object_id">
                        Type d'objet
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <select name="object_type_id" id="object_type_id" >
                        <option value="0">Choose object type</option>
                        <?php foreach($types as $type): ?>
                            <?php $selected = ($type_id == $type['id']) ? 'selected=true' : ''; ?>
                            <option value="<?php echo $type['id'] ?>" <?php echo $selected ?>><?php echo $type['type'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="project_id">
                        Projet lié
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <select name="project_id" id="project_id" >
                        <option value="0">Choose project</option>
                        <?php foreach($projects as $project): ?>
                            <?php $selected = ($project_id == $project['id']) ? 'selected=true' : ''; ?>
                            <option value="<?php echo $project['id'] ?>" <?php echo $selected ?>><?php echo $project['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="title">
                        Titre de l'objet
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="title" id="title" placeholder="Titre" value="<?php echo $title ?>" />
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="description">
                        Description de l'objet
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <textarea name="description" id="description"><?php echo $description ?></textarea>
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="price">
                        Prix de l'objet
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="price" id="price" placeholder="Prix" value="<?php echo $price ?>" />
                </td>
            </tr>            
        </table>
        
        <table class="form-table meo-form-table" id="table-meta-form-object">
            <?php if(!empty($metas)): ?>
                <?php foreach($metas as $meta): ?>
                    <?php foreach($meta_values as $meta_data): ?>
                        <?php if($meta_data->meta_id == $meta->id): ?>
                            <tr class="form-field form-required">
                                <th>
                                    <?php echo $meta->meta_key ?>
                                    <span class="description">(obligatoire)</span>
                                </th>
                                <td>
                                    <input type="text" name="<?php echo $meta->meta_slug ?>" id="<?php echo $meta->meta_slug ?>" placeholder="<?php echo $meta->meta_key ?>" value="<?php echo $meta_data->value; ?>" />
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
        
        <p class="submit">
            <input id="createmeta" class="button button-primary" type="submit" name="createmeta" value="<?php echo $page_title; ?>">
        </p>
    </form>
</div>