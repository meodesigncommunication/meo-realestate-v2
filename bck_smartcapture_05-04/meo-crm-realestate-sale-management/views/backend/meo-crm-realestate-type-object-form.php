<?php 

// Init variables
$datas = array();
$id = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id']  : 0 ;
$page_title = (isset($_GET['id']) && !empty($_GET['id'])) ? 'Modifier un type' : 'Ajouter un type';
$link_add_type = '';
$flash_message = '';
$helper = new MeoCrmCoreHelper();
$validation = new MeoCrmCoreValidationForm();
$header = TypeObjectModel::getHeaderTable();
$list_action = TypeObjectModel::getActionTable();
$types = TypeObjectModel::selectAllTypeObjects();
$link_admin = get_admin_url().'admin.php';
$link_list_type_object = $link_admin.'?page=manage_type_object';

$type_selected = (isset($_GET['id']) && !empty($_GET['id'])) ? TypeObjectModel::selectTypeObjectById($id) : array() ;
$name_type = (isset($type_selected) && !empty($type_selected)) ? $type_selected[0]->type : '';

// Init classes
$helperList = new MeoCrmCoreListHelper();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // Init value
    $id = $_POST['id_type'];
    $name = $datas['type'] = $_POST['name_type'];
    
    // Declare a validation rules
    $rules = array(
        'name_type' => 'required|min:5|max:100'
    );
    
    $results_validation = $validation->validationDatas($rules, $_POST);        
    $results = $validation->checkResultValidation($results_validation);
    $validate = $results['result'];
    
    if($validate){
        if(isset($id) && !empty($id)){ // Update type
            $results = TypeObjectModel::updateType($id, $datas);
            if($results['valid']){
                $message = 'Update success';
                $flash_message = $helper->getFlashMessageAdmin($message, 1);
            }else{
                $message = 'Update error';
                $flash_message = $helper->getFlashMessageAdmin($message, 0);
            }
        }else{ // Insert type
            $results = TypeObjectModel::insertType($datas);
            if($results['valid']){
                $message = 'Insert success';
                $flash_message = $helper->getFlashMessageAdmin($message, 1);
            }else{
                $message = 'Insert error';
                $flash_message = $helper->getFlashMessageAdmin($message, 0);
            }
        }
    }
    
    // Reset input value
    $id = $results['id'];
    $name_type = $name;
}

?>
<div class="wrap meo-crm-object-list">
    <?php 
        if(!empty($flash_message))
        {
            echo $flash_message;    
        }      
    ?>
    <h1><?php echo $page_title; ?><a class="page-title-action" href="<?php echo $link_list_type_object; ?>">Retour</a></h1>
    <form name="from_project" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_type" id="id_type" value="<?php echo $id ?>" />
        <table class="form-table meo-form-table">
            <tr class="form-field form-required">
                <th>
                    <label for="name_project">
                        Nom du type
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="name_type" id="name_project" placeholder="Nom du type" value="<?php echo (!empty($name_type))? $name_type : ''; ?>" />
                </td>
            </tr>
        </table>
        <p class="submit">
            <input id="createtype" class="button button-primary" type="submit" name="createtype" value="<?php echo $page_title; ?>">
        </p>
    </form>
</div>