<?php 
    global $wpdb;
    $link_admin = get_admin_url().'admin.php';
    $projects = ProjectModel::selectAllProject(); 
    $link_list_type_object = $link_admin.'?page=manage_sale';
?>
<h1>Gestion des ventes</h1>
<div class="filter-sale">
    <label>Project List</label>
    <select id="project-list-sale" name="project-list-sale" class="project-list-sale">
        <option value="0">Choose a project</option>
        <?php foreach($projects as $project): ?>
            <option value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div id="sale-list-content">
    
</div>