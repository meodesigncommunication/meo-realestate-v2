////////////
// FIELDS //
////////////

function accessManagement_update(userid, pluginToHTML){
	var formId = "form-"+userid+"-"+pluginToHTML;
	var access = 0;
	if(jQuery("form#"+formId+" input.accessCheck").is(':checked')){
		access = 1;
	}
	
	// Ajax code
	var data = {
		'action': 'update_access_management',
		'userID': userid,
		'pluginToHTML': pluginToHTML,
		'access': access
	};
	jQuery.post(meo_crm_access_management_ajax.ajax_url, data, function(response) {
		jQuery("form#"+formId+" span").html(response);
		jQuery("form#"+formId+" span").hide(2000);
		jQuery("form#"+formId+" span").html();
		jQuery("form#"+formId+" span").show();
	});
}