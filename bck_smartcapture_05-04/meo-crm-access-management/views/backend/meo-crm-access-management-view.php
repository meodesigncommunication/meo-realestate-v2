<?php 
function meo_crm_access_management_admin_page(){
	
	# List all plugins
	$plugins = get_plugins();
	
	# List all clients
	$users = get_users(array('role' => CLIENT_ROLE));
	
	# Get Access Management data from db
	$accessManagementMatrix = MeoCrmAccessManagement::getAccessManagementRecords();
	
	foreach($accessManagementMatrix as $k => $accessRecord){
		// $accessManagementUserMatrix[$accessRecord['user_id']] = array( $accessRecord['plugin'] => $accessRecord['access'] );
		$userArray = 'user_'.$accessRecord['user_id'];
		
		if(!isset($$userArray) && !is_array($$userArray)){
			$$userArray = array();
		}
		
		//$$userArray[$accessRecord['plugin']] = $accessRecord['access'];
		if($accessRecord['access'] == 1) {
			array_push($$userArray, $accessRecord['plugin']);
		}
	}
	
	echo '<h3>MEO CRM Plugins</h3>';
	
	# Build Matrix HTML
	echo '<table id="meo_crm_access_management_table" cellspacing="5" cellpadding="5" border="1">';
	echo '<tr><th></th>';
	foreach($plugins as $pluginPath => $pluginDetails){
		
		if( strpos($pluginPath, 'meo') !== false && is_plugin_active( $pluginPath ) && strpos($pluginPath, 'access') === false && strpos($pluginPath, 'core') === false ){
			echo '<th>'.str_replace('MEO CRM ', '', $pluginDetails['Name']).'</th>';
		}
		
	}
	echo '</tr>';
	foreach($users as $k => $userObj){
		
		// Dynamic Var
		$userArrayToCheck = 'user_'.$userObj->data->ID;
		
		echo '<tr>';
		echo '<td class="mgmt_table_clientname">'.$userObj->data->user_nicename.'</td>';
		foreach($plugins as $pluginPath => $pluginDetails){
		
			if( strpos($pluginPath, 'meo') !== false && is_plugin_active( $pluginPath ) && strpos($pluginPath, 'access') === false && strpos($pluginPath, 'core') === false ){
				
				// Dynamic Var 
				$checked = '';
				if(is_array($$userArrayToCheck) && in_array($pluginPath, $$userArrayToCheck)) { $checked = ' checked="checked"'; }
				
				// Build the matrix
				$pluginToHTML = str_replace(' ', '_', strtolower($pluginDetails['Name']));
				echo '<td><form id="form-'.$userObj->data->ID.'-'.$pluginToHTML.'"><input type="checkbox" class="accessCheck" id="'.$userObj->data->ID.'-'.$pluginToHTML.'" '.$checked.' /><input type="button" class="access-validator-btn" value="save"onclick="accessManagement_update(\''.$userObj->data->ID.'\', \''.$pluginToHTML.'\')" /><span></span></form></td>';
			}
		
		}
		echo '</tr>';
	}
	echo '</table>';
	
}
/*
echo '<div>Present a nice table of courtiers availability.<br/>Possibility to put some receiver on hold, during vacation for example.</div>';

$records = MeoCrmUsersAvailability::getRecords();

# Building records headers HTML code
$recordHeaders =  meo_crm_users_availability_getTableHeaders();
$recordHeadersHTML = '';
	foreach($recordHeaders as $keyHeader => $recordHeader){
		$recordHeadersHTML .= '<td class="users_availability-td-secondary">'.$recordHeader.'</td>';
	}
$recordHeadersSize = count($recordHeaders)+1;
?>
<div id="usersAvailabilityPageAdmin">
<table cellspacing="10" cellpadding="10" border="1" id="usersAvailabilityTable">
<?php 
	$initialSite = 0;
	foreach($records as $record) {
		
		// Prepare Extra Cells for the current record
		$extraCellsHTML = meo_crm_users_availability_prepareExtraCells($record);
		
		// New Project records
		if($record['project_id'] != $initialProject) {
			$initialProject = $record['project_id'];
			// New line
			$projectName = "Project ".$initialProject; // TODO :: Create function to get project infos
			
			echo '<tr><td colspan="'.$recordHeadersSize.'" class="users_availability-td-primary">'.$projectName.'</td></tr>';
			#echo '<tr>
			#		<td class="users_availability-td-primary">Courtier</td>
			#		<td class="users_availability-td-secondary">Disponibilit&eacute;</td>
			#		<td class="users_availability-td-secondary">Activ&eacute;</td>
			#		<td class="users_availability-td-secondary">Dernier Envoi</td>
			#	</tr>';
			
			echo '<tr><td class="users_availability-td-primary">Courtier</td>'.$recordHeadersHTML.'</tr>';
		}
		
		// User Info
		$currentUser = get_user_by('id', $record['user_id']);
		
		echo '<tr id="'.$currentUser->data->ID."-".$record['project_id'].'" data-userid="'.$currentUser->data->ID.'" data-projectid="'.$record['project_id'].'">
				<td class="users_availability-td-secondary">'.$currentUser->data->display_name.'</td>
				<td class="availability editable">'.($record['availability'] == 1 ? 'disponible' : 'indisponible').'</td>
				<td class="on_hold editable">'.($record['on_hold'] == 1 ? 'actif' : 'inactif').'</td>
				<td>'.$record['last_send'].'</td>
				'.$extraCellsHTML.'
			</tr>';
	}
	
	?>
</table>
</div>
<?php 
*/
do_action('accessManagementPageAdmin');