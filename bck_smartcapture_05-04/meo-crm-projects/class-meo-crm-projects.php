<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoCrmProjects {
	
	# Projects Attributes
	
		private $id;
		private $name;
		private $url;
		private $app_enabled;
		private $updated_at;
		private $created_at;
	
		public function __construct() { }
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
	
	# Manage Project in the Database
	
		###########
		# PROJECT #
		###########
		
		# ADD
		public static function addProject ($data){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_TABLE;
				
			if($wpdb->insert( $table, $data )) {
				return $wpdb->insert_id;
			}
			return false;
		}
		
		# UPDATE
		public static function updateProject ($data, $where){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_TABLE;
				
			return $wpdb->update( $table, $data, $where );
		}
		
		# DELETE
		public static function deleteProjectById ($projectID){
			
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_TABLE;
				
			// Will return 1 or false
			return $wpdb->delete( $table, array( 'id' => $projectID ) );
		}
		
		################
		# PROJECT USER #
		################
		
		# ADD
		public static function addProjectUser ($data){
				
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE;
		
			if($wpdb->insert( $table, $data )) {
				return $wpdb->insert_id;
			}
			return false;
		}
		
		# UPDATE
		public static function updateProjectUser ($data, $where){
				
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE;
		
			return $wpdb->update( $table, $data, $where );
		}
		
		# DELETE
		public static function deleteProjectUserByIds ($projectID, $userID){
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE;
		
			// Will return 1 or false
			return $wpdb->delete( $table, array( 'project_id' => $projectID, 'user_id' =>$userID ) );
		}
		
		# DELETE
		public static function deleteProjectUserByProjectId ($projectID){
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE;
		
			// Will return 1 or false
			return $wpdb->delete( $table, array( 'project_id' => $projectID ) );
		}
		
		# DELETE
		public static function deleteProjectUserByUserId ($userID){
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE;
		
			// Will return 1 or false
			return $wpdb->delete( $table, array( 'user_id' =>$userID ) );
		}
		
	# Get Projects
		
		#getProjects
		
		public static function getProjects (){
				
			global $wpdb;
				
			// Get Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CRM_PROJECTS_TABLE . " ORDER BY id ASC";
				
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) {
				$return = array();
				
				foreach($records as $record){
					$return[$record['id']] = $record;
				}
				return $return;
			}
			
			return false;
		}
		
		#getProjectByID
		
		public static function getProjectByID ($ProjectID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix . MEO_CRM_PROJECTS_TABLE . " WHERE id=". $ProjectID."";
			
			$record = $wpdb->get_results($query, ARRAY_A);
		
			if($record && is_array($record) && count($record) > 0) return reset($record);
		
			return false;
		}
		
		# getProjectsByUserID
		
		public static function getProjectsByUserID($userID, $returnType = ARRAY_A)
		{
			global $wpdb;
			
			$WHERE = " INNER JOIN ". $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE ." AS pu ON p.id = pu.project_id 
						WHERE user_id = ". $userID;
			
			# Check if user is an admin, this case, get all the projects
			if(user_can( $userID, 'administrator' )){
				$WHERE = '';
			}
			
			$query  = "SELECT p.* FROM " . $wpdb->prefix . MEO_CRM_PROJECTS_TABLE . " AS p ". $WHERE;
			
			$results = $wpdb->get_results($query, $returnType);
		
			return $results;
		}
		
		# getClientsByProjectID
		
		public static function getClientsByProjectID($ProjectID){
		
			global $wpdb;
		
			// Get Record
			$query = "SELECT u.ID FROM ". $wpdb->prefix ."users u
						LEFT JOIN ". $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE ." pc ON u.id = pc.user_id 
						LEFT JOIN ". $wpdb->prefix . "usermeta AS um ON u.ID = um.user_id
						WHERE pc.project_id = ". $ProjectID ." 
								AND um.meta_value LIKE '%". CLIENT_ROLE ."%'";
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) {
				$nbResults = count($records);
		
				if($nbResults > 0){
					$clientsId = array();
					foreach($records as $c => $projectUser){
						$clientsId[] = $projectUser['ID'];
					}
					return $clientsId;
				}
					
			}
		
			return false;
		}
		
		# getCourtiersByProjectID
		
		public static function getCourtiersByProjectID($ProjectID){
		
			global $wpdb;
		
			// Get Record
			$query = "SELECT u.ID FROM ".$wpdb->prefix."users u
						LEFT JOIN ".$wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE." pc ON u.id = pc.user_id
						LEFT JOIN ".$wpdb->prefix."usermeta AS um ON u.ID = um.user_id
						WHERE pc.project_id = ".$ProjectID."
								AND um.meta_value LIKE '%courtier%'";
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) {
				$nbResults = count($records);
		
				if($nbResults > 0){
					$courtiersId = array();
					foreach($records as $c => $projectUser){
						$courtiersId[] = $projectUser['ID'];
					}
					return $courtiersId;
				}
					
			}
		
			return false;
		}
		
}
