<?php 
/**
 * The template to edit Projects
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */


global $current_user;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}
$role = '';
if(current_user_can('administrator')) { $role = 'administrator'; }
else if(current_user_can(CLIENT_ROLE)) { $role = 'client'; }
else {
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}
	
get_header();


?>
<div id="primary" class="content-area meo-crm-projects-front-container edit-projects" data-project="none">
		<main id="main" class="project-main" role="main">
			
				<div class="wrap">
					<h2>Edit Projects</h2>
					<em></em>
					<br/>
				</div>
				
				<?php 
				// Get Projects
				$userProjects = meo_crm_projects_getProjects();
				
				if($userProjects){
				
					?>
					<div class="redband">
						<div id="meo-crm-contacts-filter">
							<form>
								<select id="meo-crm-projects-projectSelector" name="siteId">
									<option value="0" class="0">Select a Project</option>
									<?php 
			
									$contactsReady = false;
									$projectContacts = array();
									
									foreach($userProjects as $p => $project){
										
										if($project){
											
											$nbContacts = meo_crm_contacts_countContactsByUserIDandProjectID($current_user->ID, $project['id']);
											
											$nbContactsText = '';
											if($nbContacts > 0){
												// $nbContactsText = ' (<span>'.$nbContacts.'</span>)';
											}
											
											echo '<option value="'.$project['id'].'" class="'.$project['id'].'">'.$project['name'].''.$nbContactsText.'</option>';
										}
										
									}?>
								</select>
							</form>
						</div>
					</div>
					
					<div id="edit-project-container">
					
					
					</div>
					<?php 
				} // /if userprojects
				else MeoCrmCoreTools::meo_crm_core_403();
				?>
		</main>
	</div>
<?php 
get_footer();
?>