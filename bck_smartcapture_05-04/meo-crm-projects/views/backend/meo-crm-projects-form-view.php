<?php
    # Defines / Constants
    define('MESSAGE_PROJET_INSERT', 'Le projet a &eacute;t&eacute; correctement enregistr&eacute;');
    define('MESSAGE_PROJET_INSERT_ERROR', 'Le projet n\'a pas &eacute;t&eacute; correctement enregistr&eacute;');    
    define('MESSAGE_PROJET_UPDATE', 'Le projet a &eacute;t&eacute; correctement mis &agrave; jour');
    define('MESSAGE_PROJET_UPDATE_ERROR', 'Le projet n\'a pas &eacute;t&eacute; correctement mis &agrave; jour');
    
    global $wpdb, $current_user;
    
    // Load class
    $helper = new MeoCrmCoreHelper();
    $validation = new MeoCrmCoreValidationForm();
    
    $link_admin = get_admin_url().'admin.php';
    $link_list_project = $link_admin.'?page=manage_project';
    
    $current_user_id = ($current_user->ID == 1)? 0 : $current_user->ID;
    $user_project = array();
    
    $page_title = (isset($_GET['id']) && !empty($_GET['id']))? 'Modifier un projet' : 'Nouveau projet';
    $path_upload = wp_upload_dir();

    
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        // Set variables
        $flash_message  = '';
        $destination = $path_upload['basedir'].'/projects/'.$_GET['id'].'/';
        
        // Get POST value
        $data = array();
        $data['id_edit'] = $_POST['id_edit'];
        $data['name_project'] = $_POST['name_project'];
        $data['url_project'] = (isset($_POST['url_project']) && !empty($_POST['url_project'])) ? $_POST['url_project'] : '' ;
        $data['signature_project'] = (isset($_POST['signature_project']) && !empty($_POST['signature_project'])) ? $_POST['signature_project'] : '' ;
        $data['app_enabled_project'] = (isset($_POST['app_enabled_project']) && !empty($_POST['app_enabled_project'])) ? $_POST['app_enabled_project'] : '' ;
        $data['app_realestate'] = (isset($_POST['app_realestate']) && !empty($_POST['app_realestate'])) ? $_POST['app_realestate'] : '' ;
        $data['clients'] = $_POST['clients'];
        
        // Declare a validation rules
        $rules = array(
            'name_project' => 'required|min:5|max:100',
            'url_project' => 'url',
            'clients' => 'required|array'
        );
        
        $results_validation = $validation->validationDatas($rules, $_POST);        
        $results = $validation->checkResultValidation($results_validation);
        $validate = $results['result'];
        
        // Test a validation rules
        if($validate)
        { 
            // Check if a update or a new entry
            if(isset($_GET['id']) && !empty($_GET['id']))
            {
                $tmp_file = $_FILES['files']['tmp_name'];
                $type_file = $_FILES['files']['type'];
                $name_file = $_FILES['files']['name'];
                
                /*
                 * START UPDATE ENTRY
                 */
                $project = array(
                    'name' => $data['name_project'],
                    'url' => $data['url_project'],
                    'picture_project' => 'projects/'.$_GET['id'].'/'.$name_file,
                    'signature' => $data['signature_project'],
                    'app_enabled' => (isset($data['app_enabled_project']) && !empty($data['app_enabled_project'])) ? $data['app_enabled_project'] : 0,
                    'app_realestate' => (isset($data['app_realestate']) && !empty($data['app_realestate'])) ? $data['app_realestate'] : 0
                );

                // Upload image project
                $destination = $path_upload['basedir'].'/projects/'.$_GET['id'].'/';

                if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                {
                    $checkUpload = true;
                    $project_old = meo_crm_projects_getProjectInfo($_GET['id']);

                    // Upload the file in folder
                    if(is_dir($destination))
                    {
                        if(!move_uploaded_file($tmp_file, $destination.$_FILES['files']['name']))
                        {
                            $checkUpload = false;
                            $errors[] = 'Erreur durant l\'upload de fichier';
                        }

                    }else{
                        if(MeoCrmCoreFileManager::createFolder($_GET['id'].'/',$path_upload['basedir'].'/projects/', 0777, true))
                        {
                            if(!move_uploaded_file($tmp_file, $destination.$_FILES['files']['name']))
                            {
                                $checkUpload = false;
                                $errors[] = 'Erreur durant l\'upload de fichier';
                            }
                        }
                    }

                    if($checkUpload)
                    {
                        if(!empty($project_old['picture_project']) && file_exists($path_upload['basedir'].'/'.$project_old['picture_project']))
                        {
                            if(unlink($path_upload['basedir'].'/'.$project_old['picture_project'])){ }
                        }
                    }

                }

                
                $clients = array();

                $project['picture_project'] = 'projects/'.$_GET['id'].'/'.$_FILES['files']['name'];
                
                foreach($data['clients'] as $client)
                {
                    $clients[] = array(
                        'project_id' => $_GET['id'],
                        'user_id' => $client
                    );
                }
                
                $whereUpdate = array(
                    'id' => $_GET['id']
                );
                
                $whereDelete = array(
                    'project_id' => $_GET['id']
                );

                if(ProjectModel::updateProject($project,$clients,$whereUpdate,$whereDelete) && createProjectFolder($_GET['id']))
                {
                    apply_filters('projectsFormValidate', $_GET['id'], $data['url_project']);
                    $message  = MESSAGE_PROJET_UPDATE;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 1);
                }else{
                    $message  = MESSAGE_PROJET_UPDATE_ERROR;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                }
                
            }else{  
                
                /*
                 * START NEW ENTRY
                 */
                
                $project = array(
                    'name' => $data['name_project'],
                    'url' => $data['url_project'],
                    'signature' => $data['signature_project'],
                    'app_enabled' => (isset($data['app_enabled_project']) && !empty($data['app_enabled_project'])) ? $data['app_enabled_project'] : 0,
                    'app_realestate' => (isset($data['app_realestate']) && !empty($data['app_realestate'])) ? $data['app_realestate'] : 0,
                    'created_at' => date('Y-m-d H:m:i')
                );   
                
                $result = ProjectModel::insertProject($project,$data['clients']);
                
                if($result['valid'] && createProjectFolder($result['project_id']))
                {

                    $tmp_file = $_FILES['files']['tmp_name'];
                    $type_file = $_FILES['files']['type'];
                    $name_file = $_FILES['files']['name'];

                    // Upload image project
                    $destination = $path_upload['basedir'].'/projects/'.$result['project_id'].'/';

                    $project = array(
                        'picture_project' => 'projects/'.$result['project_id'].'/'.$name_file
                    );

                    $whereUpdate = array(
                        'id' => $result['project_id']
                    );

                    $result = ProjectModel::updateProject($project, array(), $whereUpdate, array());

                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        // Upload the file in folder
                        if(is_dir($destination))
                        {
                            $check_upload = MeoCrmCoreFileManager::uploadMedia($_FILES['files'], $destination);
                        }else{
                            if(MeoCrmCoreFileManager::createFolder($result['project_id'].'/',$path_upload['basedir'].'/projects/',0777,true))
                            {
                                $check_upload = MeoCrmCoreFileManager::uploadMedia($_FILES['files'], $destination);
                            }
                        }
                    }

                    apply_filters('projectsFormValidate', $result['project_id'], $data['url_project']);

                    $message  = MESSAGE_PROJET_INSERT;

                    $flash_message  = $helper->getFlashMessageAdmin($message, 1);

                    echo '<script type="text/javascript">document.location.href="'.$link_list_project.'";</script>';

                }else{

                    $message  = MESSAGE_PROJET_INSERT_ERROR;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 0);

                }

                $data['id_edit'] = $_GET['id'];
                $data['name_project'] = (isset($project['name']) && !empty($project['name'])) ? $project['name'] : '' ;
                $data['url_project'] = (isset($project['url']) && !empty($project['url'])) ? $project['url'] : '' ;
                $data['picture_project'] = (isset($project['picture_project']) && !empty($project['picture_project'])) ? $project['picture_project'] : '' ;
                $data['signature_project'] = (isset($project['signature']) && !empty($project['signature'])) ? $project['signature'] : '' ;
                $data['app_enabled_project'] = (isset($project['app_enabled']) && !empty($project['app_enabled'])) ? $project['app_enabled'] : 0 ;
                $data['app_realestate'] = (isset($project['app_realestate']) && !empty($project['app_realestate'])) ? $project['app_realestate'] : 0 ;
                $data['clients'] = (isset($project['clients']) && !empty($project['clients'])) ? $project['clients'] : array() ;
                
            }
        }else{
            $flash_message  = $helper->getFlashMessageAdmin($results['message'], 0);
        }
    }else{
        
        // Get POST value
        $data = array();       
        
        if(isset($_GET['id']) && !empty($_GET['id']))
        {
            $project = meo_crm_projects_getProjectInfo($_GET['id']);
            
            // TODO::Add some tests
            $data['id_edit'] = $_GET['id'];
            $data['name_project'] = $project['name'];
            $data['url_project'] = $project['url'];
            $data['picture_project'] = $project['picture_project'];
            $data['signature_project'] = $project['signature'];
            $data['app_enabled_project'] = $project['app_enabled'];
            $data['app_realestate'] = $project['app_realestate'];
            $data['clients'] = $project['clients'];
        }else{
            $data['clients'] = array();
        }       
    }

    $allClients = meo_crm_users_getClients();
?>
<div class="wrap meo-crm-users-list">
    <?php 
        if(!empty($flash_message)){
            echo $flash_message;    
        }      
    ?>
    <h1><?php echo $page_title; ?><a class="page-title-action" href="<?php echo $link_list_project; ?>">Retour</a></h1>
    <form name="from_project" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_edit" id="id_edit" value="" />
        <table class="form-table meo-form-table">
            <tr class="form-field form-required">
                <th>
                    <label for="name_project">Nom du projet <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="name_project" id="name_project" placeholder="Nom du projet" value="<?php echo (!empty($data['name_project']))? $data['name_project'] : ''; ?>" />
                </td>
            </tr>
            <tr class="form-field">
                <th>
                    <label for="url_project">URL site du projet</label>
                </th>
                <td>
                    <input type="text" name="url_project" id="url_project" placeholder="URL site du projet" value="<?php echo (!empty($data['url_project']))? $data['url_project'] : ''; ?>" />
                </td>
            </tr>
            <tr class="form-field">
                <th>
                    <label for="signature_project">Signature Email</label>
                </th>
                <td>
                    <?php $content_editor = (!empty($data['signature_project']))? $data['signature_project'] : ''; ?>
                    <?php wp_editor( $content_editor, 'signature_project' ); ?>
                </td>
            </tr>
            <tr>
                <th>
                    <label>Image du projet</label>
                </th>
                <td>
                    <div class="input-group">
                        <input type="file" name="files" id="files" />
                        <?php
                        if(!empty($data['picture_project']))
                        {
                            echo '<img width="200" src="'.$path_upload['baseurl'].'/'.$data['picture_project'].'" />';
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <tr class="form-field">
                <th>
                    <label for="app_enabled_project">Activ&eacute; pour l'application</label>
                </th>
                <td>
                    <?php $checked = ($data['app_enabled_project'])? 'checked="true"' : '' ; ?>
                    <input style="width: 10px !important;" <?php echo $checked; ?> type="checkbox" name="app_enabled_project" id="app_enabled_project" value="1" />
                </td>
            </tr>
            <tr class="form-field">
                <th>
                    <label for="app_realestate">Application immobili&egrave;re</label>
                </th>
                <td>
                    <?php $checked = ($data['app_realestate'])? 'checked="true"' : '' ; ?>
                    <input style="width: 10px !important;" <?php echo $checked; ?> type="checkbox" name="app_realestate" id="app_realestate" value="1" />
                </td>
            </tr>
            
            <tr>
                <th>
                    <label for="app_enabled_project">Clients li&eacute;s</label>
                </th>
                <td>
                    <?php 
                    foreach($allClients as $c => $client){

                        $checked = '';

                        if(is_array($data['clients']) && count($data['clients'])){
                        	
                            foreach($data['clients'] as $client_checked){
                            	
                                if($client_checked == $client['ID']){
                                    $checked = 'checked="true"';
                                }
                            }
                        }
	                    ?>
	                    <div class="meo-group-checkbox">
	                        <input <?php echo $checked; ?> id="client_<?php echo $client['ID'] ?>" class="" type="radio" name="clients[]" value="<?php echo $client['ID'] ?>">
	                        <label for="client_<?php echo $client['ID'] ?>"><?php echo $client['display_name'] ?></label>                    
	                    </div>
	                    <?php 
                    } 
                    ?>
                </td>
            </tr>
        </table>
        <?php do_action('projectsFormFields') ?>
        <p class="submit">
            <input id="createproject" class="button button-primary" type="submit" name="createproject" value="<?php echo $page_title; ?>">
        </p>
    </form>
</div>
