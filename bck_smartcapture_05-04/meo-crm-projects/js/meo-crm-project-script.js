/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.$ = jQuery;

$(document).ready(function(){
   
    /*
     * 
     */
    $('.meo-crm-projects-doaction').click(function(){
        //List action value
        var doaction = $(this).parent().find('select.bulk-action-selector').val();
        var list_ids = {'list':[]};        
        if(doaction == 'delete')
        {
            // Récupère les ids à supprimer
            $('input.meo-crm-list-checkbox ').each(function(){
                if($(this).is(':checked'))
                {
                    list_ids.list.push({'id':$(this).val()});
                }          
            });
           if(confirm('Voulez-vous supprimer ces projets ?'))
            {
                $.post(
                   ajaxurl,
                   {
                       'action': 'multiple_delete_project',
                       'ids': list_ids
                   },
                   function(response){
                        console.log(response);
                        var obj = $.parseJSON(response);
                        $('div#zoneMessage').html(obj.message);
                        $('#content-table').html(obj.table);
                    }
                );
            }
        }
    });
   
    /*
     * 
     */
    $('.meo-crm-projects-search-btn').click(function(){
        
        var search = $('.meo-crm-projects-search-input').val();
        var client_id = $('.bulk-client-selector').val();
        
        console.log(client_id);        
        if(client_id == -1)
        {
            client_id = null;
        }        
        $.post(
            ajaxurl,
            {
                'action': 'getSearchListProjectResult',
                'search': search,
                'client_id': client_id
            },
            function(response){
                $('#content-table').html(response);
            }
        );
    });
   
});
