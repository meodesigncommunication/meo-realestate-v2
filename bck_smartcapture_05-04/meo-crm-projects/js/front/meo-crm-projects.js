/* Functions */

	function submitFormSignatureFileFront()
	{

		jQuery('#form_signature_logo').on('submit',(function(e){

			e.preventDefault();

			var ajaxData = new FormData(this);
			ajaxData.append('action', 'meo_crm_projects_update_project_signature_logo');

			$.ajax({
				url: meo_crm_projects_ajax.ajax_url,
				type: 'POST',
				data: ajaxData,
				contentType: false,
				cache: false,
				processData:false,
				success: function(response)
				{
					console.log(response);
					if(response != '') {
						var obj = $.parseJSON(response);
						jQuery('#img_signature_logo').attr('src', obj.src);
						jQuery('#old_signature_logo').val(obj.old_signature_logo);
					}else{
						console.log('erreur');
					}
				}
			});

		}));
	}

	function saveProjectFieldFront(field, project_id){
		// Init
		var content = '';
		if(field == 'signature'){
			content = tinyMCE.activeEditor.getContent();
		}
		else content = jQuery('#'+field).val();

		// Ajax code
		jQuery.ajax({
			type: 'POST',
			url: meo_crm_projects_ajax.ajax_url,
			data:{
				'action': 'meo_crm_projects_update_project_field_front',
				'fieldName': field,
				'fieldValue': content,
				'projectID': project_id,
			},
			async:false
		}).done(function(response) {
			alert(response);
		});
	}
	
	/* Document ready functions */

	jQuery(document).ready(function() {

		// Action on the project dropdown list
		jQuery("#meo-crm-projects-projectSelector").change(
			function(e){
			
				tinymce.remove();
				
				var project_id = jQuery( "option:selected", this ).val();

			    jQuery.ajax({
					type: 'POST',
					url: meo_crm_projects_ajax.ajax_url,
					data:{
						'action': 'meo_crm_projects_front_edit_project',
						'project_id': project_id
					},
					async:false
				}).done(function(response) {

					jQuery("#edit-project-container").html(response);

					tinymce.init( {
						selector: ".signature"+project_id,
						theme: "modern",
						skin: "lightgray",
						menubar : false,
						statusbar : false,
						toolbar: [
							"bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | undo redo"
						],
						plugins : "paste",
						paste_auto_cleanup_on_paste : true,
						paste_postprocess : function( pl, o ) {
							o.node.innerHTML = o.node.innerHTML.replace( /&nbsp;+/ig, " " );
						}
					} );

					submitFormSignatureFileFront();
				});
		});
		
	});