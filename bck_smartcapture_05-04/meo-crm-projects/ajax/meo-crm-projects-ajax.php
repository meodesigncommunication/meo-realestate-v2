<?php 

# Add Ajax calls and functions


	# Get project data that need to be edited
	
		add_action('wp_ajax_meo_crm_projects_front_edit_project', 'meo_crm_projects_front_edit_project');
		add_action('wp_ajax_nopriv_meo_crm_projects_front_edit_project', 'meo_crm_projects_front_edit_project');
		
		function meo_crm_projects_front_edit_project() {
			
			global $current_user;
			
			if(isset($_POST['project_id']) && $_POST['project_id'] != ''){
				
				# Get project Data
				$project = meo_crm_projects_getProjectInfo($_POST['project_id']);

				$url_length = strlen($project['url']);
				$url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));
				
				echo '<h1>'.$project['name'].'</h1>';
				
				# Edit Project's fields
				?>
		        <table class="form-table meo-form-table">
		            <tr class="form-field">
		                <th>
		                    <label for="signature">Signature Email</label>
		                </th>
		                <td class="email-signature-ajax-container">
		                    <?php 
		                    $content_editor = (!empty($project['signature']))? $project['signature'] : '';
		                    
		                    $settings = array( 	'media_buttons' => false, 
                    							'teeny' => true, 
		                    					'textarea_name' => 'signature',
		                    					'editor_class' => 'signature'.$_POST['project_id'],
                    							'quicktags' => array( 'buttons' => 'strong,em,del,ul,ol,li,close' ),
                    							'tinymce' => array( 'theme_advanced_buttons1' => 'bold, italic, ul, pH, temp' ),
                    					);
		                    
		                   	wp_editor( 	stripslashes($content_editor), 'signature', $settings );
		                   	?>
							<p>
								<input type="submit" name="signature_submit" class="" onclick="saveProjectFieldFront('signature', <?php echo $_POST['project_id']; ?>);" value="Save" />
							</p>
		                </td>
		            </tr>
					<tr class="form-field">
						<th>
							<label for="signature">Image signature</label>
						</th>
						<td class="file-signature-ajax-container">
							<form method="POST" id="form_signature_logo" name="form_signature_logo" enctype="multipart/form-data">
								<input type="hidden" name="project_id" id="project_id_field" value="<?php echo $_POST['project_id']; ?>" />
								<input type="hidden" name="old_signature_logo" id="old_signature_logo" value="<?php echo $project['signature_logo']; ?>" />
								<input type="file" name="signature_logo" id="signature_logo" />
								<img id="img_signature_logo" src="<?php echo $url_site . '/wp/wp-content/uploads/' . $project['signature_logo']; ?>" alt="" />
								<p>
									<input type="submit" id="signature_logo_submit" name="signature_submit" class="" value="Save" />
								</p>
							</form>
						</td>
					</tr>
		        </table>
				<?php 
				# Call other plugins
				do_action('meo_crm_projects_front_edit_project_ajax', $project);
			}
			else echo 'false';
				
			wp_die();
		}
		
	# Save Project's Field Front
	
		add_action('wp_ajax_meo_crm_projects_update_project_field_front', 'meo_crm_projects_update_project_field_front');
		add_action('wp_ajax_nopriv_meo_crm_projects_update_project_field_front', 'meo_crm_projects_update_project_field_front');
		
		function meo_crm_projects_update_project_field_front (){

			$project_id = $_POST['projectID'];

			if(isset($project_id)){

				$project = meo_crm_projects_getProjectInfo($project_id);

				$access = AccessProjectModel::selectAccessProjectByProjectId($project_id);

				// Init Class
				$meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

				// Init variable to connect to external db
				$hostDB = $meoCrmCoreCryptData->decode($access->host_db);
				$nameDB = $meoCrmCoreCryptData->decode($access->name_db);
				$loginDB = $meoCrmCoreCryptData->decode($access->login_db);
				$passwordDB = $meoCrmCoreCryptData->decode($access->password_db);

				// Init variable to connect to FTP
				$hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
				$baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
				$loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
				$passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);

				// Create a new WPDB link to the other DB
				$external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);

				$data = array( $_POST['fieldName'] => $_POST['fieldValue']);
				$where = array( 'id' => $project_id );
				
				if(MeoCrmProjects::updateProject ($data, $where)){

					$wp_options = OptionModel::selectOptionByName($external_wpdb,'"signature"');

					if(isset($wp_options) && !empty($wp_options))
					{
						$dataUpdateOption = array(
							'option_value' => $_POST['fieldValue']
						);
						$whereUpdateOption = array(
							'option_name' => 'signature'
						);
						$resultUpdate = OptionModel::updateOption($external_wpdb,$dataUpdateOption,$whereUpdateOption);

						if($resultUpdate['success'])
						{
							echo 'Saved';
						}else echo 'error saving update option external';

					}else{
						$dataInsertOption = array(
							'option_name' => 'signature',
							'option_value' => $_POST['fieldValue']
						);
						$resultInsert = OptionModel::insertOption($external_wpdb,$dataInsertOption);

						if($resultInsert['success'])
						{
							echo 'Saved';
						}else echo 'error saving insert option external';
					}
				}
				else echo 'Error saving project';
			}
			else echo 'No project was selected';
			
			wp_die();
		}


	# Save Project's Field Signature_logo

		add_action('wp_ajax_meo_crm_projects_update_project_signature_logo', 'meo_crm_projects_update_project_signature_logo');
		add_action('wp_ajax_nopriv_meo_crm_projects_update_project_signature_logo', 'meo_crm_projects_update_project_signature_logo');

		function meo_crm_projects_update_project_signature_logo (){

			$data = array();
			$folder_created = false;
			$project_id = $_POST['project_id'];
			$old_signature_logo = $_POST['old_signature_logo'];

			if(isset($project_id)){

				$project = meo_crm_projects_getProjectInfo($project_id);

				$access = AccessProjectModel::selectAccessProjectByProjectId($project_id);

				// Init Class
				$meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

				// Init variable to connect to external db
				$hostDB = $meoCrmCoreCryptData->decode($access->host_db);
				$nameDB = $meoCrmCoreCryptData->decode($access->name_db);
				$loginDB = $meoCrmCoreCryptData->decode($access->login_db);
				$passwordDB = $meoCrmCoreCryptData->decode($access->password_db);

				// Init variable to connect to FTP
				$hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
				$baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
				$loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
				$passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);

				// Create a new WPDB link to the other DB
				$external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);

				// Create a connection with external FTP
				$ftp = ftp_connect($hostFTP, 21);
				ftp_login($ftp, $loginFTP, $passwordFTP);

				$content_dir_path = $baseDirFTP.'uploads/';

				if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, 'project'))
				{
					$folder_created = true;
				}

				$url_length = strlen($project['url']);
				$url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

				$content_dir_url = 'project/';
				$content_dir_path = $content_dir_path.'project/';

				if($folder_created) {
					// ************************************************************
					// Upload signature logo
					// ************************************************************
					if (!empty($_FILES['signature_logo']['name'])) {
						$nextStep = true;
						if (!empty($old_signature_logo)) {
							if (!ftp_delete($ftp, $baseDirFTP . 'uploads/' . $old_signature_logo)) {
								$errors[] = 'Ancienne signature logo pas supprimé !!';
								$nextStep = false;
							}
						}
						if ($nextStep) {
							$tmp_file = $_FILES['signature_logo']['tmp_name'];
							$type_file = $_FILES['signature_logo']['type'];
							$name_file = $_FILES['signature_logo']['name'];

							if ($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png') {
								if (ftp_put($ftp, $content_dir_path . $name_file, $tmp_file, FTP_BINARY) === true) {

									$data['signature_logo'] =  $content_dir_url . $name_file;
									$where = array( 'id' => $project_id );

									if(MeoCrmProjects::updateProject ($data, $where)){

										$wp_options = OptionModel::selectOptionByName($external_wpdb,'"signature_logo"');

										if(isset($wp_options) && !empty($wp_options))
										{
											$dataUpdateOption = array(
												'option_value' => $content_dir_url . $name_file
											);
											$whereUpdateOption = array(
												'option_name' => 'signature_logo'
											);
											$resultUpdate = OptionModel::updateOption($external_wpdb,$dataUpdateOption,$whereUpdateOption);

											if($resultUpdate['success'])
											{
												//Lien vers image upload
												$old_signature_logo = $content_dir_url . $name_file;
												$link_signature_logo = $url_site . '/wp/wp-content/uploads/' . $content_dir_url . $name_file;

											}else $errors[] = 'Error saving update Option external';

										}else{
											$dataInsertOption = array(
												'option_name' => 'signature_logo',
												'option_value' => $content_dir_url . $name_file
											);
											$resultInsert = OptionModel::insertOption($external_wpdb,$dataInsertOption);

											if($resultInsert['success'])
											{
												//Lien vers image upload
												$old_signature_logo = $content_dir_url . $name_file;
												$link_signature_logo = $url_site . '/wp/wp-content/uploads/' . $content_dir_url . $name_file;

											}else $errors[] = 'Error saving insert Option external';
										}
									}
									else $errors[] = 'Error saving project';

								} else {
									$errors[] = 'Erreur durant l\'upload du fichier !!';
								}
							} else {
								$errors[] = 'Erreur de type du fichier !!';
							}
						}
					}
				}
			}
			else echo 'No project was selected';

			if(isset($link_signature_logo) && !empty($link_signature_logo))
			{
				echo json_encode(array('src' => $link_signature_logo,'old_signature_logo' => $old_signature_logo));
			}else{
				echo false;
			}

			die();
		}
?>