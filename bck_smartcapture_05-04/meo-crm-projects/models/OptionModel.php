<?php

class OptionModel
{
    private static $table = 'wp_options';

    public static function selectOptionByName($external_wpdb, $name)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' ';
        $query .= 'WHERE option_name = '.$name.' ';

        $results = $external_wpdb->get_results($query);

        return $results;
    }
    public static function insertOption($external_wpdb,$datas)
    {
        $check = true;

        if($external_wpdb->insert(static::$table,$datas) === false)
        {
            $check = false;
        }

        if($check)
        {
            return array(
                'success' => true,
                'id' => $external_wpdb->insert_id
            );
        }else{
            return array(
                'success' => false
            );
        }

    }
    public static function updateOption($external_wpdb,$datas,$where)
    {
        $check = true;

        if($external_wpdb->update(static::$table,$datas,$where) === false)
        {
            $check = false;
        }

        if($check)
        {
            return array(
                'success' => true
            );
        }else{
            return array(
                'success' => false
            );
        }
    }
}