<?php

class MeoCrmProjectController
{
    
    public function storeNewProject()
    {
        
    }
    
    public function validFormProject()
    {
        
    }
    
    public function flashMessage($message, $type)
    {
        if($type)
        {
            $html  = '<div id="message" class="updated notice is-dismissible" style="margin-left: 0px !important">';
            $html .=    '<p>';
            $html .=        $message;
            $html .=    '</p>';
            $html .= '</div>';
        }else{
            $html  = '<div class="error" style="margin-left: 0px !important">';
            $html .=    '<p>';
            $html .=        $message;
            $html .=    '</p>';
            $html .= '</div>';
        }
        return $html;
    }
    
    public function getListProject($search=null,$client_id=null)
    {        
        if(!empty($search) || !empty($client_id))
        {
            $allProjects = ProjectModel::selectSearchProject($search,$client_id);
        }else{
            $allProjects = ProjectModel::selectAllProject();
        }
        
        /*echo "<pre>";
        print_r($allProjects);
        echo "</pre>";*/
        
        return $this->getGenerateTableCell($allProjects);
    }
    
    /*
     * Génére les cellules du tableau de liste des projets
     * 
     * @param $datas(Array) => résultat d'une requête
     * @return 
     */
    private function getGenerateTableCell($datas=array())
    {
        $link_admin = get_admin_url().'admin.php';        
        
        if(is_array($datas) && !empty($datas))
        {
            $html = '';
            
            foreach($datas as $project)
            {
                if($project->app_enabled)
                {
                    $app = '<i style="color:green" class="fa fa-check"></i>';
                }else{
                    $app = '<i style="color:red;" class="fa fa-close"></i>';
                }
                
                $link_edit_project = $link_admin.'?page=add_project&id='.$project->id;
                
                $html .= '<tr>';
                $html .=    '<td class="w30px"><input class="meo-crm-projects-checkbox" id="prject_'.$project->id.'" type="checkbox" name="projects[]" value="'.$project->id.'" /></td>';
                $html .=    '<td class="w30px">'.$project->id.'</td>';
                $html .=    '<td>'.$project->name.'</td>';
                $html .=    '<td>'.$project->url.'</td>';
                $html .=    '<td class="w100px">'.$app.'</td>';
                $html .=    '<td>';
                $html .=        '<a class="button action mr5px" href="'.$link_edit_project.'"><i class="fa fa-pencil"></i></a>';
                $html .=        '<a class="button action mr5px" onclick="trash('.$project->id.')"><i class="fa fa-trash"></i></a>';
                $html .=    '</td>';
                $html .= '</tr>';
            }
        }else{
            $html .= '<tr>';
            $html .=    '<td colspan="5">Aucun résultat</td>';
            $html .= '</tr>';
        }
        
        return $html;
    }
}
