<?php
ini_set('display_errors',1);

/*
 * 
 */
add_action( 'wp_ajax_nopriv_manageFileUploaded', 'manageFileUploaded' );  
add_action( 'wp_ajax_manageFileUploaded', 'manageFileUploaded' );
function manageFileUploaded()
{
    // Init Variable
    $check = true;
    $current_user = wp_get_current_user();
    $path_upload = wp_upload_dir();
    $destination = $path_upload['basedir'].'/projects/'.$_POST['project'].'/medias/';
    
    // Upload the file in folder
    if(is_dir($destination))
    {
        $check_upload = MeoCrmCoreFileManager::uploadMedia($_FILES['files'], $destination);
    }else{
        if(MeoCrmCoreFileManager::createFolder('medias/',$path_upload['basedir'].'/projects/'.$_POST['project'].'/',0777,true))
        {
            $check_upload = MeoCrmCoreFileManager::uploadMedia($_FILES['files'], $destination);
        }else{
            echo 'Folder not created !!!';
        }
    }

    
    // Loop files uploaded for insert in DB
    foreach($check_upload['file_upload_result'] as $file)
    {
        if($file['success'])
        {
            // Init Variable
            $filename = $file['file_name'];
            $mime_type = $file['mime_type'];
            $url = $path_upload['baseurl'].'/projects/'.$_POST['project'].'/medias/';
            $full_url = $url.$filename;
            
            //Insert a new media in DB
            $result = FileModel::insertFile($filename,$full_url,$current_user,$mime_type,$_POST['project']);
            
            //echo 'Last query = '.$result['last_query'].'<br/>';
            
            // Check if insert is valid
            if(!$result['valid'])
            {
                $check = false;
            }
        }
    }
    
    // Check if all insert is validate
    if($check)
    {
        $check_upload['message'] = 'Enregistrement des médias effectué avec succès';
    }else{
        $check_upload['message'] = 'Erreur d\'insertion dans du média';
    }
    
    echo json_encode($check_upload);  
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_manageFileUploadedSpinner', 'manageFileUploadedSpinner' );  
add_action( 'wp_ajax_manageFileUploadedSpinner', 'manageFileUploadedSpinner' );
function manageFileUploadedSpinner()
{
    // Init Variable
    $check = true;
    $count = 0;
    $current_user = wp_get_current_user();
    $path_upload = wp_upload_dir();
    $destination = $path_upload['basedir'].'/projects/'.$_POST['project_id'].'/spinners/'.$_POST['spinner_id'].'/';    

    // Upload the file in folder
    $check_upload = MeoCrmCoreFileManager::uploadMedia($_FILES['files'], $destination);
    
    // Loop files uploaded for insert in DB
    foreach($check_upload['file_upload_result'] as $file)
    {
        if($file['success'])
        {
            // Init Variable
            $filename = $file['file_name'];
            $mime_type = $file['mime_type'];
            $url = $path_upload['baseurl'].'/projects/'.$_POST['project_id'].'/spinners/'.$_POST['spinner_id'].'/';
            $url_dir = $path_upload['basedir'].'/projects/'.$_POST['project_id'].'/spinners/'.$_POST['spinner_id'].'/'.$filename;
            $full_url = $url.$filename;            
            
            //Insert a new media in DB
            $result = FileModel::insertFileSpinner($filename,$full_url,$current_user,$mime_type,$_POST['project_id'],true,$url_dir);
            
            // Check if insert is valid
            if(!$result['valid'])
            {
                $check = false;
            }else{
                // Init attachment spinner
                $datas = array(
                    'spinner_id' => $_POST['spinner_id'],
                    'attachment_id' => $result['post_id'],
                    'sort' => $count
                );
                
                // Insert spinner attachment
                $result = SpinnerModel::insertSpinnerAttachment($datas);
                if(!$result['valid'])
                {
                    $check = false;
                }
            }
        }
        $count++;
    }
    
    // Check if all insert is validate
    if($check)
    {
        $check_upload['message'] = 'Enregistrement des médias effectué avec succès';
    }else{
        $check_upload['message'] = 'Erreur d\'insertion dans du média';
    }
    
    
    echo json_encode($check_upload);  
    die();
}

/*
 *  AJAX function for search some medias
 */
add_action( 'wp_ajax_nopriv_searchMedia', 'searchMedia' );  
add_action( 'wp_ajax_searchMedia', 'searchMedia' );
function searchMedia()
{
    // Init variables
    $search = $_POST['search'];
    $author = $_POST['author'];
    $project = $_POST['project'];
    $mime_type = $_POST['mime_type'];   
    $date_format = $_POST['date_format'];
    $date_start = (!empty($_POST['date_start'])) ? MeoCrmCoreHelper::convertDateFormatToDbFormat($_POST['date_start'],$date_format,true) : '';
    $date_end = (!empty($_POST['date_end'])) ? MeoCrmCoreHelper::convertDateFormatToDbFormat($_POST['date_end'],$date_format,true,'23:59:59') : '';
    
    // Execute search query
    $medias = FileModel::searchMedias($search, $author, $project, $mime_type, $date_start, $date_end);
    
    // Show medias
    echo MeoCrmCoreFileManager::showMedias($medias,false,'search');
    
    die();
}

/*
 * AJAX function for delete a media selected
 */
add_action( 'wp_ajax_nopriv_deleteMedia', 'deleteMedia' );  
add_action( 'wp_ajax_deleteMedia', 'deleteMedia' );
function deleteMedia()
{
    // Init variables
    $id = $_POST['id'];
    $url = $_POST['url'];
    $type_return = $_POST['type'];
    
    if($type_return == 'search'){
        $search = $_POST['search'];
        $author = $_POST['author'];
        $project = $_POST['project'];
        $mime_type = $_POST['mime_type'];   
        $date_format = $_POST['date_format'];
        $date_start = (!empty($_POST['date_start'])) ? MeoCrmCoreHelper::convertDateFormatToDbFormat($_POST['date_start'],$date_format,true) : '';
        $date_end = (!empty($_POST['date_end'])) ? MeoCrmCoreHelper::convertDateFormatToDbFormat($_POST['date_end'],$date_format,true,'23:59:59') : '';
    }
    
    if($type_return == 'gallery'){
        $gallery_id = $_POST['gallery_id'];
        $project_id = $_POST['project_id'];
    }
    
    if($type_return == 'spinner'){
        $spinner_id = $_POST['spinner_id'];
    }
    
    $check = false;
    
    // Execute deleted file
    if(unlink($url))
    {
        if(FileModel::deleteMedia($id))
        {
            $check = true;
        }
    }
    if($check){
        // All media
        if($type_return == 'all')
        {
            $medias = FileModel::selectAllMedias();
            echo MeoCrmCoreFileManager::showMedias($medias);
        }

        // Media search
        if($type_return == 'search')
        {
            $medias = FileModel::searchMedias($search, $author, $project, $mime_type, $date_start=null, $date_end=null);
            echo MeoCrmCoreFileManager::showMedias($medias,false,'search');
        }

        // Media gallery
        if($type_return == 'gallery')
        {
            $gallery_attachment = GalleryModel::selectGalleryAttachmentByGalleryId($gallery_id);
            $medias = FileModel::searchMedias('', '', $project_id, '');

            if(is_array($gallery_attachment) && !empty($gallery_attachment))
            {
                foreach($gallery_attachment as $attachment)
                {
                    if(is_array($medias) && !empty($medias))
                    {
                        foreach($medias as $key => $media)
                        {
                            if($attachment->attachment_id == $key)
                            {
                                $html .= MeoCrmCoreFileManager::showMediaElement($media,$key,true,'gallery',true);
                                $not_show[] = $key;
                            }
                        }
                    }
                }
            }

            if(is_array($medias) && !empty($medias))
            {
                foreach($medias as $key => $media)
                {
                    $show_image = true;

                    foreach($not_show as $value)
                    {
                        if($value == $key)
                        {
                            $show_image = false;
                        }                                
                    }

                    if($show_image)
                    {
                        $html .= MeoCrmCoreFileManager::showMediaElement($media,$key,true,'gallery',false);
                    }
                }
            }else{
                $html .= '<p>Aucun média existant</p>';
            }
        }

        // Media Spinner
        if($type_return == 'spinner')
        {
            $medias = array();
            $attachments_spinner = SpinnerModel::selectSpinnerMedia($spinner_id);
            foreach($attachments_spinner as $attachment_spinner){
                $medias[$attachment_spinner->attachment_id] = FileModel::selectMediaById($attachment_spinner->attachment_id);
            }
            echo MeoCrmCoreFileManager::showMedias($medias);
        }
    }
    
    die();
}

/*
 * AJAX function for delete a gallery selected
 */
add_action( 'wp_ajax_nopriv_deleteAllSpinnerMedia', 'deleteAllSpinnerMedia' );  
add_action( 'wp_ajax_deleteAllSpinnerMedia', 'deleteAllSpinnerMedia' );
function deleteAllSpinnerMedia()
{
    $check = true;
    $helper = new MeoCrmCoreHelper();
    $spinner_id = $_POST['spinner_id'];
    
    $medias = SpinnerModel::selectSpinnerMedia($spinner_id);
    
    foreach($medias as $media)
    {
        if(!SpinnerModel::deleteSpinner($media))
        {
            $check = false;
        }
    }
    
    if($check)
    {
        $message = 'Delete Success';
        $flash_message  = $helper->getFlashMessageAdmin($message, 0);
    }else{
        $message = 'Delete Error';
        $flash_message  = $helper->getFlashMessageAdmin($message, 0);
    }
    
    echo $flash_message;
    
    die();
}

/*
 * AJAX function for delete a gallery selected
 */
add_action( 'wp_ajax_nopriv_deleteGallery', 'deleteGallery' );  
add_action( 'wp_ajax_deleteGallery', 'deleteGallery' );
function deleteGallery()
{ 
    $id = $_POST['id'];    
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmProjectController = new MeoCrmProjectController();  
    
    if(GalleryModel::deleteGallery($id))
    {
        $message = 'Le projet a bien été supprimé';
        $htmlMessage = $meoCrmProjectController->flashMessage($message, 1);
        $galleries = GalleryModel::selectAllGalleries();
        $table = $helperList->getList($galleries, MeoCrmMediaConfiguration::getGalleryListHeader(), MeoCrmMediaConfiguration::getGalleryListAction(), true);
    }else{
        $message = '<strong>ERREUR: </strong>Le projet n\'a pas pu être supprimé';
        $htmlMessage = $meoCrmProjectController->flashMessage($message, 0);
        $galleries = GalleryModel::selectAllGalleries();
        $table = $helperList->getList($galleries,  MeoCrmMediaConfiguration::getGalleryListHeader(), MeoCrmMediaConfiguration::getGalleryListAction(), true);
    }
    
    $result = array(
        'message' => $htmlMessage,
        'table' => $table
    );
    
    echo json_encode($result);
    die();
}

/*
 * AJAX function for delete a gallery selected
 */
add_action( 'wp_ajax_nopriv_deleteSpinner', 'deleteSpinner' );  
add_action( 'wp_ajax_deleteSpinner', 'deleteSpinner' );
function deleteSpinner()
{
    $check = true;
    $urls = array();
    $attachment = array();
    $id = $_POST['id'];
    $previous_url = '';
    $previous_post = '';
    $pervious_spinner = '';
    $pervious_attachment = '';
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmProjectController = new MeoCrmProjectController();  
    
    $results = SpinnerModel::selectAllAttachmentSpinner($id);
    
    if(is_array($results) && !empty($results))
    {
        foreach($results as $result)
        {
            $attachment[0]['metas'][] = $result->meta_id;

            if($result->meta_key == 'path_file')
            {
                $attachment[0]['url'][] = $result->meta_value;
            }

            if($pervious_spinner != $result->spinner_id)
            {
                $attachment[0]['spinner_id'] = $result->spinner_id;
                $pervious_spinner = $result->spinner_id;
            }

            if($previous_post != $result->post_id)
            {
                $attachment[0]['post_id'][] = $result->post_id;
                $previous_post = $result->post_id;
            }

            if($pervious_attachment != $result->spinner_attachment_id)
            {
                $attachment[0]['attachment_id'][] = $result->spinner_attachment_id;
                $pervious_attachment = $result->spinner_attachment_id;
            }
        }

        foreach($attachment[0]['url'] as $url)
        {
            if(!unlink($url))
            {
                $check = false;
            }
        }

        if($check)
        {
            if(SpinnerModel::deleteSpinnerMeta($attachment[0]['spinner_id'],$attachment))
            {
                $message = 'Le spinner a bien été supprimé';
                $htmlMessage = $meoCrmProjectController->flashMessage($message, 1);
                $spinners = SpinnerModel::selectAllSpinners();
                $table = $helperList->getList($spinners, MeoCrmMediaConfiguration::getSpinnerListHeader(), MeoCrmMediaConfiguration::getSpinnerListAction(), true);
            }else{
                $message = '<strong>ERREUR: </strong>Le spinner n\'a pas pu être supprimé';
                $htmlMessage = $meoCrmProjectController->flashMessage($message, 0);
                $spinners = SpinnerModel::selectAllSpinners();
                $table = $helperList->getList($spinners, MeoCrmMediaConfiguration::getSpinnerListHeader(), MeoCrmMediaConfiguration::getSpinnerListAction(), true);
            }
        }
    }else{
        if(SpinnerModel::deleteOnlySpinner($id))
        {
            $message = 'Le spinner a bien été supprimé';
            $htmlMessage = $meoCrmProjectController->flashMessage($message, 1);
            $spinners = SpinnerModel::selectAllSpinners();
            $table = $helperList->getList($spinners, MeoCrmMediaConfiguration::getSpinnerListHeader(), MeoCrmMediaConfiguration::getSpinnerListAction(), true);
        }else{
            $message = '<strong>ERREUR: </strong>Le spinner n\'a pas pu être supprimé';
            $htmlMessage = $meoCrmProjectController->flashMessage($message, 0);
            $spinners = SpinnerModel::selectAllSpinners();
            $table = $helperList->getList($spinners, MeoCrmMediaConfiguration::getSpinnerListHeader(), MeoCrmMediaConfiguration::getSpinnerListAction(), true);
        }
    }
    $result = array(
        'message' => $htmlMessage,
        'table' => $table
    );
    
    echo json_encode($result);        
    die();
}

/*
 *  AJAX function for search some medias
 */
add_action( 'wp_ajax_nopriv_saveMediaSpinner', 'saveMediaSpinner' );  
add_action( 'wp_ajax_saveMediaSpinner', 'saveMediaSpinner' );
function saveMediaSpinner()
{
    // Init variables
    $medias = $_POST['datas'];
    $spinner_id = $_POST['spinner_id'];
    $helper = new MeoCrmCoreHelper();
    
    if(SpinnerModel::updateSpinnerMedia($medias,$spinner_id))
    {
        $message = 'Save success';
        $flash_message  = $helper->getFlashMessageAdmin($message, 1);
    }else{
        $message = 'Save error';
        $flash_message  = $helper->getFlashMessageAdmin($message, 0);
    }
    
    echo $flash_message;
    
    die();
}

add_action( 'wp_ajax_nopriv_changeMediaGallery', 'changeMediaGallery' );  
add_action( 'wp_ajax_changeMediaGallery', 'changeMediaGallery' );
function changeMediaGallery()
{
    global $wpdb;
    
    $html = '';
    $project_id = $_POST['project_id'];
    
    $medias = FileModel::searchMedias('', '', $project_id, '');
    
    //$html = $wpdb->last_query;
    
    foreach($medias as $key => $media)
    {
        $html .= MeoCrmCoreFileManager::showMediaElement($media,$key,'gallery',true,false,'selectedMedia(this)');
    }
    
    if(empty($html))
    {
        $html .= '<p>Il n\'y a pas de médias disponible pour ce projet</p>';
    }else{
        $html .= '<div style="clear:both"></div>';
    }
    
    echo $html;
    
    die();
}