<?php
    $projects = ProjectModel::selectAllProject();
?>
<h1>Upload de médias</h1>
<select class="mt30px" name="project" id="project">
    <?php foreach($projects as $project): ?>
        <option value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
    <?php endforeach; ?>
</select>
<div id="dropfile">
    Drop an image from your computer
</div>
<input type="file" name="file">
<div id="listfile">
    
</div>