    <?php

    # Defines / Constants
    define('MESSAGE_SPINNER_INSERT', 'Le spinner a &eacute;t&eacute; correctement enregistr&eacute;e');
    define('MESSAGE_SPINNER_INSERT_ERROR', 'Le spinner n\'a pas &eacute;t&eacute; correctement enregistr&eacute;e');    
    define('MESSAGE_SPINNER_UPDATE', 'Le spinner a &eacute;t&eacute; correctement mis &agrave; jour');
    define('MESSAGE_SPINNER_UPDATE_ERROR', 'Le spinner n\'a pas &eacute;t&eacute; correctement mis &agrave; jour');
    define('MESSAGE_SPINNER_UPDATE_ATTACHMENT_ERROR', 'Les fichiers attach&eacute; n\'ont pas &eacute;t&eacute; mis à jour correctement');
    define('MESSAGE_SPINNER_FOLDER_ERROR', 'Le spinner est enregistr&eacute; mais le dossier dans upload n\'a pas &eacute;t&eacute; correctement cr&eacute;&eacute;');
    
    global $wpdb;
    
    // Load class 
    $helper = new MeoCrmCoreHelper();
    $validation = new MeoCrmCoreValidationForm();
    
    // Variables
    $insert = '';
    $medias = array();
    $page_title = (isset($_GET['id']) && !empty($_GET['id']))? 'Modifier spinner' : 'Ajouter spinner';
    $current_user = wp_get_current_user();
    $projects = ProjectModel::selectAllProject(wp_get_current_user()); 
    $link_admin = get_admin_url().'admin.php';
    $link_list_spinner = $link_admin.'?page=show_spinner';
    $link_add_spinner = $link_admin.'?page=add_spinner&id=';
    
    
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {      
        
        // Set variables
        $flash_message  = '';
        
        // Url Upload folder        
        $url_folder = wp_upload_dir();
        
        // Get POST value
        $data = array();
        $insert = false;
        $data['id'] = $_POST['spinner_id'];
        $data['author_id'] = $_POST['author_id'];
        $data['project_id'] = $_POST['project_id'];
        $data['name'] = $_POST['name'];
        $data['description'] = $_POST['description'];      
        $medias = $_POST['medias'];
        
        // Declare a validation rules
        $rules = array(
            'name' => 'required|min:5|max:100',
            'description' => 'required|min:5'
        );
        
        //
        $results_validation = $validation->validationDatas($rules, $_POST);        
        $results = $validation->checkResultValidation($results_validation);
        
        // Test a validation rules
        $validate = $results['result'];  
        if($validate)
        {
            
            if((isset($_GET['id']) && !empty($_GET['id'])) || (isset($data['id']) && !empty($data['id'])))
            {
                $medias = FileModel::searchMedias('', '', $data['project_id'], '');                
                
                $where = array(
                    'id' => $_GET['id']
                );
                
                if(SpinnerModel::updateSpinner($data, $where))
                {        
                    
                    if(SpinnerModel::updateSpinnerMedia($_GET['id'],$data_media))
                    {
                        $message  = MESSAGE_SPINNER_UPDATE;
                        $flash_message  = $helper->getFlashMessageAdmin($message, 1);
                    }else{
                        $message  = MESSAGE_SPINNER_UPDATE_ATTACHMENT_ERROR;
                        $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                    }
                }else{
                    $message  = MESSAGE_SPINNER_UPDATE_ERROR;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                }                     
                      
            }else{
                
                $result = SpinnerModel::insertSpinner($data);
                
                if($result['valid'])
                {
                    $insert = true;
                    $id_new = $result['spinner_id']; 
                    
                    // Set param $_GET['id']
                    $_GET['id'] = $result['spinner_id'];
                    
                    // Create folder for spinner
                    $final_folder = $result['spinner_id'].'/';
                    $spinner_folder = $url_folder['basedir'].'/projects/'.$data['project_id'].'/spinners/';
                    $chmod = 0777;
                    $recursive = true;                   
                    
                    if(MeoCrmCoreFileManager::createFolder($final_folder, $spinner_folder, $chmod, $recursive))
                    {
                         $insert = true;
                    }else{
                        $message  = MESSAGE_SPINNER_FOLDER_ERROR;
                        $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                    }                    
                    
                }else{
                    //SpinnerModel::deleteSpinner($_GET['id']);
                    $message  = MESSAGE_SPINNER_INSERT_ERROR;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 0);                    
                }
                
            }
            
        }else{
            $flash_message  = $helper->getFlashMessageAdmin($results['message'], 0);
        }
    }else{
        
        // Get POST value
        $data = array();
        
        if(isset($_GET['id']) && !empty($_GET['id']))
        {     
            $spinner = SpinnerModel::selectSpinnerById($_GET['id']); 
            $data['id'] = $spinner[0]->id;
            $data['author'] = $spinner[0]->author;
            $data['project_id'] = $spinner[0]->project_id;
            $data['name'] = $spinner[0]->name;
            $data['description'] = $spinner[0]->description;
        }
    }
    
    if($insert)
    {
        echo '<script language="javascript" type="text/javascript">window.location.href = "'.$link_add_spinner.$id_new.'";</script>';
    }
    
    if(isset($_GET['id']) && !empty($_GET['id']))
    {
        // Get the spinner media
        $attachments_spinner = SpinnerModel::selectSpinnerMedia($_GET['id']);
        
        foreach($attachments_spinner as $attachment_spinner){
            $medias[$attachment_spinner->attachment_id] = FileModel::selectMediaById($attachment_spinner->attachment_id);
        }
    }
    
?>
<div class="wrap meo-crm-users-list">    
    <div id="message">
    <?php 
        if(!empty($flash_message))
        {
            echo $flash_message;    
        }      
    ?>
    </div>   
    <h1><?php echo $page_title; ?><a class="page-title-action" href="<?php echo $link_list_spinner; ?>">Retour</a></h1>    
    <div id="tabs">
        <ul>
            <?php if(isset($_GET['id']) && !empty($_GET['id'])): ?>
                <li><a href="#tabs-1">Informations</a></li>    
                <?php if(!isset($medias) || empty($medias)): ?>
                    <li><a href="#tabs-2">Upload M&eacute;dias</a></li>
                <?php endif; ?>
                <?php if(isset($medias) && !empty($medias)): ?>
                    <li><a href="#tabs-3">M&eacute;dias</a></li>
                    <li><a href="#tabs-4">Aper&ccedil;u</a></li>
                <?php endif; ?>
            <?php endif; ?>
        </ul> 
        <div id="tabs-1">
            <form name="from_spinner" method="POST" enctype="multipart/form-data"> 
                <input type="hidden" name="spinner_id" id="spinner_id" value="<?php echo (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : $data['id']; ?>" />
                <input type="hidden" name="author_id" id="author_id" value="<?php echo (isset($data['author_id']) && !empty($data['author_id'])) ? $data['author_id'] : $current_user->ID ?>" />
                <table class="form-table meo-form-table">
                    <tr class="form-field form-required">
                        <th>
                            <label for="name_spinner">
                                Projet
                            </label>
                        </th>
                        <td>
                            <select id="project_id" name="project_id">
                                <?php foreach($projects as $project): ?>
                                    <?php $checked  = '' ?>
                                    <?php if(isset($data['project_id']) && $data['project_id'] == $project['id']): ?>
                                        <?php $checked  = 'selected=true' ?>
                                    <?php endif; ?>
                                    <option <?php echo $checked ?> value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="form-field form-required">
                        <th>
                            <label for="name_spinner">
                                Nom du spinner
                                <span class="description">(obligatoire)</span>
                            </label>
                        </th>
                        <td>
                            <input type="text" name="name" id="name" placeholder="" value="<?php echo (isset($data['name'])) ? $data['name'] : '' ?>" />
                        </td>
                    </tr>
                    <tr class="form-field form-required">
                        <th>
                            <label for="description">
                                Description du spinner
                                <span class="description">(obligatoire)</span>
                            </label>
                        </th>
                        <td>
                            <textarea id="description" name="description"><?php echo (isset($data['description'])) ? $data['description'] : '' ?></textarea>
                        </td>
                    </tr>
                </table> 
                <div style="clear:both"></div>
                <p class="submit">
                    <input id="createproject" class="button button-primary" type="submit" name="createproject" value="<?php echo $page_title; ?>">
                </p>
            </form>
        </div>
        <?php if((isset($_GET['id']) && !empty($_GET['id'])) && empty($medias)): ?>
            <div id="tabs-2">
                <?php $url = get_site_url().'/wp-admin/admin-ajax.php'; ?>
                <input type="hidden" id="action" name="action" value="manageFileUploadedSpinner" />
                <form class="box" method="post" action="<?php echo $url; ?>" enctype="multipart/form-data">    
                    <input type="hidden" name="spinner_id" id="spinner_id" value="<?php echo $_GET['id']; ?>" />
                    <input type="hidden" name="project_id" id="project_id_input" value="<?php echo $data['project_id'] ?>" />
                    <input type="hidden" name="author_id" id="author_id" value="<?php echo (isset($data['author_id']) && !empty($data['author_id'])) ? $data['author_id'] : $current_user->ID ?>" />
                    <div class="box__input">
                        <div class="icon">
                            <i class="fa fa-download" aria-hidden="true"></i>
                        </div>        
                        <input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" multiple />
                        <label for="file"><strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
                        <label for="btn-upload">Click on the button for uplaod all files</label>
                        <button class="box__button button button-primary" type="submit">Upload</button>
                    </div>
                    <div class="box__uploading">Uploading&hellip;</div>
                    <div class="box__success">Done!</div>
                    <div class="box__error">Error!</div>
                </form>
                
                <div id="list-upload" class="bloc-drag-drop">
                    <!-- List File Uploaded -->
                </div>
                
                <div id="loader" class="loader hidden">
                    <p>Wait upload in progress</p>
                    <img width="50" src="<?php echo get_site_url().'/wp-content/plugins/meo-crm-core/images/loader.gif'; ?>" alt="wait please" />
                </div>  
                
                <div id="loader-drop" class="loader-drop hidden">
                    <p>Please wait</p>
                    <img width="50" src="<?php echo get_site_url().'/wp-content/plugins/meo-crm-core/images/loader.gif'; ?>" alt="wait please" />
                </div>
                
            </div>
        <?php endif; ?>
        <?php if(isset($medias) && !empty($medias)): ?>
            <div id="tabs-3">
                <div id="container-medias" class="bloc-drag-drop">
                    <?php echo MeoCrmCoreFileManager::showMedias($medias,false,'spinner'); ?>
                </div>
                <div style="clear:both"></div>
                <p class="submit">
                    <input id="spinner-valid-sort" class="button button-primary" type="button" name="createproject" value="Modifier l'ordre">
                    <input id="spinner-delete-all" class="button button-danger" type="button" name="deletemedia" value="Supprimer toutes les medias">
                </p>
            </div>
            <div id="tabs-4">
                <?php echo MeoCrmCoreFileManager::createJSArrayMedia($medias); ?>
                <div id="container-spinner" class="">
                    <!-- /#/ -->
                    <img src="" id="preview_spinner_manuel" />                    
                </div>
            </div>
        <?php endif; ?>
</div>