<?php 
    $link_admin = get_admin_url().'admin.php';
    $link_add_gallery = $link_admin.'?page=add_gallery';
    $link_add_media = $link_admin.'?page=add_media';
    $edit_url = $link_add_gallery.'&id=@id';
    
    $header = MeoCrmMediaConfiguration::getGalleryListHeader();     
    $list_action = MeoCrmMediaConfiguration::getGalleryListAction();  
    
    $galleries = GalleryModel::selectAllGalleries();
    $helperList = new MeoCrmCoreListHelper();    
?>
<div class="wrap meo-crm-media-list">
    <h1>
        Biblioth&egrave;que des m&eacute;dias
        <a class="page-title-action" href="<?php echo $link_add_gallery; ?>">Ajouter une galerie</a>
    </h1>
    <div id="zoneMessage"></div>
    <div id="content-table">
        <?php echo $helperList->getList($galleries, $header, $list_action, true) ?>
    </div>
</div>
