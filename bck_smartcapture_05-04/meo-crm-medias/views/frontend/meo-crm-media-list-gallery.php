<?php

    $link_add_gallery = site_url('meo-crm-gallery-form-medias');
    
    $header = MeoCrmMediaConfiguration::getGalleryListHeader();     
    $list_action = MeoCrmMediaConfiguration::getGalleryListAction();  
    
    $galleries = GalleryModel::selectAllGalleries();
    $helperList = new MeoCrmCoreListHelper();

get_header();
?>

<div class="wrap meo-crm-media-list">
    <h1>
        Biblioth&egrave;que des m&eacute;dias
        <a class="page-title-action" href="<?php echo $link_add_gallery; ?>">Ajouter une galerie</a>
    </h1>
    <div id="zoneMessage"></div>
    <div id="content-table">
        <?php echo $helperList->getList($galleries, $header, $list_action, true) ?>
    </div>
</div>

<?php
get_footer();
?>