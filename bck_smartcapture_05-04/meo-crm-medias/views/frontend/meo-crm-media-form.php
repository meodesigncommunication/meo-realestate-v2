<?php

//Init Define
define('MESSAGE_MEDIA_INSERT', 'Le média a &eacute;t&eacute; correctement enregistr&eacute;e');
define('MESSAGE_MEDIA_INSERT_ERROR', 'Le média n\'a pas &eacute;t&eacute; correctement enregistr&eacute;e');    
define('MESSAGE_MEDIA_UPDATE', 'Le média a &eacute;t&eacute; correctement mis &agrave; jour');
define('MESSAGE_MEDIA_UPDATE_ERROR', 'Le média n\'a pas &eacute;t&eacute; correctement mis &agrave; jour');

// Init variable
$id = (isset($_GET['id']) && !empty($_GET['id']))? $_GET['id'] : 0 ;
$media = FileModel::selectMediaById($id);
$helper = new MeoCrmCoreHelper();
$validation = new MeoCrmCoreValidationForm();

$link_admin = get_admin_url().'admin.php';
$link_manage_gallery = $link_admin.'?page=manage_gallery';

// Get mime type for check media type
list($group,$type) = explode('/', $media['post_mime_type']);

// Envoi du formulaire
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $data['post_title'] = $_POST['title'];
    $data['post_content'] = $_POST['description'];
    
    // Declare a validation rules
    $rules = array(
        'title' => 'required|min:5|max:100',
        'description' => 'required|min:5'
    );
    
    // Validation du formulaire
    $results_validation = $validation->validationDatas($rules, $_POST);     
    $results = $validation->checkResultValidation($results_validation);

    // Test a validation rules
    $validate = $results['result'];  
    if($validate)
    {
        // Update du media
        $where = array(
            'ID' => $id
        );        
        if(FileModel::updateMedia($data, $where))
        {
            $message  = MESSAGE_MEDIA_UPDATE;
            $flash_message  = $helper->getFlashMessageAdmin($message, 1);
        }else{
            $message  = MESSAGE_MEDIA_UPDATE_ERROR;
            $flash_message  = $helper->getFlashMessageAdmin($message, 0);
        } 
    }else{
        $flash_message  = $helper->getFlashMessageAdmin($results['message'], 0);
    }
}else{
    $data['post_title'] = $media['post_title'];
    $data['post_content'] = $media['post_content'];
}

// Détecte le type de fichier pour déterminer l'affichage
switch($group)
{
    case 'image':
        $preview  = '<img id="media-image" src="'.$media['guid'].'" alt="" class="img-rounded" />';
        break;

    case 'audio':
        $preview  = '<div class="media-preview-image" style="background: #dedede no-repeat;"><i class="fa fa-file-audio-o"></i></div>';
        break;

    case 'video':
        $preview  = '<video  id="media-image" controls>';
        $preview .=     '<source src="'.$media['guid'].'" type="'.$media['post_mime_type'].'">';
        $preview .= '</video>';
        break;
    
    case 'application':
        if($type = 'pdf')
        {
            $preview  = '<div id="media-image" class="media-pdf" style="background: #dedede no-repeat;"><iframe src="'.$media['guid'].'" style="width: 100%;height: 100%;border: none;"></iframe></div>';
        }
        break;

    default:
        break;
}

get_header();
?>
<main class="media-list" role="main">
    <section>
        <?php
            if(!empty($flash_message))
            {
                echo $flash_message;
            }
        ?>
        <h1>Édition d'un média <a id="btn-previous-page" class="page-title-action" href="#" onclick="window.history.back()">Retour</a></h1>
        <div class="row" style="margin-left: 0px !important; margin-right: 0px !important;">
            <form name="from_media_data" method="POST">
                <div class="col-data-media">

                    <!--<textarea style="" id="to-copy" spellcheck="false"><?php echo $media['guid'] ?></textarea>
                    <input id="to-copy" spellcheck="false" value="<?php echo $media['guid'] ?>">
                    <button id="copy" type="button">Copy in clipboard<span class="copiedtext"aria-hidden="true">Copied</span></button>
                    <textarea id="cleared" placeholder="Paste your copied content here. Just to test…"></textarea>-->

                    <div class="row">
                        <div class="col-md-7">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nom de l'image</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Nom de l'image" value="<?php echo $data['post_title'] ?>" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Description</label>
                                    <textarea class="form-control" id="description" name="description" rows="3"><?php echo (!empty($data['post_content'])) ? $data['post_content'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">URL</label>
                                    <input type="text" class="form-control" id="url" name="url" disabled="disabled" value="<?php echo $media['guid'] ?>">
                                    <div style="margin-top: 10px;">
                                        <button id="copy" class="btn btn-default" type="button">Copy URL</button>
                                        <input id="to-copy" style="opacity: 0" spellcheck="false" value="<?php echo $media['guid'] ?>">
                                        <textarea id="cleared" style="opacity: 0"  placeholder="Paste your copied content here. Just to test…"></textarea>
                                    </div>
                                    <p class="help-block">Ajouté le <?php echo $media['post_date']?></p>
                                </div>
                                <button type="submit" class="btn btn-default">Enregistrer</button>
                            </form>
                        </div>
                        <div class="col-md-5">
                            <?php echo $preview ?>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </section>
</main>
<script>

    var toCopy  = document.getElementById( 'to-copy' ),
        btnCopy = document.getElementById( 'copy' ),
        paste   = document.getElementById( 'cleared' );

    btnCopy.addEventListener( 'click', function(){
        toCopy.select();
        paste.value = '';

        if ( document.execCommand( 'copy' ) ) {
            btnCopy.classList.add( 'copied' );
            paste.focus();

            var temp = setInterval( function(){
                btnCopy.classList.remove( 'copied' );
                clearInterval(temp);
            }, 600 );

        } else {
            console.info( 'document.execCommand went wrong…' )
        }

        return false;
    } );

    /*jQuery(document).ready(function(){
        jQuery("#media-copy-url").click(function(){

            var copyTextarea = document.querySelector();
            copyTextarea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

           /* alert("execute copy");
            var holdtext = $("#url").select();
            document.execCommand("Copy");
        });
    });*/
</script>
<?php
get_footer();
?>
