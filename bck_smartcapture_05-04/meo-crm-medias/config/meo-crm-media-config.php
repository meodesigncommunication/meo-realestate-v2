<?php
    
    // CONFIG URL
    define('URL_ADMIN_BASE', get_admin_url());
    define('URL_ADMIN_GALLERY_EDIT', URL_ADMIN_BASE.'admin.php?page=add_gallery&id=@id'); 
    define('URL_ADMIN_SPINNER_EDIT', URL_ADMIN_BASE.'admin.php?page=add_spinner&id=@id');   
    
    class MeoCrmMediaConfiguration
    {
        private static $gallery_list_header = array(
            array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w70px center-text'),
            array('key' => 'project_name', 'name' => 'Projet', 'type_data' => 'base', 'class' => ''),
            array('key' => 'display_name', 'name' => 'Auteur', 'type_data' => 'base', 'class' => ''),
            array('key' => 'gallery_name', 'name' => 'Galerie', 'type_data' => 'base', 'class' => ''),
            array('key' => 'slug', 'name' => 'Slug', 'type_data' => 'base', 'class' => ''),
            array('key' => 'private', 'name' => 'Privé', 'type_data' => 'boolean', 'class' => 'w70px center-text'),
            array('key' => 'status', 'name' => 'Actif', 'type_data' => 'boolean', 'class' => 'w50px center-text'),
        );
        
        private static $gallery_list_action = array(
            array('url_action' => URL_ADMIN_GALLERY_EDIT, 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id') ,
            array('click_action' => 'trash(@id,\'deleteGallery\')', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
        );
        
        private static $spinner_list_header = array(
            array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w70px center-text'),
            array('key' => 'spinner_name', 'name' => 'Spinner', 'type_data' => 'base', 'class' => ''),
            array('key' => 'project_name', 'name' => 'Projet', 'type_data' => 'base', 'class' => ''),
            array('key' => 'display_name', 'name' => 'Auteur', 'type_data' => 'base', 'class' => ''),
        );
        
        private static $spinner_list_action = array(
            array('url_action' => URL_ADMIN_SPINNER_EDIT, 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id') ,
            array('click_action' => 'trash(@id,\'deleteSpinner\',\'Voulez-vous supprimer cette élément, ainsi que tous ces médias ?\')', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
        );
        
        public static function getGalleryListHeader()
        {
            return static::$gallery_list_header;
        }
        
        public static function getGalleryListAction()
        {
            return static::$gallery_list_action;
        }
        
        public static function getSpinnerListHeader()
        {
            return static::$spinner_list_header;
        }
        
        public static function getSpinnerListAction()
        {
            return static::$spinner_list_action;
        }
    }

