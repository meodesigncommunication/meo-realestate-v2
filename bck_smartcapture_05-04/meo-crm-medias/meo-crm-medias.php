<?php

/*
Plugin Name: MEO CRM Media
Description: Media Plugin MEO CRM site
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEOGALLERIESTABLE', 'meo_crm_galleries');
define('MEOSPINNERTABLE', 'meo_crm_spinner');
define('MEOSPINNERATTACHEMENTTABLE','meo_crm_spinner_attachement');
define('MEOGALLERIESATTACHEMENTTABLE','meo_crm_gallery_attachement');

# SLUG
define('MEO_MEDIA_SLUG_PLUGIN', 			'meo-crm-medias');
define('MEO_MEDIA_LIST_SLUG', 			'meo-crm-list-medias');
define('MEO_MEDIA_UPLOAD_SLUG', 			'meo-crm-upload-medias');
define('MEO_MEDIA_GALLERY_LIST_SLUG', 			'meo-crm-gallery-list-medias');
define('MEO_MEDIA_GALLERY_FORM_SLUG', 			'meo-crm-gallery-form-medias');

# LINK PAGE TEMPLATE
define('MEO_CRM_MEDIA_LIST_TPL_FRONT', 	'../../'.MEO_MEDIA_SLUG_PLUGIN.'/views/frontend/meo-crm-media-list.php');
define('MEO_CRM_MEDIA_UPLOAD_TPL_FRONT', 	'../../'.MEO_MEDIA_SLUG_PLUGIN.'/views/frontend/meo-crm-upload-media.php');
define('MEO_CRM_MEDIA_FORM_TPL_FRONT', 	'../../'.MEO_MEDIA_SLUG_PLUGIN.'/views/frontend/meo-crm-media-form.php');
define('MEO_CRM_MEDIA_GALLERY_LIST_TPL_FRONT', 	'../../'.MEO_MEDIA_SLUG_PLUGIN.'/views/frontend/meo-crm-media-list-gallery.php');
define('MEO_CRM_MEDIA_GALLERY_FROM_TPL_FRONT', 	'../../'.MEO_MEDIA_SLUG_PLUGIN.'/views/frontend/meo-crm-media-add-gallery.php');

# Required Files
require_once( $plugin_root . 'ajax/functions.php' );
require_once( $plugin_root . 'models/FileModel.php' );
require_once( $plugin_root . 'models/GalleryModel.php' );
require_once( $plugin_root . 'models/SpinnerModel.php' );
require_once( $plugin_root . 'config/meo-crm-media-config.php' );
require_once( $plugin_root . '../meo-crm-projects/models/ProjectModel.php' );
require_once( $plugin_root . '../meo-crm-users/models/UserModel.php' );
require_once( $plugin_root . '../meo-crm-core/classes/meo-crm-core-helper.php' );
require_once( $plugin_root . '../meo-crm-core/classes/meo-crm-core-list-helper.php' );
require_once( $plugin_root . '../meo-crm-core/classes/meo-crm-core-validation-form.php' );

# Globals
global $meo_crm_media_db_version;

$meo_crm_media_db_version = '1.0';

# Plugin activation
function meo_crm_media_activate () {
   global $wpdb;
   global $meo_crm_media_db_version;
   
    $installed_dependencies = false;
    if ( is_plugin_active( 'meo-crm-projects/meo-crm-projects.php' )) {
            $installed_dependencies = true;
    }
    
    if(!$installed_dependencies) {
        
        // WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
        // and this will prevent WordPress to activate the plugin.
        echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Project plugins before', 'meo-realestate').'</h3></div>';
        
        //Adding @ before will prevent XDebug output
        @trigger_error(__('Please install and activate the MEO CRM Project plugins before.', 'meo-realestate'), E_USER_ERROR);
        exit;
        
    }else{

    	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	
        $charset_collate = $wpdb->get_charset_collate();
        
        /*
         * Create table Galleries
         */
        $table_name = $wpdb->prefix . MEOGALLERIESTABLE;        
        $sql = "CREATE TABLE $table_name (
		           id INTEGER(11) NOT NULL AUTO_INCREMENT,
                           project_id INTEGER(11) NOT NULL,
                           author_id INTEGER(11) NOT NULL,
		           name VARCHAR(255) NOT NULL,
		           description TEXT NULL,
		           private TINYINT(2) DEFAULT '0',
                           status TINYINT(2) DEFAULT '0',
		           updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		           created_at TIMESTAMP DEFAULT '0000-00-00 00:00:00',
		           PRIMARY KEY (id)
					) $charset_collate;";
        dbDelta( $sql );
        
        /*
         * Create table Spinner
         */
        $table_name = $wpdb->prefix . MEOSPINNERTABLE;         
        $sql = "CREATE TABLE $table_name (
	            id INTEGER(11) NOT NULL AUTO_INCREMENT,
                    author_id INTEGER(11) NOT NULL,
                    project_id INTEGER(11) NOT NULL,
	            name VARCHAR(255) NOT NULL,
                    slug VARCHAR(255) NOT NULL,
                    description TEXT NULL,
                    count SMALLINT(2) DEFAULT '0',
                    export SMALLINT(2) DEFAULT '0',
                    updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    created_at TIMESTAMP DEFAULT '0000-00-00 00:00:00',
	            PRIMARY KEY (id)
					) $charset_collate;";     
        dbDelta( $sql );
        
        /*
         * Create table make a link beetween spinner and medias
         */
        $table_name = $wpdb->prefix . MEOSPINNERATTACHEMENTTABLE;         
        $sql = "CREATE TABLE $table_name (
	            id INTEGER(11) NOT NULL AUTO_INCREMENT,
                    spinner_id INTEGER(11) NOT NULL,
                    attachment_id INTEGER(11) NOT NULL,
                    sort INTEGER(11) NOT NULL,
                    updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    created_at TIMESTAMP DEFAULT '0000-00-00 00:00:00',
	            PRIMARY KEY (id)
					) $charset_collate;";   
        dbDelta( $sql );
        
        /*
         * Create table make a link beetween galleries and medias
         * (type_attachement défini si c'est une image ou un spinner lié ('post','spinner')
         */
        $table_name = $wpdb->prefix . MEOGALLERIESATTACHEMENTTABLE;         
        $sql = "CREATE TABLE $table_name (
	            id INTEGER(11) NOT NULL AUTO_INCREMENT,
                    gallery_id INTEGER(11) NOT NULL,
                    attachment_id INTEGER(11) NOT NULL,
                    type_attachment VARCHAR(255) NOT NULL DEFAULT 'post',
                    sort INTEGER(11) NOT NULL,
                    updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    created_at TIMESTAMP DEFAULT '0000-00-00 00:00:00',
	            PRIMARY KEY (id)
					) $charset_collate;";   
        dbDelta( $sql );
        
        $projects = ProjectModel::selectAllProject();        
        $base_folder = 'projects/';
        $url_folder = plugin_dir_path( __FILE__ ).'../../uploads/';
        $chmod = 0777;
        $recursive = true;
        $check = true;
        
        foreach($projects as $key => $value)
        {
            $project_folder = $value->id.'/';
            $full_path_folder = $url_folder.$base_folder.$project_folder;
            
            if(!MeoCrmCoreFileManager::createFolder($project_folder, $url_folder.$base_folder, $chmod, $recursive))
            {
                $check = false;
            }
        }  
        
        if(!$check)
        {
            die('Folder Crearted Error');
        }
                
        add_option( 'meo_crm_media_db_version', $meo_crm_media_db_version );         
    }
}
register_activation_hook( __FILE__, 'meo_crm_media_activate' );

/*
 * Remove Menu Item
 */
/*function remove_menu_items() {
    global $menu;
    $restricted = array(__('Media'));
    end ($menu);
    
    while (prev($menu))
    {
        $value = explode(' ',$menu[key($menu)][0]);
        if(in_array($value[0] != NULL?$value[0]:"" , $restricted))
        {
            unset($menu[key($menu)]);
        }
    }
}
add_action('admin_menu', 'remove_menu_items');

function meo_crm_galleries_admin_menu() {
        $page_title = 'Média';
        $menu_title = 'Média';
        $capability = 'manage_options';
        $menu_slug = 'manage_gallery';
        $function = 'page_admin_manage_gallery';
        $icon_url = 'dashicons-admin-media';
        $position = 9;
        add_menu_page ( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
        add_submenu_page($menu_slug,'Upload de média','Upload de média',$capability,'add_media','add_page_admin_media');
        add_submenu_page($menu_slug,'Galeries','Galeries',$capability,'show_galerie','page_admin_show_galerie');
        add_submenu_page($menu_slug,'Ajouter une galerie','Ajouter une galerie',$capability,'add_gallery','add_page_admin_gallery');
        add_submenu_page($menu_slug,'Spinners','Spinners',$capability,'show_spinner','page_admin_show_spinner');
        add_submenu_page($menu_slug,'Ajouter un spinner','Ajouter un spinner',$capability,'add_spinner','add_page_admin_spinner');
        add_submenu_page('','Éditer un média','Éditer un média',$capability,'edit_media','add_page_admin_edit_media');
}
add_action( 'admin_menu', 'meo_crm_galleries_admin_menu' );*/



# Add Scripts and Styles

/*add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script_style_media' );
function load_custom_wp_admin_script_style_media() {

        # JS
        wp_register_script( 'meo_crm_media_dropfile',  '/wp-content/plugins/meo-crm-medias/js/meo-crm-media-dropfile.js', false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_media_dropfile' );
        
        wp_register_script( 'meo_crm_media_script',  '/wp-content/plugins/meo-crm-medias/js/meo-crm-media-script.js', false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_media_script' );
        
        wp_localize_script( 'meo_crm_media_script', 'meo_crm_media_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

        # CSS
        wp_register_style( 'meo_crm_media_css',  '/wp-content/plugins/meo-crm-medias/css/meo-crm-media-style.css', false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_media_css' );

}*/

add_action( 'wp_enqueue_scripts', 'load_custom_script_style_media' );
function load_custom_script_style_media() {

    # JS
    wp_register_script( 'meo_crm_media_dropfile',  '/wp-content/plugins/meo-crm-medias/js/meo-crm-media-dropfile.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_media_dropfile' );

    wp_register_script( 'meo_crm_media_script',  '/wp-content/plugins/meo-crm-medias/js/meo-crm-media-script.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_media_script' );

    wp_localize_script( 'meo_crm_media_script', 'meo_crm_media_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

    # CSS
    wp_register_style( 'meo_crm_media_css',  '/wp-content/plugins/meo-crm-medias/css/meo-crm-media-style.css', false, '1.0.0' );
    wp_enqueue_style( 'meo_crm_media_css' );

}

/* PAGE ADMINISTRATION */
/*function page_admin_manage_gallery() {
    include_once 'views/backend/meo-crm-media-list.php';
}

function page_admin_show_galerie() {
    include_once 'views/backend/meo-crm-media-list-gallery.php';
}

function page_admin_show_spinner() {
    include_once 'views/backend/meo-crm-media-list-spinner.php';
}

function add_page_admin_media() {
    include_once 'views/backend/meo-crm-media-add-media.php';    
}

function add_page_admin_edit_media() {
    include_once 'views/backend/meo-crm-media-form.php';    
}

function add_page_admin_gallery() {    
    include_once 'views/backend/meo-crm-media-add-gallery.php';
}
function add_page_admin_spinner() {    
    include_once 'views/backend/meo-crm-media-add-spinner.php';
}*/

/* FRONTEND TEMPLATE */
add_filter('meo_crm_core_templates_collector', 'meo_crm_medias_upload_templates_register', 1, 1);
function meo_crm_medias_upload_templates_register($pluginsTemplates){

    $pluginsTemplates[MEO_CRM_MEDIA_LIST_TPL_FRONT]   		= 'MEO CRM Medias Liste';
    $pluginsTemplates[MEO_CRM_MEDIA_UPLOAD_TPL_FRONT]   		= 'MEO CRM Medias Upload';
    $pluginsTemplates[MEO_CRM_MEDIA_FORM_TPL_FRONT]   		= 'MEO CRM Medias Form';

    $pluginsTemplates[MEO_CRM_MEDIA_GALLERY_LIST_TPL_FRONT]   		= 'MEO CRM Medias Gallery List';
    $pluginsTemplates[MEO_CRM_MEDIA_GALLERY_FROM_TPL_FRONT]   		= 'MEO CRM Medias Gallery Form';

    return $pluginsTemplates;
}