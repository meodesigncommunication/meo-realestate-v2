<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

global $current_user;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access courtier availability data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}
$role = '';
if(current_user_can('administrator')) { $role = 'administrator'; }
else if(current_user_can(CLIENT_ROLE)) { $role = 'client'; }
else {
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}

get_header(); ?>
<div id="primary" class="content-area meo-crm-contacts-front-container">
	<main id="main" class="project-main" role="main">
	<?php 
	
	# Get User Project(s)
	$userProjects = meo_crm_projects_getProjects();
	
	if($userProjects){
		
		?>
		<div id="usersAvailabilityPageAdmin">
			<table cellspacing="10" cellpadding="10" border="1" id="usersAvailabilityTable">
			<?php 
			
			# Building records headers HTML code
			$recordHeaders =  meo_crm_users_availability_getTableHeaders();
			$recordHeadersHTML = '';
			foreach($recordHeaders as $keyHeader => $recordHeader){
				$recordHeadersHTML .= '<td class="users_availability-td-primary">'.$keyHeader.'</td>';
			}
			$recordHeadersSize = count($recordHeaders)+1;
			
			# Building the rest
			foreach($userProjects as $p => $project){
				
				if(array_key_exists('clients', $project) && is_array($project['clients']) && count($project['clients']) > 0){
					$client = meo_crm_users_getClientDataById(reset($project['clients']));
				}
				
				# Test if Availability is activated for the current Project
				if($project && array_key_exists('availability', $project) && $project['availability'] == 1){
					
					?>
					<tr><th colspan="<?php echo $recordHeadersSize; ?>" class="users_availability-td-primary"><?php echo $project['name']; ?></th></tr>
					<?php 
					
					# Get Courtiers
					$courtiers = meo_crm_users_getCourtiersByProjectID($project['id']);
					
					$clientIsUsed = false;
					# Try Client if Project has no Courtier
					if(!$courtiers){
						$courtiers = array(0 => $client);
						$clientIsUsed = true;
					}
					
					if($courtiers){
						
						if(!$clientIsUsed){
						?>
						<tr><td class="users_availability-td-primary">Courtier</td><?php echo $recordHeadersHTML; ?></tr>
						<?php 
						}
						
						foreach($courtiers as $c => $courtier){
							
							$htmlRow = '<tr id="'.$courtier['ID']."-".$project['id'].'" data-userid="'.$courtier['ID'].'" data-projectid="'.$project['id'].'">
											<td class="users_availability-td-secondary">'.$courtier['display_name'].'</td>';
							# Get Record per Courtier and Project
							$record = MeoCrmUsersAvailability::getAvailabilityByProjectIDUserID($project['id'], $courtier['ID']);
							
							if(!$clientIsUsed){
								
								if($record){
									
									// Prepare Extra Cells for the current record
									$extraCellsHTML = meo_crm_users_availability_prepareExtraCells($record);
									
									
										$htmlRow.= '<td class="availability editable">'.($record['availability'] == 1 ? 'disponible' : 'indisponible').'</td>
													<td class="activity editable">'.($record['activity'] == 1 ? 'actif' : 'inactif').'</td>
													<td>'.$record['last_send'].'</td>
													'.$extraCellsHTML.'';
									
								} // /if records
								else {
									$htmlRow.= '<td colspan="'.($recordHeadersSize-1).'">! No records !</td>';
								}
								
							}
							else {
								
								$htmlRow.= '<td colspan="'.($recordHeadersSize-1).'" style="text-align:center;">'.meo_crm_contacts_countContactsByProjectID($project['id']).' contact(s)</td>';
								
							}
								
								
							
							$htmlRow.= '</tr>';
							
							echo $htmlRow;
							
						} // /foreach courtiers
						
					} // /if courtiers
					
				} // /if projects
					
			} // /foreach userprojects
			?>
			</table>
		</div>
		<?php 
		/*
		
		
		# Access
		echo apply_filters('meo_crm_content_access_filter_front', $pluginContent, MEO_USERS_AVAILABILITY_PLUGIN_SLUG, get_current_user_id());
		*/
		do_action('usersAvailabilityPageFront');
	} // /if userprojects
	else {
		MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access projects list data availability");
		MeoCrmCoreTools::meo_crm_core_403();
	}
?>
	</main>
</div>
<?php 
get_footer(); 
?>