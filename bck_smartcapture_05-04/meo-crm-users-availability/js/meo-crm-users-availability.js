////////////
// FIELDS //
////////////

function usersAvailability_updateField(field, userid, projectid){
	var formClass = field+"_"+userid+"-"+projectid;
	var contentToSave = jQuery("form."+formClass+" select.fieldToSave option:selected").val();
	var contentToPrint = jQuery("form."+formClass+" select.fieldToSave option:selected").text();
	
	// Ajax code
	var data = {
		'action': 'update_users_availability_field',
		'userID': userid,
		'projectID': projectid,
		'fieldName': field,
		'fieldValue': contentToSave
	};
	jQuery.post(meo_crm_users_availability_ajax.ajax_url, data, function(response) {
		// alert('Got this from the server: ' + response);
	});
	// Add class
	jQuery("tr#"+userid+"-"+projectid+" td."+field).html(contentToPrint);
	jQuery("tr#"+userid+"-"+projectid+" td."+field).addClass('editable');
}

jQuery(document).ready(function() {

	jQuery("#usersAvailabilityTable tr td.editable").dblclick(
		function(e){
			var tdClass = jQuery(this).attr('class');
			if(tdClass != 'id' && tdClass != 'id editable' && tdClass.indexOf("editable") > -1) {
				jQuery(this).removeClass('editable');
				var trUserId = jQuery(this).parent().attr('data-userid');
				var trProjectId = jQuery(this).parent().attr('data-projectid');
				var trRecordId = trUserId+'-'+trProjectId;
				var tdFieldName = jQuery(this).attr('class');
				var tdInitialValue = jQuery(this).html();
				
				if(tdFieldName == 'availability') {
					if(tdInitialValue == 'disponible') {var selectOne = 'selected="selected"'; var selectZero = '';}
					else {var selectOne = ''; var selectZero = 'selected="selected"';}
					var tdValue = '<form class="'+tdFieldName+'_'+trRecordId+'"><select name="fieldToSave" class="fieldToSave"><option value="1" '+selectOne+'>disponible</option><option value="0" '+selectZero+'>indisponible</option></select><input type="button" data-id="'+tdFieldName+'_'+trRecordId+'" value="save" class="fieldUpdater" onclick="usersAvailability_updateField(\''+tdFieldName+'\', \''+trUserId+'\', \''+trProjectId+'\')" /></form>';
				}
				else if(tdFieldName == 'activity'){
					if(tdInitialValue == 'actif') {var selectOne = 'selected="selected"'; var selectZero = '';}
					else {var selectOne = ''; var selectZero = 'selected="selected"';}
					var tdValue = '<form class="'+tdFieldName+'_'+trRecordId+'"><select name="fieldToSave" class="fieldToSave"><option value="1" '+selectOne+'>actif</option><option value="0" '+selectZero+'>inactif</option></select><input type="button" data-id="'+tdFieldName+'_'+trRecordId+'" value="save" class="fieldUpdater" onclick="usersAvailability_updateField(\''+tdFieldName+'\', \''+trUserId+'\', \''+trProjectId+'\')" /></form>';
				}
				
				//alert(jQuery(this).attr('class')+' '+jQuery(this).parent().attr('id'));
				jQuery(this).html(tdValue);
			}
	});
	
});