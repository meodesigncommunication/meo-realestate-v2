/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-plan').click(function(){
        resetAllPlan();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-plan-list').css('display','block');
        loadTablePlan();
    });
    
    $('#back-plan-dashboard').click(function(){
        resetAllPlan();       
        $('#meo-crm-realestate-plan-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-plan').click(function(){
        resetAllPlan();  
        $('#meo-crm-realestate-plan-list').css('display','none');
        $('#meo-crm-realestate-plan-form').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'getSpinnerList',
                'id': 0,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                $('#plan_spinner').html(response);
            }
        );
    });
    
    $('#back-plan').click(function(){
        resetAllPlan(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-plan-form').css('display','none');
        $('#meo-crm-realestate-plan-list').css('display','block');
        loadTablePlan();
    });
    
    
    // Execute the submit form
    $('#form_plan').on('submit',(function(e) {
        
        e.preventDefault();
        
        $('#meo-crm-waiting-bloc').css('display','block');
        
        var ajaxData = new FormData(this);        
        ajaxData.append('action', 'savePlan');
        ajaxData.append('project_id', $('#realestate_project_id').val());
        
        if($('#plan_document_old').val() != '')
        {
            ajaxData.append('plan_document_old', $('#plan_document_old').val());
        }
        
        if($('#plan_image_2d_old').val() != '')
        {
            ajaxData.append('plan_image_2d_old', $('#plan_image_2d_old').val());
        } 
        
        if($('#plan_image_3d_old').val() != '')
        {
            ajaxData.append('plan_image_3d_old', $('#plan_image_3d_old').val());
        }
        
        if($('#plan_name').val() != '' && $('#plan_description').val() != '')
        {
            $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                data: ajaxData,
                contentType: false,
                cache: false,
                processData:false,
                success: function(response)
                {
                    console.log(response);
                    var obj = $.parseJSON(response);
                    $('#zone-message').html(obj.message);
                    $('#developpement_id').val(obj.id);
                    
                    $('#link_plan_document').attr('href', $('#url_upload').val()+'/'+obj.document);
                    $('#link_plan_document').css('display', 'block');
                    $('#input_plan_document').val(obj.document);

                    $('#img_plan_image_2d').attr('src', $('#url_upload').val()+'/'+obj.plan_2d);
                    $('#input_plan_image_2d').val(obj.plan_2d);
                    
                    $('#img_plan_image_3d').attr('src', $('#url_upload').val()+'/'+obj.plan_3d);
                    $('#input_plan_image_3d').val(obj.plan_3d);
                    
                    $('#meo-crm-waiting-bloc').css('display','none');
                }
            });
        }else{
            alert('Veuillez renseigner le titre et la description !');
        }
        
    }));
    
});

/* START FUNCTION */

function editPlan(id)
{
    resetAllPlan();  
    $('#meo-crm-waiting-bloc').css('display','block');
    $('#meo-crm-realestate-plan-list').css('display','none');
    $('#meo-crm-realestate-plan-form').css('display','block');
    $('#plan_id').val(id);
    
    // Get Data Plan By ID
    $.post(
        ajaxurl,
        {
            'action': 'getPlanById',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            var obj = $.parseJSON(response); 
            console.log('SPINNER ID = '+obj.spinner_id);
            $('#plan_title').val(obj.title);            
            $('#plan_description').val(obj.description);            
            $('#link_plan_document').attr('href', $('#url_upload').val()+'/'+obj.document);
            $('#link_plan_document').css('display', 'block');
            $('#input_plan_document').val(obj.document);
            $('#img_plan_image_2d').attr('src', $('#url_upload').val()+'/'+obj.plan_2d);
            $('#input_plan_image_2d').val(obj.plan_2d);
            $('#img_plan_image_3d').attr('src', $('#url_upload').val()+'/'+obj.plan_3d);
            $('#input_plan_image_3d').val(obj.plan_3d);            
            $('#meo-crm-waiting-bloc').css('display','none');            
            $.post(
                ajaxurl,
                {
                    'action': 'getSpinnerList',
                    'id': obj.spinner_id,
                    'project_id': $('#realestate_project_id').val()
                },
                function(response){
                    $('#plan_spinner').html(response);
                }
            );
        }
    );
}

function deletePlan(id)
{
    if(confirm('Voulez-vous vraiment supprimer cette element ?')){
        $('#meo-crm-waiting-bloc').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'deletePlan',
                'id': id,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                console.log(response);
                var obj = $.parseJSON(response);
                $('#zone-message').html(obj.message);
                loadTablePlan();
                $('#meo-crm-waiting-bloc').css('display','none');            
            }
        );
    }
}

function loadTablePlan()
{
    $.post(
        ajaxurl,
        {
            'action': 'getPlanList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-plan').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllPlan()
{
    $('#zone-message').html('');
    $('#table-container-plan').html('');    
    $('#plan_id').val('');
    $('#plan_title').val('');
    $('#plan_description').val('');
    $('#plan_document').val('');
    $('#plan_image_2d').val('');
    $('#plan_image_3d').val('');
    $('#input_plan_document').attr('href', '');
    $('#link_plan_document').css('display', 'none');
    $('#img_plan_image_2d').attr('src', '');
    $('#input_plan_image_2d').val('');                    
    $('#img_plan_image_3d').attr('src', '');
    $('#input_plan_image_3d').val('');
}
/* END FUNCTION */