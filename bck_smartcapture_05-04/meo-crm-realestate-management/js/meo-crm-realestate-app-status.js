/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-status').click(function(){
        resetAllStatus();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-status-list').css('display','block');
        loadTableStatus();
    });
    
    $('#back-status-dashboard').click(function(){
        resetAllStatus();       
        $('#meo-crm-realestate-status-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-status').click(function(){
        resetAllStatus();  
        $('#meo-crm-realestate-status-list').css('display','none');
        $('#meo-crm-realestate-status-form').css('display','block');
    });
    
    $('#back-status').click(function(){
        resetAllStatus(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-status-form').css('display','none');
        $('#meo-crm-realestate-status-list').css('display','block');
        loadTableStatus();
    });
    
    
    // Execute the submit form
    $('#form_status').on('submit',(function(e) {
        
        e.preventDefault();
        
        if($('#status_name').val() != '')
        {
            $.post(
                ajaxurl,
                {
                    'action': 'saveStatus',
                    'id': $('#status_id').val(),
                    'status_name': $('#status_name').val(),
                    'project_id': $('#realestate_project_id').val()
                },
                function(response){
                    var obj = $.parseJSON(response);
                    $('#zone-message').html(obj.message);
                    $('#status_id').val(obj.id);
                }
            );
        }else{
            alert('Veuillez renseigner le nom du status !');
        }
        
    }));
    
});

/* START FUNCTION */

function editStatus(id)
{
    resetAllStatus();  
    $('#meo-crm-waiting-bloc').css('display','block');
    $('#meo-crm-realestate-status-list').css('display','none');
    $('#meo-crm-realestate-status-form').css('display','block');
    $('#status_id').val(id);
    $.post(
        ajaxurl,
        {
            'action': 'getStatus',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            var obj = $.parseJSON(response);
            $('#status_id').val(obj.id);
            $('#status_name').val(obj.name);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function deleteStatus(id)
{
    if(confirm('Voulez-vous vraiment supprimer cette element ?')){
        $('#meo-crm-waiting-bloc').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'deleteStatus',
                'id': id,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                var obj = $.parseJSON(response);
                $('#zone-message').html(obj.message);
                loadTableStatus();
                $('#meo-crm-waiting-bloc').css('display','none');            
            }
        );
    }
}

function loadTableStatus()
{
    $.post(
        ajaxurl,
        {
            'action': 'getStatusList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-status').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllStatus()
{
    $('#zone-message').html('');
    $('#table-container-status').html('');    
    $('#status_id').val('');
    $('#status_name').val('');
}
/* END FUNCTION */