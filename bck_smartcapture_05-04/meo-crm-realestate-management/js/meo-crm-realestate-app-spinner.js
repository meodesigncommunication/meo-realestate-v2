/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-spinner').click(function(){  
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-spinner-list').css('display','block');
        loadTableSpinner();        
    });
    
    $('#back-spinner-dashboard').click(function(){ 
        $('#meo-crm-realestate-spinner-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
});

function exportSpinner(id)
{
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'exportSpinner',
            'spinner_id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            var obj = JSON.parse(response);
            $('#zone-message').html(obj.message);  
            loadTableSpinner();
        }
    );
}

function deleteExportedSpinner(id)
{
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'deleteExportedSpinner',
            'spinner_id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            var obj = JSON.parse(response);
            $('#zone-message').html(obj.message);  
            loadTableSpinner();
        }
    );
}

function loadTableSpinner()
{
    $.post(
        ajaxurl,
        {
            'action': 'getProjectSpinner',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            var obj = JSON.parse(response);          
            $('#table-container-spinner-app').html(obj.list_app);
            $('#table-container-spinner-website').html(obj.list_website);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}