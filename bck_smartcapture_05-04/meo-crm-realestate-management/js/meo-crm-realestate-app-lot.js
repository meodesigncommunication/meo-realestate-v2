/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-lot').click(function(){
        resetAllLot();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-lot-list').css('display','block');
        loadTableLot();
        $('#meo-crm-waiting-bloc').css('display','none');
    });
    
    // Duplicate Row
    $('button.duplicate-row').click(function(){
        duplicate_floor();
    });
    
    $('#back-lot-dashboard').click(function(){
        resetAllLot(); 
        $('#meo-crm-realestate-lot-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-lot').click(function(){
        resetAllLot();  
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-lot-list').css('display','none');
        $('#meo-crm-realestate-lot-form').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'getLotFormData',
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                var obj = $.parseJSON(response);
                $('#plan_lot_id').html(obj.plan);
                $('#list_status_id').html(obj.status);
                $('#floor_id_list').html(obj.floor);
                $('#dynamic-form').html(obj.meta);
                $.post(
                    ajaxurl,
                    {
                        'action': 'getFormImagePlanFloor',
                        'project_id': $('#realestate_project_id').val()
                    },
                    function(response){
                        $('#plan_building_floor').html(response);
                        $('#meo-crm-waiting-bloc').css('display','none');
                    }
                );
            }
        );
    });
    
    $('#back-lot').click(function(){
        resetAllLot(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-lot-form').css('display','none');
        $('#meo-crm-realestate-lot-list').css('display','block');
        loadTableLot();
    });
    
    
    // Execute the submit form
    $('#form_lot').on('submit',(function(e) {
        
        e.preventDefault();
        
        $('#meo-crm-waiting-bloc').css('display','block');
        
        var ajaxData = new FormData(this);        
        ajaxData.append('action', 'saveLot');
        ajaxData.append('project_id', $('#realestate_project_id').val());
        
        if($('#lot_title').val() != '' && $('#lot_description').val() != '')
        {
            $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                data: ajaxData,
                contentType: false,
                cache: false,
                processData:false,
                success: function(response)
                {
                    console.log(response);
                    var obj = $.parseJSON(response);                    
                    $('#zone-message').html(obj.message);                   
                    $('#lot_id').val(obj.id);  
                    $('#meo-crm-waiting-bloc').css('display','none');
                }
            });
        }else{
            alert('Veuillez renseigner le titre et la description !');
        }
        
    }));
    
});

/* START FUNCTION */
// Change select list
function hideTableFloorPlan(element){
    if($(element).val() != -1)
    {
        $(element).parent('td').find('table').addClass('meo-unvisible');
    }else{
        $(element).parent('td').find('table').removeClass('meo-unvisible');    
    }
};
    
function duplicate_floor()
{
    var count = 0;        
    $( "table.meo-duplicate-table tr.empty-row" ).clone().css('display','block').removeClass('empty-row').addClass('lot-floor-row').insertAfter( "table.meo-duplicate-table tr.empty-row" ).find('table.meo-unvisible').removeClass('meo-unvisible');        
    $('table.meo-duplicate-table tr.lot-floor-row').each(function(){
        count++;
        $(this).find('.floor_entry').val(count);
    });
}

function clickCheckbox(element)
{
    $('table.meo-duplicate-table tr.lot-floor-row').each(function(){
        if( $(this).find('.floor_entry').val() != $(element).val())
        {
            $(this).find('.floor_entry').prop('checked',false);
        }
    });
}

function editLot(id)
{
    resetAllLot();    
    $('#meo-crm-waiting-bloc').css('display','block');
    $('#meo-crm-realestate-lot-list').css('display','none');
    $('#meo-crm-realestate-lot-form').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'getLotFormData',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            var obj = $.parseJSON(response);
            $('#plan_lot_id').html(obj.plan);
            $('#list_status_id').html(obj.status);            
            $('#floor_id_list').html(obj.floor);
            $('#dynamic-form').html(obj.meta);
            $.post(
                ajaxurl,
                {
                    'action': 'getFormImagePlanFloor',
                    'project_id': $('#realestate_project_id').val()
                },
                function(response){
                    
                    $('#plan_building_floor').html(response);
                    
                    $.post(
                        ajaxurl,
                        {
                            'action': 'getLotById',
                            'id': id,
                            'project_id': $('#realestate_project_id').val()
                        },
                        function(response){
                            
                            var obj = $.parseJSON(response);
                            var lot = obj.lot;
                            var lotMetasValue = obj.lot_meta;
                            var lot_floor = obj.lot_floor;                            
                            
                            // Lot Values 
                            $('#lot_id').val(lot.id);
                            $('#plan_lot_id').val(lot.plan_id).change();
                            $('#list_status_id').val(lot.status_id).change();
                            $('#type_lot').val(lot.type_lot).change();                            
                            $('#lot_title').val(lot.title);                              
                            $('#lot_description').val(lot.description);                            
                            $('#lot_description').html(lot.description);                            
                            $('#lot_room').val(lot.rooms);
                            $('#lot_surface').val(lot.surface);
                            $('#lot_price').val(lot.price);
                            
                            // Lot Meta Value
                            $.each(lotMetasValue, function(){
                                $('#meta_'+this.meta_id).val(this.value);
                            });                                                        
                            
                            for(var i=0; i < lot_floor.length; i++)
                            {
                                $('button.duplicate-row').trigger('click'); 
                            }
                            var i=0;
                            $('.meo-duplicate-table tr.lot-floor-row').each(function(){
                                $(this).find('#floor_id_list').val(lot_floor[i].floor_id).change();
                                $(this).find('#image_lot_floor_plan').attr('src', $('#url_upload').val()+'/'+lot_floor[i].plan_floor);
                                $(this).find('#old_image_lot_floor_plan').val(lot_floor[i].plan_floor);
                                $(this).find('#image_lot_floor_plan').css('display','block');                                
                                $(this).find('.floor_lot_id').val(lot_floor[i].id);                                
                                $(this).parent().find('.plan_floor_building_list').val(lot_floor[i].status_plan_floor_id).change;
                                
                                if($(this).parent().find('.plan_floor_building_list').val() != -1)
                                {
                                    $(this).parent().find('.plan_floor_building_list').parent('td').find('table').addClass('meo-unvisible');
                                }else{
                                    $(this).parent().find('.plan_floor_building_list').parent('td').find('table').removeClass('meo-unvisible');    
                                }
                                
                                if(lot_floor[i].lot_entry == 1)
                                {
                                    $(this).find('.floor_entry').prop('checked', true);
                                }
                                i++;
                                
                            });
                            
                            $('#meo-crm-waiting-bloc').css('display','none');
                        }
                    );
                    
                }
            );
        }
    ); 
}

function deleteLot(id)
{
    if(confirm('Voulez-vous vraiment supprimer cette element ?')){
        $('#meo-crm-waiting-bloc').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'deleteLot',
                'id': id,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                console.log(response);
                var obj = $.parseJSON(response);
                $('#zone-message').html(obj.message);
                loadTableLot();
                $('#meo-crm-waiting-bloc').css('display','none');            
            }
        );
    }
}

function loadTableLot()
{
    $.post(
        ajaxurl,
        {
            'action': 'getLotList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-lot').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllLot()
{
    $('#zone-message').html('');
    $('#table-container-lot').html(''); 
    $('#lot_id').val('');
    $('#lot_title').val('');
    $('#lot_description').val('');
    $('#lot_room').val('');
    $('#lot_surface').val('');
    $('#lot_price').val('');
    $('tr.lot-floor-row').remove();
}

function removeTableRow(element)
{
    $(element).parent('td').parent('tr').parent('tbody').parent('table').parent('td').parent('tr').remove();
}
/* END FUNCTION */