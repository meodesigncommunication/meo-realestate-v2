/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-sector').click(function(){
        resetAllSector();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-sector-list').css('display','block');
        loadTableSector();
    });
    
    $('#back-sector-dashboard').click(function(){
        resetAllSector();       
        $('#meo-crm-realestate-sector-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-sector').click(function(){
        resetAllSector();  
        $('#meo-crm-realestate-sector-list').css('display','none');
        $('#meo-crm-realestate-sector-form').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'getSelectDeveloppement',
                'id': 0,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                console.log(response);
                $('#developpement_select_list').html(response);
            }
        );
    });
    
    $('#back-sector').click(function(){
        resetAllSector(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-sector-form').css('display','none');
        $('#meo-crm-realestate-sector-list').css('display','block');
        loadTableSector();
    });
    
    
    // Execute the submit form
    $("#form_sector").on('submit',(function(e) {
        
        e.preventDefault();
        
        if($('#developpement_list').val() >= 1)
        {   
            
            var ajaxData = new FormData(this);        
            ajaxData.append('action', 'saveSector');
            ajaxData.append('project_id', $('#realestate_project_id').val());

            if($('#sector_id').val() != '')
            {
                ajaxData.append('sector_id', $('#sector_id').val());
            }

            // Get the image links if informs
            if($('#plan_sector_3d_old').val() != '')
            {
                ajaxData.append('plan_sector_3d_old', $('#plan_sector_3d_old').val());
            }        
            if($('#plan_sector_2d_old').val() != '')
            {
                ajaxData.append('plan_sector_2d_old', $('#plan_sector_2d_old').val());
            }       
            if($('#plan_sector_developpement_3d_old').val() != '')
            {
                ajaxData.append('plan_sector_developpement_3d_old', $('#plan_sector_developpement_3d_old').val());
            }        
            if($('#plan_sector_developpement_2d_old').val() != '')
            {
                ajaxData.append('plan_sector_developpement_3d_old', $('#plan_sector_developpement_3d_old').val());
            }

            $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                data: ajaxData,
                contentType: false,
                cache: false,
                processData:false,
                success: function(response)
                {
                    console.log(response);                    
                    var obj = $.parseJSON(response);
                    $('#zone-message').html(obj.message);
                    $('#sector_id').val(obj.id);
                    alert(obj.id);
                    
                    $('#balise_sector_image_3d').attr('src', $('#url_upload').val()+'/'+obj.images.sector_image_3d);
                    $('#plan_sector_3d_old').val(obj.sector_image_3d);

                    $('#balise_sector_image_2d').attr('src', $('#url_upload').val()+'/'+obj.images.sector_image_2d);
                    $('#plan_sector_2d_old').val(obj.sector_image_2d);
                    
                    $('#balise_sector_developpement_image_3d').attr('src', $('#url_upload').val()+'/'+obj.images.sector_developpement_image_3d);
                    $('#plan_sector_developpement_3d_old').val(obj.sector_developpement_image_3d);
                    
                    $('#balise_sector_developpement_image_2d').attr('src', $('#url_upload').val()+'/'+obj.images.sector_developpement_image_2d);
                    $('#plan_sector_developpement_2d_old').val(obj.sector_developpement_image_2d);
                }
            });
        }else{
            alert('Veuillez sélectionner un developpement !');
        }
    }));
    
});

/* START FUNCTION */

function editSector(id)
{
    resetAllSector();
    $('#meo-crm-realestate-sector-list').css('display','none');
    $('#meo-crm-realestate-sector-form').css('display','block');
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'getSectorById',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            var obj = $.parseJSON(response);
            $('#sector_id').val(obj.id);
            $('#sector_title').val(obj.title);
            $('#sector_description').val(obj.description);
            $('#coordinate_sector_developpement_3d').val(obj.coordinate_plan_developpement_3d);
            $('#coordinate_sector_developpement_2d').val(obj.coordinate_plan_developpement_2d);            
            $('#balise_sector_image_3d').attr('src', $('#url_upload').val()+'/'+obj.plan_sector_3d);
            $('#plan_sector_3d_old').val(obj.plan_sector_3d);            
            $('#balise_sector_image_2d').attr('src', $('#url_upload').val()+'/'+obj.plan_sector_2d);
            $('#plan_sector_2d_old').val(obj.plan_sector_2d);            
            $('#balise_sector_developpement_image_3d').attr('src', $('#url_upload').val()+'/'+obj.image_hover_plan_developpement_3d);
            $('#plan_sector_developpement_3d_old').val(obj.image_hover_plan_developpement_3d);            
            $('#balise_sector_developpement_image_2d').attr('src', $('#url_upload').val()+'/'+obj.image_hover_plan_developpement_2d);
            $('#plan_sector_developpement_2d_old').val(obj.image_hover_plan_developpement_2d);            
            $('#meo-crm-waiting-bloc').css('display','none');
            
            $.post(
                ajaxurl,
                {
                    'action': 'getSelectDeveloppement',
                    'id': obj.developpement_id,
                    'project_id': $('#realestate_project_id').val()
                },
                function(response){
                    console.log(response);
                    $('#developpement_select_list').html(response);
                }
            );
        }
    );
}

function deleteSector(id)
{
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'deleteSector',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            var obj = $.parseJSON(response);
            loadTableSector();            
            $('#zone-message').html(obj.message);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function loadTableSector()
{
    $.post(
        ajaxurl,
        {
            'action': 'getSectorList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-sector').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllSector()
{
    $('#zone-message').html('');
    $('#table-container-sector').html('');    
    $('#sector_id').val('');
    $('#sector_title').val('');
    $('#sector_description').val('');
    $('#coordinate_sector_developpement_3d').val('');
    $('#coordinate_sector_developpement_2d').val('');
    $('#sector_image_3d').val('');
    $('#balise_sector_image_3d').attr('src', '');
    $('#plan_sector_3d_old').val('');
    $('#sector_image_2d').val('');
    $('#balise_sector_image_2d').attr('src', '');
    $('#plan_sector_2d_old').val('');
    $('#sector_developpement_image_3d').val('');
    $('#balise_sector_developpement_image_3d').attr('src', '');
    $('#plan_sector_developpement_3d_old').val('');
    $('#sector_developpement_image_2d').val('');
    $('#balise_sector_developpement_image_2d').attr('src', '');
    $('#plan_sector_developpement_2d_old').val('');
}
/* END FUNCTION */