<?php

class BuildingPlanFloorModel
{
    
    public static function getAllBuildingFloorPlan($external_wpdb)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE.' ';       
        $results = $external_wpdb->get_results( $query );
        return $results;
    }
    
    public static function getBuildinFloorPlanById($external_wpdb, $id_coordinate)
    {
        $query = '  SELECT c.id AS coordinates_id, c.title, c.coordinates_front, c.coordinates_back, pc.id AS image_id, pc.status_id, pc.image, pc.front_face
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE . ' AS c
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_LINK_COORDINATES_PLAN_FLOOR_LOT_BUILDING_TABLE . ' AS lc ON lc.coordinates_id = c.id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE . ' AS pc ON lc.plan_lot_id = pc.id
                    WHERE c.id = ' . $id_coordinate ;
        $results = $external_wpdb->get_results( $query );
        return $results;
    }
    
    public static function insertCoordinateFloorPlan($external_wpdb, $datas)
    {
        $check = true;
        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE, $datas) === false)
        {
            $check = false;
        }
        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }    
    public static function updateCoordinateFloorPlan($external_wpdb, $datas, $where)
    {
        $check = true;
        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE,$datas,$where) === false)
        {
            $check = false;
        }
        
        return [
            'success' => $check
        ];
    }    
    public static function deleteCoordinateFloorPlan($external_wpdb, $where = array())
    {
        $check = true;
        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE, $where) === false)
        {
            $check = false;
        }
        
        return [
            'success' => $check
        ];
    }
    
    public static function insertImageFloorPlan($external_wpdb, $datas)
    {
        $check = true;
        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE, $datas) === false)
        {
            $check = false;
        }
        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateImageFloorPlan($external_wpdb, $datas, $where)
    {
        $check = true;
        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE, $datas, $where) === false)
        {
            $check = false;
        }
        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function insertLinkFloorPlan($external_wpdb, $datas)
    {
        $check = true;
        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LINK_COORDINATES_PLAN_FLOOR_LOT_BUILDING_TABLE, $datas) === false)
        {
            $check = false;
        }
        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
}
