<?php

class BuildingModel
{
    public static function getAllBuilding($external_wpdb)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_BUILDING_TABLE . ' ';
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getBuildingById($external_wpdb,$id)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_BUILDING_TABLE . ' ';
        $query .= 'WHERE id='.$id;
        $results = $external_wpdb->get_results( $query );        
        return (isset($results[0]))? $results[0] : null;
    }
    
    public static function insertBuilding($external_wpdb,$datas)
    {
        global $wpdb;
        $check = true;        
        if($external_wpdb->insert($wpdb->prefix.MEO_CRM_REALESTATE_BUILDING_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateBuilding($external_wpdb,$datas,$where)
    {
        global $wpdb;
        $check = true;        
        if($external_wpdb->update($wpdb->prefix.MEO_CRM_REALESTATE_BUILDING_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
    
    public static function deleteBuilding($external_wpdb,$where)
    {
        global $wpdb;
        $check = true;        
        if($external_wpdb->delete($wpdb->prefix.MEO_CRM_REALESTATE_BUILDING_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
}
