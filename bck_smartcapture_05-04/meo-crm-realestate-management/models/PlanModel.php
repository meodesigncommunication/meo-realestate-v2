<?php

class PlanModel
{

    public static function getAllPlan($external_wpdb)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE.' ';
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getPlanById($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE.' ';
        $query .= 'WHERE id='.$id;
        $results = $external_wpdb->get_results( $query );        
        return $results[0];
    }
    
    public static function insertPlan($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updatePlan($external_wpdb,$datas,$where)
    {
        $check = true;        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
    
    public static function deletePlan($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
}
