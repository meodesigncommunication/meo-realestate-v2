<?php
class PriceListModel
{
    public static function getPriceListParamByProjectId($project_id)
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OPTIONS_TABLE . ' 
                    WHERE project_id = '.$project_id;

        return $query;
    }
    public static function getPriceListOtherPriceByTemplateId()
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES_TABLE ;
        return $query;
    }
    public static function getPriceListHeaderByTemplateId()
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_HEADER_VALUE_TABLE ;

        return $query;
    }
}