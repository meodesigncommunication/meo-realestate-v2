<?php

class MetaLotModel
{
   
    public static function getAllMetaLot($external_wpdb)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE.' ';
        $query .= 'ORDER BY meta_order ASC ';
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getMetaLotById($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE.' ';
        $query .= 'WHERE id='.$id;
        $results = $external_wpdb->get_results( $query );        
        return $results[0];
    }
    
    public static function getValueMetaLotByLotId($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE.' ';
        $query .= 'WHERE lot_id='.$id;
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function insertMetaLot($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateMetaLot($external_wpdb,$datas,$where)
    {
        $check = true;        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
    
    public static function deleteMetaLot($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
    
    /*
     *  Part For meta_value Table
     */
    public static function getMetaValueLotById($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE.' ';
        $query .= 'WHERE lot_id='.$id;
        $results = $external_wpdb->get_results( $query );        
        return $results[0];
    }

    public static function getMetaValueLotByLotIdAndMetaId($external_wpdb,$lot_id,$meta_id)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE.' ';
        $query .= 'WHERE lot_id='.$lot_id.' AND meta_id='.$meta_id;
        $results = $external_wpdb->get_results( $query );
        return $results;
    }
    
    public static function insertMetaValueLot($external_wpdb,$data)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE,$data) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateMetaValueLot($external_wpdb,$datas,$where)
    {
        $check = true;        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => (isset($where['id'])) ? $where['id'] : array()
        ];
    }
    
    public static function deleteMetaValueLot($external_wpdb,$where)
    {
        $check = true;    
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => (isset($where['id']) && !empty($where['id'])) ? $where['id'] : 0
        ];
    }
}
