<?php

class FloorLotModel
{    
    public static function getAllFloorLot($external_wpdb)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE . ' ';
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getFloorLotByLotId($external_wpdb,$id)
    {                
        $query  = 'SELECT fl.id AS floor_lot_id, fl.lot_entry, fl.lot_id, fl.floor_id, fl.plan_floor, f.title AS floor_title , cplb.title AS coordinate_title 
                   FROM ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE . ' AS fl  
                   LEFT JOIN ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE . ' AS f ON fl.floor_id = f.id
                   LEFT JOIN ' . getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE . ' AS cplb ON fl.status_plan_floor_id = cplb.id
                   WHERE lot_id='.$id;
        $results = $external_wpdb->get_results( $query );   
        return $results;
    }
    
    public static function getFloorLotByLotIdAndFloorId($external_wpdb,$lot_id,$floor_id)
    {                
        $query  = 'SELECT * 
                   FROM ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE . ' AS fl  
                   WHERE lot_id='.$lot_id.'
                   AND floor_id = '.$floor_id;
        $results = $external_wpdb->get_results( $query );   
        return $results;
    }
    
    public static function insertFloorLot($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateFloorLot($external_wpdb,$datas,$where)
    {
        $check = true;        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check
        ];
    }
    
    public static function deleteFloorLot($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check
        ];
    }
}
