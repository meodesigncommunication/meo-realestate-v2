<?php
//echo 'test';exit();
global $current_user;
$check_data = true;
$errors = array();
$input_class_name = '';
$input_class_desc = '';
$status_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];
$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);
$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);
$spinners = MeoCrmRealestateManagement::getAllSpinner($external_wpdb);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(!empty($status_id))
    {
        $data = array();
        $data['name'] = $_POST['status'];
        $data['color'] = $_POST['color'];
        
        if(empty($data['name'])){ $check_data = false; $input_class_name = 'warningField'; }
        if(empty($data['color'])){ $check_data = false; $input_class_desc = 'warningField'; }

        if(!empty($data))
        {
            $where = array('id' => $status_id);
            $data['hide_price'] = (isset($_POST['hide_price']) && !empty($_POST['hide_price'])) ? $_POST['hide_price'] : 0;
            $data['hide_price'] = (isset($_POST['sold']) && !empty($_POST['sold'])) ? $_POST['sold'] : 0;
            $resultUpdate = MeoCrmRealestateManagement::updateStatus($external_wpdb,$data,$where);
            if($resultUpdate['success'])
            {
                header('Location:/meo-crm-realestate-form-status/?project='.$project_id.'&id='.$status_id);
            }
        }
        
    }else{
       
        // Enregistrer les datas dans la DB
        $data = array();
        $data['name'] = $_POST['status'];
        $data['color'] = $_POST['color'];
        
        if(empty($data['name'])){ $check_data = false; $input_class_name = 'warningField'; }
        if(empty($data['color'])){ $check_data = false; $input_class_desc = 'warningField'; }
        
        if($check_data)
        {
            $data['hide_price'] = (isset($_POST['hide_price']) && !empty($_POST['hide_price'])) ? $_POST['hide_price'] : 0;
            $data['sold'] = (isset($_POST['sold']) && !empty($_POST['sold'])) ? $_POST['sold'] : 0;
            $resultInsert = MeoCrmRealestateManagement::insertStatus($external_wpdb, $data);
        }else{
            $resultInsert = array(
                'success' => 0
            );
        }
        
        if($resultInsert['success'])
        {
            $insert_id = $resultInsert['id'];
            
            if(isset($insert_id) && !empty($insert_id))
            {
                header('Location:/meo-crm-realestate-form-status/?project='.$project_id.'&id='.$insert_id);
            }
            
        }else{
            $errors[] = 'Le titre ou la description ou les coordonn&eacute;e du secteur ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
            echo 'error';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($status_id))
{
    $status = MeoCrmRealestateManagement::getStatusById($external_wpdb, $status_id);  
    echo $external_wpdb->last_query;
    
    $old_name = (!empty($status[0]->name)) ? $status[0]->name : '';
    $old_color = (!empty($status[0]->color)) ? $status[0]->color : '';
    $old_hide_price = (!empty($status[0]->hide_price)) ? $status[0]->hide_price : '';
    $old_sold = (!empty($status[0]->sold)) ? $status[0]->sold : '';
}else{
    $old_name = '';
    $old_color = '';
    $old_hide_price = 0;
    $old_sold = 0;
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
?>
    <form method="post" name="add_status" id="form_status" action="<?php //echo site_url('/meo-crm-realestate-form-status/').'?project='.$project_id.'&id='.$status_id ?>">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>Status</h2>  
        
        <div class="input-group">
            <label>Status</label>
            <input type="text" name="status" id="status" value="<?php echo $old_name ?>" placeholder="Status" class="<?php echo $input_class_name ?>" />
        </div>
        
        <div class="input-group">
            <label>Couleur</label>
            <input type="hidden" name="color" id="color" value="<?php echo $old_color; ?>" />
            <div id="colorSelector" class="colorSelector">
                <div style="background-color: <?php echo $old_color; ?>;"></div>
            </div>
        </div>

        <div class="group-checkbox">
            <input type="checkbox" name="hide_price" id="hide_price" value="1" class="" <?php echo ($old_hide_price) ? 'checked' : '' ?> />
            <label>Cacher les prix</label>
        </div>

        <div class="group-checkbox">
            <input type="checkbox" name="sold" id="sold" value="1" class="" <?php echo ($old_sold) ? 'checked' : '' ?> />
            <label>Status de type vendu</label>
        </div>
        
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-status-list/?project='.$project_id ?>" name=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
<?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
        jQuery("#colorSelector").ColorPicker({
            color: "#<?php echo $old_color; ?>",
            onShow: function (colpkr) { jQuery(colpkr).fadeIn(500); return false; },
            onHide: function (colpkr) { jQuery(colpkr).fadeOut(500); return false; },
            onChange: function (hsb, hex, rgb) {
                jQuery("#colorSelector div").css("backgroundColor", "#" + hex);
                jQuery("#color").val("#" + hex);
            }
        });
    });
</script>
<?php get_footer(); ?>