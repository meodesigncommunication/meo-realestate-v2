<?php
global $current_user;
$check_data = true;
$errors = array();
$input_class_title = '';
$input_class_desc = '';
$plan_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
$project = meo_crm_projects_getProjectInfo($project_id);
$baseDirFTP = $project['realestate']['ftp']['base_dir'];
$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);
$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);
$upload_path = wp_upload_dir();
$pdf_destination = $upload_path['basedir'].'/'.PRICE_LIST_UPLOAD_FOLDER.'/'.$project_id.'/';
$pdf_price_list_path = MEO_CRM_PROJECTS_FOLDER_DIR.$project_id.'/pdfs/price_list.pdf';
$pdf_price_list_url = MEO_CRM_PROJECTS_FOLDER_URL.'/'.$project_id.'/pdfs/price_list.pdf';


do_action('meo_crm_realestate_create_price_list',$project_id);

// Get Meta data from project website
$realestateMetas = $external_wpdb->get_results(MeoCrmRealestateManagement::getRealestateMetas($external_wpdb));

$query_post_price_list = MeoCrmRealestateManagement::getPostAttachmentPriceList();
$post_price_list = MeoCrmCoreExternalDbQuery::select($external_wpdb, $query_post_price_list);
$file_pdf_id = (!empty($post_price_list)) ? $post_price_list[0]->ID : 0;


$header = array(
    'building_name' => 'Batiment',
    'lot_name' => 'Appartement',
    'floor_title' => 'Etage',
    'rooms' => 'Nbr. pieces',
    'surface' => 'Surface',
    'price' => 'Prix de vente'
);

// -- Get Price list --
$price_list_param = $external_wpdb->get_results(MeoCrmRealestateManagement::getPriceListParamByProjectId($project_id));

// -- Get data in DB --
if(!empty($price_list_param))
{
    $template_pdf_id = $price_list_param[0]->id;
    $template_pdf_landscape = $price_list_param[0]->pdf_template_landscape;
    $template_pdf_portrait = $price_list_param[0]->pdf_template_portrait;
    $color_theme = $price_list_param[0]->color_theme;
    $color_info = $price_list_param[0]->color_info;

    // Get all other pric of the price list
    $price_list_header = $external_wpdb->get_results(MeoCrmRealestateManagement::getPriceListHeaderByTemplateId());
    $price_list_objects = $external_wpdb->get_results(MeoCrmRealestateManagement::getPriceListOtherPriceByTemplateId());

}else{
    $template_pdf_id = 0;
    $template_pdf_landscape = '';
    $template_pdf_portrait = '';
    $color_theme = 'ffffff';
    $color_info = 'ffffff';
}

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $table = getExternalPrefix().MEO_CRM_REALESTATE_PRICE_LIST_OPTIONS_TABLE;

    // --- INSERT ---
    $count = 0;
    $checkInsert = true;
    $checkTruncate = true;
    $labelHeader_1 = $_POST['label_1'];
    $labelHeader_2 = $_POST['label_2'];
    $positionHeader = $_POST['position'];
    $tablePriceListHeader = getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_HEADER_VALUE_TABLE;

    if(isset($price_list_header) && !empty($price_list_header))
    {
        foreach($price_list_header as $value)
        {
            $where = array('id' => $value->id);
            if(!$external_wpdb->delete($tablePriceListHeader,$where))
            {
                $checkTruncate = false;
            }
        }
    }

    if($checkTruncate)
    {
        if(isset($_POST['header']) && !empty($_POST['header']))
        {
            foreach ($_POST['header'] as $key_posted_header => $posted_header) {
                $dataHeader = array();
                $dataHeader['header_key'] = $key_posted_header;
                $dataHeader['label_1'] = $labelHeader_1[$key_posted_header];
                $dataHeader['label_2'] = $labelHeader_2[$key_posted_header];
                $dataHeader['position'] = (!empty($positionHeader[$key_posted_header])) ? $positionHeader[$key_posted_header] : 0;
                if (!$external_wpdb->insert($tablePriceListHeader, $dataHeader)) {
                    $checkInsert = false;
                }
            }
            if($checkInsert)
            {
                $price_list_header = $external_wpdb->get_results(MeoCrmRealestateManagement::getPriceListHeaderByTemplateId());
            }
        }
    }

    // --------------------------
    if(!empty($template_pdf_id)) //update data
    {
        if(MeoCrmCoreFileManager::createFolder('', $pdf_destination, '0777', true))
        {
            $create_folder = true;
        }

        if($create_folder)
        {
            $where = array();
            $dataUpldate = array();

            if(!empty($_FILES['template_landscape_pdf']['name'])) {
                $nextStep = true;

                if (!empty($template_pdf_landscape)) {
                    if (!unlink($pdf_destination . $template_pdf_landscape)) {
                        $errors[] = 'Ancien PDF pas supprimé !!';
                        $nextStep = false;
                    }
                }

                if ($nextStep) {
                    $tmp_file = $_FILES['template_landscape_pdf']['tmp_name'];
                    $type_file = $_FILES['template_landscape_pdf']['type'];
                    $name_file = $_FILES['template_landscape_pdf']['name'];
                    if ($type_file == 'application/pdf') {
                        $resultUploadPdf = MeoCrmCoreFileManager::uploadMedia($_FILES['template_landscape_pdf'], $pdf_destination);
                        if ($resultUploadPdf['success']) {
                            $dataUpldate['pdf_template_landscape'] = $name_file;

                        } else $errors[] = 'Erreur durant l\'upload du template PDF !!';
                    }
                }
            }

            if(!empty($_FILES['template_portrait_pdf']['name']))
            {
                $nextStep = true;

                if (!empty($template_pdf_portrait)) {
                    if (!unlink($pdf_destination . $template_pdf_portrait)) {
                        $errors[] = 'Ancien PDF pas supprimé !!';
                        $nextStep = false;
                    }
                }

                if ($nextStep) {
                    $tmp_file = $_FILES['template_portrait_pdf']['tmp_name'];
                    $type_file = $_FILES['template_portrait_pdf']['type'];
                    $name_file = $_FILES['template_portrait_pdf']['name'];
                    if ($type_file == 'application/pdf') {
                        $resultUploadPdf = MeoCrmCoreFileManager::uploadMedia($_FILES['template_portrait_pdf'], $pdf_destination);
                        if ($resultUploadPdf['success'])
                        {
                            $dataUpldate['pdf_template_portrait'] = $name_file;

                        } else $errors[] = 'Erreur durant l\'upload du template PDF !!';
                    }
                }
            }

            $dataUpldate['color_theme'] = $_POST['color_theme'];
            $dataUpldate['color_info'] = $_POST['color_info'];

            $where['id'] = $template_pdf_id;

            if(!$external_wpdb->update($table, $dataUpldate, $where))
            {
                $external_wpdb->last_query;
                $errors[] = 'Erreur insert data (liste de prix) !!';
            }else{
                do_action('meo_crm_realestate_create_price_list',$project_id);
                header("Refresh:0");
            }

        }
    }else{ // Insert data

        $dataInsert = array();
        $create_folder = false;

        if(!empty($_FILES['template_landscape_pdf']['name']))
        {

            if(MeoCrmCoreFileManager::createFolder('', $pdf_destination, 0755, true))
            {
                $create_folder = true;
            }

            if($create_folder)
            {
                $type_file = $_FILES['template_landscape_pdf']['type'];
                $name_file = $_FILES['template_landscape_pdf']['name'];

                if($type_file == 'application/pdf')
                {
                    $resultUploadPdf = MeoCrmCoreFileManager::uploadMedia($_FILES['template_landscape_pdf'],$pdf_destination);
                    if($resultUploadPdf['success'])
                    {
                        $dataInsert['pdf_template_landscape'] = $name_file;

                    }else{

                        $errors[] = 'Erreur durant l\'upload du template PDF !!';
                    }
                }
            }
        }

        if(!empty($_FILES['template_portrait_pdf']['name']))
        {

            if(MeoCrmCoreFileManager::createFolder('', $pdf_destination, 0755, true))
            {
                $create_folder = true;
            }

            if($create_folder)
            {
                $type_file = $_FILES['template_portrait_pdf']['type'];
                $name_file = $_FILES['template_portrait_pdf']['name'];

                if($type_file == 'application/pdf')
                {
                    $resultUploadPdf = MeoCrmCoreFileManager::uploadMedia($_FILES['template_portrait_pdf'],$pdf_destination);
                    if($resultUploadPdf['success'])
                    {
                        $dataInsert['pdf_template_portrait'] = $name_file;

                    }else{

                        $errors[] = 'Erreur durant l\'upload du template PDF !!';
                    }
                }
            }
        }

        // Form Data
        $dataInsert['project_id'] = $_POST['project_id'];
        $dataInsert['color_theme'] = $_POST['color_theme'];
        $dataInsert['color_info'] = $_POST['color_info'];

        if(!$external_wpdb->insert($table, $dataInsert))
        {
            $errors[] = 'Erreur insert data (liste de prix) !!';
        }else{
            do_action('meo_crm_realestate_create_price_list',$project_id);
            header("Refresh:0");
        }
    }
}
get_header();
if($project){

    $url_length = strlen($project['url']);
    $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

?>
    <form method="post" name="price_list_pdf" id="form_price_list_pdf" enctype="multipart/form-data">
        <input type="hidden" name="param_id" value="<?php echo $template_pdf_id ?>" />
        <input type="hidden" name="ajax_url" value="<?php echo get_site_url().'/wp-admin/admin-ajax.php' ?>" />
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />

        <h2>Liste de prix PDF</h2>

        <?php if(!empty($file_pdf_id)): ?>
            <p>
                ID du fichier: <?php echo $file_pdf_id ?> | Encod&eacute; : <?php echo MeoCrmCoreCryptData::encodeAttachmentId($file_pdf_id); ?>
            </p>
            <a href="<?php echo $pdf_price_list_url ?>" target="_blank" title="Voir la liste de prix">Voir la liste de prix</a>
        <?php endif; ?>

        <div class="input-group">
            <label>Template PDF Portrait</label>
            <input type="file" name="template_portrait_pdf" id="template_portrait_pdf" class="" />
            <?php if(!empty($template_pdf_portrait)): ?>
                <a target="_blank" class="" href="<?php echo $upload_path['baseurl'].'/'.PRICE_LIST_UPLOAD_FOLDER.'/'.$project_id.'/'.$template_pdf_portrait ?>" title="Template de liste de prix">
                    Voir le template PDF Portrait
                </a>
            <?php endif; ?>
        </div>

        <div class="input-group">
            <label>Template PDF Landscape</label>
            <input type="file" name="template_landscape_pdf" id="template_landscape_pdf" class="" />
            <?php if(!empty($template_pdf_landscape)): ?>
                <a target="_blank" class="" href="<?php echo $upload_path['baseurl'].'/'.PRICE_LIST_UPLOAD_FOLDER.'/'.$project_id.'/'.$template_pdf_landscape ?>" title="Template de liste de prix">
                    Voir le template PDF Landscape
                </a>
            <?php endif; ?>
        </div>
        
        <div class="input-group">
            <label>Couleur du th&egrave;me</label>
            <input type="hidden" name="color_theme" id="color_theme" value="<?php echo $color_theme; ?>" />
            <div id="colorSelector" class="colorSelector">
                <div style="background-color: <?php echo $color_theme; ?>;"></div>
            </div>
        </div>

        <div class="input-group">
            <label>Couleur info PDF</label>
            <input type="hidden" name="color_info" id="color_info" value="<?php echo $color_info; ?>" />
            <div id="colorSelector2" class="colorSelector">
                <div style="background-color: <?php echo $color_info; ?>;"></div>
            </div>
        </div>
        <div class="input-group">
            <label>En-t&ecirc;te liste de prix</label>
            <?php

            foreach($header as $key => $head){

                $position = '';
                $header_key = '';
                $label = '';

                if(isset($price_list_header) && !empty($price_list_header))
                {
                    foreach ($price_list_header as $price_list_header_single) {
                        if ($price_list_header_single->header_key == $key) {
                            $position = $price_list_header_single->position;
                            $header_key = $price_list_header_single->header_key;
                            $label_1 = $price_list_header_single->label_1;
                            $label_2 = $price_list_header_single->label_2;
                        }
                    }
                }else{
                    $label_1 = '';
                    $label_2 = '';
                }
                if($key == 'price' && isset($realestateMetas) && !empty($realestateMetas))
                {
                    foreach($realestateMetas as $meta_key => $meta)
                    {
                        $position_meta = '';
                        $header_key_meta = '';
                        $label_meta_1 = '';
                        $label_meta_2 = '';

                        if( isset($price_list_header) && !empty($price_list_header) )
                        {
                            foreach($price_list_header as $price_list_header_single_meta)
                            {
                                if($price_list_header_single_meta->header_key == $meta->meta_slug)
                                {
                                    $position_meta = $price_list_header_single_meta->position;
                                    $header_key_meta = $price_list_header_single_meta->header_key;
                                    $label_meta_1 = $price_list_header_single_meta->label_1;
                                    $label_meta_2 = $price_list_header_single_meta->label_2;
                                }
                            }
                        }

                        $checked_meta = (!empty($header_key_meta)) ? 'checked' : '';

                        echo '<div class="header_pdf_price_list_checkbox">';
                        echo    '<input type="input" name="position['. $meta->meta_slug . ']" value="' . $position_meta . '" />';
                        echo    '<input type="checkbox" name="header['. $meta->meta_slug . ']" value="' . $meta->meta_key . '" '.$checked_meta.' />';
                        echo    '<label>' . $meta->meta_key . '</label>';
                        echo    '<input type="text" class="label_header" name="label_1['. $meta->meta_slug . ']" value="' . $label_meta_1 . '" '.$checked_meta.' />';
                        echo    '<input type="text" class="label_header" name="label_2['. $meta->meta_slug . ']" value="' . $label_meta_2 . '" '.$checked_meta.' />';
                        echo '</div>';
                    }
                }

                $checked = (!empty($header_key)) ? 'checked' : '';

                echo '<div class="header_pdf_price_list_checkbox">';
                echo    '<input type="input" name="position['. $key . ']" value="' . $position. '" />';
                echo    '<input type="checkbox" name="header['. $key . ']" value="' . $head . '" ' . $checked . ' />';
                echo    '<label>' . $head . '</label>';
                echo    '<input type="text" class="label_header" name="label_1['. $key . ']" value="' . $label_1 . '" />';
                echo    '<input type="text" class="label_header" name="label_2['. $key . ']" value="' . $label_2 . '" />';
                echo '</div>';

            }

            ?>


        </div>
        <hr/>
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-management-tool/?project='.$project_id ?>" title=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
    <?php if(!empty($template_pdf_id)): ?>
        <hr/>
        <table id="price_list_object">
            <thead>
                <tr>
                    <th>Intitul&eacute;</th>
                    <th>Prix</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" id="label_price" name="label_price" value="" /></td>
                    <td><input type="text" id="price" name="price" value="" /></td>
                    <td><a id="save_object_price" href="#" title="save price"><i class="fa fa-floppy-o" aria-hidden="true"></i></a></td>
                </tr>
                <?php foreach($price_list_objects as $price_list_object): ?>
                    <tr data-id="<?php echo $price_list_object->id ?>">
                        <td><?php echo $price_list_object->label ?></td>
                        <td><?php echo $price_list_object->price ?> CHF</td>
                        <td>
                            <a href="#" title="delete row" class="delete_row_object"><i class="fa fa-close" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php
    endif;
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){

        jQuery('#save_object_price').click(function(e){
            e.preventDefault();
            var param_id = jQuery('input[name="param_id"]').val();
            var project_id = jQuery('input[name="project_id"]').val();
            var label = jQuery('#label_price').val();
            var price = jQuery('#price').val();

            jQuery.ajax({
                url  : jQuery('input[name="ajax_url"]').val(),
                type : 'POST',
                data : {
                    'action': 'meoCrmRealeastateAddPriceListObject',
                    'param_id': param_id,
                    'project_id': project_id,
                    'label': label,
                    'price': price
                },
                success : function(response){
                    if(response)
                    {
                        jQuery('#label_price').val('')
                        jQuery('#price').val('');
                        location.reload();
                    }else{
                        alert('Une erreur c\'est produite !');
                    }

                }
            });
        });

        jQuery('.delete_row_object').click(function(e){
            e.preventDefault();
            var delete_id = jQuery(this).parents('tr').attr('data-id');
            var project_id = jQuery('input[name="project_id"]').val();

            jQuery.ajax({
                url  : jQuery('input[name="ajax_url"]').val(),
                type : 'POST',
                data : {
                    'action': 'meoCrmRealeastateRemovePriceListObject',
                    'id': delete_id,
                    'project_id': project_id,
                },
                success : function(response){
                    if(response)
                    {
                        location.reload();
                    }else{
                        alert('Une erreur c\'est produite !');
                    }

                }
            });
        });

        jQuery("#colorSelector").ColorPicker({
            color: "#<?php echo $color_theme; ?>",
            onShow: function (colpkr) { jQuery(colpkr).fadeIn(500); return false; },
            onHide: function (colpkr) { jQuery(colpkr).fadeOut(500); return false; },
            onChange: function (hsb, hex, rgb) {
                jQuery("#colorSelector div").css("backgroundColor", "#" + hex);
                jQuery("#color_theme").val("#" + hex);
            }
        });

        jQuery("#colorSelector2").ColorPicker({
            color: "#<?php echo $color_info; ?>",
            onShow: function (colpkr) { jQuery(colpkr).fadeIn(500); return false; },
            onHide: function (colpkr) { jQuery(colpkr).fadeOut(500); return false; },
            onChange: function (hsb, hex, rgb) {
                jQuery("#colorSelector2 div").css("backgroundColor", "#" + hex);
                jQuery("#color_info").val("#" + hex);
            }
        });

    });
</script>
<?php get_footer(); ?>