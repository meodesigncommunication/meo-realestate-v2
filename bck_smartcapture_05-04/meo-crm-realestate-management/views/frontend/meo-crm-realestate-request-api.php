<?php
/*$data = array();
$project_id = (isset($_GET['project_id']) && !empty($_GET['project_id'])) ? $_GET['project_id'] : 37;

$projects = meo_crm_projects_getProjectInfo($project_id);

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($projects['realestate']['db']['login'],$projects['realestate']['db']['password'],$projects['realestate']['db']['name'],$projects['realestate']['db']['host']);

$developpements = ApiRequestModel::selectBuildingsWithLot($external_wpdb);

echo '<pre>';
print_r($developpements);
echo '</pre>';

//echo json_encode($developpements);*/

if(isset($_GET['log']) && isset($_GET['pwd'])){

    $creds = array();
    $jsonData = '';
    $index = 1;
    $array_project_list = array();
    $array_app_enable = array();
    $creds['user_login'] = $_GET['log'];
    $creds['user_password'] = $_GET['pwd'];
    $creds['remember'] = false;

    $user = wp_signon( $creds, false );

    if ( is_wp_error($user) ){
        $array_app_enable[0]['login'] = 0;
        echo json_encode($array_app_enable);  //Display error message if log-in fails
    }else{

        $array_app_enable['user_data'] = $user;
        $array_app_enable['login'] = 1;

        $userID = $user->ID;
        $projects = MeoCrmProjects::getProjectsByUserID($userID);

        foreach ($projects as $key => $project) {

            if ($project) {

                $dbAccess = array();
                $ftpAccess = array();

                $access = AccessProjectModel::selectAccessProjectByProjectId($project['id']);
                $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

                $dbAccess['host'] = (isset($access->host_db) && !empty($access->host_db)) ? $meoCrmCoreCryptData->decode($access->host_db) : '';
                $dbAccess['name'] = (isset($access->host_db) && !empty($access->name_db)) ? $meoCrmCoreCryptData->decode($access->name_db) : '';
                $dbAccess['login'] = (isset($access->host_db) && !empty($access->login_db)) ? $meoCrmCoreCryptData->decode($access->login_db) : '';
                $dbAccess['password'] = (isset($access->host_db) && !empty($access->password_db)) ? $meoCrmCoreCryptData->decode($access->password_db) : '';

                $ftpAccess['host'] = (isset($access->host_ftp) && !empty($access->host_ftp)) ? $meoCrmCoreCryptData->decode($access->host_ftp) : '';
                $ftpAccess['base_dir'] = (isset($access->base_dir_ftp) && !empty($access->base_dir_ftp)) ? $meoCrmCoreCryptData->decode($access->base_dir_ftp) : '';
                $ftpAccess['login'] = (isset($access->login_ftp) && !empty($access->login_ftp)) ? $meoCrmCoreCryptData->decode($access->login_ftp) : '';
                $ftpAccess['password'] = (isset($access->password_ftp) && !empty($access->password_ftp)) ? $meoCrmCoreCryptData->decode($access->password_ftp) : '';

                $project['realestate']['db'] = $dbAccess;
                $project['realestate']['ftp'] = $ftpAccess;

                $array_project_list[] = $project;

            }
        }

        foreach ($array_project_list as $key => $project) {
            if ($project['app_enabled']) {
                if (isset($project['app_realestate']) && $project['app_realestate']) {
                    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'], $project['realestate']['db']['password'], $project['realestate']['db']['name'], $project['realestate']['db']['host']);
                    $developpements = ApiRequestModel::selectBuildingsWithLot($external_wpdb);

                    $array_app_enable['projects'][$index]['project'] = $project;
                    $array_app_enable['projects'][$index]['developpements'] = $developpements;

                } else {
                    $array_app_enable['projects'][$index]['project'] = $project;
                }

                $index++;
            }
        }

        echo json_encode($array_app_enable);
    }
}