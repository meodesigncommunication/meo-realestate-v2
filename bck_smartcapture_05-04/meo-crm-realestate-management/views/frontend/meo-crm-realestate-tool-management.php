<?php
global $current_user;
$project_id = (isset($_GET["project"]) && !empty($_GET["project"])) ? $_GET["project"] : 0;

if(!empty($project_id))
{
    do_action('meo_crm_realestate_create_price_list',$project_id);
}

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}

$projects = meo_crm_projects_getProjects();

get_header();

if($projects){

?>
<div class="redband">
    <div id="projects_select_list">
        <select id="projects" name="projects">
            <option value="0">Choisir un projet</option> 
            <?php foreach($projects as $project):
                if($project['app_realestate']):
                    $selected = ($project_id == $project['id']) ? 'selected="true"' : ''; ?>
                    <option value="<?php echo $project['id'] ?>" <?php echo $selected; ?>><?php echo $project['name'] ?></option>
                <?php endif;
            endforeach; ?>
        </select> 
    </div>
    <div id="export_realestate_datas">
        <a href="#" title="Exporter les datas"><i class="fa fa-download" aria-hidden="true"></i></a>
    </div>
</div>
<div id="list-form-realestate">
    <div class="icon-item">
        <span class="icon-form" data-form="developpement">
            <i class="fa fa-suitcase" aria-hidden="true"></i>
            <p>Developpements</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="sector">
            <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
            <p>Secteurs</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="building">
            <i class="fa fa-building" aria-hidden="true"></i>
            <p>Immeubles</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="lot">
            <i class="fa fa-home" aria-hidden="true"></i>
            <p>Lots</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="plan">
            <i class="fa fa-map" aria-hidden="true"></i>
            <p>Plans</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="status">
            <i class="fa fa-tags" aria-hidden="true"></i>
            <p>Status de lot</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="meta">
            <i class="fa fa-cog" aria-hidden="true"></i>
            <p>Metadonn&eacute;es de lot</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="plan-floor-lot">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <p>Typo lot &eacute;tage par b&acirc;timent</p>
        </span>
    </div>
    <div class="icon-item">
        <span class="icon-form" data-form="price-list-pdf">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
            <p>Liste de prix</p>
        </span>
    </div>
    <?php do_action('list_item_realestate_tool'); ?>
</div>

<?php 
}else MeoCrmCoreTools::meo_crm_core_403(); ?>

<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
       $('#projects') .change(function(){
           
           //TODO : requête AJAX recuperation des datas du projet
           
       });
    });
</script>

<?php get_footer(); ?>