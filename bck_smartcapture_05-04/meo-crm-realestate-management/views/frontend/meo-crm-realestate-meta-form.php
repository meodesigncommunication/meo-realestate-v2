<?php
//echo 'test';exit();
global $current_user;
$check_data = true;
$errors = array();
$input_class_name = '';
$input_class_desc = '';
$meta_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];

$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

$spinners = MeoCrmRealestateManagement::getAllSpinner($external_wpdb);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(!empty($meta_id))
    {
        $datas = array();
        $datas['meta_key'] = $_POST['meta_key'];
        $datas['meta_slug'] = $_POST['meta_slug'];
        $datas['meta_type'] = $_POST['meta_type'];
        $datas['meta_unit'] = $_POST['meta_unit'];

        if(empty($datas['meta_key'])){ $check_data = false; $input_class_meta_key = 'warningField'; }
        if(empty($datas['meta_slug'])){ $check_data = false; $input_class_meta_slug = 'warningField'; }
        if(empty($datas['meta_type'])){ $check_data = false; $input_class_meta_type = 'warningField'; }

        if(!empty($datas) && $check_data)
        {
            $where = array('id' => $meta_id);
            $result = MeoCrmRealestateManagement::updateMetaLot($external_wpdb,$datas,$where);

            if($result['success'])
            {
                header('Location:/meo-crm-realestate-form-meta/?project='.$project_id.'&id='.$meta_id);
            }
        }

    }else{

        $datas = array();
        $datas['meta_key'] = $_POST['meta_key'];
        $datas['meta_slug'] = $_POST['meta_slug'];
        $datas['meta_type'] = $_POST['meta_type'];
        $datas['meta_unit'] = $_POST['meta_unit'];

        if(empty($datas['meta_key'])){ $check_data = false; $input_class_meta_key = 'warningField'; }
        if(empty($datas['meta_slug'])){ $check_data = false; $input_class_meta_slug = 'warningField'; }
        if(empty($datas['meta_type'])){ $check_data = false; $input_class_meta_type = 'warningField'; }

        if($check_data)
        {
            $resultInsert = MeoCrmRealestateManagement::insertMetaLot($external_wpdb, $datas);
        }else $resultInsert = array('success' => 0);

        if($resultInsert['success'])
        {
            $insert_id = $resultInsert['id'];

            if(isset($insert_id) && !empty($insert_id))
            {
                header('Location:/meo-crm-realestate-form-meta/?project='.$project_id.'&id='.$insert_id);
            }

        }else{
            $errors[] = 'Erreur durant l\'insertion';
        }
    }

    if(is_array($errors) && !empty($errors))
    {
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($meta_id))
{
    $meta = MeoCrmRealestateManagement::getMetaLotById($external_wpdb, $meta_id);

    $old_meta_key = (!empty($meta[0]->meta_key)) ? $meta[0]->meta_key : '';
    $old_meta_slug = (!empty($meta[0]->meta_slug)) ? $meta[0]->meta_slug : '';
    $old_meta_type = (!empty($meta[0]->meta_type)) ? $meta[0]->meta_type : '';
    $old_meta_order = (!empty($meta[0]->meta_order)) ? $meta[0]->meta_order : 0;
    $old_meta_filter = (!empty($meta[0]->meta_filter)) ? $meta[0]->meta_filter : '';
    $old_meta_unit = (!empty($meta[0]->meta_unit)) ? $meta[0]->meta_unit : '';
}else{
    $old_meta_key = '';
    $old_meta_slug = '';
    $old_meta_type = '';
    $old_meta_order = '';
    $old_meta_unit = '';
    $old_meta_filter = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    ?>
    <form method="post" name="add_status" id="form_status" action="<?php //echo site_url('/meo-crm-realestate-form-status/').'?project='.$project_id.'&id='.$meta_id ?>">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>Meta</h2>

        <div class="input-group">
            <label>Meta</label>
            <input type="text" name="meta_key" id="meta_key" value="<?php echo $old_meta_key ?>" placeholder="Meta" class="<?php echo (!empty($input_class_meta_key))? $input_class_meta_key : '' ?>" />
        </div>

        <div class="input-group">
            <label>Meta slug</label>
            <input type="text" name="meta_slug" id="meta_slug" value="<?php echo $old_meta_slug ?>" placeholder="Slug" class="<?php echo (!empty($input_class_meta_slug))? $input_class_meta_slug : '' ?>" />
        </div>

        <div class="input-group">
            <label>Meta type</label>
            <select name="meta_type" id="meta_type" class="<?php echo (!empty($input_class_meta_type))? $input_class_meta_type : '' ?>">
                <option value="0"<?php echo (empty($old_meta_type)) ? ' selected ' : '' ?>>Choisir un type</option>
                <option value="text"<?php echo ($old_meta_type == 'text') ? ' selected ' : '' ?>>Texte</option>
                <option value="boolean"<?php echo ($old_meta_type == 'boolean') ? ' selected ' : '' ?>>Boolean</option>
                <option value="integer"<?php echo ($old_meta_type == 'integer') ? ' selected ' : '' ?>>Nombre entier</option>
                <option value="float"<?php echo ($old_meta_type == 'float') ? ' selected ' : '' ?>>Nombre virgule</option>
            </select>
        </div>

        <div class="input-group">
            <label>Meta ordre</label>
            <input type="text" name="meta_order" id="meta_order" value="<?php echo $old_meta_order ?>" placeholder="Ordre" class="" />
        </div>

        <div class="input-group">
            <label>Meta unit&eacute;</label>
            <input type="text" name="meta_unit" id="meta_unit" value="<?php echo $old_meta_unit ?>" placeholder="Unit&eacute;" class="" />
        </div>

        <input type="checkbox" name="meta_filter" id="meta_filter" value="1" class="" <?php echo ($old_meta_filter) ? 'checked' : '' ?> />&nbsp;&nbsp;<label>Meta filtre</label>

        <br/>

        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-meta-list/?project='.$project_id ?>" name=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
    <?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?>
    <script type="text/javascript">
        window.$ = jQuery;
        $(document).ready(function(){
        });
    </script>
<?php get_footer(); ?>