<?php 
   
 get_header();  
 
	 # Get User Project(s)
	 $userProjects = meo_crm_projects_getProjects();
	 
	 if($userProjects){

	 	?>
 		 	<h1>Gestion des lots</h1>
 			<div class="redband">
 			    <input type="hidden" name="ajax_url" value="<?php echo admin_url('admin-ajax.php'); ?>" />
 			    <label>Project List</label>
 			    <select id="project-list-lot-manager" name="project-list-sale" class="project-list-lot">
 			        <option value="0">Choose a project</option>
 			        <?php foreach($userProjects as $p => $project): ?>
 			            <option value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
 			        <?php endforeach; ?>
 			    </select>
 			</div>
 			<div id="lot-list-content">
 			    
 			</div>
 		<?php 
	 }
	 else {
	 	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access projects list data real estate management");
	 	MeoCrmCoreTools::meo_crm_core_403();
	 	die();
	 }

get_footer(); 

?>