<?php 
/*
 * Plugin one page
 * Page to show 
 */

global $wpdb;
$current_user = wp_get_current_user();
$projects = ProjectModel::getProjectsImmo();
$url = get_site_url().'/wp-admin/admin-ajax.php';
$projects_user = ProjectModel::getUserProjects($current_user);

?>
<div id="meo-crm-realestate-container" class="wrap"> 
    <input type="hidden" name="url_ajax" id="url_ajax" value="<?php echo $url ?>" />
    <input type="hidden" name="url_upload" id="url_upload" value="" />
    <div id="zone-message"></div>
    
    <!-- TOOLBAR -->
    <div id="toolbar-realestate">
        <div class="toolbar-projects">
            <label for="realestate-list-project">Choisir un projet immobilier</label>
            <select id="realestate-list-project" name="realestate-list-project" class="realestate-list-project">
                <option selected="selected" value="-1">Choose a realestate project</option>
                <?php foreach($projects as $project): ?>
                    <option value="<?php echo $project->id ?>"><?php echo $project->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="toolbar-import">
            <i class="fa fa-download" aria-hidden="true"></i>
        </div>
        <div style="clear:both;"></div>
        <input type="hidden" id="realestate_project_id" name="realestate_project_id" value="0" />        
    </div>
    <!-- / TOOLBAR -->
    
    <div id="meo-crm-dynamic-content">    
        <!-- DASHBOARD -->
        <div id="meo-crm-realestate-dashboard">
            <div id="dashicon-developpement" class="dashicon-realestate">
                <div id="">
                  <i class="fa fa-suitcase" aria-hidden="true"></i>
                  <p>Developpements</p>
                </div>
            </div>
            <div id="dashicon-sector" class="dashicon-realestate">
                <!--<i class="fa fa-map-signs" aria-hidden="true"></i>-->
                <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
                <p>Sectors</p>
            </div>
            <div id="dashicon-building" class="dashicon-realestate">
                <i class="fa fa-building" aria-hidden="true"></i>
                <p>Buildings</p>
            </div>
            <div id="dashicon-floor" class="dashicon-realestate">
                <i class="fa fa-database" aria-hidden="true"></i>
                <p>Floors</p>
            </div>
            <div id="dashicon-lot" class="dashicon-realestate">
                <i class="fa fa-home" aria-hidden="true"></i>
                <p>Lots</p>
            </div>
            <div id="dashicon-plan" class="dashicon-realestate">
                <i class="fa fa-map" aria-hidden="true"></i>
                <p>Plans</p>
            </div>
            <div id="dashicon-status" class="dashicon-realestate">
                <i class="fa fa-tags" aria-hidden="true"></i>
                <p>Status</p>
            </div>
            <div id="dashicon-meta-lot" class="dashicon-realestate">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <p>Manage Lot Field</p>
            </div>
            <div id="dashicon-spinner" class="dashicon-realestate">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                <p>Show Spinner exported</p>
            </div>
            <div id="disabled_dashboard"></div> 
        </div>
        <!-- / DASHBOARD -->
        <!-- DEVELOPPEMENT -->
            <!-- DEVELOPPEMENT LIST -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-developpement-list">
                <h1>
                    Liste des Developpements
                    <a id="new-developpement" class="page-title-action" href="#">Ajouter un developpement</a>
                    <a id="back-developpement-dashboard" class="page-title-action" href="#">Retour</a>
                </h1>
                <div id="table-container-developpement"></div>
            </div>
            <!-- / DEVELOPPEMENT LIST -->    
            <!-- DEVELOPPEMENT FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-developpement-form">
                <h1>
                    Formulaire Developpements
                    <a id="back-developpement" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_developpement" enctype="multipart/form-data">  
                    <input type="hidden" name="developpement_id" id="developpement_id" value="" />
                    <div class="form-developpement">
                        <table class="form-table meo-form-table">
                            <tr  class="form-field">
                                <th>
                                    <label for="developpement_title">
                                        Titre du developpement
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="developpement_title" id="developpement_title" value="" />
                                </td>
                            </tr>                    
                            <tr  class="form-field">
                                <th>
                                    <label for="developpement_description">
                                        Descripiton du developpement
                                    </label>
                                </th>
                                <td>
                                    <textarea name="developpement_description" id="developpement_description"></textarea>
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="url_project">
                                        Plan developpement 3D
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="developpement_image_3d" id="developpement_image_3d" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="balise_developpement_image_3d" />
                                    <input type="hidden" id="plan_developpement_3d_old" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="url_project">
                                        Plan developpement 2D
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="developpement_image_2d" id="developpement_image_2d" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="balise_developpement_image_2d" />
                                    <input type="hidden" id="plan_developpement_2d_old" value="" />
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                            <button id="developpement_submit" class="button button-primary" type="submit" name="developpement_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / DEVELOPPEMENT FORM -->
        <!-- / DEVELOPPEMENT -->        
        <!-- SECTOR -->
            <!-- SECTOR LIST -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-sector-list">
                <h1>
                    Liste des secteurs
                    <a id="new-sector" class="page-title-action" href="#">Ajouter un secteur</a>
                    <a id="back-sector-dashboard" class="page-title-action" href="#">Retour</a>
                </h1>
                <div id="table-container-sector"></div>
            </div>
            <!-- / SECTOR LIST -->    
            <!-- SECTOR FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-sector-form">
                <h1>
                    Formulaire Secteur
                    <a id="back-sector" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_sector" enctype="multipart/form-data">  
                    <input type="hidden" name="sector_id" id="sector_id" value="" />
                    <div class="form-sector">
                        <table class="form-table meo-form-table">
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_title">
                                        Developpement lié
                                    </label>
                                </th>
                                <td id="developpement_select_list"></td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_title">
                                        Titre du secteur
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="sector_title" id="sector_title" value="" />
                                </td>
                            </tr>                    
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_description">
                                        Descripiton du secteur
                                    </label>
                                </th>
                                <td>
                                    <textarea name="sector_description" id="sector_description"></textarea>
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_image_3d">
                                        Plan secteur 3D
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="sector_image_3d" id="sector_image_3d" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="balise_sector_image_3d" />
                                    <input type="hidden" id="plan_sector_3d_old" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_image_2d">
                                        Plan secteur 2D
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="sector_image_2d" id="sector_image_2d" value="">
                                </td>
                            </tr>                            
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="balise_sector_image_2d" />
                                    <input type="hidden" id="plan_sector_2d_old" value="" />
                                </td>
                            </tr>                            
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_developpement_image_3d">
                                        Plan developpement 3D hover
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="sector_developpement_image_3d" id="sector_developpement_image_3d" value="">
                                </td>
                            </tr>                            
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="balise_sector_developpement_image_3d" />
                                    <input type="hidden" id="plan_sector_developpement_3d_old" value="" />
                                </td>
                            </tr>                            
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_developpement_image_2d">
                                        Plan developpement 2D hover
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="sector_developpement_image_2d" id="sector_developpement_image_2d" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="balise_sector_developpement_image_2d" />
                                    <input type="hidden" id="plan_sector_developpement_2d_old" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_title">
                                        Coordinate imageMap 3D
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="coordinate_sector_developpement_3d" id="coordinate_sector_developpement_3d" value="" />
                                </td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="sector_title">
                                        Coordinate imageMap 2D
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="coordinate_sector_developpement_2d" id="coordinate_sector_developpement_2d" value="" />
                                </td>
                            </tr>  
                        </table>
                        <p class="submit">
                            <button id="sector_submit" class="button button-primary" type="submit" name="sector_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / SECTOR FORM -->
        <!-- / SECTOR -->        
        <!-- BUILDING -->
            <!-- BUILDING LIST -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-building-list">
                <h1>
                    Liste des Buildings
                    <a id="new-building" class="page-title-action" href="#">Ajouter un building</a>
                    <a id="back-building-dashboard" class="page-title-action" href="#">Retour</a>
                </h1>
                <div id="table-container-building"></div>
            </div>
            <!-- / BUILDING LIST -->    
            <!-- BUILDING FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-building-form">
                <h1>
                    Building
                    <a id="back-building" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_building" enctype="multipart/form-data">  
                    <input type="hidden" name="building_id" id="building_id" value="" />
                    <div class="form-building">
                        <table class="form-table meo-form-table">   
                            <tr  class="form-field">
                                <th>
                                    <label for="building_title">
                                        Choix secteur
                                    </label>
                                </th>
                                <td>
                                    <select name="building_sector_list" id="building_sector_list"></select>
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="building_title">
                                        Titre
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="building_title" id="building_title" value="" />
                                </td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="building_description">
                                        Description
                                    </label>
                                </th>
                                <td>
                                    <textarea name="building_description" id="building_description"></textarea>
                                </td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="building_indoor_parc">
                                        Parking indoor
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="building_indoor_parc" id="building_indoor_parc" value="" />
                                </td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="building_outdoor_parc">
                                        Parking outdoor
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="building_outdoor_parc" id="building_outdoor_parc" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="building_sector_image_3d_hover">
                                        Image building 3D hover
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="building_sector_image_3d_hover" id="building_sector_image_3d_hover" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_building_sector_image_3d_hover" />
                                    <input type="hidden" name="input_building_sector_image_3d_hover" id="input_building_sector_image_3d_hover" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="coordinate_building_sector_3d">
                                        Coordinate imageMap 3D
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="coordinate_building_sector_3d" id="coordinate_building_sector_3d" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="building_sector_image_2d_hover">
                                        Image building 2D hover
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="building_sector_image_2d_hover" id="building_sector_image_2d_hover" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_building_sector_image_2d_hover" />
                                    <input type="hidden" name="input_building_sector_image_2d_hover" id="input_building_sector_image_2d_hover" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="coordinate_building_sector_2d">
                                        Coordinate imageMap 2D
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="coordinate_building_sector_2d" id="coordinate_building_sector_2d" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="building_front_face">
                                        Image building Front
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="building_front_face" id="building_front_face" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_building_front_face" />
                                    <input type="hidden" id="input_building_front_face" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="building_back_face">
                                        Image building Back
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="building_back_face" id="building_back_face" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_building_back_face" />
                                    <input type="hidden" id="input_building_back_face" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="building_preview">
                                        Image building Preview
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="building_preview" id="building_preview" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_building_preview" />
                                    <input type="hidden" id="input_building_preview" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="building_sector">
                                        Image building Sector
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="building_sector" id="building_sector" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_building_sector" />
                                    <input type="hidden" id="input_building_sector" value="" />
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                            <button id="building_submit" class="button button-primary" type="submit" name="status_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / BUILDING FORM -->
        <!-- / BUILDING -->  
        <!-- FLOOR -->
            <!-- FLOOR LIST -->
                <div class="meo-crm-realestate-window" id="meo-crm-realestate-floor-list">
                    <h1>
                        Liste des Etages 
                        <a id="new-floor" class="page-title-action" href="#">Ajouter un étage</a>
                        <a id="back-floor-dashboard" class="page-title-action" href="#">Retour</a>
                    </h1>
                    <div id="table-container-floor"></div>
                </div>
            <!-- / FLOOR LIST -->    
        <!-- FLOOR FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-floor-form">
                <h1>
                    Floor
                    <a id="back-floor" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_floor" enctype="multipart/form-data">  
                    <input type="hidden" name="floor_id" id="floor_id" value="" />
                    <div class="form-floor">
                        <table class="form-table meo-form-table">
                            <tr  class="form-field">
                                <th>
                                    <label for="floor_building_id">
                                        Building Linked
                                    </label>
                                </th>
                                <td>
                                    <select id="floor_building_id" name="floor_building_id"></select>
                                </td>
                            </tr>   
                            <tr  class="form-field">
                                <th>
                                    <label for="floor_title">
                                        Title
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="floor_title" id="floor_title" value="" />
                                </td>
                            </tr>   
                            <tr  class="form-field">
                                <th>
                                    <label for="floor_position">
                                        Position
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="floor_position" id="floor_position" value="" />
                                </td>
                            </tr> 
                        </table>
                        <p class="submit">
                            <button id="floor_submit" class="button button-primary" type="submit" name="floor_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / FLOOR FORM -->
        <!-- / FLOOR -->
         <!-- PLAN -->
            <!-- PLAN LIST -->
                <div class="meo-crm-realestate-window" id="meo-crm-realestate-plan-list">
                    <h1>
                        Liste des Plans
                        <a id="new-plan" class="page-title-action" href="#">Ajouter un plan</a>
                        <a id="back-plan-dashboard" class="page-title-action" href="#">Retour</a>
                    </h1>
                    <div id="table-container-plan"></div>
                </div>
            <!-- / PLAN LIST -->    
        <!-- PLAN FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-plan-form">
                <h1>
                    Plan
                    <a id="back-plan" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_plan" enctype="multipart/form-data">  
                    <input type="hidden" name="plan_id" id="plan_id" value="" />
                    <div class="form-developpement">
                        <table class="form-table meo-form-table">
                            <tr  class="form-field">
                                <th>
                                    <label for="plan_spinner">
                                        Spinner
                                    </label>
                                </th>
                                <td>
                                    <select name="plan_spinner" id="plan_spinner">
                                    </select>
                                </td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="plan_title">
                                        Titre
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="plan_title" id="plan_title" value="" />
                                </td>
                            </tr>  
                            <tr  class="form-field">
                                <th>
                                    <label for="plan_description">
                                        Description
                                    </label>
                                </th>
                                <td>
                                    <textarea name="plan_description" id="plan_description"></textarea>
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="plan_document">
                                        Document PDF
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="plan_document" id="plan_document" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a style="display: none;" target="_blank" id="link_plan_document">Voir le document</a>
                                    <input type="hidden" name="input_plan_document" id="input_plan_document" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="plan_image_2d">
                                        Plan 2D
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="plan_image_2d" id="plan_image_2d" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_plan_image_2d" />
                                    <input type="hidden" id="input_plan_image_2d" name="input_plan_image_2d" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="plan_image_3d">
                                        Plan 3D
                                    </label>
                                </th>
                                <td>
                                    <input type="file" name="plan_image_3d" id="plan_image_3d" value="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img width="300" src="" alt="" id="img_plan_image_3d" />
                                    <input type="hidden" id="input_plan_image_3d" name="input_plan_image_3d" value="" />
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                            <button id="plan_submit" class="button button-primary" type="submit" name="plan_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / PLAN FORM -->
        <!-- / PLAN -->
        
        <!-- LOT -->
            <!-- LOT LIST -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-lot-list">
                <h1>
                    Liste des lots
                    <a id="new-lot" class="page-title-action" href="#">Ajouter un lot</a>
                    <a id="back-lot-dashboard" class="page-title-action" href="#">Retour</a>
                </h1>
                <div id="table-container-lot"></div>
            </div>
            <!-- / LOT LIST -->    
            <!-- LOT FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-lot-form">
                <h1>
                    Lot
                    <a id="back-lot" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_lot" enctype="multipart/form-data">  
                    <input type="hidden" name="lot_id" id="lot_id" value="" />
                    <div class="form-lot">
                        
                        <table class="form-table meo-form-table meo-form-section">
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="plan_lot_id">
                                        Plans du lot
                                    </label>
                                </th>
                                <td>
                                    <select name="plan_lot_id" id="plan_lot_id"></select>
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="status_id">
                                        Status du lot
                                    </label>
                                </th>
                                <td>
                                    <select name="status_id" id="list_status_id"></select>
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="type_lot">
                                        Type de lot
                                    </label>
                                </th>
                                <td>
                                    <select name="type_lot" id="type_lot">
                                        <option value="-1">Choose a lot type</option>
                                        <option value="appartement">Appartement</option>
                                        <option value="maison">Maison</option>
                                    </select>
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="lot_title">
                                        Titre
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="lot_title" id="lot_title" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="lot_description">
                                        Description
                                    </label>
                                </th>
                                <td>
                                    <textarea id="lot_description" name="lot_description"></textarea>
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="lot_room">
                                        Pièces
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="lot_room" id="lot_room" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="lot_surface">
                                        Surface du lot
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="lot_surface" id="lot_surface" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th style="width: 15% !important">
                                    <label for="lot_price">
                                        Prix du lot
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="lot_price" id="lot_price" value="" />
                                </td>
                            </tr> 
                        </table>
                        <table  id="dynamic-form" class="form-table meo-form-table meo-form-section"></table>
                        <div id="table-list-floor"  style="border-top:1px solid #aaa">
                            <h4>Etage du lot <button type="button" class="duplicate-row"><i class="fa fa-plus"></i></button></h4>
                            <table class="form-table meo-form-table meo-form-section meo-duplicate-table">                            
                                <tr style="display:none; width:100%;" class="empty-row">
                                    <td style="width:100%; border-bottom: 1px solid #acacac; display:block">
                                        <input type="hidden" class="floor_lot_id" name="floor_lot_id[]" value="">
                                        <table>
                                            <tr>
                                                <td>
                                                    <select id="floor_id_list" name="floor_id_list[]"></select>
                                                </td>
                                                <td>
                                                    <input type="file" name="input_lot_floor_plan[]" id="input_lot_floor_plan" value="" />
                                                    <img style="display:none" id="image_lot_floor_plan" src="" />
                                                    <input type="hidden" name="old_image_lot_floor_plan[]" id="old_image_lot_floor_plan" value="" />
                                                </td>
                                                <td>
                                                    <label for="floor_entry">Entrée</label>
                                                    <input type="checkbox" name="floor_entry[]" onclick="clickCheckbox(this)" class="floor_entry" value="1" />
                                                </td>
                                                <td>
                                                    <button type="button" onclick="removeTableRow(this)" class="remove-row"><i class="fa fa-trash"></i></button>
                                                </td>  
                                            </tr>
                                            <tr id="plan_building_floor" class="all_plan_building_floor" style="width:100%; border-top: 1px dashed #bdbdbd"></tr>
                                        </table>
                                    </td>                                    
                                </tr>
                            </table>                             
                        </div>
                        <p class="submit">
                            <button id="lot_submit" class="button button-primary" type="submit" name="status_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / LOT FORM -->
        <!-- / LOT -->
        
        <!-- STATUS -->
            <!-- STATUS LIST -->
                <div class="meo-crm-realestate-window" id="meo-crm-realestate-status-list">
                    <h1>
                        Liste des Status
                        <a id="new-status" class="page-title-action" href="#">Ajouter un status</a>
                        <a id="back-status-dashboard" class="page-title-action" href="#">Retour</a>
                    </h1>
                    <div id="table-container-status"></div>
                </div>
            <!-- / STATUS LIST -->    
        <!-- STATUS FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-status-form">
                <h1>
                    Status
                    <a id="back-status" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_status" enctype="multipart/form-data">  
                    <input type="hidden" name="status_id" id="status_id" value="" />
                    <div class="form-developpement">
                        <table class="form-table meo-form-table">
                            <tr  class="form-field">
                                <th>
                                    <label for="status_name">
                                        Status
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="status_name" id="status_name" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="status_name">
                                        Color
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="status_color" id="status_color" value="" />
                                </td>
                            </tr>  
                        </table>
                        <p class="submit">
                            <button id="status_submit" class="button button-primary" type="submit" name="status_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / STATUS FORM -->
        <!-- / STATUS -->
        <!-- META LOT -->
            <!-- META LOT LIST -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-meta-lot-list">
                <h1>
                    Liste des Metas de lot
                    <a id="new-meta-lot" class="page-title-action" href="#">Ajouter une meta donnée</a>
                    <a id="back-meta-lot-dashboard" class="page-title-action" href="#">Retour</a>
                </h1>
                <div id="table-container-meta-lot"></div>
            </div>
            <!-- / META LOT LIST -->    
            <!-- META LOT FORM -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-meta-lot-form">
                <h1>
                    Meta Lot
                    <a id="back-meta-lot" class="page-title-action" href="#">Retour</a>
                </h1>            
                <form method="post" id="form_meta_lot" enctype="multipart/form-data">  
                    <input type="hidden" name="meta_lot_id" id="meta_lot_id" value="" />
                    <div class="form-developpement">
                        <table class="form-table meo-form-table">
                            <tr  class="form-field">
                                <th>
                                    <label for="meta_lot_key">
                                        Meta Key
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="meta_lot_key" id="meta_lot_key" value="" />
                                </td>
                            </tr>
                            <tr  class="form-field">
                                <th>
                                    <label for="meta_lot_slug">
                                        Meta Slug
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="meta_lot_slug" id="meta_lot_slug" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="meta_lot_type">
                                        Meta Input Type
                                    </label>
                                </th>
                                <td>
                                    <select name="meta_lot_type" id="meta_lot_type">
                                        <option value="-1">Choose a input type</option>
                                        <option value="text">Texte</option>
                                        <option value="checkbox">Checkbox</option>
                                    </select>
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="meta_lot_order">
                                        Meta Ordre
                                    </label>
                                </th>
                                <td>
                                    <input type="text" name="meta_lot_order" id="meta_lot_order" value="" />
                                </td>
                            </tr> 
                            <tr  class="form-field">
                                <th>
                                    <label for="meta_lot_filter">
                                        Meta Filter
                                    </label>
                                </th>
                                <td>
                                    <input type="checkbox" name="meta_lot_filter" id="meta_lot_filter" value="1" />
                                </td>
                            </tr> 
                        </table>
                        <p class="submit">
                            <button id="meta_lot_submit" class="button button-primary" type="submit" name="status_submit">Enregistrer</button>
                        </p>
                    </div>
                </form>
            </div>
            <!-- / META LOT FORM -->
        <!-- / META LOT -->
        
        <!-- SPINNER -->
            <!-- SPINNER LIST -->
            <div class="meo-crm-realestate-window" id="meo-crm-realestate-spinner-list">
                <h1>
                    Gestion des spinners
                    <a id="back-spinner-dashboard" class="page-title-action" href="#">Retour</a>
                </h1>
                <h2 style="font-size: 16px">Spinners realestate</h2>
                <div id="table-container-spinner-app"></div>
                <h2 style="font-size: 16px">
                    Spinners exporter projet
                </h2>
                <div id="table-container-spinner-website"></div>
            </div>
            <!-- / SPINNER list -->  
        <!-- / SPINNER -->
        
        <div id="meo-crm-waiting-bloc">
            <img src="<?php echo get_site_url().'/wp-content/plugins/meo-crm-core/images/loader.gif'; ?>" alt="wait please" />
        </div>
    </div>    
</div>