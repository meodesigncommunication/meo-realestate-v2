<?php

class MeoCrmRealestateManagementPdfPriceList extends FPDI
{
    private $pos_x = 12;
    private $pos_y = 40;
    private $pos_y_page_nb = 0;
    private $page_height = 0;
    private $end_page_calc = 10;
    private $total_lot = 0;
    private $count = 0;
    private $devise = 'CHF';
    private $page_orientation = 'P';
    private $encoding = 'ISO-8859-1';
    private $font_family = 'Helvetica';
    private $upload_price_list = '/'.PRICE_LIST_UPLOAD_FOLDER.'/';
    private $pdf_parameter = array();
    private $lots = array();
    private $other_price = array();
    private $status = array();
    private $project_id = 0;
    private $column_width = 0;
    private $count_lot = 0;
    private $date_created = '';
    private $upload_path = '';
    private $template_pdf = '';
    private $output_folder = '';

    public function __construct($pdf_parameter,$lots,$status,$project_id)
    {
        parent::__construct();
        $this->project_id = $project_id;
        $this->pdf_parameter = $pdf_parameter;
        $this->lots = $lots;
        $this->total_lot = count($lots);
        $this->status = $status;
        $this->other_price = (isset($pdf_parameter['other_price']) && !empty($pdf_parameter['other_price'])) ? $pdf_parameter['other_price'] : array();
        $this->devise = 'CHF';
        $this->output_folder = '/projects/'.$project_id.'/pdfs';

        if(isset($pdf_parameter['header']) && !empty($pdf_parameter['header']))
        {
            $this->page_orientation = (count($pdf_parameter['header']) < 8) ? 'P' : 'L';
        }else{
            $this->page_orientation = 'P';
        }
    }

    public function generatePdfPriceList()
    {
        if(!empty($this->pdf_parameter))
        {
            $this->date_created = date('d-m-Y H:i:m');
            if ($this->page_orientation == 'P') {
                $this->page_height = ($this->h - 30);
            } else {
                $this->page_height = ($this->h - 40);
                $this->end_page_calc = 80;
            }
            $this->upload_path = wp_upload_dir();
            $this->SetAutoPageBreak(false);
            $this->generateNewPage(true);
            $this->generateListHeader();
            $this->generateListeData();
            if ($this->checkFolderBeforeSave()) {
                $this->Output($this->upload_path['basedir'] . $this->output_folder . '/price_list.pdf');
            }
            return $this->upload_path['baseurl'] . $this->output_folder . '/price_list.pdf';
        }else{
            return false;
        }
    }

    private function generateNewPage($firstPage=false)
    {
        $color = (isset($this->pdf_parameter['color_theme']) && !empty($this->pdf_parameter['color_theme'])) ? MeoCrmCoreHelper::hex2rgb($this->pdf_parameter['color_theme']) : MeoCrmCoreHelper::hex2rgb('#ffffff');
        $this->setFillColor($color['red'],$color['green'],$color['blue']);
        $this->setDrawColor($color['red'],$color['green'],$color['blue']);

        if($this->page_orientation == 'P')
        {
            $this->pos_y = ($this->pos_y != 40) ? 40 : $this->pos_y;
            $this->template_pdf = (isset($this->pdf_parameter['pdf_template_portrait'])) ? $this->upload_path['basedir'].$this->upload_price_list.$this->project_id.'/'.$this->pdf_parameter['pdf_template_portrait'] : '';
        }else{
            $this->pos_y = ($this->pos_y != 30) ? 30 : $this->pos_y;
            $this->template_pdf = $this->upload_path['basedir'].$this->upload_price_list.$this->project_id.'/'.$this->pdf_parameter['pdf_template_landscape'];
        }

        $pagecount = $this->setSourceFile($this->template_pdf);
        $tplIdx = $this->importPage(1);
        $this->AliasNbPages();
        $this->AddPage($this->page_orientation, "A4");
        $this->setLeftMargin(12);
        $this->useTemplate($tplIdx, 0, 0);
        $this->setX($this->pos_x);
        $this->SetY($this->pos_y);

        if($firstPage)
        {
            $color = MeoCrmCoreHelper::hex2rgb($this->pdf_parameter['color_theme']);
            $this->SetTextColor($color['red'],$color['green'],$color['blue']);
            $this->SetFont($this->font_family,'',5);
            $this->Cell(($this->w-24),3,$this->date_created,0,0,'R');
            $this->pos_y += 4;
            $this->SetY($this->pos_y);
            $this->SetFont($this->font_family,'', 12);
            $this->SetTextColor(255,255,255);
            $this->Cell(($this->w-24),5.1,'',1,0,'C',true);
            $this->pos_y += 5.1;
            $this->SetY($this->pos_y);
            $this->Cell(($this->w-24),3,'LISTE DE PRIX',1,0,'C',true);
            $this->pos_y += 3;
            $this->SetY($this->pos_y);
            $this->Cell(($this->w-24),5,'',1,0,'C',true);
            $this->pos_y += 7;
            $this->SetY($this->pos_y);
        }else{
            if ($this->page_orientation != 'P') {
                $this->pos_y += 5;
                $this->SetY($this->pos_y);
            }
        }
    }

    private function generateListHeader()
    {
        $color = MeoCrmCoreHelper::hex2rgb($this->pdf_parameter['color_theme']);
        $this->setFillColor($color['red'],$color['green'],$color['blue']);
        $this->setDrawColor($color['red'],$color['green'],$color['blue']);
        $this->setLineWidth(0.5);

        $this->SetFont($this->font_family,'',9);
        $this->SetTextColor($color['red'],$color['green'],$color['blue']);
        $this->column_width = ($this->w-24)/(count($this->pdf_parameter['header']));

        foreach($this->pdf_parameter['header'] as $header_key => $header_item)
        {
            $this->Cell($this->column_width,5,iconv("UTF-8", $this->encoding."//TRANSLIT", __($header_item['label_1'])),0,0,'C');
        }

        $this->pos_y += 5;
        $this->SetY($this->pos_y);

        foreach($this->pdf_parameter['header'] as $header_key => $header_item)
        {
            $this->Cell($this->column_width,5,iconv("UTF-8", $this->encoding."//TRANSLIT", __($header_item['label_2'])),0,0,'C');
        }

        $this->pos_y += 7;
        $this->SetY($this->pos_y);
        $this->Cell(($this->w-24),0.5,'',0,0,'C',true);
        $this->pos_y += 0.5;
        $this->SetY($this->pos_y);
    }

    private function generateListeData()
    {
        $count = 2;
        $this->SetFont($this->font_family,'',9);
        $this->SetTextColor(20,20,20);

        foreach($this->lots as $lot)
        {

            $this->count++;
            $this->SetTextColor(20,20,20);
            if($count%2)
            {
                $this->setFillColor(235,235,235);
            }else{
                $this->setFillColor(255,255,255);
            }
            foreach($this->pdf_parameter['header'] as $header_item)
            {
                if(isset($lot[$header_item['header_key']]) && !empty($lot[$header_item['header_key']]))
                {
                    if($header_item['header_key'] == 'price')
                    {
                        $hide_price = false;
                        foreach($this->status as $status)
                        {
                            if($status->id == $lot['status_id'] && $status->hide_price)
                            {
                                $hide_price = true;
                            }
                        }
                        if($hide_price)
                        {
                            $color = MeoCrmCoreHelper::hex2rgb($lot['status_color']);
                            $this->SetTextColor($color['red'],$color['green'],$color['blue']);
                            $this->Cell($this->column_width,10,iconv("UTF-8", $this->encoding."//TRANSLIT", $lot['status_name']),0,0,'C',true);
                            $this->SetTextColor(20,20,20);

                        }else $this->Cell($this->column_width,10,number_format($lot[$header_item['header_key']],0 ,".","'").' '.$this->devise ,0,0,'C',true);

                    }else{

                        $replaces_data = array(
                            array(
                                'search' => '<sup>',
                                'replace' => ''
                            ),
                            array(
                                'search' => '</sup>',
                                'replace' => ''
                            )
                        );

                        $header_key =  iconv("UTF-8", $this->encoding."//TRANSLIT", $lot[$header_item['header_key']]);

                        foreach($replaces_data as $replace)
                        {
                            $header_key =  str_replace($replace['search'], $replace['replace'], $header_key);
                        }

                        $this->Cell($this->column_width,10,$header_key,0,0,'C',true);
                    }

                }else{
                    foreach($lot['metas'] as $meta)
                    {
                        if($header_item['header_key'] == $meta['meta_slug'])
                        {
                            $meta_value = (!empty($meta['meta_value'])) ? $meta['meta_value'] : '-';
                            $this->Cell($this->column_width,10,$meta_value,0,0,'C',true);
                        }
                    }
                }
            }
            $count++;
            $this->count_lot++;
            $this->pos_y += 10;
            $this->SetY($this->pos_y);
            if($this->page_height <= ($this->pos_y+((count($this->other_price)*10)+$this->end_page_calc)) )
            {
                $this->generateOtherPriceList();
                if($this->count < $this->total_lot)
                {
                    $this->generateNewPage(false);
                    $this->generateListHeader();
                }
            }else if(count($this->lots) == $this->count_lot){
                $this->generateOtherPriceList();
            }
        }
    }

    private function generateOtherPriceList()
    {
        foreach($this->other_price as $key => $value)
        {
            $width_max = $this->w-24;
            $color = MeoCrmCoreHelper::hex2rgb($this->pdf_parameter['color_info']);
            $this->setFillColor($color['red'],$color['green'],$color['blue']);
            $this->setDrawColor($color['red'],$color['green'],$color['blue']);
            $this->Cell(($width_max-$this->column_width),10,iconv("UTF-8", $this->encoding."//TRANSLIT", $value['label']),1,0,'R',true);
            $this->Cell($this->column_width,10,number_format($value['price'],0 ,".","'").' '.$this->devise,1,0,'C',true);
            $this->pos_y += 10;
            $this->SetY($this->pos_y);
        }
        if($this->PageNo() == 1)
        {
            if ($this->page_orientation != 'P') {
                $this->pos_y_page_nb = $this->pos_y;
            }else{
                $this->pos_y_page_nb = $this->page_height-1;
            }
        }
        $this->SetY($this->pos_y_page_nb);
        $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    private function checkFolderBeforeSave()
    {
        $check_folder = true;
        if(isset($this->output_folder) && !empty($this->output_folder))
        {
            $folders = explode('/', $this->output_folder);
            $full_path = $this->upload_path['basedir'].'/';

            foreach($folders as $folder)
            {
                $full_path .= $folder.'/';
                if(!is_dir($full_path))
                {
                    if(!mkdir($full_path, 0777, true))
                    {
                        $check_folder = false;
                    }
                }
            }
            return $check_folder;
        }
    }
}