<?php 

/*
Plugin Name: MEO CRM REALESTATE MANAGEMENT
Description: Plugin to manage Realestate data. Requires MEO CRM Core plugin
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
  
FEATURES CUSTOM POST TYPE BEFORE IN "PORTE DU LAC"
SPINNER CUSTOM POST TYPE BEFORE IN "PORTE DU LAC"
 
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

define( 'WP_DEBUG', true );

$plugin_root = plugin_dir_path( __FILE__ );

define('DEVISE','CHF');
define('MEO_REALESTATE_MANAGEMENT_PLUGIN_ROOT', $plugin_root);
define('MEO_REALESTATE_MANAGEMENT_SLUG', 'meo-crm-realestate-management');
define('MEOACCESSPROJECTSTABLE','meo_crm_access_project');
define('PRICE_LIST_UPLOAD_FOLDER','price_list_tempate');

/*
 * CONSTANT NAME FOLDER
 */
define('FOLDER_DEVELOPPEMENT', 'developpement');
define('FOLDER_SECTOR', 'sectors');
define('FOLDER_BUILDING', 'buildings');
define('FOLDER_TYPO_STATUS_FLOOR_LOT', 'typo-status-floor-lot');
define('FOLDER_FLOOR_LOT', 'floor-lot');
define('FOLDER_PLANS', 'plans');

/* MEO_CRM_REALESTATE_PLANS_TABLE
 * CONTSTANT TABLE NAME DATABASE
 */
define('PREFIX_TABLE','wp_');
define('MEO_CRM_REALESTATE_POSTS_TABLE','posts');
define('MEO_CRM_REALESTATE_POSTMETA_TABLE','postmeta');
define('MEO_CRM_ACCESS_PROJECT_TABLE','meo_crm_access_project');
define('MEO_CRM_REALESTATE_BUILDING_TABLE','meo_crm_realestate_buildings');
define('MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE','meo_crm_realestate_coordinates_plan_lot_building');
define('MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE','meo_crm_realestate_developpements');
define('MEO_CRM_REALESTATE_FLOORS_TABLE','meo_crm_realestate_floors');
define('MEO_CRM_REALESTATE_FLOORS_LOT_TABLE','meo_crm_realestate_floor_lot');
define('MEO_CRM_REALESTATE_LINK_COORDINATES_PLAN_FLOOR_LOT_BUILDING_TABLE','meo_crm_realestate_link_coordinates_plan_floor_lot_building');
define('MEO_CRM_REALESTATE_LOTS_TABLE','meo_crm_realestate_lots');
define('MEO_CRM_REALESTATE_LOT_METAS_TABLE','meo_crm_realestate_lot_metas');
define('MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE','meo_crm_realestate_lot_meta_value');
define('MEO_CRM_REALESTATE_PLANS_TABLE','meo_crm_realestate_plans');
define('MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE','meo_crm_realestate_plan_lot_building');
define('MEO_CRM_REALESTATE_SECTORS_TABLE','meo_crm_realestate_sectors');
define('MEO_CRM_REALESTATE_SPINNERS_TABLE','meo_crm_realestate_spinners');
define('MEO_CRM_REALESTATE_SPINNERS_POST_TABLE','meo_crm_realestate_spinner_post');
define('MEO_CRM_REALESTATE_STATUS_TABLE','meo_crm_realestate_status');
define('MEO_CRM_REALESTATE_PRICE_LIST_OPTIONS_TABLE','meo_crm_realestate_price_list_options');
define('MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES_TABLE','meo_crm_realestate_price_list_other_prices');
define('MEO_CRM_REALESTATE_PRICE_LIST_HEADER_VALUE_TABLE','meo_crm_realestate_price_list_header_value');

/*
 * CONTSTANT PAGE SLUG
 */
define('MEO_REALESTATE_MANAGEMENT_SLUG', 'meo-crm-realestate-management');
define('MEO_REALESTATE_MANAGEMENT_REQUEST_API_SLUG', 'meo-crm-realestate-request-api');
define('MEO_CRM_REALESTATE_MANAGE_TOOLPAGE_SLUG', 'meo-crm-realestate-management-tool');
define('MEO_CRM_REALESTATE_LIST_DEVELOPPEMENT_SLUG', 'meo-crm-realestate-developpement-list');
define('MEO_CRM_REALESTATE_FORM_DEVELOPPEMENT_SLUG', 'meo-crm-realestate-developpement-form');
define('MEO_CRM_REALESTATE_LIST_SECTOR_SLUG', 'meo-crm-realestate-sector-list');
define('MEO_CRM_REALESTATE_FORM_SECTOR_SLUG', 'meo-crm-realestate-sector-form');
define('MEO_CRM_REALESTATE_LIST_BUILDING_SLUG', 'meo-crm-realestate-building-list');
define('MEO_CRM_REALESTATE_FORM_BUILDING_SLUG', 'meo-crm-realestate-building-form');
define('MEO_CRM_REALESTATE_LIST_LOT_SLUG', 'meo-crm-realestate-lot-list');
define('MEO_CRM_REALESTATE_FORM_LOT_SLUG', 'meo-crm-realestate-lot-form');
define('MEO_CRM_REALESTATE_LIST_PLAN_SLUG', 'meo-crm-realestate-plan-list');
define('MEO_CRM_REALESTATE_FORM_PLAN_SLUG', 'meo-crm-realestate-plan-form');
define('MEO_CRM_REALESTATE_LIST_STATUS_SLUG', 'meo-crm-realestate-status-list');
define('MEO_CRM_REALESTATE_FORM_STATUS_SLUG', 'meo-crm-realestate-form-status');
define('MEO_CRM_REALESTATE_LIST_META_SLUG', 'meo-crm-realestate-meta-list');
define('MEO_CRM_REALESTATE_FORM_META_SLUG', 'meo-crm-realestate-meta-form');
define('MEO_CRM_REALESTATE_FORM_LIST_PRICE_PDF_SLUG', 'meo-crm-realestate-price-list-pdf-form');
define('MEO_CRM_REALESTATE_LIST_PLAN_FLOOR_LOT_SLUG', 'meo-crm-realestate-plan-floor-lot-list');
define('MEO_CRM_REALESTATE_FORM_PLAN_FLOOR_LOT_SLUG', 'meo-crm-realestate-form-plan-floor-lot');

/*
 * CONTSTANT PAGE TEMPLATE PATH
 */
define('MEO_CRM_REALESTATE_MANAGE_LOT_TPL', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-list-lot-front.php');
define('MEO_CRM_REALESTATE_MANAGE_REQUEST_API', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-request-api.php');
define('MEO_CRM_REALESTATE_MANAGE_TOOLPAGE', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-tool-management.php');
define('MEO_CRM_REALESTATE_LIST_DEVELOPPEMENT', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-developpement-list.php');
define('MEO_CRM_REALESTATE_FORM_DEVELOPPEMENT', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-developpement-form.php');
define('MEO_CRM_REALESTATE_LIST_SECTOR', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-sector-list.php');
define('MEO_CRM_REALESTATE_FORM_SECTOR', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-sector-form.php');
define('MEO_CRM_REALESTATE_LIST_BUILDING', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-building-list.php');
define('MEO_CRM_REALESTATE_FORM_BUILDING', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-building-form.php');
define('MEO_CRM_REALESTATE_LIST_LOT', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-lot-list.php');
define('MEO_CRM_REALESTATE_FORM_LOT', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-lot-form.php');
define('MEO_CRM_REALESTATE_LIST_PLAN', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-plan-list.php');
define('MEO_CRM_REALESTATE_FORM_PLAN', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-plan-form.php');
define('MEO_CRM_REALESTATE_LIST_STATUS', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-status-list.php');
define('MEO_CRM_REALESTATE_FORM_STATUS', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-status-form.php');
define('MEO_CRM_REALESTATE_LIST_META', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-meta-list.php');
define('MEO_CRM_REALESTATE_FORM_META', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-meta-form.php');
define('MEO_CRM_REALESTATE_FORM_LIST_PRICE_PDF', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-price-list-pdf-form.php');
define('MEO_CRM_REALESTATE_LIST_PLAN_FLOOR_LOT', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-plan-floor-lot-list.php');
define('MEO_CRM_REALESTATE_FORM_PLAN_FLOOR_LOT', '../../meo-crm-realestate-management/views/frontend/meo-crm-realestate-plan-floor-lot-form.php');

# Globals
global $meo_crm_realestate_management_db_version;

# Required Files
// AJAX FILES FUNCTIONS
require_once( $plugin_root . 'ajax/ajax-other-functions.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-developpement.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-sector.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-floor.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-building.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-status.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-plan.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-lot.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-meta-lot.php' );
require_once( $plugin_root . 'ajax/ajax-function-realestate-spinner.php' );
require_once( $plugin_root . 'ajax/ajax-function-lot-frontend.php' );

// CLASSES
require_once( $plugin_root . 'class-meo-crm-realestate-management.php' );
require_once( $plugin_root . 'classes/meo-crm-realestate-management-pdf-price-list.php' );

// MODELS
require_once( $plugin_root . 'models/AccessProjectModel.php' );
require_once( $plugin_root . 'models/DeveloppementModel.php' );
require_once( $plugin_root . 'models/MetaLotModel.php' );
require_once( $plugin_root . 'models/BuildingModel.php' );
require_once( $plugin_root . 'models/BuildingPlanFloorModel.php' );
require_once( $plugin_root . 'models/FloorModel.php' );
require_once( $plugin_root . 'models/FloorLotModel.php' );
require_once( $plugin_root . 'models/SectorModel.php' );
require_once( $plugin_root . 'models/StatusModel.php' );
require_once( $plugin_root . 'models/PlanModel.php' );
require_once( $plugin_root . 'models/LotModel.php' );
require_once( $plugin_root . 'models/SpinnerImmoModel.php' );
require_once( $plugin_root . 'models/PriceListModel.php' );
require_once( $plugin_root . 'models/ApiRequestModel.php' );

$meo_crm_realestate_management_db_version = '1.0';

# Plugin activation
function meo_crm_realestate_management_activate () {

   global $wpdb;
   global $meo_crm_realestate_management_db_version;
   
    $installed_dependencies = false;

    if ( is_plugin_active( 'meo-crm-core/__meo-crm-core.php' )) {
            $installed_dependencies = true;
    }
    
    if(!$installed_dependencies) {
        
        // WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
        // and this will prevent WordPress to activate the plugin.
        echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Core plugins before', 'meo-realestate').'</h3></div>';
        
        //Adding @ before will prevent XDebug output
        @trigger_error(__('Please install and activate the MEO CRM Core plugins before.', 'meo-realestate'), E_USER_ERROR);
        exit;
        
    }else{

    	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	
        $charset_collate = $wpdb->get_charset_collate();
        
        # Access Projects Table        
        $table_name = $wpdb->prefix . MEOACCESSPROJECTSTABLE;

        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            project_id INTEGER(11) NOT NULL,
            host_ftp VARCHAR(255) NULL,
            login_ftp VARCHAR(255) NULL,
            password_ftp VARCHAR(255) NULL,
            base_dir_ftp VARCHAR(255) NULL,
            host_db VARCHAR(255) NULL,
            login_db VARCHAR(255) NULL,
            password_db VARCHAR(255) NULL,
            name_db VARCHAR(255) NULL,
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";

        dbDelta( $sql );

        add_option( 'meo_crm_realestate_management_db_version', $meo_crm_realestate_management_db_version );
    }
}
register_activation_hook( __FILE__, 'meo_crm_realestate_management_activate' );

# Add Scripts and Styles
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script_style_realestate_management' );
function load_custom_wp_admin_script_style_realestate_management() {
    
    # JS
    wp_register_script( 'meo_crm_realestate_ajax_js',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-ajax-script.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_ajax_js' );
    
    wp_register_script( 'meo_crm_realestate_app_js_developpement',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-developpement.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_developpement' );
    
    wp_register_script( 'meo_crm_realestate_app_js_sector',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-sector.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_sector' );
    
    wp_register_script( 'meo_crm_realestate_app_js_building',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-building.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_building' );
    
    wp_register_script( 'meo_crm_realestate_app_js_floor',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-floor.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_floor' );
    
    wp_register_script( 'meo_crm_realestate_app_js_lot',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-lot.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_lot' );
    
    wp_register_script( 'meo_crm_realestate_app_js_status',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-status.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_status' );
    
    wp_register_script( 'meo_crm_realestate_app_js_meta_lot',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-meta-lot.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_meta_lot' );
    
    wp_register_script( 'meo_crm_realestate_app_js_spinner',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-spinner.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_spinner' );
    
    wp_register_script( 'meo_crm_realestate_app_js_plan',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-app-plan.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_app_js_plan' );
    
    # CSS    
    wp_register_style( 'get_bootstrap',  'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', false, '1.0.0' );
    wp_enqueue_style( 'get_bootstrap' );
    
    wp_register_style( 'meo_crm_realestate_css',  '/wp-content/plugins/meo-crm-realestate-management/css/meo-crm-realestate-style.css', false, '1.0.0' );
    wp_enqueue_style( 'meo_crm_realestate_css' );   
    
}

add_action( 'wp_enqueue_scripts', 'load_custom_wp_admin_script_style_realestate_front' );
function load_custom_wp_admin_script_style_realestate_front() {
    
    wp_register_script( 'meo_crm_realestate_lot_list_front',  '/wp-content/plugins/meo-crm-realestate-management/js/meo-crm-realestate-front.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_realestate_lot_list_front' );    
    wp_localize_script( 'meo_crm_realestate_lot_list_front', 'meo_crm_realestate_front_realestate', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'load_custom_wp_script_style' );
function load_custom_wp_script_style() {
    # JS
    
    # CSS    
    wp_register_style( 'meo_crm_realestate_frontend_css',  '/wp-content/plugins/meo-crm-realestate-management/css/meo-crm-realestate-frontend.css', false, '1.0.0' );
    wp_enqueue_style( 'meo_crm_realestate_frontend_css' );
}

# Add plugin menu in admin navigation
/*add_action( 'admin_menu', 'meo_crm_realestate_management_admin_menu' );
function meo_crm_realestate_management_admin_menu() {
        $page_title = 'Realestate';
        $menu_title = 'Realestate';
        $capability = 'manage_options';
        $menu_slug = 'manage_realestate';
        $function = 'page_admin_realestate';
        $icon_url = 'dashicons-admin-multisite';
        $position = 6;
        add_menu_page ( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}*/


# Add projects fields
add_action('projectsFormFields', 'meo_crm_realestate_management_projectsFormFields');
function meo_crm_realestate_management_projectsFormFields()
{
    include_once 'views/backend/meo-crm-realestate-management-form-project.php';
}

# Add projects fields Validation
add_filter('projectsFormValidate', 'meo_crm_realestate_projectsformvalidate', 2);	
function meo_crm_realestate_projectsformvalidate($projectID)
{        
    $MeoCrmCoreCryptData = new MeoCrmCoreCryptData($_POST['url_project']);     
    
    $datas = array();        
    $datas['project_id'] = $projectID;
    $datas['host_ftp'] = $MeoCrmCoreCryptData->encode($_POST['host_ftp']);
    $datas['login_ftp'] = $MeoCrmCoreCryptData->encode($_POST['login_ftp']);
    $datas['password_ftp'] = $MeoCrmCoreCryptData->encode($_POST['password_ftp']);  
    $datas['base_dir_ftp'] = $MeoCrmCoreCryptData->encode($_POST['base_dir_ftp']);
    $datas['host_db'] = $MeoCrmCoreCryptData->encode($_POST['host_db']);
    $datas['name_db'] = $MeoCrmCoreCryptData->encode($_POST['name_db']);
    $datas['login_db'] = $MeoCrmCoreCryptData->encode($_POST['login_db']);
    $datas['password_db'] = $MeoCrmCoreCryptData->encode($_POST['password_db']);
    $datas['updated_at'] = date('Y-m-d H:i:s');    
    
    if(!empty($_POST['access_project_id']))
    {
        $where = array('id' => $_POST['access_project_id']);
        $result = AccessProjectModel::updateAccessProject($datas,$where);
    }else{
        $datas['created_at'] = date('Y-m-d H:i:s');
        $result = AccessProjectModel::insertAccessProject($datas);
    }
        
    return $projectID;
}

// Add frontend template 
add_filter('meo_crm_core_templates_collector', 'meo_crm_realestate_templates_register', 1, 1);
function meo_crm_realestate_templates_register($pluginsTemplates){
    $pluginsTemplates[MEO_CRM_REALESTATE_MANAGE_LOT_TPL] = 'MEO CRM Manage List Lot';
    $pluginsTemplates[MEO_CRM_REALESTATE_MANAGE_TOOLPAGE] = 'MEO CRM Realestate Tool page';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_DEVELOPPEMENT] = 'MEO CRM Realestate List Developpement';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_DEVELOPPEMENT] = 'MEO CRM Realestate Form Developpement';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_SECTOR] = 'MEO CRM Realestate List Sector';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_SECTOR] = 'MEO CRM Realestate Form Sector';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_BUILDING] = 'MEO CRM Realestate List Building';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_BUILDING] = 'MEO CRM Realestate Form Building';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_LOT] = 'MEO CRM Realestate List Lot';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_LOT] = 'MEO CRM Realestate Form Lot';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_PLAN] = 'MEO CRM Realestate List Plan';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_PLAN] = 'MEO CRM Realestate Form Plan';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_STATUS] = 'MEO CRM Realestate List Status';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_STATUS] = 'MEO CRM Realestate Form Status';    
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_META] = 'MEO CRM Realestate List Meta';
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_META] = 'MEO CRM Realestate Form Meta';  
    $pluginsTemplates[MEO_CRM_REALESTATE_FORM_LIST_PRICE_PDF] = 'MEO CRM Realestate Form List Price PDF';
    $pluginsTemplates[MEO_CRM_REALESTATE_LIST_PLAN_FLOOR_LOT] = 'MEO CRM Realestate List Plan Floor Lot';
    $pluginsTemplates[MEO_CRM_REALESTATE_MANAGE_REQUEST_API] = 'MEO CRM Realestate Request API';
    return $pluginsTemplates;
}

// Add frontend page 
add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_realestate_pages_register' );		
function meo_crm_realestate_pages_register($pluginsPages) {
    # Create Page for Users Availability
    $pluginsPages[MEO_REALESTATE_MANAGEMENT_SLUG] = array(
        'post_title' => 'MEO CRM REALESTATE Management',
        'post_content' => 'Page to be used for manage the contact lot sale ',
        '_wp_page_template' => MEO_CRM_REALESTATE_MANAGE_LOT_TPL
    );
    $pluginsPages[MEO_CRM_REALESTATE_MANAGE_TOOLPAGE_SLUG] = array(
        'post_title' => 'Realestate Management Immo',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_MANAGE_TOOLPAGE
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_DEVELOPPEMENT_SLUG] = array(
        'post_title' => 'Realestate Management Developpement List',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_DEVELOPPEMENT
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_DEVELOPPEMENT_SLUG] = array(
        'post_title' => 'Realestate Management Developpement Form',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_DEVELOPPEMENT
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_SECTOR_SLUG] = array(
        'post_title' => 'Realestate Management Secteur List',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_SECTOR
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_SECTOR_SLUG] = array(
        'post_title' => 'Realestate Management Secteur Form',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_SECTOR
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_BUILDING_SLUG] = array(
        'post_title' => 'Realestate Management Building List',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_BUILDING
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_BUILDING_SLUG] = array(
        'post_title' => 'Realestate Management Building Form',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_BUILDING
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_LOT_SLUG] = array(
        'post_title' => 'Realestate Management Lot List',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_LOT
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_LOT_SLUG] = array(
        'post_title' => 'Realestate Management Lot Form',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_LOT
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_PLAN_SLUG] = array(
        'post_title' => 'Realestate Management Plan List',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_PLAN
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_PLAN_SLUG] = array(
        'post_title' => 'Realestate Management Plan Form',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_PLAN
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_STATUS_SLUG] = array(
        'post_title' => 'Realestate Management Status List',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_STATUS
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_STATUS_SLUG] = array(
        'post_title' => 'Realestate Management Status Form',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_STATUS
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_META_SLUG] = array(
        'post_title' => 'Realestate Management Immo',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_META
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_META_SLUG] = array(
        'post_title' => 'Realestate Management Immo',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_META
    );    
    $pluginsPages[MEO_CRM_REALESTATE_LIST_PLAN_FLOOR_LOT_SLUG] = array(
        'post_title' => 'Realestate Management Immo',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_LIST_PLAN_FLOOR_LOT
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_PLAN_FLOOR_LOT_SLUG] = array(
        'post_title' => 'Realestate Management Immo',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_PLAN_FLOOR_LOT
    );
    $pluginsPages[MEO_CRM_REALESTATE_FORM_LIST_PRICE_PDF_SLUG] = array(
        'post_title' => 'Realestate Management Immo',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_FORM_LIST_PRICE_PDF
    );
    $pluginsPages[MEO_REALESTATE_MANAGEMENT_REQUEST_API_SLUG] = array(
        'post_title' => 'Realestate Management Immo API',
        'post_content' => '',
        '_wp_page_template' => MEO_CRM_REALESTATE_MANAGE_REQUEST_API
    );
    return $pluginsPages;
}

#Frist page menu meo-crm-realestate management
function page_admin_realestate()
{
    include_once 'views/backend/meo-crm-realestate-base-page.php';
}


function meo_crm_realestate_get_development_data($project,$building,$lot) {
	
	if(is_array($project) && array_key_exists('url', $project) && array_key_exists('api_uri', $project)){
		$development_url = $project['url'] . $project['api_uri'] . '?action=get_development&building='.$building.'&lot='.$lot.'&api_key=' . $project['api_key'];
		$development_json = @file_get_contents($development_url);
		$development_data = ($development_json === false ? array() : json_decode($development_json));
		
		return $development_data;
	}
	return false;
}

function getExternalPrefix()
{
    return PREFIX_TABLE;
}

add_filter('meocrmprojects_getProjectData', 'meo_crm_realestate_external_access', 10, 1);
function meo_crm_realestate_external_access($project)
{
    if($project)
    {
        $dbAccess = array();
        $ftpAccess = array();
        $returnProject = array(); //['realestate']['ftp']

        $access = AccessProjectModel::selectAccessProjectByProjectId($project['id']);

        $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

        $dbAccess['host'] = (isset($access->host_db) && !empty($access->host_db)) ? $meoCrmCoreCryptData->decode($access->host_db) : '';
        $dbAccess['name'] = (isset($access->host_db) && !empty($access->name_db)) ? $meoCrmCoreCryptData->decode($access->name_db) : '';
        $dbAccess['login'] = (isset($access->host_db) && !empty($access->login_db)) ? $meoCrmCoreCryptData->decode($access->login_db) : '';
        $dbAccess['password'] = (isset($access->host_db) && !empty($access->password_db)) ? $meoCrmCoreCryptData->decode($access->password_db) : '';

        $ftpAccess['host'] = (isset($access->host_ftp) && !empty($access->host_ftp)) ? $meoCrmCoreCryptData->decode($access->host_ftp) : '';
        $ftpAccess['base_dir'] = (isset($access->base_dir_ftp) && !empty($access->base_dir_ftp)) ? $meoCrmCoreCryptData->decode($access->base_dir_ftp) : '';
        $ftpAccess['login'] = (isset($access->login_ftp) && !empty($access->login_ftp)) ? $meoCrmCoreCryptData->decode($access->login_ftp) : '';
        $ftpAccess['password'] = (isset($access->password_ftp) && !empty($access->password_ftp)) ? $meoCrmCoreCryptData->decode($access->password_ftp) : '';

        $returnProject['realestate']['db'] = $dbAccess;
        $returnProject['realestate']['ftp'] = $ftpAccess;

        $returnProject = array_merge($project, $returnProject);

    }
    else $returnProject = $project;

    return $returnProject;
}

add_action('meo_crm_realestate_create_price_list','createPriceListPdf');
function createPriceListPdf($project_id)
{
    $project = meo_crm_projects_getProjectInfo($project_id);

    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

    $ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'],$project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

    $pdf_parameter = MeoCrmRealestateManagement::getPriceListPdfParam($project_id,$external_wpdb);

    $lots = MeoCrmRealestateManagement::getAllLotsForPriceList($external_wpdb);

    $status = StatusModel::getAllStatus($external_wpdb);

    $pdf = new MeoCrmRealestateManagementPdfPriceList($pdf_parameter,$lots,$status,$project_id);
    $pdf_path = $pdf->generatePdfPriceList();

    if(!empty($pdf_path)) {
        $query = MeoCrmRealestateManagement::getPostAttachmentPriceList();
        $post_price_list = MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);

        if (empty($post_price_list))
        {
            $dataPost = array();
            $checkUpload = true;
            $created_date = date('Y-m-d H:m:i');

            $dataPost['post_author'] = 1;
            $dataPost['post_date'] = $created_date;
            $dataPost['post_date_gmt'] = $created_date;
            $dataPost['post_content'] = '';
            $dataPost['post_title'] = 'Liste de prix';
            $dataPost['post_excerpt'] = '';
            $dataPost['comment_status'] = 'closed';
            $dataPost['ping_status'] = 'closed';
            $dataPost['post_password'] = '';
            $dataPost['post_name'] = 'price-list';
            $dataPost['to_ping'] = '';
            $dataPost['pinged'] = '';
            $dataPost['post_modified'] = $created_date;
            $dataPost['post_modified_gmt'] = $created_date;
            $dataPost['post_content_filtered'] = '';
            $dataPost['post_parent'] = 0;
            $dataPost['guid'] = $pdf_path;
            $dataPost['menu_order'] = 0;
            $dataPost['post_type'] = 'attachment';
            $dataPost['post_mime_type'] = 'application/pdf';
            $dataPost['comment_count'] = 0;

            $result = MeoCrmCoreExternalDbQuery::insert($external_wpdb, getExternalPrefix() . MEO_CRM_REALESTATE_POSTS, $dataPost);

            if ($result['success']) {
                $path_after_upload = explode('/pdfs/', $pdf_path);
                $postMetas = array(
                    '_wp_attached_file' => 'project/' . $path_after_upload[1],
                    '_edit_lock' => '',
                    '_edit_last' => '',
                    '_price_enabler' => '1'
                );
                foreach ($postMetas as $meta_key => $meta_value) {
                    $dataPostMeta = array();
                    $dataPostMeta['post_id'] = $result['id'];
                    $dataPostMeta['meta_key'] = $meta_key;
                    $dataPostMeta['meta_value'] = $meta_value;

                    $resultMeta = MeoCrmCoreExternalDbQuery::insert($external_wpdb, getExternalPrefix() . MEO_CRM_REALESTATE_POSTMETA, $dataPostMeta);
                    if (!$resultMeta['success']) {
                        $checkUpload = false;
                    }
                }
            } else {
                $checkUpload = false;
            }
            if ($checkUpload) {
                $external_dir_path = $project['realestate']['ftp']['base_dir'] . 'uploads/';
                $external_pdf_path = $project['realestate']['ftp']['base_dir'] . 'uploads/project/' . $path_after_upload[1];

                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $external_dir_path, 'project'))
                {
                    $folder_created = true;
                }

                if (ftp_put($ftp, $external_pdf_path, $pdf_path, FTP_BINARY) === true) {
                    return true;
                }
            }
        } else {
            $path_after_upload = explode('/pdfs/', $pdf_path);

            $external_dir_path = $project['realestate']['ftp']['base_dir'] . 'uploads/';
            $external_pdf_path = $project['realestate']['ftp']['base_dir'] . 'uploads/project/' . $path_after_upload[1];

            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $external_dir_path, 'project'))
            {
                $folder_created = true;
            }

            if (ftp_put($ftp, $external_pdf_path, $pdf_path, FTP_BINARY) === true) {
                return true;
            } else {
                return false;
            }
        }
    }else{
        return false;
    }
}


