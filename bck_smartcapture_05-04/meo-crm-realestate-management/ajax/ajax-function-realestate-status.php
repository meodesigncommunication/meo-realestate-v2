<?php 

/*
 *  Show Status List
 */
add_action( 'wp_ajax_nopriv_getStatusList', 'getStatusList' );  
add_action( 'wp_ajax_getStatusList', 'getStatusList' );
function getStatusList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = StatusModel::getAllStatus($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['name'] = $result->name;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'name', 'name' => 'name', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editStatus(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteStatus(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

/*
 *  Show Status by id
 */
add_action( 'wp_ajax_nopriv_getStatus', 'getStatus' );  
add_action( 'wp_ajax_getStatus', 'getStatus' );
function getStatus()
{
    // INIT VARIABLE    
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = StatusModel::getStatusById($external_wpdb,$id);
    
    echo json_encode($results);
    
    die();
}

/*
 *  Create and update Status
 */
add_action( 'wp_ajax_nopriv_saveStatus', 'saveStatus' );  
add_action( 'wp_ajax_saveStatus', 'saveStatus' );
function saveStatus()
{
    // Init From Value
    $id = $_POST['id'];
    $name = $_POST['status_name'];
    $color = $_POST['status_color'];
    $project_id = $_POST['project_id'];
    
    // Init variable
    $check = true;
    $datas = array();
    $errors = array();  
    
    // DB Request
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    if(isset($id) && !empty($id))
    {
        // Update current status
        $datas['name'] = $name;
        $datas['color'] = $color;
        $where = array('id' => $id);
        $result = StatusModel::updateStatus($external_wpdb, $datas, $where);
        
        if($result['success'])
        {
            $check = true;
            $message = $helperCore->getFlashMessageAdmin('Status update success', 1);
        }else{
            $check = false;
            $message = $helperCore->getFlashMessageAdmin('Status update error', 0);
        }
    }else{
        // Insert new status
        $datas['name'] = $name;
        $datas['color'] = $color;
        $result = StatusModel::insertStatus($external_wpdb, $datas);
        
        if($result['success'])
        {
            $check = true;
            $message = $helperCore->getFlashMessageAdmin('Status create success', 1);
            $id = $result['id'];
        }else{
            $check = false;
            $message = $helperCore->getFlashMessageAdmin('Status create error', 0);
            $id = 0;
        }
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
    );
    
    echo json_encode($result);
    
    die();
}

// Get Sector entry by id
add_action( 'wp_ajax_nopriv_getStatusById', 'getStatusById' );  
add_action( 'wp_ajax_getStatusById', 'getStatusById' );
function getStatusById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $status = StatusModel::getStatusById($external_wpdb,$id);
    
    echo json_encode($status);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deleteStatus', 'deleteStatus' );  
add_action( 'wp_ajax_deleteStatus', 'deleteStatus' );
function deleteStatus()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Delete Status by id
    $where = array( 'id' => $id );    
    $result = StatusModel::deleteStatus($external_wpdb,$where);
    
    // Check if all steps are OK for message return 
    if($result['success'])
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Status delete success', 1);
        
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Status delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message
    );
    
    echo json_encode($result);
    
    die();
}