<?php 

/*
 *  Show Lot List
 */
add_action( 'wp_ajax_nopriv_getLotList', 'getLotList' );  
add_action( 'wp_ajax_getLotList', 'getLotList' );
function getLotList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = LotModel::getAllLot($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['title'] = $result->title;
        $datas[$result->id]['description'] = $result->description;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'title', 'name' => 'title', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'description', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editLot(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteLot(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

/*
 *  Insert or Update Lot
 */
add_action( 'wp_ajax_nopriv_saveLot', 'saveLot' );  
add_action( 'wp_ajax_saveLot', 'saveLot' );
function saveLot()
{
    
   // print_r($_POST); print_r($_FILES); exit();
    
    // Init Variables
    $check = true;
    $errors = array();
    $files = array();
    $datas = array();
    $project_id = $_POST['project_id'];
    
    // Data For Table wp_meo_crm_realestate_lots
    $lot_id = $_POST['lot_id'];
    $plan_lot_id = $_POST['plan_lot_id'];
    $status_id = $_POST['status_id'];
    $type_lot = $_POST['type_lot'];
    $lot_title = $_POST['lot_title'];
    $lot_description = $_POST['lot_description'];
    $lot_room = $_POST['lot_room'];
    $lot_surface = $_POST['lot_surface'];
    $lot_price = $_POST['lot_price'];
    $metas = $_POST['meta'];
    
    // Data For Table wp_meo_crm_realestate_lot_meta_value
    $metas = $_POST['meta'];
    
    // Data For Table Manage Status Floor Plan
    $plan_lot_building_id = $_POST['coordinate_plan_lot_building_id'];
    $plan_lot_building_title = $_POST['coordinate_plan_lot_building_title'];
    $plan_lot_building_coordinate = $_POST['coordinate_plan_lot_building_coordinate'];
    
    // Data For Table wp_meo_crm_floor_lot
    $floor_id = (isset($_POST['floor_id'])) ? $_POST['floor_id'] : 0;
    $floor_entry = $_POST['floor_entry'];
    
    // Get All File
    $image_lot_floor_plan = $_FILES['input_lot_floor_plan'];
    $plan_image_floor = $_FILES['plan_image_floor'];
    $plan_image_floor_hover = $_FILES['plan_image_floor_hover'];
    $plan_image_floor_back = $_FILES['plan_image_floor_back'];
    $plan_image_floor_back_hover = $_FILES['plan_image_floor_back_hover'];
    
    // Request Database
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    //Init Class
    $helperCore = new MeoCrmCoreHelper();
    $helperList = new MeoCrmCoreListHelper();
    $validation = new MeoCrmCoreValidationForm();    
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init Variable For Connect To External DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Connect To The External DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Base URL for upload images
    $upload_path = $baseDirFTP.'uploads/';
    $upload_url = $project['url'].'wp-content/uploads/';    
    
    $base_path_typo_status_floor = $baseDirFTP.'uploads/typo-status-floor-lot/';
    $base_url_typo_status_floor = 'typo-status-floor-lot/';
    
    $base_path_floor_lot = $baseDirFTP.'uploads/floor-lot/';
    $base_url_floor_lot = 'floor-lot/';
    
    // Create folder necessary for upload lot images
    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path, 'typo-status-floor-lot'))
    {
            $folder_typo_created = true;
    }    
    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path, 'floor-lot'))
    {
            $folder_lot_created = true;
    }      
    
    // Traitement des donnees recus
    if(isset($lot_id) && !empty($lot_id))
    {
        // FORM VALIDATION
        $rules = array(
            'plan_lot_id' => 'required|numeric',
            'status_id' => 'required|numeric',
            'type_lot' => 'required|text',
            'lot_title' => 'required|min:5|max:100',
            'lot_description' => 'required|text',
            'lot_room' => 'required|float',
            'lot_surface' => 'required|numeric',
            'lot_price' => 'required|numeric',
            'meta' => 'required|array'
        );
        
        $results_validation = $validation->validationDatas($rules, $_POST);        
        $results = $validation->checkResultValidation($results_validation);
        
        if($results['result']) 
        {     
            // BASE VALUES
            $success = array();
            $datas = array();    
            $success[] = 'Validation data is correct';
            $datas['plan_id'] = $_POST['plan_lot_id'];
            $datas['status_id'] = $_POST['status_id'];
            $datas['type_lot'] = $_POST['type_lot'];
            $datas['title'] = $_POST['lot_title'];
            $datas['description'] = $_POST['lot_description'];
            $datas['rooms'] = $_POST['lot_room'];
            $datas['surface'] = $_POST['lot_surface'];
            $datas['price'] = $_POST['lot_price'];
            
            $where = array('id' => $lot_id);
            $result = LotModel::updateLot($external_wpdb, $datas, $where);
            
            if($result['success'])
            {
                $success[] = 'Update table lot success';
                // META VALUES
                $metas = $_POST['meta'];                
                foreach($metas as $key => $value)
                {
                    $datas = array();
                    $datas['value'] = $value;
                    $where = array('lot_id' => $lot_id,'meta_id' => $key);
                    $result = MetaLotModel::updateMetaValueLot($external_wpdb, $datas, $where);
                    if(!$result['success'])
                    {
                        $check = false;
                        $errors[] = 'Lot meta not updated meta_value = '.$value;
                    }else{
                        $success[] = 'Update table lot meta success';
                    }                        
                }
                
                // Floor Lot
                if($check)
                {
                    // Init Value
                    $floor_ids = $_POST['floor_id_list'];
                    $floor_entry = $_POST['floor_entry'][0];
                    
                    foreach($floor_ids as $key => $floor_id)
                    {
                        if($key > 0)
                        {
                            // DATA FOR FLOORS
                            $check_upload = true;
                            $floor_datas = array();  
                            $floor_lot_id = $_POST['floor_lot_id'][$key];     
                            $floor_datas['floor_id'] = $_POST['floor_id_list'][$key];
                            $floor_datas['lot_entry'] = ($key == $floor_entry) ? 1 : 0;                             
                            $plan_floor_building_list = $_POST['plan_floor_building_list'][$key];
                            
                            if(isset($_FILES['input_lot_floor_plan']['name'][$key]) && !empty($_FILES['input_lot_floor_plan']['name'][$key]))
                            {
                                $floor_datas['plan_floor'] = $base_url_floor_lot.$lot_id.'/'.$_FILES['input_lot_floor_plan']['name'][$key];
                                $floor_plan_path = $base_path_floor_lot.$lot_id.'/'.$_FILES['input_lot_floor_plan']['name'][$key];
                                
                                $type_file = $_FILES['input_lot_floor_plan']['type'][$key];
                                if( !is_uploaded_file($_FILES['input_lot_floor_plan']['tmp_name'][$key]) )
                                {
                                    $check_upload = false;
                                    $errors[] = 'Upload image plan floor error (tmp file not exists)';
                                }                                    
                                if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                {
                                    $check_upload = false;
                                    $errors[] = 'Upload image plan floor error (type file error)';
                                }
                                if( !ftp_put($ftp, $floor_plan_path, $_FILES['input_lot_floor_plan']['tmp_name'][$key], FTP_BINARY) === false )
                                {
                                    $check_upload = false;
                                    $errors[] = 'Upload image plan floor error (upload error)';
                                }
                                
                                if($check_upload)
                                {
                                    $success[] = 'Upload image plan floor is success';
                                    if(isset($_POST['old_image_lot_floor_plan'][$key]) && !empty($_POST['old_image_lot_floor_plan'][$key]))
                                    {
                                        list($url,$filename) = explode('/'.$lot_id.'/', $_POST['old_image_lot_floor_plan'][$key]);
                                        $path_file = $base_path_floor_lot.$lot_id.'/'.$filename;
                                        if(!ftp_delete($ftp, $path_file))
                                        {
                                            $check = false;
                                            $errors[] = 'File: '.$path_file.' can not deleted';
                                        }else{
                                            $success[] = 'Delete old image plan floor is success';
                                        }
                                    }
                                }
                                
                            }
                            
                            // Check if plan floor lot is a new or exist
                            if($plan_floor_building_list > 0)
                            {
                                $floor_datas['status_plan_floor_id'] = $plan_floor_building_list;                                
                            }else{
                                if(isset($_POST['coordinate_plan_lot_building_title'][$key]) && !empty($_POST['coordinate_plan_lot_building_title'][$key]))
                                {
                                    //wp_meo_crm_realestate_coordinates_plan_lot_building
                                    $datas = array();
                                    $datas['title'] = $_POST['coordinate_plan_lot_building_title'][$key];
                                    $datas['coordinates'] = $_POST['coordinate_plan_lot_building_coordinate'][$key];
                                    $datas['coordinates_back'] = $_POST['coordinate_plan_lot_building_coordinate_back'][$key]; 

                                    $result = BuildingPlanFloorModel::insertCoordinateFloorPlan($external_wpdb, $datas);
                                    $coordinates_id = $result['id'];

                                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $base_path_typo_status_floor, $coordinates_id))
                                    {
                                            $folder_lot_created = true;
                                    }

                                    $uplaod_base_folder_typo = $base_path_typo_status_floor.$coordinates_id.'/';

                                    //wp_meo_crm_realestate_plan_lot_building                        
                                    foreach($_FILES['plan_image_floor']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 0;  
                                        $datas['front_face'] = 1;   

                                        // Insert data in DB
                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);

                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        // Check if uploaded
                                        if(isset($_FILES['plan_image_floor']['name'][$status][$key]) && !empty($_FILES['plan_image_floor']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }
                                    }
                                    foreach($_FILES['plan_image_floor_hover']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 1;  
                                        $datas['front_face'] = 1;

                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);

                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        // Check if uploaded
                                        if(isset($_FILES['plan_image_floor_hover']['name'][$status][$key]) && !empty($_FILES['plan_image_floor_hover']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor_hover']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor_hover']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor_hover']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }
                                    }
                                    foreach($_FILES['plan_image_floor_back']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 0;  
                                        $datas['front_face'] = 0;

                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);
                                        
                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        // on vérifie maintenant l'extension
                                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                        {
                                            $check = false;
                                        }

                                        // Check if uploaded
                                        if(isset($_FILES['plan_image_floor_back']['name'][$status][$key]) && !empty($_FILES['plan_image_floor_back']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor_back']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor_back']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor_back']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }

                                    }
                                    foreach($_FILES['plan_image_floor_back_hover']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 1;  
                                        $datas['front_face'] = 0;
                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);

                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        if(isset($_FILES['plan_image_floor_back_hover']['name'][$status][$key]) && !empty($_FILES['plan_image_floor_back_hover']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor_back_hover']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor_back_hover']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor_back_hover']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }
                                    }    

                                    foreach($ids_image_plan as $value)
                                    {
                                        $result = BuildingPlanFloorModel::insertLinkFloorPlan($external_wpdb, $value);                                    
                                        if(!$result['success'])
                                        {
                                            $check = false;
                                            $errors[] = '- error: insert link plan floor lot ( SQL error = '.$external_wpdb->last_error.' )';
                                        }
                                    }
                                }
                                $floor_datas['status_plan_floor_id'] = $coordinates_id;
                            }  
                            // Delete all floor for this lot ($lot_id)
                            $where = array('lot_id' => $lot_id);
                            $result = FloorLotModel::deleteFloorLot($external_wpdb,$where);
                            if($result['success'])
                            {
                                // If floor_id exist go to update else create new                            
                                $floor_datas['lot_id'] = $lot_id;
                                $result = FloorLotModel::insertFloorLot($external_wpdb, $floor_datas);
                                if($result['success'])
                                {
                                    $success[] = 'Insert Floor Lot Success';
                                }else{
                                    $check = false;
                                    $errors[] = '- error: insert floor lot ( SQL error = '.$external_wpdb->last_error.' )';
                                }
                            }
                            
                        }
                    }
                    
                    
                }                 
            }else{
                $check = false;
                $errors[] = '- error: update lot ( SQL error = '.$external_wpdb->last_error.' )';
            }
        }
        
        if($check)
        {
            $content_message = 'Lot success update proccess<br/>';
            foreach($success as $step_success)
            {
                $content_message .= $step_success; 
                $content_message .= '<br/>'; 
            }
            $message = $helperCore->getFlashMessageAdmin($content_message, 1);
        }else{
            $content_message = 'Lot error update proccess<br/>';
            foreach($errors as $error)
            {
                $content_message .= $error; 
                $content_message .= '<br/>'; 
            }
            $message = $helperCore->getFlashMessageAdmin($content_message, 0);
        }
        
    }else{        
        // FORM VALIDATION
        $rules = array(
            'plan_lot_id' => 'required|numeric',
            'status_id' => 'required|numeric',
            'type_lot' => 'required|text',
            'lot_title' => 'required|min:5|max:100',
            'lot_description' => 'required|text',
            'lot_room' => 'required|float',
            'lot_surface' => 'required|numeric',
            'lot_price' => 'required|numeric',
            'meta' => 'required|array'
        );        
        $results_validation = $validation->validationDatas($rules, $_POST);        
        $results = $validation->checkResultValidation($results_validation);        
        if($results['result']) 
        {            
            // DATAS FOR SAVE A LOT
            $datas = array();
            $ids_image_plan = array();
            $datas['status_id'] = $_POST['status_id'];
            $datas['plan_id'] = $_POST['plan_lot_id'];
            $datas['title'] = $_POST['lot_title'];
            $datas['description'] = $_POST['lot_description'];
            $datas['rooms'] = $_POST['lot_room'];
            $datas['surface'] = $_POST['lot_surface'];
            $datas['price'] = $_POST['lot_price'];
            $datas['type_lot'] = $_POST['type_lot'];
            $result = LotModel::insertLot($external_wpdb, $datas);
            $lot_id = $result['id'];
            // Method for save a lot            
            
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $base_path_floor_lot, $lot_id)){
                $folder_created = true;
            }  
            
            // Path for upload  in folder 
            $upload_path_lot_floor = $base_path_floor_lot.$lot_id.'/';
            
            if($result['success'])
            {                
                foreach($_POST['meta'] as $key => $meta)
                {
                    $datas = array();
                    $datas['lot_id'] = $lot_id;
                    $datas['meta_id'] = $key;
                    $datas['value'] = $meta;
                    $result = MetaLotModel::insertMetaValueLot($external_wpdb,$datas);              
                }
                
                if($result['success'])
                {
                    foreach($_POST['coordinate_plan_lot_building_title'] as $key => $coordinate_plan_lot_building_title)
                    {
                        if($key != 0)
                        {
                            $ids_image_plan = array();
                            if(isset($_POST['plan_floor_building_list'][$key]) && $_POST['plan_floor_building_list'][$key] != -1)
                            {
                                $status_plan_floor_id = $_POST['plan_floor_building_list'][$key];
                            }else{
                                if(isset($_POST['coordinate_plan_lot_building_title'][$key]) && !empty($_POST['coordinate_plan_lot_building_title'][$key]))
                                {
                                    //wp_meo_crm_realestate_coordinates_plan_lot_building
                                    $datas = array();
                                    $datas['title'] = $_POST['coordinate_plan_lot_building_title'][$key];
                                    $datas['coordinates'] = $_POST['coordinate_plan_lot_building_coordinate'][$key];
                                    $datas['coordinates_back'] = $_POST['coordinate_plan_lot_building_coordinate_back'][$key]; 

                                    $result = BuildingPlanFloorModel::insertCoordinateFloorPlan($external_wpdb, $datas);
                                    $coordinates_id = $result['id'];

                                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $base_path_typo_status_floor, $coordinates_id))
                                    {
                                            $folder_lot_created = true;
                                    }

                                    $uplaod_base_folder_typo = $base_path_typo_status_floor.$coordinates_id.'/';

                                    //wp_meo_crm_realestate_plan_lot_building                        
                                    foreach($_FILES['plan_image_floor']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 0;  
                                        $datas['front_face'] = 1;   

                                        // Insert data in DB
                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);

                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        // Check if uploaded
                                        if(isset($_FILES['plan_image_floor']['name'][$status][$key]) && !empty($_FILES['plan_image_floor']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }
                                    }
                                    foreach($_FILES['plan_image_floor_hover']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 1;  
                                        $datas['front_face'] = 1;

                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);

                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        // Check if uploaded
                                        if(isset($_FILES['plan_image_floor_hover']['name'][$status][$key]) && !empty($_FILES['plan_image_floor_hover']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor_hover']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor_hover']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor_hover']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }
                                    }
                                    foreach($_FILES['plan_image_floor_back']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 0;  
                                        $datas['front_face'] = 0;

                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);
                                        
                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );
                                        
                                        // on vérifie maintenant l'extension
                                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                        {
                                            $check = false;
                                        }

                                        // Check if uploaded
                                        if(isset($_FILES['plan_image_floor_back']['name'][$status][$key]) && !empty($_FILES['plan_image_floor_back']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor_back']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor_back']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor_back']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }

                                    }
                                    foreach($_FILES['plan_image_floor_back_hover']['name'] as $status => $image)
                                    {
                                        $datas = array();
                                        $datas['status_id'] = $status;
                                        $datas['image'] = $base_url_typo_status_floor.$coordinates_id.'/'.$image[$key];
                                        $datas['hover'] = 1;  
                                        $datas['front_face'] = 0;
                                        $result = BuildingPlanFloorModel::insertImageFloorPlan($external_wpdb, $datas);

                                        // Create array for save link image coordinate
                                        $ids_image_plan[] = array(
                                            'coordinates_id' => $coordinates_id,
                                            'plan_lot_id' => ($result['success']) ? $result['id'] :  '0'
                                        );

                                        if(isset($_FILES['plan_image_floor_back_hover']['name'][$status][$key]) && !empty($_FILES['plan_image_floor_back_hover']['name'][$status][$key]))
                                        {
                                            $type_file = $_FILES['plan_image_floor_back_hover']['type'][$status][$key];
                                            if( !is_uploaded_file($_FILES['plan_image_floor_back_hover']['tmp_name'][$status][$key]) )
                                            {
                                                $check_upload = false;
                                            }                                    
                                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                            {
                                                $check_upload = false;
                                            }
                                            if( !ftp_put($ftp, $uplaod_base_folder_typo.$image[$key], $_FILES['plan_image_floor_back_hover']['tmp_name'][$status][$key], FTP_BINARY) === false )
                                            {
                                                $check_upload = false;
                                            }
                                        }
                                    }    

                                    foreach($ids_image_plan as $value)
                                    {
                                        $result = BuildingPlanFloorModel::insertLinkFloorPlan($external_wpdb, $value);                                    
                                        if(!$result['success'])
                                        {
                                            $check = false;
                                            $errors[] = '- error: insert link plan floor lot';
                                        }
                                    }
                                }
                            }

                            $datas = array();
                            $datas['floor_id'] = $_POST['floor_id_list'][$key];
                            $datas['lot_id'] = $lot_id;
                            $datas['status_plan_floor_id'] = (!isset($status_plan_floor_id) || empty($status_plan_floor_id)) ? $coordinates_id : $status_plan_floor_id;
                            $datas['lot_entry'] = ($_POST['floor_entry'][0] == $key) ? '1' : '' ;
                            $datas['plan_floor'] = $base_url_floor_lot.$lot_id.'/'.$_FILES['input_lot_floor_plan']['name'][$key];

                            $result = FloorLotModel::insertFloorLot($external_wpdb, $datas);

                            // Check if uploaded
                            if(isset($_FILES['input_lot_floor_plan']['name'][$key]) && !empty($_FILES['input_lot_floor_plan']['name'][$key]))
                            {
                            $type_file = $_FILES['input_lot_floor_plan']['type'][$key];
                                if( !is_uploaded_file($_FILES['input_lot_floor_plan']['tmp_name'][$key]) )
                                {
                                    $check_upload = false;
                                }                                    
                                if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                                {
                                    $check_upload = false;
                                }
                                if( !ftp_put($ftp, $upload_path_lot_floor.$_FILES['input_lot_floor_plan']['name'][$key], $_FILES['input_lot_floor_plan']['tmp_name'][$key], FTP_BINARY) === false )
                                {
                                    $check_upload = false;
                                }
                            }

                            if(!$result['success'])
                            {
                                $check = false;
                                $query = $external_wpdb->last_query;
                                $txt = $external_wpdb->last_error;
                                $errors[] = '- error: insert lot floor: request = '.$query.' | error = '.$txt;
                            }                        
                        } 
                    }
                }else{
                    $check = false;
                    $errors[] = '- error: insert lot meta';
                }
            }else{
                $check = false;
                $errors[] = '- error: insert lot';
            }
            
            // If data is insert with success in DB 
            if($check)
            {
                $message = $helperCore->getFlashMessageAdmin('Lot save success', 1);
            }else{
                foreach($errors as $error)
                {
                    $content_message .= $error;
                }
                $message = $helperCore->getFlashMessageAdmin($content_message, 0);
            }
            
        }else{
            foreach($errors as $error)
            {
                $content_message .= $error;
            }
            $message = $helperCore->getFlashMessageAdmin($content_message, 0);
        }  
    }
    
    // Array to return 
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $lot_id
    );
    
    // Array Json Encoded
    echo json_encode($result);
    
    die();
}

/*
 *  Get All data to need a lot form
 */
add_action( 'wp_ajax_nopriv_getLotFormData', 'getLotFormData' );  
add_action( 'wp_ajax_getLotFormData', 'getLotFormData' );
function getLotFormData()
{
    // INIT VARIABLE
    $html = '';    
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $buildings = BuildingModel::getAllBuilding($external_wpdb);
    $plans = PlanModel::getAllPlan($external_wpdb);
    $metas = MetaLotModel::getAllMetaLot($external_wpdb);
    $list_status = StatusModel::getAllStatus($external_wpdb);
    $floors = FloorModel::getAllFloor($external_wpdb);
    
    // Create list plans for select form
    $html_plan = '<option value="-1">Choose a plan</option>';
    foreach ($plans as $plan)
    {
        $html_plan .= '<option value="'.$plan->id.'">'.$plan->title.'</option>';
    }  
    
    // Create list status for select form
    $html_status = '<option value="-1">Choose a status</option>';
    foreach ($list_status as $status)
    {
        $html_status .= '<option value="'.$status->id.'">'.$status->name.'</option>';
    } 
    
    // Create list floors for select form
    $html_floors = '<option value="-1">Choose a floor</option>';
    foreach ($floors as $floor)
    {
        $batiment = '';        
        foreach($buildings as $building)
        {
            if($floor->building_id == $building->id)
            {
                $batiment = $building->title;
            }
        }
        $html_floors .= '<option value="'.$floor->id.'">'.$batiment.' - '.$floor->title.'</option>';
    }  
    
    // Create list meta lot for form
    $html_meta = '';
    foreach ($metas as $meta)
    {
        $html_meta .= '<tr  class="form-field">';
        $html_meta .= '<th style="width: 15% !important"><label for="'.$meta->meta_slug.'">'.$meta->meta_key.'</label></th>';
        switch($meta->meta_type)
        {
            case 'text':
                $html_meta .= '<td><input id="meta_'.$meta->id.'" type="text" name="meta['.$meta->id.']" value=""></td>';
                break;            
            case 'checkbox':
                $html_meta .= '<td><input id="meta_'.$meta->id.'" type="checkbox" name="meta['.$meta->id.']" value="1"></td>';
                break;
            default:
                $html_meta .= '<td><input id="meta_'.$meta->id.'" type="text" name="meta['.$meta->id.']" value=""></td>';
                break;  
        }        
        $html_meta .= '</tr>';
    } 
    
    
    $results = array(
        'plan' => $html_plan,
        'meta' => $html_meta,
        'status' => $html_status,
        'floor' => $html_floors
    );
    
    echo json_encode($results);
    
    die();
}

/*
 *  Get All data to need a lot form
 */
add_action( 'wp_ajax_nopriv_getFormImagePlanFloor', 'getFormImagePlanFloor' );  
add_action( 'wp_ajax_getFormImagePlanFloor', 'getFormImagePlanFloor' );
function getFormImagePlanFloor()
{
    // INIT VARIABLE
    $html = '<td colspan="4"><table>'; 
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $list_status = StatusModel::getAllStatus($external_wpdb);
    $list_plan_floor = BuildingPlanFloorModel::getAllBuildingFloorPlan($external_wpdb);
    
    if(is_array($list_plan_floor) && !empty($list_plan_floor))
    {
        $html .= '<select class="plan_floor_building_list" onchange="hideTableFloorPlan(this)" name="plan_floor_building_list[]">';
        $html .=    '<option value="-1">Choose a plan floor</option>';
        foreach($list_plan_floor as $plan_floor)
        {
            $html .= '<option value="'.$plan_floor->id.'">'.$plan_floor->title.'</option>';
        }
        $html .= '</select>';
    }
    
    $html .= '<input type="hidden" name="coordinate_plan_lot_building_id" id="coordinate_plan_lot_building_id" value="" />';
    $html .= '<tr  class="form-field">';
    $html .= '  <th colspan="2" style="width: 15% !important">';
    $html .=        '<label for="coordinate_plan_lot_building_title">';
    $html .=            'Titre plan étage';
    $html .=        '</label>';
    $html .=    '</th>';
    $html .=    '<td colspan="3">';
    $html .=        '<input type="text" name="coordinate_plan_lot_building_title[]" id="coordinate_plan_lot_building_title" value="" />';
    $html .=    '</td>';
    $html .= '</tr>';
    $html .= '<tr  class="form-field">';
    $html .= '  <th colspan="2" style="width: 15% !important">';
    $html .=        '<label for="coordinate_plan_lot_building_coordinate">';
    $html .=            'Coordonnée plan par étage';
    $html .=        '</label>';
    $html .=    '</th>';
    $html .=    '<td colspan="3">';
    $html .=        '<input type="text" name="coordinate_plan_lot_building_coordinate[]" id="coordinate_plan_lot_building_coordinate" value="" />';
    $html .=    '</td>';
    $html .= '</tr>';    
    $html .= '<tr  class="form-field">';
    $html .= '  <th colspan="2" style="width: 15% !important">';
    $html .=        '<label for="coordinate_plan_lot_building_coordinate">';
    $html .=            'Coordonnée plan par étage back';
    $html .=        '</label>';
    $html .=    '</th>';
    $html .=    '<td colspan="3">';
    $html .=        '<input type="text" name="coordinate_plan_lot_building_coordinate_back[]" id="coordinate_plan_lot_building_coordinate_back" value="" />';
    $html .=    '</td>';
    $html .= '</tr>'; 
    $html .= '<tr>';
    $html .=    '<th>Status</th>';
    $html .=    '<th>Plan étage</th>';
    $html .=    '<th>Plan étage hover</th>';
    $html .=    '<th>Plan étage (back face)</th>';
    $html .=    '<th>Plan étage hover (back face)</th>';
    $html .= '</tr>';
    
    foreach($list_status as $status)
    {
        $html .= '<tr>';
        $html .=    '<td>'.$status->name.'</td>';
        $html .=    '<td><input type="file" name="plan_image_floor['.$status->id.'][]" /></td>';
        $html .=    '<td><input type="file" name="plan_image_floor_hover['.$status->id.'][]" /></td>';
        $html .=    '<td><input type="file" name="plan_image_floor_back['.$status->id.'][]" /></td>';
        $html .=    '<td><input type="file" name="plan_image_floor_back_hover['.$status->id.'][]" /></td>';
        $html .= '</tr>';
    }
    
    $html .= '</table></td>'; 
    
    echo $html;
    
    die();
    
}

/*
 *  Delete lot
 */
add_action( 'wp_ajax_nopriv_deleteLot', 'deleteLot' );  
add_action( 'wp_ajax_deleteLot', 'deleteLot' );
function deleteLot()
{
    $check = true;
    $errors = array();
    $success = array();
    $lot_id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperCore = new MeoCrmCoreHelper();
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init Variable For Connect To External DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Connect To The External DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Base URL for upload images
    $upload_path = $baseDirFTP.'uploads/floor-lot/'.$lot_id.'/';
    $upload_url = 'floor-lot/'.$lot_id.'/';
    
    // Delete lot table data
    $where = array('id' => $lot_id);
    $result = LotModel::deleteLot($external_wpdb, $where);
    
    if($result['success'])
    {
        $success[] = '- lot deleted with success';        
        // Delete meta lot value table data
        $where = array('lot_id' => $lot_id);
        $result = MetaLotModel::deleteMetaValueLot($external_wpdb, $where);        
        if($result['success'])
        {
            $success[] = '- lot deleted with success';        
            // Delete meta lot value table data
            $where = array('lot_id' => $lot_id);
            $result = FloorLotModel::deleteFloorLot($external_wpdb, $where);
            if($result['success'])
            {
                $success[] = '- floor lot deleted with success';  
            }else{
                $check = false;
                $errors[] = '- floor lot data can not deleted';
            }
        }else{
            $check = false;
            $errors[] = '- meta lot value datas can not deleted';
        }        
    }else{
        $check = false;
        $errors[] = '- lot data can not deleted';
    }
    
    if($check)
    {
        // Deleting file link to the lot
        $list_files = ftp_nlist( $ftp , $upload_path );    
        foreach($list_files as $key => $file)
        {
            list($path_file, $filename) = explode('/'.$lot_id.'/', $file);
            if($filename != '.' && $filename != '..')
            {
                if(ftp_delete ( $ftp , $list_files[$key] ))
                {
                    $success[] = '- deleted file ('.$filename.') with success'; 
                }else{
                    $check = false;
                    $errors[] = '- deleted file ('.$filename.') error';
                }
            }
        }
    
        $content_message = '<strong>Lot delete with success</strong>';
        foreach($success as $value)
        {
            $content_message .= '<br/>'.$value;
        }
        $message = $helperCore->getFlashMessageAdmin($content_message, 1);
    }else{
        $content_message = '<strong>Lot delete with success</strong>';
        foreach($error as $value)
        {
            $content_message .= '<br/>'.$value;
        }
        $message = $helperCore->getFlashMessageAdmin($content_message, 0);
    }
    
    // Array to return 
    $result = array(
        'success' => $check,
        'message' => $message
    );
    
    // Array Json Encoded
    echo json_encode($result);
    die();
}

/*
 *  Get lot by id
 */
add_action( 'wp_ajax_nopriv_getLotById', 'getLotById' );  
add_action( 'wp_ajax_getLotById', 'getLotById' );
function getLotById()
{
    $datas = array();
    $lot_id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Get Lot Datas
    $datas['lot'] = LotModel::getLotById($external_wpdb, $lot_id);
    
    // Get Lot Meta
    $datas['lot_meta'] = MetaLotModel::getValueMetaLotByLotId($external_wpdb, $lot_id);
    
    // Get Lot Meta
    $datas['lot_floor'] = FloorLotModel::getFloorLotByLotId($external_wpdb, $lot_id);

    echo json_encode($datas);
    die();
}
