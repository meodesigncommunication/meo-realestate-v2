<?php

/*
 *  Show Developpement List
 */
add_action( 'wp_ajax_nopriv_getFloorList', 'getFloorList' );  
add_action( 'wp_ajax_getFloorList', 'getFloorList' );
function getFloorList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = FloorModel::getAllFloor($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['building_id'] = $result->building_id;
        $datas[$result->id]['title'] = $result->title;
        $datas[$result->id]['position'] = $result->position;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'building_id', 'name' => 'Building', 'type_data' => 'base', 'class' => 'center-text'),
        array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => 'center-text'),
        array('key' => 'position', 'name' => 'Position', 'type_data' => 'base', 'class' => 'center-text'),
    ); 
    
    $list_action = array(
        array('click_action' => 'editFloor(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteFloor(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

add_action( 'wp_ajax_nopriv_saveFloor', 'saveFloor' );  
add_action( 'wp_ajax_saveFloor', 'saveFloor' );
function saveFloor()
{
    
    // Init From Value
    $id = $_POST['floor_id'];
    $building_id = $_POST['floor_building_id'];
    $title = $_POST['floor_title'];
    $position = $_POST['floor_position'];
    
    // Init variable
    $check = true;
    $datas = array();
    $errors = array();
    $project_id = $_POST['project_id'];
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
        
    // Check if required value are informs
    if(!empty($title) && $position != '' && !empty($building_id))
    {
        // Check if update or insert
        if(isset($id) && !empty($id))
        {
            
            $datas['building_id'] = $building_id;
            $datas['title'] = $title;
            $datas['position'] = $position;
            $where = array('id' => $id);
            
            $result = FloorModel::updateFloor($external_wpdb, $datas, $where);
        
            if($result['success'])
            {
                $check = true;
                $message = $helperCore->getFlashMessageAdmin('Floor create success', 1);
                $id = $result['id'];
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Floor create error'.$external_wpdb->last_error, 0);
                $id = 0;
            }
           
        }else{
            
            $datas['building_id'] = $building_id;
            $datas['title'] = $title;
            $datas['position'] = $position;
            
            $result = FloorModel::insertFloor($external_wpdb, $datas);
        
            if($result['success'])
            {
                $check = true;
                $message = $helperCore->getFlashMessageAdmin('Floor create success', 1);
                $id = $result['id'];
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Floor create error', 0);
                $id = 0;
            }
            
        }        
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
        'building_id' => $building_id
    );
    
    echo json_encode($result);
    
    die();
}

add_action( 'wp_ajax_nopriv_getListBuilding', 'getListBuilding' );  
add_action( 'wp_ajax_getListBuilding', 'getListBuilding' );
function getListBuilding()
{
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB    
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);   
    
    // Get all  
    $results = BuildingModel::getAllBuilding($external_wpdb);
    
    $html .=    '<option value="-1">Choisir un Batiment</option>';    
    foreach($results as $result)
    {
        $selected = '';
        if((isset($id) && !empty($id)) && $result->id == $id)
        {
            $selected = 'selected';
        }
        $html .= '<option value="'.$result->id.'" '.$selected.'>'.$result->title.'</option>';
    }    
    echo $html;    
    die();
}

// Get Floor entry by id
add_action( 'wp_ajax_nopriv_getFloorById', 'getFloorById' );  
add_action( 'wp_ajax_getFloorById', 'getFloorById' );
function getFloorById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $floor = FloorModel::getFloorById($external_wpdb,$id);
    
    echo json_encode($floor[0]);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deleteFloor', 'deleteFloor' );  
add_action( 'wp_ajax_deleteFloor', 'deleteFloor' );
function deleteFloor()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Get current developpement for take a two image link
    $floor = FloorModel::getFloorById($external_wpdb, $id);    
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Delete developpement on DB
    if($check)
    {
        $where = array( 'id' => $id );    
        $result = FloorModel::deleteFloor($external_wpdb,$where);
    }else{
        $result = array();
        $result['success'] = false;
    }
    
    // Check if all steps are OK for message return 
    if($result['success'])
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Floor delete success', 1);
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Floor delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message
    );
    
    echo json_encode($result);
    
    die();
}