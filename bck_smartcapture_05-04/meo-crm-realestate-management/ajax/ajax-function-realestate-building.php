<?php

   // TODO::Test if $project is not false
/*
 *  Show Developpement List
 */
add_action( 'wp_ajax_nopriv_getAllBuilding', 'getAllBuilding' );  
add_action( 'wp_ajax_getAllBuilding', 'getAllBuilding' );
function getAllBuilding()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = BuildingModel::getAllBuilding($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['sector_id'] = $result->sector_id;
        $datas[$result->id]['title'] = $result->title;
        $datas[$result->id]['description'] = $result->description;
        $datas[$result->id]['coordinate_plan_sector_3d'] = $result->coordinate_plan_sector_3d;
        $datas[$result->id]['coordinate_plan_sector_2d'] = $result->coordinate_plan_sector_2d;        
        $datas[$result->id]['image_hover_plan_sector_3d'] = $result->image_hover_plan_sector_3d;
        $datas[$result->id]['image_hover_plan_sector_2d'] = $result->image_hover_plan_sector_2d;        
        $datas[$result->id]['image_front_face'] = $result->image_front_face;
        $datas[$result->id]['image_back_face'] = $result->image_back_face;        
        $datas[$result->id]['image_preview'] = $result->image_preview;
        $datas[$result->id]['image_building_sector'] = $result->image_building_sector;        
        $datas[$result->id]['park_place_outdoor'] = $result->park_place_outdoor;
        $datas[$result->id]['park_place_indoor'] = $result->park_place_indoor;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'Description', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editBuilding(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteBuilding(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );   
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

/*
 *  Show Developpement List
 */
add_action( 'wp_ajax_nopriv_getListSector', 'getListSector' );  
add_action( 'wp_ajax_getListSector', 'getListSector' );
function getListSector()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = SectorModel::getAllSector($external_wpdb); 
    
    $html = '<option value="-1">Choose a sector linked</option>';
    
    foreach($results as $result)
    {
        if( (isset($id) && !empty($id)) )
        {
            $selected = ($result->id == $id) ? 'selected' : '';
        }
        $html .= '<option value="'.$result->id.'" '.$selected.'>'.$result->title.'</option>';
    }
    
    echo $html;
    
    die();
}


add_action( 'wp_ajax_nopriv_saveBuilding', 'saveBuilding' );  
add_action( 'wp_ajax_saveBuilding', 'saveBuilding' );
function saveBuilding()
{    
    // Init variable
    $check = true;
    $errors = array();
    $file_list = array();
    $folder_created = false;
    $deleted_picture = array(); 
    $id = $_POST['building_id'];
    $project_id = $_POST['project_id'];;
    
    // Standard Input Datas
    $datas = array();
    $datas['sector_id'] = $_POST['building_sector_list'];
    $datas['title'] = $_POST['building_title'];
    $datas['description'] = $_POST['building_description'];
    $datas['coordinate_plan_sector_3d'] = $_POST['coordinate_building_sector_3d'];
    $datas['coordinate_plan_sector_2d'] = $_POST['coordinate_building_sector_2d'];
    $datas['park_place_outdoor'] = $_POST['building_outdoor_parc'];
    $datas['park_place_indoor'] = $_POST['building_indoor_parc']; 
    
    // File Input Datas (ONLY FOR UPDATE BUILDING)
    $old_files = array();
    $old_files['image_hover_plan_sector_3d'] = $_POST['building_sector_image_3d_hover']; 
    $old_files['image_hover_plan_sector_2d'] = $_POST['building_sector_image_2d_hover']; 
    $old_files['image_front_face'] = $_POST['building_front_face']; 
    $old_files['image_back_face'] = $_POST['building_back_face']; 
    $old_files['image_preview'] = $_POST['building_preview']; 
    $old_files['image_building_sector'] = $_POST['building_sector']; 
    
    $required = array(
        'sector_id',
        'title',
        'coordinate_plan_sector_3d',
        'coordinate_plan_sector_2d',
    );

    // Init variable with DB results
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Base URL for upload images
    $upload_path = $baseDirFTP.'uploads/';
    $base_url = $project['url'].'wp-content/uploads';
    $base_path = $upload_path.'buildings/';
        
    // Check if required value are informs
    foreach($datas as $key => $value)
    {
        foreach($required as $require)
        {            
            if($key == $require)
            {
                if(empty($value))
                {
                    $check = false;
                }
            }
        }
    }
        
    // Check if update or insert
    if($check)
    {
        if(isset($id) && !empty($id))
        {           
            // Upadate standard data
            $where = array('id' => $id);
            $results = BuildingModel::updateBuilding($external_wpdb,$datas,$where); 
            
            if($results['success'])
            {
                
                // Path upload building
                $content_dir_url = 'buildings/'.$id.'/';
                $content_dir_path = $baseDirFTP.'uploads/buildings/'.$id.'/';
                        
                // New Files upload
                $file_list['image_hover_plan_sector_3d'] = $_FILES['building_sector_image_3d_hover'];
                $file_list['image_hover_plan_sector_2d'] = $_FILES['building_sector_image_2d_hover'];
                $file_list['image_front_face'] = $_FILES['building_front_face'];
                $file_list['image_back_face'] = $_FILES['building_back_face'];
                $file_list['image_preview'] = $_FILES['building_preview'];
                $file_list['image_building_sector'] = $_FILES['building_sector'];
                
                // Array with the real data db name
                $db_datas = array(
                    'building_sector_image_3d_hover' => 'image_hover_plan_sector_3d', 
                    'building_sector_image_2d_hover' => 'image_hover_plan_sector_2d', 
                    'building_front_face' => 'image_front_face',
                    'building_back_face' => 'image_back_face',
                    'building_preview' => 'image_preview',
                    'building_sector' => 'image_building_sector'
                );
                
                // Reset empty datas 
                $datas = array();
                $images = array();
                
                // Permet de lister les images a supprimer et upload la nouvelle
                foreach($file_list as $key => $file)
                {                    
                    if(isset($file['name']) && !empty($file['name']))
                    {
                        
                        list($url, $filename) = explode('/'.$id.'/', $old_files[$key]);
                        
                        $deleted_picture[$key] = $old_files[$key];
                        
                        if( !is_uploaded_file($file['tmp_name']) )
                        {
                            $errors[] = 'Temp File "'.$key.'" is not uploaded';
                            $check = false;
                        }
                        if( !strstr($file['type'], 'jpg') && !strstr($file['type'], 'jpeg') && !strstr($file['type'], 'png') )
                        {
                            $errors[] = $key.' is not a correct file type ('.$file['type'].')';
                            $check = false;
                        }
                        if( ftp_put($ftp, $content_dir_path.$file['name'], $file['tmp_name'], FTP_BINARY) === false )
                        {
                            $errors[] = $key.' is not upload on the external server';
                            $check = false;
                        }
                        
                        if($check)
                        {
                            $images[$key] = $datas[$key] = $content_dir_url . $file['name'];
                        }
                        
                    }else{
                        $images[$key] = $old_files[$key];
                    }
                }
                
                // Updating a data in DB
                if(is_array($datas) && !empty($datas))
                {
                    $where = array('id' => $id);
                    $result = BuildingModel::updateBuilding($external_wpdb, $datas, $where);
                    if(!$result['success'])
                    {
                        $check = false;
                    }
                }
                
                // Delete image in array delete_picture
                if(is_array($deleted_picture) && !empty($deleted_picture))
                {
                    foreach($deleted_picture as $key => $picture)
                    {
                        list($url, $filename) = explode('/'.$id.'/', $picture);                    
                        if(!ftp_delete($ftp, $content_dir_path.$filename))
                        {  
                            $check = false;
                        }
                    }
                } 
                
                // Generate message for form
                if($check)
                {
                    $message = $helperCore->getFlashMessageAdmin('Building update success', 1); 
                }else{
                    $check = false;
                    $content_message = '';
                    foreach($errors as $error)
                    {
                        $content_message .= ' - '.$error.'<br/>'; 
                    }
                    $message = $helperCore->getFlashMessageAdmin($content_message,0);
                } 
                
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Building update error',0);
            }
            
        }else{
            
            // Insert here
            $results = BuildingModel::insertBuilding($external_wpdb,$datas);

            // Upload and Insert Image
            if($results['success'])
            {

                $id = $results['id'];

                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path, 'buildings'))
                {
                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path.'buildings/', $id))
                    {
                        $folder_created = true;
                    }
                }

                if($folder_created)
                {

                    $content_dir_url = 'buildings/'.$id.'/';
                    $content_dir_path = $baseDirFTP.'uploads/buildings/'.$id.'/';

                    $file_list['image_hover_plan_sector_3d'] = $_FILES['building_sector_image_3d_hover'];
                    $file_list['image_hover_plan_sector_2d'] = $_FILES['building_sector_image_2d_hover'];
                    $file_list['image_front_face'] = $_FILES['building_front_face'];
                    $file_list['image_back_face'] = $_FILES['building_back_face'];
                    $file_list['image_preview'] = $_FILES['building_preview'];
                    $file_list['image_building_sector'] = $_FILES['building_sector'];

                    foreach($file_list as $key => $file)
                    {
                        $datas[$key] = $content_dir_url . $file['name'];
                    }

                    $where = array(
                        'id' => $id
                    );

                    $result = BuildingModel::updateBuilding($external_wpdb, $datas, $where);

                    if($result['success'])
                    {
                        // Check if file is uploaded 
                        foreach($file_list as $key => $file)
                        {
                            if( !is_uploaded_file($file['tmp_name']) )
                            {
                                $errors[] = 'Temp File "'.$key.'" is not uploaded';
                                $check = false;
                            }

                            if( !strstr($file['type'], 'jpg') && !strstr($file['type'], 'jpeg') && !strstr($file['type'], 'png') )
                            {
                                $errors[] = $key.' is not a correct file type ('.$file['type'].')';
                                $check = false;
                            }

                            if( ftp_put($ftp, $content_dir_path.$file['name'], $file['tmp_name'], FTP_BINARY) === false )
                            {
                                $errors[] = $key.' is not upload on the external server';
                                $check = false;
                            }
                        }
                        
                        $images = array(
                            'image_hover_plan_sector_3d' => $datas['image_hover_plan_sector_3d'],
                            'image_hover_plan_sector_2d' => $datas['image_hover_plan_sector_2d'],            
                            'image_front_face' => $datas['image_front_face'],
                            'image_back_face' => $datas['image_back_face'],            
                            'image_preview' => $datas['image_preview'],
                            'image_building_sector' => $datas['image_building_sector']
                        );

                        if($check)
                        {
                            // ITS OK INSERT AND UPLOAD !!!
                            $check = true;
                            $message = $helperCore->getFlashMessageAdmin('Building save success', 1); 
                        }  
                    }else{
                        $check = false;
                        $errors[] = 'Update table for put a image url error';
                    }
                }
            }
        }        
    }else{
        $check = false;
        $content_message = 'Error in data send, please informs all required data !';
        $message = $helperCore->getFlashMessageAdmin($content_message,0);
    }
    
    if(!$check)
    {
        $check = false;
        $content_message = '';
        foreach($errors as $error)
        {
            $content_message .= ' - '.$error.'<br/>'; 
        }
        $message = $helperCore->getFlashMessageAdmin($content_message,0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
        'images' => (isset($images)) ? $images : array()
    );
    
    echo json_encode($result);
    
    die();
}

add_action( 'wp_ajax_nopriv_getSelectBuilding', 'getSelectBuilding' );  
add_action( 'wp_ajax_getSelectBuilding', 'getSelectBuilding' );
function getSelectBuilding()
{
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB    
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);   
    
    // Get all  
    $results = DeveloppementModel::getAllDeveloppement($external_wpdb);
    
    $html  = '<select id="developpement_list" name="developpement_list">';
    $html .=    '<option value="-1">Choisir un developpement</option>';    
    foreach($results as $result)
    {
        $selected = '';
        if((isset($id) && !empty($id)) && $result->id == $id)
        {
            $selected = 'selected';
        }
        $html .= '<option value="'.$result->id.'" '.$selected.'>'.$result->title.'</option>';
    }
    $html .= '</select>';    
    echo $html;    
    die();
}

// Get Sector entry by id
add_action( 'wp_ajax_nopriv_getBuildingById', 'getBuildingById' );  
add_action( 'wp_ajax_getBuildingById', 'getBuildingById' );
function getBuildingById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $building = BuildingModel::getBuildingById($external_wpdb,$id);
    
    echo json_encode($building);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deleteBuilding', 'deleteBuilding' );  
add_action( 'wp_ajax_deleteBuilding', 'deleteBuilding' );
function deleteBuilding()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Get current developpement for take a two image link
    $building = BuildingModel::getBuildingById($external_wpdb, $id);
    
    $images = array();
    $images[] = $building->image_hover_plan_sector_3d;
    $images[] = $building->image_hover_plan_sector_2d;
    $images[] = $building->image_front_face;
    $images[] = $building->image_back_face;
    $images[] = $building->image_preview;
    $images[] = $building->image_building_sector;
    
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // URL for image
    $content_dir_path = $baseDirFTP.'uploads/buildings/'.$id.'/';
    
    // Delete images on FTP
    foreach($images as $image)
    {
        list($url, $filename) = explode('/'.$id.'/', $image);
        if(file_exists($content_dir_path.$filename))
        {
            if(!ftp_delete($ftp, $content_dir_path.$filename))
            {
                $check = false;
            }   
        }
    }
    
    // Delete developpement on DB
    if($check)
    {
        $where = array( 'id' => $id );    
        $result = BuildingModel::deleteBuilding($external_wpdb,$where);
    }else{
        $result = array();
        $result['success'] = false;
    }
    
    // Check if all steps are OK for message return 
    if($result['success'])
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Building delete success', 1);
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Building delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id
    );
    
    echo json_encode($result);
    
    die();
}