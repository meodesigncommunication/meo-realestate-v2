<?php

/*
 *  Create table in immo website database
 */
add_action( 'wp_ajax_nopriv_createdExternalTableForRealestate', 'createdExternalTableForRealestate' );  
add_action( 'wp_ajax_createdExternalTableForRealestate', 'createdExternalTableForRealestate' );
function createdExternalTableForRealestate()
{
    // INIT VARIABLE
    global $wpdb; 
    $check = true;
    $access_project_id = $_POST['access_project_id'];  
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = MeoCrmRealestateManagement::selectAccessProjectById($access_project_id);
    $project = meo_crm_projects_getProjectInfo($access[0]->project_id);
    
    // INIT CLASS
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access[0]->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access[0]->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access[0]->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access[0]->password_db);

    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);

    // CREATE ALL TABLE IN EXTERNAL DATABASE
    // CREATE Price List PDF TABLE
    $table_name = getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OPTIONS_TABLE;
    $query = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            project_id INTEGER(11) NOT NULL,
            pdf_template_portrait VARCHAR(255) NULL,
            pdf_template_landscape VARCHAR(255) NULL,
            color_theme VARCHAR(255) NULL,
            color_info VARCHAR(255) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    // CREATE PRICE LIST PDF Parameter TABLE
    $table_name = getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES_TABLE;
    $query = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            label VARCHAR(255) NOT NULL,
            price VARCHAR(255) NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }

    $table_name = getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_HEADER_VALUE_TABLE;
    $query = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            header_key VARCHAR(255) NOT NULL,
            label_1 VARCHAR(255) NOT NULL,
            label_2 VARCHAR(255) NOT NULL,
            position SMALLINT(5) NOT NULL DEFAULT 0,
            PRIMARY KEY (id)
        ) $charset_collate;";
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }

    // CREATE DEVELOPPEMENTS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            title VARCHAR(255) NULL,
            description TEXT NULL,
            plan_developpement_3d VARCHAR(255) NULL,
            plan_developpement_2d VARCHAR(255) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);    
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE SECTORS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_SECTORS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            developpement_id INTEGER(11) NOT NULL,
            title VARCHAR(255) NULL,
            description TEXT NULL,
            plan_sector_3d VARCHAR(255) NULL,
            plan_sector_mobile VARCHAR(255) NULL,
            plan_sector_2d VARCHAR(255) NULL,            
            coordinate_plan_developpement_3d VARCHAR(255) NULL,
            image_hover_plan_developpement_3d VARCHAR(255) NULL,            
            coordinate_plan_developpement_2d VARCHAR(255) NULL,
            image_hover_plan_developpement_2d VARCHAR(255) NULL,            
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE BUILDINGS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_BUILDING_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            sector_id INTEGER(11) NOT NULL,
            title VARCHAR(255) NULL,
            description TEXT NULL, 
            coordinate_plan_sector_3d VARCHAR(255) NULL,
            image_hover_plan_sector_3d VARCHAR(255) NULL,    
            coordinate_plan_sector_mobile VARCHAR(255) NULL,
            image_hover_plan_sector_mobile VARCHAR(255) NULL, 
            coordinate_plan_sector_2d VARCHAR(255) NULL,
            image_hover_plan_sector_2d VARCHAR(255) NULL,
            image_front_face VARCHAR(255) NULL,
            image_back_face VARCHAR(255) NULL,            
            image_preview VARCHAR(255) NULL,
            image_building_sector VARCHAR(255) NULL,
            park_place_outdoor SMALLINT(5) NULL,
            park_place_indoor SMALLINT(5) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE FLOORS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            building_id INTEGER(11) NOT NULL,
            title VARCHAR(255),
            position SMALLINT(5),
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE STATUS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_STATUS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            name VARCHAR(255) NULL,
            color VARCHAR(255) NULL,
            hide_price TINYINT(1) NULL,
            sold TINYINT(1) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE LOTS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            status_id INTEGER(11) NOT NULL,
            plan_id INTEGER(11) NOT NULL,
            document_id INTEGER(11) NOT NULL,
            title VARCHAR(255) NULL,
            description TEXT NULL, 
            rooms FLOAT NULL,
            surface FLOAT NULL,
            type_lot VARCHAR(255) NULL,
            price FLOAT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE PLANS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_PLANS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            spinner_id INTEGER(11) NOT NULL,
            title VARCHAR(255) NULL,
            description TEXT NULL,
            document VARCHAR(255) NULL,
            plan_2d VARCHAR(255) NULL,
            plan_3d VARCHAR(255) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE FLOOR_LOT TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_LOT_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            floor_id INTEGER(11) NOT NULL,
            lot_id INTEGER(11) NOT NULL,
            status_plan_floor_id INTEGER(11) NOT NULL,
            lot_entry TINYINT unsigned NOT NULL DEFAULT 0,
            plan_floor VARCHAR(255) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE COORDINATES_PLAN_LOT_BUILDING TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            status_id INTEGER(11) NOT NULL,
            image VARCHAR(255) NULL,
            hover TINYINT unsigned NOT NULL DEFAULT 0,
            front_face TINYINT unsigned NOT NULL DEFAULT 0,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    
    
    // CREATE COORDINATES_PLAN_LOT_BUILDING TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            title VARCHAR(255) NULL,
            coordinates_front VARCHAR(255) NULL,
            coordinates_back VARCHAR(255) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE COORDINATES_PLANS_FLOOR TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_LINK_COORDINATES_PLAN_FLOOR_LOT_BUILDING_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            coordinates_id INTEGER(11) NOT NULL,
            plan_lot_id INTEGER(11) NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE LOT_METAS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_LOT_METAS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            meta_key VARCHAR(255) NULL,
            meta_slug VARCHAR(255) NULL,
            meta_order SMALLINT(5) NULL,
            meta_type ENUM('text','boolean'),
            meta_filter tinyint(1),
            meta_unit VARCHAR(255) NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE LOT_METAS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            lot_id INTEGER(11) NOT NULL,
            meta_id INTEGER(11) NOT NULL,
            value LONGTEXT NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE SPINNER TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_SPINNERS_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            title VARCHAR(255) NULL,
            description TEXT NULL, 
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    // CREATE LOT_METAS TABLE
    $query = "CREATE TABLE IF NOT EXISTS " . getExternalPrefix() . MEO_CRM_REALESTATE_SPINNERS_POST_TABLE ." (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            post_id INTEGER(11) NOT NULL,
            spinner_id INTEGER(11) NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";    
    $result = $external_wpdb->query($query);
    if(!$result)
    {
        $check = false;
    }
    
    echo $check;
    
    wp_die();
}

add_action( 'wp_ajax_nopriv_getProjectUrl', 'getProjectUrl' );  
add_action( 'wp_ajax_getProjectUrl', 'getProjectUrl' );
function getProjectUrl()
{
    $id = $_POST['project_id'];
    $project = meo_crm_projects_getProjectInfo($id);
    
    echo $project['url'];    
    wp_die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateListLotFrontendInterface', 'meoCrmRealeastateListLotFrontendInterface' );  
add_action( 'wp_ajax_meoCrmRealeastateListLotFrontendInterface', 'meoCrmRealeastateListLotFrontendInterface' );
function meoCrmRealeastateListLotFrontendInterface()
{
    $output = '';
    $project_id = $_POST['project_id'];   
    $project = meo_crm_projects_getProjectInfo($project_id);    
    $development_data = meo_crm_realestate_get_development_data($project, 1, 1);
    
    if($development_data && is_object($development_data)){
    	$status = $development_data->status;    
	    $buildings = $development_data->buildings;
	    $lots = $development_data->lots;
	    
	    $output .=  '<table class="table wp-list-table widefat fixed striped posts">';
	    $output .=      '<thead>';
	    $output .=          '<tr>';
	    $output .=              '<th>Lot</th>';    
	    $output .=              '<th>Floor</th>';
	    $output .=              '<th>Building</th>';
	    $output .=              '<th>Type lot</th>';
	    $output .=              '<th>Rooms</th>';
	    $output .=              '<th>Surface</th>';
	    $output .=              '<th>Status</th>';
	    $output .=              '<th>Prix</th>';
	    $output .=          '<tr>';
	    $output .=      '</thead>';
	    $output .=      '<tbody>';
	    foreach($lots as $lot)
	    {

	        $floor_count = 0;
	        
	        $output .=  '<tr>';
	        $output .=      '<td>';
	        $output .=          $lot->title;
	        $output .=          '<input class="lot_id" type="hidden" value="'.$lot->id.'" />';
	        $output .=      '</td>';
	        
	        $output .=      '<td>';

            if(isset($lot->floors) && !empty($lot->floors))
            {
                foreach($lot->floors as $floor)
                {
                    if($floor_count > 0)
                    {
                        $output .= ' | ';
                    }
                    $building_id = $floor->building_id;
                    $output .= $floor->title;
                    $floor_count++;
                }
	        }

	        $output .=      '</td>';
	        
	        $output .=      '<td>';
	        foreach($buildings as $building)
	        {
	            if($building_id == $building->id)
	            {
	                $output .= $building->title;
	            }
	        }
	        $output .=      '</td>';
	        
	        $output .=      '<td>'.$lot->type_lot.'</td>';
	        $output .=      '<td>'.$lot->rooms.'</td>';
	        $output .=      '<td>'.$lot->surface.'m<sup>2</sup></td>';
	        
	        $output .=      '<td class="updated_list editable"><p>'.$lot->status.'</p>';
	        $output .=          '<div class="form-input-list">';
	        $output .=              '<select name="sale_lot_status" class="list_status">';
	        foreach($status as $status_val)
	        {
	            if($lot->status == $status_val->name)
	            {
	                $output .=  '<option value="'.$status_val->id.'" selected="true">'.$status_val->name.'</option>'; 
	            }else{
	                $output .=  '<option value="'.$status_val->id.'">'.$status_val->name.'</option>'; 
	            }
	        }
	        $output .=            '</select>';
	        //$output .=            '<input type="button" class="btn-update btn-update-status" name="update-status" value="save"  onclick="saveLotStatus(this,'.$lot->id.')" />';
	        $output .=            '<i class="cursor-hand fa fa-check-square save-state fieldUpdater" data-id="city-1922" onclick="saveLotStatus(this,'.$lot->id.')" aria-hidden="true"></i>';
	        $output .=            '<i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelEditableField(this)" aria-hidden="true"></i>';
	        $output .=          '</div>';
	        $output .=      '</td>';
	        
	        $output .=      '<td class="updated_list editable">';
	        $output .=          '<p>'.number_format($lot->price,0 ,".","'").' CHF</p>';
	        $output .=          '<div class="form-input-list">';
	        $output .=              '<input type="text" name="lot_price" class="lot_price" value="'.$lot->price.'">&nbsp;CHF&nbsp;&nbsp;';
            $output .=              '<i class="cursor-hand fa fa-check-square save-state fieldUpdater" data-id="city-1922" onclick="saveLotPrice(this,'.$lot->id.')" aria-hidden="true"></i>';
            $output .=              '<i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelEditableField(this)" aria-hidden="true"></i>';
	        $output .=          '</div>';
	        $output .=      '</td>';
	        $output .=  '<tr>';
	    }
	    $output .=      '</tbody>';
	    $output .= '</table>';
    }
	    
    
    echo $output;
    
    wp_die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateSaveLotStatus', 'meoCrmRealeastateSaveLotStatus' );  
add_action( 'wp_ajax_meoCrmRealeastateSaveLotStatus', 'meoCrmRealeastateSaveLotStatus' );
function meoCrmRealeastateSaveLotStatus()
{
    $datas = array();
    $where = array();
    $lot_id = $_POST['lot_id'];
    $status = $_POST['status'];
    $project_id = $_POST['project_id'];
    
    // DB Request
    $project = meo_crm_projects_getProjectInfo($project_id);

    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);
    
    $datas['status_id'] = $status;
    $where['id'] = $lot_id;
    
    $result = MeoCrmRealestateManagement::updateLot($external_wpdb,$datas,$where);

    do_action('meo_crm_realestate_create_price_list',$project_id);    
    wp_die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateSaveLotPrice', 'meoCrmRealeastateSaveLotPrice' );  
add_action( 'wp_ajax_meoCrmRealeastateSaveLotPrice', 'meoCrmRealeastateSaveLotPrice' );
function meoCrmRealeastateSaveLotPrice()
{
    $datas = array();
    $where = array();
    $lot_id = $_POST['lot_id'];
    $price = $_POST['price'];
    $project_id = $_POST['project_id'];
    
    // DB Request
    $project = meo_crm_projects_getProjectInfo($project_id);

    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);
    
    $datas['price'] = $price;
    $where['id'] = $lot_id;
    
    $result = MeoCrmRealestateManagement::updateLot($external_wpdb,$datas,$where);

    do_action('meo_crm_realestate_create_price_list',$project_id);    
    wp_die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateSaveFloorBuilding', 'meoCrmRealeastateSaveFloorBuilding' );
add_action( 'wp_ajax_meoCrmRealeastateSaveFloorBuilding', 'meoCrmRealeastateSaveFloorBuilding' );
function meoCrmRealeastateSaveFloorBuilding()
{
    $data['building_id'] = $_POST['building_id'];
    $data['title'] = $_POST['floor_name'];
    $data['position'] = 0;
    $project_id = $_POST['project_id'];

    $project = meo_crm_projects_getProjectInfo($project_id);
    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

    MeoCrmRealestateManagement::insertFloor($external_wpdb,$data);
    wp_die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateSaveFloorBuildingOrder', 'meoCrmRealeastateSaveFloorBuildingOrder' );
add_action( 'wp_ajax_meoCrmRealeastateSaveFloorBuildingOrder', 'meoCrmRealeastateSaveFloorBuildingOrder' );
function meoCrmRealeastateSaveFloorBuildingOrder()
{
    $count = 0;
    $floors = $_POST['floors'];
    $project_id = $_POST['project_id'];
    $project = meo_crm_projects_getProjectInfo($project_id);
    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

    foreach($floors as $floor)
    {
        $data = array(
            'position' =>  $count
        );
        $where = array(
          'id' => $floor
        );
        $count++;
        MeoCrmRealestateManagement::updateFloor($external_wpdb,$data,$where);
    }
    wp_die();
}