<?php

/*
 *  Show Developpement List
 */
add_action( 'wp_ajax_nopriv_getSectorList', 'getSectorList' );  
add_action( 'wp_ajax_getSectorList', 'getSectorList' );
function getSectorList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = SectorModel::getAllSector($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['developpement_id'] = $result->developpement_id;
        $datas[$result->id]['title'] = $result->title;
        $datas[$result->id]['description'] = $result->description;
        $datas[$result->id]['plan_sector_3d '] = $result->plan_sector_3d ;
        $datas[$result->id]['plan_sector_2d '] = $result->plan_sector_2d ;        
        $datas[$result->id]['coordinate_plan_developpement_3d '] = $result->coordinate_plan_developpement_3d ;
        $datas[$result->id]['coordinate_plan_developpement_2d '] = $result->coordinate_plan_developpement_2d ;        
        $datas[$result->id]['image_hover_plan_developpement_3d '] = $result->image_hover_plan_developpement_3d ;
        $datas[$result->id]['image_hover_plan_developpement_2d '] = $result->image_hover_plan_developpement_2d ;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'developpement_id', 'name' => 'Developpement', 'type_data' => 'base', 'class' => 'center-text'),
        array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'Description', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editSector(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteSector(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

add_action( 'wp_ajax_nopriv_saveSector', 'saveSector' );  
add_action( 'wp_ajax_saveSector', 'saveSector' );
function saveSector()
{
    
    // Init From Value
    $id = $_POST['sector_id'];
    $developpement_id = $_POST['developpement_list'];
    $title = $_POST['sector_title'];
    $description = $_POST['sector_description'];
    $coordinate_3d = $_POST['coordinate_sector_developpement_3d'];
    $coordinate_2d = $_POST['coordinate_sector_developpement_2d'];
    
    // Init variable
    $check = true;
    $datas = array();
    $errors = array();
    $actionDB = '';
    $folder_created = false;
    $deleted_picture = array();    
    $project_id = $_POST['project_id'];
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Base URL for upload images
    $upload_path = $baseDirFTP.'uploads/';
    $base_url = $project['url'].'wp-content/uploads';
    $base_path = $upload_path.'sectors/';
        
    // Check if required value are informs
    if(!empty($title) && !empty($description) && !empty($coordinate_3d) && !empty($coordinate_2d))
    {
        // Check if update or insert
        if(isset($id) && !empty($id))
        {
            // Init array
            $pictures = array();
            $delete_pictures = array();
            
            //Init URL
            $content_dir_url = 'sectors/'.$id.'/';
            $content_dir_path = $baseDirFTP.'uploads/sectors/'.$id.'/';
            
            // Datas For Update
            $datas['developpement_id'] = $developpement_id;
            $datas['title'] = $title;
            $datas['description'] = $description;
            $datas['coordinate_plan_developpement_3d'] = $coordinate_3d;
            $datas['coordinate_plan_developpement_2d'] = $coordinate_2d;
            
            // Pictures Array
            $pictures['plan_sector_3d']['filename'] = $_FILES['sector_image_3d']['name'];
            $pictures['plan_sector_3d']['index'] = 'plan_sector_3d_old';            
            $pictures['plan_sector_2d']['filename'] = $_FILES['sector_image_2d']['name'];
            $pictures['plan_sector_2d']['index'] = 'plan_sector_2d_old';            
            $pictures['image_hover_plan_developpement_3d']['filename'] = $_FILES['sector_developpement_image_3d']['name'];
            $pictures['image_hover_plan_developpement_3d']['index'] = 'plan_sector_developpement_3d_old';            
            $pictures['image_hover_plan_developpement_2d']['filename'] = $_FILES['sector_developpement_image_2d']['name'];
            $pictures['image_hover_plan_developpement_2d']['index'] = 'plan_sector_developpement_2d_old';
            
            // Permet de définir les images a updater
            foreach($pictures as $key => $picture)
            {
                if(empty($picture['filename']))
                {
                    $datas[$key] = (isset($picture['index'])) ? $_POST[$picture['index']] : '';
                }else{
                    $datas[$key] = $content_dir_url.$picture['filename'];
                    $deleted_picture[$key] = $_POST[$picture['index']];
                }
            }
            
            // Image as change delete this and add new
            if(is_array($deleted_picture) && !empty($deleted_picture))
            {
                foreach($deleted_picture as $key => $picture)
                {
                    list($url, $filename) = explode('/'.$id.'/', $picture);                    
                    if(!ftp_delete($ftp, $content_dir_path.$filename))
                    {  
                        $check = false;
                    }else{
                        
                        if($key == 'plan_sector_3d')
                        {
                            $tmp_file = $_FILES['sector_image_3d']['tmp_name'];
                            $type_file = $_FILES['sector_image_3d']['type'];
                            $name_file = $_FILES['sector_image_3d']['name'];
                            
                        }else if($key == 'plan_sector_2d'){
                            
                            $tmp_file = $_FILES['sector_image_2d']['tmp_name'];
                            $type_file = $_FILES['sector_image_2d']['type'];
                            $name_file = $_FILES['sector_image_2d']['name'];
                            
                        }else if($key == 'image_hover_plan_developpement_3d'){
                            
                            $tmp_file = $_FILES['sector_developpement_image_3d']['tmp_name'];
                            $type_file = $_FILES['sector_developpement_image_3d']['type'];
                            $name_file = $_FILES['sector_developpement_image_3d']['name'];
                            
                        }else if($key == 'image_hover_plan_developpement_2d'){
                            
                            $tmp_file = $_FILES['sector_developpement_image_2d']['tmp_name'];
                            $type_file = $_FILES['sector_developpement_image_2d']['type'];
                            $name_file = $_FILES['sector_developpement_image_2d']['name'];
                            
                        }
                        
                        // Check if uploaded
                        if( !is_uploaded_file($tmp_file) )
                        {
                            $check = false;
                        }
                        
                        // on vérifie maintenant l'extension
                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                        {
                            $check = false;
                        }
                        
                        // on copie le fichier dans le dossier de destination
                        if( !ftp_put($ftp, $content_dir_path.$name_file, $tmp_file, FTP_BINARY) === false )
                        {
                            $check = false;
                        }
                    }
                }
            }
            
            // if upload picture is ok you can update informations
            if($check)
            {
                $where = array('id' => $id);            
                $result = SectorModel::updateSector($external_wpdb, $datas, $where);            
                $actionDB = 'update'; 
                
                $sector_image_3d = $datas['plan_sector_3d'];
                $sector_image_2d = $datas['plan_sector_2d'];
                $sector_developpement_image_3d = $datas['image_hover_plan_developpement_3d'];
                $sector_developpement_image_2d = $datas['image_hover_plan_developpement_2d'];
                
                if($result['success'])
                {
                    $check = true;
                    $message = $helperCore->getFlashMessageAdmin('Sector edit success', 1);
                }else{
                    $check = false;
                    $message = $helperCore->getFlashMessageAdmin('Sector edit error', 0);
                }
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Sector edit error', 0);
            }   
            
        }else{
            // Its an insert
            $datas['developpement_id'] = $developpement_id;
            $datas['title'] = $title;
            $datas['description'] = $description;
            $datas['coordinate_plan_developpement_3d'] = $coordinate_3d;
            $datas['coordinate_plan_developpement_2d'] = $coordinate_2d;
            
            // Insert here
            $results = SectorModel::insertSector($external_wpdb,$datas);
            
            // Upload and Insert Image
            if($results['success'])
            {
                
                $id = $results['id'];
                $actionDB = 'insert';
                        
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path, 'sectors'))
                {
                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path.'sectors/', $id))
                    {
                        $folder_created = true;
                    }
                }

                if($folder_created)
                {

                    $content_dir_url = 'sectors/'.$id.'/';
                    $content_dir_path = $baseDirFTP.'uploads/sectors/'.$id.'/';

                    $datas['plan_sector_3d'] = $content_dir_url.$_FILES['sector_image_3d']['name'];
                    $datas['plan_sector_2d'] = $content_dir_url.$_FILES['sector_image_2d']['name'];
                    $datas['image_hover_plan_developpement_3d'] = $content_dir_url.$_FILES['sector_developpement_image_3d']['name'];
                    $datas['image_hover_plan_developpement_2d'] = $content_dir_url.$_FILES['sector_developpement_image_2d']['name'];

                    $where = array(
                        'id' => $id
                    );

                    $result = SectorModel::updateSector($external_wpdb, $datas, $where);
                    
                    if($result['success'])
                    {
                        // Check if file is uploaded
                        $tmp_file_sector_image_3d = $_FILES['sector_image_3d']['tmp_name'];
                        $tmp_file_sector_image_2d = $_FILES['sector_image_2d']['tmp_name'];
                        $tmp_file_sector_developpement_image_3d = $_FILES['sector_developpement_image_3d']['tmp_name'];
                        $tmp_file_sector_developpement_image_2d = $_FILES['sector_developpement_image_2d']['tmp_name'];

                        if( !is_uploaded_file($tmp_file_sector_image_3d) && !is_uploaded_file($tmp_file_sector_image_2d) &&!is_uploaded_file($tmp_file_sector_developpement_image_3d) && !is_uploaded_file($tmp_file_sector_developpement_image_2d) )
                        {
                            $errors[] = 'Temp File is not uploaded';
                            $check = false;
                        }

                        // on vérifie maintenant l'extension de l'image 3d    
                        $type_files = array();
                        $type_files['sector_image_3d'] = $_FILES['sector_image_3d']['type'];
                        $type_files['sector_image_2d'] = $_FILES['sector_image_2d']['type'];
                        $type_files['sector_developpement_image_3d'] = $_FILES['sector_developpement_image_3d']['type'];
                        $type_files['sector_developpement_image_2d'] = $_FILES['sector_developpement_image_2d']['type'];

                        foreach($type_files as $key => $value)
                        {
                            if( !strstr($value, 'jpg') && !strstr($value, 'jpeg') && !strstr($value, 'png') )
                            {
                                $errors[] = $key.' is not a correct file type ('.$value.')';
                                $check = false;
                            }
                        }

                        // on copie le fichier dans le dossier de destination
                        $name_files = array();

                        $name_files['sector_image_3d']['name'] = $_FILES['sector_image_3d']['name'];
                        $name_files['sector_image_3d']['tmp'] = $tmp_file_sector_image_3d;

                        $name_files['sector_image_2d']['name'] = $_FILES['sector_image_2d']['name'];
                        $name_files['sector_image_2d']['tmp'] = $tmp_file_sector_image_2d;

                        $name_files['sector_developpement_image_3d']['name'] = $_FILES['sector_developpement_image_3d']['name'];
                        $name_files['sector_developpement_image_3d']['tmp'] = $tmp_file_sector_developpement_image_3d;

                        $name_files['sector_developpement_image_2d']['name'] = $_FILES['sector_developpement_image_2d']['name'];
                        $name_files['sector_developpement_image_2d']['tmp'] = $tmp_file_sector_developpement_image_2d;

                        foreach($name_files as $key => $name_file)
                        {
                            if( ftp_put($ftp, $content_dir_path.$name_file['name'], $name_file['tmp'], FTP_BINARY) === false )
                            {
                                $errors[] = $key.' is not upload on the external server';
                                $check = false;
                            }
                        }

                        if($check)
                        {
                            // ITS OK INSERT AND UPLOAD !!!
                            $check = true;
                            $message = $helperCore->getFlashMessageAdmin('Sector save success', 1);                            
                            $sector_image_3d = $content_dir_url.$_FILES['sector_image_3d']['name'];
                            $sector_image_2d = $content_dir_url.$_FILES['sector_image_2d']['name'];
                            $sector_developpement_image_3d = $content_dir_url.$_FILES['sector_developpement_image_3d']['name'];
                            $sector_developpement_image_2d = $content_dir_url.$_FILES['sector_developpement_image_2d']['name'];
                        }  
                    }else{
                        $check = false;
                        $errors[] = 'Update table for put a image url error';
                    }
                }
            }
        }        
    }
    
    if(!$check)
    {
        $check = false;
        $content_message = '';
        foreach($errors as $error)
        {
            $content_message .= ' - '.$error.'<br/>'; 
        }
        $message = $helperCore->getFlashMessageAdmin($content_message,0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
        'images' => array(
            'sector_image_3d' => (isset($sector_image_3d)) ? $sector_image_3d : '',
            'sector_image_2d' => (isset($sector_image_2d)) ? $sector_image_2d : '',
            'sector_developpement_image_3d' => (isset($sector_developpement_image_3d)) ? $sector_developpement_image_3d : '',
            'sector_developpement_image_2d' => (isset($sector_developpement_image_2d)) ? $sector_developpement_image_2d : ''
        )
    );
    
    echo json_encode($result);
    
    die();
}

add_action( 'wp_ajax_nopriv_getSelectDeveloppement', 'getSelectDeveloppement' );  
add_action( 'wp_ajax_getSelectDeveloppement', 'getSelectDeveloppement' );
function getSelectDeveloppement()
{
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB    
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);   
    
    // Get all  
    $results = DeveloppementModel::getAllDeveloppement($external_wpdb);
    
    $html  = '<select id="developpement_list" name="developpement_list">';
    $html .=    '<option value="-1">Choisir un developpement</option>';    
    foreach($results as $result)
    {
        $selected = '';
        if((isset($id) && !empty($id)) && $result->id == $id)
        {
            $selected = 'selected';
        }
        $html .= '<option value="'.$result->id.'" '.$selected.'>'.$result->title.'</option>';
    }
    $html .= '</select>';    
    echo $html;    
    die();
}

// Get Sector entry by id
add_action( 'wp_ajax_nopriv_getSectorById', 'getSectorById' );  
add_action( 'wp_ajax_getSectorById', 'getSectorById' );
function getSectorById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $sector = SectorModel::getSectorById($external_wpdb,$id);
    
    echo json_encode($sector);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deleteSector', 'deleteSector' );  
add_action( 'wp_ajax_deleteSector', 'deleteSector' );
function deleteSector()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Get current developpement for take a two image link
    $sector = SectorModel::getSectorById($external_wpdb, $id);
    $images = array();
    $images[] = $sector->plan_sector_3d;
    $images[] = $sector->plan_sector_2d;
    $images[] = $sector->image_hover_plan_developpement_3d;
    $images[] = $sector->image_hover_plan_developpement_2d;
    
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // URL for image
    $content_dir_path = $baseDirFTP.'uploads/sectors/'.$id.'/';
    
    // Delete images on FTP
    foreach($images as $image)
    {
        list($url, $filename) = explode('/'.$id.'/', $image);
        if(!empty($filename))
        {
            if(!ftp_delete($ftp, $content_dir_path.$filename))
            {
                $check = false;
            }  
        }
    }
    
    // Delete developpement on DB
    if($check)
    {
        $where = array( 'id' => $id );    
        $result = SectorModel::deleteSector($external_wpdb,$where);
    }else{
        $result = array();
        $result['success'] = false;
    }
    
    // Check if all steps are OK for message return 
    if($result['success'])
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Sector delete success', 1);
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Sector delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id
    );
    
    echo json_encode($result);
    
    die();
}