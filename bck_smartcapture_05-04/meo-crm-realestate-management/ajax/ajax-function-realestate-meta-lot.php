<?php 

/*
 *  Show Status List
 */
add_action( 'wp_ajax_nopriv_getMetaLotList', 'getMetaLotList' );  
add_action( 'wp_ajax_getMetaLotList', 'getMetaLotList' );
function getMetaLotList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = MetaLotModel::getAllMetaLot($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['meta_key'] = $result->meta_key;
        $datas[$result->id]['meta_slug'] = $result->meta_slug;
        $datas[$result->id]['meta_type'] = $result->meta_type;
        $datas[$result->id]['meta_order'] = $result->meta_order;
        $datas[$result->id]['meta_filter'] = $result->meta_filter;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'meta_key', 'name' => 'Meta', 'type_data' => 'base', 'class' => ''),
        array('key' => 'meta_slug', 'name' => 'Slug', 'type_data' => 'base', 'class' => ''),
        array('key' => 'meta_type', 'name' => 'Type', 'type_data' => 'base', 'class' => ''),
        array('key' => 'meta_order', 'name' => 'Ordre', 'type_data' => 'base', 'class' => ''),
        array('key' => 'meta_filter', 'name' => 'Filtre', 'type_data' => 'boolean', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editMetaLot(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteMetaLot(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

/*
 *  Create and update Status
 */
add_action( 'wp_ajax_nopriv_saveMetaLot', 'saveMetaLot' );  
add_action( 'wp_ajax_saveMetaLot', 'saveMetaLot' );
function saveMetaLot()
{
    // Init From Value
    $id = $_POST['id'];
    $meta_key = $_POST['meta_key'];
    $meta_slug = $_POST['meta_slug'];
    $meta_type = $_POST['meta_type'];
    $meta_order = $_POST['meta_order'];
    $meta_filter = $_POST['meta_filter'];
    $project_id = $_POST['project_id'];
    
    // Init variable
    $check = true;
    $datas = array();
    $errors = array();  
    
    // DB Request
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    if(isset($id) && !empty($id))
    {
        // Update current status
        $datas['meta_key'] = $meta_key;
        $datas['meta_slug'] = $meta_slug;
        $datas['meta_type'] = $meta_type;
        $datas['meta_order'] = $meta_order;
        $datas['meta_filter'] = $meta_filter;
        
        $where = array('id' => $id);
        $result = MetaLotModel::updateMetaLot($external_wpdb, $datas, $where);
        
        if($result['success'])
        {
            $check = true;
            $message = $helperCore->getFlashMessageAdmin('Meta update success', 1);
        }else{
            $check = false;
            $message = $helperCore->getFlashMessageAdmin('Meta update error '.$external_wpdb->last_error, 0);
        }
    }else{
        // Insert new status
        $datas['meta_key'] = $meta_key;
        $datas['meta_slug'] = $meta_slug;
        $datas['meta_type'] = $meta_type;
        $datas['meta_order'] = $meta_order;
        $datas['meta_filter'] = $meta_filter;
        
        $result = MetaLotModel::insertMetaLot($external_wpdb, $datas);
        
        if($result['success'])
        {
            $check = true;
            $message = $helperCore->getFlashMessageAdmin('Meta create success', 1);
            $id = $result['id'];
        }else{
            $check = false;
            $message = $helperCore->getFlashMessageAdmin('Meta create error | '.$external_wpdb->last_error, 0);
            $id = 0;
        }
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
    );
    
    echo json_encode($result);
    
    die();
}

// Get Sector entry by id
add_action( 'wp_ajax_nopriv_getMetaLotById', 'getMetaLotById' );  
add_action( 'wp_ajax_getMetaLotById', 'getMetaLotById' );
function getMetaLotById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $status = MetaLotModel::getMetaLotById($external_wpdb,$id);
    
    echo json_encode($status);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deleteMetaLot', 'deleteMetaLot' );  
add_action( 'wp_ajax_deleteMetaLot', 'deleteMetaLot' );
function deleteMetaLot()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Delete Status by id
    $where = array( 'id' => $id );    
    $result = MetaLotModel::deleteMetaLot($external_wpdb,$where);
    
    // Check if all steps are OK for message return 
    if($result['success'])
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Status delete success', 1);
        
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Status delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message
    );
    
    echo json_encode($result);
    
    die();
}