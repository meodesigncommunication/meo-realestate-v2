<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/

class MeoCrmContactsUsersRelationship {
	
	# Build
	
		public function __construct() {	}
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
	
	# Get Table Headers (const or private static array, when PHP5.6)
		/*
		public static function getTableHeaders (){
			// AllowedFields to be printed on the contacts table
			$allowedFields = array(	'availability', 'activity', 'last_send');
				
			$fields = array();
			foreach($allowedFields as $field) {
				$fields[ucwords(str_replace('_', ' ', $field))] = $field;
			}
			return $fields;
		}*/
	
		
	# Manage User Availability in the Database
		
		
		# Contact User Relationship
		
		public static function addContactUserRelationship ($data){
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE;
				
			$newID = $wpdb->insert( $table, $data );
			
			if($newID) { return $wpdb->insert_id; }
			return false;
		}
		
		public static function updateContactUserRelationship ($data, $where){
			
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE;
				
			$wpdb->update( $table, $data, $where );
		}
		
		public static function deleteContactUserRelationshipState ($where){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE;
			
			$wpdb->delete( $table, $where );
		}
		
		
		# Project Ralationship Status
		
		public static function addProjectRelationshipStatus ($data){
			
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE;
			
			$newID = $wpdb->insert( $table, $data );

			if($newID) { return $wpdb->insert_id; }
			return false;
		}
		
		public static function updateProjectRelationshipStatus ($data, $where){
			
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE;
		
			$wpdb->update( $table, $data, $where );
		}
		
		public static function deleteProjectRelationshipStatus ($where){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE;
			
			$wpdb->delete( $table, $where );
		}
		
		
		# Contact User Interaction
		
		public static function addContactUserInteraction ($data){
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_INTERACTIONS_TABLE;
		
			$newID = $wpdb->insert( $table, $data );
				
			if($newID) { return $wpdb->insert_id; }
			return false;
		}
		
		public static function updateContactUserInteraction ($data, $where){
				
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_INTERACTIONS_TABLE;
		
			$wpdb->update( $table, $data, $where );
		}
		
		public static function deleteContactUserInteraction ($where){
				
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CONTACTS_USERS_INTERACTIONS_TABLE;
				
			$wpdb->delete( $table, $where );
		}
		
	
	###############
	# USERS LEVEL #
	###############
		
		#getRecords
		
		public static function getRecords (){
				
			global $wpdb;
				
			// Get Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_USERS_RELATIONSHIP_TABLE . " ORDER BY date_updated DESC";
			
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records && is_array($records) && count($records) > 0) return $records;
			
			return false;
		}
		
		#getStatusByProjectID
		
		public static function getStatusByProjectID ($ProjectID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE . " WHERE project_id = ".$ProjectID." ORDER BY status_order ASC";
		
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
		
		#getUserByContactID
		
		public static function getUserByContactID ($ContactID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT u.id, u.display_name AS courtier_name, u.user_email AS courtier_email, cur.date_updated, cur.note FROM ".$wpdb->prefix."users u 
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur ON cur.user_id = u.id 
						WHERE cur.contact_id = ".$ContactID."  
							ORDER BY cur.date_updated DESC 
								LIMIT 0, 1";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
		
		
	######################
	# INTERACTIONS LEVEL #
	######################
		
		#getInteractions
		
		public static function getInteractions (){
		
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_USERS_INTERACTIONS_TABLE . " 
						ORDER BY date_updated DESC";
				
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records && is_array($records) && count($records) > 0) return $records;
				
			return false;
		}
		
		#getInteractionsByContactID
		
		public static function getInteractionsByContactID ($ContactID){
		
			global $wpdb;
		
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_USERS_INTERACTIONS_TABLE . " 
						WHERE contact_id = ".$ContactID." 
							ORDER BY date_updated ASC";
		
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
		
		#getInteractionByID
		
		public static function getInteractionByID ($ID){
		
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_USERS_INTERACTIONS_TABLE . " 
						WHERE id = ".$ID." 
							ORDER BY date_updated DESC 
								LIMIT 0, 1";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
		
		
	#######################
	# RELATIONSHIP STATUS #
	#######################
		
		#getStatusByContactID
		
		public static function getStatusByContactID ($ContactID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT cur.id as record_id, cur.status_id, curs.label, curs.color, cur.date_updated, cur.note 
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur 
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE ." curs ON curs.id = cur.status_id 
						WHERE cur.contact_id = ".$ContactID."
							ORDER BY cur.date_updated ASC, curs.status_order ASC";
			
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
		
		#getLatestStatusByContactID
		
		public static function getLatestStatusByContactID ($ContactID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT cur.id as record_id, cur.status_id, curs.label, curs.color, cur.date_updated, cur.note
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE ." curs ON curs.id = cur.status_id
							WHERE cur.contact_id = ".$ContactID." AND cur.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID." AND cur.status_id != 0
								ORDER BY cur.date_updated ASC
								LIMIT 0, 1";
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) return reset($records);
		
			return false;
		}
		
		#countContactsPerStatusIDbyCourtierIDProjectID
		
		public static function countContactsPerStatusIDbyCourtierIDProjectID($courtierID, $projectID, $date1 = '', $date2 = ''){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT cur.status_id, COUNT(cur.contact_id) as nbcontacts   
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur 
						LEFT JOIN (
									SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact 
									FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur2 
										WHERE cur2.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID."
									GROUP BY contact
									) as t2 
							ON cur.contact_id = t2.contact 
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id 
							WHERE cur.date_updated = t2.last_status
								AND c.project_id = ".$projectID."  
								AND cur.user_id = ".$courtierID."  
								AND c.date_deleted IS NULL";
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$query.= "		GROUP BY cur.status_id";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) {
				$return = array();
				
				foreach($records as $r => $result){
					$return[$result['status_id']] = $result['nbcontacts'];
				}
				return $return;
			}
		
			return false;
		}
		
		#countContactsByStatusIDbyCourtierIDProjectID
		
		public static function countContactsByStatusIDbyCourtierIDProjectID($statusID, $courtierID, $projectID, $date1 = '', $date2 = ''){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT COUNT(cur.contact_id) as nbcontacts
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN (
									SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact
									FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur2 
									WHERE cur2.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID."
									GROUP BY contact
									) as t2
							ON cur.contact_id = t2.contact
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
							WHERE cur.date_updated = t2.last_status
								AND c.project_id = ".$projectID."
								AND cur.user_id = ".$courtierID." 
								AND cur.status_id = ".$statusID."  
								AND c.date_deleted IS NULL";
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$query.= "		GROUP BY cur.status_id";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			$result = reset($records);
			return $result['nbcontacts'];
		}
		


		#countContactsPerSecondLastStatusIDExitDetailsbyCourtierIDProjectID :: Will probably replace other methods as it has been improved to get all details in one query
		
		public static function countContactsPerSecondLastStatusIDExitDetailsbyCourtierIDProjectID($courtierID, $projectID, $date1 = '', $date2 = ''){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT COUNT(cur.contact_id) as nbcontacts, cur.status_id, t2.note_exit
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN (
								SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact, t3.exit_note as note_exit
								FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur2
		
								INNER JOIN (SELECT curs.status_id as status_id, curs.contact_id, curs.note as exit_note
										FROM ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." curs
										WHERE curs.status_id = ".MEO_RELATIONSHIP_STATE_EXIT_ID."
								) as t3
								ON cur2.contact_id = t3.contact_id
									WHERE cur2.status_id != t3.status_id
										AND cur2.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID."
								GROUP BY contact
						) as t2
						ON cur.contact_id = t2.contact
		
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
							WHERE cur.date_updated = t2.last_status
							AND c.date_deleted IS NULL
							AND c.project_id = ".$projectID."
							AND cur.user_id = ".$courtierID." ";
				
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
				
			$query.= "		GROUP BY cur.status_id, t2.note_exit";
			
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records && is_array($records) && count($records) > 0) {
				$return = array();
		
				foreach($records as $r => $result){
					$return[$result['status_id']][$result['note_exit']] = $result['nbcontacts'];
					
					if(!array_key_exists('total', $return[$result['status_id']])){
						$return[$result['status_id']]['total'] = 0;
					}
					
					$return[$result['status_id']]['total']+= $result['nbcontacts'];
				}
				
				return $return;
			}
		
			return false;
		}
		
		# countContactsPerSecondLastStatusIDbyCourtierIDProjectID : Get number of EXIT per state before EXIT (except reassignement)
		
		public static function countContactsPerSecondLastStatusIDbyCourtierIDProjectID($courtierID, $projectID, $date1 = '', $date2 = ''){
		
			global $wpdb;
		
			// Get Records
			/*$query = "SELECT COUNT(cur.contact_id) as nbcontacts, cur.status_id
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN (
								SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact
								FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur2
						
								INNER JOIN (SELECT curs.id as status_id, cur3.contact_id
										FROM ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE ." curs
										LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur3 ON curs.id = cur3.status_id
										WHERE curs.id = (
												SELECT curs.id as status_id
												FROM ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE ." curs
												ORDER BY curs.status_order DESC
												LIMIT 0, 1
										)
								) as t3
								ON cur2.contact_id = t3.contact_id
									WHERE cur2.status_id != t3.status_id
										AND cur2.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID." 
								GROUP BY contact
						) as t2
						ON cur.contact_id = t2.contact
						
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
							WHERE cur.date_updated = t2.last_status
							AND c.project_id = ".$projectID."
							AND cur.user_id = ".$courtierID." 
						GROUP BY cur.status_id";*/
			
			$query = "SELECT COUNT(cur.contact_id) as nbcontacts, cur.status_id
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN (
								SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact
								FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur2
						
								INNER JOIN (SELECT curs.status_id as status_id, curs.contact_id
										FROM ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." curs
										WHERE curs.status_id = ".MEO_RELATIONSHIP_STATE_EXIT_ID."
								) as t3
								ON cur2.contact_id = t3.contact_id
									WHERE cur2.status_id != t3.status_id
										AND cur2.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID." 
								GROUP BY contact
						) as t2
						ON cur.contact_id = t2.contact
						
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
							WHERE cur.date_updated = t2.last_status 
							AND c.date_deleted IS NULL
							AND c.project_id = ".$projectID."
							AND cur.user_id = ".$courtierID." ";
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$query.= "		GROUP BY cur.status_id";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) {
				$return = array();
				
				foreach($records as $r => $result){
					$return[$result['status_id']] = $result['nbcontacts'];
				}
				return $return;
			}
		
			return false;
		}
		
		#countContactsPerRefererPerStatusbyProjectID
		
		public static function countContactsPerRefererPerStatusbyProjectID($projectID, $date1 = '', $date2 = ''){
		
			global $wpdb;
		
			// Get Records
			$query ="SELECT IFNULL(c.referer, 'Direct') as referer, cur.status_id, IFNULL(curs.label, 'Record') as label, IFNULL(curs.color, '".MEO_RELATIONSHIP_STATE_DEFAULT_COLOR."') as color, COUNT(cur.contact_id) as nbcontacts
						FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE ." curs ON curs.id = cur.status_id
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
						LEFT JOIN (
								SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact
								FROM ".$wpdb->prefix. MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur2 
									WHERE cur2.status_id != ".MEO_RELATIONSHIP_STATE_REASSIGNED_ID."
								GROUP BY contact
						) as t2
						ON cur.contact_id = t2.contact
							WHERE cur.date_updated = t2.last_status 
							AND c.date_deleted IS NULL
							AND c.project_id = ".$projectID;

			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$query.= "		GROUP BY c.referer, cur.status_id 
						ORDER BY nbcontacts DESC, c.referer ASC";
			
			/*
			SELECT IFNULL(c.referer, 'Direct') as referer, cur.status_id, IFNULL(curs.label, 'Record') as label, IFNULL(curs.color, '#cccccc') as color, COUNT(cur.contact_id) as nbcontacts 
			FROM wp_meo_crm_contacts_users_relationship cur 
				LEFT JOIN wp_meo_crm_contacts_users_relationship_status curs 
					ON curs.id = cur.status_id 
				LEFT JOIN wp_meo_crm_contacts c 
					ON cur.contact_id = c.id 
				LEFT JOIN (
							SELECT curs.status_id as status_id, curs.contact_id
									FROM wp_meo_crm_contacts_users_relationship curs
									WHERE curs.status_id = 999999
							) as t3 
				ON cur.contact_id = t3.contact_id 
				LEFT JOIN 
				( 
					SELECT MAX(cur2.date_updated) as last_status, cur2.contact_id as contact 
						FROM wp_meo_crm_contacts_users_relationship cur2 
						
							WHERE cur2.status_id != 9999 
								GROUP BY contact 
				) as t2 
					ON cur.contact_id = t2.contact 
				WHERE cur.date_updated = t2.last_status 
					AND c.project_id = 28 
					AND c.date_added > '2012-10-18' 
					AND c.date_added < '2016-10-18' 
				GROUP BY c.referer, cur.status_id 
				ORDER BY nbcontacts DESC, c.referer ASC
			 */
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) {
				$return = array();
				
				foreach($records as $r => $result){
					if(!array_key_exists($result['referer'], $return) || !is_array($return[$result['referer']])){
						$return[$result['referer']] = array();
					}
					$return[$result['referer']][$result['status_id']] = array( 	'nbcontacts' => $result['nbcontacts'],
																				'label' => $result['label'],
																				'color' => $result['color']
																		);
				}
				return $return;
			}
		
			return false;
		}
		
		# countContactsPerExitStateByProjectID
		
		public static function countContactsPerExitStateByProjectID($projectID, $date1 = '', $date2 = ''){
			
			global $wpdb;
			
			$query = "SELECT COUNT(cur.contact_id) as nbcontacts , cur.note
						FROM ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur 
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
							WHERE cur.status_id = ".MEO_RELATIONSHIP_STATE_EXIT_ID." 
								AND c.date_deleted IS NULL 
								AND c.project_id = ".$projectID;
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$query.= "	GROUP BY cur.note";
			
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records && is_array($records) && count($records) > 0) {
				$return = array();
			
				foreach($records as $r => $result){
					$return[$result['note']] = $result['nbcontacts'];
				}
				return $return;
			}
			
			return false;
		}
		
		# countContactsPerExitStateByProjectIDByUserID
		
		public static function countContactsPerExitStateByProjectIDByUserID($projectID, $courtierID, $date1 = '', $date2 = ''){
			
			global $wpdb;
			
			$query = "SELECT COUNT(cur.contact_id) as nbcontacts , cur.note
						FROM ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE ." cur
						LEFT JOIN ".$wpdb->prefix. MEO_CONTACTS_TABLE ." c ON cur.contact_id = c.id
							WHERE cur.status_id = ".MEO_RELATIONSHIP_STATE_EXIT_ID." 
								AND c.date_deleted IS NULL 
								AND c.project_id = ".$projectID."
								AND cur.user_id = ".$courtierID." ";
				
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
				
			$query.= "	GROUP BY cur.note";
			
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records && is_array($records) && count($records) > 0) {
				$return = array();
					
				foreach($records as $r => $result){
					$return[$result['note']] = $result['nbcontacts'];
				}
				return $return;
			}
				
			return false;
		}
}
