////////////
// FIELDS //
////////////

/* Shared functions / Tools */

	// Compare two dates, to mainly write it in an interval div
	function compareTwoDates(date1, date2){
		var returnnbOfDays = false;
		if(date1 != '' && date2 != ''){
			var data = {
					'action': 'meo_crm_relationship_datediff',
					'date1': date1,
					'date2': date2
				};
			jQuery.ajax({
					  type: 'POST',
					  url: meo_crm_relationship_ajax.ajax_url,
					  data: data,
					  async:false
					}).done(function(nbOfDays) {
						returnnbOfDays = nbOfDays;
					});
		}
		return returnnbOfDays;
	}
	
	// Look for previous and next state divs and interval divs
	function searchSiblingDivsToUpdateIntervals(currentDivjQueryObj, referalDate){
		
		// Is there a previous state?
		var previousIntervalDiv = jQuery(currentDivjQueryObj).prev();
		var previousState = jQuery(currentDivjQueryObj).prev().prev();
		
		if(jQuery(previousIntervalDiv).length && jQuery(previousState).length){
			
			var previousDate = jQuery(previousState).find('.a-state-date_updated').html();
			
			if(previousDate != ''){
				jQuery(previousIntervalDiv).html(compareTwoDates(previousDate, referalDate));
			}
		}
		
		// Is there a next state?
		var nextIntervalDiv = jQuery(currentDivjQueryObj).next();
		var nextState = jQuery(currentDivjQueryObj).next().next();
		
		if(jQuery(nextIntervalDiv).length && jQuery(nextState).length){

			var nextDate = jQuery(nextState).find('.a-state-date_updated').html();
			
			if(nextDate != ''){
				jQuery(nextIntervalDiv).html(compareTwoDates(nextDate, referalDate));
			}
		}
	}
	
	
/* Document ready functions */

	jQuery(document).ready(function() {
		
		// Make element editable
		jQuery(".contact-relationship-states span.editable").dblclick(
			function(e){
				clickToEdit(this);
		});
		
		// Make element editable
		jQuery("table#contact-relationship-interactions td.editable").dblclick(
			function(e){
				clickToEditInteraction(this);
		});
	});

	
/* Action functions */

	// To be available for new DOM object (created on the fly by jQuery)
	function clickToEdit(jQueryObj){
		
		var that = jQueryObj;
		if( ( jQuery(jQueryObj).attr("data-locked") && jQuery(jQueryObj).attr("data-locked") == "locked" ) || !jQuery(jQueryObj).hasClass("editable") ){
			return false;
		}
		
		jQuery(jQueryObj).attr("data-locked", "locked");
		
		var contact_id = jQuery(".contact-relationship-states").attr("data-contact");
		var user_id = jQuery(".contact-relationship-states").attr("data-user");
		var status_id = jQuery(jQueryObj).parent(".a-state").attr("data-status");
		
		jQuery(jQueryObj).removeClass('editable');
		var aStateField = jQuery(jQueryObj).attr("class");
		var initialValue = jQuery(jQueryObj).html();
		
		var htmlForm = '<form id="'+aStateField+'_'+contact_id+'-'+user_id+'-'+status_id+'">';
		
		htmlForm+= '<input type="text" name="fieldToSave" value="'+initialValue+'" class="fieldToSave" />';
		htmlForm+= '<input type="hidden" name="contact_id" value="'+contact_id+'" class="idToSave" />';
		htmlForm+= '<input type="hidden" name="user_id" value="'+user_id+'" class="idToSave" />';
		htmlForm+= '<input type="hidden" name="status_id" value="'+status_id+'" class="idToSave" id="status_id" />';
		
		// No save button for new contact state
		if(!jQuery(jQueryObj).attr("data-state") || (jQuery(jQueryObj).attr("data-state") && jQuery(jQueryObj).attr("data-state") != "new")){
			htmlForm+= '<i data-id="'+contact_id+'-'+user_id+'-'+status_id+'" class="fieldUpdater cursor-hand fa fa-check-square update-field" onclick="updateFieldRelContactState(\''+aStateField+'\', \''+contact_id+'-'+user_id+'-'+status_id+'\')" aria-hidden="true"></i>';
		}
		htmlForm+= '</form>';
		
		jQuery(jQueryObj).html(htmlForm);
		
		// Add datepicker for date field
		if(aStateField == 'a-state-date_updated'){
			jQuery('.fieldToSave').datepicker({dateFormat : 'dd-mm-yy'});
		}
	}
	
	// Update the contact edited field
	function updateFieldRelContactState(field, id){
		
		var formId = field+"_"+id;
		var contentToSave = jQuery("form#"+formId+" .fieldToSave").val();
		var contentToPrint = contentToSave;
		var status_id = jQuery("form#"+formId+" #status_id").val();
		
		if(field == 'a-state-date_updated'){
			// Update intervals if any
			searchSiblingDivsToUpdateIntervals(jQuery("div#"+id), contentToSave);
		}
		
		// Ajax code
		var data = {
			'action': 'update_relcontactstate_field',
			'contact_user_status': id,
			'fieldName': field,
			'fieldValue': contentToSave
		};
		jQuery.post(meo_crm_relationship_ajax.ajax_url, data, function(response) {});
		
		// Add class and remove form
		jQuery("div#"+id+" span."+field).html(contentToPrint);
		jQuery("div#"+id+" span."+field).addClass('editable');
		jQuery("div#"+id+" span."+field).attr('data-locked', '');
	}
	
	// Delete The last state only
	function meoDeleteContactState(contact_id, user_id, status_id){
		var data = {
				'action': 'meo_crm_relationship_deleteContactState',
				'contact_id': contact_id,
				'user_id': user_id,
				'status_id': status_id,
			};
			jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
				
				var divID = contact_id+'-'+user_id+'-'+status_id;
				
				// Delete/Reset div
				jQuery("#"+divID+" .a-state-bullet").css('background-color', meo_crm_relationship_ajax.rel_state_default_color);
				jQuery("#"+divID+" .a-state-note").html('').removeClass('editable');
				jQuery("#"+divID+" .a-state-date_updated").html('').removeClass('editable');
				jQuery("#"+divID+" .a-state-delete").html('');
				
				// Is there a previous state interval?
				var previousIntervalDiv = jQuery("#"+divID).prev();
				if(jQuery(previousIntervalDiv).length){
					jQuery(previousIntervalDiv).remove();
				}
				
				// Make the previous one, if not the first state, deletable
				var prevStatelDiv = jQuery("#"+divID).prev('.a-state');
				if(jQuery(prevStatelDiv).length && jQuery(prevStatelDiv).attr("data-status") != "0"){
					var prevStateRecord = jQuery(prevStatelDiv).attr("data-status");
					var deletableHTML = '<i class="delete-contact-status cursor-hand fa fa-minus-square" onclick="meoDeleteContactState('+contact_id+', '+user_id+', \''+prevStateRecord+'\');" aria-hidden="true"></i>';
					jQuery(prevStatelDiv).find('.a-state-delete').html(deletableHTML);
				}
				
				/* PROGRESSIVE */
				
				// Make the next state, if exists, not editable
				var nextStateDiv = jQuery("#"+divID).next('.a-state');
				if(jQuery(nextStateDiv).length){
					jQuery(nextStateDiv).find(".a-state-add").html('');
				}
				
				// Make the current state editable
				jQuery("#"+divID+" .a-state-add").html('<i onclick="meoMakeEditableContactState('+contact_id+', '+user_id+', '+status_id+');" class="fa fa-plus-square cursor-hand add-contact-status" aria-hidden="true"></i>').attr('data-locked', 'locked');
			});
	}
	
	// Make state editable
	function meoMakeEditableContactState(contact_id, user_id, status_id){
		var divID = contact_id+'-'+user_id+'-'+status_id;
		
		// https://www.sitepoint.com/jquery-todays-date-ddmmyyyy/
		var fullDate = new Date();
		var twoDigitMonth 	= (fullDate.getMonth()+1)+"";	if(twoDigitMonth.length==1)		twoDigitMonth="0" +twoDigitMonth;
		var twoDigitDate 	= fullDate.getDate()+"";		if(twoDigitDate.length==1)		twoDigitDate="0" +twoDigitDate;
		var twoDigitHours   = fullDate.getHours()+"";		if(twoDigitHours.length==1)		twoDigitHours="0" +twoDigitHours;
		var twoDigitMinutes = fullDate.getMinutes()+"";		if(twoDigitMinutes.length==1)	twoDigitMinutes="0" +twoDigitMinutes;
		var twoDigitSeconds = fullDate.getSeconds()+"";		if(twoDigitSeconds.length==1)	twoDigitSeconds="0" +twoDigitSeconds;
		
		var currentDate = twoDigitDate + "-" + twoDigitMonth + "-" + fullDate.getFullYear() + " " + twoDigitHours + ":" + twoDigitMinutes + ":" + twoDigitSeconds;
				
		var intialColor = jQuery("#"+divID+" .a-state-bullet").attr('data-color');
		jQuery("#"+divID+" .a-state-bullet").css('background-color', intialColor);
		jQuery("#"+divID+" .a-state-note").attr('data-locked', 'unlocked').attr('data-state', 'new').html('...Note...');
		jQuery("#"+divID+" .a-state-date_updated").attr('data-locked', 'unlocked').attr('data-state', 'new').html(currentDate);
		jQuery("#"+divID+" .a-state-add").html('<div class="rel-state-cancel-save"><i onclick="meoCancelContactState('+contact_id+', '+user_id+', \''+status_id+'\');" aria-hidden="true" class="cursor-hand fa fa-times-circle cancel-state"></i><i onclick="meoAddContactState('+contact_id+', '+user_id+', \''+status_id+'\');" aria-hidden="true" class="cursor-hand fa fa-check-square save-state"></i></div>');
		// Add interval
		var intervalHTML = '<div class="a-state-interval" id="interval-'+status_id+'"></div>';
		jQuery("#"+divID).before(intervalHTML);
		
		// Update intervals if any
		searchSiblingDivsToUpdateIntervals(jQuery("#"+divID), currentDate);
	}
	
	// Cancel the edition process on the state
	function meoCancelContactState(contact_id, user_id, status_id){
		var divID = contact_id+'-'+user_id+'-'+status_id;
		
		jQuery("#"+divID+" .a-state-bullet").css('background-color', meo_crm_relationship_ajax.rel_state_default_color);
		jQuery("#"+divID+" .a-state-note").attr('data-locked', 'locked').html('');
		jQuery("#"+divID+" .a-state-date_updated").attr('data-locked', 'locked').html('');
		jQuery("#"+divID+" .a-state-add").html('<i onclick="meoMakeEditableContactState('+contact_id+', '+user_id+', \''+status_id+'\');" class="fa fa-plus-square cursor-hand add-contact-status" aria-hidden="true"></i>').attr('data-locked', 'locked');
		
		// Is there a previous state interval?
		var previousIntervalDiv = jQuery("#"+divID).prev();
		
		if(jQuery(previousIntervalDiv).length){
			jQuery(previousIntervalDiv).remove();
		}
	}
	
	/* Add state after the last one only */
	function meoAddContactState(contact_id, user_id, status_id){
		
		var divID = contact_id+'-'+user_id+'-'+status_id;
		
		// Saving note and/or date (Try input fields first)
		// If no input field, the user probably clicked 'save' without editing anything, so try the html content
		var state_noteField = jQuery("#"+divID+" .a-state-note .fieldToSave");
		var state_dateField = jQuery("#"+divID+" .a-state-date_updated .fieldToSave");
		var state_note = '';
		var state_date = '';
		
		if(jQuery(state_noteField).length){
			state_note = jQuery(state_noteField).val();
		}
		else state_note = jQuery("#"+divID+" .a-state-note").html();
		
		if(jQuery(state_dateField).length){
			state_date = jQuery(state_dateField).val();
		}
		else state_date = jQuery("#"+divID+" .a-state-date_updated").html();
		
		
		var data = {	'action': 'meo_crm_relationship_addContactState',
						'contact_id': contact_id,
						'user_id': user_id,
						'status_id': status_id,
						'note' : state_note,
						'date_updated' : state_date
					};
			// Save content
			jQuery.post(meo_crm_relationship_ajax.ajax_url, data, function(response) {
				
				// Make the next state, if exists, editable
				var nextStateDiv = jQuery("#"+divID).next('.a-state');
				if(jQuery(nextStateDiv).length){
					var nextStateStatus = jQuery(nextStateDiv).attr("data-status");
					jQuery(nextStateDiv).find(".a-state-add").html('<i onclick="meoMakeEditableContactState('+contact_id+', '+user_id+', '+nextStateStatus+');" class="fa fa-plus-square cursor-hand add-contact-status" aria-hidden="true"></i>').attr('data-locked', 'locked');;
				}
				// Make the previous one, if not the first state, not deletable
				var prevStatelDiv = jQuery("#"+divID).prev().prev('.a-state');
				if(jQuery(prevStatelDiv).length && jQuery(prevStatelDiv).attr("data-status") != "0"){
					jQuery(prevStatelDiv).find('.a-state-delete').html('');
				}
				// Update intervals (normaly, only the previous one exists)
				searchSiblingDivsToUpdateIntervals(jQuery("#"+divID), state_date);
				// Make the current one, deletable and fields editable
				var deletableHTML = '<i onclick="meoDeleteContactState('+contact_id+', '+user_id+', '+status_id+');" class="fa fa-minus-square cursor-hand delete-contact-status" aria-hidden="true"></i>';
				// jQuery("#"+divID+" .a-state-add").removeClass("a-state-add").addClass("a-state-delete").html(deletableHTML);
				jQuery("#"+divID+" .a-state-delete").html(deletableHTML);
				jQuery("#"+divID+" .a-state-add").html('');
				jQuery("#"+divID+" .a-state-note").attr('data-locked', 'unlocked').attr('data-state', '').html(state_note);
				jQuery("#"+divID+" .a-state-date_updated").attr('data-locked', 'unlocked').attr('data-state', '').html(state_date);
			});
			/**/
	}
	
	
	// EXIT STATE
	
	
	function meoBringInBackContact(contact_id, user_id, status_id){
		
		var data = {
				'action': 'meo_crm_relationship_deleteContactState',
				'contact_id': contact_id,
				'user_id': user_id,
				'status_id': status_id,
			};
			jQuery.ajax({
				  type: 'POST',
				  url:  meo_crm_relationship_ajax.ajax_url,
				  data: data,
				  async:false
				}).done(function(response) {
					// Reload from the server side
					location.reload(true);
				});
		
	}
	
	function meoExitContact(contact_id, user_id, status_id){
		
		// Make all other states not editable not deletable
		jQuery(".contact-relationship-states .a-state").css("background", "#f4f4f4");
		jQuery(".contact-relationship-states .a-state *").removeClass("editable");
		jQuery(".contact-relationship-states .a-state").find('.a-state-delete').html('');
		jQuery(".contact-relationship-states .a-state").find('.a-state-add').html('');
		
		var divID = contact_id+'-'+user_id+'-'+status_id;
		
		// Saving note and/or date (Try input fields first)
		// If no input field, the user probably clicked 'save' without editing anything, so try the html content
		var state_noteField = jQuery("#"+divID+" .a-state-note #exit_status_options.fieldToSave option:selected");
		var state_dateField = jQuery("#"+divID+" .a-state-date_updated .fieldToSave");
		var state_note = jQuery(state_noteField).val();
		var state_date = '';
		
		if(jQuery(state_dateField).length){
			state_date = jQuery(state_dateField).val();
		}
		else state_date = jQuery("#"+divID+" .a-state-date_updated").html();
		
		var data = {	'action': 'meo_crm_relationship_addContactState',
						'contact_id': contact_id,
						'user_id': user_id,
						'status_id': status_id,
						'note' : state_note,
						'date_updated' : state_date
					};
			// Save content
			jQuery.post(meo_crm_relationship_ajax.ajax_url, data, function(response) {
				
				// Make all other states not editable not deletable
				jQuery(".contact-relationship-states .a-state").addClass("exited-contact");
				jQuery(".contact-relationship-states .a-state *").removeClass("editable").attr('data-locked', 'locked');
				jQuery(".contact-relationship-states .a-state").find('.a-state-delete').html('');
				jQuery(".contact-relationship-states .a-state").find('.a-state-add').html('');
				
				// Make the EXIT state removable and fields editable
				var bringableHTML = '<i onclick="meoBringInBackContact('+contact_id+', '+user_id+', '+status_id+');" class="fa fa-sign-in cursor-hand add-contact-status" aria-hidden="true"> REACTIVATE</i>';
				// jQuery("#"+divID+" .a-state-add").removeClass("a-state-add").addClass("a-state-delete").html(deletableHTML);
				jQuery("#"+divID+" .a-state-add").html(bringableHTML);
				jQuery("#"+divID+" .a-state-delete").html('');
				jQuery("#"+divID+" .a-state-note").attr('data-locked', 'unlocked').attr('data-state', '').html(state_note);
				jQuery("#"+divID+" .a-state-date_updated").attr('data-locked', 'unlocked').attr('data-state', '').html(state_date);
			});
			/**/
	}
	
	
	// REASSIGNMENT	
	
	
	// Insert the reassign state div when reassiging a courtier/user
	function meoInsertReassignState(contact_id, user_id, record_id, username){
		
		// https://www.sitepoint.com/jquery-todays-date-ddmmyyyy/
		var fullDate = new Date();
		var twoDigitMonth = (fullDate.getMonth()+1)+"";if(twoDigitMonth.length==1)	twoDigitMonth="0" +twoDigitMonth;
		var twoDigitDate = fullDate.getDate()+"";if(twoDigitDate.length==1)	twoDigitDate="0" +twoDigitDate;
		var twoDigitHours   = fullDate.getHours()+"";		if(twoDigitHours.length==1)		twoDigitHours="0" +twoDigitHours;
		var twoDigitMinutes = fullDate.getMinutes()+"";		if(twoDigitMinutes.length==1)	twoDigitMinutes="0" +twoDigitMinutes;
		var twoDigitSeconds = fullDate.getSeconds()+"";		if(twoDigitSeconds.length==1)	twoDigitSeconds="0" +twoDigitSeconds;
		
		var currentDate = twoDigitDate + "-" + twoDigitMonth + "-" + fullDate.getFullYear() + " " + twoDigitHours + ":" + twoDigitMinutes + ":" + twoDigitSeconds;

		// Create a reassign state
		var html = '<div class="a-state" data-record="'+record_id+'" data-status="'+meo_crm_relationship_ajax.rel_state_reasign_id+'" id="record_'+record_id+'">';
		html+= '<span class="a-state-label" data-locked="locked">Reassigned</span>';
		html+= '<span class="a-state-bullet" style="background-color:'+meo_crm_relationship_ajax.rel_state_default_color+';" data-color="'+meo_crm_relationship_ajax.rel_state_default_color+'"></span>';
		html+= '<span class="a-state-note" data-locked="locked">Reassigned<br/>>'+username+'</span>';
		html+= '<span class="a-state-date_updated" data-locked="locked">'+currentDate+'</span>';
		html+= '<span class="a-state-delete"></span>';
		html+= '<span class="a-state-add"></span>';
		html+= '</div>';
		
		jQuery('.contact-reasignment-states').append(html);
	}
	
	
	// INTERACTIONS
	
	
	// To be available for new DOM object (created on the fly by jQuery)
	function clickToEditInteraction(jQueryObj){
		
		var that = jQueryObj;
		if(jQuery(jQueryObj).attr("data-locked") && jQuery(jQueryObj).attr("data-locked") == "locked"){
			return false;
		}
		
		jQuery(jQueryObj).attr("data-locked", "locked");
		
		var record_id = jQuery(jQueryObj).parent("tr").attr("data-interaction");
		
		jQuery(jQueryObj).removeClass('editable');
		
		var anInterationField = jQuery(jQueryObj).attr("class");
		var initialValue = jQuery(jQueryObj).html();
		
		var htmlForm = '<form id="'+anInterationField+'_'+record_id+'">';
		
		htmlForm+= '<input type="text" name="fieldToSave" value="'+initialValue+'" class="fieldToSave" />';
		htmlForm+= '<input type="hidden" name="record_id" value="'+record_id+'" class="idToSave" id="record_id_field" />';
		
		// No save button for new contact state
		if(!jQuery(jQueryObj).parent("tr").attr("data-interaction") || (jQuery(jQueryObj).parent("tr").attr("data-interaction") && jQuery(jQueryObj).parent("tr").attr("data-interaction") != "new")){
			// htmlForm+= '<i data-id="'+contact_id+'-'+user_id+'-'+record_id+'" class="fieldUpdater cursor-hand fa fa-check-square update-field" onclick="updateFieldRelContactState(\''+aStateField+'\', \''+contact_id+'-'+user_id+'-'+record_id+'\')" aria-hidden="true"></i>';
		
			htmlForm+= '<i data-id="'+anInterationField+'-'+record_id+'" class="cursor-hand fa fa-check-square save-interaction fieldUpdater" onclick="updateInteractionField(\''+anInterationField+'\', \''+record_id+'\')" aria-hidden="true"></i>';
			htmlForm+= '<i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+initialValue+'\')" aria-hidden="true"></i>';
		}
		/**/
		htmlForm+= '</form>';
		
		jQuery(jQueryObj).html(htmlForm);
		
		// Add datepicker for date field
		if(anInterationField == 'date_updated'){
			jQuery('.fieldToSave').datepicker({dateFormat : 'dd-mm-yy'});
		}
	}
	
	
	// Delete interaction
	function meoDeleteContactInteraction(deleteBtnObj, record_id){
		var data = {
				'action': 'meo_crm_relationship_deleteContactInteraction',
				'record_id': record_id,
			};
			jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
				
				// Remove the current one #interaction_+record_id
				jQuery(deleteBtnObj).parents("tr").remove();
			});
	}
	
	// Update the contact relationship interaction field
	function updateInteractionField(field, id){
		
		var formId = field+"_"+id;
		var contentToSave = jQuery("form#"+formId+" .fieldToSave").val();
		var contentToPrint = contentToSave;
		var record_id = jQuery("form#"+formId+" #record_id_field").val();
		
		// Ajax code
		var data = {
			'action': 'update_relcontactinteraction_field',
			'record_id': id,
			'fieldName': field,
			'fieldValue': contentToSave
		};
		jQuery.post(meo_crm_relationship_ajax.ajax_url, data, function(response) {});
		
		// Add class and remove form
		jQuery("tr#interaction_"+record_id+" td."+field).html(contentToPrint);
		jQuery("tr#interaction_"+record_id+" td."+field).addClass('editable');
		jQuery("tr#interaction_"+record_id+" td."+field).attr('data-locked', '');
	}
	
	// Add Contact Interaction
	function meoAddContactInteraction(btnObj, contact_id, user_id){

		var trObj = jQuery(btnObj).parents('tr');
		
		// Saving note and/or date (Try input fields first)
		// If no input field, the user probably clicked 'save' without editing anything, so try the html content
		var interaction_noteField = jQuery(trObj).find("td.note .fieldToSave");
		var interaction_dateField = jQuery(trObj).find("td.date_updated .fieldToSave");
		var interaction_note = '';
		var interaction_date = '';
		
		// Note
		if(jQuery(interaction_noteField).length){
			interaction_note = jQuery(interaction_noteField).val();
		}
		else interaction_note = jQuery(trObj).find("td.note").html();
		
		// Date Updated
		if(jQuery(interaction_dateField).length){
			interaction_date = jQuery(interaction_dateField).val();
		}
		else interaction_date = jQuery(trObj).find("td.date_updated").html();
		
		var data = {	'action': 'meo_crm_relationship_addContactInteraction',
						'contact_id': contact_id,
						'user_id': user_id,
						'note' : interaction_note,
						'date_updated' : interaction_date
					};
			// Save content
			jQuery.post(meo_crm_relationship_ajax.ajax_url, data, function(CURid) {
				
				if(parseInt(CURid) > 0) {
					// Change div ID and interval id
					jQuery(trObj).attr('id', 'record_'+CURid);
					jQuery(trObj).attr('data-interaction', CURid);
					
					// https://www.sitepoint.com/jquery-todays-date-ddmmyyyy/
					var fullDate = new Date();
					var twoDigitMonth 	= (fullDate.getMonth()+1)+"";	if(twoDigitMonth.length==1)		twoDigitMonth="0" +twoDigitMonth;
					var twoDigitDate 	= fullDate.getDate()+"";		if(twoDigitDate.length==1)		twoDigitDate="0" +twoDigitDate;
					var twoDigitHours   = fullDate.getHours()+"";		if(twoDigitHours.length==1)		twoDigitHours="0" +twoDigitHours;
					var twoDigitMinutes = fullDate.getMinutes()+"";		if(twoDigitMinutes.length==1)	twoDigitMinutes="0" +twoDigitMinutes;
					var twoDigitSeconds = fullDate.getSeconds()+"";		if(twoDigitSeconds.length==1)	twoDigitSeconds="0" +twoDigitSeconds;
					
					var currentDate = twoDigitDate + "-" + twoDigitMonth + "-" + fullDate.getFullYear() + " " + twoDigitHours + ":" + twoDigitMinutes + ":" + twoDigitSeconds;
					
					
					// Create a next state add
					var html = '<tr data-interaction="new">';
					html+= '<td class="date_updated editable" data-locked="unlocked" onclick="clickToEditInteraction(this);">'+currentDate+'</td>';
					html+= '<td class="note editable" data-locked="unlocked" onclick="clickToEditInteraction(this);">...</td>';
					html+= '<td class="action"><span onclick="meoAddContactInteraction(this, '+contact_id+', '+user_id+');" class="cursor-hand"><i class="fa fa-plus-square" aria-hidden="true"></i></span></td>';
					// html+= '<span class="a-state-add"><i onclick="meoMakeEditableContactState('+contact_id+','+user_id+',\'new\');" class="fa fa-plus-square cursor-hand add-contact-status" aria-hidden="true"></i></span>';
					html+= '</tr>';
				
					jQuery( html ).insertAfter( trObj );
					
					
					// Make the current one, deletable and fields editable
					var deletableHTML = '<span class="an-interaction-delete"><i onclick="meoDeleteContactInteraction(this, '+CURid+');" class="fa fa-minus-square cursor-hand delete-contact-status" aria-hidden="true"></i></span>';
					jQuery(trObj).find("td.action").html(deletableHTML);
					jQuery(trObj).find("td.note").attr('data-locked', 'unlocked').html(interaction_note);
					jQuery(trObj).find("td.date_updated").attr('data-locked', 'unlocked').html(interaction_date);
				}
			});
	}