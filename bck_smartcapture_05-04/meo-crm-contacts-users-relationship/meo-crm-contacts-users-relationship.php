<?php 

/*
Plugin Name: MEO CRM Contacts Users Relationship
Description: Plugin to administrate relationship between Contacts and Users. Requires MEO CRM Contacts and MEO CRM Users plugins
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_CONTACTS_USERS_RELATIONSHIP_PLUGIN_SLUG', 		'meo-crm-contacts-users-relationship');
define('MEO_CONTACTS_USERS_RELATIONSHIP_TABLE', 			'meo_crm_contacts_users_relationship');			// Contacts Users Relationship db table
define('MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE', 		'meo_crm_contacts_users_relationship_status');	// Contacts Users Relationship Status db table
define('MEO_CONTACTS_USERS_INTERACTIONS_TABLE', 			'meo_crm_contacts_users_interactions');			// Contacts Users Interactions db table
define('MEO_RELATIONSHIP_STATE_DEFAULT_COLOR', 				'#dfdfdf');										// Contacts State default color if not reached yet
define('MEO_RELATIONSHIP_STATE_REASSIGNED_ID', 				9999);											// Contacts State default id for reassigned state
define('MEO_RELATIONSHIP_STATE_EXIT_COLOR', 				'#474747');										// Contacts State exit color
define('MEO_RELATIONSHIP_STATE_EXIT_ID', 					999999);										// Contacts State default id for reassigned state

# Globals
global $meo_crm_contacts_users_rel_db_version, $meo_crm_contacts_users_rel_exit_statuses;
$meo_crm_contacts_users_rel_db_version = '1.0';

$meo_crm_contacts_users_rel_exit_statuses = array(
				"Exit",
                "Contact impossible",
				"Client perdu",
				"R&eacute;servation annul&eacute;e",
				"Vente annul&eacute;e",
				"Pbm prix",
				"Pbm distribution",
				"Produit non adapt&eacute;",
				"Pbm situation",
				"Annulation r&eacute;servation",
				"Annulation vente",
				"Autre",
		);
/*
// TODO::Get project id too
function getUsersIdNameByRole($role){
	$args = array(
			'role'         => $role,
	);
	
	$usersObj = get_users( $args );
	
	$returnUsersArray = array();
	foreach($usersObj as $index => $userObj){
		$returnUsersArray[$userObj->ID] = $userObj->display_name;
	}
	return $returnUsersArray;
}


global $meo_crm_users_selector;


$meo_crm_users_selector = getUsersIdNameByRole('Courtier'); // wp_dropdown_users(array('echo' => false, 'class' => $class, 'selected' => 7));
*/

# Required Files
require_once ( $plugin_root . 'class-meo-crm-contacts-users-relationship.php' );
require_once ( $plugin_root . 'ajax/meo-crm-contacts-users-relationship-ajax.php' );


/*
 * Check for Dependencies :: MEO CRM Contacts / Users
 */
function meo_crm_contacts_users_relationship_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-contacts/meo-crm-contacts.php' ) && is_plugin_active( 'meo-crm-users/meo-crm-users.php' ) ) {
		$installed_dependencies = true;
	}
	
	if(!$installed_dependencies) {
		
		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Contacts and Users plugins before', 'meo-realestate').'</h3></div>';
		
		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Contacts and Users plugins before.', 'meo-realestate'), E_USER_ERROR);
		exit;
		
		
	}
	else {
		
		// Everything is fine
		
		global $wpdb, $meo_crm_contacts_users_rel_db_version;
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		$charset_collate = $wpdb->get_charset_collate();
		
		
		# Relationships
		$table_name = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE;
		
		// Create relationship table
		$sql = "CREATE TABLE $table_name (
					id int(11) NOT NULL,
					contact_id integer(11) NOT NULL,
					user_id integer(11) NOT NULL,
					date_updated datetime DEFAULT NULL,
					status_id integer(11) NULL,
					note TEXT DEFAULT NULL
					) $charset_collate;";

		dbDelta( $sql );
		
		$sql2 = "ALTER TABLE $table_name
  					ADD CONSTRAINT uq_".$table_name." UNIQUE(id);";
		
		dbDelta( $sql2 );
		
		
		# Status
		$table_name = $wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_STATUS_TABLE;
		
		// Create relationship status table
		$sql = "CREATE TABLE $table_name (
					id int(11) NOT NULL,
					project_id int(11) DEFAULT NULL,
					label VARCHAR(64) NOT NULL,
					status_order integer(11) NOT NULL,
					color VARCHAR(7) NOT NULL
					) $charset_collate;";
		
		dbDelta( $sql );
		
		$sql2 = "ALTER TABLE $table_name
		ADD CONSTRAINT uq_".$table_name." UNIQUE(id);";
		
		dbDelta( $sql2 );
		
		
		# Interactions
		$table_name = $wpdb->prefix . MEO_CONTACTS_USERS_INTERACTIONS_TABLE;
		
		// Create interactions table
		$sql = "CREATE TABLE $table_name (
					id int(11) NOT NULL AUTO_INCREMENT,
					contact_id integer(11) NOT NULL,
					user_id integer(11) NOT NULL,
					date_updated datetime DEFAULT NULL,
					note TEXT DEFAULT NULL
					) $charset_collate;";
		
		dbDelta( $sql );
		
		$sql2 = "ALTER TABLE $table_name
					ADD UNIQUE(id);";
		
		// Update db with the version info
		add_option( 'meo_crm_contacts_users_rel_db_version', $meo_crm_contacts_users_rel_db_version );
	}
	
}
register_activation_hook( __FILE__, 'meo_crm_contacts_users_relationship_activate' );



/* Actions


# Action from users availability page, to add extra HTML content
add_action('usersAvailabilityPageAdmin', 'user_emails_history_usersAvailabilityPageAdmin');

function user_emails_history_usersAvailabilityPageAdmin(){
	echo '<div id="user_email_history-ActivityPage-container"><div class="ueh-list-container"></div></div>';
}
 */

# Add Scripts and Styles

add_action( 'admin_enqueue_scripts', 'meo_crm_relationship_scripts_styles' );
add_action( 'wp_enqueue_scripts', 'meo_crm_relationship_scripts_styles' );

function meo_crm_relationship_scripts_styles() {

	#JS
	wp_register_script( 'meo_crm_relationship_js',  plugins_url('js/meo-crm-contacts-users-relationship.js', __FILE__), false, '1.0.0' );
	wp_enqueue_script( 'meo_crm_relationship_js' );

	wp_localize_script( 'meo_crm_relationship_js', 'meo_crm_relationship_ajax', array( 
																						'ajax_url' => admin_url( 'admin-ajax.php' ), 
																						'rel_state_default_color' => MEO_RELATIONSHIP_STATE_DEFAULT_COLOR,
																						'rel_state_reasign_id' => MEO_RELATIONSHIP_STATE_REASSIGNED_ID, 
																						'rel_state_exit_id' => MEO_RELATIONSHIP_STATE_EXIT_ID
																					)
	);
	
	# CSS
	wp_register_style( 'meo_crm_relationship_css',  plugins_url('css/meo-crm-contacts-users-relationship.css', __FILE__), false, '1.0.0' );
	wp_enqueue_style( 'meo_crm_relationship_css' );

}

# PROJECTS

# Add projects fields
add_action('projectsFormFields', 'meo_crm_relationship_status_projectsFields', 1);

# This function is also used on the Front, with the forced ID param
function meo_crm_relationship_status_projectsFields($forceProjectID = false){

	$data = array();
	$existingStatus = false;
	$saveButton = '';			// To save new status
	$projectID = 0;
	// Check if we already have a record to update, but check project id first
	
	# Backend admin
	if(isset($_GET['id']) && !empty($_GET['id'])){
		$projectID = $_GET['id'];
		$existingStatus = MeoCrmContactsUsersRelationship::getStatusByProjectID($projectID);
		
		# /!\ IF NO STATUS ID, WE CAN NOT USE AJAX FOR NEW STATUS, as project id will be empty /!\
		$saveButton = '<input type="button" onclick="meoSaveStatus(\'\');" class="button button-primary" value="Save Status">';
	}
	# Frontend ajax
	else if($forceProjectID && $forceProjectID > 0){
		$projectID = $forceProjectID;
		$existingStatus = MeoCrmContactsUsersRelationship::getStatusByProjectID($projectID);
		
		# /!\ IF NO STATUS ID, WE CAN NOT USE AJAX FOR NEW STATUS, as project id will be empty /!\
		$saveButton = '<input type="button" onclick="meoSaveStatus(\'\');" class="button button-primary" value="Save Status">';
	}
	else {
		echo '<table class="form-table meo-form-table">
				<tr class="form-field">
	                <th colspan="2">
	                    <label>
	                        PROJECT STATUS
	                    </label>
	                </th>
					<td style="text-align:left">
						Create and Save Project first, to be able to add a Project Status
					</td>
	            </tr>
			  <table>';
		return false;
	}

	echo '<table class="form-table meo-form-table">
			<tr class="form-field">
                <th colspan="2">
                    <label for="meo_crm_user_emails_history">
                        PROJECT STATUS
                    </label>
                </th>
            </tr>';
	
	if(!$forceProjectID){
			echo '
				<tr class="form-field">
	                <th>
	                    <label for="email_history">Status Management</label>
	                </th>';
	}
	else echo '<tr class="form-field">';
	
	echo '			<td class="status-management">
						<h3 class="existing-status">Existing Status</h3>
						<div id="project-existing-statuses">';
							if($existingStatus) {
								
								echo '';
								
								foreach($existingStatus as $k => $status){
		
									echo '<div id="status-form'.$status['id'].'" class="existing-status status">
											<label for="label" class="status-label">Label: </label><input type="text" name="label" id="label'.$status['id'].'" value="'.$status['label'].'" class="status-label" />
											<label for="color" class="status-color">Color: </label><input type="hidden" name="color" id="color'.$status['id'].'" value="'.$status['color'].'" />
											<div id="colorSelector'.$status['id'].'" class="colorSelector"><div style="background-color: '.$status['color'].';"></div></div>
											<label for="status_order" class="status-order">Order: </label><input type="text" name="status_order" id="status_order'.$status['id'].'" value="'.$status['status_order'].'" class="status-order" />
											<input type="button" onclick="meoSaveStatus('.$status['id'].');" class="button button-primary" value="Save Status">
											<input type="button" onclick="meoDeleteStatus('.$status['id'].');" class="button button-primary" value="Delete">
										  </div>
										<script>
											jQuery(document).ready(function(){ jQuery("#colorSelector'.$status['id'].'").ColorPicker({
													color: "'.$status['color'].'",
													onShow: function (colpkr'.$status['id'].') { jQuery(colpkr'.$status['id'].').fadeIn(500); return false; },
													onHide: function (colpkr'.$status['id'].') { jQuery(colpkr'.$status['id'].').fadeOut(500); return false; },
													onChange: function (hsb, hex, rgb) {
														jQuery("#colorSelector'.$status['id'].' div").css("backgroundColor", "#" + hex);
														jQuery("#color'.$status['id'].'").val("#" + hex);
													}
												});
											});
										</script>';
								} // /foreach
							} // /if existingstatus
	echo				'</div>
						<h3 class="new-status">New Status</h3>
						<div id="status-form" class="new-status status">
							<label for="label" class="status-label">Label: </label><input type="text" name="label" id="label" value="" class="status-label" />
							<label for="color" class="status-color">Color: </label><input type="hidden" name="color" id="color" value="" />
							<div id="colorSelector" class="colorSelector"><div style="background-color: #ffffff;"></div></div>
							<label for="status_order" class="status-order">Order: </label><input type="text" name="status_order" id="status_order" value="" class="status-order" />
							'.$saveButton.'
						</div>
						<script>
							jQuery(document).ready(function(){ jQuery("#colorSelector").ColorPicker({
										color: "#ffffff", 
										onShow: function (colpkr) { jQuery(colpkr).fadeIn(500); return false; },
										onHide: function (colpkr) { jQuery(colpkr).fadeOut(500); return false; },
										onChange: function (hsb, hex, rgb) { jQuery("#colorSelector div").css("backgroundColor", "#" + hex);
										jQuery("#color").val("#" + hex);
									}
								});
							});
							';
						if($saveButton != '' && $projectID != ''){
							echo 'function meoSaveStatus(id){
									var statusLabel = jQuery(\'input#label\'+id).val();
									var statusColor = jQuery(\'input#color\'+id).val();
									var statusOrder = jQuery(\'input#status_order\'+id).val();
									var subAction = \'update\';
									if(id == \'\'){subAction = \'add\';}
									
									var data = {
												\'action\': \'meo_crm_relationship_save_status\',
												\'subaction\': subAction,
												\'label\': statusLabel,
												\'color\': statusColor,
												\'order\': statusOrder,
												\'status_id\': id,
												\'project_id\': '.$projectID.',
											};
											jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) { 
												if(id == \'\' && response != \'\'){
													
													// Add element to the current statuses
													var newStatusId = response;
													var newStatus = \'<div id="status-form\'+newStatusId+\'" class="existing-status status">\';
													newStatus+= \'	<label for="label" class="status-label">Label: </label><input type="text" name="label" id="label\'+newStatusId+\'" value="\'+statusLabel+\'" class="status-label" />\';
													newStatus+= \'	<label for="color" class="status-color">Color: </label><input type="hidden" name="color" id="color\'+newStatusId+\'" value="\'+statusColor+\'" />\';
													newStatus+= \'	<div id="colorSelector\'+newStatusId+\'" class="colorSelector"><div style="background-color: \'+statusColor+\';"></div></div>\';
													newStatus+= \'	<label for="status_order" class="status-order">Order: </label><input type="text" name="status_order" id="status_order\'+newStatusId+\'" value="\'+statusOrder+\'" class="status-order" />\';
													newStatus+= \'	<input type="button" onclick="meoSaveStatus(\'+newStatusId+\');" class="button button-primary" value="Save Status">\';
													newStatus+= \'	<input type="button" onclick="meoDeleteStatus(\'+newStatusId+\');" class="button button-primary" value="Delete">\';
													newStatus+= \'</div>\';
														
													jQuery("#project-existing-statuses").append(newStatus);
													
													jQuery("#colorSelector"+newStatusId+"").ColorPicker({
														color: "#ffffff", 
														onShow: function (colpkr) { jQuery(colpkr).fadeIn(500); return false; },
														onHide: function (colpkr) { jQuery(colpkr).fadeOut(500); return false; },
														onChange: function (hsb, hex, rgb) { jQuery("#colorSelector div").css("backgroundColor", "#" + hex);
															jQuery("#color").val("#" + hex);
														}
													});
														
													jQuery(".new-status.status input#label").val("");
													jQuery(".new-status.status input#color").val("");
													jQuery(".new-status.status div#colorSelector").css("background-color", "#ffffff");
													jQuery(".new-status.status input#status_order").val("");
												}
											});
									return false;
								}
								function meoDeleteStatus(id){
									jQuery("<div></div>").appendTo("body")
									    .html(\'<div><h6 id="dialog-content">[Status Ref.:\'+id+\']<br/>Ce status va &ecirc;tre enlev&eacute; sur les Contacts le poss&egrave;dant.</h6></div>\')
										    .dialog({
										        modal: true,
										        title: "Please confirm DELETE STATUS",
										        zIndex: 10000,
										        autoOpen: true,
										        width: "400px",
										        resizable: false,
										        buttons: {
										            Yes: function () {
										            	var that = this;
										                
														var data = {
																		"action": "meo_crm_relationship_delete_status",
																		"status_id": id,
																		"project_id": '.$projectID.',
																	};
																	jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
																		jQuery("#status-form"+id).fadeOut(2000, function() {
																		    jQuery(this).html();
																		  });
																	});
														jQuery(this).dialog("close");
										                
										            },
										            No: function () {
										                jQuery(this).dialog("close");
										            }
										        },
										        close: function (event, ui) {
										            jQuery(this).remove();
										        }
										    });
								}';
						}
	echo					'</script>
	                </td>
				  </tr>
	         </table>';
}




# Add projects fields Validation
// add_filter('projectsFormValidate', 'meo_crm_relationship_status_projectsformvalidate', 2);

function meo_crm_relationship_status_projectsformvalidate($projectID) {

	$data = array();
	$data['project_id'] = $projectID;

	// Collect data
	$data['email_history'] = $_POST['email_history'];

	// Check if we already have a record to update, ot if insert a new one
	$results = MeoCrmUserEmailsHistory::getProjectRecordsByProjectID($projectID);

	if($results) {
		// Update
		MeoCrmUserEmailsHistory::updateProjectEmailHistory($data, array('project_id' => $projectID));
	}
	else {
		// Insert
		MeoCrmUserEmailsHistory::addProjectEmailHistory($data);
	}

	return $projectID;
}

########################
# EXTEND OTHER PLUGINS #
########################


	# MEO CRM Projects
	
	add_filter('meocrmprojects_getProjectData', 'meo_crm_relationship_getProjectData');
	
	# Add Projects Relationship Status data to the corresponding Project
	function meo_crm_relationship_getProjectData($project){
	
		if($project){
		
			$returnProject = array();
			
			# Get Analytics fields for the current Project
			$relationshipStatusProjectDetails = MeoCrmContactsUsersRelationship::getStatusByProjectID($project['id']);
			
			if(is_array($relationshipStatusProjectDetails)) {
				# Combine all fields
				$returnProject = array_merge($project, array('rel_status' => $relationshipStatusProjectDetails));
			}
			else $returnProject = $project;
				
			return $returnProject;
			
		}
		return false;
	}
	
	add_filter('meocrmcontacts_allowedFields', 'meo_crm_contacts_users_rel_contacts_courtiers_allowedFields', 10, 2);
	
	function meo_crm_contacts_users_rel_contacts_courtiers_allowedFields($allowedFields, $precision = ''){
		
		// Admin and Client
		if( current_user_can(CLIENT_ROLE) || current_user_can('administrator') ){
			$allowedFields['Courtier'] = 'courtier';				// Will show courtier's name
		}
		
		$allowedFields['Iteraction'] = 'last_interaction';			// Will show last interaction time
		
		if($precision == 'CSV_EXPORT'){
			$allowedFields['Status'] = 'current_status';			// Will print status label
		}
		else $allowedFields['Status'] = 'current_status_html';		// Will print div show status info
		
		return $allowedFields;
	}
	
	add_filter('meocrmcontacts_editablefields', 'meo_crm_contacts_users_rel_contacts_courtiers_editableField', 10, 2);
	
	function meo_crm_contacts_users_rel_contacts_courtiers_editableField($fields, $precision = ''){
		
		# Admin and Client can edit Courtier
		if( current_user_can(CLIENT_ROLE) || current_user_can('administrator') ){
			array_push($fields, 'courtier');
		}
		
		return $fields;
	}
	
	
	# Adding New Contact
	
	add_filter('meocrmcontacts_addContactFields', 'meo_crm_contacts_users_rel_contacts_courtiers_addContactField', 10, 2);
	
	function meo_crm_contacts_users_rel_contacts_courtiers_addContactField($fields, $precision = ''){
		array_push($fields, 'courtier');
		return $fields;
	}
	
	add_action('meo_crm_contacts_addContact_Submit',  'meo_crm_contacts_user_rel_submitted_form');
	
	function meo_crm_contacts_user_rel_submitted_form($alldata){
		// TODO::Check for projects without courtier
		if(		is_array($alldata) && array_key_exists('contact_id', $alldata) && $alldata['contact_id'] != '' 
				&& array_key_exists('courtier', $alldata) && $alldata['courtier'] != 'NULL' && $alldata['courtier'] != '' 
				&& array_key_exists('happeningNow', $alldata) && $alldata['happeningNow'] != ''
		){
			# TODO::Update Contact/User relationship table and check if relationship (eventually if feature is not enabled by default)
			$relationData = array('contact_id' => $alldata['contact_id'], 'user_id' => $alldata['courtier'], 'status_id' => 0, 'date_updated' => $alldata['happeningNow']);
			MeoCrmContactsUsersRelationship::addContactUserRelationship($relationData);
		}
		
	}
	
	
	# Deleting Contact
	
	add_action('meo_crm_contacts_delete_contact_by_id', 'meo_crm_contacts_user_rel_delete_contact_by_id');
	
	function meo_crm_contacts_user_rel_delete_contact_by_id($contact_id){
		
		MeoCrmContactsUsersRelationship::deleteContactUserRelationshipState(array('contact_id' => $contact_id));
		
	}
	
	
	add_filter('meocrmcontacts_getContactData', 'meo_crm_contacts_users_rel_getContactData', 11);
	
	function meo_crm_contacts_users_rel_getContactData($contact){
	
		# Get corresponding Courtier
		$courtier = MeoCrmContactsUsersRelationship::getUserByContactID($contact['id']);
	
		if($courtier){
			$courtier = reset($courtier);
			$contact['courtier'] = $courtier['courtier_name'];
			$contact['courtier_id'] = $courtier['id'];
		}
		else {
			# TODO::Check if it's valid to use client id instead
			/*
			$contact['courtier'] = '';
			$contact['courtier_id'] = '';
			*/
			$client = meo_crm_users_getClientDataById($contact['client_id']);
			$contact['courtier'] = $client['display_name'];
			$contact['courtier_id'] = $contact['client_id'];
		}
	
	
		# Relationship Status and States
			
		$checkProject = false;
		$checkContact = false;
		$contactExit  = false;
		$contactRelationshipData = array(); 	// Will contain Project Status and Contact States, mixed
		$contactUserReasignmentData = array(); 	// Will contain reasignment states only
		$contactCurrentStatus = ''; // Will contain the current/last status (except 9999)
		$contactCurrentStatusHTML = '<div class="status_table" style="background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.';" alt="" title=""></div>';
		
		// First contact is the first state
		$contactRelationshipData[0] = array(
				'record_id'		=> false,	// record id on MEO_CONTACTS_USERS_RELATIONSHIP_TABLE
				'status_id' 	=> 0,
				'label' 		=> $contact['pdf'],
				'color' 		=> MEO_RELATIONSHIP_STATE_DEFAULT_COLOR,
				'date_updated' 	=> strftime ('%d-%m-%Y %H:%M:%S', strtotime($contact['date_added'])),
				'note' 			=> $contact['note'],
				'interval' 		=> '',
				'delete' 		=> false, 	// To allow state deletion
				'add' 			=> false	// To allow state addition
		);
		$previousDate = strftime ('%d-%m-%Y', strtotime($contact['date_added']));
	
		# Relationship data for Contact's Project
		$projectRelStatusResults = MeoCrmContactsUsersRelationship::getStatusByProjectID($contact['project_id']);
		
		# If Project has status, we can manage them for the Contact
		if($projectRelStatusResults){
						
			$checkProject = true;
	
			# Relationship States data for Contact
			$contactRelStates = MeoCrmContactsUsersRelationship::getStatusByContactID($contact['id']);
			
			# Contact has no state, so project first status is addable
			if(!$contactRelStates){
				$totalContactStates = 0;
			}
			# If Contact has states, parse them against project status, then add the option to add Contact state
			//if($contactRelStates){
			else {
				
				# Remove the default state (the one when we stored the contact for the first time (status id 0)
				unset($contactRelStates[0]);
				
				$totalContactStates = count($contactRelStates);
				
				if($totalContactStates > 0){
					
					
					
					
					########################################################
					# Check Reasignment States, Exit State and remove them #
					########################################################
						
					foreach($contactRelStates as $ck => $contactRelState){
						
						$dateStateDB = $contactRelState['date_updated'];
						$dateState = strftime ('%d-%m-%Y %H-%M-%S', strtotime($dateStateDB)); //  %H:%M:%S
							
						# Reasignment
					
						if($contactRelState['status_id'] == MEO_RELATIONSHIP_STATE_REASSIGNED_ID){
								
							$currentState = array(
									'record_id' 	=> $contactRelState['record_id'],
									'status_id' 	=> MEO_RELATIONSHIP_STATE_REASSIGNED_ID,
									'label' 		=> 'Reassigned',
									'color' 		=> MEO_RELATIONSHIP_STATE_DEFAULT_COLOR,
									'date_updated' 	=> $dateState,
									'note' 			=> $contactRelState['note']
							);
								
							$contactUserReasignmentData[] = $currentState;
								
							# Remove the contact state for next loop
							unset($contactRelStates[$ck]);
							$totalContactStates--;
						}
					
						# Exit
					
						else if($contactRelState['status_id'] == MEO_RELATIONSHIP_STATE_EXIT_ID){
					
							$currentState = array(
									'record_id' 	=> $contactRelState['record_id'],
									'status_id' 	=> MEO_RELATIONSHIP_STATE_EXIT_ID,
									'label' 		=> 'EXIT',
									'color' 		=> MEO_RELATIONSHIP_STATE_EXIT_COLOR,
									'date_updated' 	=> $dateState,
									'note' 			=> $contactRelState['note'],
									'interval' 		=> '',
									'delete' 		=> false,
									'add'			=> false
							);
					
							$contactExit = $currentState;
								
							# Last/Current Contact state is EXIT
							$contactCurrentStatus = $currentState['label'];
							$contactCurrentStatusHTML = '<div class="status_table" style="background-color:'.$currentState['color'].';" alt="'.$currentState['label'].'" title="'.$currentState['label'].'"></div>';
					
							# Remove the contact state for next loop
							unset($contactRelStates[$ck]);
							$totalContactStates--;
						}
					}
					
					
					
					if($totalContactStates > 0){
					
						$checkContact = true;
					
					}
					
					
					
					
					
					// $checkContact = true;
				}
			}
				
				$projRelStatusIndex = 0;
				
				foreach($projectRelStatusResults as $k => $projectRelStatus){
					
					$projRelStatusIndex++;
				
					# Default value of the current state
					$currentState = array(
							'record_id' 	=> false,
							'status_id' 	=> $projectRelStatus['id'],
							'label' 		=> $projectRelStatus['label'],
							'color' 		=> $projectRelStatus['color'], // Previously was default color, but now handled on the front
							'date_updated' 	=> '',
							'note' 			=> '',
							'interval' 		=> '',
							'delete' 		=> false,
							'add' 			=> false
							);
					$dateState = '';
					$dateStateDB = '';
				
					# Contact has state(s)
					if($checkContact){
						
						# We still have Contact States to check
						if($totalContactStates > 0){
							
							########################################################
							# Check Reasignment States, Exit State and remove them #
							########################################################
							/*
							foreach($contactRelStates as $ck => $contactRelState){
								
								# Reasignment
								
								if($contactRelState['status_id'] == MEO_RELATIONSHIP_STATE_REASSIGNED_ID){
									
									$dateStateDB = $contactRelState['date_updated'];
									$dateState = strftime ('%d-%m-%Y %H-%M-%S', strtotime($dateStateDB)); //  %H:%M:%S
									
									$currentState = array(
											'record_id' 	=> $contactRelState['record_id'],
											'status_id' 	=> MEO_RELATIONSHIP_STATE_REASSIGNED_ID,
											'label' 		=> 'Reassigned',
											'color' 		=> MEO_RELATIONSHIP_STATE_DEFAULT_COLOR,
											'date_updated' 	=> $dateState,
											'note' 			=> $contactRelState['note']
									);
							
									$contactUserReasignmentData[] = $currentState;
							
									# Remove the contact state for next loop
									unset($contactRelStates[$ck]);
									$totalContactStates--;
								}
								
								# Exit
								
								else if($contactRelState['status_id'] == MEO_RELATIONSHIP_STATE_EXIT_ID){
										
									$currentState = array(
											'record_id' 	=> $contactRelState['record_id'],
											'status_id' 	=> MEO_RELATIONSHIP_STATE_EXIT_ID,
											'label' 		=> 'EXIT',
											'color' 		=> MEO_RELATIONSHIP_STATE_EXIT_COLOR,
											'date_updated' 	=> $dateState,
											'note' 			=> $contactRelState['note'],
											'interval' 		=> '',
											'delete' 		=> false,
											'add'			=> false
									);
								
									$contactExit = $currentState;
									
									# Last/Current Contact state is EXIT
									$contactCurrentStatus = $currentState['label'];
									$contactCurrentStatusHTML = '<div class="status_table" style="background-color:'.$currentState['color'].';" alt="'.$currentState['label'].'" title="'.$currentState['label'].'"></div>';									
										
									# Remove the contact state for next loop
									unset($contactRelStates[$ck]);
									$totalContactStates--;
								}
							}
							
							########################################################
							*/
							
							##########################
							# Check Project Statuses #
							##########################
							
							foreach($contactRelStates as $ck => $contactRelState){
								# Contact has current project status state
								if($projectRelStatus['id'] == $contactRelState['status_id']){
				
									$dateStateDB = $contactRelState['date_updated'];
									$dateState = strftime ('%d-%m-%Y', strtotime($dateStateDB)); //  %H:%M:%S
				
									$interval = date_diff(date_create($dateState), date_create($previousDate));
									$previousDate = $dateState;
				
									$currentState = array(
											'record_id' 	=> $contactRelState['record_id'],
											'status_id' 	=> $projectRelStatus['id'],
											'label' 		=> $projectRelStatus['label'],
											'color' 		=> $projectRelStatus['color'],
											'date_updated' 	=> $dateState,
											'note' 			=> $contactRelState['note'],
											'interval' 		=> $interval->format('%aj'),
											'delete' 		=> false,
											'add'			=> false
									);
				
									# Remove the contact state for next loop
									unset($contactRelStates[$ck]);
									$totalContactStates--;
				
									# We checked all the Contact States, so the current one is the last one (as they are ordered), and no EXIT
									if( $totalContactStates == 0 && !$contactExit) {
										# To only be able to delete the latest state on the front end
										$currentState['delete'] = true;
										$contactCurrentStatus = $contactRelState['label'];
										$contactCurrentStatusHTML = '<div class="status_table" style="background-color:'.$contactRelState['color'].';" alt="'.$contactRelState['label'].'" title="'.$contactRelState['label'].'"></div><span class="small-status">' . $contactRelState['label'] . '</span>';
									}
				
									break;
								} // /if($projectRelStatus['id'] == $contactRelState['id'])
																
							} // /foreach($contactRelStates as $ck => $contactRelState)
				
						} // /if($totalContactStates > 0){
						# No more Contact States to check
						else {
							$checkContact = false;
							$currentState['add'] = true; // The current state is the next state that the contact can get (be added to)
						}
							
					} // /if($checkContact)
					// Contact has no state
					else {
						
						// The first Project state has to be 'addable'
						if($projRelStatusIndex == 1){
							$currentState['record_id'] = 'new';
							$currentState['add'] = true;
						}
					}
				
					$contactRelationshipData[$k+1] = $currentState;
				} // /foreach($projectRelStatusResults as $k => $projectRelStatus)
					
				unset($projectRelStatusResults);
			//} // /if($contactRelStates)
				
			# Add the last status EXIT
			

			# Add relationship to Contact
			$contact['relationship'] 		= $contactRelationshipData;
			$contact['current_status'] 		= $contactCurrentStatus;
			$contact['current_status_html'] = $contactCurrentStatusHTML;
			$contact['interactions'] 		= MeoCrmContactsUsersRelationship::getInteractionsByContactID($contact['id']);
			$contact['reasignment'] 		= $contactUserReasignmentData;
			$contact['exit']				= $contactExit;
			
			$contact['last_interaction']	= false;
			if(array_key_exists('interactions', $contact) && is_array($contact['interactions']) && count($contact['interactions']) > 0){
				
				# Prepare DIV element containing interactions list
				$divInteractions = "";
				foreach($contact['interactions'] as $i =>$interaction){
					$divInteractions.= "<em class='em-date'>".$interaction['date_updated']."</em><em class='em-note'> - ".stripslashes($interaction['note'])."</em>";
				}
				$divInteractions.= "";
				
				$lastInteraction = end($contact['interactions']);
				$happeningNow = strftime ('%d-%m-%Y %H:%M:%S', strtotime(date('Y-m-d H:i:s')));
				$lastInteractionTime = strftime ('%d-%m-%Y %H:%M:%S', strtotime($lastInteraction['date_updated']));
				$interval = date_diff(date_create($happeningNow), date_create($lastInteractionTime));
				$contact['last_interaction'] = $interval->format('%aj') . ' <i class="fa fa-comments-o interactions-details" title="'.$divInteractions.'" aria-hidden="true"></i>';
			}
			
			unset($contactRelationshipData);
			unset($contactUserReasignmentData);
			
		} // / if($projectRelStatusResults)
		
		return $contact;
	}
	
	
	#########
	# FRONT #
	#########
	
	
	# Edit Contact page top part
	add_action('meo_crm_contacts_editContact_top', 'meo_crm_contacts_users_editContact_top');
	
	function meo_crm_contacts_users_editContact_top($contact){
		
	}
	
	# Edit Contact page bottom part
	add_action('meo_crm_contacts_editContact_bottom', 'meo_crm_contacts_users_editContact_bottom');
	
	function meo_crm_contacts_users_editContact_bottom($contact){
		
		meo_crm_contacts_users_printStatus($contact);
		meo_crm_contacts_users_printInteractions($contact);
	}
	
	# Edit Project Front page
	add_action('meo_crm_projects_front_edit_project_ajax', 'meo_crm_contacts_users_rel_front_edit_project');
	
	function meo_crm_contacts_users_rel_front_edit_project($project){
		meo_crm_relationship_status_projectsFields($project['id']);
	}
	
	###########################
	# Corresponding Functions #
	###########################
	
	
	function meo_crm_contacts_users_printStatus($contact){
	
		global $current_user, $meo_crm_contacts_users_rel_exit_statuses;
		
		if(!array_key_exists('courtier_id', $contact) || $contact['courtier_id'] == ''){
			$userID = $current_user->ID;
		}
		else $userID = $contact['courtier_id'];
	
		# Init
		$html = '';
		
		$exitContact = false;
		
		# Contact relationship states
		if(array_key_exists('relationship', $contact) && is_array($contact['relationship'])){
	
			$html.= '<div class="redband center">Status</div>
							<div class="contact-relationship-states" data-contact="'.$contact['id'].'" data-user="'.$userID.'">';
	
			foreach($contact['relationship'] as $ck => $contactState){
				# Init
				$locked = '';
				$editable = '';
				$bgColor = $contactState['color'];
				$stateClass = '';
					
				# First state (Can't change the date)
				if($contactState['status_id'] != 0){
					$editable = ' editable';
				}
					
				# Contact State not reached
				if($contactState['date_updated'] == ''){
					$locked = ' data-locked="locked"';
					$bgColor = MEO_RELATIONSHIP_STATE_DEFAULT_COLOR;
				}
				
				# Contact has exited
				if(array_key_exists('exit', $contact) && is_array($contact['exit'])){
					$editable = '';
					$locked = ' data-locked="locked"';
					$stateClass = ' exited-contact';
					$exitContact = true;
					$contactState['delete'] = false;
					$contactState['add'] = false;
				}
					
				# There is an interval set
				if($contactState['interval']){
					$html.= '<div class="a-state-interval" id="interval-'.$contactState['status_id'].'">'.$contactState['interval'].'</div>';
				}
					// id="record_'.$contactState['record_id'].'"
				$html.= '<div class="a-state'.$stateClass.'" data-record="'.$contactState['record_id'].'" data-status="'.$contactState['status_id'].'" id="'.$contact['id'].'-'.$contact['courtier_id'].'-'.$contactState['status_id'].'">
							<span class="a-state-label">'.$contactState['label'].'</span>
							<span class="a-state-bullet" style="background-color:'.$bgColor.';" data-color="'.$contactState['color'].'"></span>
							<span class="a-state-note'.$editable.'"'.$locked.'>'.$contactState['note'].'</span>
							<span class="a-state-date_updated'.$editable.'"'.$locked.'>'.$contactState['date_updated'].'</span>';
					
				# Other Contact States than the first one (time we recorded the contact) can be managed
				if($contactState['status_id'] != 0){
					if($contactState['date_updated'] != '' && $contactState['delete']) {
						$html.= '<span class="a-state-delete"><i onclick="meoDeleteContactState('.$contact['id'].','.$contact['courtier_id'].',\''.$contactState['status_id'].'\');" class="fa fa-minus-square cursor-hand delete-contact-status" aria-hidden="true"></i></span>';
					} else $html.= '<span class="a-state-delete"></span>';
	
					if($contactState['date_updated'] == '' && $contactState['add']) {
						$html.= '<span class="a-state-add"><i onclick="meoMakeEditableContactState('.$contact['id'].','.$contact['courtier_id'].',\''.$contactState['status_id'].'\');" class="fa fa-plus-square cursor-hand add-contact-status" aria-hidden="true"></i></span>';
					} else $html.= '<span class="a-state-add"></span>';
				}
					
				$html.= '</div>';
			} // /foreach
			
	
			$html.= '</div>';



			# Add the last status EXIT
			$html.= '<div class="a-state exit-state" data-record="false" data-status="'.MEO_RELATIONSHIP_STATE_EXIT_ID.'" id="'.$contact['id'].'-'.$contact['courtier_id'].'-'.MEO_RELATIONSHIP_STATE_EXIT_ID.'">
						<span class="a-state-label">EXIT</span>
						<span class="a-state-bullet" style="background-color:'.MEO_RELATIONSHIP_STATE_EXIT_COLOR.';" data-color="'.MEO_RELATIONSHIP_STATE_EXIT_COLOR.'"></span>
						<span class="a-state-note">
							<select id="exit_status_options" class="fieldToSave">';
							foreach($meo_crm_contacts_users_rel_exit_statuses as $exit_status){
								$exitSelected = '';
								if($exitContact){
									if($exit_status == htmlentities($contact['exit']['note'])){
										$exitSelected = ' selected';
									}
								}
								$html.= '<option value="'.$exit_status.'"'.$exitSelected.'>'.$exit_status.'</option>';
							}
			$html.= '		</select>
						</span>
						<span class="a-state-date_updated editable">'.date('d-m-Y H:i:s').'</span>
						<span class="a-state-delete">';
			if(!$exitContact){
				$html.=		'<i onclick="meoExitContact('.$contact['id'].','.$contact['courtier_id'].',\''.MEO_RELATIONSHIP_STATE_EXIT_ID.'\');" class="fa fa-sign-out cursor-hand delete-contact-status" aria-hidden="true"> EXIT</i>';
			}
				
			$html.=				'</span>
						<span class="a-state-add">';
			if($exitContact){
				$html.=	'<i onclick="meoBringInBackContact('.$contact['id'].','.$contact['courtier_id'].',\''.MEO_RELATIONSHIP_STATE_EXIT_ID.'\');" class="fa fa-sign-in cursor-hand add-contact-status" aria-hidden="true"> REACTIVATE</i>';
			}
				
			'</span>';
			
			$html.= '</div>
					<div class="clear"></div>';
			// /End EXIT state
			
			# Print Reasignments if any
			
			if(array_key_exists('reasignment', $contact) && is_array($contact['reasignment']) && count($contact['reasignment']) > 0 && current_user_can('administrator')){
				$html.= '<div class="redband center">Reasignment (Admin Only)</div>
							<div class="contact-reasignment-states" data-contact="'.$contact['id'].'" data-user="'.$userID.'">';
					
					foreach($contact['reasignment'] as $ck => $contactState){
					
					$html.= '<div class="a-state" data-record="'.$contactState['record_id'].'" data-status="'.$contactState['status_id'].'" id="'.$contact['id'].'-'.$contact['courtier_id'].'-'.$contactState['status_id'].'">
							<span class="a-state-label">'.$contactState['label'].'</span>
							<span class="a-state-bullet" style="background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.';" data-color="'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.'"></span>
							<span class="a-state-note">'.$contactState['note'].'</span>
							<span class="a-state-date_updated">'.$contactState['date_updated'].'</span>
							<span class="a-state-delete"></span>
							<span class="a-state-add"></span>';
						
					$html.= '</div>';
				} // /foreach
				
				$html.= '</div>
						<div class="clear"></div>';
			} // /if array reasignment
	
		} // /if array relationship
			
		echo $html;
	}
	
	function meo_crm_contacts_users_printInteractions($contact){

		global $current_user;
		
		# Init
		$html = '<div class="redband center">Interactions</div>
					<div class="contact-relationship-interactions" data-contact="'.$contact['id'].'">
						<table id="contact-relationship-interactions" cellspacing="0" cellpadding="0">
							<tr>
								<th class="header-date">Date</th>
								<th class="header-note">Note</th>
								<th></th>
							</tr>';
		
		# Interactions
		if(array_key_exists('interactions', $contact) && is_array($contact['interactions']) && count($contact['interactions']) > 0){
		
			$html.= '';
				
			foreach($contact['interactions'] as $ci => $contactInteraction){
		
				$html.= '<tr id="interaction_'.$contactInteraction['id'].'" data-interaction="'.$contactInteraction['id'].'">
							<td class="date_updated editable" data-locked="unlocked">'.$contactInteraction['date_updated'].'</td>
							<td class="note editable" data-locked="unlocked">'.$contactInteraction['note'].'</td>
							<td class="action"><span class="an-interaction-delete"><i onclick="meoDeleteContactInteraction(this, '.$contactInteraction['id'].');" class="fa fa-minus-square cursor-hand delete-contact-status" aria-hidden="true"></i></span></td>
						</tr>';
			}
				
		} // /if array key exists
		
		
		if(!array_key_exists('courtier_id', $contact) || $contact['courtier_id'] == ''){
			$userID = $current_user->ID;
		}
		else $userID = $contact['courtier_id'];
		
		$html.= '		<tr data-interaction="new">
							<td class="date_updated editable" data-locked="unlocked">'.date('d-m-Y H:i:s').'</td>
							<td class="note editable" data-locked="unlocked">...</td>
							<td class="action"><span onclick="meoAddContactInteraction(this, \''.$contact['id'].'\', \''.$userID.'\');" class="cursor-hand"><i class="fa fa-plus-square" aria-hidden="true"></i></span></td>
						</tr>
				</table>';
		
		echo $html;
	}
?>