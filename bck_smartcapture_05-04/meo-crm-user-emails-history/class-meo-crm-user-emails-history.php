<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoCrmUserEmailsHistory {
	
	# Build
	
		public function __construct() {
			
		}
		
	# Corresponding Database Table
	
		public static function activate() {
			
		}
		
		public static function deactivate() {
			// Do nothing
		}
	
	# Get Table Headers (const or private static array, when PHP5.6)
		
		public static function getTableHeaders (){
			// AllowedFields to be printed on the contacts table
			$allowedFields = array(	'availability', 'on_hold', 'last_send');
				
			$fields = array();
			foreach($allowedFields as $field) {
				$fields[ucwords(str_replace('_', ' ', $field))] = $field;
			}
			return $fields;
		}
		
	# Manage History in the Database
	
		public static function addEmailHistory ($data){
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE;
			
			$newID = $wpdb->insert( $table, $data );

			if($newID) { return $newID; }
			return false;
		}
		
		public static function updateEmailHistory ($data, $where){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE;
			
			$wpdb->update( $table, $data, $where );
		}
		
		public static function deleteEmailHistoryByIds ($userID, $contactID){
			
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE;
			
			return $wpdb->delete( $table, array( 'user_id' => $userID, 'contact_id' => $contactID ) );
		}
		
		public static function deleteEmailHistoryByUserId ($userID){
				
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE;
				
			return $wpdb->delete( $table, array( 'user_id' => $userID ) );
		}
		
		public static function deleteEmailHistoryByContactId ($contactID){
				
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE;
				
			return $wpdb->delete( $table, array( 'contact_id' => $contactID ) );
		}
	
	# Get Records
		
		#getRecords
		
		public static function getRecords (){
				
			global $wpdb;
				
			// Get Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " ORDER BY site_id ASC";
				
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) return $records;
			
			return false;
		}
		
		#getCountEmailsByRecord
		
		public static function getCountEmailsByRecord ($Record){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT COUNT(contact_id) AS nb_emails FROM ".$wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE." ueh
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_TABLE." c ON ueh.contact_id = c.id 
						WHERE ueh.user_id = ".$Record['user_id']." AND c.project_id = ".$Record['project_id']." 
								AND date_deleted IS NULL";
			
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return reset($records);
		
			return false;
		}
		
		#getEmailsHistoryByIDs
		
		public static function getEmailsHistoryByIDs ($UserID, $ProjectID){
		
			global $wpdb;
		
			$query = "SELECT c.id, c.last_name AS contact_lastname, c.first_name AS contact_firstname, ueh.date_sent AS date_sent FROM ".$wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE." ueh
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_TABLE." c ON ueh.contact_id = c.id 
						WHERE ueh.user_id = ".$UserID." AND c.project_id = ".$ProjectID." 
							ORDER BY date_sent DESC";
			
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return $records;
		
			return false;
		}
		
	# Get Users
	
		public static function getUsersByProjectID ($Project_ID){
			
			global $wpdb;
			
			// Get Users
			$query = "SELECT user_id FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " WHERE project_id=" . $Project_ID . " ORDER BY user_id ASC";
			
			$userIDs = $wpdb->get_results($query, ARRAY_A);
			
			if($userIDs){
				$include = array();
	        	foreach($userIDs as $k => $u){
	        		$include[] = $u['user_id'];
	        	}
	        	$users = get_users(array('include' => $include));
	        
	        	return $users;
			}
			
			return false;
		}
		
		public static function getUsers ($where = '', $returnType = OBJECT){
			
			global $wpdb;
			
			// Get all Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " " . $where . " ORDER BY id DESC";
			
			$all_contacts = $wpdb->get_results($query, $returnType);
			
			if($all_contacts){
			
				return $all_contacts;
			}
			
			return false;
		}
		
		##################
		# PROJECTS LEVEL #
		##################
		
			# Manage Project Availability
			
			# addProjectEmailHistory
			
			public static function addProjectEmailHistory ($data){
				
				global $wpdb;
					
				$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_PROJECTS_TABLE;
			
				$newID = $wpdb->insert( $table, $data );

				if($newID) { return $newID; }
				return false;
			}
			
			# updateProjectEmailHistory
			
			public static function updateProjectEmailHistory ($data, $where){
				
				global $wpdb;
			
				$table = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_PROJECTS_TABLE;
			
				$wpdb->update( $table, $data, $where );
			}
			
			# getProjectRecordsByProjectID
			
			public static function getProjectRecordsByProjectID ($ProjectID){
			
				global $wpdb;
			
				// Get Records
				$query = "SELECT * FROM " . $wpdb->prefix.MEO_USER_EMAILS_HISTORY_PROJECTS_TABLE . " WHERE project_id=".$ProjectID."";
			
				$records = $wpdb->get_results($query, ARRAY_A);
				
				if($records && is_array($records) && count($records) > 0) return $records;
			
				return false;
			}
}
