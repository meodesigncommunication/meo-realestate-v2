<?php 

function meo_crm_contacts_admin_page(){
	
	$allowedFields = meo_crm_contacts_getAllowedFields();
	$editableFields = meo_crm_contacts_getEditableFields();
	
	global $contactsTypes;
	
	// Get Projects
	$userProjects = meo_crm_projects_getProjects();
	
	if($userProjects){
		?>
		<div class="contactsMainContainer">
		
			<div class="wrap">
				<h2>MEO CRM Contacts List</h2>
			</div>
			
			<div class="redband">
				<div id="meo-crm-contacts-filter">
					<form>
						<select id="meo-crm-contacts-projectSelector" name="siteId">
							<option value="0">Select a Project</option>
							<?php 
	
							$contactsReady = false;
							$projectContacts = array();
							
							foreach($userProjects as $p => $project){
								
								# Init
								$selected = '';
								if(!$contactsReady){
	
									$projectContacts = meo_crm_contacts_getContactsByProjectID($project['id']);
									
									if($projectContacts && is_array($projectContacts) && count($projectContacts) > 0){
										$contactsReady = true;
										$selected = ' selected';
									}
								}
								
								echo '<option value="'.$project['id'].'"'.$selected.'>'.$project['name'].' ('.meo_crm_contacts_countContactsByProjectID($project['id']).')</option>';
								
							}?>
						</select>
					</form>
				</div>
				<div id="meo-crm-contacts-export">
					<a id="csv-export" href="#" title="CSV Export" data-precision="CSV_EXPORT"><i class="fa fa-file-text" aria-hidden="true"></i></a>
				</div>
			</div>
			
			<div id="google-map">
			    <div id="map"></div>
			</div>
			
			<table id="contactsTable" cellspacing="0" class="tablesorter" data-colspan="<?php echo count($allowedFields)+3; ?>" data-precision="ADMIN_LIST">
				<thead>
					<tr class="tableHeaders">
						<?php 
						foreach($allowedFields as $kField){
							echo '<th>'.ucwords(str_replace('_',' ',$kField)).'</th>';
						}
						?>
						<th class="center">Map</th>
						<th>Coor</th>
						<th></th>
					</tr>
				</thead>
				<tbody class="contactsListContainer"> 
				<?php 			
					foreach($projectContacts as $contact) {
						// Trigger a function for contact line
						$contact = meo_crm_contacts_prepareContactCellsFrontIndividual($contact);
						
						if($contact){
							
							// To see the contact on the map, only if there is a latitude and longitude
							$contactFullName = $contact['last_name'].' '.$contact['first_name'];
							$lat = $contact['latitude'];
							$lon = $contact['longitude'];
							

							$deletedUserClass = '';
							if(!is_null($contact['date_deleted'])){
								$deletedUserClass = ' warning-text';
							}
						?>
						<tr id="<?php echo $contact['id']; ?>" class="project-<?php echo $contact['project_id']; ?> tr_contact<?php echo $deletedUserClass; ?>" data-project="<?php echo $contact['project_id']; ?>" data-user="<?php if(array_key_exists('courtier_id', $contact)) echo $contact['courtier_id']; ?>">
							<?php 
								foreach($allowedFields as $kField => $field_name){
									
									$field_value = $contact[$field_name];
									$editable = ' editable';
									
									if($field_name == 'project_id') {
										$projectInfo = meo_crm_projects_getProjectInfo($field_value);
										$field_value = $projectInfo['name'];
										$editable = '';
									}
									else if($field_name == 'date_added') {
										$field_value = substr($field_value, 0, -9);
									}
									else if($field_name == 'type' && $field_value != ''){
										$field_value = '<i class="fa '.$contactsTypes[$field_value].' cursor-hand" aria-hidden="true" title="'.$field_value.'"></i>';
									}
										
									echo '<td class="'.$field_name.$editable.'">'.$field_value.'</td>';
								}
								
								echo '<td class="see_map center">';
								
								if($lat > 0 && $lon > 0) {
									echo '<i class="fa fa-globe cursor-hand" aria-hidden="true" title="See on the map" onclick="meo_crm_contacts_setMap('.$lat.', '.$lon.', \''.$contactFullName.'\');moveToTop(jQuery(this).parents(\'tr\'));"></i>';
								}
								
								echo '</td>';
							?>
							<td class="calc_coordinates center">
								<a href="#" class="" title="Recalculate position/coordinates" onclick="updateContactCoordinatesBackend(this);moveToTop(jQuery(this).parents('tr'));"><i class="fa fa-compass" aria-hidden="true"></i></a>
							</td>
							<td><a class="edit-contact" href="<?php echo home_url('/').MEO_CONTACT_SLUG; ?>/?id=<?php echo $contact['id']; ?>"><i class="fa fa-pencil"></i></a></td>
						</tr>
					<?php }
					} 
					?>
				</tbody>
			</table>
			
			<?php /*
			<div id="pager" class="pager>
				<form>
					<img src="<?php echo plugins_url().'/'.MEO_CONTACTS_PLUGIN_SLUG; ?>/img/first.png" class="first" />
					<img src="<?php echo plugins_url().'/'.MEO_CONTACTS_PLUGIN_SLUG; ?>/img/prev.png" class="prev" />
					<input type="text" class="pagedisplay"/>
					<img src="<?php echo plugins_url().'/'.MEO_CONTACTS_PLUGIN_SLUG; ?>/img/next.png" class="next" />
					<img src="<?php echo plugins_url().'/'.MEO_CONTACTS_PLUGIN_SLUG; ?>/img/last.png" class="last" />
					<select class="pagesize">
						<option value="10">>LIMIT</option>
						<option value="2">2 per page</option>
						<option value="5">5 per page</option>
						<option value="10">10 per page</option>
						
					</select>
				</form>
			</div>
			*/ ?>
			
			<div id="contacts_dump_container"></div>
		</div>
		
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJUW5npc6oqm-919njldenzPwnscUTLE&callback=meo_crm_contacts_initMap"></script>
		<?php 
	}
}
do_action('contactsPageAdmin');
?>