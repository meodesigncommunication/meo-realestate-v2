<?php 
/**
 * The template for displaying individual Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */


global $current_user;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access add contact with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}


# Precision will be used to get fields to add
$precision = 'ADD_CONTACT';

$allowedFields = meo_crm_contacts_getAddContactFields($precision);						// Contact fields and other plugins related
$allowedContactFieldsOnly = meo_crm_contacts_getAddContactFields($precision, false);	// Contact fields only

$emailIncorrect = false;

	# Check if form has been submitted and all fields are filled
	if(isset($_POST['save-add-contact'])){
		
		$formComplete = true;
		$data = array(); 	// Fields for the Contacts db only
		$alldata = array();	// All submitted fields.
		
		foreach ($allowedFields as $kField => $field) {
			
			$alldata[$field] = $_POST[$field];
			
			if(!isset($_POST[$field]) || $_POST[$field] == ''){
				if($field != 'note'){
					$formComplete = false;
					break;
				}
			}
			else {
				if($field == 'email'){
					# Verify email syntax
					if(is_email( $_POST[$field] )){
						# Verify if email already exists
						if(MeoCrmContacts::getContactsByEmailAndProjectID($_POST[$field], $_POST['project_id'])){
							$emailIncorrect = true;
							$formComplete = false;
						}
					}
					else {
						$emailIncorrect = true;
						$formComplete = false;
					}
				}
				# Building data to add the Contact
				if(in_array($field, $allowedContactFieldsOnly)){
					$data[$field] = $_POST[$field];
					
					if($field == 'country'){
						$data[$field] = MeoCrmCoreLocator::getCountryNameByCode( $_POST['country'], $_POST['language'] );
					}
				}
			}
		}
		
		# Form is complete
		if($formComplete){
			$happeningNow = date('Y-m-d H:i:s');
			
			$data['date_added'] = $happeningNow;
			$data['referer'] = MEO_CONTACTS_STATUS_MANUAL;
			
			# Find the client id based on the project id
			$clients = meo_crm_projects_getClientsByProjectID($_POST['project_id']);
			if($clients){
				$data['client_id'] = reset($clients);
			}
			
			# Add Contact and get his ID
			$data = apply_filters( 'meo_crm_contact_record_dataContact', $data );
			$newContactID = MeoCrmContacts::addContact($data);
			
			
			if($newContactID){
				
				# Building the PDF file
				$PDFpath = MEO_CRM_PROJECTS_FOLDER_DIR . $data['project_id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER;
				$PDFfile = $PDFpath . '/' . meo_crm_analytics_building_contact_pdf($data);
				
				if (file_exists($PDFfile)) {
					unlink($PDFfile);
				}
				try {
					# Get Project data
					$project = meo_crm_projects_getProjectInfo($data['project_id']);
					
					# Get the PDF Template to write on it
					$PDFtemplate = MEO_CRM_PROJECTS_FOLDER_DIR . $data['project_id'] . '/' . MEO_CRM_ANALYTICS_PDF_DIRNAME . '/' . $project['pdf_template'];
					
					$pdf = new MeoCrmContactPdf('en', $PDFtemplate);
					$pdf->generate($data, 0, 0);
					$pdf->Output($PDFfile, 'F');
				
				}
				catch (Exception $e) {
				
					echo 'Error generating PDF for ' . $potential_contact['analytics_id'] . '<br>';
					MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error generating PDF for " . print_r($data, true));
				
					continue;
				}
				

				$alldata['contact_id'] = $newContactID;
				$alldata['happeningNow'] = $happeningNow;
				
				do_action('meo_crm_contacts_addContact_Submit', $alldata);
				
				# Redirect to the contact edition page
				$url = home_url().'/'.MEO_CONTACT_SLUG.'/?id='.$newContactID;
				
				wp_redirect($url);
				exit;
			}
				
		}
	}
	
	get_header();
	?>
	<div id="primary" class="content-area meo-crm-contacts-front-container individual-contact" data-project="none">
		<main id="main" class="project-main" role="main">
			
				<div class="wrap">
					<h2 id="new-contact">Add Contact</h2>
					<em></em>
					<br/>
				</div>
				
				<form method="post" id="new-contact-form">
				<div class="portlet details contact-crm-template clear">
			
					<div class="portlet-body">
			
						<h4><span class="first-contact"></span></h4>
						
						<table class="table individual-contact" id="contactsTable" data-precision="<?php echo $precision; ?>">
							<tbody>
								<?php $field_index = 0;
								foreach ($allowedFields as $kField => $field) {
									
									$postedValue = '';
									$emptyFieldClass = '';
									if(isset($_POST[$field])){
										$postedValue = $_POST[$field];
										
										# Check warning fields
										if( ($_POST[$field] == '' && $field != 'note') || ($field == 'email' && $emailIncorrect) ){
											$emptyFieldClass = 'warningField';
										}
									}
									
									$value = '<input type="text" value="'.$postedValue.'" name="'. $field.'" id="'.$field.'" class="'.$emptyFieldClass.'" />';
									
									
									# Specific cases
									
									# EMAIL
									if($field == 'email'){

										$value.= ' <i class="fa fa-eye cursor-hand email-checker" aria-hidden="true" title="email ok" onclick="checkEmailAvailability(this);"></i><span class="emailCheckResult">';
										
										if($emailIncorrect){
											$value.= ' <i class="fa fa-exclamation-triangle" aria-hidden="true" title="Incorrect email format or existing email"></i>';
										}
										else if(isset($_POST['email']) && !$emailIncorrect) {
											$value.= ' <i class="fa fa-check" aria-hidden="true" title="email ok"></i>';
										}
										
										$value.= '</span>';
									}
									# TYPE
									else if($field == 'type'){
										
										$value = '<input type="hidden" value="'.MEO_CONTACTS_STATUS_MANUAL.'" name="type" id="type" /><i class="fa fa-hand-paper-o" aria-hidden="true" title="Manual"></i>';
									}
									# COUNTRY
									else if($field == 'country'){
										
										$value = MeoCrmCoreLocator::generateCountriesListHTMLDropDown(array('class' => $emptyFieldClass, 'selectedcountry' => $postedValue));
									}
									# LANGUAGE
									else if($field == 'language'){
										
										if($postedValue == ''){
											$postedValue = MeoCrmCoreLocator::DEFAULTLANG;
										}
										$value = MeoCrmCoreLocator::generateLanguagesListHTMLDropDown($postedValue, 'language', 'fieldToSave '.$emptyFieldClass);
									}
									# PROJECT ID
									else if($field == 'project_id'){
										
										# Create dropdown list of User's projects
										$userProjects = meo_crm_projects_getProjectsByUserID($current_user->ID);
										
										if($userProjects){
											$value = '<select id="project_id" name="project_id">';
											foreach($userProjects as $p => $project){
												$selected = '';
												if($project){
													if($postedValue == $project['id']) {
														$selected = ' selected';
													}
													$value.= '<option class="'.$project['id'].'" value="'.$project['id'].'"'.$selected.'>'.$project['name'].'</option>';
												}
											}
											$value.= '</select>';
										}
										
									}
									# COURTIER
									else if($field == 'courtier'){

										# Current user is admin: list all Courtiers under all Projects
										# Current user is client: list all Courtier under his Projects
										# Current user is courtier: himself
										
										if( current_user_can(COURTIER_ROLE) ){
											$value = '<input type="hidden" value="'.$current_user->ID.'" name="courtier" id="courtier" />'.$current_user->display_name;
										} else {
											$courtierProjects = meo_crm_projects_getProjectsByUserID($current_user->ID);
											
											if($courtierProjects){
												$selectedBoolean = false;
												$value = '<select id="courtier" name="courtier">';
												foreach($courtierProjects as $p => $project){
													if($project){
														# Grouping by Project (id will be used to interact with projects dropdown field)
														$value.= '<optgroup label="'.$project['name'].'" class="'.$project['id'].'">';
														$courtiers = meo_crm_users_getCourtiersByProjectID($project['id']);
														
														if($courtiers){
															foreach($courtiers as $i => $user){
																$selected = '';
																if($postedValue == $user['ID'] && !$selectedBoolean) {
																	$selected = ' selected';
																	$selectedBoolean = true;
																}
																$value.= '<option value="'.$user['ID'].'"'.$selected.'>'.$user['display_name'].'</option>';
															}
														}
														else {
															// No Courtier
															$value.= '<option value="NULL">-> No Courtier</option>';
														}
														
														$value.= '</optgroup>';
													}
												}
												$value.= '</select>';
											}
											else {
												// No Courtier
												$value = '<input type="hidden" value="NULL" name="courtier" id="courtier" />No Courtier';
											}
										}
										
									}
									
									if ($field_index % 2 == 0) {
										?><tr class="tr_contact" id="" data-project="" data-user=""><?php
									} ?>
									<th style="width: 16.6%"><?php echo ucwords(str_replace('_', ' ', $field)); ?></th>
									<td style="width: 33.3%" class="<?php echo $field; ?>"><?php echo $value; ?></td>
									<?php if ($field_index % 2 != 0) {
										?></tr><?php
									}
									$field_index++;
								}
								if ($field_index % 2 != 0) { ?>
										<th style="width: 16.6%">&nbsp;</th>
										<td style="width: 33.3%">&nbsp;</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						
					</div>
				</div>
			
				<div class="meo_crm_analytics_piwikvisitor redband">
					<input type="submit" value="Save" name="save-add-contact" id="save-add-contact" />
				</div>
				</form>
				<?php 
			?>
		</main><!-- .project-main -->
	
	
	</div><!-- .content-area -->
<?php 
get_footer(); ?>