<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
global $current_user, $contactsSearchFields;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	wp_die();
}
$role = '';
if(current_user_can('administrator')) { $role = 'administrator'; }
else if(current_user_can(CLIENT_ROLE)) { $role = 'client'; }
else if(current_user_can(COURTIER_ROLE)) { $role = 'courtier'; }
get_header();

$limitStart = 0;
$limitQty = MEO_CONTACTS_LIST_LIMIT;
$projectContacts = array();

		# Precision will be used to get Allowed/Editable fields, and passed to jQuery thanks to data-precision
		$precision = 'FRONT_LIST';
		
		$allowedFields = meo_crm_contacts_getAllowedFields($precision);
		$editableFields = meo_crm_contacts_getEditableFields($precision);
		
		
		
// Get Projects
$userProjects = meo_crm_projects_getProjects();
?>

<div id="primary" class="content-area meo-crm-contacts-front-container" data-role="<?php echo $role; ?>">
	<main id="main" class="project-main" role="main">
	
		<div id="contacts-search-container">
		
			<table id="contacts-search-criterias">
				<thead>
					<tr class="tableHeaders">
						<th>Keyword</th>
						<th>Project(s)</th>
						<th>Field(s)</th>
						<th></th>
						<th>Results</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="center"><input type="text" name="contacts-search-term" id="contacts-search-term" value="" placeholder="Keyword" /></td>
						<td>
							<ul id="meo-crm-contacts-projectSelector">
								<?php 
		
								$contactsReady = false;
								$projectContacts = array();
								
								foreach($userProjects as $p => $project){
									
									if($project){
										echo '<li><input type="checkbox" name="search-projects[]" class="search-projects" value="'.$project['id'].'" checked="checked" /> '.$project['name'];
									}
									
								}?>
							</li>
						</td>
						<td>
							<ul id="contacts-search-fields">
							<?php 
							foreach($contactsSearchFields as $field){
								echo '<li><input type="checkbox" name="search-fields[]" class="search-fields" value="'.$field.'" checked="checked" />'.ucwords(str_replace('_',' ',$field)).'</li>';
							}
							?>
							</ul>
						</td>
						<td><input type="" name="contacts-search-go" id="contacts-search-go" value="SEARCH" onclick="submitSearchContacts('reloadTable');" /></td>
						<td id="search-results-counter"></td>
					</tr>
				</tbody>			
			</table>
			
			<div class="redband" id="redband-export" style="display:none;">
				<div id="meo-crm-contacts-export">
					<span id="current_user_map"></span> &nbsp;
					<i class="fa fa-spinner fa-spin white" aria-hidden="true" id="map-loading" style="display:none"></i> &nbsp;
					<i class="fa fa-globe cursor-hand white" aria-hidden="true" title="See All Contacts on the map" onclick="submitSearchContacts('false'<?php // Set it to 'true' to avoid reloading the query, but check the js function first for more explanation ?>);"></i> &nbsp;
					<a id="csv-export-search" href="#" title="CSV Export" data-precision="CSV_EXPORT"><i class="fa fa-file-text" aria-hidden="true"></i></a>
				</div>
			</div>
			
			<div id="search-loading" style="display:none;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></div>
			
			<div id="google-map" style="display:none;">
			    <div id="map"></div>
			</div>
			
			<table id="contactsTable" cellspacing="0" class="tablesorter" style="display:none;">
				<thead>
					<tr class="tableHeaders">
						<th>Project</th>
						<?php 
						foreach($allowedFields as $kField => $fieldName){
							echo '<th>'.ucwords(str_replace('_',' ',$kField)).'</th>';
						}
						?>
						<th class="center">Map</th>
						<?php if(current_user_can('administrator')){ ?><th class="td-icon"></th><?php } ?>
						<th class="td-icon"></th>
						<th class="td-icon"></th>
						<?php if(current_user_can('administrator')){ ?><th class="td-icon"></th><?php } ?>
					</tr>
				</thead>
				<tbody class="contactsSearchContainer">
				</tbody>			
			</table>
		</div>
		
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJUW5npc6oqm-919njldenzPwnscUTLE"></script>
		
		<?php // else MeoCrmCoreTools::meo_crm_core_403(); ?>
	</main>
</div>
<?php get_footer(); ?>