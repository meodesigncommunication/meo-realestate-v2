<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
global $current_user, $contactsTypes;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	wp_die();
}
$role = '';
if(current_user_can('administrator')) { $role = 'administrator'; }
else if(current_user_can(CLIENT_ROLE)) { $role = 'client'; }
else if(current_user_can(COURTIER_ROLE)) { $role = 'courtier'; }
get_header();

$limitStart = 0;
$limitQty = MEO_CONTACTS_LIST_LIMIT;
$projectContacts = array();
$currentProjectID = 0;
?>

<div id="primary" class="content-area meo-crm-contacts-front-container" data-role="<?php echo $role; ?>">
	<main id="main" class="project-main" role="main">
		<?php 
		# Precision will be used to get Allowed/Editable fields, and passed to jQuery thanks to data-precision
		$precision = 'FRONT_LIST';
		
		$allowedFields = meo_crm_contacts_getAllowedFields($precision);
		$editableFields = meo_crm_contacts_getEditableFields($precision);
		
		// Get Projects
		$userProjects = meo_crm_projects_getProjects();
		
		if($userProjects){ 
		
		?>
			<div class="wrap">
				<div id="meo-crm-contacts-add">
					<a id="add-contact" href="<?php echo home_url().'/'.MEO_ADD_CONTACT_SLUG; ?>" title="Add Contact"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Contact</a>
				</div>
			</div>
			<div class="redband">
				<div id="meo-crm-contacts-filter">
					<form>
						<select id="meo-crm-contacts-projectSelector" name="siteId" data-initstart="<?php echo $limitStart; ?>" data-initlimit="<?php echo $limitQty;?>">
							<option value="0" class="0">Select a Project</option>
							<?php 
	
							$contactsReady = false;
							$projectContacts = array();
							
							foreach($userProjects as $p => $project){
								
								if($project){
									# Init
									$selected = '';
									if(!$contactsReady){
		
										$projectContacts = meo_crm_contacts_getContactsByProjectID($project['id'], $limitStart, $limitQty);
										$currentProjectID = $project['id'];
										
										if($projectContacts && is_array($projectContacts) && count($projectContacts) > 0){

											$limitStart = count($projectContacts); // For load more link, max will be the limit number
											$contactsReady = true;
											$selected = ' selected';
										}
									}
									
									$nbContacts = meo_crm_contacts_countContactsByUserIDandProjectID($current_user->ID, $project['id']);
									$nbContactsText = '';
									$disabled = '';
									if($nbContacts == 0){
										$disabled = ' disabled';
									}
									else $nbContactsText = ' (<span>'.$nbContacts.'</span>)';
									
									echo '<option value="'.$project['id'].'" class="'.$project['id'].'"'.$selected.''.$disabled.'>'.$project['name'].''.$nbContactsText.'</option>';
								}
								
							}?>
						</select>
					</form>
				</div>
				<div id="meo-crm-contacts-export">
					<span id="current_user_map"></span> 
					<i class="fa fa-spinner fa-spin white" aria-hidden="true" id="map-loading" style="display:none"></i> &nbsp;
					<i class="fa fa-globe cursor-hand white" aria-hidden="true" title="See All Contacts on the map" onclick="meo_crm_contacts_initMapAllDisplayed();"></i> &nbsp;
					<a id="csv-export" href="#" title="CSV Export" data-precision="CSV_EXPORT"><i class="fa fa-file-text" aria-hidden="true"></i></a>
				</div>
			</div>
			
			<div id="google-map">
			    <div id="map"></div>
			</div>
			
			<table id="contactsTable" cellspacing="0" class="tablesorter" data-colspan="<?php echo count($allowedFields)+3; ?>" data-precision="CSV_EXPORT">
				<thead>
					<tr class="tableHeaders">
						<?php 
						foreach($allowedFields as $kField => $fieldName){
							echo '<th>'.ucwords(str_replace('_',' ',$kField)).'</th>';
						}
						?>
						<th class="center">Map</th>
						<?php if(current_user_can('administrator')){ ?><th class="td-icon"></th><?php } ?>
						<th class="td-icon"></th>
						<th class="td-icon"></th>
						<?php if(current_user_can('administrator')){ ?><th class="td-icon"></th><?php } ?>
					</tr>
				</thead>
				<tbody class="contactsListContainer"> 
				<?php if($projectContacts && is_array($projectContacts) && count($projectContacts) > 0){
					
						foreach($projectContacts as $contact) {
							
							if($contact){
								
								// To see the contact on the map, only if there is a latitude and longitude
								$contactFullName = $contact['last_name'].' '.$contact['first_name'];
								$lat = $contact['latitude'];
								$lon = $contact['longitude'];
								
								$deletedUserClass = '';
								if(!is_null($contact['date_deleted'])){
									$deletedUserClass = ' warning-text';
								}
							?>
							<tr id="<?php echo $contact['id']; ?>" class="project-<?php echo $contact['project_id']; ?> tr_contact<?php echo $deletedUserClass; ?>" data-project="<?php echo $contact['project_id']; ?>" data-user="<?php if(array_key_exists('courtier_id', $contact)) echo $contact['courtier_id']; ?>">
								<?php 
									# Collected Fields
									foreach($allowedFields as $kField => $field_name){
										
										$editable = '';
										$extraClass = '';
										if(in_array($field_name, $editableFields)) $editable = ' editable';
										
										$field_value = array_key_exists($field_name, $contact) ? $contact[$field_name] : '';
										
										if($field_name == 'type' && $field_value != ''){
											$field_value = '<i class="fa '.$contactsTypes[$field_value].'" aria-hidden="true" title="'.$field_value.'"></i>';
										}
										else if($field_name == 'date_added' && $field_value != ''){
												$field_value = substr($field_value, 0, -9);
										}
										else if($field_name == 'last_interaction' && $field_value != ''){
											$extraClass = ' center';
										}
											
										echo '<td class="'.$field_name.$editable.$extraClass.'">'.$field_value.'</td>';
									}
									
									# Map / Coordinates / Edition
									
									echo '<td class="see_map center">';
									
									if($lat > 0 && $lon > 0) {
										echo '<i class="fa fa-globe cursor-hand" aria-hidden="true" title="See on the map" onclick="meo_crm_contacts_setMap('.$lat.', '.$lon.', \''.$contactFullName.'\');moveToTop(jQuery(this).parents(\'tr\'));"></i>';
									}
									
									echo '</td>';
									
									if(current_user_can('administrator')){
										echo '<td class="calc_coordinates center">
												<a href="#" class="" title="Recalculate position/coordinates" onclick="updateContactsCoordinates(this);moveToTop(jQuery(this).parents(\'tr\'));"><i class="fa fa-compass" aria-hidden="true"></i></a>
											</td>';
									}
								?>
								<td><a class="edit-contact" href="<?php echo home_url('/').MEO_CONTACT_SLUG; ?>/?id=<?php echo $contact['id']; ?>" target="_blank"><i class="fa fa-pencil"></i></a></td>
								<td class="delete-activate-contact">
									<?php 
									if(current_user_can('administrator')){
										if(is_null($contact['date_deleted'])){
										?>
											<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em>
										<?php 
										}
										else {
										?>
											<em class=""><i class="fa fa-refresh cursor-hand" aria-hidden="true" onclick="updateActivateContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em>
										<?php 
										}
									}
									else if(current_user_can(CLIENT_ROLE) && is_null($contact['date_deleted'])){
										?>
											<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em>
										<?php 
									}
									?>
								</td>
								<?php 
								if(current_user_can('administrator')){
								?>
								<td><em class="warning-text"><i class="fa fa-trash-o cursor-hand" aria-hidden="true" onclick="deleteContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em></td>
								<?php 
								}
								?>
							</tr>
						<?php }
							unset($contact);
						}
						
					}
					?>
				</tbody>
			</table>
			<?php 
			# Test if we need to hide the page or not on landing
			$style = '';
			$countContacts = meo_crm_contacts_countContactsByUserIDandProjectID($current_user->ID, $currentProjectID);
			
			if($limitQty >= $countContacts){
				$style = ' style="display:none;"';
			}
			?>
			
			<div class="redband center" id="pager"<?php echo $style; ?>><i id="load-more" class="fa fa-user-plus white cursor-hand" aria-hidden="true" data-start="<?php echo $limitStart; ?>" data-limit="<?php echo $limitQty; ?>" onclick="contactsListLoadMore(this);"> Load  <?php echo $limitQty; ?> More</i> - <em>(Viewing <span class="paginator"><?php echo $limitQty.' of <span>'.$countContacts.'</span>'; ?></span>)</em> | <i id="load-all" class="fa fa-users white cursor-hand" aria-hidden="true" onclick="contactsListAll(this);"> View All <em>(may take time)</em></i></div>
			
			<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJUW5npc6oqm-919njldenzPwnscUTLE&callback=meo_crm_contacts_initMap"></script>
			<?php 
			unset($projectContacts);
		} // /if userprojects
		else MeoCrmCoreTools::meo_crm_core_403();
		?>
	</main>
</div>
<?php get_footer(); ?>