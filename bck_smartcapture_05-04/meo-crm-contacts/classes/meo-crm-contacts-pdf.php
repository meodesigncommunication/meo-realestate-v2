<?php

class MeoCrmContactPdf extends FPDI {
	const PDF_ENCODING = 'ISO-8859-1';
	const FONT = 'Helvetica';

	var $language = 'fr';
	var $template = null;
    var $project = '';

	public function __construct($lang, $template) {
		parent::__construct();

		$locales = array(
			'fr' => array("fr_CH", "fr_FR", "fr_FR.UTF-8", "fr_FR@euro" ),
			'en' => array("en_GB", "en_GB.UTF-8", "en_US", "en_US.UTF-8" ),
			'de' => array("de_CH", "de_DE", "de_DE.UTF-8", "de_DE@euro" )
		);

		if (array_key_exists($lang, $locales)) {
			$this->language = $lang;
		}

		$locale = $locales[$this->language];
		setlocale(LC_CTYPE, $locale);

		$this->template = $template;

		$this->SetCreator("MEO Real Estate");
		$this->SetKeywords("MEO Real Estate");
	}

	public function generate($data, $interactions, $relationstates) {
		// Currently only have a French template
		$language_pages = array(
			'fr' => 1,
			'en' => 1,
			'de' => 1
		);

        if(isset($data['project']) && !empty($data['project']))
        {
            $this->project = $data['project'];
        }

		$pagecount = $this->setSourceFile($this->template);
		$tplidx = $this->importPage($language_pages[$this->language]);

		$this->AddPage();
		$this->useTemplate($tplidx, 0, 0);
		$this->addContactDetails($data);
		$this->addMap($data);
		$this->showGraphs($data);
		if(is_array($interactions) && count($interactions) > 0) {
        	$this->addInteractions($interactions);
		}
		if(is_array($relationstates) && count($relationstates) > 0) {
			$this->addRelationstates($relationstates);
		}
	}

	private function addContactDetails($data) {
		$this->SetFont(self::FONT, '', 21);
		$this->SetTextColor(189, 189, 189); // #bdbdbd
		$this->SetXY(123, 24.396 - 4.85);
		$fullname = $this->convertCharset($data['first_name'] . ' ' . $data['last_name']);
		$fullname = $this->uppercaseString($fullname);
		$this->Cell(0, 10, $fullname, 0, 0, 'R');

		$this->SetXY(123, 33.286 - 4.85);
		$this->Cell(0, 10, $this->convertCharset($data['phone']), 0, 0, 'R');


		$this->SetFont(self::FONT, 'B', 14);
		$this->SetXY(83.256, 43.032);
		$this->SetTextColor(110, 110, 110); // #6e6e6e
		if(array_key_exists('submit_time', $data)){
			$this->Cell(0, 10, $this->convertCharset($data['submit_time']));
		}

		$this->SetFont(self::FONT, 'B', 12);
		$this->SetXY(50.33, 59.693);
		$this->Cell(0, 10, $this->convertCharset($data['first_name']));

		$this->SetXY(146.756, 59.693);
		$this->Cell(0, 10, $this->convertCharset($data['last_name']));

		$fields = array(
			'address'  => array('x' =>  50.33, 'y' =>  75.130 - 4.85),
			'postcode' => array('x' =>  50.33, 'y' =>  85.714 - 4.85),
			'country'  => array('x' =>  50.33, 'y' =>  96.298 - 4.85),
			'email'    => array('x' =>  50.33, 'y' => 106.882 - 4.85),
			'referrer' => array('x' =>  50.33, 'y' => 116.189 - 4.85),
			'address2' => array('x' => 146.756, 'y' =>  75.130 - 4.85),
			'city'     => array('x' => 146.756, 'y' =>  85.714 - 4.85)

		);

		$this->SetFont(self::FONT, '', 12);
		foreach ($fields as $field => $coords) {
			if(array_key_exists($field, $data)){
				$this->SetXY($coords['x'], $coords['y']);
				$this->Cell(0, 10, $this->convertCharset($data[$field]));
			}
		}

		$this->SetTextColor(255, 255, 255); // #ffffff
		$this->SetXY(146.756, 96.298 - 4.85);
		$this->Cell(0, 10, $this->convertCharset($data['phone']));
	}

	private function addMap($data) {
		$images_left  =  12.75;
		$images_top   = 122.16;
		$image_width  = 184.50;
		$image_height =  45.78;

		$map_url = 'http://maps.googleapis.com/maps/api/staticmap?center=' . urlencode($data['address']) . '+' . urlencode($data['postcode']) . '+' . urlencode($data['city']) . ',' . urlencode($data['country']) .
		                                                       '&markers=' . urlencode($data['address']) . '+' . urlencode($data['postcode']) . '+' . urlencode($data['city']) . ',' . urlencode($data['country']) .
		                                                       '&zoom=13&size=1140x150&maptype=roadmap&scale=2';

		$this->Image($map_url, $images_left, $images_top, $image_width, $image_height, 'png');
	}

	private function showGraphs($data) {
		$this->SetTextColor(110, 110, 110); // #6e6e6e
		$this->SetFont(self::FONT, '', 12);

		if(isset($data['pages_viewed'])){
			$this->showPageStats($data['pages_viewed'], 108.38, 223.71);
		}
		if(isset($data['lots_viewed'])){
			$this->showLotsViewed($data['lots_viewed'],  12.75, 174.99);
		}
		if(array_key_exists('lots_downloaded', $data)){
			$this->showDownloadedLots($data['lots_downloaded']);
		}
		if(isset($data['lot_sizes'])){
			$this->showLotSizes($data['lot_sizes'], 108.38, 174.99);
		}
	}

	private function showPageStats($pages, $box_left, $box_top) {
		if (empty($pages)) {
			return;
		}
		$max_bar_width = 37.45;
		$label_offset_x = 2.12;
		$first_bar_offset_x = 35;
		$first_bar_offset_y = 9.95;
		$bar_height = 4.42;
		$bar_gap_y = 2.82;
		$label_gap_x = 1.5;

		$this->SetDrawColor(10, 176, 182); // #0AB0B6
		$this->SetFillColor(10, 176, 182);

		$index = 0;
		$first_width = null;

		foreach ($pages as $page) {
			if ($index == 0) {
				$first_width = empty($page->sum_time_spent) ? 1 : $page->sum_time_spent;
			}
			if ($index > 4) {
				break;
			}

			$label = trim($page->label);

			$this->SetXY($box_left + $label_offset_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y));
			$this->Cell(0, $bar_height, $this->convertCharset($label));

			$bar_width = $page->sum_time_spent / $first_width * $max_bar_width;
			$this->Rect($box_left + $first_bar_offset_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y), $bar_width, $bar_height, 'FD');

			$duration = gmdate("i:s", $page->sum_time_spent);
			$this->SetXY($box_left + $first_bar_offset_x + $bar_width + $label_gap_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y));
			$this->Cell(0, $bar_height, $duration);

			$index++;
		}
	}

	private function showLotsViewed($pages, $box_left, $box_top) {
		if (empty($pages)) {
			return;
		}
		$max_bar_width = 37.45;
		$label_offset_x = 2.12;
		$first_bar_offset_x = 32;
		$first_bar_offset_y = 9.95;
		$bar_height = 4.42;
		$bar_gap_y = 2.82;
		$label_gap_x = 1.5;

		$this->SetDrawColor(10, 176, 182); // #0AB0B6
		$this->SetFillColor(10, 176, 182);

		$index = 0;
		$first_width = null;

        if (!empty($pages) && count($pages) > 0) {
            $first_width = null;
            foreach ($pages as $lot) {
                if ($lot && is_object($lot) && isset($lot->label)) {

                    if ($first_width === null) {
                        $first_width = empty($lot->sum_time_spent) ? 1 : $lot->sum_time_spent;
                    }

                    $pos_x = $box_left + $label_offset_x;
                    $pos_y = $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y);

                    $label = $lot->label . ' - ' . $lot->nb_rooms;
                    $duration = gmdate("i:s", $lot->sum_time_spent);
                    $width_duration_graph = round( $lot->sum_time_spent / $first_width * 100 / 2.5);

                    $this->SetXY($pos_x, $pos_y);
                    $this->Cell(0, $bar_height, $this->convertCharset($label));

                    $this->Rect($box_left + $first_bar_offset_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y), $width_duration_graph, $bar_height, 'FD');

                    $this->SetXY($box_left + $first_bar_offset_x + $width_duration_graph + $label_gap_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y));
                    $this->Cell(0, $bar_height, $duration);

                    $box_top += ($bar_height*1.6);
                }
            }
        }
	}

	private function showDownloadedLots($lots_downloaded) {
		$index = 0;
		$bar_height = 4.42; // Supposition, as it was missing
		$seen = array();
		if(is_array($lots_downloaded)){
			foreach ($lots_downloaded as $lot_code) {
				if ($index > 4) {
					break;
				}
				if (array_key_exists($lot_code, $seen) && $seen[$lot_code]) {
					continue;
				}
	
				$this->SetXY(14.87, 235.87 + $index * 7.24);
				$this->Cell(0, $bar_height, $this->convertCharset(htmlentities($lot_code)));
	
				$seen[$lot_code] = 1;
	
				$index++;
			}
		}
	}

	private function showLotSizes($lot_sizes, $box_left, $box_top) {
		$max_bar_width = 37.45;
		$label_offset_x = 2.12;
		$first_bar_offset_x = 15;
		$first_bar_offset_y = 9.95;
		$bar_height = 4.42;
		$bar_gap_y = 2.82;
		$label_gap_x = 1.5;

		$this->SetDrawColor(10, 176, 182); // #0AB0B6
		$this->SetFillColor(10, 176, 182);

		$index = 0;

        $first_width = null;
        foreach ($lot_sizes as $size) {

            if ($first_width === null) {
                $first_width = empty($size->sum_time_spent) ? 1 : $size->sum_time_spent;
            }

            $pos_y = $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y);
            $pos_x = $box_left + $label_offset_x;
            $label = $size->label . 'p.';
            $duration = gmdate("i:s", $size->sum_time_spent);
            $width_duration_graph = round( $size->sum_time_spent / $first_width * 100 / 2.5);

            $this->SetXY($pos_x, $pos_y);
            $this->Cell(0, $bar_height, $this->convertCharset($label));

            $this->Rect($box_left + $first_bar_offset_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y), $width_duration_graph, $bar_height, 'FD');

            $this->SetXY($box_left + $first_bar_offset_x + $width_duration_graph + $label_gap_x, $box_top + $first_bar_offset_y + $index * ($bar_height + $bar_gap_y));
            $this->Cell(0, $bar_height, $duration);

            $index++;

        }
	}

	private function convertCharset($string) {
		
		if(empty($string) || $string == '') return $string;
		
		$string = preg_replace("|<br ?/?>|i", "\n", $string);
		$string = preg_replace("|^[\r\n]+|", "", $string);
		$string = preg_replace("|[\r\n]+|", "\n", $string);
		
		return iconv('UTF-8', self::PDF_ENCODING.'//TRANSLIT', $this->unhtmlentities($string));
	}

	private function uppercaseString($string) {
		$result = $string;

		// Need to do in two steps.  Doing without ISO-8859-1 will strip
		// accented characters.  Doing without UTF will leave the lowercase
		if (mb_detect_encoding($string, self::PDF_ENCODING) !== false) {
			$result = mb_strtoupper($result, self::PDF_ENCODING);
		}
		if (mb_detect_encoding($result, 'UTF-8', true) !== false) {
			$result = mb_strtoupper($result,'UTF-8');
		}
		$result = strtoupper($result);
		return $result;
	}

	private function getHtmlTranslationTable() {
		$trans = get_html_translation_table(HTML_ENTITIES);
		$trans = array_flip ($trans);

		$trans['&sbquo;']  = ',';   // Single Low-9 Quotation Mark
		$trans['&fnof;']   = '';    // Latin Small Letter F With Hook
		$trans['&bdquo;']  = '';    // Double Low-9 Quotation Mark
		$trans['&hellip;'] = '...'; // Horizontal Ellipsis
		$trans['&dagger;'] = '';    // Dagger
		$trans['&Dagger;'] = '';    // Double Dagger
		$trans['&circ;']   = '^';   // Modifier Letter Circumflex Accent
		$trans['&permil;'] = '';    // Per Mille Sign
		$trans['&Scaron;'] = 'S';   // Latin Capital Letter S With Caron
		$trans['&lsaquo;'] = '<';   // Single Left-Pointing Angle Quotation Mark
		$trans['&OElig;']  = 'OE';  // Latin Capital Ligature OE
		$trans['&lsquo;']  = "'";   // Left Single Quotation Mark
		$trans['&rsquo;']  = "'";   // Right Single Quotation Mark
		$trans['&ldquo;']  = '"';   // Left Double Quotation Mark
		$trans['&rdquo;']  = '"';   // Right Double Quotation Mark
		$trans['&bull;']   = '-';   // Bullet
		$trans['&ndash;']  = '-';   // En Dash
		$trans['&mdash;']  = '-';   // Em Dash
		$trans['&tilde;']  = '~';   // Small Tilde
		$trans['&trade;']  = 'TM';  // Trade Mark Sign
		$trans['&scaron;'] = 's';   // Latin Small Letter S With Caron
		$trans['&rsaquo;'] = '>';   // Single Right-Pointing Angle Quotation Mark
		$trans['&oelig;']  = 'oe';  // Latin Small Ligature OE
		$trans['&Yuml;']   = 'Y';   // Latin Capital Letter Y With Diaeresis
		$trans['&nbsp;']   = ' ';   // non-breaking space
		$trans["&amp;"]    = "&";
		$trans["&lt;"]     = "<";
		$trans["&gt;"]     = ">";
		$trans["&laquo;"]  = "�";
		$trans["&raquo;"]  = "�";
		$trans["&para;"]   = "�";
		$trans["&euro;"]   = '�';
		$trans["&copy;"]   = "�";
		$trans["&reg;"]    = "�";
		$trans["&plusmn;"] = "�";
		$trans["&tilde;"]  = "~";
		$trans["&circ;"]   = "^";
		$trans["&quot;"]   = '"';
		$trans["&permil;"] = "?";
		$trans["�"]        = "'";
		$trans["�"]        = '-';


		// Reverse WordPress convert_chars() function
		$trans['&#8364;']  = $trans["&euro;"];
		$trans['&#8218;']  = $trans['&sbquo;'];
		$trans['&#402;']   = $trans['&fnof;'];
		$trans['&#8222;']  = $trans['&bdquo;'];
		$trans['&#8230;']  = $trans['&hellip;'];
		$trans['&#8224;']  = $trans['&dagger;'];
		$trans['&#8225;']  = $trans['&Dagger;'];
		$trans['&#710;']   = $trans['&circ;'];
		$trans['&#8240;']  = $trans['&permil;'];
		$trans['&#352;']   = $trans['&Scaron;'];
		$trans['&#8249;']  = $trans['&lsaquo;'];
		$trans['&#338;']   = $trans['&OElig;'];
		$trans['&#381;']   = 'Z'; // �
		$trans['&#8216;']  = $trans['&lsquo;'];
		$trans['&#8217;']  = $trans['&rsquo;'];
		$trans['&#8220;']  = $trans['&ldquo;'];
		$trans['&#8221;']  = $trans['&rdquo;'];
		$trans['&#8226;']  = $trans['&bull;'];
		$trans['&#8211;']  = $trans['&ndash;'];
		$trans['&#8212;']  = $trans['&mdash;'];
		$trans['&#732;']   = $trans['&tilde;'];
		$trans['&#8482;']  = $trans['&trade;'];
		$trans['&#353;']   = $trans['&scaron;'];
		$trans['&#8250;']  = $trans['&rsaquo;'];
		$trans['&#339;']   = $trans['&oelig;'];
		$trans['&#382;']   = 'z'; // �
		$trans['&#376;']   = $trans['&Yuml;'];

		return $trans;
	}

	private function unhtmlentities($string)  {
		$trans_tbl = $this->getHtmlTranslationTable();
		$ret = strtr ($string, $trans_tbl);
		// preg_replace ( mixed $pattern , mixed $replacement , mixed $subject [, int $limit = -1 [, int &$count ]] )
		// return preg_replace('/&#(\d+);/me', "chr('\\1')",$ret);
		// preg_replace_callback ( mixed $pattern , callable $callback , mixed $subject [, int $limit = -1 [, int &$count ]] )
		return $ret;
		$return = preg_replace_callback(
					'/&#(\d+);/m',
					function($matches){
						return "chr('\\1')";
					},
					$ret
				);
	}
        
        private function addInteractions($interactions)
        {
            $this->AddPage();
            $this->SetFont(self::FONT, '', 21);
            $this->SetTextColor(189, 189, 189); // #bdbdbd
            $coordY = $this->GetY();
            $this->SetY($coordY+10);
            $this->Cell(0,10,'Interactions',0,0,'L');
            $this->SetY($coordY+30);            
            foreach($interactions as $interaction)
            {
                $this->SetFont(self::FONT, '', 10);
                $this->SetTextColor(110, 110, 110); // #bdbdbd
                $this->Cell(25,5,$interaction->date_added,0,0,'L');
                $this->ln();
                $this->SetTextColor(140, 140, 140); // #bdbdbd
                $this->Cell(0,8,$interaction->description,0,0,'L');
                $this->ln();
                $this->ln();
            }
        }
        
        private function addRelationstates($relationstates)
        {
        	$this->AddPage();
        	$this->SetFont(self::FONT, '', 21);
        	$this->SetTextColor(189, 189, 189); // #bdbdbd
        	$coordY = $this->GetY();
        	$this->SetY($coordY+10);
        	$this->Cell(0,10,'Stade(s) de la relation',0,0,'L');
        	$this->SetY($coordY+30);
        	foreach($relationstates as $relationstate)
        	{
        		$this->SetFont(self::FONT, '', 10);
        		$this->SetTextColor(110, 110, 110); // #bdbdbd
        		$this->Cell(25,5,$relationstate->date_added,0,0,'L');
        		$this->ln();
        		$this->SetTextColor(140, 140, 140); // #bdbdbd
        		$this->Cell(0,8,$relationstate->state,0,0,'L');
        		$this->ln();
        		$this->ln();
        	}
        }
}
