<?php // Doc : http://developer.piwik.org/2.x/api-reference/reporting-api

function meo_crm_analytics_getPiwikChartsConfig($idSite, $period = "day", $date = "today") {
	

	// period=range
	// date=2016-05-10,2016-05-17
	
	
	$piwikAnalyticsMasterArray = array (
			/*
			"UserCountryMap" => array(
			
					"info" => array(
							"title" => 'UserCountryMap',
							"description" => '',
					),
					"moduleToWidgetize" => 'UserCountryMap',
					"actionToWidgetize" => 'visitorMap',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "",
			),
			*/
			"UserCountry" => array(
					
					"info" => array(
							"title" => 'Visits Per Country',
							"description" => '',
					),
					"moduleToWidgetize" => 'UserCountry',
					"actionToWidgetize" => 'getCountry',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "",
			),
			
			"getWebsites" => array(
						
					"info" => array(
							"title" => 'Referer / Origin',
							"description" => '',
					),
					"moduleToWidgetize" => 'Referrers',
					"actionToWidgetize" => 'getWebsites',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "",
			),
			
			"Actions" => array(
						
					"info" => array(
							"title" => 'Visited pages',
							"description" => '',
					),
					"moduleToWidgetize" => 'Actions',
					"actionToWidgetize" => 'getPageUrls',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "",
			),
			
			"VisitorInterest" => array(
			
					"info" => array(
							"title" => 'Visits duration',
							"description" => '',
					),
					"moduleToWidgetize" => 'VisitorInterest',
					"actionToWidgetize" => 'getNumberOfVisitsPerVisitDuration',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "graphPie",
			),
			
			"getNumberOfVisitsPerPage" => array(
						
					"info" => array(
							"title" => 'Pages per visit',
							"description" => '',
					),
					"moduleToWidgetize" => 'VisitorInterest',
					"actionToWidgetize" => 'getNumberOfVisitsPerPage',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "graphPie",
			),
			
			"UserSettings" => array(
						
					"info" => array(
							"title" => 'Mobile VS Desktop',
							"description" => '',
					),
					"moduleToWidgetize" => 'UserSettings',
					"actionToWidgetize" => 'getMobileVsDesktop',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "graphPie",
			),
			
			"Referrers" => array(
			
					"info" => array(
							"title" => 'Keywords',
							"description" => '',
					),
					"moduleToWidgetize" => 'Referrers',
					"actionToWidgetize" => 'getKeywords',
					"idSite" 			=> $idSite,
					"period" 			=> $period,
					"date" 				=> $date,
					"viewDataTable" 	=> "",
			),
						
			
			#############
			# GEOGRAPHY #
			#############
			
			# Country / Continent Table
			
			
			# City / Country
			
			
			##############
			# TECHNOLOGY #
			##############

			# Device
			
			###########
			# CONTENT #
			###########
			
			# Page
			
			
			
	);
	
	return $piwikAnalyticsMasterArray;
}

?>