<?php 

/*
Plugin Name: MEO CRM Analytics
Description: Plugin to visualize Google Analytics statistics. Requires MEO CRM Core and MEO CRM Projects plugins
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_ANALYTICS_PLUGIN_ROOT', 		$plugin_root);
define('MEO_ANALYTICS_PLUGIN_SLUG', 		'meo-crm-analytics');
define('MEO_ANALYTICS_SLUG', 				'meo-crm-analytics');			// Analytics page slug (Frontend)
define('MEO_ANALYTICS_TABLE', 				'meo_crm_analytics');			// Analytics db table
define('MEO_ANALYTICS_CONTACTPDF_FOLDER',	'pdfs');						// Contacts PDF folder name under the corresponding project id

define('MEO_ANALYTICS_SERVER', 				'piwik.meoanalytics.com');
# Smart Capture Legacy : but still adapted						// Email PDF page slug (Frontend)
define('MEO_ANALYTICS_SCHEDULE_TABLE', 		'meo_crm_analytics_schedule');			// Email scheduler db table
define('MEO_CRM_ANALYTICS_EMAIL_CONTENT', 	"Bonjour,"."\n"."
											Veuillez trouver ci-joint le fiche de contact SmartCapture pour {CLIENT_FIRST_NAME} {CLIENT_SURNAME}."."\n"."
											<a href='{CLIENT_ADMIN_URL}'>{CLIENT_ADMIN_URL}</a>."."\n"."
											Avec nos meilleures salutations,"."\n"."										
											L'&eacute;quipe MEO");

/* FRONT PAGES */
define('MEO_CRM_ANALYTICS_GOOGLE_TPL_FRONT', 	'../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/meo-crm-analytics-google-view.php');
define('MEO_ANALYTICS_GOOGLE_SLUG', 			'google-analytics');
define('MEO_CRM_ANALYTICS_PIWIK_TPL_FRONT', 	'../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/meo-crm-analytics-piwik-view.php');
define('MEO_ANALYTICS_PIWIK_SLUG', 				'piwik-analytics');
define('MEO_CRM_ANALYTICS_PROCONSTA_TPL_FRONT', '../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/meo-crm-analytics-project-contact-status.php');
define('MEO_ANALYTICS_PROCONSTA_SLUG', 			'project-contact-status');
define('MEO_CRM_ANALYTICS_PROCONSRC_TPL_FRONT', '../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/meo-crm-analytics-project-contact-source.php');
define('MEO_ANALYTICS_PROCONSRC_SLUG', 			'project-contact-source');
define('MEO_CRM_ANALYTICS_EMAILPDF_TPL_FRONT', 	'../../'.MEO_ANALYTICS_PLUGIN_SLUG.'/views/frontend/meo-crm-analytics-smartcapture-email-pdf-view.php');
define('MEO_ANALYTICS_EMAILPDF_SLUG', 			'email-pdfs');

# Upload File Path
$wpUploadsDir = wp_upload_dir();
define('MEO_CRM_ANALYTICS_PDF_DIRNAME', 'pdf_templates');			// For template PDF
//define('MEO_CRM_ANALYTICS_PDF_URL', $wpUploadsDir['baseurl'].'/meo_crm_analytics/pdf_templates/');
define('MEO_CRM_ANALYTICS_LOGO_DIR', $wpUploadsDir['basedir'].'/meo_crm_analytics/logos');					// For logos
define('MEO_CRM_ANALYTICS_LOGO_URL', $wpUploadsDir['baseurl'].'/meo_crm_analytics/logos/');
// We are going to store the generated pdfs under wp_uploads/projects/ID/contacts/pdfs
define('MEO_CRM_ANALYTICS_PROJECT_CONTACT_PDF_DIR', $wpUploadsDir['basedir'].'/projects/');			// For Generated PDF



# Required Files
require_once( $plugin_root . 'class-meo-crm-analytics.php');
require_once( $plugin_root . 'classes/meo-crm-analytics-piwik.php');
require_once( $plugin_root . 'views/backend/meo-crm-analytics-view.php');
require_once( $plugin_root . 'ajax/meo-crm-analytics-ajax.php');
require_once( $plugin_root . 'config/meo-crm-analytics-google-charts.php');
require_once( $plugin_root . 'config/meo-crm-analytics-piwik-charts.php');

# Globals
global $meo_crm_analytics_db_version, $meo_crm_analytics_schedule_db_version, $analyticsOptions, $analyticsProjectFields, $expectedAnalyticsFields;

$meo_crm_analytics_db_version = '1.0';
$meo_crm_analytics_schedule_db_version = '1.0';
$expectedAnalyticsFields = array('submit_time', 'last_name', 'first_name', 'email', 'phone', 'address', 'postcode', 'city', 'country', 'language', 'contact_type');

// Analytics options (will be stored in db wp_options)
$analyticsOptions = array(
		'mca_cron_key' 			=> array( "type" => "text",  	"label" => "Cron Authentication Key", 		"notes" => "Used to verify that the cron url param is correct" ),
		'mca_piwik_site_id' 	=> array( "type" => "text",  	"label" => "Piwik Site Id", 				"notes" => "MEO Real Estate V2 Piwik tracking code. Auto Tracking" ),
		'mca_api_key' 			=> array( "type" => "text",  	"label" => "Smart Capture API Key", 		"notes" => "Generate a random API key, eg<br/>Go to <a href='http://www.md5.cz/' target='_blank'>md5.cz (new tab will open)</a>, type a random string, take the first 16 characters of the result.<br/>Must be the same as set on the website written below" ),
		'mca_smart_capture_url' => array( "type" => "text",  	"label" => "Smart Capture URL", 			"notes" => "Set to corresponding MEO Realestate site" ),
		'mca_google_id' 		=> array( "type" => "text", 	"label" => "Google Analytics Tracking ID",	"notes" => "MEO Real Estate V2 GA tracking code. AUto tracking" ),
		'mca_email_sender' 		=> array( "type" => "email", 	"label" => "Email Sender", 					"notes" => "" ),
		'mca_email_sender_name' => array( "type" => "text",  	"label" => "Email Sender Name", 			"notes" => "" ),
		'mca_email_bcc'			=> array( "type" => "email", 	"label" => "Email Bcc (comma separated)", 	"notes" => "" ),
		'mca_email_subject'		=> array( "type" => "text", 	"label" => "Email Subject", 				"notes" => "" ),
		'mca_email_content'		=> array( "type" => "textarea", "label" => "Email Content", 				"notes" => " (Instructions: Replaceable fields: [download_url] [lot_code] [logo] [sender_email])" ),
		'mca_logo'				=> array( "type" => "file",  	"label" => "Logo",							"notes" => "" ),
);

// Fields to be added to the project form (will be stored in db table MEO_ANALYTICS_TABLE)
$analyticsProjectFields = array(
		'google_id' 	=> array( "type" => "text", "label" => "Google Analytics Tracking ID",		"notes" => "" ),
		'piwik_id' 		=> array( "type" => "text", "label" => "Piwik Site Id",						"notes" => "" ),
		'piwik_token' 	=> array( "type" => "text", "label" => "Piwik User Token",					"notes" => "Piwik User Token to connect system when making Curl calls." ),
		'api_key' 		=> array( "type" => "text", "label" => "Smart Capture API Key",				"notes" => "Same value as corresponding IMMO site, check IMMO site analytics settings page for instructions." ),
		'api_uri' 		=> array( "type" => "text", "label" => "Smart Capture API URi",				"notes" => "Path on the corresponding IMMO site, where 'Ajax' call will be made. Most of the time '/wp/wp-admin/admin-ajax.php'." ),
		'email'			=> array( "type" => "text", "label" => "Email address (comma separated)",	"notes" => "Email(s) to send new Contact alert to." ),
		'email_content'	=> array( "type" => "textarea", "label" => "Email Message",					"notes" => "Email content that will be sent to the client (and courtier if so), when there is a new Contact." ),
		'pdf_template'	=> array( "type" => "file", "label" => "PDF Template",						"notes" => "PDF file that will be used as a template when generating the new Contact PDF document." ),
);

/*
 * Check for Dependencies :: MEO CRM Projects
 */
function meo_crm_analytics_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-projects/meo-crm-projects.php' ) && is_plugin_active( 'meo-crm-core/meo-crm-core.php' ) ) {
		$installed_dependencies = true;
	}

	if(!$installed_dependencies) {

		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Core and MEO CRM Projects plugins before', 'meo-realestate').'</h3></div>';

		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Core and MEO CRM Projects plugins before.', 'meo-realestate'), E_USER_ERROR);
		exit;


	}else{

            // Everything is fine

            global $wpdb, $meo_crm_analytics_db_version, $analyticsOptions;

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

            $table_name = $wpdb->prefix . MEO_ANALYTICS_TABLE;
            $charset_collate = $wpdb->get_charset_collate();

            // Create Analytics table
            $sql = "CREATE TABLE $table_name (
                                    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                    project_id int(11) NOT NULL,
                                    google_id varchar(64) NULL,
                                    piwik_id int(11) NULL,
                                    piwik_token varchar(64) NULL,
                                    api_key varchar(64) NULL,
                                    api_uri varchar(64) NOT NULL DEFAULT '/wp/wp-admin/admin-ajax.php',
                                    email varchar(128) NULL,
                                    email_content TEXT NOT NULL,
                                    pdf_template varchar(128) NULL,
                                    PRIMARY KEY (id),
                                    ) $charset_collate;";

            dbDelta( $sql );

            $sql2 = "ALTER TABLE $table_name
                                    ADD CONSTRAINT uq_".$table_name." UNIQUE(project_id);";

            dbDelta( $sql2 );

            // Update db with the version info
            add_option( 'meo_crm_analytics_db_version', $meo_crm_analytics_db_version );

            /* Smart Capture */

            $table_name = $wpdb->prefix . MEO_ANALYTICS_SCHEDULE_TABLE;

            // Create Email Schedule table
            $sql = "CREATE TABLE " . $table_name . " (
                                    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                    analytics_id varchar(100) NOT NULL,
                                    project_id bigint(20) unsigned NOT NULL,
                                    creation_date datetime NOT NULL,
                                    update_date datetime NOT NULL,
                                    sent_date datetime,
                                    in_error enum('yes','no') default 'no',
                                    PRIMARY KEY (id),
                                    UNIQUE KEY analytics_and_site (analytics_id, project_id)
                                    ) $charset_collate;";

            dbDelta( $sql );

            // Update db version
            add_option( 'meo_crm_analytics_schedule_db_version', $meo_crm_analytics_schedule_db_version );
                
            // Create action to generate page
            do_action('plugins_meo_crm_active_create_page');

            createFolderAllProject(MEO_CRM_ANALYTICS_PDF_DIRNAME);
            createFolderAllProject('pdfs','contacts');

            // Prepare options
            foreach ($analyticsOptions as $analyticsOption => $optionDetail){
                    add_option($analyticsOption, '');
            }  
        }	
}
register_activation_hook( __FILE__, 'meo_crm_analytics_activate' );


# Add Scripts and Styles

	add_action( 'admin_enqueue_scripts', 'meo_crm_analytics_scripts_styles' );
	add_action( 'wp_enqueue_scripts', 'meo_crm_analytics_scripts_styles' );
	
	function meo_crm_analytics_scripts_styles() {
		
		# Google JS and CSS are in the Core, for convinience
		wp_register_script( 'meo_crm_analytics_js',  plugins_url('js/meo-crm-analytics.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_analytics_js' );
		
		wp_localize_script( 'meo_crm_analytics_js', 'meo_crm_analytics_ajax', array('ajax_url' => admin_url( 'admin-ajax.php' )) );
		
		# CSS
		wp_register_style( 'meo_crm_analytics_css',  plugins_url('css/meo-crm-analytics.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_analytics_css' );
	
	}

#########
# ADMIN #
#########


# Admin Menu
add_action( 'admin_menu', 'meo_crm_analytics_admin_menu' );

function meo_crm_analytics_admin_menu() {
	add_menu_page( 'MEO CRM Analytics',
			'Analytics',
			'manage_options',
			MEO_ANALYTICS_PLUGIN_SLUG,
			'meo_crm_analytics_admin_page',
			'dashicons-chart-line',
			7  );
}
	
	# Add plugin submenu in admin navigation
	
	add_action( 'admin_menu', 'meo_crm_analytics_admin_submenu', 100 );
	
	function meo_crm_analytics_admin_submenu() {
		add_submenu_page(MEO_ANALYTICS_PLUGIN_SLUG,
				'Google',
				'Google',
				'manage_options',
				'analytics_google',
				'add_page_analytics_google');
		add_submenu_page(MEO_ANALYTICS_PLUGIN_SLUG,
				'Piwik',
				'Piwik',
				'manage_options',
				'analytics_piwik',
				'add_page_analytics_piwik');
	}
	
	# Admin page to visualize users availability
	
	function add_page_analytics_google() {
		include_once 'views/backend/meo-crm-analytics-google-view.php';
		// meo_crm_analytics_admin_page();
	}
	
	function add_page_analytics_piwik() {
		include_once 'views/backend/meo-crm-analytics-piwik-view.php';
	}
	
	# Add projects fields
	add_action('projectsFormFields', 'meo_crm_analytics_projectsFields');
	
	function meo_crm_analytics_projectsFields(){
		
		global $analyticsProjectFields;
		
		$data = array();
		// Check if we already have a record to update, but check project id first
		if(isset($_GET['id']) && !empty($_GET['id'])){
			$results = MeoCrmAnalytics::getAnalyticsByProjectID($_GET['id']);
			if($results) {
				$data = reset($results);
			}
		}
		
		echo '<table class="form-table meo-form-table">
				<tr class="form-field">
	                <th colspan="2">
	                    <label for="google_id">
	                        MEO CRM ANALYTICS
	                    </label>
	                </th>
	            </tr>';
		
		foreach($analyticsProjectFields as $field => $extra){
			$value = '';
			
			if(!empty($data[$field])) { $value = $data[$field]; }
			
			echo '<tr class="form-field">
				                <th>
				                    <label for="'.$field.'">'.$extra['label'].'</label>
				                </th>
				                <td>';
					if($extra['type'] == 'textarea'){
						if($value == ''){
							$value = MEO_CRM_ANALYTICS_EMAIL_CONTENT;
						}
						else $value = stripslashes($value);
						wp_editor( $value, str_replace('_', '-', $field), array('textarea_name' => $field, 'textarea_rows' => 10, 'media_buttons' => false, 'teeny' => true) );
					}
					else if($extra['type'] == 'file'){
						echo '<input type="'.$extra['type'].'" name="'.$field.'" id="'.$field.'" value="" /> ';
						if($value != '' && isset($_GET['id'])){
							$projectPDFfolderPath = MEO_CRM_PROJECTS_FOLDER_URL . '/' . $_GET['id'] . '/' . MEO_CRM_ANALYTICS_PDF_DIRNAME . '/';
							echo '<a href="'.$projectPDFfolderPath.$value.'" target="_blank" class="">'.$value.'</a>';
						}
					}
					else {
						echo '<input type="'.$extra['type'].'" name="'.$field.'" id="'.$field.'" placeholder="'.$extra['label'].'" value="'.$value.'" />';
					}
					
					if($extra['notes'] != ''){
						echo '<br/><em>'.$extra['notes'].'</em>';
					}
				                    
			echo   '</td>
				  </tr>';
			
		}
		
		
		echo '</table>';
	}
	
	
	# Add projects fields Validation
	add_filter('projectsFormValidate', 'meo_crm_analytics_projectsformvalidate', 2);
	
	function meo_crm_analytics_projectsformvalidate($projectID) {
            
		global $analyticsProjectFields;
                
		$data = array();
		$data['project_id'] = $projectID;
                
                createProjectUnderFolder($projectID, MEO_CRM_ANALYTICS_PDF_DIRNAME);                
                createProjectUnderFolder($projectID, 'pdfs', 'contacts');                
		
		// Collect data
		foreach($analyticsProjectFields as $field => $extra){
			
			if($field == 'pdf_template') {
				# There is a file to check
				if(isset($_FILES[$field]) && array_key_exists('name', $_FILES[$field]) && !empty($_FILES[$field]['name'])){
					$data[$field] = $_FILES[$field]['name'];
					# TODO :: Add controls over the uploaded file (size, mime, etc.)
					$projectPDFfolderPath = MEO_CRM_PROJECTS_FOLDER_DIR . '/' . $projectID . '/' . MEO_CRM_ANALYTICS_PDF_DIRNAME . '/';
					move_uploaded_file($_FILES[$field]['tmp_name'], $projectPDFfolderPath.$_FILES[$field]['name']);
				}
			}
			else $data[$field] = $_POST[$field];
				
		}
		
		// Check if we already have a record to update, ot if insert a new one
		$results = MeoCrmAnalytics::getAnalyticsByProjectID($projectID);
		
		// Update
		if($results) {
			MeoCrmAnalytics::updateAnalytics($data, array('project_id' => $projectID));
		}
		// Insert
		else {
			MeoCrmAnalytics::addAnalytics($data);
		}
                
        return $projectID;
	}

# Admin Page

	function meo_crm_analytics_drawChart($analyticsMasterArray){
	
		foreach($analyticsMasterArray as $chart => $chartOptions) {
				
			echo "var ".$chart." = new gapi.analytics.googleCharts.DataChart({
					    reportType: 'ga',"."\n";
				
			foreach($chartOptions as $optionKey => $optionSet){
	
				echo $optionKey.": {"."\n";
	
				# Building Chart
				foreach($optionSet as $param => $value){
					
					if($param != 'info'){
						
						if($param != 'options'){
							echo "'".$param."': '".$value."',"."\n";
						}
						else {
		
							echo "'options': {"."\n";
		
							foreach($optionSet['options'] as $optparam => $optvalue){
									
								if(!is_array($optvalue)){
									echo "'".$optparam."': '".$optvalue."',"."\n";
								}
								else {
									echo "'".$optparam."': {"."\n";
		
									foreach($optvalue as $k => $v){
		
										if(!is_array($v)){
											echo $k.": '".$v."',"."\n";
										}
										else {
											echo "'".$k."': {"."\n";
												
											foreach($v as $kv => $vv){
												echo $kv.": '".$vv."',"."\n";
											}
												
											echo "}"."\n";
										}
									}
		
									echo "}"."\n";
								}
									
							}
		
							echo "}"."\n";
		
						}
					}
				}
	
				echo " },"."\n";
			}
			echo "});"."\n";
		}
	}

	# Add User Analytics Field and validation on admin User profile page

		# Field
		
		add_action('personal_options', 'meo_crm_analytics_edit_user_fields', 1, 1);
		
		function meo_crm_analytics_edit_user_fields($profileuser){
			
			$piwik_site_id = get_option('mca_piwik_site_id');
			
			$piwik_visitor_id = get_user_meta($profileuser->ID, 'piwik_visitor_id', true);
			
			if($piwik_site_id){
				
				$piwikURL = '';
				
				if($piwik_visitor_id){
					# Building Piwik Link
					$piwikVisitorUrl = "http://".MEO_ANALYTICS_SERVER."/index.php?
												date=".date('Y-m-d')."
												&module=Widgetize
												&action=iframe
												&visitorId=".$piwik_visitor_id."
												&idSite=".$piwik_site_id."
												&period=day
												&moduleToWidgetize=Live
												&actionToWidgetize=getVisitorProfilePopup";
					
					$piwikURL = '<a href="'.$piwikVisitorUrl.'" class="fancybox fancybox.iframe"><i class="fa fa-area-chart" aria-hidden="true"></i></a>';
				}
				
				echo '<tr class="">
						<th scope="row">Piwik Visitor ID</th>
						<td><label for="rich_editing"><input type="text" value="'.$piwik_visitor_id.'" name="piwik_visitor_id" /> '.$piwikURL.'</label>
			                <script type="text/javascript">
								jQuery(document).ready(function (){
									jQuery(".fancybox").fancybox();
								});
							</script>
			            </td>
					  </tr>';
			}
			return ;
		}
		
		# Validation
		
		add_action('personal_options_update', 'meo_crm_analytics_update_user_fields', 1, 1);
		
		function meo_crm_analytics_update_user_fields($user_id){
		
			if(isset($_POST['piwik_visitor_id']) && !empty($_POST['piwik_visitor_id'])){
				update_user_meta( $user_id, 'piwik_visitor_id', $_POST['piwik_visitor_id'] );
			}
		}
	
	# Add column header and content on admin Users page
	
		# Header
		
		add_filter( 'manage_users_columns', 'meo_crm_analytics_modify_user_table' );
		
		function meo_crm_analytics_modify_user_table( $column ) {
			$column['piwik_visits'] = 'Piwik Visits';
			return $column;
		}
		
		# Content
		
		add_filter( 'manage_users_custom_column', 'meo_crm_analytics_modify_user_table_row', 10, 3 );
		
		function meo_crm_analytics_modify_user_table_row( $val, $column_name, $user_id ) {
			switch ($column_name) {
				case 'piwik_visits' :
					$piwik_site_id = get_option('mca_piwik_site_id');
					
					$piwik_visitor_id = get_user_meta($user_id, 'piwik_visitor_id', true);
					
					if($piwik_visitor_id && $piwik_site_id){
						# Building Piwik Link
						$piwikVisitorUrl = "http://".MEO_ANALYTICS_SERVER."/index.php?
											date=".date('Y-m-d')."
											&module=Widgetize
											&action=iframe
											&visitorId=".$piwik_visitor_id."
											&idSite=".$piwik_site_id."
											&period=day
											&moduleToWidgetize=Live
											&actionToWidgetize=getVisitorProfilePopup";
						
						$piwikURL = '<a href="'.$piwikVisitorUrl.'" class="fancybox fancybox.iframe"><i class="fa fa-area-chart" aria-hidden="true"></i></a>';
						
						return $piwikURL.'
			                <script type="text/javascript">
								jQuery(document).ready(function (){
									jQuery(".fancybox").fancybox();
								});
							</script>'; // get_the_author_meta( 'phone', $user_id );
					}
					break;
				default:
			}
			return $val;
		}


#########
# FRONT #
#########


	# PAGE CREATION
	
		# Register Plugin templates
		
		add_filter('meo_crm_core_templates_collector', 'meo_crm_analytics_templates_register', 1, 1);
		
		function meo_crm_analytics_templates_register($pluginsTemplates){
		
			$pluginsTemplates[MEO_CRM_ANALYTICS_GOOGLE_TPL_FRONT]   	= 'MEO CRM Analytics Google';
			$pluginsTemplates[MEO_CRM_ANALYTICS_PIWIK_TPL_FRONT]   		= 'MEO CRM Analytics Piwik';
			$pluginsTemplates[MEO_CRM_ANALYTICS_EMAILPDF_TPL_FRONT] 	= 'MEO CRM Analytics Email PDF';
			$pluginsTemplates[MEO_CRM_ANALYTICS_PROCONSTA_TPL_FRONT] 	= 'MEO CRM Analytics Project-Contact-Status';
			$pluginsTemplates[MEO_CRM_ANALYTICS_PROCONSRC_TPL_FRONT] 	= 'MEO CRM Analytics Project-Contact-Source';
			return $pluginsTemplates;
		}
		
		# Register Plugin Pages
		
		add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_analytics_pages_register' );
		
		function meo_crm_analytics_pages_register($pluginsPages) {
		
			# Create Page for Analytics Google
			$pluginsPages[MEO_ANALYTICS_GOOGLE_SLUG] = array( 'post_title' 	=> 'Google Analytics',
														'post_content' 		=> 'Page to be used for Google Analytics Reports.',
														'_wp_page_template' => MEO_CRM_ANALYTICS_GOOGLE_TPL_FRONT);
			
			# Create Page for Analytics Piwik
			$pluginsPages[MEO_ANALYTICS_PIWIK_SLUG] = array( 'post_title' 	=> 'Piwik Analytics',
														'post_content' 		=> 'Page to be used for Piwik Analytics Reports.',
														'_wp_page_template' => MEO_CRM_ANALYTICS_PIWIK_TPL_FRONT);
			
			# Create Page for Analytics Email PDF
			$pluginsPages[MEO_ANALYTICS_EMAILPDF_SLUG] = array(	'post_title'=> 'MEO CRM Analytics Email PDF',
														'post_content' 		=> 'Page to be used for Analytics Email PDF exports.',
														'_wp_page_template' => MEO_CRM_ANALYTICS_EMAILPDF_TPL_FRONT);
			
			
			# Create Page for Analytics Project Contact Status
			$pluginsPages[MEO_ANALYTICS_PROCONSTA_SLUG] = array('post_title'=> 'Project/Contact/Status',
														'post_content' 		=> 'Page to be used for Project Contact Status report.',
														'_wp_page_template' => MEO_CRM_ANALYTICS_PROCONSTA_TPL_FRONT);
			
			
			# Create Page for Analytics Project Contact Source
			$pluginsPages[MEO_ANALYTICS_PROCONSRC_SLUG] = array('post_title'=> 'Project/Contact/Source',
														'post_content' 		=> 'Page to be used for Project Contact Source report.',
														'_wp_page_template' => MEO_CRM_ANALYTICS_PROCONSRC_TPL_FRONT);
			
			
			return $pluginsPages;
		}

		
##################
# Tracking codes #
##################
		
		
	# Piwik Tracking Code

	add_action( 'wp_head', 'meo_crm_analytics_piwik_tracking_code' );
	
	function meo_crm_analytics_piwik_tracking_code() {
	
		// Code was originally in meo-realestate-development-plugin
	
		global $wp_query, $current_user;
	
		$piwik_site_id = get_option('mca_piwik_site_id');
	
		if (empty($piwik_site_id) || !isset($current_user)) {
			return;
		}
		?>
			<!-- Piwik -->
			<script type="text/javascript">
				var analytics_post_id = <?php echo $wp_query->post->ID; ?>,
				    analytics_post_type = '<?php echo $wp_query->post->post_type; ?>';
		
				var triesCounter = 0;
				piwikload = function() {
					if (typeof jQuery === "undefined") {
						triesCounter++;
						if (triesCounter < 20) {
							setTimeout(piwikload, 50);
						}
					}
				};
				var _paq = _paq || [];
				_paq.push(['setCustomVariable', '1', 'post_id',        analytics_post_id,   "page"]);
				_paq.push(['setCustomVariable', '2', 'post_type',      analytics_post_type, "page"]);
				_paq.push(['setCustomVariable', '3', 'logged_user',    '<?php echo '['.$current_user->user_nicename.']'.$current_user->user_email; ?>',    "page"]);
				_paq.push(['trackPageView']);
				_paq.push(['enableLinkTracking']);
				_paq.push([ function() {
					piwik_user_id = this.getVisitorId();
					piwikload();
				}]);
				(function() {
					var u=(("https:" == document.location.protocol) ? "https" : "http") + "://<?php echo MEO_ANALYTICS_SERVER; ?>/";
					_paq.push(['setTrackerUrl', u+'piwik.php']);
					_paq.push(['setSiteId', <?php echo $piwik_site_id; ?>]);
					var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
					g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
				})();
			</script>
			<noscript><p><img src="http://<?php echo MEO_ANALYTICS_SERVER; ?>/piwik.php?idsite=<?php echo $piwik_site_id; ?>" style="border:0;" alt="" /></p></noscript>
			<!-- End Piwik Code --><?php
		}
	
	# Google Analytics Tracking Code
	
	add_action( 'wp_footer', 'meo_crm_analytics_google_analytics_front' );
	
	function meo_crm_analytics_google_analytics_front() {
		
		global $current_user;
		
		$googleAnalyticsId = get_option('mca_google_id');
		
		if($googleAnalyticsId){
			echo "<script>
					  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
					
					  ga('create', '".$googleAnalyticsId."', 'auto');";
	
			if($current_user){ echo "ga('set', 'userId', '[".$current_user->user_nicename."]".$current_user->user_email."');"; }
			  
			echo "ga('send', 'pageview');
				</script>";
		}
	}


###########
# WIDGETS #
###########

		
	/* Google */
		
	/* Piwik */
		
		function meo_crm_analytics_piwik_widget($moduleToWidgetize, $actionToWidgetize, $idSite, $period, $date, $viewDataTable = '') {
		
			$iframeURL = "http://piwik.meoanalytics.com/index.php?module=Widgetize&action=iframe&widget=1";
			$iframeURL.= "&moduleToWidgetize=".$moduleToWidgetize."&actionToWidgetize=".$actionToWidgetize;
			$iframeURL.= "&idSite=".$idSite."&period=".$period."&date=".$date;
			if($viewDataTable != ''){
				$iframeURL.= "&viewDataTable=".$viewDataTable;
			}
			$iframeURL.= "&disableLink=1&widget=1";
		
			return '<div id="widgetIframe">
					<iframe width="100%" height="450" src="'.$iframeURL.'" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe>
				</div>';
		}
	
	
		# Build PDF file name
		
		function meo_crm_analytics_building_contact_pdf($contact){
		
			$firstPart = str_replace(array('.','-','_','@'), '', $contact['email']);
			$secondPart = '.pdf';
			if(isset($contact['analytics_id']) && $contact['analytics_id'] != ''){
				$secondPart = $contact['analytics_id'] . '.pdf';
			}
			else if($contact['type'] == MEO_CONTACTS_STATUS_MANUAL){
				$secondPart = MEO_CONTACTS_STATUS_MANUAL.'.pdf';
			}
		
			return $firstPart . '_' . $secondPart;
		}
	
	
########################
# EXTEND OTHER PLUGINS #
########################
		
	
	####################
	# MEO CRM Projects #
	####################
	
	
		# Add Analytics data to the corresponding Projects
		
		add_filter('meocrmprojects_getProjectData', 'meo_crm_analytics_getProjectData');
	
		function meo_crm_analytics_getProjectData($project){
			
			if($project){

				$returnProject = array();
				
				# Get Analytics fields for the current Project
				$analyticsProjectDetails = MeoCrmAnalytics::getAnalyticsByProjectID($project['id']);
				
				if(is_array($analyticsProjectDetails)) {
					unset($analyticsProjectDetails[0]['id']); // To avoid conflicts
					# Combine all fields
					$returnProject = array_merge($project, reset($analyticsProjectDetails));
				}
				else $returnProject = $project;
				
				# Create 'pdfs' folder under 'projectID'/contacts
				# TODO::Call Core function to create the 'pdfs'
				// $path = MEO_CRM_PROJECTS_FOLDER_DIR . $project['id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER
				
				return $returnProject;
			}
			return false;
		}
		/*
		# Add Analytics data to the corresponding Project
	
		add_filter('meocrmprojects_getproject', 'meo_crm_analytics_project_details');
		
		function meo_crm_analytics_project_details($project){
		
			$projects = array( '0' => $project );
		
			$returnProjects = meo_crm_analytics_projects_details($projects);
			
			$project = reset($returnProjects);
			
			return $project;
		}
		*/
		
		# Edit Project Front page
		add_action('meo_crm_projects_front_edit_project_ajax', 'meo_crm_analytics_front_edit_project');
		
		function meo_crm_analytics_front_edit_project($project){
			
			if(array_key_exists('email', $project)){
				
				echo '<table class="form-table meo-form-table">
						<tr class="form-field">
			                <th>
			                    <label>
			                        PROJECT EMAILS
			                    </label>
			                </th>
							<td style="text-align:left">
								Email addresses (comma separated), to send you new contact emails
							</td>
			            </tr>
						<tr class="form-field">
			                <th></th>
							<td style="text-align:left">
								<input name="email" id="email" placeholder="Email address (comma separated)" value="'.$project['email'].'" type="text" style="width:90%;" />
								<input type="button" onclick="meoAnalyticsSaveProjectField(\''.$project['project_id'].'\', \'email\', this);" class="button button-primary" value="Save Email" /> <span class="saved-project-field"></span>
							</td>
			            </tr>
					  <table>';
				
			}
			// meo_crm_relationship_status_projectsFields($project['id']);
		}
	
	
	####################
	# MEO CRM Contacts #
	####################
	
	
		# Add PDF in the allowed fields
		
		add_filter('meocrmcontacts_allowedFields', 'meo_crm_analytics_contacts_allowedfields', 10, 2);
		
		function meo_crm_analytics_contacts_allowedfields($allowedFields, $precision = ''){
			if($precision != 'CSV_EXPORT') {
				$allowedFields['PDF'] = 'pdf';
			}
			
			return $allowedFields;
		}
		
		# Add Contact PDF link, if exists, to the Contact's profile
		
		add_filter('meocrmcontacts_getContactData', 'meo_crm_analytics_getContactData', 10);
		
		function meo_crm_analytics_getContactData($contact){
			
			$contact['pdf'] = '';
			
			if((array_key_exists('analytics_id', $contact) && $contact['analytics_id'] != '') || $contact['type'] == MEO_CONTACTS_STATUS_MANUAL){
				# Building the PDF file
				$last_name    = preg_replace('/[^A-Za-z0-9]/', '', $contact['last_name']);
				$first_name = preg_replace('/[^A-Za-z0-9]/', '', $contact['first_name']);
				
				$PDFpath = '/' . $contact['project_id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER;
				$PDFfile = $PDFpath . '/' . meo_crm_analytics_building_contact_pdf($contact);
					
				if (file_exists(MEO_CRM_PROJECTS_FOLDER_DIR . $PDFfile)) {
					// dashicons-before dashicons-media-text
					$contact['pdf'] = '<a href="'.MEO_CRM_PROJECTS_FOLDER_URL . $PDFfile.'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
				}
			}
			
			return $contact;
		}
		
		# Deleting Contact
		
		add_action('meo_crm_contacts_delete_contact_by_id', 'meo_crm_analytics_delete_contact_by_id');
		
		function meo_crm_analytics_delete_contact_by_id($contact_id){
		
			$contact = MeoCrmContacts::getContactsByContactID($contact_id);
			
			if($contact){
				# Building the PDF file
				$PDFpath = MEO_CRM_PROJECTS_FOLDER_DIR . $contact['project_id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER;
				$PDFfile = $PDFpath . '/' . meo_crm_analytics_building_contact_pdf($contact);
				
				if (file_exists($PDFfile)) {
					unlink($PDFfile);
				}
			}
		
		}