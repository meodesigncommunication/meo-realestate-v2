<?php 
/**
 * The template for displaying individual Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 
?>
<div id="primary" class="content-area meo-crm-contacts-front-container">
	<main id="main" class="project-main align-filters" role="main">
		<div class="clear"></div>
		<div class="meo-crm-analytics-contacts-projects-status">
<?php 
# Get current user (client)

$clients = meo_crm_users_getClients();

# Will be use to change project background color
$indexColor = 0;

if($clients && is_array($clients)){
	
	# Init
	$htmlContent = '';
	$htmlFilter = '';
	
	$htmlContent.= '<div id="stats-container-1">
 					<table id="table-client">';
	
	foreach($clients as $c => $client){
		
		# CLIENT row
		$htmlContent.= '<tr>
				<td>';
		
		$htmlFilter.= '<optgroup label="'.$client['display_name'].'" data-client="'.$client['ID'].'">';
		
		# Get List of his projects
		$clientProjects = meo_crm_projects_getProjectsByUserID($client['ID']);
		
		if($clientProjects && is_array($clientProjects)){
			
			$htmlContent.= '<table id="table-project">';
			
			foreach($clientProjects as $p => $project){
				
				if($project){
					
					# HTML
					$htmlFilter.= '<option value="'.$project['id'].'">'.$project['name'].'</option>';
					
					# GET Project's Contacts
					$nbOfContactsTotalProj = meo_crm_contacts_countContactsByProjectID($project['id']);
					
					$projectHasStatus = false;
					
					# Get list of Status for the current project
					if(array_key_exists('rel_status', $project) && is_array($project['rel_status']) && count($project['rel_status']) > 0){ // 1
					
						$projectHasStatus = true;
						
						# Add EXIT state at the end
						
						$project['rel_status'][] = array ( 
								'id' => MEO_RELATIONSHIP_STATE_EXIT_ID,
								'project_id' => $project['id'],
								'label' => 'EXIT',
								'status_order' => MEO_RELATIONSHIP_STATE_EXIT_ID,
								'color' =>  MEO_RELATIONSHIP_STATE_EXIT_COLOR);
						
						
						# Build table headers
						$projectStatusHeaders = '<tr>
													<th>Record</th>';
						foreach($project['rel_status'] as $ps => $projectStatus){
								$projectStatusHeaders.= '<th>'.$projectStatus['label'].'</th>';
						}
						$projectStatusHeaders.= '</tr>';
						
					}
					
					$indexColor++;
					
					$percentBtn = '';
					if($nbOfContactsTotalProj > 0){
						$percentBtn = '<br/><div id="stats-referer-status-filter"><i class="fa fa-percent status-toggler cursor-hand" aria-hidden="true"></i></div>';
					}
					
					# PROJECT row
					$htmlContent.= '<tr class="project-color-'.($indexColor%2 == 0 ? '1' : '0').'">
							<th>'.$client['display_name'].'<br/>'.$project['name'].'<br/>'.$nbOfContactsTotalProj.' contacts'.$percentBtn.'</th>
							<td>';
					
					# Get list of users (Courtier) per Project
					$courtiers = meo_crm_users_getCourtiersByProjectID($project['id']);
					
					# Try Client if Project has no Courtier
					if(!$courtiers){
						$courtiers = array(0 => $client);
					}
					
					if($courtiers){
						
						$htmlContent.= '<table id="table-courtier">';
						
						foreach($courtiers as $uc => $courtier){
							
							# GET Courtier's Contacts
							$nbOfContactsTotalCourtier = meo_crm_contacts_countContactsByUserIDandProjectID($courtier['ID'], $project['id']);
							
							$percentageCourtierText = '';
							if($nbOfContactsTotalProj > 0){
								$percentageCourtier = round(($nbOfContactsTotalCourtier*100) / $nbOfContactsTotalProj);
								$percentageCourtierText = '<br/><span class="stat-perc-src percentages">'.$percentageCourtier.'%</span>';
							}
							
							# COURTIER row
							$htmlContent.= '<tr>
									<th>'.$courtier['display_name'].'<br/>'.$nbOfContactsTotalCourtier.' contacts'.$percentageCourtierText.'</span></th>
									<td>';
							
							if($projectHasStatus && $nbOfContactsTotalCourtier > 0){
								
								$htmlContent.= '<table id="table-status">'.$projectStatusHeaders.'<tr>';
								
	
								# Get Courtier Contacts stats in one query for all status (except 9999 - reassigned state)
								
								# Last States count
								$courtierLastStats = MeoCrmContactsUsersRelationship::countContactsPerStatusIDbyCourtierIDProjectID($courtier['ID'], $project['id']);
								# Get number of Contact EXIT per state before EXIT (except reassignement state)
								// $courtierSecondLastStats = MeoCrmContactsUsersRelationship::countContactsPerSecondLastStatusIDbyCourtierIDProjectID($courtier['ID'], $project['id']);
								$courtierSecondLastStats = MeoCrmContactsUsersRelationship::countContactsPerSecondLastStatusIDExitDetailsbyCourtierIDProjectID($courtier['ID'], $project['id']);
									
									# Color of the EXIT status (last status in the array of status)
									$exitbackgroundColor = end($project['rel_status'])['color'];
								
								# Get stats for state 0 (recording a new user)
								$courtierStatsZero = MeoCrmContactsUsersRelationship::countContactsByStatusIDbyCourtierIDProjectID(0, $courtier['ID'], $project['id']);
								
								$htmlExit = '';
								
								# Get total of EXIT per Courtier Contacts after recording use (state 0)
								if(is_array($courtierSecondLastStats) && array_key_exists(0, $courtierSecondLastStats)) {
									
									$htmlExit = '<span class="exit-details">
													<ul><li><strong>EXIT Details</strong></li>';
									
									foreach($courtierSecondLastStats[0] as $note => $nb){
										if($note != 'total'){
											$htmlExit.= '<li>'.$note.' : '.$nb.'</li>';
										}
									}
									
									$htmlExit.= '</ul>
												</span>';
									
									$nbexits = $courtierSecondLastStats[0]['total'];
									$percentageExit = round(($nbexits*100) / $nbOfContactsTotalCourtier);
									$topzeroclass = '';
									if($percentageExit > 90){
										$topzeroclass = ' top-zero';
									}
									$nbexitshtml = '<span class="nbexits" style="background-color:'.$exitbackgroundColor.'; height:'.$percentageExit.'%;"><span class="stat-text'.$topzeroclass.'">'.$nbexits.'</span></span>';
								}
								else $nbexitshtml ='';
								
								$percentage = round(($courtierStatsZero*100/$nbOfContactsTotalCourtier));
								$topzeroclass = '';
								if($percentage > 90){
									$topzeroclass = ' top-zero';
								}
								$htmlContent.= '<td>'.$htmlExit.'<span class="nbcontacts" style="background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.'; height:'.$percentage.'%;"><span class="stat-text'.$topzeroclass.'">'.$courtierStatsZero.'</span></span>'.$nbexitshtml.'</td>';
								
								foreach($project['rel_status'] as $ps => $projectStatus){
									
									# Add EXIT details on all status
									$htmlExit = '';
									
									// Current Status
									if(is_array($courtierLastStats) && array_key_exists($projectStatus['id'], $courtierLastStats)) {$nbcontacts = $courtierLastStats[$projectStatus['id']];}
									else $nbcontacts = 0;
									
									// Current Status was last before exit (supposingly EXIT is the last status)
									if(is_array($courtierSecondLastStats) && array_key_exists($projectStatus['id'], $courtierSecondLastStats)) {
										
										$htmlExit = '<span class="exit-details">
														<ul><li><strong>EXIT Details</strong></li>';
										
										foreach($courtierSecondLastStats[$projectStatus['id']] as $note => $nb){
											if($note != 'total'){
												$htmlExit.= '<li>'.$note.' : '.$nb.'</li>';
											}
										}
										
										$htmlExit.= '</ul>
													</span>';
										
										$nbexits = $courtierSecondLastStats[$projectStatus['id']]['total'];
										$percentageExit = round(($nbexits*100) / $nbOfContactsTotalCourtier);
										$topzeroclass = '';
										if($percentageExit > 90){
											$topzeroclass = ' top-zero';
										}
										$nbexitshtml = '<span class="nbexits" style="background-color:'.$exitbackgroundColor.'; height:'.$percentageExit.'%;"><span class="stat-text'.$topzeroclass.'">'.$nbexits.'</span></span>';
									}
									else $nbexitshtml ='';
									
									$percentage = round(($nbcontacts*100/$nbOfContactsTotalCourtier));


									# Add ExitDetails if EXIT state is current
									if($projectStatus['id'] == MEO_RELATIONSHIP_STATE_EXIT_ID){
										# Exit details (TODO::this call can be avoided as we performed a query getting all details now)
										$exitDetails = MeoCrmContactsUsersRelationship::countContactsPerExitStateByProjectIDByUserID($project['id'], $courtier['ID']);
									
										if($exitDetails && is_array($exitDetails)){
												
											$htmlExit = '<span class="exit-details">
																<ul><li><strong>TOTAL EXIT</strong></li>';
												
											foreach($exitDetails as $text => $nb){
												$htmlExit.= '<li>'.$text.' : '.$nb.'</li>';
											}
												
											$htmlExit.= '</ul>
														</span>';
										}
									}
									
									$topzeroclass = '';
									if($percentage > 90){
										$topzeroclass = ' top-zero';
									}
									
									
									$htmlContent.= '<td>'.$htmlExit.'<span class="nbcontacts" style="background-color:'.$projectStatus['color'].'; height:'.$percentage.'%;"><span class="stat-text'.$topzeroclass.'">'.$nbcontacts.'</span></span>'.$nbexitshtml.'</td>';
									
								} // /foreach projects
								
								$htmlContent.= '</tr></table>';
								
							} // /if projecthasstatus
							
							$htmlContent.= '</td></tr>';
							
						} // /foreach courtiers
						
						$htmlContent.= '</table>';
						
					} // /if courtiers
					
					else {
						$htmlContent.= meo_crm_contacts_countContactsByUserIDandProjectID($client['ID'], $project['id']);
					}
					
					$htmlContent.= '	</td>
						  </tr>';
				} // /if project
				
			} // /foreach clientProjects
			
			$htmlContent.= '</table>';
			
		} // /if clientProjects
		
		$htmlContent.= '	</td>
			  </tr>';
		
	} // /foreach clients
	
	$htmlContent.= '</table>
					</div>
					<div id="stats-container-2"></div>';
	$htmlFilter.= '</optgroup>';
	
	?>
	<div class="selectors-container redband">
		<section id="view-selector-1" class="view-selector">
			<select id="siteId-1" name="siteId">
				<option value="0">Select a Project</option>
				<?php echo $htmlFilter; ?>
			</select>
		</section>
		<section id="date-selector" class="date-selector">
			<div>From: <input id="start-date-1" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div>To: <input id="end-date-1" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div><input id="submit_proconsta-1" name="submit" type="submit" /></div>
		</section>
		<div class="compare">VS</div>
		<section id="view-selector-2" class="view-selector">
			<select id="siteId-2" name="siteId-2">
				<option value="0">Select a Project</option>
				<?php echo $htmlFilter; ?>
			</select>
		</section>
		<section id="date-selector-2" class="date-selector">
			<div>From: <input id="start-date-2" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div>To: <input id="end-date-2" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div><input id="submit_proconsta-2" name="submit" type="submit" /></div>
		</section>
		<div class="clear"></div>
	</div>
	<?php 
	echo $htmlContent;
	
} // /if clients
else MeoCrmCoreTools::meo_crm_core_403();
?>
	</div>
	</main><!-- .project-main -->
</div><!-- .content-area -->
<?php 
# Load the library
require_once(MEO_ANALYTICS_PLUGIN_ROOT.'/js/meo-crm-analytics.js.php');
get_footer();
?>