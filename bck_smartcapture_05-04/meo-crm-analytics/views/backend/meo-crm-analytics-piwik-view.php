<?php 

// Init
$piwikAnalyticsMasterArray = meo_crm_analytics_getPiwikChartsConfig('32670458');



?>
<!-- Step 1: Create the containing elements. -->
<div class="selectors-container">
	<section id="auth-button"></section>
	<section id="view-selector">
		<select id="siteId" name="siteId">
			<option value="32670456">App Test</option>
			<option value="2">cassiopee-test</option>
			<option value="32670454">champs-meunier</option>
			<option value="32670461">charmey-test</option>
			<option value="32670455">estavayer-test</option>
			<option value="32670459">froideville-test</option>
			<option value="32670462">Gefiswiss-corporate-test</option>
			<option value="32670457">Grisoni Vision</option>
			<option value="32670458">habiter-estavayer</option>
			<option value="32670464">les-rives-de-bramois</option>
			<option value="32670453">parallele-prod</option>
			<option value="32670452">parallele-test</option>
			<option value="32670463">Steiner-sion-test</option>
			<option value="32670460">trois-moineaux</option>
			<option value="1">zebucity</option>
			<option value="32670469	">MEO Immobilier V2</option>
		</select>
	</section>
	<section id="date-selector">
		<div>From: <input id="start-date" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" /></div>
		<div>To: <input id="end-date" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" /></div>
		<div><input id="property-id" name="property-id" type="hidden" /><input id="submitpiwik" name="submitpiwik" type="submit" /></div>
	</section>
	<div class="clear"></div>
</div>
<div id="piwik-charts-main">
<?php 
# Build chart containers
foreach($piwikAnalyticsMasterArray as $chart => $chartOptions) {

	echo "<div class='piwik-chart-container'>
				<h3>".$chartOptions['info']['title']."</h3>
				<section id='".$chart."' class='google-chart'>
				".meo_crm_analytics_piwik_widget($chartOptions['moduleToWidgetize'], $chartOptions['actionToWidgetize'], $chartOptions['idSite'], $chartOptions['period'], $chartOptions['date'])."
				</section>
			 </div>";

}
?>
</div>
<?php 
	# Load the library
	require_once(MEO_ANALYTICS_PLUGIN_ROOT.'/js/meo-crm-analytics.js.php');
?>