<?php function meo_crm_analytics_admin_page() { 
	
	global $analyticsProjectFields, $analyticsOptions;
	
	// Populate data
	$data = array();
	
	foreach ($analyticsOptions as $analyticsOption => $optionDetail){
		$data[$analyticsOption] = get_option($analyticsOption);
	}
	
	// Form Validation
	if (isset($_POST['submit_analytics_options'])){
	
		foreach ($analyticsOptions as $analyticsOption => $optionDetail){
			update_option($analyticsOption, $_POST[$analyticsOption]);
			// Over ride populated data
			$data[$analyticsOption] = $_POST[$analyticsOption];
		}
	}
	
	?>
	
	<form method="post" name="smart-capture-options">
	
		<div>
			<table class="form-table meo-form-table">
				<tr class="form-field">
					<td colspan="2"><h2>Analytics / Smart Capture Options</h2></td>
				</tr>
				<tr class="form-field">
					<th><label>Piwik MEO Server</label></th>
					<td><em><?php echo MEO_ANALYTICS_SERVER; ?> (same on IMMO sites)</em></td>
				</tr>
				<?php 
				// Prepare options
				foreach ($analyticsOptions as $analyticsOption => $optionDetail){
					echo '<tr class="form-field">
							<th><label>'.$optionDetail['label'].'</label></th>
							<td>';
							if($optionDetail['type'] == 'textarea'){
								wp_editor( $data[$analyticsOption], str_replace('_', '-', $analyticsOption), array('textarea_name' => $analyticsOption, 'textarea_rows' => 10, 'media_buttons' => false, 'teeny' => true) );
							}
							else echo '<input name="'.$analyticsOption.'" type="'.$optionDetail['type'].'" value="'.$data[$analyticsOption].'" />';
							
							if($optionDetail['notes'] != ''){
								echo '<br/><em>Notes: '.$optionDetail['notes'].'</em>';
							}
							
					echo '</td>
						</tr>';
				}
				?>
				<tr class="form-field">
					<td colspan="2"><input type="submit" name="submit_analytics_options" value="Save" class="button button-primary" /></td>
				</tr>
			</table>
		</div>
		
	</form>
	
	<?php 
	
	$records = MeoCrmAnalytics::getAnalytics();
	
	?>
	<h2>Analytics Settings per Project</h2>
	
	<table cellpadding="3" cellspacing="3" border="1" class="meo_crm_analytics_summary_table">
		<tr>
			<th>id</th>
			<th>Project</th>
			<?php 
			foreach($analyticsProjectFields as $field => $value){
				
				echo '<th>'.$field.'</th>';
			}
			?>
		</tr>
		<?php 
		foreach($records as $record){
			
			$projectInfo = meo_crm_projects_getProjectInfo($record['project_id']); // depends on MEO CRM Project
			
			echo '<tr>
				<td><a href="'.admin_url().'admin.php?page=add_project&id='.$projectInfo['id'].'">'.$projectInfo['id'].'</a></td>
				<td><strong>'.$projectInfo['name'].'</strong></td>';
			
			foreach($analyticsProjectFields as $field => $value){
				
				$value = stripslashes($record[$field]);
				
				if($field == 'pdf_template' && $value != ''){
					$PDFpath = MEO_CRM_PROJECTS_FOLDER_URL . '/' . $record['project_id'] . '/' . MEO_CRM_ANALYTICS_PDF_DIRNAME . '/' . $value;
					$value = '<a href="'.$PDFpath.'" target="_blank"><i class="dashicons-before dashicons-media-text" title="'.$value.'"></i></a>';
				}
				
				echo '<td class="'.$field.'">'.$value.'</td>';
			}
			
			echo '</tr>';
		}
		
		?>
	</table>
	<?php 

}
?>