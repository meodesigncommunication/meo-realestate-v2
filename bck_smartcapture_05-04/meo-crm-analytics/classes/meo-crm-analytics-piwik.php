<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
Doc: http://developer.piwik.org/2.x/api-reference/reporting-api
*/
	
	
	#########
	# PIWIK #
	#########


class MeoCrmAnalyticsPiwik {
	
		# Make Piwik CURL call
		
		private static function makePiwikCurlCall($url, $project, $analytics_id = ''){
			
			# CURL Call
			$curl = new WP_Http_Curl();
				
			$response = $curl->request($url, array(
					'headers' => array(
							'Accept'       => 'application/json',
							'Content-type' => 'application/json',
							'user-agent' => $_SERVER['HTTP_USER_AGENT']?$_SERVER['HTTP_USER_AGENT']:''
					),
					'method' => 'GET',
					'redirection' => 0,
					'stream' => false,
					'filename' => false,
					'decompress' => false
			));
				
			# Error
			if ( is_wp_error( $response ) || empty( $response['body']) ) {
			
				MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, array(
						'error'        => 'Failed to get data from piwik (error in request or empty body)',
						'site_id'      => empty($project) ? '' : $project['id'],
						'site_name'    => empty($project) ? '' : $project['name'],
						'analytics_id' => $analytics_id,
						'url'          => $url,
						'response'     => $response
				));
				return false;
			}
			
			$result = json_decode($response['body']);
				
			# Error
			if ( !$result || empty($result) || ( is_object($result) && isset($result->result) && $result->result == "error" ) ) {
			
				MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, array(
						'error'        => 'Failed to get data from piwik (JSON object empty).  Probably analytics ID not found in piwik',
						'site_id'      => empty($project) ? '' : $project['id'],
						'site_name'    => empty($project) ? '' : $project['name'],
						'analytics_id' => $analytics_id,
						'url'          => $url,
						'response'     => $response
				));
				return false;
			}
			
			return $result;
		}
		
		# getVisitorProfileData (to test and replace getLastVisitData : Call Piwik to get last visit details)
		
		public static function getVisitorProfileData($project, $analytics_id) {
	
			# Settings for Piwik MEO Server call
			$url = 'http://' . MEO_ANALYTICS_SERVER .'/'.
					"?module=API" .
					"&method=Live.getVisitorProfile" .
					"&format=JSON" .
					"&idSite=" . $project['piwik_id'] .
					"&token_auth=" . $project['piwik_token'] .
					"&visitorId=" . $analytics_id;

			# CURL Call
			$result = self::makePiwikCurlCall($url, $project, $analytics_id);

			return $result;
		}
		
	
		# getLastVisitData : Call Piwik to get last visit details
		
		public static function getLastVisitData($project, $analytics_id) {
			
			# Settings for Piwik MEO Server call
			$url = 'http://' . MEO_ANALYTICS_SERVER .'/'.
						"?module=API" .
						"&method=Live.getLastVisitsDetails" .
						"&format=json" .
						"&idSite=" . $project['piwik_id'] .
						"&period=year" .
						"&date=today" .
						"&token_auth=" . $project['piwik_token'] .
						"&segment=visitorId==" . $analytics_id .
						"&filter_limit=1";
			
			# CURL Call
			$result = self::makePiwikCurlCall($url, $project, $analytics_id);
					
			return $result;
		}
		
		
		
		# getContactDataByAnalyticsId : 
		
		public static function getContactDataByAnalyticsId($project, $analytics_id) {
			
			global $expectedAnalyticsFields;
			
			# Get corresponding project data : Will call the IMMO site
			$development_data = meo_crm_realestate_get_development_data($project, 0, 1);
			
			$lots = false;
			if($development_data && isset($development_data->lots)){
				$lots = $development_data->lots; // TODO :: Check if we need only the Lots
			}
			
		
			$result = self::getPiwikData($project, $analytics_id, $lots);
			
			$user_url = $project['url'] . $project['api_uri'] . '?action=get_contact&api_key=' . $project['api_key'] . '&analytics_id=' . $analytics_id;
			$user_json = @file_get_contents($user_url);
			$user_data = ($user_json === false ? array() : json_decode($user_json));
			
			# Add specific expected fields to returned result
			if(is_array($user_data)){
				$user_data_field = reset($user_data);
				if(is_object($user_data_field)){
					foreach($expectedAnalyticsFields as $field) {
						$result[$field] = $user_data_field->{$field};
					}
				}
			}
			
			# Check for dowloaded lots
			$lots_downloaded = array();
			
			if(is_array($user_data)){
				foreach ($user_data as $download) {
					# We have a lot that doc has been downloaded
					if (!empty($download->lot_code)) {
						$lots_downloaded[] = $download->lot_code;
					}
					# Adjusting to most recent time
					if ($download && isset($download->submit_time) && $download->submit_time < $result['submit_time']) {
						$result['submit_time'] = $download->submit_time;
					}
				}
				$result['lots_downloaded'] = $lots_downloaded;
			}
		
			return $result;
		}
		
		
		# getPiwikData : 
		
		public static function getPiwikData($project, $analytics_id, $lots) {
			
			$result = array();
			
			/*
			 $piwik_url = self::getPiwikRequestUrl($project, $analytics_id);
			 $piwik_json = @file_get_contents($piwik_url);
			 $piwik_data = ($piwik_json === false ? array() : json_decode($piwik_json));
			 */
			# New version to get Piwik data
			
			$piwik_visitor_data = self::getPiwikVisitorProfile($project, $analytics_id);
			
			
			# Lots viewed (page statistics; post_type = lot)
			$lots_viewed = self::getPiwikVisitorLotsEmergency($piwik_visitor_data);
			$result['lots_viewed'] 	= self::filterViewedPages($project, $lots_viewed);
			
			
			# Get also level of interest per lot size
			$lotSizesData = self::getPiwikVisitorLotSizesEmergency($piwik_visitor_data);
			$result['lot_sizes'] = $lotSizesData;
			
			
			# Pages viewed (page statistics; post_type != lot)
			$pages_viewed = self::getPiwikVisitorPagesEmergency($piwik_visitor_data);
			$result['pages_viewed'] = self::filterViewedPages($project, $pages_viewed);
			
			
			# Check for dowloads
			$downloads = self::getPiwikVisitorDownloadsEmergency($piwik_visitor_data);
			$result['downloads'] = $downloads;
			
			/*
			$result['total_rooms_viewed'] = $total_rooms_viewed;
			
			# Referer
			$referer = '';
			
			if (!empty($piwik_data) and isset($piwik_data[3])) {
				foreach($piwik_data[3] as $ref) {
					if ($ref->label) {
						$referer = $ref->label;
					}
				}
			}
			$result['referer'] = $referer;
			
			# Rooms Per Lot
			$rooms_per_lots = array();
			
			if (!empty($lots)) {
				foreach ($lots as $lot) {
					if(is_object($lot) && property_exists($lot, 'pieces') && property_exists($lot, 'name')){
						$rooms_per_lots[$lot->name] = $lot->pieces;
					}
				}
			}
			$result['rooms_per_lots'] = $rooms_per_lots;
			*/
			return $result;
		}
		
		
		# getPiwikRequestUrl : Build Piwik request URL with specific params
		
		public static function getPiwikRequestUrl($project, $raw_analytics_id) {
			
			$raw_analytics_id = urldecode($raw_analytics_id);
			$raw_analytics_ids = preg_split('/,/', $raw_analytics_id);
		
			$visitor_segment = '';
			$delimiter = '';
			
			foreach ($raw_analytics_ids as $analytics_id_element) {
				
				$analytics_id = preg_replace('/[^a-z0-9A-Z]/', '', $analytics_id_element);
				
				if (!empty($analytics_id)) {
					$visitor_segment .= $delimiter . "visitorId%3D%3D" . $analytics_id;
					$delimiter = ',';
				}
			}
		
			# Preparing the URL
			
			$url = 'http://' . MEO_ANALYTICS_SERVER .'/?';
			
			# Listing params
			
			$parameters = array(
					"module" => "API",
					"method" => "API.getBulkRequest",
					"format" => "json"
			);
		
			$shared_request_parameters = array(
					"idSite"     => $project['piwik_id'],
					"token_auth" => $project['piwik_token'],
					"period"     => "year",
					"date"       => "today"
			);
		
			$parameters_by_request = array(
					# Lots IDs viewed
					'0' => array(
							"segment"  => $visitor_segment,
							"method"   => "CustomVariables.getCustomVariables",
							"expanded" => "1",
					),
		
					# Lots viewed (page statistics; post_type = lot)
					'1' => array(
							"segment"            => $visitor_segment . ";customVariablePageName2==post_type;customVariablePageValue2==lot",
							"method"             => "Actions.getPageTitles",
							"filter_sort_column" => "sum_time_spent"
					),
		
					# Other pages viewed (page statistics; post_type != lot)
					'2' => array(
							"segment"            => $visitor_segment . ";customVariablePageName2==post_type;customVariablePageValue2!=lot",
							"method"             => "Actions.getPageTitles",
							"filter_sort_column" => "sum_time_spent"
					),
		
					'3' => array(
							"segment" => $visitor_segment,
							"method"  => "Referrers.getAll"
					)
			);
			
			# Building URL
			
			foreach ($parameters as $key => $value) {
				$url .= $key . "=" . $value . "&";
			}
			
			foreach($parameters_by_request as $index => $request) {
				$request_url = '';
				
				foreach ($shared_request_parameters as $key => $value) {
					$request_url .= $key . "=" . $value . "&";
				}
				foreach ($request as $key => $value) {
					$request_url .= $key . "=" . $value . "&";
				}
				
				$request_url = rtrim($request_url, "&");
		
				$url .= "urls[" . $index . "]=" . urlencode($request_url) . "&";
			}
		
			$url = rtrim($url, "&");
		
			return $url;
		}
		
		
		# filterViewedPages : Adjust page label / info for the report
		
		public static function filterViewedPages($project, $pages) {
			$result = array();
			
			if (!empty($pages)) {
				foreach ($pages as $page) {
                    if(is_object($page)) {
                        /* if (array_key_exists('page_name_regex', $project)) {
                            $page->label = preg_replace($project['page_name_regex'], '$1', $page->label);
                        }
                        if (isset($page->label) && $page->label === "parall�le") {
                            $page->label = "plan masse";
                        }*/
                        if (array_key_exists('name', $project)) {
                            $page->label = str_replace(' - '.$project['name'], '', $page->label);
                        }
                        /*
                        if (array_key_exists('page_name_exclusions', $project) && in_array(trim($page->label), $project['page_name_exclusions'])) {
                            continue;
                        }
                        */
                        if (strpos($page->label, 'File Request') !== false) {
                        	continue;
                        }

                        $result[] = $page;
                    }
				}
			}
			
			return $result;
		}
		
		
		# getNbVisitsPerReferer : Call Piwik to get number of visits per referer
		
		public static function getNbVisitsPerReferer($project, $date = 'today', $period = 'day') {
			
			if(!$project || !array_key_exists('piwik_id', $project) || $project['piwik_id'] == '' || $project['piwik_id'] == 0){
				return false;
			}
			
			# Settings for Piwik MEO Server call
			$url = 'http://' . MEO_ANALYTICS_SERVER .'/'.
					"?module=API" .
					"&method=API.getBulkRequest" .
					"&format=json" .
					"&token_auth=" . $project['piwik_token'] .
					"&urls[0]=method%3dReferrers.getWebsites".
					"%26idSite%3d". $project['piwik_id'] ."".
					"%26date%3d". $date ."".
					"%26period%3d" . $period ."";
			
			# CURL Call
			$result = self::makePiwikCurlCall($url, $project);
			
			if(!$result){
				return false;
			}
			

			$refererVisits = array();
			
			
			foreach($result[0] as $r => $refererData){
				$refererVisits[$refererData->label] = array(
						'nb_visits' => $refererData->nb_visits,
						'nb_visits_converted' => $refererData->nb_visits_converted
				);
			}
			
			# Add info of Direct Visits
			# Settings for Piwik MEO Server call
			$url = 'http://' . MEO_ANALYTICS_SERVER .'/'.
					"?module=API" .
					"&method=API.getBulkRequest" .
					"&format=json" .
					"&token_auth=" . $project['piwik_token'] .
					"&urls[0]=method%3dReferrers.getReferrerType".
					"%26idSite%3d". $project['piwik_id'] ."".
					"%26date%3d". $date ."".
					"%26period%3d" . $period ."";
				
			# CURL Call
			$result = self::makePiwikCurlCall($url, $project);
			
			if($result && is_array($result)){
				$typeReferer = reset($result);
                if(is_object($typeReferer)){
                    foreach($typeReferer as $r => $tRefererObj){
                        if($tRefererObj->label == 'Direct Entry'){
                            $refererVisits['Direct'] = array(
                                    'nb_visits' => $tRefererObj->nb_visits,
                                    'nb_visits_converted' => $tRefererObj->nb_visits_converted
                            );
                        }
                    }
				}
			}
		
			return $refererVisits;
		}		
		
		
		# getNbVisits : Call Piwik to get number of visits
		
		public static function getNbVisits($project, $date = 'today', $period = 'day') {
				
			if(!$project || !array_key_exists('piwik_id', $project) || $project['piwik_id'] == '' || $project['piwik_id'] == 0){
				return false;
			}
				
			# Settings for Piwik MEO Server call
			$url = 'http://' . MEO_ANALYTICS_SERVER .'/'.
					"?module=API" .
					"&method=API.getBulkRequest" .
					"&format=json" .
					"&token_auth=" . $project['piwik_token'] .
					"&urls[0]=method%3dVisitsSummary.get" .
					"%26idSite%3d". $project['piwik_id'] ."" .
					"%26date%3d". $date ."" .
					"%26period%3d" . $period ."";
				
			# CURL Call
			$result = self::makePiwikCurlCall($url, $project);
				
			if(!$result){
				return false;
			}
				
		
			$nbVisitsArray = array(
				'nb_visits' => $result[0]->nb_visits,
				'nb_visits_converted' => $result[0]->nb_visits_converted
			);
			
			return $nbVisitsArray;
		}
		
		# Get Piwik Visitor Profile
		
		public static function getPiwikVisitorProfile($project, $analytics_id){
			
			# Live.getVisitorProfile
			$pages_piwik_url = 'http://' . MEO_ANALYTICS_SERVER .'/?module=API&'.
					'method=Live.getVisitorProfile&'.
					'idSite='.$project['piwik_id'].'&format=JSON&'.
					'token_auth='.$project['piwik_token'].'&'.
					'visitorId='.$analytics_id;
			$pages_piwik_json = @file_get_contents($pages_piwik_url);
			$pages_piwik_data = ($pages_piwik_json === false ? false : json_decode($pages_piwik_json));
			
			return $pages_piwik_data;
		}
		
		# Function to get called in case some Piwik data is missing
		
		public static function getPiwikVisitorPagesEmergency($piwik_visitor_data){

			if($piwik_visitor_data && isset($piwik_visitor_data->lastVisits) && is_array($piwik_visitor_data->lastVisits)){
	
				# Use the 3 first visits data
				$vIndex = 0;
				$globalVisit = array();

				foreach($piwik_visitor_data->lastVisits as $v => $visit){
					$vIndex++;
					if($visit && isset($visit->actionDetails) && is_array($visit->actionDetails)){
	
						foreach($visit->actionDetails as $pageAction){
	
							if($pageAction->type == 'action'){
								# Page hasn't been visited already
								if(!array_key_exists($pageAction->pageIdAction, $globalVisit)){
	
									$pageObject = new stdClass();
									$pageObject->label = $pageAction->pageTitle;
									$pageObject->nb_visits = 1;
									$pageObject->nb_hits = 1;

									if(isset($pageAction->timeSpent)){
										$pageObject->sum_time_spent = $pageAction->timeSpent;
									}
									else $pageObject->sum_time_spent = 1; // 1sec per default instead of 0



									$globalVisit[$pageAction->pageIdAction] = $pageObject;
								}
								# Page already visited
								else {
									if(isset($pageAction->timeSpent)){
											$globalVisit[$pageAction->pageIdAction]->sum_time_spent += $pageAction->timeSpent;
										}
										$globalVisit[$pageAction->pageIdAction]->nb_hits += 1;
									}
								}
	
							}
						}
						if($vIndex == 3) break;
					}
	
					// Obtain a list of columns
					$pageLabel = array();
					$pageTime = array();
					$pageNbVisits = array();
					$pageNbHits = array();
					foreach ($globalVisit as $key => $row) {
						$pageLabel[$key]  = $row->label;
						$pageTime[$key] = $row->sum_time_spent;
						$pageNbVisits[$key] = $row->nb_visits;
						$pageNbHits[$key] = $row->nb_hits;
					}
	
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($pageTime, SORT_DESC, $pageLabel, SORT_ASC, $pageNbVisits, SORT_DESC, $pageNbHits, SORT_ASC, $globalVisit);
	
					return $globalVisit;
				}
				return array();
			}
	
			# Lots
			public static function getPiwikVisitorLotsEmergency($piwik_visitor_data){
				
				if($piwik_visitor_data && isset($piwik_visitor_data->lastVisits) && is_array($piwik_visitor_data->lastVisits)){
		
					# Use the 3 first visits data
					$vIndex = 0;
					$globalVisit = array();
	
					foreach($piwik_visitor_data->lastVisits as $v => $visit){
						$vIndex++;
						if($visit && isset($visit->actionDetails) && is_array($visit->actionDetails)){
		
							foreach($visit->actionDetails as $pageAction){
		
								if($pageAction->type == 'event' && $pageAction->eventCategory == 'Lot Visit'){
		
									# Page hasn't been visited already
									if(!array_key_exists($pageAction->eventAction, $globalVisit)){
		
										$pageObject = new stdClass();
										$pageObject->label = $pageAction->eventAction;
										$pageObject->nb_visits = 1;
										$pageObject->nb_hits = 1;
										$pageObject->nb_rooms = $pageAction->eventName;
	
										if(isset($pageAction->timeSpent)){
											$pageObject->sum_time_spent = $pageAction->timeSpent;
										}
										else $pageObject->sum_time_spent = 1; // 1sec per default instead of 0
	
	
	
										$globalVisit[$pageAction->eventAction] = $pageObject;
									}
									# Page already visited
									else {
										if(isset($pageAction->timeSpent)){
											$globalVisit[$pageAction->eventAction]->sum_time_spent += $pageAction->timeSpent;
										}
										$globalVisit[$pageAction->eventAction]->nb_hits += 1;
									}
								}
	
							}
						}
						if($vIndex == 3) break;
					}
	
					// Obtain a list of columns
					$pageLabel = array();
					$pageTime = array();
					$pageNbVisits = array();
					$pageNbHits = array();
					foreach ($globalVisit as $key => $row) {
						$pageLabel[$key]  = $row->label;
						$pageTime[$key] = $row->sum_time_spent;
						$pageNbVisits[$key] = $row->nb_visits;
						$pageNbHits[$key] = $row->nb_hits;
					}
	
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($pageTime, SORT_DESC, $pageLabel, SORT_ASC, $pageNbVisits, SORT_DESC, $pageNbHits, SORT_ASC, $globalVisit);
	
					return $globalVisit;
				}
				return array();
			}
			
			# Lot Sizes = Level of Interest
			public static function getPiwikVisitorLotSizesEmergency($piwik_visitor_data){
				
				if($piwik_visitor_data && isset($piwik_visitor_data->lastVisits) && is_array($piwik_visitor_data->lastVisits)){
			
					# Use the 3 first visits data
					$vIndex = 0;
					$globalVisit = array();
			
					foreach($piwik_visitor_data->lastVisits as $v => $visit){
						$vIndex++;
						if($visit && isset($visit->actionDetails) && is_array($visit->actionDetails)){
			
							foreach($visit->actionDetails as $pageAction){
			
								if($pageAction->type == 'event' && $pageAction->eventCategory == 'Lot Visit'){
			
									# Page hasn't been visited already
									if(!array_key_exists($pageAction->eventName, $globalVisit)){
			
										$pageObject = new stdClass();
										$pageObject->label = $pageAction->eventName;
										$pageObject->nb_visits = 1;
										$pageObject->nb_hits = 1;
			
										if(isset($pageAction->timeSpent)){
											$pageObject->sum_time_spent = $pageAction->timeSpent;
										}
										else $pageObject->sum_time_spent = 1; // 1sec per default instead of 0
			
			
			
										$globalVisit[$pageAction->eventName] = $pageObject;
									}
									# Page already visited
									else {
										if(isset($pageAction->timeSpent)){
											$globalVisit[$pageAction->eventName]->sum_time_spent += $pageAction->timeSpent;
										}
										$globalVisit[$pageAction->eventName]->nb_hits += 1;
									}
								}
			
							}
						}
						if($vIndex == 3) break;
					}
			
					// Obtain a list of columns
					$pageLabel = array();
					$pageTime = array();
					$pageNbVisits = array();
					$pageNbHits = array();
					foreach ($globalVisit as $key => $row) {
						$pageLabel[$key]  = $row->label;
						$pageTime[$key] = $row->sum_time_spent;
						$pageNbVisits[$key] = $row->nb_visits;
						$pageNbHits[$key] = $row->nb_hits;
					}
			
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($pageTime, SORT_DESC, $pageLabel, SORT_ASC, $pageNbVisits, SORT_DESC, $pageNbHits, SORT_ASC, $globalVisit);
			
					return $globalVisit;
				}
				return array();
			}
			
			# Downloads
			public static function getPiwikVisitorDownloadsEmergency($piwik_visitor_data){
				
				if($piwik_visitor_data && isset($piwik_visitor_data->lastVisits) && is_array($piwik_visitor_data->lastVisits)){
			
					# Use the 3 first visits data
					$vIndex = 0;
					$globalVisit = array();
			
					foreach($piwik_visitor_data->lastVisits as $v => $visit){
						$vIndex++;
						if($visit && isset($visit->actionDetails) && is_array($visit->actionDetails)){
			
							foreach($visit->actionDetails as $pageAction){
			
								if($pageAction->type == 'event' && $pageAction->eventCategory == 'download'){
			
									# Page hasn't been visited already
									if(!array_key_exists($pageAction->eventAction, $globalVisit)){
			
										$pageObject = new stdClass();
										$pageObject->label = $pageAction->eventAction;
										$pageObject->nb_visits = 1;
										$pageObject->nb_hits = 1;
			
										if(isset($pageAction->timeSpent)){
											$pageObject->sum_time_spent = $pageAction->timeSpent;
										}
										else $pageObject->sum_time_spent = 1; // 1sec per default instead of 0
			
			
			
										$globalVisit[$pageAction->eventAction] = $pageObject;
									}
									# Page already visited
									else {
										if(isset($pageAction->timeSpent)){
											$globalVisit[$pageAction->eventAction]->sum_time_spent += $pageAction->timeSpent;
										}
										$globalVisit[$pageAction->eventAction]->nb_hits += 1;
									}
								}
			
							}
						}
						if($vIndex == 3) break;
					}
			
					// Obtain a list of columns
					$pageLabel = array();
					$pageTime = array();
					$pageNbVisits = array();
					$pageNbHits = array();
					foreach ($globalVisit as $key => $row) {
						$pageLabel[$key]  = $row->label;
						$pageTime[$key] = $row->sum_time_spent;
						$pageNbVisits[$key] = $row->nb_visits;
						$pageNbHits[$key] = $row->nb_hits;
					}
			
					// Sort the data with volume descending, edition ascending
					// Add $data as the last parameter, to sort by the common key
					array_multisort($pageTime, SORT_DESC, $pageLabel, SORT_ASC, $pageNbVisits, SORT_DESC, $pageNbHits, SORT_ASC, $globalVisit);
			
					return $globalVisit;
				}
				return array();
			}
}
