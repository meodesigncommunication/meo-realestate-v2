// This file is used to be called on all pages
// The js.php file is only called on statistics pages manually

function meoAnalyticsSaveProjectField(project_id, field, btn){
	
	jQuery(btn).siblings('.saved-project-field').fadeOut();
	
	// Get expected field value
	var valueToSave = jQuery(btn).siblings('#'+field).val();
	
	// 1@3.56
	if(valueToSave.length > 6){
		var data = {	'action': 'meo_crm_analytics_saveProjectField',
						'project_id': project_id,
						'field': field,
						'value': valueToSave
					};
		// Save content
		jQuery.post(meo_crm_analytics_ajax.ajax_url, data, function(response) {
			
			jQuery(btn).siblings('.saved-project-field').html(response).fadeIn();
		});
	}
		
}