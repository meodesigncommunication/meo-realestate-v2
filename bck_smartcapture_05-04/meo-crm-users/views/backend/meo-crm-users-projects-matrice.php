<?php
global $wpdb;
$check = true;
$errors = array();
$userModel = new UsersModel();
$clients = meo_crm_users_getClients();
$meoCrmUsersHelper = new MeoCrmUsersHelper();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $matrix_datas = $_POST['matrix_datas'];
    
    foreach($matrix_datas as $matrix_data)
    {
        list($project_id, $broker_id) = explode('_', $matrix_data);
        ProjectModel::deleteUserProjectByUser($broker_id);
    }
    
    foreach($matrix_datas as $matrix_data)
    {
        $datas = array();
        list($project_id, $broker_id) = explode('_', $matrix_data); 
        $datas['project_id'] = $project_id;
        $datas['user_id'] = $broker_id;
        $result = ProjectModel::insertProjectUser($datas);
        if(!$result['valid'])
        {
            $check = false;
            $errors[] = 'Insert project user error id_user = '.$broker_id.' AND project_id = '.$project_id;
        }
    }
    
    if($check)
    {
        $message = 'Modification de la matrice effectu&eacute; avec succ&egrave;s';
        $show_message .= $meoCrmUsersHelper->flashMessage($message,1);
    }else{
        $show_message .= $meoCrmUsersHelper->flashMessage($errors,0);
    }    
}


?>
<div class="wrap meo-crm-users-list">
    <?php echo $show_message; ?>
    <label for="client_list">Clients&nbsp;</label>
    <select id="client_list" name="client_list">
        <option value="0">Choose the client</option>
        <?php foreach($clients as $client): ?>
            <option value="<?php echo $client->ID ?>"><?php echo $client->display_name ?></option>
        <?php endforeach; ?>
    </select>
    <form method="POST">
        <div id="matrix-container">
            
        </div>
        <p class="submit">
            <input id="savematrix" class="button button-primary" type="submit" name="savematrix" value="Enregistrer la matrice">
        </p>
    </form>
</div>


