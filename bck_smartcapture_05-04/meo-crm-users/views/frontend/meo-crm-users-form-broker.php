<?php

global $current_user, $expectedUserFields;

get_header();

if(!isset($current_user) || !$current_user || (!current_user_can('administrator') && !current_user_can(CLIENT_ROLE))){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access courtier add/edit with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
}
else {
	
	# Init
	$courtier_id 			= false;
	$show_message 			= '';
	$pageTitle 				= 'Ajouter un courtier';
	$values 				= array();
	foreach($expectedUserFields as $field => $fieldData){
		$values[$field] 	= false;
	}
	$values['client_id'] 	= false;
	$meoCrmUsersHelper 		= new MeoCrmUsersHelper();
	
	# Courtier Edition
	if(isset($_GET['courtier_id']) && !empty($_GET['courtier_id'])){
	    $courtier_id 		= $_GET['courtier_id'];
	    $pageTitle 			= 'Modifier un courtier';
	}
	
	# Submitted Form
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		# Add/Update (return interger courtierID or array of errors)
	    $result = meo_crm_users_processUserForm($_POST);
	    
	    # Everything is ok
	    if(is_numeric($result)) {
	    	
	    	$courtier_id = $result;
	    	
	    	$show_message .= $meoCrmUsersHelper->flashMessage($pageTitle.': Op&eacute;ration r&eacute;ussie.', 1);
	    	
	        ProjectModel::deleteUserProjectByUser($courtier_id);
	        
	        foreach($_POST['projects'] as $project_id) {
	        	
	            $data = array( 'project_id' => $project_id, 'user_id' => $courtier_id );
	            
	            if(!ProjectModel::insertProjectUser($data)) {
	            	$show_message .= $meoCrmUsersHelper->flashMessage('Probl&egrave;me lors de l\'enregistrement du projet ['.$projet_id.']', 0);
	            }
	            
	            apply_filters('meo_crm_users_updateUserProjectData', $courtier_id, $project_id);
	        }
	    }
	    # We have errors
	    else {
	    	
	    	if(is_array($result)){
	    		
	    		foreach($result as $error) {
		
		    		$show_message .= $meoCrmUsersHelper->flashMessage($error, 0);
				}
	    	}
	    	else $show_message .= $meoCrmUsersHelper->flashMessage('<strong>ERREUR : </strong>Une erreur s\'est produite lors de la modification', 0);
	    }
	} // /submitted
	
	$clientProjects = array();
	
	# Get Courtier info if any
	if($courtier_id) {
		
		$courtier = meo_crm_users_getCourtierDataById($courtier_id);
		
		# Init values for the form
		$values['id'] = $courtier_id;
		
		foreach($expectedUserFields as $field => $fieldData){
			if(array_key_exists($field, $courtier)){
				$values[$field] = $courtier[$field];
			}
		}
		
		# Get projects of the corresponding Client
		if(array_key_exists('client_id', $courtier) && $courtier['client_id'] > 0){
			$values['client_id'] = $courtier['client_id'];
			$clientProjects = meo_crm_projects_getProjectsByUserID($courtier['client_id']);
		}
		
	}
	# ADD/DEFAULT
	else {
		# Get current Client Projects
		if(current_user_can(CLIENT_ROLE)){
			$clientProjects = meo_crm_projects_getProjectsByUserID($current_user->ID);
		}
	}
}

# Building Page
?>

<div class="wrap meo-crm-users-list">

    <?php 
    echo (!empty($show_message))? '<div class="warning-text">'.$show_message.'</div>' : '';
    echo '<h1>'.$pageTitle.'</h1>'; 
    ?>
    
    <form id="form_broker" method="POST" name="from_broker">
    
        <div style="width: 90%">
            <input id="courtier_id" class="" type="hidden" value="<?php echo $courtier_id; ?>" placeholder="" name="courtier_id" />
            
            <?php 
            foreach($expectedUserFields as $field => $fieldData){
            
            	echo '<div class="form-group">
		                <label for="'.$field.'">'.$fieldData['label'].'</label>';
            	if($fieldData['type'] == 'text'){
            		echo '<input id="'.$field.'" class="form-control" type="text" value="'.$values[$field].'" placeholder="'.$fieldData['label'].'" name="'.$field.'" />';
            	}
            	else if($fieldData['type'] == 'textarea'){
            		echo '<textarea class="form-control" id="'.$field.'" name="'.$field.'">'.$values[$field].'</textarea>';
            	}
		                
		        echo '</div>';
            }
            ?>
            
            <div class="form-group">
                <label for="user_pass">Password</label>
                <input id="user_pass1" class="form-control" type="password" name="user_pass1" autocomplete="off" style="display:none;" />
                <input id="user_pass" class="form-control" type="password" name="user_pass" autocomplete="off">
            </div>
            
            <div class="form-group">
                <label for="conf_user_pass">Confirmation password</label>
                <input id="conf_user_pass" class="form-control" type="password" name="conf_user_pass" />
            </div>
            
            <?php 
            if(current_user_can('administrator')){ 
            ?>
                <div class="form-group">
                    <label for="conf_user_pass">Client [Admin Only]</label>
                    <select name="client_id" id="client_id" class="form-control">
                        <option value="0">Choose a client to link</option>
                        <?php
                            $clients = meo_crm_users_getClients();
                            
                            foreach ($clients as $key => $client){
                            	$selected = '';
                            	$disabled = ' disabled';
                            	# Client has Projects
                            	if(array_key_exists('projects', $client) && is_array($client['projects']) && count($client['projects']) > 0){
                            		$disabled = '';
                            	}
                                if(array_key_exists('client_id', $values) && $values['client_id'] == $client['ID']){
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$client['ID'].'"'.$selected.''.$disabled.'>'.$client['display_name'].'</option>';
                            }
                        ?>
                    </select>
                </div>
            <?php 
            }
            else if(current_user_can(CLIENT_ROLE)){
            	?>
            	<div class="form-group">
	                <input id="client_id" class="form-control" type="hidden" value="<?php echo $current_user->ID; ?>" name="client_id" />
	            </div>
            	<?php 
            }
            ?>
            
            <div class="form-group">
                <label for="projects">Projects</label>
                <div id="client_projects">
                    <?php 
                    # It should always be populated
					if(count($clientProjects) > 0){
                    	
						foreach($clientProjects as $project){
	                    	
							$checked = '';
							
							if( $courtier_id && array_key_exists('projects', $courtier) && is_array($courtier['projects']) && array_key_exists($project['id'], $courtier['projects']) ){
								$checked = ' checked';
							}
							
							?>
								<div class="form-group">                    
                                    <input type="checkbox" id="project_<?php echo $project['id']; ?>" name="projects[]" value="<?php echo $project['id']; ?>"<?php echo $checked; ?> />
                                    <label for="project_<?php echo $project['id']; ?>"><?php echo $project['name']; ?></label>
                                </div>
                            <?php 
							
						}
						
					}
                    ?>
                </div>
            </div>
        </div>
        <p class="submit">
            <input id="createusersub" class="button button-primary" type="submit" name="createuser" value="<?php echo $pageTitle ?>" />
        </p>
    </form>
</div>
<?php 
get_footer(); 
?>