<?php
/**
 * Template frontend permettant d'afficher la liste des Courtiers (par admin_client sauf si meo)
 * 
 * Author : MEO Design & Communication
 * Date   : 12/04/2016
 * 
 */

// Variables
global $current_user;

get_header();

if(!isset($current_user) || !$current_user || (!current_user_can('administrator') && !current_user_can(CLIENT_ROLE))){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access courtier list with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
}
else {
	$meoCrmUsersHelper = new MeoCrmUsersHelper();
	
	$courtiers = meo_crm_users_getCourtiers();
	$clients = meo_crm_users_getClients();
	
	get_header(); 
	?>
	    <input id="client_id" type="hidden" value="<?php echo $current_user->ID; ?>">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wrap">
                                <h2>Courtiers</h2>
                            </div>
                            <div id="meo-toolbar">
                                <a href="<?php echo home_url().'/'.MEO_CRM_USERS_EDIT_USER_URL.'/'; ?>" title="" class="meo-btn-toolbar"><i class="fa fa-plus"></i></a>
                                <div class="meo-search-toolbar">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                            <input id="search" type="text" class="form-control" placeholder="Rechercher">
                                        </div>
                                        <button type="button" id="btn_search" class="btn btn-primary">Rechercher</button>
                                        <div class="clear-both"></div>
                                    </div>
                                </div>
                                <div class="clear-both"></div>
                            </div>
                            <table id="brokers_table" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><input class="checkboxBroker" type="checkbox" name="checkedAll"/></th>
                                        <th>#ID</th>
                                        <th>Nom complet</th>
                                        <th>Adresse email</th>
                                        <th>Identifiant</th>
                                        <?php if(current_user_can('administrator')) { ?><th>Client</th><?php } ?>
                                        <th>Projets</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th><input class="checkboxBroker" type="checkbox" name="checkedAll"/></th>
                                        <th>#ID</th>
                                        <th>Nom complet</th>
                                        <th>Adresse email</th>
                                        <th>Identifiant</th>
                                        <?php if(current_user_can('administrator')) { ?><th>Client</th><?php } ?>
                                        <th>Projets</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php 
                                    $html = '';
                                    foreach($courtiers as $courtier)
                                    {
                                    	$html .= '<tr>
                                    				<td><input class="checkboxBroker" type="checkbox" name="user[]" value="'.$courtier['ID'].'" /></td>
                                    				<td>'.$courtier['ID'].'</td>
													<td>'.$courtier['display_name'].'</td>
													<td>'.$courtier['user_email'].'</td>
													<td>'.$courtier['user_login'].'</td>
													<td>';
                                    	
                                    	if(current_user_can('administrator')){
                                    		
                                    		if(array_key_exists('client_id', $courtier) && $courtier['client_id'] > 0){
                                    			
                                    			$client = meo_crm_users_getClientDataById($courtier['client_id']);
                                    			$html.= $client['display_name'];
                                    		}
                                    		
                                    		$html.= '</td>
                                        			 <td>';
                                    	}
                                    	
                                    	if(array_key_exists('projects', $courtier) && is_array($courtier['projects'])){
                                    		$html.= '<ul class="users-projects-courtier-list">';
                                    		
                                    		foreach($courtier['projects'] as $pid => $pname){
                                    			$html.= '<li title="'.$pid.'"><strong>'.$pname.'</strong> - <em>'.meo_crm_contacts_countContactsByUserIDandProjectID($courtier['ID'], $pid).' contacts</em></li>';
                                    		}
                                    		
                                    		$html.= '</ul>';
                                    	}
                                    	
                                    	$html .=    '</td>
        											<td>
														<a href="'.home_url().'/'.MEO_CRM_USERS_EDIT_USER_URL.'?courtier_id='.$courtier['ID'].'" class="button action mr5px"><i class="fa fa-pencil"></i></a>
    													<a onclick="deleteBroker('.$courtier['ID'].')" class="button action meo-btn-list-action meo-btn-trash"><i class="fa fa-trash"></i></a>
        											</td>
        										  </tr>';
                                    }
                                    echo $html;
                                    ?>  
                                </tbody>
                            </table> 
                            <div class="meo-table-action">
                                <select class="form-control" name="list-action" id="list-action">
                                    <option value="-1">Actions group&eacute;es</option>
                                    <option value="1">Supprimer la s&eacute;l&eacute;ction</option>
                                </select>
                                <button id='list_action' type="button" class="btn btn-primary">Appliquer</button>
                                <div class="clear-both"></div>
                            </div>
                        </div>
                    </div>
			</main>
		</div>
	<?php 
}
get_footer(); 
?>