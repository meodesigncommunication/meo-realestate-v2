<?php

class UsersModel
{

	
    public static function selectAllUsers($show_meta = false)
    {
        global $wpdb;
        $user = array();        
        
        
        $query  = 'SELECT * 
        			FROM '.$wpdb->prefix . WP_USERS_TABLE.' AS u 
        			LEFT JOIN '.$wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE.' AS um ON um.user_id=u.id ';
        $results = $wpdb->get_results($query);     
        
        // Ajout au tableau user des infos
        foreach($results as $result){
            $user[$result->ID]['ID']  = $result->ID;
            $user[$result->ID]['display_name']  = $result->display_name;
            $user[$result->ID]['nickname']  = $result->user_login;
            $user[$result->ID]['email']  = $result->user_email;
            $user[$result->ID]['password']  = (isset($result->user_pass))? $result->user_pass : '';
            $user[$result->ID]['client_id']  = $result->client_id;
            
            if($show_meta){
                // Séléction de la table usermeta
                $query  = 'SELECT * 
                			FROM '.$wpdb->prefix . WP_USERMETA_TABLE.' AS um 
                				WHERE um.user_id='.$result->ID.' 
                					AND (meta_key="first_name" 
                					OR meta_key="last_name" 
                					OR meta_key="signature") ';
                $resultMeta = $wpdb->get_results($query); 

                // Ajout dans le tableau user (prénom,nom)
                foreach($resultMeta as $usermeta){
                    $user[$result->ID][$usermeta->meta_key]  = $usermeta->meta_value;
                }  
            }             
        }                  
        
        return $user;   
    }
    /*
     * Permet de séléctionner un utillisateur
     * 
     * @param $id(Integer)
     * @return $user (Array)
     */
    public function selectUser($id)
    {
        global $wpdb;
        $user = array();        
        
        // Séléction de la table users
        $query  = 'SELECT * 
        			FROM '.$wpdb->prefix . WP_USERS_TABLE.' AS u 
        			LEFT JOIN '.$wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE.' AS um ON um.user_id=u.id 
        				WHERE u.id='.$id;
        $result = $wpdb->get_results($query);
        
        // Ajout au tableau user des infos
        $user['nickname']  = $result[0]->user_login;
        $user['email']  = $result[0]->user_email;
        $user['password']  = $result[0]->user_pass;
        $user['client_id']  = $result[0]->client_id;   
        
        // Séléction de la table usermeta
        $query  = 'SELECT * 
        			FROM '.$wpdb->prefix . WP_USERMETA_TABLE.' AS um 
        				WHERE um.user_id='.$id.' 
        					AND (meta_key="first_name" 
        					OR meta_key="last_name" 
        					OR meta_key="signature") ';
        $resultMeta = $wpdb->get_results($query); 
        
        // Ajout dans le tableau user (prénom,nom)
        foreach($resultMeta as $usermeta)
        {
            $user[$usermeta->meta_key]  = $usermeta->meta_value;
        }      
        
        return $user;
    }
}

