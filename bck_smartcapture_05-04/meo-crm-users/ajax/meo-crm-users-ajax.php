<?php 


/*
 * Suppression d'un courtier
*/
add_action( 'wp_ajax_delete_broker', 'delete_broker' );
function delete_broker()
{
	// Variables
	global $wpdb;
	$id = $_POST['id'];
	$link_admin = get_admin_url().'admin.php';
	$link_add_broker = $link_admin.'?page=add_manage_broker';

	// Class
	$meoCrmUsersHelper = new MeoCrmUsersHelper();
	$meoCrmUsersTableHelper = new MeoCrmUsersTableHelper();

	// Execution des requêtes de suppression
	$wpdb->query('START TRANSACTION');
	$checkUserDelete = MeoCrmUsers::deleteUser($id);
	$checkUserMetaDelete = MeoCrmUsers::deleteUserMeta($id);
	$checkUserManageDelete = MeoCrmUsers::deleteUserManagement($id);

	if($checkUserDelete && $checkUserMetaDelete && $checkUserManageDelete)
	{
		$wpdb->query('COMMIT');
		$message = 'L\'utilisateur a &eacute;t&eacute; correctement supprim&eacute;';
		$htmlMessage = $meoCrmUsersHelper->flashMessage($message,true);
	}else{
		$wpdb->query('ROLLBACK');
		$message = '<strong>ERREUR : </strong>L\'utilisateur n\'a pas pu être supprim&eacute;';
		$htmlMessage = $meoCrmUsersHelper->flashMessage($message,false);
	}

	// Init Datas
	$meoCrmUsersTableHelper->datas = meo_crm_users_getCourtiers();

	$table = $meoCrmUsersTableHelper->generateListTable($link_add_broker);

	$result = array(
			'message' => $htmlMessage,
			'table' => $table
	);

	echo json_encode($result);

	die();
}

/*
 * Suppression de plusieurs courtiers
 */
add_action( 'wp_ajax_multiple_delete_broker', 'multiple_delete_broker' );
function multiple_delete_broker() {

	global $wpdb;
	$validate = true;
	$htmlMessage = '';
	$list_ids = $_POST['ids'];
	$doaction = $_POST['doaction'];
	$link_admin = get_admin_url().'admin.php';
	$link_add_broker = $link_admin.'?page=add_manage_broker';

	// Class
	$meoCrmUsersHelper = new MeoCrmUsersHelper();
	$meoCrmUsersTableHelper = new MeoCrmUsersTableHelper();

	// Boucle pour la suppression de la liste des ids
	foreach ($list_ids['list'] as $data)
	{
		$wpdb->query('START TRANSACTION');
		$checkUserDelete = MeoCrmUsers::deleteUser($data['id']);
		$checkUserMetaDelete = MeoCrmUsers::deleteUserMeta($data['id']);
		$checkUserManageDelete = MeoCrmUsers::deleteUserManagement($data['id']);

		if(!$checkUserDelete || !$checkUserMetaDelete || !$checkUserManageDelete)
		{
			$validate = false;
		}
	}

	// Contrôle si les requêtes se sont bien ex&eacute;cut&eacute;
	if($validate)
	{
		$wpdb->query('COMMIT');
		$message = 'Les utilisateurs ont &eacute;t&eacute; correctement supprim&eacute;s';
		$htmlMessage = $meoCrmUsersHelper->flashMessage($message,true);
	}else{
		$wpdb->query('ROLLBACK');
		$message = '<strong>ERREUR : </strong>Les utilisateurs n\'ont pas pu être correctement supprim&eacute;s';
		$htmlMessage = $meoCrmUsersHelper->flashMessage($message,false);
	}

	// Init Datas
	$meoCrmUsersTableHelper->datas = meo_crm_users_getCourtiers();

	$table = $meoCrmUsersTableHelper->generateListTable($link_add_broker);

	$result = array(
			'message' => $htmlMessage,
			'table' => $table
	);

	echo json_encode($result);

	die();
}

/*
 * Rechercher dans la liste des courtier
 */
add_action( 'wp_ajax_list_search', 'list_search' );
function list_search()
{
	global $wpdb;
	$search = $_POST['search'];
	$client_id = $_POST['client_id'];
	$link_admin = get_admin_url().'admin.php';
	$link_add_broker = $link_admin.'?page=add_manage_broker';

	// Class
	$meoCrmUsersTableHelper = new MeoCrmUsersTableHelper();

	$meoCrmUsersTableHelper->datas = meo_crm_users_getCourtiersBySearch($search);

	echo $meoCrmUsersTableHelper->generateListTable($link_add_broker);

	die();
}

/*
 * Rechercher dans la liste des courtier
 */
add_action( 'wp_ajax_meo_crm_users_loadProject', 'meo_crm_users_loadProject' );
function meo_crm_users_loadProject()
{
	
	$client_id = $_POST['client_id'];
	
	if($client_id){
		
		$html = '';
		$courtierProjects = array();
		$clientProjects = meo_crm_projects_getProjectsByUserID($client_id);
		
		if(!empty($_POST['courtier_id'])){
			$courtierProjects = meo_crm_projects_getProjectsByUserID($_POST['courtier_id']);
		}
	
		foreach($clientProjects as $cp => $cProject){
			
			$checked = '';
			if(is_array($courtierProjects) && array_key_exists($cProject['id'], $courtierProjects)){
				$checked = ' checked';
			}
			
			$html.= '<div class="form-group">
						<input type="checkbox" id="project_'.$cProject['id'].'" name="projects[]" value="'.$cProject['id'].'"'.$checked.' />&nbsp;
						<label for="project_'.$cProject['id'].'">'.$cProject['name'].'</label>
					</div>';
			
		}
		echo $html;
	}
		
	wp_die();
}


/*
 * Cr&eacute;er la matrice de droit des utilisateur sur les project

add_action( 'wp_ajax_meo_crm_users_getCourtiersList', 'meo_crm_users_getCourtiersList' );

	function meo_crm_users_getCourtiersList(){
		
		# Init
		$clientId = $_POST['clientId'];
		
		if($clientId){
			
			// Get necessary datas for create the matrix
			$courtiers = meo_crm_users_getCourtiersByClientID($clientId);
			$projects = meo_crm_projects_getProjectsByUserID($clientId);
		
			$html  = '<table id="matrix-user-project">
						<thead>
							<tr>
								<td>&nbsp;</td>';
			
								foreach($courtiers as $courtier){
									$html .= '<td>'.$courtier['display_name'].'</td>';
								}
					
			$html .=        '</tr>
						</thead>
						<tbody>';
			
					foreach($projects as $project){
						
						$html .= '<tr>
									<td>'.$project['name'].'</td>';
						
							foreach($courtiers as $courtier){
								$checked = '';
								# Courtier has project?
								if($courtier && array_key_exists('projects', $courtier) && is_array($courtier['projects']) && array_key_exists($project['id'], $courtier['projects'])){
									$checked = ' checked';
								}
								$html .= '<td><input type="checkbox" name="matrix_datas[]" value="'.$project['id'].'_'.$courtier['ID'].'" '.$checked.' /></td>';
							}
					}
					
			$html .=    '</tbody>
					</table>';
		
			echo $html;
		}
	
		wp_die();
	}
 */

/*
 * Rechercher dans la liste des courtier
 */
add_action( 'wp_ajax_list_search_front', 'list_search_front' );
function list_search_front()
{
	global $wpdb;
	$search = $_POST['search'];
	$client_id = $_POST['client_id'];

	$meoCrmUsersFrontTableHelper = new MeoCrmUsersFrontTableHelper();
	$courtiers = meo_crm_users_getCourtiersBySearch($search);
	
	$html = '';
	foreach($courtiers as $c => $courtier)
	{
		$html .= '<tr>
					<td><input class="checkboxBroker" type="checkbox" name="user[]" value="'.$courtier['ID'].'" /></td>
					<td>'.$courtier['ID'].'</td>
					<td>'.$courtier['display_name'].'</td>
					<td>'.$courtier['user_email'].'</td>
					<td>'.$courtier['user_login'].'</td>
					<td>
						<a href="'.get_site_url().'/meo-crm-users-form-broker?courtier_id='.$courtier['ID'].'" class="button action mr5px"><i class="fa fa-pencil"></i></a>
						<a onclick="deleteBroker('.$courtier['ID'].')" class="button action meo-btn-list-action meo-btn-trash"><i class="fa fa-trash"></i></a>
					</td>
				  </tr>';
	}
	echo $html;

	wp_die();
}

/*
 * Rechercher dans la liste des courtier
 */
add_action( 'wp_ajax_remove_broker', 'remove_broker' );
function remove_broker()
{
	global $wpdb;
	$broker_id = $_POST['id'];
	$creator = wp_get_current_user();

	// Class
	$userModel = new UsersModel();
	$meoCrmUsersHelper = new MeoCrmUsersHelper();
	$meoCrmUsersFrontTableHelper = new MeoCrmUsersFrontTableHelper();

	if(MeoCrmUsers::deleteUser($broker_id)){
		$datas  = $userModel->selectAllUsersByCreatorID($creator);
	}

	echo $meoCrmUsersFrontTableHelper->showTableCell($datas);

	die();
}

/*
 * Rechercher dans la liste des courtier
 */
add_action( 'wp_ajax_multi_remove_broker', 'multi_remove_broker' );
function multi_remove_broker()
{
	global $wpdb;
	$checked = true;
	$ids = $_POST['ids'];
	$creator = wp_get_current_user();

	// Class
	$userModel = new UsersModel();
	$meoCrmUsersHelper = new MeoCrmUsersHelper();
	$meoCrmUsersFrontTableHelper = new MeoCrmUsersFrontTableHelper();

	foreach($ids['list'] as $list){
		if(!MeoCrmUsers::deleteUser($list['id'])){
			$checked = false;
		}
	}

	if($checked)
	{
		$datas = $userModel->selectAllUsersByCreatorID($creator);
		echo $meoCrmUsersFrontTableHelper->showTableCell($datas);
	}else{
		return false;
	}

	die();
}
?>