<?php

class MeoCrmUsersHelper
{
    public function showMessageGetRequest($get_show_message,$valide_message,$error_message,$message_subject)
    {
        if(isset($get_show_message) && $get_show_message)
        {
            if(isset($valide_message) && $valide_message)
            {
                switch($message_subject)
                {
                    case 'user_create':
                        $message = 'L\'utilisateur à bien été créé';
                        break;
                    default:
                       $message = 'Validé !';
                }
                $show_message = $this->flashMessage($message,true);
            }else if(isset($error_message) && $error_message){
                switch($message_subject)
                {
                    case 'user_create':
                        $message = '<strong>ERREUR : </strong>l\'utilisateur n\'a pas pu être créé';
                        break;
                    default:
                       $message = '<strong>ERREUR : </strong>un problème est survenu';
                }
                $show_message = $this->flashMessage($message,false);
            }
            return $show_message;
        }
        return null;
    }
    
    public function flashMessage($message, $type)
    {
        if($type)
        {
            $html  = '<div id="message" class="updated notice is-dismissible" style="margin-left: 0px !important">';
            $html .=    '<p>';
            $html .=        $message;
            $html .=    '</p>';
            $html .= '</div>';
        }else{
            $html  = '<div class="error" style="margin-left: 0px !important">';
            $html .=    '<p>';
            $html .=        $message;
            $html .=    '</p>';
            $html .= '</div>';
        }
        return $html;
    }
    
    public function flashMessageFront($message, $type)
    {
        if($type)
        {
            $html  = '<div id="message" class="alert alert-success" style="margin-left: 0px !important">';
            $html .=    '<p>';
            $html .=        $message;
            $html .=    '</p>';
            $html .= '</div>';
        }else{
            $html  = '<div class="alert alert-danger" style="margin-left: 0px !important">';
            $html .=    '<p>';
            $html .=        $message;
            $html .=    '</p>';
            $html .= '</div>';
        }
        return $html;
    }
}
