/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.$ = jQuery;

$(document).ready(function() {
    /*
     * List Users Courtiers Check all 
     */
    $('input[name=checkedAll]').click(function(){
        if($(this).is(':checked'))
        {
            $('input.checkboxBroker').each(function(){
                $(this).attr('checked', true);
            });
        }else{
            $('input.checkboxBroker').each(function(){
                $(this).attr('checked', false);
            });
        }
    });
    
    /*
     * List Users Courtiers recherche requête ajax 
     */
    $('#btn_search').click(function(){
        $.post(
        	meo_crm_users_ajax.ajax_url,
            {
                'action': 'list_search_front',
                'search': $('#search').val(),
                'client_id': $('#client_id').val()
            },
            function(response){
                //console.log(response);
                $('#brokers_table tbody').html(response);
            }
        );  
    });
    
    /*
     * List Users Courtiers suppression multiple 
     */
    $('#list_action').click(function(){
        
        var list_ids = {'list':[]};
        var action = $('#list-action').val();
        
        // Récupère les ids séléctionné
        $('input.checkboxBroker').each(function(){
            if($(this).is(':checked'))
            {
                list_ids.list.push({'id':$(this).val()});
            }          
        });
        
        // Check l'action séléctionnée
        if(action != -1)
        {
            // Détermine l'action qui à été choisi
            switch(action) {
                case '1':
                    console.log('suppression des courtiers');
                    deleteMultiBroker(list_ids);
                    break;
            }
        }
    });
    
    /* GESTION DES PROJET FORM COURTIER */
    $('#client_id').change(function(){
    	meo_crm_users_clientProjectsList($('#courtier_id').val(),$(this).val());
    });
});

/*
 * Suppression d'un courtier
 */
function deleteBroker(id){
   if(confirm('Voulez-vous vraiment supprimer cette utilisateur ?'))
   {
       $.post(
    	meo_crm_users_ajax.ajax_url,
           {
               'action': 'remove_broker',
               'id': id
           }
        ).done(function(response){
               //console.log(response);
               $.post(
                    ajaxurl,
                    {
                        'action': 'meo_crm_users_getCourtiersList',
                        'clientId': $('#client_id').val()
                    },
                    function(response){
                        console.log(response);
                        $('div#matrix-container').html(response);
                    }
                );
               $('#brokers_table tbody').html(response);
               alert('L\'utilisateur a bien été supprimé');
        }).fail(function(response){
               alert('<i class="fa fa-close" ></i> L\'utilisateurs n\'a pu étre supprimé correctement');
        });   
   }
}

/*
 * Suppression de plusieurs courtier
 */
function deleteMultiBroker(ids){
   if(confirm('Voulez-vous vraiment supprimer ces utilisateurs ?'))
   {
        $.post(
        meo_crm_users_ajax.ajax_url,
           {
               'action': 'multi_remove_broker',
               'ids': ids
           }
        ).done(function(response){
            $('#brokers_table tbody').html(response);
            $.post(
            	meo_crm_users_ajax.ajax_url,
                {
                    'action': 'meo_crm_users_getCourtiersList',
                    'clientId': $('#client_id').val()
                },
                function(response){
                    console.log(response);
                    $('div#matrix-container').html(response);
                }
            );
            alert('Les utilisateurs ont bien été supprimés');
        }).fail(function(response){
            console.log(response);
            alert('Les utilisateurs n\'ont pas été supprimés correctement');
        }); 
   }
}

/*
 * Show project list
 */
function meo_crm_users_clientProjectsList(courtier_id,client_id)
{
	jQuery('.meo-crm-users-list #client_projects').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
    jQuery.post(
    	meo_crm_users_ajax.ajax_url,
        {
           'action': 'meo_crm_users_loadProject',
           'courtier_id': courtier_id,
           'client_id': client_id
        },
        function(response){
            jQuery('.meo-crm-users-list #client_projects').html(response);   
        }
    );
}