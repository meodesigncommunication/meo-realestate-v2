<?php
/* 
 * Template name: MEO CRM REALESTATE Lot
 */
global $wpdb;
$lot_id = $_GET['id'];
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$lot = RealestateModel::selectLotById($lot_id); // get lot datas

$data = array();
$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['page'] = 'Lot detail';
$data['plugin_path'] = plugins_url();
$data['lot'] = $lot;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';

Timber::render('twig/meo-crm-realestate-lot.html.twig', $data);