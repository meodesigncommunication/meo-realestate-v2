$(document).ready(function(){
    
    executeFilter();
    
    $('.fancybox').fancybox();  
    
    $('.btn-situation').fancybox({
        width: 500,
        height: 500
    });
    
    /*
     * Manage Filter for lot list page
     */
    $('#room-filter .filter-item').click(function(){
        $('#room-filter .filter-item.active').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var room = $(this).attr('data-value');
        $('input[name="room-selected"]').val(room);
        setSessionFilter('room-selected',room);
        executeFilter();
    });    
    $('#floor-filter .filter-item').click(function(){
        $('#floor-filter .filter-item.active').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var floor = $(this).attr('data-value');
        $('input[name="floor-selected"]').val(floor);
        setSessionFilter('floor-selected',floor);
        executeFilter();
    });    
    $('#surface-filter .filter-item').click(function(){
        $('#surface-filter .filter-item.active').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var surface = $(this).attr('data-value');
        $('input[name="surface-selected"]').val(surface);
        setSessionFilter('surface-selected',surface);
        executeFilter();
    });  
    $('.meta-value-selectable').click(function(){ 
        $(this).parent().find('li').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var meta_value = $(this).attr('data-value'); 
        var input_id = $(this).attr('data-id-input'); 
        $('input[meta-id="' + input_id + '"]').val(meta_value);
        setSessionFilter(input_id,meta_value);
        executeFilter();
    });
    // Remove filter
    $('#remove-filter').click(function(){
        $('input[name="room-selected"]').val('');
        $('input[name="floor-selected"]').val('');
        $('input[name="surface-selected"]').val('');        
        $('.meta-selected').each(function(){
            $(this).val('');
        });
        $('.active').each(function(){
            $(this).removeClass('active');
        });
        deleteSessionFilter();
        executeFilter();
    });
});
function setSessionFilter(filter,value)
{
    $.ajax({
        url  : $('input[name="ajaxurl"]').val(), // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data : ({
            action: 'setSessionFilter',
            index: filter,
            value: value
        }),
        success : function(response){ 
            console.log(response);
        }
    });
}

function deleteSessionFilter()
{
    $.ajax({
        url  : $('input[name="ajaxurl"]').val(), // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data : ({
            action: 'deleteSessionFilter'
        }),
        success : function(response){ 
            console.log('session deleted = '+response);
        }
    });
}

function executeFilter()
{
    var temp = '';
    var index = '';
    var metas = {};
    var room = $('input[name="room-selected"]').val();
    var floor = $('input[name="floor-selected"]').val();
    var surface = $('input[name="surface-selected"]').val();
    
    $('.meta-selected').each(function(){ 
        index = $(this).attr('meta-id');
        if($(this).val() != ''){
            metas['id_'+index] = $(this).val();
        }
    });
    
    $.ajax({
        url  : $('input[name="ajaxurl"]').val(), // La ressource ciblée
        type : 'POST', // Le type de la requête HTTP.
        data : ({
            action: 'getListLotFilter',
            room: room,
            floor: floor,
            surface: surface,
            metas: metas
        }),
        success : function(response){ 
            if(response != '')
            {
                $('#message-error').css('display','none');
                var obj = $.parseJSON(response);            
                $('article').css('display','none');            
                $.each(obj, function( index, value ) {
                    console.log(index);
                    $('article[data-id="'+value.id+'"]').css('display','block');
                });
            }else{
                $('#message-error').css('display','block');
                $('article').css('display','none');
            }
        }
    });
}
