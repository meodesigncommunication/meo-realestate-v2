$(document).ready(function(){
    
   /* building front image map */
    $('.building_map area').mouseover(function(){
        var id = $(this).attr('data-id');
        $('.status_lot_hover_'+id).each(function(){
            $(this).css('display','block');
        }); 
        $('.status_lot_'+id).each(function(){
            $(this).css('display','none');
        });    
    });
    $('.building_map area').mouseout(function(){
        var id = $(this).attr('data-id');
        $('.status_lot_hover_'+id).each(function(){
            $(this).css('display','none');
        }); 
        $('.status_lot_'+id).each(function(){
            $(this).css('display','block');
        }); 
    });

    /* building back image map */
    $('.building_map area').mouseover(function(){
        var id = $(this).attr('data-id');
        $('.status_lot_hover_back_'+id).each(function(){
            $(this).css('display','block');
        }); 
        $('.status_lot_back_'+id).each(function(){
            $(this).css('display','none');
        }); 
    });
    $('.building_map area').mouseout(function(){
        var id = $(this).attr('data-id');        
        $('.status_lot_hover_back_'+id).each(function(){
            $(this).css('display','none');
        }); 
        $('.status_lot_back_'+id).each(function(){
            $(this).css('display','block');
        }); 
    }); 
    
});