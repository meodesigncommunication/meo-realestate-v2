<?php
interface DevelopmentDao {
	public function getDevelopments($ids = array());
	public function getSectors($ids = array());
	public function getBuildings($ids = array());
	public function getFloors($ids = array());
	public function getLots($ids = array());
	public function getPlans($ids = array());
	public function getEntries($ids = array());

	public function getLotTypes();
}
