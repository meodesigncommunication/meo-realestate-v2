<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoPiwikIntegration {

	// Get analytics id from cookie
	public static function mscPiwikGetAnalyticsIdFromCookie() {

		$piwik_site_id = get_option('piwik_site_id');
		
		if (empty($piwik_site_id)) {
			return null;
		}

		$analytics_id = null;

		$cookie_stub = "_pk_id_" . $piwik_site_id . "_";
		foreach ($_COOKIE as $cookie_name => $cookie_value) {
			if (preg_match('/^' . $cookie_stub . '/', $cookie_name)) { // Cookie ends with a hash that we'll have to ignore
				$cookie_parts = split('\.', $cookie_value);
				// 128 in regexp is just to force an upper limit.  At the time of writing, piwik IDs are 16 characters long
				if (!empty($cookie_parts) && !empty($cookie_parts[0]) && preg_match('/^[0-9a-zA-Z]{16,128}$/', $cookie_parts[0])) {
					$analytics_id = $cookie_parts[0];
				}
			}
		}

		return $analytics_id;
	}
}
