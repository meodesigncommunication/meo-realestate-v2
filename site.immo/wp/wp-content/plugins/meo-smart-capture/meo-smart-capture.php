<?php
/*
Plugin Name: MEO Smart Capture
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited.
Proprietary and confidential

info@allmeo.com
*/

define('MSC_TEXT_DOMAIN', 'meo-smart-capture');
define('MSC_MIN_PHP_VERSION', '5.0');

// If all the dependencies are there, we're good to go
if ($dependencies_present) {

	$plugin_dir = dirname(plugin_basename(__FILE__));
	load_plugin_textdomain(MSC_TEXT_DOMAIN, false, $plugin_dir . '/languages/');

}
