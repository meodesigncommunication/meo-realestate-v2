<?php 
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if ($_SERVER['SERVER_ADDR'] == "::1") {
	define('DB_NAME', 'meointranet');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_HOST', 'localhost');
}
else {
	/* TEST SERVER */
	/*
	define('DB_NAME', 'rml_mrev2');
	define('DB_USER', 'rml_meoestate');
	define('DB_PASSWORD', '3st@t3!MEO');
	define('DB_HOST', 'rml.myd.sharedbox.com');
	*/
	define('DB_NAME', 'nobi_smartv2');
	define('DB_USER', 'nobi_mrev2');
	define('DB_PASSWORD', 'Mrev2!p@meo');
	define('DB_HOST', 'nobi.myd.sharedbox.com');
	define('IS_PRODUCTION', true);
}

if (!defined('IS_PRODUCTION')) {
	define('IS_PRODUCTION', false);
}


define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('AUTH_KEY',         '!ZW#.6W-F-+M4H0;wky8`Q|~tCFm#`#wF:G{eQ9&G:}I=Ck81XYhArTKN~b$g+:Z');
define('SECURE_AUTH_KEY',  'Cr*}J_23-d?P?Al(fr[U{#=WzlU($Gp8*{HTB=u#l$9jO]1*ss~n+)WrOu<:_wd-');
define('LOGGED_IN_KEY',    'jlb3F+?,UJx8D)]muXZ`+4?s?U7-_Wk TXo 84t5}~-DNqlV^H6y@eYQKE OVOK1');
define('NONCE_KEY',        'zzq|pb_2g:b>@tvFN?4.(/s:%![Y_H5;(D.t*OiU+&)DR8ZD%!*_IWgFZp=tU0_.');
define('AUTH_SALT',        'yC=hY&9=Mb8UqKMb+Au-.*5|v8;2#6h+|LQt ]=2t2|}]~_uxR`dx(-F#TEO>F6]');
define('SECURE_AUTH_SALT', 'h{F9<G[Y-O&Mk3tsZ;%g@lG|hUIB:9lH?<`r4(/+mX7:Awn-OV{de|~Zz#~EX9p0');
define('LOGGED_IN_SALT',   'm--~G2RrjU_-/ogmUVujO}dq-mtFbnD&Afvrhv%A9-W9mRVY7Q+GzHP83WF)7)on');
define('NONCE_SALT',       '&Y -y}*^!BhCB&8-RFY{Fn@+ z,_)|p-hUJ|t v8%CI8#w?n@5OVCotOhx|T*!y{');

$table_prefix  = 'wp_';

define('WP_DEBUG', false );
define('SAVEQUERIES', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
