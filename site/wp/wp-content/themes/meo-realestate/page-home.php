<?php 

/*
Template Name: MEO CRM Theme Homepage
*/

get_header(); ?>

	<main role="main" class="homepage">
		<!-- section -->
		<section>

			<h1><?php the_title(); ?></h1>

				<!-- nav -->
                <div class="wrapper-content nav">
                    <nav class="nav-home" role="navigation">
                    	<div class="menu-list-container">
                        <?php 
						// html5blank_nav();
						$args = array(
								'theme_location'  => 'header-menu',
								'menu'            => '',
								'container'       => 'div',
								'container_class' => 'menu-{menu slug}-container',
								'container_id'    => '',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul>%3$s</ul>',
								'depth'           => 0,
								'walker'          => ''
								);
						
						$mainMenu = 'Primary Menu';
						if(current_user_can(CLIENT_ROLE)){
							$mainMenu = 'Client Menu';
						} else if(current_user_can(COURTIER_ROLE)){
                        	$mainMenu = 'Courtier Menu';
                        }
						
						$menu_items = wp_get_nav_menu_items($mainMenu, $args);
						foreach($menu_items as $k => $menu_item){
							
							# Parent Menu
							if($menu_item->menu_item_parent == 0){
								echo '</div><div class="menu-list-container"><div class="menu-parent-item home-menu-item">'.$menu_item->title.'</div>';
							}
							else {
								echo '<div class="menu-child-item home-menu-item"><a href="'.$menu_item->url.'">'.$menu_item->title.'</a></div>';
							}
						}
						?>
						</div>
						<div class="clear"></div>
                    </nav>
                </div>
                <!-- /nav -->
                
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; 
		
		// do_action('meo_crm_theme_homepage');

		else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
		
		
		
		
		
	

    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
		

		</section>
		<!-- /section -->
	</main>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>
