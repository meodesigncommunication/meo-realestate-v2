<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

add_action('login_head', 'meo_crm_theme_redirect_login');

function meo_crm_theme_redirect_login(){
	
	if(is_user_logged_in()){
		wp_redirect( home_url() );
	}
}