<?php 
global $current_user;

if(!is_user_logged_in() && !is_front_page()){ wp_redirect(home_url('/')); exit; }

?><!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
            <meta charset="<?php bloginfo('charset'); ?>">
            <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

            <link href="//www.google-analytics.com" rel="dns-prefetch">
            <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
            <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">            
            <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="<?php bloginfo('description'); ?>">

            <?php wp_head(); ?>
            <script>
                // conditionizr.com
                // configure environment tests
                conditionizr.config({
                    assets: '<?php echo get_template_directory_uri(); ?>',
                    tests: {}
                });
            </script>
            
            <?php /* */ ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    

	</head>
	<body <?php body_class(); ?>>

                <!-- header -->
                <header class="header clear" role="banner">
                    <div class="wrapper">
                        <!-- logo -->
                        <div class="logo">
                                <a href="<?php echo home_url(); ?>">
                                        <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Logo" class="logo-img">
                                </a>
                        </div>
                        <!-- /logo -->
                        <div class="page-title"><?php if(is_user_logged_in()){ the_title(); } ?></div>
                        <?php if(is_user_logged_in()){ ?>
                        <div class="connected-user">
						  <div class="connected-user-identifier-text">
						  <?php 
						  
							  echo $current_user->display_name; ?><br/><?php wp_loginout(home_url('/')); 
							  if(current_user_can('administrator')){ echo '<br/><a href="'.admin_url().'">WP Backend</a>'; }
						  
						  ?>
						  </div>
						  <div class="connected-user-identifier-avatar"><i class="fa fa-user" aria-hidden="true"></i></div>
						</div>
						<?php } ?>
                    </div>                    
                </header>
                <!-- /header -->
                
                <?php 
                if(!is_user_logged_in()){
                    ?>
                    <div id="main-content">
	                    <?php 
                    	
	                    
                    	do_action('login_form');
                    	
                    	$args = array(
                    			'echo'           => true,
                    			'remember'       => false,
                    			'redirect'       => home_url('/'),
                    			'form_id'        => 'loginform',
                    			'id_username'    => 'user_login',
                    			'id_password'    => 'user_pass',
                    			'id_remember'    => 'rememberme',
                    			'id_submit'      => 'wp-submit',
                    			'label_username' => __( 'Username' ),
                    			'label_password' => __( 'Password' ),
                    			'label_remember' => __( '' ),
                    			'label_log_in'   => __( 'Log In' ),
                    			'value_username' => '',
                    			'value_remember' => false
                    	);
                    	wp_login_form( $args );
                    	
                    	get_footer('not-logged-in');
                    	wp_die();
				}
				?>
				
                <!-- nav -->
                <div class="wrapper-content nav">
                    <nav class="nav" role="navigation">
                        <?php 
                        // html5blank_nav();
                        $mainMenu = 'Primary Menu';
                        if(current_user_can(CLIENT_ROLE)){
                        	$mainMenu = 'Client Menu';
                        } else if(current_user_can(COURTIER_ROLE)){
                        	$mainMenu = 'Courtier Menu';
                        }
                        
                        wp_nav_menu(
							array(
								'theme_location'  => 'header-menu',
								'menu'            => $mainMenu,
								'container'       => 'div',
								'container_class' => 'menu-{menu slug}-container',
								'container_id'    => '',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul>%3$s</ul>',
								'depth'           => 0,
								'walker'          => ''
								)
							);
                        ?>
                    </nav>
                </div>
                <!-- /nav -->
                
                <div id="main-content">
                    <div class="wrapper-content">

                    