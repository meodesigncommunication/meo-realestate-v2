                            </div>
                        </div>

                        <!-- /wrapper -->
                        
                        <!-- footer -->
			<footer class="footer" role="contentinfo">
                            <div class="wrapper">
                                <!-- copyright -->
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> Copyright M|E|O Real Estate
				</p>
				<!-- /copyright -->
                            </div>
			</footer>
			<!-- /footer -->		

		<?php wp_footer(); ?>

	</body>
</html>
