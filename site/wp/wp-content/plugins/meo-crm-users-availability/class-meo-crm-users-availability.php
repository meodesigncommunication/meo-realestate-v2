<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/

class MeoCrmUsersAvailability {
	
	# Build
	
		public function __construct() { }
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
	
	# Get Table Headers (const or private static array, when PHP5.6)
		
		public static function getTableHeaders (){
			// AllowedFields to be printed on the contacts table
			$allowedFields = array(	'availability', 'activity', 'last_send');
				
			$fields = array();
			foreach($allowedFields as $field) {
				$fields[ucwords(str_replace('_', ' ', $field))] = $field;
			}
			return $fields;
		}
	
	# Manage User Availability in the Database
		
		public static function addUserAvailability ($data){
			
			if(array_key_exists('user_id', $data) && array_key_exists('project_id', $data)){
				
				if(!array_key_exists('availability', $data)){$data['availability'] = 1;}
				if(!array_key_exists('activity', $data)){$data['activity'] = 0;}
				
				global $wpdb;
				
				$table = $wpdb->prefix . MEO_USERS_AVAILABILITY_TABLE;
					
				if($wpdb->insert( $table, $data )) {
					return $wpdb->insert_id;
				}
			}
			
			return false;
		}
		
		public static function updateUserAvailability ($data, $where){
			
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_USERS_AVAILABILITY_TABLE;
				
			return $wpdb->update( $table, $data, $where );
		}
		
		public static function deleteUserAvailabilityByIds ($userID, $projectID){
			// TODO :: Check if IDs exist?
			//         Custom query
		}
	
	###############
	# USERS LEVEL #
	###############
		
		#getRecords
		
		public static function getRecords (){
				
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " ORDER BY project_id ASC";
			
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) return $records;
			
			return false;
		}
		
		#getAvailabilityByProjectID
		
		public static function getAvailabilityByProjectID ($ProjectID){
		
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " WHERE project_id=".$ProjectID." ORDER BY user_id ASC";
		
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return $records;
		
			return false;
		}
		
		#getAvailabilityByUserID
		
		public static function getAvailabilityByUserID ($UserID){
		
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " WHERE user_id=".$UserID." ORDER BY project_id ASC";
		
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return $records;
		
			return false;
		}
		
		#getAvailabilityByProjectIDUserID
		
		public static function getAvailabilityByProjectIDUserID($ProjectID, $UserID){
			
			global $wpdb;
				
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " 
						WHERE user_id=".$UserID." 
							AND project_id=".$ProjectID."";
			
			$record = $wpdb->get_results($query, ARRAY_A);
			
			if($record) return reset($record);
			
			return false;
		}
		
	# Get Users
	
		public static function getUsersByProjectID ($Project_ID){
			
			global $wpdb;
			
			// Get Users
			$query = "SELECT user_id FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " WHERE project_id=" . $Project_ID . " ORDER BY user_id ASC";
			
			$userIDs = $wpdb->get_results($query, ARRAY_A);
			
			if($userIDs){
				$include = array();
	        	foreach($userIDs as $k => $u){
	        		$include[] = $u['user_id'];
	        	}
	        	$users = get_users(array('include' => $include));
	        
	        	return $users;
			}
			
			return false;
		}
		
		public static function getAvailableUsersByProjectID ($Project_ID, $limit = ''){
				
			global $wpdb;
				
			// Get Users
			$query = "SELECT user_id FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " 
						WHERE project_id=" . $Project_ID . " AND availability = 1 AND activity = 1 
							ORDER BY user_id ASC";
			if($limit != '') {
				$query.= ' LIMIT '.$limit;
			}
			
			$userIDs = $wpdb->get_results($query, ARRAY_A);
			
			if($userIDs){
				$includeIDs = array();
				foreach($userIDs as $k => $userIDar){
					$includeIDs[] = $userIDar['user_id'];
				}
				$users = get_users(array('include' => $includeIDs));
				
				return $users;
			}
				
			return false;
		}
		
		public static function getUsers ($where = '', $returnType = OBJECT){
			
			global $wpdb;
			
			// Get all Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE . " " . $where . " ORDER BY id DESC";
			
			$all_users = $wpdb->get_results($query, $returnType);
			
			return $all_users;
		}
		
		public static function resetUsersAvailabilityByProjectID ($Project_ID){
			
			global $wpdb;
			
			$table = $wpdb->prefix.MEO_USERS_AVAILABILITY_TABLE;
			$data = array('availability' => 1);
			$where = array('project_id' => $Project_ID);
			
			$wpdb->update( $table, $data, $where );
		}
		
	##################
	# PROJECTS LEVEL #
	##################
	
		# Manage Project Availability
		
		public static function addProjectAvailability ($data){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_USERS_AVAILABILITY_PROJECTS_TABLE;
				
			if($wpdb->insert( $table, $data )){
				return $wpdb->insert_id;
			}
			return false;
		}
		
		public static function updateProjectAvailability ($data, $where){
			
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_USERS_AVAILABILITY_PROJECTS_TABLE;
		
			return $wpdb->update( $table, $data, $where );
		}
		
		#getProjectRecordsByProjectID
		
		public static function getProjectRecordsByProjectID ($ProjectID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_USERS_AVAILABILITY_PROJECTS_TABLE . " WHERE project_id=".$ProjectID."";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
}
