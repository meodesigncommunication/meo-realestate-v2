<?php 

/*
Plugin Name: MEO CRM Users Availability
Description: Plugin to visualize Users Availability. Requires MEO CRM Users plugin
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_USERS_AVAILABILITY_PLUGIN_SLUG', 	'meo-crm-users-availability');
define('MEO_USERS_AVAILABILITY_TABLE', 			'meo_crm_users_availability');			// Users Availability db table
define('MEO_USERS_AVAILABILITY_PROJECTS_TABLE', 'meo_crm_users_availability_projects');	// Projects Availability Activation/Deactivation db table
define('MEO_USERS_AVAILABILITY_SLUG', 			'meo-crm-users-availability');			// Users Availability page slug (Frontend)
define('MEO_CRM_USERS_AVAILABILITY_TPL_FRONT', 	'../../'.MEO_USERS_AVAILABILITY_PLUGIN_SLUG.'/views/frontend/meo-crm-users-availability-view.php');


# Required Files
require_once ( $plugin_root . 'class-meo-crm-users-availability.php' );


# Globals
global $meo_crm_users_availability_db_version;

$meo_crm_users_availability_db_version = '1.0';


/*
 * Check for Dependencies :: MEO CRM Users
 */
function meo_crm_users_availability_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-users/meo-crm-users.php' ) ) {
		$installed_dependencies = true;
	}
	
	if(!$installed_dependencies) {
		
		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Users plugin before', 'meo-realestate').'</h3></div>';
		
		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Users plugin before.', 'meo-realestate'), E_USER_ERROR);
		exit;
		
		
	}
	else {
		
		// Everything is fine
		
		global $wpdb, $meo_crm_users_availability_db_version;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = $wpdb->get_charset_collate();
		
		
		# Users level
		
			$table_name = $wpdb->prefix . MEO_USERS_AVAILABILITY_TABLE;
			
			// Create availability table (project_id is present as a user can have to work on one or several projects)
			$sql = "CREATE TABLE $table_name (
					user_id int(11) NOT NULL,
					project_id int(11) NOT NULL,
					availability int(11) NOT NULL DEFAULT '1',
					activity int(11) NOT NULL DEFAULT '0',
					last_send datetime NOT NULL
			) $charset_collate;";
	
			dbDelta( $sql );
			
			$sql2 = "
			ALTER TABLE $table_name
	  		ADD CONSTRAINT uq_".$table_name." UNIQUE(user_id, project_id);";
			
			dbDelta( $sql2 );
			
			// Update db with the version info
			add_option( 'meo_crm_users_availability_db_version', $meo_crm_users_availability_db_version );
		
		
		# Projects level
		
			$table_name = $wpdb->prefix . MEO_USERS_AVAILABILITY_PROJECTS_TABLE;
			
			// Create Activity table for global project activation/deactivation
			$sql = "CREATE TABLE $table_name (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					project_id int(11) NOT NULL,
					availability int(11) NOT NULL DEFAULT '0', 
					PRIMARY KEY (id) 
					) $charset_collate;";
			
			dbDelta( $sql );
			
			$sql2 = "ALTER TABLE $table_name
			ADD CONSTRAINT uq_".$table_name." UNIQUE(project_id);";
			
			dbDelta( $sql2 );
		
                
        // Create action for generate page
        do_action('plugins_meo_crm_active_create_page');
	}
	
}
register_activation_hook( __FILE__, 'meo_crm_users_availability_activate' );

# Add Scripts and Styles

	add_action( 'admin_enqueue_scripts', 'meo_crm_users_availability_scripts_styles' );
	add_action( 'wp_enqueue_scripts', 'meo_crm_users_availability_scripts_styles' );
	
	function meo_crm_users_availability_scripts_styles() {
		
		# JS
		wp_register_script( 'meo_crm_users_availability_js',  plugins_url('js/meo-crm-users-availability.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_users_availability_js' );
	
		wp_localize_script( 'meo_crm_users_availability_js', 'meo_crm_users_availability_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
	
		# CSS
		wp_register_style( 'meo_crm_users_availability_css',  plugins_url('css/meo-crm-users-availability.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_users_availability_css' );
	
	}

# Add Ajax calls and functions
	
	# Update User Availability Field
	
	add_action( 'wp_ajax_update_users_availability_field', 'update_users_availability_field_callback' );
	add_action( 'wp_ajax_nopriv_update_users_availability_field', 'update_users_availability_field_callback' );
	
	function update_users_availability_field_callback() {
		global $wpdb;
	
		$userID = intval( $_POST['userID'] );
		$projectID = intval( $_POST['projectID'] );
		$fieldName = $_POST['fieldName'];
		$fieldValue = $_POST['fieldValue'];
	
		$data = array($fieldName => $fieldValue);
		$where = array('user_id' => $userID, 'project_id' => $projectID);
	
		MeoCrmUsersAvailability::updateUserAvailability($data, $where);
	
		wp_die();
	}

	
#########
# ADMIN #
#########
	
	
	# Add plugin submenu in admin navigation
	
	add_action( 'admin_menu', 'meo_crm_users_availability_admin_menu', 100 );
	
	function meo_crm_users_availability_admin_menu() {
		add_submenu_page('manage_broker',
							'Disponibilit&eacute;s',
							'Disponibilit&eacute;s',
							'manage_options',
							'visualize_users_availability',
							'add_page_visualize_users_availability');
	}

	# Admin page to visualize users availability
	
	function add_page_visualize_users_availability() {
		include_once 'views/backend/meo-crm-users-availability-view.php';
	}
	
	# Table Head
	
	// Get Plugin Class allowed fields and more
	function meo_crm_users_availability_getTableHeaders(){
	
		$tableHeaders = MeoCrmUsersAvailability::getTableHeaders();
	
		// Call to compatible plugins
		$tableHeaders = apply_filters('meocrmusersavailability_tableheaders', $tableHeaders);
	
		return $tableHeaders;
	}
	
	// TODO :: Improve it
	function meo_crm_users_availability_prepareExtraCells($record){
	
		// Call to compatible plugins
		$extraCellsHTML = apply_filters('meocrmusersavailabitlity_prepareextracells', $record);
	
		return $extraCellsHTML;
	}
	
	# PROJECTS
	
	# Add projects fields
	add_action('projectsFormFields', 'meo_crm_users_availability_projectsFields', 1);
	
	function meo_crm_users_availability_projectsFields(){
	
		$data = array();
		// Check if we already have a record to update, but check project id first
		if(isset($_GET['id']) && !empty($_GET['id'])){
			$results = MeoCrmUsersAvailability::getProjectRecordsByProjectID($_GET['id']);
			if($results) {
				$data = reset($results);
			}
		}

		$value = 0;
		if(!empty($data['availability'])) { $value = $data['availability']; }
		
		$activate = '';
		$deactivate = '';
		
		if( $value == 1 ) {$activate = ' checked="checked"';}
		else $deactivate = ' checked="checked"';
		
		echo '<table class="form-table meo-form-table">
				<tr class="form-field">
	                <th colspan="2">
	                    <label for="meo_crm_users_availability">
	                        MEO CRM USERS AVAILABILITY
	                    </label>
	                </th>
	            </tr>
				<tr class="form-field">
	                <th>
	                    <label for="availability">Activate / Deactivate</label>
	                </th>
	                <td>
						<input type="radio" name="availability" value="1"'.$activate.'> activate 
  						<input type="radio" name="availability" value="0"'.$deactivate.'> deactivate
	                </td>
				  </tr>
	         </table>';
	}
	
	
	# Add projects fields Validation
	add_filter('projectsFormValidate', 'meo_crm_users_availability_projectsformvalidate', 2);
	
	function meo_crm_users_availability_projectsformvalidate($projectID) {
	
		$data = array();
		$data['project_id'] = $projectID;
	
		# Collect data
		$data['availability'] = $_POST['availability'];
		
		# Check if we already have a record to update, or if insert a new one
		$results = MeoCrmUsersAvailability::getProjectRecordsByProjectID($projectID);
		
		# Update
		if($results) {
			
			MeoCrmUsersAvailability::updateProjectAvailability($data, array('project_id' => $projectID));
		}
		# Insert
		else MeoCrmUsersAvailability::addProjectAvailability($data);
		
		# Check at this point if we have Availability records for the Project Courtiers, if any: to create the missing ones
		# ! Only if Project Availability is set to 1
		if($data['availability'] == 1){
			
			$courtiers = meo_crm_users_getCourtiersByProjectID($projectID);
			
			if($courtiers){
				
				foreach($courtiers as $courtier){
					
					# If available, check if a record exists for the user
					$record = MeoCrmUsersAvailability::getAvailabilityByProjectIDUserID($projectID, $courtier['ID']);
						
					# If no record: create
					if(!$record){
						MeoCrmUsersAvailability::addUserAvailability( array( 'user_id' => $courtier['ID'], 'project_id' => $projectID) );
					}
					
				}
					
			}
		}
			
	
		return $projectID;
	}

	
#########
# FRONT #
#########
	
	
	# PAGE CREATION
	
		# Register Plugin templates
		
		add_filter('meo_crm_core_templates_collector', 'meo_crm_users_availability_templates_register', 1, 1);
		
		function meo_crm_users_availability_templates_register($pluginsTemplates){
		
			$pluginsTemplates[MEO_CRM_USERS_AVAILABILITY_TPL_FRONT] = 'MEO CRM Users Availability';
		
			return $pluginsTemplates;
		}
		
		# Register Plugin Pages
		
		add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_users_availability_pages_register' );
		
		function meo_crm_users_availability_pages_register($pluginsPages) {
			
			# Create Page for Users Availability
			$pluginsPages[MEO_USERS_AVAILABILITY_SLUG] = array(
					'post_title' 		=> 'MEO CRM Users Availability',
					'post_content' 		=> 'Page to be used for users availability management.',
					'_wp_page_template' => MEO_CRM_USERS_AVAILABILITY_TPL_FRONT);
		
			return $pluginsPages;
		}

		
/* Extend other Plugins */
		
		
		# MEO CRM Projects
		
		add_filter('meocrmprojects_getProjectData', 'meo_crm_users_availability_getProjectData');
		
		# Add Availability data to the corresponding Project
		function meo_crm_users_availability_getProjectData($project){
		
			if($project){

				$returnProject = array();
					
				# Get Availability fields for the current Project
				$availabilityProjectDetails = MeoCrmUsersAvailability::getProjectRecordsByProjectID($project['id']);
				
				if(is_array($availabilityProjectDetails)) {
					unset($availabilityProjectDetails[0]['id']); // To avoid conflicts
					# Combine all fields
					
					$returnProject = array_merge($project, reset($availabilityProjectDetails));
				}
				else $returnProject = $project;


				return $returnProject;
					
			}
			return false;
		}
		
		# MEO CRM Users
		
		add_filter('meo_crm_users_updateUserProjectData', 'meo_crm_users_availability_addUserAvailability', 10, 2);
		
		function meo_crm_users_availability_addUserAvailability($userID, $projectID){
			
			# Get project Data
			$project = meo_crm_projects_getProjectInfo($projectID);
			
			if($project){
				
				# Check if Availability option is available for the current Project
				if(array_key_exists('availability', $project) && $project['availability'] == 1){
					
					# If available, check if a record exists for the user
					$record = MeoCrmUsersAvailability::getAvailabilityByProjectIDUserID($projectID, $userID);
					
						# If no record: create
						if(!$record){
							MeoCrmUsersAvailability::addUserAvailability( array( 'user_id' => $userID, 'project_id' => $projectID) );
						}
				}
					
			}
				
				
		}
?>