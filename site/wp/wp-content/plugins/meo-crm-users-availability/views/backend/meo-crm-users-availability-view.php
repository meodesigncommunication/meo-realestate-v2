<?php 

# Get User Project(s)
$userProjects = meo_crm_projects_getProjects();

if($userProjects){

	?><div>
		<div id="usersAvailabilityPageAdmin">
			<table cellspacing="10" cellpadding="10" border="1" id="usersAvailabilityTable">
			<?php 
			foreach($userProjects as $p => $project){
				
				$records = MeoCrmUsersAvailability::getAvailabilityByProjectID($project['id']);
				
				if($records){
					
					# Building records headers HTML code
					$recordHeaders =  meo_crm_users_availability_getTableHeaders();
					$recordHeadersHTML = '';
					foreach($recordHeaders as $keyHeader => $recordHeader){
						$recordHeadersHTML .= '<td class="users_availability-td-primary">'.$keyHeader.'</td>';
					}
					$recordHeadersSize = count($recordHeaders)+1;
						
					?>
					
							<tr><th colspan="<?php echo $recordHeadersSize; ?>" class="users_availability-td-primary"><?php echo $project['name']; ?></th></tr>
							<tr><td class="users_availability-td-primary">Courtier</td><?php echo $recordHeadersHTML; ?></tr>
					
							<?php 
							foreach($records as $record) {
								
								// Prepare Extra Cells for the current record
								$extraCellsHTML = meo_crm_users_availability_prepareExtraCells($record);
							
								// Courtier Info
								$courtier = meo_crm_users_getCourtierDataById($record['user_id']);
								
								if($courtier){
									echo '<tr id="'.$courtier['ID']."-".$record['project_id'].'" data-userid="'.$courtier['ID'].'" data-projectid="'.$record['project_id'].'">
											<td class="users_availability-td-secondary">'.$courtier['display_name'].'</td>
											<td class="availability editable">'.($record['availability'] == 1 ? 'disponible' : 'indisponible').'</td>
											<td class="activity editable">'.($record['activity'] == 1 ? 'actif' : 'inactif').'</td>
											<td>'.$record['last_send'].'</td>
											'.$extraCellsHTML.'
										</tr>';
							}
					
				} // /if records
					
			} // /foreach userprojects
			?>
			</table>
		</div>
	</div>
	<?php 
		
		do_action('usersAvailabilityPageAdmin');
		
	} // /if userprojects
?>