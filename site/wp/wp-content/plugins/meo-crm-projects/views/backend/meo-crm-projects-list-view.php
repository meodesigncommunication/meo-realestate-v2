<?php 

// Set Classes and variables
$helperList = new MeoCrmCoreListHelper();
$meoCrmProjectController = new MeoCrmProjectController();
$current_user = wp_get_current_user();
$current_user_id = ($current_user->ID == 1)? 0 : $current_user->ID;
$link_admin = get_admin_url().'admin.php';
$link_add_project = $link_admin.'?page=add_project';
$edit_url = $link_add_project.'&id=@id';

$header = ProjectModel::getHeaderTable();
$list_action = ProjectModel::getActionTable();

// Select
$projects = meo_crm_projects_getProjects(); // ProjectModel::selectAllProject(wp_get_current_user());
$allClients = meo_crm_users_getClients();

?>  
<div class="wrap meo-crm-projects-list">
    <h1>
        Gestion des projets
        <a class="page-title-action" href="<?php echo $link_add_project; ?>">Ajouter</a>
    </h1>
    
    <div id="zoneMessage"></div>
    
    <div class="alignleft actions bulkactions">
        <label class="screen-reader-text" for="bulk-action-selector-bottom">S&eacute;lectionnez l'action group&eacute;e</label>
        <select class="bulk-action-selector" name="action2">
            <option value="-1">Actions group&eacute;es</option>
            <option value="delete">Supprimer </option>
        </select>
        <input class="button action meo-crm-projects-doaction" type="submit" value="Appliquer">
    </div>
    <div class="alignleft actions bulkactions">
        <label class="screen-reader-text" for="bulk-action-selector-bottom">Rechercher</label>
        <input class="meo-crm-projects-search-input" type="search" name="search" value="" placeholder="" />
        <select class="bulk-client-selector" name="projects">
            <option value="-1">S&eacute;lectionner un utilisateur</option>
            <?php 
                foreach($allClients as $client)
                {
                    echo '<option value="'.$client->ID.'">'.$client->display_name.'</option>';
                }
            ?>
        </select>
        <input class="button action meo-crm-projects-search-btn" type="submit" value="Rechercher">
    </div>
    
    <!-- TABLE GENERATE -->
    <div id="content-table">
        <?php echo $helperList->getList($projects, $header, $list_action, true) ?>
    </div>
    <!-- /TABLE GENERATE -->
    
    <div class="alignleft actions bulkactions">
        <label class="screen-reader-text" for="bulk-action-selector-bottom">S&eacute;lectionnez l'action group&eacute;e</label>
        <select class="bulk-action-selector" name="action2">
            <option value="-1">Actions group&eacute;es</option>
            <option value="delete">Supprimer </option>
        </select>
        <input class="button action meo-crm-projects-doaction" type="submit" value="Appliquer">
    </div>
    <div class="alignleft actions bulkactions">
        <label class="screen-reader-text" for="bulk-action-selector-bottom">Rechercher</label>
        <input class="meo-crm-projects-search-input" type="search" name="search" value="" placeholder="" />
        <select class="bulk-client-selector" name="projects">
            <option value="-1">S&eacute;lectionner un utilisateur</option>
            <?php 
                foreach($allClients as $client)
                {
                    echo '<option value="'.$client->ID.'">'.$client->display_name.'</option>';
                }
            ?>
        </select>
        <input class="button action meo-crm-projects-search-btn" type="submit" value="Rechercher">
    </div>
    
</div>