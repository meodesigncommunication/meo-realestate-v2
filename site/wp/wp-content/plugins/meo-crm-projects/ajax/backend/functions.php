<?php

/*
 * 
 */
add_action( 'wp_ajax_getSearchListProjectResult', 'getSearchListProjectResult' );
function getSearchListProjectResult()
{
    $helperList = new MeoCrmCoreListHelper();
    $search = $_POST['search'];
    $client_id = $_POST['client_id'];
    
    $header = ProjectModel::getHeaderTable();
    $list_action = ProjectModel::getActionTable();
    
    $projects = ProjectModel::selectSearchProject($search,$client_id);
    
    echo $helperList->getList($projects, $header, $list_action, true);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_multiple_delete_project', 'multiple_delete_project' );
function multiple_delete_project()
{
    $check = true;
    $ids = $_POST['ids'];
    $projectModel = new ProjectModel();
    $helperList = new MeoCrmCoreListHelper();
    $header = ProjectModel::getHeaderTable();
    $list_action = ProjectModel::getActionTable();
    $meoCrmProjectController = new MeoCrmProjectController(); 
    
    foreach($ids['list'] as $id)
    {
        if(!ProjectModel::deleteProject($id['id']))
        {
            $check = false;
        }
    }
    if($check)
    {
        $message = 'Les projets a bien été supprimé';
        $htmlMessage = $meoCrmProjectController->flashMessage($message, 1);
        $projects = ProjectModel::selectAllProject();
        $table = $helperList->getList($projects, $header, $list_action, true);
    }else{
        $message = '<strong>ERREUR: </strong>Les projets n\'ont pas pu être supprimé';
        $htmlMessage = $meoCrmProjectController->flashMessage($message, 0);
        $projects = ProjectModel::selectAllProject();
        $table = $helperList->getList($projects, $header, $list_action, true);
    }
    
    $result = array(
        'message' => $htmlMessage,
        'table' => $table
    );
    
    echo json_encode($result);
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_delete_project', 'delete_project' );
function delete_project()
{
    $id = $_POST['id'];
    $header = ProjectModel::getHeaderTable();
    $helperList = new MeoCrmCoreListHelper();
    $list_action = ProjectModel::getActionTable();
    $meoCrmProjectController = new MeoCrmProjectController();  
    
    if(ProjectModel::deleteProject($id))
    {
        $message = 'Le projet a bien été supprimé';
        $htmlMessage = $meoCrmProjectController->flashMessage($message, 1);
        $projects = ProjectModel::selectAllProject();
        $table = $helperList->getList($projects, $header, $list_action, true);
    }else{
        $message = '<strong>ERREUR: </strong>Le projet n\'a pas pu être supprimé';
        $htmlMessage = $meoCrmProjectController->flashMessage($message, 0);
        $projects = ProjectModel::selectAllProject();
        $table = $helperList->getList($projects, $header, $list_action, true);
    }
    
    $result = array(
        'message' => $htmlMessage,
        'table' => $table
    );
    
    echo json_encode($result);
    die();
}

