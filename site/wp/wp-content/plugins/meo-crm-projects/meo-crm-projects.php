<?php
/*
Plugin Name: MEO CRM Project
Description: Plugin permettant la gestion de projet
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_CRM_PROJECTS_TABLE', 		'meo_crm_projects');
define('MEO_CRM_PROJECTS_USER_TABLE', 	'meo_crm_project_user');
define('MEO_CRM_PROJECTS_PLUGIN_SLUG', 	'meo-crm-projects');
define('MEO_PROJECTS_SLUG', 			'meo-crm-projects');
define('MEO_CRM_PROJECTS_TPL_FRONT', 	'../../'.MEO_CRM_PROJECTS_PLUGIN_SLUG.'/views/frontend/meo-crm-projects-edit-view.php');

# Upload File Path
define('MEO_CRM_PROJECTS_FOLDER_DIR', $wpUploadsDir['basedir'].'/projects/');	// Server path
define('MEO_CRM_PROJECTS_FOLDER_URL', $wpUploadsDir['baseurl'].'/projects');	// URL path

# Required Files
require_once( $plugin_root . '/models/OptionModel.php' );
require_once( $plugin_root . '/models/ProjectModel.php' );
require_once( $plugin_root . '/controllers/MeoCrmProjectController.php' );
require_once( $plugin_root . '/ajax/backend/functions.php');
require_once( $plugin_root . '/ajax/meo-crm-projects-ajax.php');
require_once( $plugin_root . 'class-meo-crm-projects.php' );

# Globals
global $meo_crm_projects_db_version;

$meo_crm_projects_db_version = '1.0';

# Plugin activation
function meo_crm_projects_activate () {
   global $wpdb;
   global $meo_crm_projects_db_version;
   
    $installed_dependencies = false;
    if ( is_plugin_active( 'meo-crm-users/meo-crm-users.php' )) {
            $installed_dependencies = true;
    }
    
    if(!$installed_dependencies) {
        
        // WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
        // and this will prevent WordPress to activate the plugin.
        echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Users plugins before', 'meo-realestate').'</h3></div>';
        
        //Adding @ before will prevent XDebug output
        @trigger_error(__('Please install and activate the MEO CRM Users plugins before.', 'meo-realestate'), E_USER_ERROR);
        exit;
        
    }else{

    	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	
        $charset_collate = $wpdb->get_charset_collate();
        
        # Projects table
        
        $table_name = $wpdb->prefix . MEO_CRM_PROJECTS_TABLE;
        
        $sql = "CREATE TABLE $table_name (
		           id INTEGER(11) NOT NULL AUTO_INCREMENT,
		           name VARCHAR(255) NOT NULL,
		           url VARCHAR(255) NULL,
		           picture VARCHAR(255) NULL,
		           app_enabled TINYINT(2) DEFAULT '0',
                   app_realestate TINYINT(2) DEFAULT '0',
                   signature TEXT NULL,
                   signature_logo VARCHAR(255) NULL,
		           updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		           created_at DATETIME DEFAULT NULL,
		           PRIMARY KEY (id)
					) $charset_collate;";
        
        dbDelta( $sql );

        # Project User table
        
        $table_name = $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE;
         
        $sql = "CREATE TABLE $table_name (
	            id INTEGER(11) NOT NULL AUTO_INCREMENT,
	            project_id INTEGER(11),
	            user_id INTEGER(11),
	            PRIMARY KEY (id)
					) $charset_collate;";    
        
        dbDelta( $sql );
        
        add_option( 'meo_crm_projects_db_version', $meo_crm_projects_db_version );
         
    }
}
register_activation_hook( __FILE__, 'meo_crm_projects_activate' );


# Add Scripts and Styles

	# Admin
	
	add_action( 'admin_enqueue_scripts', 'meo_crm_projects_scripts_styles_admin' );

	function meo_crm_projects_scripts_styles_admin() {

        # JS
    	wp_register_script( 'meo_crm_projects_js',  plugins_url('js/meo-crm-project-script.js', __FILE__), false, '1.0.0', true );
        wp_enqueue_script( 'meo_crm_projects_js' );

        # CSS
        wp_register_style( 'meo_crm_projects_css',  plugins_url('css/meo-crm-project-style.css', __FILE__), false, '1.0.0' );
        wp_enqueue_style( 'meo_crm_projects_css' );

	}
	
	# Front
	
	add_action( 'wp_enqueue_scripts', 'meo_crm_projects_scripts_styles_front' );
	
	function meo_crm_projects_scripts_styles_front() {
		
		wp_enqueue_script( 'tinymce_js', includes_url( 'js/tinymce/' ) . 'wp-tinymce.php', array( 'jquery' ), false, true );
		
		# JS
		wp_register_script( 'meo_crm_projects_js',  plugins_url('js/front/meo-crm-projects.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_projects_js' );
	
		wp_localize_script( 'meo_crm_contacts_js', 'meo_crm_projects_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
		
		# CSS
		wp_register_style( 'meo_crm_projects_css',   plugins_url('css/meo-crm-project-style.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_projects_css' );
	}


#########
# ADMIN #
#########

	
# Add plugin menu in admin navigation

add_action( 'admin_menu', 'meo_crm_projects_admin_menu' );

function meo_crm_projects_admin_menu() {
        $page_title = 'Projet';
        $menu_title = 'Projet';
        $capability = 'manage_options';
        $menu_slug = 'manage_project';
        $function = 'page_admin_manage_project';
        $icon_url = 'dashicons-portfolio';
        $position = 8;
        add_menu_page ( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
        add_submenu_page($menu_slug,'Ajouter un Projet','Ajouter un Projet',$capability,'add_project','add_page_admin_project');
}

# Display list

function page_admin_manage_project() {    
    include_once 'views/backend/meo-crm-projects-list-view.php';
}

# Fonction appelé pour la vu liste

function add_page_admin_project() {    
    include_once 'views/backend/meo-crm-projects-form-view.php';
}
	

/* Functions */
	
	
	# Get Projects
	
	function meo_crm_projects_getProjects() {
	
        $projects = MeoCrmProjects::getProjects();
        
        if($projects){
        		
        	$returnedProjects = array();
        		
        	foreach($projects as $project){
				
        		$project = apply_filters('meocrmprojects_getProjectData', $project);
        		if($project){
        			$returnedProjects[$project['id']] = $project;
        		}
        	}
        		
        	return $returnedProjects;
        }
        	
        return false;
	}
	
	# Get Projects by User ID
	
	function meo_crm_projects_getProjectsByUserID($userID){
		
		$projects = MeoCrmProjects::getProjectsByUserID($userID);
	
		if($projects){
		
			$returnedProjects = array();
		
			foreach($projects as $project){
				$project = apply_filters('meocrmprojects_getProjectData', $project);
				if($project){
					$returnedProjects[$project['id']] = $project;
				}
			}
		
			return $returnedProjects;
		}
		 
		return false;
	}
	
	# Get Clients by Project ID
	
	function meo_crm_projects_getClientsByProjectID($projectID){
	
		$users = MeoCrmProjects::getClientsByProjectID($projectID);
	
		return $users;
	}
	
	# Get Courtiers by Project ID
	
	function meo_crm_projects_getCourtiersByProjectID($projectID){
	
		$users = MeoCrmProjects::getCourtiersByProjectID($projectID);
	
		return $users;
	}
	
	# Get Project Info
	
	function meo_crm_projects_getProjectInfo($projectID, $filtered = true) {
	
        $project = MeoCrmProjects::getProjectByID($projectID);
		
        if($filtered){
        	return apply_filters('meocrmprojects_getProjectData', $project);
        }
        return $project;
	}
	
	# Extra Project Data

	add_filter('meocrmprojects_getProjectData', 'meo_crm_projects_getProjectData');
	
	# Will add clients and courtiers list if any related to the project
	function meo_crm_projects_getProjectData($project){
			
		if($project){
	
			$returnProject = array();
				
			# Get Clients
			
			$projectClients = meo_crm_projects_getClientsByProjectID($project['id']);
				
			if($projectClients && is_array($projectClients)) {
				# Combine all fields
				$returnProject = array_merge($project, array('clients' => $projectClients));
			}
			else $returnProject = $project;
			
			
			# Get Courtiers
			
			$projectCourtiers = meo_crm_projects_getCourtiersByProjectID($project['id']);
				
			if($projectCourtiers && is_array($projectCourtiers)) {
				# Combine all fields
				$returnProject = array_merge($returnProject, array('courtiers' => $projectCourtiers));
			}
				
			return $returnProject;
		}
		return false;
	}
	
	

# Create folder with project path
function createProjectFolder($project_id)
{
    $path = MEO_CRM_PROJECTS_FOLDER_DIR;
    if(!MeoCrmCoreFileManager::ifFolderExist($path.$project_id))
    {
        if(MeoCrmCoreFileManager::createFolder($project_id, $path, 0777, false)){
            return true;
        }
        return false;
        
    }
    return true;
}

# Create folder with project (project_id) path
function createProjectUnderFolder($project_id, $folder_name, $subpath='')
{
    if(!empty($subpath)){
        $path = MEO_CRM_PROJECTS_FOLDER_DIR.$project_id.'/';
    } else {
        $path = MEO_CRM_PROJECTS_FOLDER_DIR.$project_id.'/'.$subpath.'/';
    }
    
    if(MeoCrmCoreFileManager::createFolder($folder_name, $path, 0777, false)){
        return true;
    }
    return false;
}

/*
 * Create folder in project for all project 
 */
function createFolderAllProject($folder_name, $subpath='')
{
    $check = true;
    $projects = ProjectModel::selectAllProject();
    
    foreach($projects as $project)
    {
        if(empty($subpath)){
            $path = MEO_CRM_PROJECTS_FOLDER_DIR.$project['id'].'/';
        }else{
            $path = MEO_CRM_PROJECTS_FOLDER_DIR.$project['id'].'/'.$subpath.'/';
        }
        
        if(MeoCrmCoreFileManager::ifFolderExist($path))
        {
            if(!MeoCrmCoreFileManager::createFolder($folder_name, $path, 0777, false))
            {
                $check = false;
            }
        }else{
            if(MeoCrmCoreFileManager::ifFolderExist(MEO_CRM_PROJECTS_FOLDER_DIR))
            {
                if(MeoCrmCoreFileManager::createFolder($project['id'].'/', MEO_CRM_PROJECTS_FOLDER_DIR, 0777, false))
                {
                    if(empty($subpath)){
                        if(!MeoCrmCoreFileManager::createFolder($folder_name, $path, 0777, false))
                        {
                            $check = false;
                        }
                    }else{
                        if(MeoCrmCoreFileManager::createFolder($subpath, MEO_CRM_PROJECTS_FOLDER_DIR.$project['id'].'/', 0777, false))
                        {
                            if(!MeoCrmCoreFileManager::createFolder($folder_name, $path, 0777, false))
                            {
                                $check = false;
                            }
                        }
                    }
                    
                    
                }else{
                    $check = false;
                }
            }else{
                $check = false;
            }
        }
    }  
    
    return $check;
}
	
	
	#########
	# FRONT #
	#########
	
	
	# PAGE CREATION
	
	# Register Plugin templates
	
	add_filter('meo_crm_core_templates_collector', 'meo_crm_projects_templates_register', 1, 1);
	
	function meo_crm_projects_templates_register($pluginsTemplates){
			
		$pluginsTemplates[MEO_CRM_PROJECTS_TPL_FRONT]   		= 'Edit Projects';
	
		return $pluginsTemplates;
	}
	
	# Register Plugin Pages
	
	add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_projects_pages_register' );
	
	function meo_crm_projects_pages_register($pluginsPages) {
	
		# Create Page for Contacts
		$pluginsPages[MEO_PROJECTS_SLUG] = array(	'post_title' 				=> 'Edit Projects',
													'post_content' 		=> 'Page to be used to edit projects.',
													'_wp_page_template' => MEO_CRM_PROJECTS_TPL_FRONT);
	
		return $pluginsPages;
	}
	
	
################
# EXTENDS CORE #
################

	add_action('set_current_user', 'meo_crm_projects_getUserProjects');
	
	function meo_crm_projects_getUserProjects(){
		global $current_user;
	
		if($current_user){
			// $current_user->projects = ProjectModel::getUserProjects($current_user);
			# Collect all projects data, but will reduce to id and name
			# Remove reduction to work with complete data
			$userProjects = meo_crm_projects_getProjectsByUserID($current_user->ID);
				
			if($userProjects){
					
				$userTempProjects = array();
					
				foreach($userProjects as $p => $project){
			
					if($project){
						# To get only id and name, so far
						$userTempProjects[$project['id']] = $project['name'];
					}
				}
			
				$current_user->projects = $userTempProjects;
			}
		}
	}
	
#########################
#    EXTENDS PLUGINS    #
#########################
	
	add_action('meocrmusers_getCourtierData', 'meo_crm_projects_getUserProjectData');
	add_action('meocrmusers_getClientData', 'meo_crm_projects_getUserProjectData');
	
	function meo_crm_projects_getUserProjectData($user){
	
		if($user){
			# Collect all projects data, but will reduce to id and name
			# Remove reduction to work with complete data
			$userProjects = meo_crm_projects_getProjectsByUserID($user['ID']);
			
			if($userProjects){
			
				$userTempProjects = array();
			
				foreach($userProjects as $p => $project){
		
					if($project){
						# To get only id and name, so far
						$userTempProjects[$project['id']] = $project['name'];
					}
				}
				
				$user['projects'] = $userTempProjects;
			}
			
		}
	
		return $user;
	
	}
	

#######################
#    EXTENDS THEME    #
#######################

	add_action('meo_crm_theme_homepage', 'meo_crm_projects_theme_homepage');
	
	function meo_crm_projects_theme_homepage(){
		
		global $current_user;
		
		$html = '<div class="homeblock blockprojects">';
		
		// Get Projects
		$userProjects = meo_crm_projects_getProjects();
		
		if($userProjects){
			
			$html.= '<table>
						<tr>
							<td>Your Projects</td>
						</tr>';
			
			foreach($userProjects as $p => $project){
				
				if($project){
					
					$nbContacts = meo_crm_contacts_countContactsByUserIDandProjectID($current_user->ID, $project['id']);
					
					$html.= '<tr><td>'.$project['name'].' (<span>'.$nbContacts.'</span>)</td></tr>';
				}
			}
			
			$html.= '</table>';
		}
		
		$html.= '</div>';
		
		echo $html;
		
	}