<?php 

class ProjectModel
{
    private static $table = 'wp_meo_crm_projects';
    private static $table_link = array(
        
                    'table_linked' => array(
                        'type_join' => 'LEFT JOIN',
                        'table' => 'wp_meo_crm_project_user',
                        'as' => 'pu',
                        'data_link_primary' => 'p.id',
                        'data_link_foreign' => 'pu.project_id'
                     )
    );
    
    public static function getHeaderTable()
    {
        return array(
            array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'name', 'name' => 'Projet', 'type_data' => 'base', 'class' => ''),
            array('key' => 'url', 'name' => 'URL', 'type_data' => 'base', 'class' => ''),
            array('key' => 'app_realestate', 'name' => 'Immobilier', 'type_data' => 'boolean', 'class' => 'w80px center-text'),
            array('key' => 'app_enabled', 'name' => 'Actif', 'type_data' => 'boolean', 'class' => 'w50px center-text'),
        ); 
    }
    
    public static function getActionTable()
    {
        $link_admin = get_admin_url().'admin.php';
        $link_add_project = $link_admin.'?page=add_project';
        $edit_url = $link_add_project.'&id=@id';

        return array(
            array('url_action' => $edit_url, 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id') ,
            array('click_action' => 'trash(@id,\'delete_project\')', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
        );
    }
    
    /*
     * Récupère tous les projets
     * 
     * @return $results(Array)
     */
    public static function selectAllProject($current_user=null)
    {
        //$s = new static();
        global $wpdb;
        $datas = array();
        $previous_value = '';
        
        $query  = 'SELECT p.id, p.name, p.url, p.app_realestate, p.app_enabled, p.created_at, p.updated_at, pu.user_id ';
        $query .= 'FROM '.static::$table.' AS p ';
        if(is_array(static::$table_link) && !empty(static::$table_link))
        {
            foreach(static::$table_link as $link)
            {
                $query .= $link['type_join'].' '.$link['table'].' AS '.$link['as'].' ON '.$link['data_link_primary'].' = '.$link['data_link_foreign'].' ';
            }
        }
        if(!empty($current_user) && $current_user->roles[0] <> 'administrator')
        {
            $query .= 'WHERE pu.user_id = '.$current_user->ID.' ';
        }
        $query .= 'ORDER BY p.id ASC';        
        
        $results = $wpdb->get_results($query); 
        $count = 0;
        $users = array();
        
        foreach($results as $result)
        {
            
            if($previous_value != $result->name)
            {
                if($count > 0)
                {
                    if(is_array($users) && !empty($users))
                    {
                        $datas[$count-1]['users'] = $users;  
                        $users = array();
                    }
                }
                
                $datas[$count] = array(
                    'id' => $result->id,
                    'name' => $result->name,
                    'url' => $result->url,
                    'app_enabled' => $result->app_enabled,
                    'app_realestate' => $result->app_realestate,
                    'created_at' => $result->created_at,
                    'updated_at' => $result->updated_at
                );
                $count++;
                $previous_value = $result->name;
            }
            
            $users[] = $result->user_id;
            
        }
        
        return $datas;
    }
    
    /*
     * Récupère tous les projets
     * 
     * @return $results(Array)
     */
    public static function selectProjectById($id=null)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' AS p ';
        
        if(is_array(static::$table_link) && !empty(static::$table_link))
        {
            foreach(static::$table_link as $link)
            {
                $query .= $link['type_join'].' '.$link['table'].' AS '.$link['as'].' ON '.$link['data_link_primary'].' = '.$link['data_link_foreign'].' ';
            }
        }
        
        $query .=  'WHERE p.id = '.$id.' ';        
        $query .= 'ORDER BY p.id ASC';
        
        $results = $wpdb->get_results($query); 
        
        return $results;
    }
    
    /*
     * Récupère tous les projets
     * 
     * @return $results(Array)
     */
    public static function projectById($id=null)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' AS p ';        
        $query .=  'WHERE p.id = '.$id.' ';        
        $query .= 'ORDER BY p.id ASC';
        
        $results = $wpdb->get_results($query); 
        
        return $results;
    }
    
    /*
     * Select USER PROJECT TABLE with user_id and project_id
     */
    public static function selectProjectUserById($user_id,$project_id)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM wp_meo_crm_project_user AS pu ';
        $query .= 'WHERE user_id = '.$user_id.' AND project_id = '.$project_id;
        $results = $wpdb->get_results($query);        
        return count($results);
    }
    
    /*
     * Récupère les projets lié à un user
     * 
     * @return $results(Array)
     */
    public static function getProjectsByUser($user_id)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM wp_meo_crm_project_user AS pu ';
        $query .= 'INNER JOIN '.self::$table.' AS p ON p.id=pu.project_id ';
        $query .= 'WHERE user_id = '.$user_id;        
        $results = $wpdb->get_results($query);        
        return $results;
    }
    
    /*
     * Récupère les projets lié à un user
     * 
     * @return $results(Array)
     */
    public static function getUsersByProjectID($Project_ID)
    {
        global $wpdb;
        $query  = 'SELECT pu.user_id ';
        $query .= 'FROM wp_meo_crm_project_user AS pu ';
        // $query .= 'INNER JOIN '.self::$table.' AS p ON p.id=pu.project_id ';
        $query .= 'WHERE project_id = '.$Project_ID;
        
        $userIDs = $wpdb->get_results($query, ARRAY_A);
		
        if($userIDs){
        	$include = array();
        	foreach($userIDs as $k => $u){
        		$include[] = $u['user_id'];
        	}
        	$users = get_users(array('include' => $include));
        
        	return $users;
        }
        	
        return false;
    }
    
    /*
     * Récupère les projets lié au current user
     * 
     * @return $results(Array)
     */
    public static function getUserProjects($current_user)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM wp_meo_crm_project_user AS pu '; // TODO::Define table name
        if(isset($current_user) && isset($current_user->roles) && is_array($current_user->roles) && count($current_user->roles) > 0 && $current_user->roles[0] <> 'administrator')
        {
            $query .= 'WHERE user_id = '.$current_user->ID;
        }
        
        $results = $wpdb->get_results($query);
        
        return $results;
    }
    
    
    /*
     * Récupère les projets immobilier
     * 
     * @return $results(Array)
     */
    public static function getProjectsImmo()
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' AS pu ';
        $query .= 'WHERE app_realestate <> 0';        
        
        $results = $wpdb->get_results($query);
        
        return $results;
    }
    
    /*
     * Récupère tous les projets
     * 
     * @return $results(Array)
     */
    public static function selectSearchProject($search='',$user_id=null)
    {
        global $wpdb;
        $datas = array();
        
        $query  = 'SELECT p.id, p.name, p.url, p.app_enabled, p.app_realestate ';
        $query .= 'FROM '.static::$table.' AS p ';
        
        if(is_array(static::$table_link) && !empty(static::$table_link))
        {
            foreach(static::$table_link as $link)
            {
                $query .= $link['type_join'].' '.$link['table'].' AS '.$link['as'].' ON '.$link['data_link_primary'].' = '.$link['data_link_foreign'].' ';
            }
        }
        if((is_array(static::$table_link) && !empty(static::$table_link)) && !empty($user_id))
        {
            $query .=  'WHERE pu.user_id = '.$user_id.' ';
            $startWhere = true;
        }else{
            $startWhere = false;
        }
        if(!empty($search))
        {
            $query .= (!$startWhere)? 'WHERE ' : 'AND ';
            $query .= '(p.name LIKE "%'.$search.'%" ';
            $query .= 'OR p.url LIKE "%'.$search.'%") ';
        }  
        $query .= 'GROUP BY pu.project_id ';
        $query .= 'ORDER BY p.id ASC';
        
        $results = $wpdb->get_results($query); 
        
        foreach($results as $result)
        {
            $datas[] = array(
                'id' => $result->id,
                'name' => $result->name,
                'url' => $result->url,
                'app_enabled' => $result->app_enabled,
                'app_realestate' => $result->app_realestate,
                'created_at' => $result->created_at,
                'updated_at' => $result->updated_at,
            );
        }
        
        return $datas;
    }
    
    /*
     * Make a insert on projects table
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function insertProject($dataProject,$dataClient)
    {
        global $wpdb;            
        if($wpdb->insert(static::$table,$dataProject))
        {
            $project_id = $wpdb->insert_id;            
            foreach($dataClient as $client)
            {
                $data = array();
                $data['project_id'] = $project_id;
                $data['user_id'] = $client;
                
                if(!$wpdb->insert('wp_meo_crm_project_user',$data))
                {
                    self::deleteProject($project_id);
                    return array('valid' => false, 'project_id' => 0);
                }
            }  
            return array('valid' => true, 'project_id' => $project_id);            
        }else{
            return array('valid' => false, 'project_id' => 0);
        }
    }
    
    /*
     * Make a insert on projects users table
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function insertProjectUser($datas)
    {
        global $wpdb;        
        if($wpdb->insert('wp_meo_crm_project_user',$datas))
        {
            return array('valid' => true, 'user_id' => $wpdb->insert_id);
        }else{
            return array('valid' => false, 'user_id' => 0);
        }
    }
    
    /*
     * Permet de mettre à jour les données
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function updateProject($project,$clients = array(),$whereUpdate,$whereDelete = array())
    {
        global $wpdb;
        $check = true;
        $db_project  = self::selectProjectById($whereUpdate['id']); 
        
        if(!empty($whereDelete) && !empty($clients))
        {
            # Delete only client, as they will be readded after
            # TODO::Check client's Courtiers, to not have courtiers related to projects that are not accessible by the client
            $allClients = meo_crm_users_getClients();
            foreach($allClients as $client)
            {
                $deleteClientOnly = $whereDelete;
                $deleteClientOnly['user_id'] = $client['ID'];
                if($wpdb->delete('wp_meo_crm_project_user', $deleteClientOnly) === false)
                {
                    $check = false;
                }
            }
        }
        
        // Update project        
        if($wpdb->update(static::$table, $project, $whereUpdate) === false)
        {
            $check = false;
        }
        
        foreach($clients as $client)
        {
            $wpdb->insert('wp_meo_crm_project_user',$client);    
        }
       
        return $check;
        
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function deleteUserProjectByUser($id)
    {
        global $wpdb;
        $check = true;
        
        $where = array('user_id' => $id);
        if($wpdb->delete('wp_meo_crm_project_user', $where))
        {
            return true;
        }else{
            return false;
        }       
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function deleteProject($id)
    {
        global $wpdb;
        $check = true;
        
        $where = array('project_id' => $id);
        if($wpdb->delete('wp_meo_crm_project_user', $where))
        {
            $where = array('id' => $id);
            if($wpdb->delete(static::$table, $where))
            {
                return true;
            }else{
                return false;
            }
        }        
    }
}