////////////
// FIELDS //
////////////

jQuery(document).ready(function() {

	jQuery(".see-history").click(
		function(e){
			var trUserId = jQuery(this).parents('tr').attr('data-userid');
			var trProjectId = jQuery(this).parents('tr').attr('data-projectid');
			
			// Ajax code
			var data = {
				'action': 'see_emails_history',
				'userID': trUserId,
				'projectID': trProjectId
			};
			jQuery.post(meo_crm_user_emails_history_ajax.ajax_url, data, function(response) {
				jQuery(".ueh-list-container").html(response);
			});
	});

});