<?php 

/*
Plugin Name: MEO CRM User Emails History
Description: Plugin to visualize User Emails History. Requires MEO CRM Users, MEO CRM Users Availability and MEO CRM Contacts plugins
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_USER_EMAILS_HISTORY_TABLE', 'meo_crm_user_emails_history');
define('MEO_USER_EMAILS_HISTORY_PROJECTS_TABLE', 'meo_crm_user_emails_history_projects');	// Projects Email History Activation/Deactivation db table


# Required Files
require_once( $plugin_root . 'class-meo-crm-user-emails-history.php');


# Globals
global $meo_crm_user_emails_history_db_version;

$meo_crm_user_emails_history_db_version = '1.0';


/*
 * Check for Dependencies :: MEO CRM Users, Users Availability and Contacts
 */
function meo_crm_user_emails_history_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-users/meo-crm-users.php' ) && is_plugin_active( 'meo-crm-users-availability/meo-crm-users-availability.php' 
			&& is_plugin_active( 'meo-crm-contacts/meo-crm-contacts.php' )) ) {
		$installed_dependencies = true;
	}
	
	if(!$installed_dependencies) {
		
		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Users, MEO CRM Users Availability and MEO CRM Contacts plugins before', 'meo-realestate').'</h3></div>';
		
		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Users, MEO CRM Users Availability and MEO CRM Contacts plugins before.', 'meo-realestate'), E_USER_ERROR);
		exit;
		
		
	}
	else {
		
		// Everything is fine
		
		global $wpdb, $meo_crm_user_emails_history_db_version;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		$table_name = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_TABLE;
		$charset_collate = $wpdb->get_charset_collate();
		
		// Create availability table (site_id is present as a user can have to work on on eor several sites)
		$sql = "CREATE TABLE $table_name (
				user_id int(11) NOT NULL,
				contact_id int(11) NOT NULL,
				date_sent datetime NOT NULL,
				date_read datetime NOT NULL
		) $charset_collate;";

		dbDelta( $sql );
		
		$sql2 = "ALTER TABLE $table_name
  					ADD CONSTRAINT uq_".$table_name." UNIQUE(user_id, contact_id);";
		
		dbDelta( $sql2 );
		
		// Update db with the version info
		add_option( 'meo_crm_user_emails_history_db_version', $meo_crm_user_emails_history_db_version );
		
		# Projects level
		
		$table_name = $wpdb->prefix . MEO_USER_EMAILS_HISTORY_PROJECTS_TABLE;
			
		// Create Activity table for global project activation/deactivation
		$sql = "CREATE TABLE $table_name (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			project_id int(11) NOT NULL,
			email_history int(11) NOT NULL DEFAULT '0',
			PRIMARY KEY (id)
			) $charset_collate;";
			
		dbDelta( $sql );
			
		$sql2 = "ALTER TABLE $table_name
		ADD CONSTRAINT uq_".$table_name." UNIQUE(project_id);";
			
		dbDelta( $sql2 );
	}
	
}
register_activation_hook( __FILE__, 'meo_crm_user_emails_history_activate' );


# Add Scripts and Styles

	add_action( 'admin_enqueue_scripts', 'meo_crm_user_emails_history_scripts_styles' );
	add_action( 'wp_enqueue_scripts', 'meo_crm_user_emails_history_scripts_styles' );
	
	function meo_crm_user_emails_history_scripts_styles() {
		
		# JS
		wp_register_script( 'meo_crm_user_emails_history_js',  plugins_url('js/meo-crm-user-emails-history.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_user_emails_history_js' );
	
		wp_localize_script( 'meo_crm_user_emails_history_js', 'meo_crm_user_emails_history_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
	
		# CSS
		wp_register_style( 'meo_crm_user_emails_history_css',  plugins_url('css/meo-crm-user-emails-history.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_user_emails_history_css' );
	
	}

	
# Add Ajax calls and functions

	# Update Contact Field
	
	add_action( 'wp_ajax_see_emails_history', 'see_emails_history_callback' );
	add_action( 'wp_ajax_nopriv_see_emails_history', 'see_emails_history_field_callback' );
	
	function see_emails_history_callback() {
		global $wpdb, $meocrmcontacts;
	
		$userID = intval( $_POST['userID'] );
		$projectID = intval( $_POST['projectID'] );
		
		$historyDetails = MeoCrmUserEmailsHistory::getEmailsHistoryByIDs($userID, $projectID);
		$userDetails = meo_crm_users_getCourtierDataById($userID);
		$projectDetails = meo_crm_projects_getProjectInfo($projectID, false);
		
		$returnHTML = '<table id="email-history">
							<tr><th>No History: '.$userDetails['display_name'].' | '.$projectDetails['name'].'</th></tr>
						</table>';
		
		if($historyDetails && count($historyDetails) > 0){
			$returnHTML = '<table id="email-history">';
			$returnHTML .= '<tr><th colspan="5">History: '.$userDetails['display_name'].' | '.$projectDetails['name'].'</th></tr>
							<tr><th>Last Name</th><th>First Name</th><th>Date</th><th>PDF</th><th></th></tr>';
				foreach($historyDetails as $index => $record){
					$contact = meo_crm_contacts_getContactByContactID($record['id']);
					
					if($contact){
						$returnHTML .= '<tr>
											<td>'.$record['contact_lastname'].'</td>
											<td>'.$record['contact_firstname'].'</td>
											<td>'.$record['date_sent'].'</td>
											<td class="center">'.$contact['pdf'].'</td>
											<td><a class="edit-contact" href="'.home_url('/').MEO_CONTACT_SLUG.'/?id='.$contact['id'].'" target="_blank"><i class="fa fa-pencil"></i></a></td>
										</tr>';
					}
				}
			$returnHTML .= '</table>';
		}
		
		echo $returnHTML;
		wp_die();
	}


/*
 * Add plugin submenu in admin navigation

add_action( 'admin_menu', 'meo_crm_user_emails_history_admin_menu', 100 );
function meo_crm_user_emails_history_admin_menu() {
	add_submenu_page('manage_broker','Historique','Historique','manage_options','visualize_user_emails_history','add_page_visualize_user_emails_history');
}
*/
/*
 * Admin page to visualize users availability

function add_page_visualize_user_emails_history() { 
	include_once 'views/backend/meo-crm-user-emails-history-view.php';
}
 */

/* Filters */


	# Add table headers for users availability table
	add_filter('meocrmusersavailability_tableheaders', 'user_email_history_header');
	
	function user_email_history_header($tableHeaders){
		// Number of emails sent to the user (per site)
		$tableHeaders['Nb Emails'] = 'count-emails';
		$tableHeaders['History'] = 'emails-history';
		return $tableHeaders;
	}
	
	
	# Add table headers corresponding cells for users availability table
	add_filter('meocrmusersavailabitlity_prepareextracells', 'prepare_user_email_history_cells');
	
	function prepare_user_email_history_cells($record){
		// Get user id and site id to count emails
		$nb_emails = MeoCrmUserEmailsHistory::getCountEmailsByRecord($record);
		return '<td class="history-td">'.$nb_emails['nb_emails'].'</td><td class="history-td"><i class="fa fa-calendar see-history" aria-hidden="true"></i></td>';
	}

	
/* Actions */
	
	
	# Action from users availability page, to add extra HTML content
	add_action('usersAvailabilityPageAdmin', 'user_emails_history_usersAvailabilityPageAdmin');
	add_action('usersAvailabilityPageFront', 'user_emails_history_usersAvailabilityPageAdmin');
	
	function user_emails_history_usersAvailabilityPageAdmin(){
		echo '<div id="user_email_history-ActivityPage-container"><div class="ueh-list-container"></div></div>';
	}


	# PROJECTS
	
	# Add projects fields
	add_action('projectsFormFields', 'meo_crm_user_emails_history_projectsFields', 1);
	
	function meo_crm_user_emails_history_projectsFields(){
	
		$data = array();
		// Check if we already have a record to update, but check project id first
		if(isset($_GET['id']) && !empty($_GET['id'])){
			$results = MeoCrmUserEmailsHistory::getProjectRecordsByProjectID($_GET['id']);
			if($results) {
				$data = reset($results);
			}
		}
	
		$value = 0;
		if(!empty($data['email_history'])) { $value = $data['email_history']; }
	
		$activate = '';
		$deactivate = '';
	
		if( $value == 1 ) {$activate = ' checked="checked"';}
		else $deactivate = ' checked="checked"';
	
		echo '<table class="form-table meo-form-table">
				<tr class="form-field">
	                <th colspan="2">
	                    <label for="meo_crm_user_emails_history">
	                        MEO CRM USER EMAILS HISTORY
	                    </label>
	                </th>
	            </tr>
				<tr class="form-field">
	                <th>
	                    <label for="email_history">Activate / Deactivate</label>
	                </th>
	                <td>
						<input type="radio" name="email_history" value="1"'.$activate.'> activate
  						<input type="radio" name="email_history" value="0"'.$deactivate.'> deactivate
	                </td>
				  </tr>
	         </table>';
	}
	
	
	# Add projects fields Validation
	add_filter('projectsFormValidate', 'meo_crm_user_emails_history_projectsformvalidate', 2);
	
	function meo_crm_user_emails_history_projectsformvalidate($projectID) {
	
		$data = array();
		$data['project_id'] = $projectID;
	
		// Collect data
		$data['email_history'] = $_POST['email_history'];
	
		// Check if we already have a record to update, ot if insert a new one
		$results = MeoCrmUserEmailsHistory::getProjectRecordsByProjectID($projectID);
	
		if($results) {
			// Update
			MeoCrmUserEmailsHistory::updateProjectEmailHistory($data, array('project_id' => $projectID));
		}
		else {
			// Insert
			MeoCrmUserEmailsHistory::addProjectEmailHistory($data);
		}
	
		return $projectID;
	}
	
	
/* Extend other Plugins */

	
	# MEO CRM Contact
	
	add_action('meo_crm_contacts_delete_contact_by_id', 'meo_crm_user_emails_history_delete_contact_by_id');
	
	# Deleting Contact
	function meo_crm_user_emails_history_delete_contact_by_id($contact_id){
	
		MeoCrmUserEmailsHistory::deleteEmailHistoryByContactId($contact_id);
	
	}
	
	
	# MEO CRM Projects
	
	add_filter('meocrmprojects_getProjectData', 'meo_crm_user_emails_history_getProjectData');
	
	# Add Emails History data to the corresponding Project
	function meo_crm_user_emails_history_getProjectData($project){
	
		if($project){

			$returnProject = array();
			
			# Get Emails History fields for the current Project
			$emailsHistoryProjectDetails = MeoCrmUserEmailsHistory::getProjectRecordsByProjectID($project['id']);
			
			if(is_array($emailsHistoryProjectDetails)) {
				unset($emailsHistoryProjectDetails[0]['id']); // To avoid conflicts
				# Combine all fields
				$returnProject = array_merge($project, reset($emailsHistoryProjectDetails));
			}
			else $returnProject = $project;
			
			return $returnProject;
		}
		return false;
	}
?>