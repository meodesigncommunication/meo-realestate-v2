<?php 

# Ajax


	#####################
	# Projects Statuses #
	#####################

		# Save Project Status
		
		add_action( 'wp_ajax_meo_crm_relationship_save_status', 'meo_crm_relationship_save_status_callback' );
		add_action( 'wp_ajax_nopriv_meo_crm_relationship_save_status', 'meo_crm_relationship_save_status_callback' );
		
			function meo_crm_relationship_save_status_callback() {
					
				$subAction = $_POST['subaction'];
				$id = intval( $_POST['status_id'] );
				$label = $_POST['label'];
				$color = $_POST['color'];
				$order = $_POST['order'];
				$projectID = $_POST['project_id'];
					
					
				if($projectID == '') wp_die();
					
				$data = array('label' => $label, 'color' => $color, 'status_order' => $order);
					
				# Add new status
				if($id == '' && $subAction == 'add') {
					$data['project_id'] = $projectID;
					echo MeoCrmContactsUsersRelationship::addProjectRelationshipStatus($data);
				}
				# Update existing status
				else if($id != '' && $subAction == 'update'){
					$where = array('id' => $id, 'project_id' => $projectID);
					MeoCrmContactsUsersRelationship::updateProjectRelationshipStatus($data, $where);
				}
					
				wp_die();
			}
	
		# Delete Project Status
		
		add_action( 'wp_ajax_meo_crm_relationship_delete_status', 'meo_crm_relationship_delete_status_callback' );
		add_action( 'wp_ajax_nopriv_meo_crm_relationship_delete_status', 'meo_crm_relationship_delete_status_callback' );
		
			function meo_crm_relationship_delete_status_callback() {
			
				$id = intval( $_POST['status_id'] );
				$projectID = $_POST['project_id'];
			
			
				if($projectID == '' || $id == '') wp_die();
					
				# Delete existing status
				if($id != '' && $projectID != ''){
					# Even if projectID is not a primary key, we use it to be sure
					$where = array('id' => $id, 'project_id' => $projectID);
					MeoCrmContactsUsersRelationship::deleteProjectRelationshipStatus($where);
					
					# Delete Contact state for Contacts having this state
					$where = array('status_id' => $id);
					MeoCrmContactsUsersRelationship::deleteContactUserRelationshipState($where);
				}
			
				wp_die();
			}
			
		
	##############################
	# Contact Relationship State #
	##############################
	
		
		# Update Contact State
		
		add_action( 'wp_ajax_update_relcontactstate_field', 'meo_crm_relationship_update_ContactState_field_callback' );
		add_action( 'wp_ajax_nopriv_update_relcontactstate_field', 'meo_crm_relationship_update_ContactState_field_callback' );
		
			function meo_crm_relationship_update_ContactState_field_callback(){
					
				$contact_user_record_ids = explode('-', $_POST['contact_user_status']);
				$contact_id = $contact_user_record_ids[0];
				$user_id 	= $contact_user_record_ids[1];
				$status_id 	= $contact_user_record_ids[2];
				$fieldName 	= str_replace('a-state-', '', $_POST['fieldName']);
				$fieldValue = $_POST['fieldValue'];
					
				if($contact_id == '' || $user_id == ''|| $status_id == '' || $fieldName == ''|| $fieldValue == '') wp_die();
					
				# Updating existing states
				if($contact_id != '' && $user_id != '' && $status_id != '' && $fieldName != '' && $fieldValue != ''){
					# Date exception
					if($fieldName == 'date_updated'){
						$fieldValue = date("Y-m-d H:i:s", strtotime($fieldValue));
					}
					# Even if statusID is not a primary key, we use it to be sure
					$where = array('contact_id' => $contact_id, 'user_id' => $user_id, 'status_id' => $status_id);
					$data = array($fieldName => $fieldValue);
					MeoCrmContactsUsersRelationship::updateContactUserRelationship($data, $where);
				}
			
				wp_die();
			}
	
	
		# Delete Contact State
		
		add_action( 'wp_ajax_meo_crm_relationship_deleteContactState', 'meo_crm_relationship_deleteContactState_callback' );
		add_action( 'wp_ajax_nopriv_meo_crm_relationship_deleteContactState', 'meo_crm_relationship_deleteContactState_callback' );
		
			function meo_crm_relationship_deleteContactState_callback() {
				$contact_id = intval( $_POST['contact_id'] );
				$user_id = intval( $_POST['user_id'] );
				$status_id = intval( $_POST['status_id'] );
					
				if($contact_id == '' || $user_id == ''|| $status_id == '') wp_die();
			
				# Delete existing states
				if($contact_id != '' && $user_id != '' && $status_id != ''){
					# Even if contact_id and user_id are not primary keys, we use them to be sure it's the correct contact/user
					$where = array('contact_id' => $contact_id, 'user_id' => $user_id, 'status_id' => $status_id);
					MeoCrmContactsUsersRelationship::deleteContactUserRelationshipState($where);
				}
					
				wp_die();
			}
			
	
		# Add Contact State
		
		add_action( 'wp_ajax_meo_crm_relationship_addContactState', 'meo_crm_relationship_addContactState_callback' );
		add_action( 'wp_ajax_nopriv_meo_crm_relationship_addContactState', 'meo_crm_relationship_addContactState_callback' );
		
			function meo_crm_relationship_addContactState_callback() {
			
				$contact_id 	= intval( $_POST['contact_id'] );
				$user_id 		= intval( $_POST['user_id'] );
				$status_id 		= intval( $_POST['status_id'] );
				$note 			= $_POST['note'];
				$date_updated 	= $_POST['date_updated'];
					
				# TODO::Add date regex check
				if($contact_id == '' || $user_id == ''|| $status_id == '' || $date_updated == '') wp_die();
					
				# Adding contact state
				if($contact_id != '' && $user_id != '' && $status_id != '' && $date_updated != ''){
			
					$data = array(
							'contact_id' 	=> $contact_id,
							'user_id' 		=> $user_id,
							'status_id' 	=> $status_id,
							'note' 			=> $note,
							'date_updated' 	=> date("Y-m-d H:i:s", strtotime($date_updated))
					);
			
					$CURid = MeoCrmContactsUsersRelationship::addContactUserRelationship($data);
			
					if($CURid) echo $CURid;
				}
			
				wp_die();
			}
			
		
	####################################
	# Contact Relationship Interaction #
	####################################
	
		
		# Update Contact Interaction
		
		add_action( 'wp_ajax_update_relcontactinteraction_field', 'meo_crm_relationship_update_ContactInteraction_field_callback' );
		add_action( 'wp_ajax_nopriv_update_relcontactinteraction_field', 'meo_crm_relationship_update_ContactInteraction_field_callback' );
		
			function meo_crm_relationship_update_ContactInteraction_field_callback(){
				
				$record_id 	= $_POST['record_id'];
				$fieldName 	= str_replace('a-state-', '', $_POST['fieldName']);
				$fieldValue = $_POST['fieldValue'];
					
				if($record_id == '' || $fieldName == ''|| $fieldValue == '') wp_die();
					
				# Updating existing states
				if($record_id != '' && $fieldName != '' && $fieldValue != ''){
					# Date exception
					if($fieldName == 'date_updated'){
						$fieldValue = date("Y-m-d H:i:s", strtotime($fieldValue));
					}
					# Even if statusID is not a primary key, we use it to be sure
					$where = array('id' => $record_id);
					$data = array($fieldName => $fieldValue);
					MeoCrmContactsUsersRelationship::updateContactUserInteraction($data, $where);
				}
			
				wp_die();
			}
	
		# Delete Contact Interaction
		
		add_action( 'wp_ajax_meo_crm_relationship_deleteContactInteraction', 'meo_crm_relationship_deleteContactInteraction_callback' );
		add_action( 'wp_ajax_nopriv_meo_crm_relationship_deleteContactInteraction', 'meo_crm_relationship_deleteContactInteraction_callback' );
		
			function meo_crm_relationship_deleteContactInteraction_callback() {
				$record_id = intval( $_POST['record_id'] );
					
				if($record_id == '') wp_die();
			
				# Delete existing states
				if($record_id != ''){
					
					$where = array('id' => $record_id);
					MeoCrmContactsUsersRelationship::deleteContactUserInteraction($where);
				}
					
				wp_die();
			}
			
	
		# Add Contact Interaction
		
		add_action( 'wp_ajax_meo_crm_relationship_addContactInteraction', 'meo_crm_relationship_addContactInteraction_callback' );
		add_action( 'wp_ajax_nopriv_meo_crm_relationship_addContactInteraction', 'meo_crm_relationship_addContactInteraction_callback' );
		
			function meo_crm_relationship_addContactInteraction_callback() {
			
				$contact_id 	= intval( $_POST['contact_id'] );
				$user_id 		= intval( $_POST['user_id'] );
				$note 			= $_POST['note'];
				$date_updated 	= $_POST['date_updated'];
					
				# TODO::Add date regex check
				if($contact_id == '' || $user_id == '' || $note == '' || $date_updated == '') wp_die();
					
				# Adding contact state
				if($contact_id != '' && $user_id != '' && $note != '' && $date_updated != ''){
			
					$data = array(
							'contact_id' 	=> $contact_id,
							'user_id' 		=> $user_id,
							'note' 			=> $note,
							'date_updated' 	=> date("Y-m-d H:i:s", strtotime($date_updated))
					);
			
					$CURid = MeoCrmContactsUsersRelationship::addContactUserInteraction($data);
			
					if($CURid) echo $CURid;
				}
			
				wp_die();
			}
		
		
		
		
		
		
		
		
		
		
		
	
	# Get Project States selecter HTML
	
	add_action( 'wp_ajax_get_relprojectstates_selecter', 'meo_crm_relationship_get_relprojectstates_selecter_callback' );
	add_action( 'wp_ajax_nopriv_get_relprojectstates_selecter', 'meo_crm_relationship_get_relprojectstates_selecter_callback' );
	
		function meo_crm_relationship_get_relprojectstates_selecter_callback() {
			$project_id = ( $_POST['project_id'] );
		
			$existingStatus = MeoCrmContactsUsersRelationship::getStatusByProjectID($project_id);
				
			$html = '<select name="fieldToSave" class="fieldToSave">';
				
			foreach($existingStatus as $k => $status){
				$html.= '<option value="'.$status['id'].'" data-color="'.$status['color'].'">'.$status['label'].'</option>';
			}
				
			$html.= '</select>';
			echo $html;
			wp_die();
		
		}
	
	# Calculate datediff
	
	add_action( 'wp_ajax_meo_crm_relationship_datediff', 'meo_crm_relationship_datediff_callback' );
	add_action( 'wp_ajax_nopriv_meo_crm_relationship_datediff', 'meo_crm_relationship_datediff_callback' );
	
		function meo_crm_relationship_datediff_callback() {
			$date1 = $_POST['date1'];
			$date2 = $_POST['date2'];
				
			# TODO::Use regex to match date format
			if($date1 != '' && $date2 != ''){
				$dateState1 = strftime ('%d-%m-%Y %H:%M:%S', strtotime($date1));
				$dateState2 = strftime ('%d-%m-%Y %H:%M:%S', strtotime($date2));
				
				$interval = date_diff(date_create($dateState1), date_create($dateState2));
					
				echo  $interval->format('%aj');
			}
				
			wp_die();
		
		}
	
	
	# Get Users by project ID to build HTML dropdown list
	
	add_action( 'wp_ajax_meo_crm_relationship_getusersbyprojectid', 'meo_crm_contacts_getusersbyprojectid_selecter_callback' );
	add_action( 'wp_ajax_nopriv_meo_crm_relationship_getusersbyprojectid', 'meo_crm_contacts_getusersbyprojectid_selecter_callback' );
	
		function meo_crm_contacts_getusersbyprojectid_selecter_callback(){
		
			# Do we have an assigned Courtier?
			$assignedUser = false;
			if(array_key_exists('courtier_id', $_POST)){
				$assignedUser = intval($_POST['courtier_id']);
			}
		
			$Project_ID = intval($_POST['project_id']);
		
			$users = ProjectModel::getUsersByProjectId($Project_ID);
		
			if($users){
				// Will contain, if exists, the previous user id that the Contact was assigned to
				$hiddenfield = '';
					
				$HTML = '<select class="fieldToSave" name="courtier">';
					
				foreach($users as $u => $user){
		
					# Courtier only
					if(in_array(COURTIER_ROLE, $user->roles)){
						# Assigned User?
						$selectedUser = "";
						if($assignedUser && $assignedUser == $user->id){
							$selectedUser = " selected='selected'";
							$hiddenfield = '<input class="user_id_old" type="hidden" value="'.$assignedUser.'" name="user_id_old" />';
						}
						$HTML.= "<option value='".$user->id."'".$selectedUser.">".$user->display_name."</option>";
					}
				}
					
				$HTML.= "</select>".$hiddenfield;
					
				echo $HTML;
			}
			wp_die();
		}
	
	# Reassign courtier to Contact
	add_action( 'wp_ajax_meo_crm_relationship_reassign', 'meo_crm_relationship_ReassignCourtier' );
	add_action( 'wp_ajax_nopriv_meo_crm_relationship_reassign', 'meo_crm_relationship_ReassignCourtier' );
	
		function meo_crm_relationship_ReassignCourtier(){
			
			global $current_user;
			
			# Get params
			$contact_id = intval( $_POST['contact_id'] );
			$user_id = intval( $_POST['user_id'] );
				
			if($contact_id == '' || $user_id == '') wp_die();
		
			# Managing contact state
			if($contact_id != '' && $user_id != ''){
		
				# Update all the relationship states with the new UserID
				$where = array('contact_id' => $contact_id);
				$data = array('user_id' => $user_id);
				MeoCrmContactsUsersRelationship::updateContactUserRelationship($data, $where);
		
				$note = 'Reassigned<br/>';
		
				# Get previous user, if any
				if(array_key_exists('user_id_old', $_POST) && $_POST['user_id_old'] != ''){
					$previousUser = get_user_by('id', intval( $_POST['user_id_old'] ));
					$note.= $previousUser->display_name.' ';
				}
		
				$note.= '<br/>> '.$_POST['user_name'];
				
				# Add who reasigned
				$note.= '<br/>by '.$current_user->display_name;
		
				# Add a state of changing Courtier
				$data = array(
						'contact_id' 	=> $contact_id,
						'user_id' 		=> $user_id,
						'status_id' 	=> MEO_RELATIONSHIP_STATE_REASSIGNED_ID,
						'note' 			=> $note,
						'date_updated' 	=> date("Y-m-d H:i:s")
				);
		
				$CURid = MeoCrmContactsUsersRelationship::addContactUserRelationship($data);
		
				if($CURid) echo $CURid;
			}
				
			wp_die();
		}
?>