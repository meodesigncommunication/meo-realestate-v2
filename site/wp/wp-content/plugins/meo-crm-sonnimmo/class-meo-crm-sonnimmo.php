<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/

class MeoCrmSonnimmo {
	
	# Build
	
		public function __construct() { }
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
		
	##################
	# PROJECTS LEVEL #
	##################
	
		# Manage Project
		
		public static function addProjectSonnimmo ($data){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_SONNIMMO_TABLE;
				
			if($wpdb->insert( $table, $data )){
				return $wpdb->insert_id;
			}
			return false;
		}
		
		public static function updateProjectSonnimmo ($data, $where){
			
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_SONNIMMO_TABLE;
		
			return $wpdb->update( $table, $data, $where );
		}
		
		#getProjectRecordsByProjectID
		
		public static function getProjectRecordsByProjectID ($ProjectID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_SONNIMMO_TABLE . " WHERE project_id=".$ProjectID."";
			
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records && is_array($records) && count($records) > 0) return $records;
		
			return false;
		}
}
