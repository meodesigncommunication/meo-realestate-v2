<?php 

/*
Plugin Name: MEO CRM Sonnimmo
Description: Plugin to send information to Sonnimmo platform. Requires MEO CRM Analytics plugin
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_SONNIMMO_PLUGIN_ROOT', 		$plugin_root);
define('MEO_SONNIMMO_PLUGIN_SLUG', 		'meo-crm-sonnimmo');
define('MEO_SONNIMMO_TABLE', 			'meo_crm_sonnimmo');
define('MEO_SONNIMMO_SERVER', 			'sonnimmo.herokuapp.com/api/clients'); // http://
define('MEO_SONNIMMO_CS_API_TOKEN', 	'5y2XkhoVEbauCzfrR1hS');

# Required Files
require_once ( $plugin_root . 'class-meo-crm-sonnimmo.php' );

# Globals
global $meo_crm_sonnimmo_db_version;

$meo_crm_sonnimmo_db_version = '1.0';


/*
 * Check for Dependencies :: MEO CRM Analytics
 */
function meo_crm_sonnimmo_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-analytics/meo-crm-analytics.php' ) ) {
		$installed_dependencies = true;
	}
	
	if(!$installed_dependencies) {
		
		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Analytics plugin before', 'meo-realestate').'</h3></div>';
		
		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Analytics plugin before.', 'meo-realestate'), E_USER_ERROR);
		exit;
		
		
	}
	else {
		
		// Everything is fine
		
		global $wpdb, $meo_crm_sonnimmo_db_version;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = $wpdb->get_charset_collate();
		
		# Projects level
		
			$table_name = $wpdb->prefix . MEO_SONNIMMO_TABLE;
			
			// Create Activity table for global project activation/deactivation
			$sql = "CREATE TABLE $table_name (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					project_id int(11) NOT NULL,
					sonnimmo int(11) NOT NULL DEFAULT '0', 
					PRIMARY KEY (id) 
					) $charset_collate;";
			
			dbDelta( $sql );
			
			$sql2 = "ALTER TABLE $table_name
			ADD CONSTRAINT uq_".$table_name." UNIQUE(project_id);";
			
			dbDelta( $sql2 );
		

			// Update db with the version info
			add_option( 'meo_crm_sonnimmo_db_version', $meo_crm_sonnimmo_db_version );
		
                
        // Create action for generate page
        do_action('plugins_meo_crm_active_create_page');
	}
	
}
register_activation_hook( __FILE__, 'meo_crm_sonnimmo_activate' );

	
#########
# ADMIN #
#########
	
	# PROJECTS
	
	# Add projects fields
	add_action('projectsFormFields', 'meo_crm_sonnimmo_projectsFields', 1);
	
	function meo_crm_sonnimmo_projectsFields(){
	
		$data = array();
		// Check if we already have a record to update, but check project id first
		if(isset($_GET['id']) && !empty($_GET['id'])){
			$results = MeoCrmSonnimmo::getProjectRecordsByProjectID($_GET['id']);
			if($results) {
				$data = reset($results);
			}
		}

		$value = 0;
		if(!empty($data['sonnimmo'])) { $value = $data['sonnimmo']; }
		
		$activate = '';
		$deactivate = '';
		
		if( $value == 1 ) {$activate = ' checked="checked"';}
		else $deactivate = ' checked="checked"';
		
		echo '<table class="form-table meo-form-table">
				<tr class="form-field">
	                <th colspan="2">
	                    <label for="meo_crm_sonnimmo">
	                        MEO CRM SONNIMMO
	                    </label>
	                </th>
	            </tr>
				<tr class="form-field">
	                <th>
	                    <label for="sonnimmo">Activate / Deactivate</label>
	                </th>
	                <td>
						<input type="radio" name="sonnimmo" value="1"'.$activate.'> activate
  						<input type="radio" name="sonnimmo" value="0"'.$deactivate.'> deactivate
	                </td>
				  </tr>
	         </table>';
	}
	
	
	# Add projects fields Validation
	add_filter('projectsFormValidate', 'meo_crm_sonnimmo_projectsformvalidate', 2);
	
	function meo_crm_sonnimmo_projectsformvalidate($projectID) {
	
		$data = array();
		$data['project_id'] = $projectID;
	
		# Collect data
		$data['sonnimmo'] = $_POST['sonnimmo'];
		
		# Check if we already have a record to update, or if insert a new one
		$results = MeoCrmSonnimmo::getProjectRecordsByProjectID($projectID);
		
		# Update
		if($results) {
			
			MeoCrmSonnimmo::updateProjectSonnimmo($data, array('project_id' => $projectID));
		}
		# Insert
		else MeoCrmSonnimmo::addProjectSonnimmo($data);
			
		return $projectID;
	}

		
/* Extend other Plugins */
		
		
		# MEO CRM Projects
		
		add_filter('meocrmprojects_getProjectData', 'meo_crm_sonnimmo_getProjectData');
		
		# Add Sonnimmo data to the corresponding Project
		function meo_crm_sonnimmo_getProjectData($project){
		
			if($project){

				$returnProject = array();
				
				# Get Sonnimmo fields for the current Project
				$sonnimmoProjectDetails = MeoCrmSonnimmo::getProjectRecordsByProjectID($project['id']);
				
				if(is_array($sonnimmoProjectDetails)) {
					unset($sonnimmoProjectDetails[0]['id']); // To avoid conflicts
					# Combine all fields
					
					$returnProject = array_merge($project, reset($sonnimmoProjectDetails));
				}
				else $returnProject = $project;


				return $returnProject;
					
			}
			return false;
		}
		
		
		# MEO CRM Analytics
		
		# Collect data and make to call to Sonnimmo
		add_filter( 'meo_crm_contact_record_dataContact', 'meo_crm_sonnimmo_handleDataContact', 1000 );
		
		function meo_crm_sonnimmo_handleDataContact($dataContact){
			
			# Get corresponding project data
			$projectData = meo_crm_projects_getProjectInfo($dataContact['project_id']);
			
			# If project is for Sonnimmo, prepare Sonnimmo data for the curl call
			if(array_key_exists('sonnimmo', $projectData) && $projectData['sonnimmo'] == 1){
				
				$referer = null;
				if(array_key_exists('referer', $dataContact) && $dataContact['referer'] != ''){
					$referer = $dataContact['referer'];
				}
				
				$sonnimmoData = array(
						'name'               => $dataContact['first_name'],
						'surname'            => $dataContact['last_name'],
						'contact_source_url' => $projectData['url'], // For tests: 'http://www.altair-aigle.ch/wp', and remove the created contact (check before if he doesn't exist already)
						// 'optional_fields'    => array(),
						// 'comment'            => null, // TODO:: Check if it's really needed to send the comment to sonnimmo
						'language'           => $dataContact['language'],
						'contact_attributes' => array(
								'email'       => $dataContact['email'],
								'phone1'      => $dataContact['phone'],
								// 'phone2'      => null,
								'address1'    => $dataContact['address'],
								// 'address2'    => null,
								'postal_code' => $dataContact['postcode'],
								'city'        => $dataContact['city'],
								// 'location'    => null,
								'country'     => MeoCrmCoreLocator::getCountryCodeByName( $dataContact['country'], $dataContact['language'] ),
								'latitude'    => $dataContact['latitude'],
								'longitude'   => $dataContact['longitude'],
								'analytics_id'=> $dataContact['analytics_id'],
								'referer' 	  => $referer
						),
				);
				
				// print_r($sonnimmoData);
				
				$sonnimmoCurl = new WP_Http_Curl();
				$sonnimmoResponse = $sonnimmoCurl->request('http://'.MEO_SONNIMMO_SERVER, array(
						'headers' => array(
								'Accept'       => 'application/json',
								'Content-type' => 'application/json',
								'user-agent' => $_SERVER['HTTP_USER_AGENT']?$_SERVER['HTTP_USER_AGENT']:''
						),
						'method' => 'POST',
						'_redirection' => 0,
						'body' => json_encode(array(
								'api_token' => MEO_SONNIMMO_CS_API_TOKEN,
								'client'    => $sonnimmoData,
						)),
						'stream' => false,
						'filename' => false,
						'decompress' => false
				));
				
				if ( is_wp_error($sonnimmoResponse) ) {
					MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Couldn't send contact to Sonnimmo [curl] found for " . print_r($dataContact, true));
				}
				else {
					MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Contact sent to Sonnimmo [curl] " . print_r($dataContact, true));
				}
			}
			
			return $dataContact;
		}
?>