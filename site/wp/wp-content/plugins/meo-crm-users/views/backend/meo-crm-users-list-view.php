<?php

global $wpdb;

// Variables
$userId = 0;
$datas = null;
$showMessage = '';
$error_message = $_GET['error'];
$valide_message = $_GET['valide'];
$message_subject = $_GET['subject'];
$get_show_message = $_GET['message'];
$link_admin = get_admin_url().'admin.php';
$link_add_broker = $link_admin.'?page=add_manage_broker';

// Call Classes
$userModel = new UsersModel();
$meoCrmUsersHelper = new MeoCrmUsersHelper();
$meoCrmUsersTableHelper = new MeoCrmUsersTableHelper();

// Init Class attributes
$meoCrmUsersTableHelper->datas = meo_crm_users_getCourtiers(); // $userModel->selectAllUsersByCreatorID(wp_get_current_user());

// Traitement pour afficher un message de validation (si besoin)
$show_message = $meoCrmUsersHelper->showMessageGetRequest($get_show_message, $valide_message, $error_message, $message_subject);

// Récupère tous les client admin
$clients = meo_crm_users_getClients();

// Affichage des elements sur la page de l'admin
echo '<div class="wrap meo-crm-users-list">';
echo    '<h1>Gestion des courtiers<a class="page-title-action" href="'.$link_add_broker.'">Ajouter</a></h1>';
echo    '<div id="bloc-global-table">';
echo        $meoCrmUsersTableHelper->generateGlobalList($link_add_broker,$clients);
echo    '</div>';
echo '</div>';