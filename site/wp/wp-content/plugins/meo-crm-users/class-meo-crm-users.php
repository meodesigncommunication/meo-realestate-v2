<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoCrmUsers {
	
	# Build
	
		public function __construct() { }
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
		
	#########
	# USERS #
	#########

		public static function addUser($datas) {
			
			global $wpdb;
			
			$table = $wpdb->prefix . WP_USERS_TABLE;
			
			if( $wpdb->insert($table, $datas) ) {
				return $wpdb->insert_id;
			}
			
			return false;
			
		}
		
		public static function deleteUser($id) {
			
			global $wpdb;
			
			$table = $wpdb->prefix . WP_USERS_TABLE;
			
			$where = array('ID' => $id);
			
			if($wpdb->delete($table, $where)){
				return true;
			}
			
			return false;
			
		}
		
	#############
	# USER META #
	#############
	
		# DELETE
		public static function deleteUserMeta($userID){
			global $wpdb;
			 
			if($wpdb->delete($wpdb->prefix . WP_USERMETA_TABLE, array('user_id' => $userID))){
				return true;
			}
			return false;
		}
		
	###################
	# USER MANAGEMENT #
	###################
	
		# ADD
		public static function addUserManagement($data) {
			 
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE;
		
			if($wpdb->insert($table, $data)) {
				return $wpdb->insert_id;
			}
		
			return false;
		}
		
		# UPDATE
		public static function updateUserManagement($data, $where) {
			 
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE;
		
			if($wpdb->update($table, $data, $where)) {
				return true;
			}
		
			return false;
		
		}
		
		# DELETE
		public static function deleteUserManagement($id) {
			 
			global $wpdb;
		
			$where = array('user_id' => $id);
		
			$table = $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE;
		
			if($wpdb->delete($table, $where)) {
				return true;
			}
		
			return false;
		}
	
	# Get Records
	
		###########
		# CLIENTS #
		###########
		
		
			# Get Clients
			public static function getClients($returnType = ARRAY_A) {
				
				global $wpdb;
			
				$query  = 'SELECT *
		    				FROM '. $wpdb->prefix . WP_USERS_TABLE .' AS u 
		    				LEFT JOIN '. $wpdb->prefix . WP_USERMETA_TABLE .' AS um ON u.ID = um.user_id
		    				LEFT JOIN '. $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE .' AS uma ON u.ID = uma.user_id
		    					WHERE um.meta_value LIKE "%'.CLIENT_ROLE.'%" 
		    						ORDER BY u.ID ASC';
					
				$results = $wpdb->get_results($query, $returnType);
			
				return $results;
			}
			
			# Get Client Id By Courtier Id
			public static function getClientIdByCourtierId($courtierId, $returnType = ARRAY_A){
				
				global $wpdb;
					
				$query  = 'SELECT client_id 
		    				FROM '. $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE .' AS uma
		    					WHERE uma.user_id = '.$courtierId.' 
		    						LIMIT 0, 1';
				
				$client = $wpdb->get_results($query, $returnType);
				
				if($client && is_array($client) && count($client) > 0) {
					$clientId = reset($client);
					return $clientId['client_id'];
				}
				
				return false;
			}
		
		
		#############
		# COURTIERS #
		#############
		
			# Get Courtiers
			public static function getCourtiers($returnType = ARRAY_A) {
				
				global $wpdb;
			
				$query  = 'SELECT *
		    				FROM '. $wpdb->prefix . WP_USERS_TABLE .' AS u
		    				LEFT JOIN '. $wpdb->prefix . WP_USERMETA_TABLE .' AS um ON u.ID = um.user_id
		    				LEFT JOIN '. $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE .' AS uma ON u.ID = uma.user_id
		    					WHERE um.meta_value LIKE "%'.COURTIER_ROLE.'%" 
		    						ORDER BY u.ID ASC';
					
				$results = $wpdb->get_results($query, $returnType);
			
				return $results;
			}
			
			# Get Courtiers by Client ID
			public static function getCourtiersByClientID($clientID, $returnType = ARRAY_A)
			{
				global $wpdb;
				
				$query  = 'SELECT *
		    				FROM '. $wpdb->prefix . WP_USERS_TABLE .' AS u 
		    				LEFT JOIN '. $wpdb->prefix . WP_USERMETA_TABLE .' AS um ON u.ID = um.user_id
		    				LEFT JOIN '. $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE .' AS uma ON u.ID = uma.user_id
		    					WHERE um.meta_value LIKE "%'.COURTIER_ROLE.'%" ';
				 
				if($clientID && user_can($clientID, CLIENT_ROLE)){
					$query .= 'AND uma.client_id='.$clientID.' ';
				}
					
				$query .= 'ORDER BY u.ID ASC';
			
				$results = $wpdb->get_results($query, $returnType);
				
				return $results;
			}
			
			# Get Courtiers by ProjectID
			public static function getCourtiersByProjectID($projectID, $returnType = ARRAY_A) {
				
				global $wpdb;
				
				$query  = 'SELECT *
		    				FROM '. $wpdb->prefix . WP_USERS_TABLE .' AS u
		    				LEFT JOIN '. $wpdb->prefix . WP_USERMETA_TABLE .' AS um ON u.ID = um.user_id
		    				LEFT JOIN '. $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE .' AS uma ON u.ID = uma.user_id
							LEFT JOIN '. $wpdb->prefix . MEO_CRM_PROJECTS_USER_TABLE .' AS pu on pu.user_id = u.ID
		    					WHERE um.meta_value LIKE "%'.COURTIER_ROLE.'%"
		    						AND pu.project_id='.$projectID.'
		    							ORDER BY u.ID ASC';
				
				$results = $wpdb->get_results($query, $returnType);
			
				return $results;
			}
			
			# Get Courtiers by Seach
			public static function getCourtiersBySearch($search, $returnType = ARRAY_A) {
				
				global $wpdb;
				
				$query  = 'SELECT * 
							FROM '.$wpdb->prefix . WP_USERS_TABLE.' AS u 
							LEFT JOIN '.$wpdb->prefix . WP_USERMETA_TABLE.' AS um ON u.ID = um.user_id 
							LEFT JOIN '.$wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE.' AS uma ON u.ID = uma.user_id 
								WHERE um.meta_value LIKE "%'.COURTIER_ROLE.'%" ';
				
				if(!empty($search))
				{
					$query .= 'AND (
								u.user_login LIKE "%'.esc_sql( $search ).'%" 
								OR u.user_nicename LIKE "%'.esc_sql( $search ).'%" 
								OR u.user_email LIKE "%'.esc_sql( $search ).'%" 
								OR u.display_name LIKE "%'.esc_sql( $search ).'%" 
								OR um.meta_value LIKE "%'.esc_sql( $search ).'%") ';
				}
				
				$query .= 'ORDER BY u.ID ASC';
				
				$results = $wpdb->get_results($query, $returnType);
				
				return $results;
			}
}
