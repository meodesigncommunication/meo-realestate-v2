/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.$ = jQuery;

$(document).ready(function() {
    /*
     *  Gestion des checkbox (All checked)
     */
    $('input.cb-select-all[type=checkbox]').click(function(){
        if($(this).is(':checked'))
        {
            $('input.cb-select-all[type=checkbox]').attr('checked', true);
            $('.meo-crm-users-checkbox').each(function(){
                $(this).attr('checked', true);
            });
        }else{
            $('input.cb-select-all[type=checkbox]').attr('checked', false);
            $('.meo-crm-users-checkbox').each(function(){
                $(this).attr('checked', false);
            });
        }
    });
    
    
    /*
     * Modifie les listes d'action en simultané 
     */
    $('select.bulk-action-selector').change(function(){
        var value = $(this).val();
        $('select.bulk-action-selector').each(function(){
            $(this).val(value);
        });
    });  
    
    /*
     * Modifie les listes d'action en simultané 
     */
    $('select.bulk-client-selector').change(function(){
        var value = $(this).val();
        $('select.bulk-client-selector').each(function(){
            $(this).val(value);
        });
    }); 
    
    /*
     *  Gestion des multiple delete
     */
    $('.meo-crm-users-doaction').click(function(){
        //List action value
        var doaction = $(this).parent().find('select.bulk-action-selector').val();
        var list_ids = {'list':[]};
        
        if(doaction == 'delete')
        {
            if(confirm('Voulez-vous supprimer ces courtiers'))
            {
                // Récupère les ids à supprimer
                $('input.meo-crm-users-checkbox').each(function(){
                    if($(this).is(':checked'))
                    {
                        list_ids.list.push({'id':$(this).val()});
                    }          
                });

                $.post(
                    ajaxurl,
                    {
                        'action': 'multiple_delete_broker',
                        'ids': list_ids,
                        'doaction': doaction
                    },
                    function(response){
                        var obj = $.parseJSON(response);
                        $('div#bloc-message').html(obj.message);
                        $('div#bloc-table').html(obj.table);
                    }
                );
            }
        }else{
            alert('Vous devez séléctionner une action dans la liste');
        }
    });
    
    /*
     *  Duplique les actions d'un champs à l'autre
     */
    $('input[name=search]').keyup(function(){
        var value = $(this).val();
        $('input[name=search]').each(function(){
            $(this).val(value);
        });
    });
    
    /*
     *  Gestion des multiple delete
     */
    $('.meo-crm-users-search-btn').click(function(){
        $.post(
            ajaxurl,
            {
                'action': 'list_search',
                'search': $('input[name=search]').val(),
                'client_id': $('.bulk-client-selector').val()
            },
            function(response){
                //console.log(response);
                $('div#bloc-table').html(response);
            }
        );
    });
    
    /* GESTION DES PROJET FORM COURTIER */
    $('#client_id').change(function(){
        list_project($('#courtier_id').val(),$(this).val());
    });
    
    /* GESTION DE L'AFFICHAGE DE LA MATRICE PROJET UTILISATEUR */
    $('#client_list').change(function(){
        $.post(
            ajaxurl,
            {
                'action': 'generate_matrix',
                'clientId': $(this).val()
            },
            function(response){
                console.log(response);
                $('div#matrix-container').html(response);
            }
        );
    });
    
});

/*
 *  Gestion des delete par ligne
 */
function trash_broker(id)
{
    if(confirm('Voulez-vous supprimer ce courtier ?'))
    {
        $.post(
           ajaxurl,
           {
               'action': 'delete_broker',
               'id': id
           },
           function(response){
                   var obj = $.parseJSON(response);
                   $('div#bloc-message').html(obj.message);
                   $('div#bloc-table').html(obj.table);
               }
        );
    }
}

/*
 * Show project list
 */
function list_project(courtier_id,client_id)
{
    $.post(
        ajaxurl,
        {
           'action': 'loadProject',
           'courtier_id': courtier_id,
           'client_id': client_id
        },
        function(response){
            $('#client_projects').html(response);   
        }
    );
}