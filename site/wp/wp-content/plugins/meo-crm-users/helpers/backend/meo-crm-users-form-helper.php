<?php

class MeoCrmUsersFormHelper
{
    // Attributs
    public $form;
    public $inputs;
    public $valueInputs;
    
    
    public function __contruct()
    {
        
    }
    
    /*
     * 
     */
    public function generateGlobalForm($label_submit)
    {
        $html  = '<form id="'.$this->form['id'].'" name="'.$this->form['name'].'" method="'.$this->form['method'].'">';
        $html .=    '<table class="form-table meo-form-table">';
        $html .=        '<tbody>';
        $html .=            $this->generateFormGroup();
        $html .=        '</tbody>';
        $html .=    '</table>';
        $html .=    '<p class="submit">';
        $html .=        '<input id="createusersub" class="button button-primary" type="submit" value="'.$label_submit.'" name="createuser">';
        $html .=    '</p>';
        $html .= '</form>';
        
        return $html;
    }
    
    /*
     * 
     */
    public function generateFormGroup()
    {
        $html = '';
        foreach($this->inputs as $input)
        {
            if($input['required'])
            {
                $class_tr = 'form-required';
                $label_required = '&nbsp;<span class="description">(obligatoire)</span>';
            }else{
                $class_tr = '';
                $label_required = '';
            }
            if($input['type'] <> 'hidden')
            {
                $html .= '<tr class="form-field '.$class_tr.'">';
                $html .=    '<th scope="row">';
                if(isset($input['label']) && !empty($input['label']))
                {
                    $html .= '<label for="'.$input['name'].'">'.$input['label'].$label_required.'</label>';
                }
                $html .=    '</th>';
                $html .=    '<td>';
                $html .=        $this->generateInput($input);
                $html .=    '</td>';
                $html .= '<tr>';
            }else{
                $html .= $this->generateInput($input);
            }
        }
        return $html;
    }
    
    /*
     * 
     */
    public function generateInput($input)
    {
        $html = '';
        
        if($input['balise'] == 'input')
        {
            $html .= '<input id="'.$input['id'].'" class="'.$input['class'].'" type="'.$input['type'].'" name="'.$input['name'].'" placeholder="'.$input['placeholder'].'" value="'.$input['value'].'" />';
        }
        if($input['balise'] == 'textarea')
        {
            $content = (isset($input['value']) && !empty($input['value'])) ? $input['value'] : $input['placeholder'] ;
            $html .= '<textarea id="'.$input['id'].'" class="'.$input['class'].'" name="'.$input['name'].'">'.$content.'</textarea>';
            //$html .= wp_editor( $content, $input['name'] );
        }
        if($input['balise'] == 'select')
        {
            $html .= '<select id="'.$input['id'].'" name="'.$input['name'].'" class="'.$input['class'].'">';
            foreach($input['values'] as $value)
            {
                $selected = '';
                if($input['selected_value'] == $value['val'])
                {
                    $selected = 'selected="selected"';
                }
                $html .= '<option '.$selected.' value="'.$value['val'].'">'.$value['txt'].'</option>';
            }
            $html .= '</select>';
        }
        if($input['balise'] == 'checkbox')
        {
            $html .= '<input type="checkbox" id="'.$input['id'].'" name="'.$input['name'].'" class="'.$input['class'].'" value="'.$input['value'].'">';
        }
        return $html;
    }
}
