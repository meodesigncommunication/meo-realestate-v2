<?php

class MeoCrmUsersTableHelper
{
    public $datas;
    
    public function generateGlobalList($link_add_broker,$clients)
    {   
        $html .= '<div id="bloc-message"></div>';
        $html .=  $this->otherElementsList($clients);
        $html .=  '<div id="bloc-table">';
        $html .=  $this->generateListTable($link_add_broker);
        $html .=  '</div>';
        $html .=  $this->otherElementsList($clients);       
        return $html;
    }
    
    public function generateListTable($link_form_update)
    {   
        $this->link_form_update = $link_form_update;
        $html  = '<table class="wp-list-table widefat fixed striped posts">';
        $html .=    $this->generateHeaderListTable();
        $html .=    $this->generateBodyListTable();
        $html .= '</table>';        
        return $html;
    }
    public function generateHeaderListTable()
    {
        $html  = '<thead>';
        $html .=    $this->generateCellHeaderListTable();
        $html .= '</thead>';
        $html .= '<tfoot>';
        $html .=    $this->generateCellHeaderListTable();
        $html .= '</tfoot>';        
        return $html;
    }
    public function generateBodyListTable()
    {
        $html  = '<tbody>';
        $html .= $this->generateCellListTable();
        $html .= '</tbody>';        
        return $html;
    }    
    private function generateCellHeaderListTable()
    {
        $html  = '<th class="w30px"><input class="cb-select-all" type="checkbox"></th>';        
        $html .= '<th>#ID</th>';
        $html .= '<th>Identifiant</th>';
        $html .= '<th>Nom complet</th>';
        $html .= '<th>Email</th>';
        $html .= '<th>Actions</th>';
        return $html;
    }    
    private function generateCellListTable()
    {
        if(!empty($this->datas))
        {
            foreach($this->datas as $data)
            {
                $html .= '<tr>';
                $html .=    '<td class="w30px"><input class="meo-crm-users-checkbox" id="broker_'.$data->ID.'" type="checkbox" name="brokers[]" value="'.$data->ID.'"</td>';
                $html .=    '<td>'.$data->ID.'</td>';
                $html .=    '<td>'.$data->user_nicename.'</td>';
                $html .=    '<td>'.$data->display_name.'</td>';
                $html .=    '<td>'.$data->user_email.'</td>';
                $html .=    '<td>';
                $html .=        '<a href="'.$this->link_form_update.'&courtier_id='.$data->ID.'" class="button action mr5px"><i class="fa fa-pencil"></i></a>';
                $html .=        '<a onclick="trash_broker('.$data->ID.')" data-id="'.$data->ID.'" class="button action meo-btn-list-action meo-btn-trash"><i class="fa fa-trash"></i></a>';
                $html .=    '</td>';
                $html .= '</tr>';
            } 
        }else{
            $html  = '<tr>';
            $html  =    '<td colspan="6">Aucun résultat</td>';
            $html .= '</tr>';
        }              
        return $html;
    }
    public function otherElementsList($clients)
    {
        $html  = '<div class="alignleft actions bulkactions">';
        $html .=    '<label class="screen-reader-text" for="bulk-action-selector-bottom">Sélectionnez l’action groupée</label>';
        $html .=    '<select class="bulk-action-selector" name="action2">';
        $html .=        '<option value="-1">Actions groupées</option>';
        $html .=        '<option value="delete">Supprimer </option>';
        $html .=    '</select>';
        $html .=    '<input class="button action meo-crm-users-doaction" type="submit" value="Appliquer">';
        $html .= '</div>';
        $html .= '<div class="alignleft actions bulkactions">';
        $html .=    '<label class="screen-reader-text" for="bulk-action-selector-bottom">Rechercher</label>';
        $html .=    '<input class="meo-crm-users-search-input" type="search" name="search" value="" placeholder="" />';
        if(wp_get_current_user()->roles[0] == 'administrator')
        {
            $html .=    '<select class="bulk-client-selector" name="client">';
            $html .=        '<option value="-1">Choisir un client</option>';
            foreach ($clients as $client)
            {
                $html .=        '<option value="'.$client->ID.'">'.$client->display_name.'</option>';
            }
            $html .=    '</select>';
        }
        $html .=    '<input class="button action meo-crm-users-search-btn" type="submit" value="Rechercher">';
        $html .= '</div>';
        $html .= '<div class="tablenav-pages one-page counter-list">';
        $html .= '  <span class="displaying-num">'.count($this->datas).' éléments</span>';
        $html .= '</div>';
        $html .= '<div class="clear-both"></div>';        
        return $html;
    }
}
