<?php

class MeoCrmUsersFrontTableHelper
{
    public function __contruct()
    {
        
    }
    
    /*
     * Créer les ligne d'un tableau pour l'affichage de données 
     * 
     * @param $users(Array)
     * @return $html(String)
     */
    public function showTableCell($users)
    {
    	$html = '';
        foreach($users as $user)
        {
            $html .= '<tr>';
            $html .=    '<td><input class="checkboxBroker" type="checkbox" name="user[]" value="'.$user->ID.'" /></td>';
            $html .=    '<td>'.$user->ID.'</td>';
            $html .=    '<td>'.$user->display_name.'</td>';
            $html .=    '<td>'.$user->user_email.'</td>';
            $html .=    '<td>'.$user->user_login.'</td>';
            $html .=    '<td>';
            $html .=        '<a href="'.get_site_url().'/meo-crm-users-form-broker?courtier_id='.$user->ID.'" class="button action mr5px"><i class="fa fa-pencil"></i></a>';
            $html .=        '<a onclick="deleteBroker('.$user->ID.')" class="button action meo-btn-list-action meo-btn-trash"><i class="fa fa-trash"></i></a>';
            $html .=    '</td>';
            $html .= '</tr>';
        }  
        return $html;
    }
}

