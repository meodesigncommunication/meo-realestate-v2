<?php
/*
Plugin Name: MEO CRM Users
Description: Plugin qui permet une gestion différente des utilisateurs pour Realestate.
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEOUSERSPLUGINSLUG', 			'meo-crm-users');
define('WP_USERS_TABLE', 				'users');
define('WP_USERMETA_TABLE', 			'usermeta');
define('MEO_CRM_USERS_MGMT_TABLE', 		'meo_crm_users_management');	// Users db table MEOUSERSTABLE
define('MEO_CRM_USERS_LIST_URL', 		'meo-crm-users-list');				// Users page slug (Frontend)
define('MEO_CRM_USERS_LIST_TPL_FRONT', 	'../../'.MEOUSERSPLUGINSLUG.'/views/frontend/meo-crm-users-front-list.php');
define('MEO_CRM_USERS_EDIT_USER_URL', 	'meo-crm-users-form-broker');
define('MEO_CRM_USERS_EDIT_USER_TPL_FRONT', 	'../../'.MEOUSERSPLUGINSLUG.'/views/frontend/meo-crm-users-form-broker.php');
define('COURTIER_ROLE',					'courtier');
define('CLIENT_ROLE',					'admin_client');

# Required Files
// Load Class Helpers
require_once( $plugin_root . 'helpers/backend/meo-crm-users-helper.php');
require_once( $plugin_root . 'helpers/backend/meo-crm-users-form-helper.php');
require_once( $plugin_root . 'helpers/backend/meo-crm-users-table-helper.php');
require_once( $plugin_root . 'helpers/frontend/meo-crm-users-table-helper.php');

// Load Class Model
require_once( $plugin_root . 'models/UserModel.php');

require_once( $plugin_root . 'class-meo-crm-users.php');


// Load AJAX Function
require_once( $plugin_root . 'ajax/meo-crm-users-ajax.php');

# Globals
global $meo_crm_users_db_version, $expectedUserFields;

$meo_crm_users_db_version = '1.0';
$expectedUserFields = array(
							'first_name' 	=> array( "type" => "text", 	"label" => 'Pr&eacute;nom',			"notes" => "" ), 
							'last_name' 	=> array( "type" => "text", 	"label" => 'Nom',					"notes" => "" ), 
							'user_login' 	=> array( "type" => "text", 	"label" => 'Identifiant',			"notes" => "" ), 
							'user_email' 	=> array( "type" => "text", 	"label" => 'Email',					"notes" => "" ), 
							// 'client_id' 	=> array( "type" => "select", 	"label" => 'Client [Admin Only]',	"notes" => "" ),
							'signature'		=> array( "type" => "textarea", "label" => "Signature",				"notes" => "Will be printed at the bottom of the contact's email."),
							);


# Plugin Activation
function meo_crm_users_install_meo_crm_users () {
	
   global $wpdb, $meo_crm_users_db_version;

   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   
   $table_name = $wpdb->prefix . MEO_CRM_USERS_MGMT_TABLE; 
   $charset_collate = $wpdb->get_charset_collate();

    $sql  = "CREATE TABLE $table_name (
		      id integer(11) NOT NULL AUTO_INCREMENT,
		      client_id integer(11) NOT NULL,
		      user_id integer(11) NOT NULL,
		      UNIQUE KEY id (id)
		    ) $charset_collate;";
    
    $sql .= "ALTER TABLE ".$table_name." ADD PRIMARY KEY(`id`);";
    
    dbDelta( $sql );
    
    add_option( 'meo_crm_users_db_version', $meo_crm_users_db_version );
    
    $capabilities = array(
	        'delete_others_pages' 		=> false,
	        'delete_others_posts' 		=> false,
	        'delete_pages' 				=> false,
	        'delete_posts' 				=> false,
	        'delete_private_pages' 		=> false,
	        'delete_private_posts' 		=> false,
	        'delete_published_pages' 	=> false,
	        'delete_published_posts' 	=> false,
	        'edit_others_pages' 		=> false,
	        'edit_others_posts' 		=> false,
	        'edit_pages' 				=> false,
	        'edit_posts' 				=> false,
	        'edit_private_pages' 		=> false,
	        'edit_private_posts' 		=> false,
	        'edit_published_pages' 		=> false,
	        'edit_published_posts' 		=> false,
	        'manage_categories' 		=> false,
	        'manage_links' 				=> false,
	        'manage_broker' 			=> false,
	        'moderate_comments' 		=> false,
	        'publish_pages' 			=> false,
	        'publish_posts' 			=> false,
	        'read' 						=> false,
	        'read_private_pages' 		=> false,
	        'read_private_posts' 		=> false,
	        'upload_files' 				=> false
    );
    
    # Roles generation
    add_role(COURTIER_ROLE, 'Courtier', $capabilities);
    add_role(CLIENT_ROLE,'Client Administrateur', $capabilities);
    
    // Create action for generate page
    do_action('plugins_meo_crm_active_create_page');
    
}
register_activation_hook( __FILE__, 'meo_crm_users_install_meo_crm_users' );

# Add Scripts and Styles

	add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script_style' );
	//add_action( 'wp_enqueue_scripts', 'load_custom_wp_admin_script_style' );
	function load_custom_wp_admin_script_style() {
		
		# JS
	    wp_register_script( 'meo_crm_users_js',  plugins_url('js/backend/meo-crm-users-script.js', __FILE__), false, '1.0.0', true );
		wp_enqueue_script( 'meo_crm_users_js' );
		
		# CSS
		wp_register_style( 'meo_crm_users_css',  plugins_url('css/backend/meo-crm-users-style.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_users_css' );
	}
	
	function wpdocs_theme_name_scripts() {
	    //wp_enqueue_style( 'style-name', '/wp-content/plugins/meo-crm-users/css/frontend/meo-crm-users-style.css' );
	    wp_enqueue_script( 'script-front', plugins_url('js/frontend/meo-crm-users-script.js', __FILE__), array(), '1.0.0', true );
	    wp_localize_script( 'script-front', 'meo_crm_users_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
	add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


#########
# ADMIN #
#########

    # Add plugin menu in admin navigation

    add_action( 'admin_menu', 'meo_crm_users_admin_menu' );

    function meo_crm_users_admin_menu() {
        $page_title = 'Courtier';
        $menu_title = 'Courtier';
        $capability = 'manage_options';
        $menu_slug = 'manage_broker';
        $function = 'page_admin_manage_broker';
        $icon_url = 'dashicons-id-alt';
        $position = 7;
        add_menu_page ( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
        add_submenu_page($menu_slug,'Ajouter un courtier','Ajouter un courtier',$capability,'add_manage_broker','add_page_admin_manage_broker');
        add_submenu_page($menu_slug,'Matrice utilisateurs','Matrice utilisateurs',$capability,'manage_project_user','page_manage_users_projects');
    }

    # Display list

    function page_admin_manage_broker() {    
        include_once 'views/backend/meo-crm-users-list-view.php';
    }

    # Permet l'affichage du formulaire d'ajout de courtier

    function add_page_admin_manage_broker() { 
        include_once 'views/backend/meo-crm-users-form-view.php';
    }

    function page_manage_users_projects() { 
        include_once 'views/backend/meo-crm-users-projects-matrice.php';
    }

	
#########
# FRONT #
#########
	
    # PAGE CREATION

        # Register Plugin templates
        add_filter('meo_crm_core_templates_collector', 'meo_crm_users_templates_register', 1, 1);

        function meo_crm_users_templates_register($pluginsTemplates){

                $pluginsTemplates[MEO_CRM_USERS_LIST_TPL_FRONT] = 'MEO CRM Users List';
                $pluginsTemplates[MEO_CRM_USERS_EDIT_USER_TPL_FRONT] = 'MEO CRM Users Form';

                return $pluginsTemplates;
        }
        
        # Register Plugin Pages
        add_action( 'plugins_meo_crm_active_create_page', 'meo_crm_users_pages_register' );

        function meo_crm_users_pages_register() {
                # Create Page for Users List
                if(!meo_crm_core_slug_exists(MEO_CRM_USERS_LIST_URL)){
                    $exports_page = array(
                                    'post_title' 	=> 'MEO CRM Users List',
                                    'post_content' 	=> 'Page to be show a broker list',
                                    'post_status' 	=> 'publish',
                                    'post_type' 	=> 'page',
                                    'post_author' 	=> 1,
                                    'post_date' 	=> date('Y-m-d H:i:s'),
                                    'post_name' 	=> MEO_CRM_USERS_LIST_URL,
                                    'meta_input' 	=> array('_wp_page_template' => 'templates/meo-crm-users-front-list.php')
                    );

                    $post_id = wp_insert_post($exports_page);
                }

                # Create Page for 
                if(!meo_crm_core_slug_exists(MEO_CRM_USERS_EDIT_USER_URL)){
                    $exports_page = array(
                                    'post_title' 	=> 'MEO CRM Users Form',
                                    'post_content' 	=> 'Page to be used for insert or edit a broker',
                                    'post_status' 	=> 'publish',
                                    'post_type' 	=> 'page',
                                    'post_author' 	=> 1,
                                    'post_date' 	=> date('Y-m-d H:i:s'),
                                    'post_name' 	=> MEO_CRM_USERS_EDIT_USER_URL,
                                    'meta_input' 	=> array('_wp_page_template' => 'templates/meo-crm-users-form-broker.php')
                    );

                    $post_id = wp_insert_post($exports_page);
                }
        }
        

/* Functions */
        
	
	###########
	# CLIENTS #
	###########
		
        
        # Get Clients
        
        function meo_crm_users_getClients($returnType = ARRAY_A){
        	
        	$clients = MeoCrmUsers::getClients($returnType);
        	
        	if($clients){
        		
        		$returnedClients = array();
        		
        		foreach($clients as $client){
        			$client = meo_crm_users_mergeMeta($client);
        			$client = apply_filters('meocrmusers_getClientData', $client);
        			if($client){
        				$returnedClients[] = $client;
        			}
        		}
        		
        		return $returnedClients;
        	}
        
        	return false;
        }
        
        # Get Client Data
        
        function meo_crm_users_getClientDataById($clientId){
        	if($clientId){
        		$client = get_user_by('id', $clientId);
        		$client = $client->to_array();
        		$client = meo_crm_users_mergeMeta($client);
        		$client = apply_filters('meocrmusers_getClientData', $client);
        		return $client;
        	}
        	return false;
        }
        
        # Get Client ID by Courtier
        
        add_filter('meocrmusers_getCourtierData', 'meo_crm_users_getClientIdByCourtier');
        
        function meo_crm_users_getClientIdByCourtier($courtier){
        	if($courtier){
        		
        		$clientId = MeoCrmUsers::getClientIdByCourtierId($courtier['ID']);
        		
        		if($clientId){
        			$courtier['client_id'] = $clientId;
        		}
        		return $courtier;
        	}
        	return false;
        }
        
        # Get Client by Courtier ID
        
        function meo_crm_users_getClientByCourtierID($courtierID){
        	if($courtierID){
        		
        		$clientId = MeoCrmUsers::getClientIdByCourtierId($courtierID);
        		echo $clientId.'-';
        		if($clientId){
        			$client = get_user_by('id', $clientId);
        			$client = $client->to_array();
        			$client = meo_crm_users_mergeMeta($client);
        			return $client;
        		}
        	}
        	return false;
        }
		
		
	#############
	# COURTIERS #
	#############
		
        
        # Get Courtiers
        
        function meo_crm_users_getCourtiers($returnType = ARRAY_A){
        	
        	$courtiers = MeoCrmUsers::getCourtiers($returnType);
        	
        	if($courtiers){
        	
        		$returnedCourtiers = array();
        	
        		foreach($courtiers as $kc => $courtier){
        			
        			$courtier = meo_crm_users_mergeMeta($courtier);
        			$courtier = apply_filters('meocrmusers_getCourtierData', $courtier);
        			if($courtier){
        				$returnedCourtiers[] = $courtier;
        			}
        		}
        	
        		return $returnedCourtiers;
        	}
        	
        	return false;
        }
        
        # Get Courtiers by Client ID
        
        function meo_crm_users_getCourtiersByClientID($clientID, $returnType = ARRAY_A){
        	 
        	$courtiers = MeoCrmUsers::getCourtiersByClientID($clientID, $returnType);
        	
        	if($courtiers){
        		 
        		$returnedCourtiers = array();
        		 
        		foreach($courtiers as $kc => $courtier){
        			 
        			$courtier = meo_crm_users_mergeMeta($courtier);
        			$courtier = apply_filters('meocrmusers_getCourtierData', $courtier);
        			if($courtier){
        				$returnedCourtiers[] = $courtier;
        			}
        		}
        		 
        		return $returnedCourtiers;
        	}
        	 
        	return false;
        }
        
        # Get Courtiers by ProjectID
        
        function meo_crm_users_getCourtiersByProjectID($projectID){
        	 
        	$courtiers = MeoCrmUsers::getCourtiersByProjectID($projectID);
			
        	if($courtiers){
        		 
        		$returnedCourtiers = array();
        		
        		foreach($courtiers as $courtier){
        			$courtier = meo_crm_users_mergeMeta($courtier);
        			$courtier = apply_filters('meocrmusers_getCourtierData', $courtier);
        			if($courtier){
        				$returnedCourtiers[] = $courtier;
        			}
        		}
        		 
        		return $returnedCourtiers;
        	}
        	 
        	return false;
        }
        
        # Get Courtier by Seach
        
        function meo_crm_users_getCourtiersBySearch($search){
        	
        	$courtiers = MeoCrmUsers::getCourtiersBySearch($search);
        	
        	if($courtiers){
        		 
        		$returnedCourtiers = array();
        	
        		foreach($courtiers as $courtier){
        			$courtier = meo_crm_users_mergeMeta($courtier);
        			$courtier = apply_filters('meocrmusers_getCourtierData', $courtier);
        			if($courtier){
        				$returnedCourtiers[] = $courtier;
        			}
        		}
        		 
        		return $returnedCourtiers;
        	}
        	
        	return false;
        }
        
        # Get Courtier Data
        
        function meo_crm_users_getCourtierDataById($courtierId){
        	if($courtierId){
        		$courtier = get_user_by('id', $courtierId);
        		$courtier = meo_crm_users_mergeMeta($courtier->to_array());
        		$courtier = apply_filters('meocrmusers_getCourtierData', $courtier);
        		return $courtier;
        	}
        	return false;
        }
        
        # Merge User Meta with User
        
        function meo_crm_users_mergeMeta($user){
        	
        	$userMeta = get_user_meta($user['ID']);
        	$tempMeta = array();
        	# Meta
        	if($userMeta && is_array($userMeta)){
        		foreach($userMeta as $meta => $arrayValue){
        			$tempMeta[$meta] = reset($arrayValue);
        		}
        		 
        		$user = array_merge($user, $tempMeta);
        	}
        	
        	return $user;
        	
        }
        
        
	#############################
	# SAVE & VALIDATE USER FORM #
	#############################
	
        
        # Add / Update User
        # @param posted $form(Array)
        # @return int courtier_id or array of errors
        
        function meo_crm_users_processUserForm($form){
        	
        	global $wpdb;
        
        	// Define current id
        	if(isset($form['client_id']) && !empty($form['client_id'])){
        		$current_id = $form['client_id'];
        	} else $current_id = wp_get_current_user()->ID;
        
        	# ADD NEW USER
        
        	if(empty($form['courtier_id'])){
        
        		# Validate Form
        		$result = meo_crm_users_validateUserForm($form);
        
        		if($result && !is_array($result)){
        			 
        			$dataUser = array(
        					'user_login' 		=> $form['user_login'],
        					'user_pass' 		=> wp_hash_password($form['user_pass']),
        					'user_nicename' 	=> $form['user_login'],
        					'user_email' 		=> $form['user_email'],
        					'user_url' 			=> '',
        					'user_registered' 	=> date('Y-m-d H:i:s'),
        					'user_status' 		=> 0,
        					'display_name' 		=> $form['first_name'].' '.$form['last_name']
        			);
        
        			# User
        			$user_id = MeoCrmUsers::addUser($dataUser);
        				
        			if($user_id){
        				
        				# User Management
        				MeoCrmUsers::addUserManagement( array( 'client_id' => $current_id, 'user_id' => $user_id ) );
        				
        				$dataMeta = array(
        						'user_login' 			=> $form['user_login'],
        						'first_name' 			=> $form['first_name'],
        						'last_name' 			=> $form['last_name'],
        						'description' 			=> '',
        						'rich_editing' 			=> 'true',
        						'comment_shortcuts' 	=> 'false',
        						'admin_color' 			=> 'fresh',
        						'use_ssl' 				=> '0',
        						'show_admin_bar_front' 	=> 'false',
        						'wp_capabilities' 		=> 'a:1:{s:8:"'.COURTIER_ROLE.'";b:1;}',
        						'wp_user_level' 		=> '0',
        						'dismissed_wp_pointers' => '',
        						'signature' 			=> $form['signature'],
        				);
        
        				# User Meta
        				foreach($dataMeta as $meta_key => $meta_value){
        					
        					update_user_meta( $user_id, $meta_key, $meta_value );
        				}
        				
        				return $user_id;
        
        			} // /if user_id
        
        		} // /if valid
        
        		# Errors
        		return $result;
        	}
        
        	# UPDATE EXISTING USER
        
        	else {
        
        		// Variable update
        		$result 				= array();
        		$dataUser 				= array();
        		$dataUserMeta 			= array();
        		$dataUserManage 		= array();
        		$change_display_name 	= false;
        		 
        		$user = meo_crm_users_getCourtierDataById($form['courtier_id']);
        
        		$result = meo_crm_users_validateUserForm($form, $user);
        
        		if($result && !is_array($result)){
        			 
        			# Check changes
        
        			if($form['first_name'] != $user['first_name']){
        				$change_display_name = true;
        				$dataUserMeta[] = array('meta_key' => 'first_name', 'meta_value' => $form['first_name']);
        			}
        
        			if($form['last_name'] != $user['last_name']){
        				$change_display_name = true;
        				$dataUserMeta[] = array('meta_key' => 'last_name', 'meta_value' => $form['last_name']);
        			}
        
        			if($form['user_login'] != $user['user_login']){
        				$dataUser['user_login'] = $form['user_login'];
        				$dataUser['user_nicename'] = $form['user_login'];
        				$dataUserMeta[] = array('meta_key' => 'nickname', 'meta_value' => $form['user_login']);
        			}
        			 
        			if($form['user_email'] != $user['user_email']){
        				$dataUser['user_email'] = $form['user_email'];
        			}
        
        			if(!empty($form['user_pass']) && wp_hash_password($form['user_pass']) != $user['password']){
        				$dataUser['user_pass'] = wp_hash_password($form['user_pass']);
        			}
        			
        			if($form['signature'] != $user['signature']){
        				$dataUserMeta[] = array('meta_key' => 'signature', 'meta_value' => $form['signature']);
        			}
        
        			if($change_display_name){
        				$dataUser['display_name'] = $form['first_name'].' '.$form['last_name'];
        			}
        
        			# Client ID changed
        			if($user['client_id'] != $form['client_id']){
        				$dataUserManage['client_id'] = $form['client_id'];
        			}
        
        			# User data Update
        			if(is_array($dataUser) && count($dataUser) > 0){
        				 
        				$dataUser['ID'] = $form['courtier_id'];
        				if(!wp_update_user( $dataUser )){
        					echo "<div>Erreur data user<br/></div>";
        				}
        			}
        
        			# User meta data update
        			if(is_array($dataUserMeta) && count($dataUserMeta) > 0){
        				 
        				$user_id = $form['courtier_id'];
        
        				foreach($dataUserMeta as $index => $meta){
        					if(!update_user_meta( $user_id, $meta['meta_key'], $meta['meta_value'] )){
        						echo "<div>Erreur data user meta [".$meta['meta_key']."]<br/></div>";
        					}
        				}
        			}
        
        			# User management data update
        			if(is_array($dataUserManage) && count($dataUserManage) > 0){
        				 
        				$where = array('user_id' => $form['courtier_id']);
        
        				if(!MeoCrmUsers::updateUserManagement($dataUserManage, $where)){
        					echo "<div>Erreur data user manage<br/></div>";
        				}
        			}
        
        			return $form['courtier_id'];
        		}
        
        		# Errors
        		return $result;
        	}
        
        	return false;
        }
        
        # Add/Update Courtier Form Validation: return boolean TRUE if no errors, otherwise, array of errors
        
        function meo_crm_users_validateUserForm($form, $user = false){
        	 
        	$errors = array();
        
        	if( ( !$user && !empty($form['user_pass']) && !empty($form['conf_user_pass']) )
        			|| $user ){
        		# PASSWORD
        		if($form['user_pass'] != $form['conf_user_pass']){
        			$errors[] = '<strong>ERREUR : </strong>Le mot de passe et la confirmation ne correspondent pas';
        		}
        	}
        
        	if(!empty($form['first_name']) && !empty($form['last_name']) && !empty($form['user_login']) && !empty($form['user_email'])){
        		 
        		# FIRST NAME
        		if(!preg_match('/^[a-zA-Z]+$/', $form['first_name'])){
        			 
        			$errors[] = '<strong>ERREUR : </strong>Le pr&eacute;nom ne peut contenir que des lettres';
        		}
        		# LAST NAME
        		if(!preg_match('/^[a-zA-Z]+$/', $form['first_name'])) {
        			 
        			$errors[] = '<strong>ERREUR : </strong>Le nom ne peut contenir que des lettres';
        		}
        		# USER LOGIN
        		if( ( username_exists($form['user_login']) && !$user )
        		|| ( username_exists($form['user_login']) && $user['user_login'] != $form['user_login'] ) ){
        
        			$errors[] = '<strong>ERREUR : </strong>L\'identifiant que vous avez choisi est d&eacute;j&agrave;� utilis&eacute;';
        		}
        		# EMAIL
        		if( ( !is_email($form['user_email']) && !$user )
        		|| ( !is_email($form['user_email']) && $user['email'] != $form['user_email'] ) ){
        
        			$errors[] = '<strong>ERREUR : </strong>Votre email n\'est pas valide';
        		}
        		if( ( email_exists($form['user_email']) && !$user )
        				|| ( email_exists($form['user_email']) && $user['user_email'] != $form['user_email'] ) ){
        			 
        			$errors[] = '<strong>ERREUR : </strong>Votre email est d&eacute;j&agrave; utilis&eacute;';
        		}
        	} else {
        		$errors[] = '<strong>ERREUR : </strong>Tous les champs n\'ont pas &eacute;t&eacute; remplis';
        	}
        
        	if(count($errors) > 0){
        		return $errors;
        	}
        	return true;
        }