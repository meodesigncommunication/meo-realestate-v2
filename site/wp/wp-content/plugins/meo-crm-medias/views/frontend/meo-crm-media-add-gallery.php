<?php

    # GLOBAL
    global $wpdb;

    # Defines / Constants
    define('MESSAGE_GALLERY_INSERT', 'La galerie a &eacute;t&eacute; correctement enregistr&eacute;e');
    define('MESSAGE_GALLERY_INSERT_ERROR', 'La galerie n\'a pas &eacute;t&eacute; correctement enregistr&eacute;e');    
    define('MESSAGE_GALLERY_UPDATE', 'La galerie a &eacute;t&eacute; correctement mis &agrave; jour');
    define('MESSAGE_GALLERY_UPDATE_ERROR', 'La galerie n\'a pas &eacute;t&eacute; correctement mis &agrave; jour');
    define('MESSAGE_GALLERY_UPDATE_ATTACHMENT_ERROR', 'Les fichiers attach&eacute; n\'ont pas &eacute;t&eacute; mis à jour correctement');

    // Load class
    $galleryModel = new GalleryModel();    
    $helper = new MeoCrmCoreHelper();
    $validation = new MeoCrmCoreValidationForm();
    
    // Variables
    $insert = false;
    $page_title = (isset($_GET['id']) && !empty($_GET['id']))? 'Modifier galerie' : 'Ajouter galerie';
    $current_user = wp_get_current_user();
    $projects = ProjectModel::selectAllProject(wp_get_current_user()); 
    
    /*if($_SERVER['REQUEST_METHOD'] == 'POST')
    {        
        // Set variables
        $flash_message  = '';
        
        // Get POST value
        $data = array();
        $insert = false;
        $data['id'] = $_POST['gallery_id'];
        $data['author_id'] = $_POST['author_id'];
        $data['project_id'] = $_POST['project_id'];
        $data['name'] = $_POST['name'];
        $data['slug'] = $_POST['slug'];
        $data['description'] = $_POST['description'];
        $data['private'] = $_POST['private'];
        $data['status'] = $_POST['status'];        
        $data_media = $_POST['medias'];
        
        // Declare a validation rules
        $rules = array(
            'name' => 'required|min:5|max:100',
            'slug' => 'required|slug',
            'description' => 'required|text|min:5'
        );
        
        //
        $results_validation = $validation->validationDatas($rules, $_POST);        
        $results = $validation->checkResultValidation($results_validation);
        
        // Test a validation rules
        $validate = $results['result'];  
        if($validate)
        {
            
            if(isset($_GET['id']) && !empty($_GET['id']))
            {
                $medias = FileModel::searchMedias('', '', $data['project_id'], '');
                
                $where = array(
                    'id' => $_GET['id']
                );
                
                if(GalleryModel::updateGallery($data, $where))
                {        
                    
                    if(GalleryModel::updateGalleryAttachement($_GET['id'],$data_media))
                    {
                        $message  = MESSAGE_GALLERY_UPDATE;
                        $flash_message  = $helper->getFlashMessageAdmin($message, 1);
                    }else{
                        $message  = MESSAGE_GALLERY_UPDATE_ATTACHMENT_ERROR;
                        $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                    }
                }else{
                    $message  = MESSAGE_GALLERY_UPDATE_ERROR;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                }                      
                      
            }else{
                
                $result = GalleryModel::insertGallery($data);

                echo $wpdb->last_query;

                if($result['valid'] && GalleryModel::updateGalleryAttachement($result['gallery_id'],$data_media))
                {
                    echo $wpdb->last_query;
                    $insert = true;
                    $id_new = $result['gallery_id'];                    
                    $message  = MESSAGE_GALLERY_INSERT;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 1);
                }else{
                    echo $wpdb->last_query;
                    $message  = MESSAGE_GALLERY_INSERT_ERROR;
                    $flash_message  = $helper->getFlashMessageAdmin($message, 0);
                }
                
            }
            
        }else{
            $flash_message  = $helper->getFlashMessageAdmin($results['message'], 0);
        }
    }else{
        
        // Get POST value
        $data = array();
        
        if(isset($_GET['id']) && !empty($_GET['id']))
        {
            $gallery = GalleryModel::selectGalleryById($_GET['id']);            
            $medias = FileModel::searchMedias('', '', $gallery[0]->project_id, '');
            
            $data['id'] = $gallery[0]->id;
            $data['author'] = $gallery[0]->author;
            $data['project_id'] = $gallery[0]->project_id;
            $data['name'] = $gallery[0]->name;
            $data['slug'] = $gallery[0]->slug;
            $data['description'] = $gallery[0]->description;
            $data['private'] = $gallery[0]->private;
            $data['status'] = $gallery[0]->status;
        }
    }
    
    if(isset($_GET['id']) && !empty($_GET['id']))
    {
        $gallery_attachment = GalleryModel::selectGalleryAttachmentByGalleryId($_GET['id']);

    }else if((isset($result['gallery_id']) && !empty($result['gallery_id']))) {
        $gallery_attachment = GalleryModel::selectGalleryAttachmentByGalleryId($result['gallery_id']);
    }

$link_list_project = site_url('meo-crm-medias-gallery-list');*/

get_header();
?>
<div class="wrap meo-crm-users-list">
    <?php 
        if(!empty($flash_message))
        {
            echo $flash_message;    
        }      
    ?>
    <h1><?php echo $page_title; ?><a class="page-title-action" href="<?php echo $link_list_project; ?>">Retour</a></h1>
    <form id="form-add-gallery" name="from_gallery" method="POST">
        <input type="hidden" name="gallery_id" id="gallery_id" value="<?php echo (isset($_GET['id'])) ? $_GET['id'] : 0; ?>" />
        <input type="hidden" name="author_id" id="author_id" value="<?php echo (isset($data['author_id']) && !empty($data['author_id'])) ? $data['author_id'] : $current_user->ID ?>" />
        <table class="form-table meo-form-table">
            <tr class="form-field form-required">
                <th>
                    <label for="name_gallery">
                        Projet
                    </label>
                </th>
                <td>
                    <select id="project_id" name="project_id">
                        <option value="-1">Veuillez choisir un projet</option>
                        <?php foreach($projects as $project): ?>
                            <?php $checked  = '' ?>
                            <?php if(isset($data['project_id']) && $data['project_id'] == $project->id): ?>
                                <?php $checked  = 'selected=true' ?>
                            <?php endif; ?>
                            <option <?php echo $checked ?> value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="name_gallery">
                        Nom de la galerie 
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="name" id="name" placeholder="" value="<?php echo (!empty($data['name']))? $data['name'] : ''; ?>" />
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="name_gallery">
                        Slug de la galerie 
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="slug" id="slug" placeholder="" value="<?php echo (!empty($data['slug']))? $data['slug'] : ''; ?>" />
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="description">
                        Description de la galerie
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <textarea id="description" name="description"><?php echo (!empty($data['description']))? $data['description'] : '' ?></textarea>
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="private">
                        Galerie privé
                    </label>
                </th>
                <td>
                    <?php if(isset($data['private']) && $data['private']): ?>
                        <?php $checked  = 'checked=true' ?>
                    <?php endif; ?>
                    <input <?php echo $checked; ?> type="checkbox" name="private" value="1" />
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="status">
                        Galerie active
                    </label>
                </th>
                <td>
                    <?php if(isset($data['status']) && $data['status']): ?>
                        <?php $checked  = 'checked=true' ?>
                    <?php endif; ?>
                    <input <?php echo $checked; ?> type="checkbox" name="status" value="1" />
                </td>
            </tr>
        </table>
        <?php if(isset($_GET['id']) && !empty($_GET['id'])): ?>
            <label style="font-weight:bold;font-size:14px;" for="">
                Médias disponible
            </label>
            <div id="container-medias" class="bloc-drag-drop selected-gallery" style="display:block;margin-top:15px;">
                <?php
                
                    $html = ''; 
                    $not_show = array();
                    
                    if(is_array($gallery_attachment) && !empty($gallery_attachment))
                    {
                        foreach($gallery_attachment as $attachment)
                        {
                            if(is_array($medias) && !empty($medias))
                            {
                                foreach($medias as $key => $media)
                                {
                                    if($attachment->attachment_id == $key)
                                    {
                                        $html .= MeoCrmCoreFileManager::showMediaElement($media,$key,'gallery',true,true,'selectedMedia(this)');
                                        $not_show[] = $key;
                                    }
                                }
                            }
                        }
                    }
                    
                    if(is_array($medias) && !empty($medias))
                    {
                        foreach($medias as $key => $media)
                        {
                            $show_image = true;
                            
                            foreach($not_show as $value)
                            {
                                if($value == $key)
                                {
                                    $show_image = false;
                                }                                
                            }
                            
                            if($show_image)
                            {
                                $html .= MeoCrmCoreFileManager::showMediaElement($media,$key,'gallery',true,false,'selectedMedia(this)');
                            }
                        }
                    }else{
                        $html .= '<p>Aucun média existant</p>';
                    }
                    
                    echo $html;
                ?>
                <div style="clear:both"></div>
            </div>
        <?php else: ?>
            <label style="font-weight:bold;font-size:14px;" for="">
                Médias disponible
            </label>
            <div id="container-medias" class="bloc-drag-drop selected-gallery" style="display:block;margin-top:15px;">
                
            </div>
            <div style="clear:both"></div>
        <?php endif; ?>
        <p class="submit">
            <input id="createproject" class="button button-primary" type="submit" name="createproject" value="<?php echo $page_title; ?>">
        </p>
    </form>
</div>

<?php
get_footer();
?>