<?php 
    global $wpdb;

    $link_add_gallery =  site_url('meo-crm-gallery-form-medias');
    $link_add_media = site_url('upload-medias');
    
    $user_projects = ProjectModel::getUserProjects(wp_get_current_user());
    $medias = FileModel::selectAllMedias(wp_get_current_user(),$user_projects);    
    $mimeTypes = FileModel::selectAllMimeType(); 
    $users = UsersModel::selectAllUsers();
    $projects = meo_crm_projects_getProjects();

get_header();
?>
<main class="media-list" role="main">
    <section>
        <h1>
            Biblioth&egrave;que des m&eacute;dias
            <a class="page-title-action" href="<?php echo $link_add_gallery; ?>">Ajouter une galerie</a>
            <a class="page-title-action" href="<?php echo $link_add_media; ?>">Upload de media</a>
        </h1>
        <!-- Filter -->
        <div class="media-toolbar wp-filter" style="padding: 10px 10px;">
            <div class="media-toolbar-secondary">
                <i class="fa fa-search" aria-hidden="true"></i>
                <input type="text" name="search" value="" placeholder="Rechercher" style="margin: 0 10px;" />

                <?php if(wp_get_current_user()->roles[0] == 'administrator'): ?>
                    <label for="users">Utilisateurs</label>
                    <select id="users" name="users" style="margin-right: 10px;">
                        <option value="0">Choisir un utilisateur</option>
                        <?php foreach($users as $user): ?>
                            <option value="<?php echo $user['ID'] ?>"><?php echo $user['display_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php endif; ?>

                <label for="projects">Projets</label>
                <select id="projects" name="projects" style="margin-right: 10px;">
                    <option value="0">Choisir un projet</option>
                    <?php foreach($projects as $project): ?>
                        <option value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <label for="projects">Type de m&eacute;dia</label>
                <select id="type" name="type" style="margin-right: 10px;">
                    <option value="0">Choisir un type de m&eacute;dia</option>
                    <?php foreach($mimeTypes as $mimeType): ?>
                        <option value="<?php echo $mimeType->post_mime_type ?>"><?php echo $mimeType->post_mime_type ?></option>
                    <?php endforeach; ?>
                </select>
                <label for="date_start">Date de</label>
                <input class="datepicker" type="text" name="date_start" value="" />
                <label for="date_end">au</label>
                <input class="datepicker" type="text" name="date_end" value="" />
                <input id="btn-search-media" type="submit" class="button action meo-crm-projects-search-btn" value="Rechercher" />
            </div>
        </div>
        <!-- / Filter -->
        <div id="container-medias">
            <?php echo MeoCrmCoreFileManager::showMedias($medias); ?>
        </div>
    </section>
</main>
<?php
get_footer();
?>