<?php
    $url = get_site_url().'/wp-admin/admin-ajax.php';
    $projects = ProjectModel::selectAllProject(wp_get_current_user());    
?>
<h1>Upload de médias</h1>

<table class="form-table meo-form-table">
    <tr class="form-field form-required">
        <th>
            <label for="project">Choisir un projet</label>
        </th>
        <td>
            <select class="w20" id="project" name="project">
                <?php foreach($projects as $project): ?>
                    <option value="<?php echo $project['id'] ?>"><?php echo $project['name'] ?></option>
                <?php endforeach; ?>
            </select>  
        </td>
    </tr>
</table>  
<input type="hidden" id="action" name="action" value="manageFileUploaded" />
<form class="box" method="post" action="<?php echo $url; ?>" enctype="multipart/form-data">    
    <div class="box__input">
        <div class="icon">
            <i class="fa fa-download" aria-hidden="true"></i>
        </div>        
        <input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" multiple />
        <label for="file"><strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
        <label for="btn-upload">Click on the button for uplaod all files</label>
        <button class="box__button button button-primary" name="btn-upload" type="submit">Upload</button>
    </div>
    <div class="box__uploading">Uploading&hellip;</div>
    <div class="box__success">Done!</div>
    <div class="box__error">Error!</div>
</form>
<div id="list-upload">
    <!-- List File Uploaded -->
</div>
<div id="loader" class="loader">
    <p>Wait upload in progress</p>
    <img width="50" src="<?php echo get_site_url().'/wp-content/plugins/meo-crm-core/images/loader.gif'; ?>" alt="wait please" />
</div>

