<?php

//Init Define
define('MESSAGE_MEDIA_INSERT', 'Le média a &eacute;t&eacute; correctement enregistr&eacute;e');
define('MESSAGE_MEDIA_INSERT_ERROR', 'Le média n\'a pas &eacute;t&eacute; correctement enregistr&eacute;e');    
define('MESSAGE_MEDIA_UPDATE', 'Le média a &eacute;t&eacute; correctement mis &agrave; jour');
define('MESSAGE_MEDIA_UPDATE_ERROR', 'Le média n\'a pas &eacute;t&eacute; correctement mis &agrave; jour');

// Init variable
$id = (isset($_GET['id']) && !empty($_GET['id']))? $_GET['id'] : 0 ;
$media = FileModel::selectMediaById($id);
$helper = new MeoCrmCoreHelper();
$validation = new MeoCrmCoreValidationForm();

$link_admin = get_admin_url().'admin.php';
$link_manage_gallery = $link_admin.'?page=manage_gallery';

// Get mime type for check media type
list($group,$type) = explode('/', $media['post_mime_type']);

// Envoi du formulaire
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $data['post_title'] = $_POST['title'];
    $data['post_content'] = $_POST['description'];
    
    // Declare a validation rules
    $rules = array(
        'title' => 'required|min:5|max:100',
        'description' => 'required|min:5'
    );
    
    // Validation du formulaire
    $results_validation = $validation->validationDatas($rules, $_POST);     
    $results = $validation->checkResultValidation($results_validation);

    // Test a validation rules
    $validate = $results['result'];  
    if($validate)
    {
        // Update du media
        $where = array(
            'ID' => $id
        );        
        if(FileModel::updateMedia($data, $where))
        {
            $message  = MESSAGE_MEDIA_UPDATE;
            $flash_message  = $helper->getFlashMessageAdmin($message, 1);
        }else{
            $message  = MESSAGE_MEDIA_UPDATE_ERROR;
            $flash_message  = $helper->getFlashMessageAdmin($message, 0);
        } 
    }else{
        $flash_message  = $helper->getFlashMessageAdmin($results['message'], 0);
    }
}else{
    $data['post_title'] = $media['post_title'];
    $data['post_content'] = $media['post_content'];
}

// Détecte le type de fichier pour déterminer l'affichage
switch($group)
{
    case 'image':
        $preview  = '<div class="media-preview-image" style="background: url('.$media['guid'].') no-repeat;"></div>';        
        $preview .= '<div class="media-data-image" style="">';
        $preview .=     '<label for="url_media">URL</label>';
        $preview .=     '<input class="url_media" type="text" name="url_media" value="'.$media['guid'].'" />' ;
        $preview .=     '<p>Uploadé le <strong>'.$media['post_date'] ;
        $preview .=     '</strong> par '.$media['post_author'].'</p>' ;
        $preview .= '</div>';        
        break;

    case 'audio':
        $preview  = '<div class="media-preview-image" style="background: #dedede no-repeat;"><i class="fa fa-file-audio-o"></i></div>';        
        $preview .= '<div class="media-data-image" style="">';
        $preview .=     '<label for="url_media">URL</label>';
        $preview .=     '<input class="url_media" type="text" name="url_media" value="'.$media['guid'].'" />' ;
        $preview .=     '<p>Uploadé le <strong>'.$media['post_date'] ;
        $preview .=     '</strong> par '.$media['post_author'].'</p>' ;
        $preview .= '</div>';        
        break;

    case 'video':
        $preview  = '<div class="media-preview-image" style="background: #dedede no-repeat;"><i class="fa fa-file-video-o"></i></div>';        
        $preview .= '<div class="media-data-image" style="">';
        $preview .=     '<label for="url_media">URL</label>';
        $preview .=     '<input class="url_media" type="text" name="url_media" value="'.$media['guid'].'" />' ;
        $preview .=     '<p>Uploadé le <strong>'.$media['post_date'] ;
        $preview .=     '</strong> par '.$media['post_author'].'</p>' ;
        $preview .= '</div>';        
        break;
    
    case 'application':
        if($type = 'pdf')
        {
            $preview  = '<div class="media-preview-image" style="background: #dedede no-repeat;"><i class="fa fa-file-pdf-o"></i></div>';        
            $preview .= '<div class="media-data-image" style="">';
            $preview .=     '<label for="url_media">URL</label>';
            $preview .=     '<input class="url_media" type="text" name="url_media" value="'.$media['guid'].'" />' ;
            $preview .=     '<p>Uploadé le <strong>'.$media['post_date'] ;
            $preview .=     '</strong> par '.$media['post_author'].'</p>' ;
            $preview .= '</div>';  
        }
        break;

    default:
        break;
}

?>
<div class="wrap meo-crm-media-list">
    <?php 
        if(!empty($flash_message))
        {
            echo $flash_message;    
        }      
    ?>
    <h1>Édition d'un média <a id="btn-previous-page" class="page-title-action" href="#" onclick="window.history.back()">Retour</a></h1>
    <div class="row"> 
        <div class="col-preview-media">
            <div id="preview-media">
                <?php echo $preview; ?>
            </div>
        </div>
        <form name="from_media_data" method="POST">
            <div class="col-data-media">        
                <table class="form-table meo-form-table">
                    <tr class="form-field form-required">
                        <th>
                            <label for="name_gallery">
                                Titre du média
                                <span class="description">(obligatoire)</span>
                            </label>
                        </th>
                        <td>
                            <input type="text" name="title" id="title" placeholder="" value="<?php echo $data['post_title'] ?>" />
                        </td>
                    </tr>
                    <tr class="form-field form-required">
                        <th>
                            <label for="description">
                                Description du média
                                <span class="description">(obligatoire)</span>
                            </label>
                        </th>
                        <td>
                            <textarea id="description" name="description"><?php echo $data['post_content'] ?></textarea>
                        </td>
                    </tr>
                </table>
                <p class="submit">
                    <input id="saveMediaData" class="button button-primary" type="submit" value="Enregistrer" name="saveMedia">
                </p>
            </div> 
        </form>
    </div>
</div>

