<?php 
    define('MESSAGE_SPINNER_INSERT', 'La galerie a &eacute;t&eacute; correctement enregistr&eacute;e');

    $link_admin = get_admin_url().'admin.php';
    $link_add_gallery = $link_admin.'?page=add_spinner';
    $link_add_media = $link_admin.'?page=add_media';
    $edit_url = $link_add_gallery.'&id=@id';
    
    $header = MeoCrmMediaConfiguration::getSpinnerListHeader();     
    $list_action = MeoCrmMediaConfiguration::getSpinnerListAction();
    $helper = new MeoCrmCoreHelper();
    $galleries = SpinnerModel::selectAllSpinners();
    $helperList = new MeoCrmCoreListHelper();
    
    if(isset($_GET['message']) && $_GET['message'] == 'MESSAGE_SPINNER_INSERT'):
        $flash_message  = $helper->getFlashMessageAdmin($_GET['message'], 1);
    endif;
?>
<div class="wrap meo-crm-media-list">
    <h1>
        Biblioth&egrave;que des m&eacute;dias
        <a class="page-title-action" href="<?php echo $link_add_gallery; ?>">Ajouter un spinner</a>
    </h1>    
    <div id="zoneMessage"><?php echo (isset($_GET['message']) && !empty($_GET['message'])) ? $flash_message : ''; ?></div>
    <div id="content-table">
        <?php echo $helperList->getList($galleries, $header, $list_action, true) ?>
    </div>
</div>
