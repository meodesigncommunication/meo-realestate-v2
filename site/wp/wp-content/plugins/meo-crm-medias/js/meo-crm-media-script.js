/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function(){
    // Enable Spinner Plugin
    // Preview spinner in spinner page  
    // Source plugin JS http://www.mathieusavard.info/threesixty/demo.html  
    // Method 'click', auto{method:'auto',autoscrollspeed:100}
    if(typeof images != 'undefined' && $.isArray(images)){
        $("#preview_spinner_manuel").threesixty({
            images:images,         
            method:'mousemove', 
            'cycle':2, 
            direction:"backward"
        });
    }
});

jQuery(function($){
    /*
    $('.loader').css('display','none');
    $('.loader-drop').css('display','none');
    */   
    // ***************************************************************
    // Input recherche date picker voir si fred a ajouter au noyau
    // ***************************************************************
    
    // défini le contenu de #media-table comme drag and drop content
    $( "#media-table" ).sortable();
    $( "#media-table" ).disableSelection();
    
    $(".bloc-drag-drop").sortable();
    $(".bloc-drag-drop").disableSelection(); 
    
    // Définir des onglets
    $( "#tabs" ).tabs();
    
    $('#form-add-gallery #project_id').change(function(){            
       $.post(
           meo_crm_media_ajax.ajax_url,
            {
                'action': 'changeMediaGallery',
                'project_id': $(this).val()
            },
            function(response){
                $('#container-medias').html(response);
            }
        );
    });
    
    // Element qui va permettre l'upload par drag and drop
    $('.box').dropfile({
        url     : meo_crm_media_ajax.ajax_url
    });
    
    // Déclaration des datepicker pour les filtre de recherche des médias
    $('input[name=date_start]').datepicker({
        dateFormat : 'dd/mm/yy',
        changeMonth: true,
        onSelect: function( selectedDate ) {
            $('input[name=date_end]').datepicker('option', 'minDate', selectedDate );
        }
    });    
    $('input[name=date_end]').datepicker({
        dateFormat : 'dd/mm/yy',
        changeMonth: true,
        onSelect: function( selectedDate ) {
            $('input[name=date_start]').datepicker('option', 'maxDate', selectedDate );
        }
    });
    
    // Permet de lancer la recherche des médias selon les filtre choisi 
    $('#btn-search-media').click(function(){
        
        $.post(
            meo_crm_media_ajax.ajax_url,
            {
                'action': 'searchMedia',
                'search': $('input[name=search]').val(),
                'author': $('#users').val(),
                'project': $('#projects').val(),
                'mime_type': $('#type').val(),
                'date_format': 'd/m/y',
                'date_start': $('input[name=date_start]').val(),
                'date_end': $('input[name=date_end]').val()
            },
            function(response){
                console.log('response: '+response);
                $('div#container-medias').html(response);
            }
        );

    }); 
    
    $('#spinner-valid-sort').click(function(){
        
        var count = 1;
        var obj = [];
        
        $('input[name="medias[]"]').each(function(){
            obj.push({id:$(this).val()});
            count++;
        });
        
        var json_data = JSON.stringify(obj);
        
        $.post(
            meo_crm_media_ajax.ajax_url,
            {
                'action': 'saveMediaSpinner',
                'datas': obj,
                'spinner_id': $('#spinner_id').val()
            },
            function(response){
                $('div#message').html(response);
            }
        );
        
    });
    
    $('#spinner-delete-all').click(function(){
        if(confirm('Voulez-vous vraiment supprimer toutes les images du spinner ?'))
        {
            var spinner_id = $('#spinner_id').val();
            
            alert(spinner_id);
            
            $.post(
                meo_crm_media_ajax.ajax_url,
                {
                    'action': 'deleteAllSpinnerMedia',
                    'spinner_id': spinner_id
                },
                function(response){
                    console.log(response);
                    location.reload();
                    alert('OKI OKI OKI OKI !!! :)');
                }
            );
            
            
        }
    });
    
    // Sélectionne un média dans les galerie ou spinner
    $('.selected-gallery .preview-image-upload').click(selectedMedia(this));
    
});

/*
 * 
 */
function selectedMedia(element)
{
    if($(element).hasClass('selected-media'))
    {
        $(element).removeClass('selected-media');
        $(element).find('input[name="medias[]"]').attr('checked',false);
    }else{
        $(element).addClass('selected-media');
        $(element).find('input[name="medias[]"]').attr('checked',true);
    }   
}
/*
 * Permet la suppression d'un média
 */
function deleteMedia(id,url,type)
{
    
    if(type == 'all'){
        // Type = all media
    }else if(type == 'search'){
        // Type = search media
    }else if(type == 'gallery'){
        // Type = gallery
    }else if(type == 'spinner'){
        // Type = spinner
    }else{
        type = 'all';
    }
    
    $.post(
        meo_crm_media_ajax.ajax_url,
        {
            'action': 'deleteMedia',
            'id': id,
            'url': url,
            'type': type,
            'search': $('input[name=search]').val(),
            'author': $('#users').val(),
            'project_id': $('#project_id').val(),
            'project': $('#projects').val(),
            'mime_type': $('#type').val(),
            'date_format': 'd/m/y',
            'date_start': $('input[name=date_start]').val(),
            'date_end': $('input[name=date_end]').val(),
            'spinner_id': $('input[name=spinner_id]').val(),
            'gallery_id': $('input[name=gallery_id]').val()
        },
        function(response){
            console.log('response: '+response);
            $('div#container-medias').html(response);
            alert('Votre fichier a bien été supprimé');
        }
    );
}

// Change THE URL without recharge a page 
function testURL()
{
    window.history.pushState(document.title,document.title,"/wp/wp-admin/admin.php?page=add_spinner");
}