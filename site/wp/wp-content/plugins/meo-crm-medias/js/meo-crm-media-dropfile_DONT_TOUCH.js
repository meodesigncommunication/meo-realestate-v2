(function($){
    
    var $form = $('.box');    
    var $input = $form.find('input[type="file"]');    
    var isAdvancedUpload = function() {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }();    
    var dataArray = [];    
    var urlAjax = '';    
    var droppedFiles;    
    var o = {
        message  : 'Déposer vos fichiers ici',
        url      : '../../../../wp-admin/admin-ajax.php',
        max_size : 100000000
    };
    
    
    // Plugin pour drag and drop des médias et les uploaders
    $.fn.dropfile = function(oo)
    {
        if(isAdvancedUpload){
            $form.addClass('has-advanced-upload');
            if(oo) $.extend(o,oo);
            if(o.url != '../../../../wp-admin/admin-ajax.php'){ 
                urlAjax = o.url;
            }
            this.each(function(){
                $('<span>').append(o.message).appendTo(this);
                $(this).bind({
                    dragenter : function(e){
                        e.preventDefault();
                    },
                    dragover : function(e){
                        e.preventDefault();
                        $(this).addClass('hover');
                    },
                    dragleave : function(e)
                    {
                        e.preventDefault();
                        $(this).removeClass('hover');
                    }
                });
                this.addEventListener('drop', function(e){                

                    e.preventDefault();  
                    $(this).removeClass('hover');

                    droppedFiles = e.dataTransfer.files; 

                    $form.find('input[type="file"]').prop('files', droppedFiles);

                    $('#list-upload').html('');

                    $.each(droppedFiles, function(index, file){

                        if(file.type == 'image/jpeg' || file.type == 'image/png' || file.type == 'image/gif' || file.type == 'image/bmp' || file.type == 'image/tiff'){
                            // Média est une image
                            // Start a new instance of FileReader
                            var fileReader = new FileReader();
                            // When the filereader loads initiate a function
                            fileReader.onload = (function(file) {
                                    return function(e) {                                             
                                            // Check file size
                                            if(file.size > o.max_size)
                                            {
                                                alert('The file '+file.name+' is to big');
                                            }else{                                        
                                                // Move each image 40 more pixels across
                                                var file_content = this.result;                                        
                                                // Push the data URI into an array
                                                dataArray.push({
                                                    lastModified : file.lastModified, 
                                                    lastModifiedDate : file.lastModifiedDate, 
                                                    name : file.name, 
                                                    size : file.size, 
                                                    type : file.type,
                                                    value : file_content
                                                });
                                                // Check file type
                                                switch(file.type) {
                                                    case 'image/jpeg':
                                                        displayPicture(file_content,file);
                                                        break;
                                                    case 'image/png':
                                                        displayPicture(file_content,file);
                                                        break;
                                                    case 'image/gif':
                                                        displayPicture(file_content,file);
                                                        break;
                                                    case 'image/bmp':
                                                        displayPicture(file_content,file);
                                                        break;
                                                    case 'image/tiff':
                                                        displayPicture(file_content,file);
                                                        break;
                                                    default:
                                                        console.log('type undefined');
                                                        break;
                                                } 
                                            }
                                    }; 
                            })(droppedFiles[index]);
                            // For data URI purposes
                            fileReader.readAsDataURL(file);
                        }else{
                            // N'est pas une image
                            if(file.size > o.max_size)
                            {
                                alert('The file '+file.name+' is to big');
                            }else{ 
                                switch(file.type) {
                                    case 'video/mp4':
                                        displayMovie(file);
                                        break;
                                    case 'video/webm':
                                        displayMovie(file);
                                        break;
                                    case 'video/ogg':
                                        displayMovie(file);
                                        break;
                                    case 'audio/mp3':
                                        displaySound(file);
                                        break;
                                    case 'audio/wav':
                                        displaySound(file);
                                        break;
                                    case 'application/pdf':
                                        displayPDF(file);
                                        break;
                                    default:
                                        console.log('type undefined');
                                        break;
                                } 
                            }
                        }                    
                    });

                }, false);
            });
        }
    }
    
    // START fonction d'affichage des médias
    function displayPicture(file_content,file)
    {
        var html_base = $('#list-upload').html(); 
        var html  = '<div class="preview-image-upload" style="background:url('+file_content+') no-repeat;">';  
        html += '&nbsp;';
        html += '<div class="label-file-upload">'+file.name+'</div>';
        html += '</div>';
        html += html_base;
        $('#list-upload').html(html);
    }
    
    function displayPDF(file)
    {        
        var html_base = $('#list-upload').html(); 
        var html  = '<div class="preview-image-upload" style="background:#dedede no-repeat;">';  
        html += '<i class="fa fa-file-pdf-o"></i>';
        html += '<div class="label-file-upload">'+file.name+'</div>';
        html += '</div>';
        html += html_base;
        $('#list-upload').html(html);
    }
    
    function displayMovie(file)
    {        
        var html_base = $('#list-upload').html(); 
        var html  = '<div class="preview-image-upload" style="background:#dedede no-repeat;">';  
        html += '<i class="fa fa-file-video-o"></i>';
        html += '<div class="label-file-upload">'+file.name+'</div>';
        html += '</div>';
        html += html_base;
        $('#list-upload').html(html);
    }
    
    function displaySound(file)
    {        
        var html_base = $('#list-upload').html(); 
        var html  = '<div class="preview-image-upload" style="background:#dedede no-repeat;">';  
        html += '<i class="fa fa-file-audio-o"></i>';
        html += '<div class="label-file-upload">'+file.name+'</div>';
        html += '</div>';
        html += html_base;
        $('#list-upload').html(html);
    }
    // END fonction d'affichage des médias

    // Action a effectuer au moment du submit form
    $form.submit(function(e){
        
        if (isAdvancedUpload) {
            
            e.preventDefault();
            
            $form.addClass('upload-file');
            
            var ajaxData = new FormData($form.get(0));
            
            ajaxData.append('action', 'manageFileUploaded');            
            ajaxData.append('project', $('#project').val()); 

            if (droppedFiles) {
                $.each( droppedFiles, function(i,file) {
                    ajaxData.append('files['+i+']',file);
                });
            }
            
            // Envoie des medias pour l'upload
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: ajaxData,
                contentType:false,
                processData:false,
                beforeSend: function(){
                    $('.loader').css('display','block');
                },
                complete: function(){
                    $('.loader').css('display','none');
                    $('#list-upload').html('');
                },
                // If upload success
                success: function(data) {
                    var reponse = jQuery.parseJSON(data);
                    if(reponse.success)
                    {
                        alert(reponse.message);
                    }else{
                        alert(reponse.message);
                    }
                },
                // If upload error
                error: function() {
                    alert('Une erreur c\'est produite lors de l\'upload des médias');
                }
            });
        }
    });
})(jQuery);