<?php 

class GalleryModel
{
    private static $table_posts = 'wp_posts';
    private static $table_postmeta = 'wp_postmeta';
    private static $table = 'wp_meo_crm_galleries';
    private static $table_gallery_attachement = 'wp_meo_crm_gallery_attachement';
    
    /*
     * Make a insert on projects table
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function selectAllGalleries()
    {
        global $wpdb;  
        $query  = 'SELECT g.id, g.name AS gallery_name, g.slug, g.private, g.status, u.display_name, p.name AS project_name ';
        $query .= 'FROM '.self::$table.' AS g ';
        $query .= 'LEFT JOIN wp_users AS u ON g.author_id=u.ID ';
        $query .= 'LEFT JOIN wp_meo_crm_projects AS p ON g.project_id=p.id ';        
        $results = $wpdb->get_results($query,ARRAY_A);         
        return $results;
    }
    
    /*
     * Make a insert on projects table
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function selectGalleryById($id)
    {
        global $wpdb;  
        $query  = 'SELECT * FROM '.self::$table.' ';
        $query .= 'WHERE id='.$id;
        
        $results = $wpdb->get_results($query,OBJECT); 
        
        return $results;
    }
    
    /*
     * Make a insert on projects table
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function selectGalleryAttachmentByGalleryId($id)
    {
        global $wpdb;  
        $query  = 'SELECT * FROM '.self::$table_gallery_attachement.' ';
        $query .= 'WHERE gallery_id='.$id.' ';
        $query .= 'ORDER BY sort';
        
        $results = $wpdb->get_results($query,OBJECT); 
        
        return $results;
    }
    
    /*
     * Make a insert on projects table
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function insertGallery($datas)
    {
        global $wpdb;    
        $datas['created_at'] = date('Y-m-d h:i:s');
        if($wpdb->insert(static::$table, $datas))
        {
            $gallery_id = $wpdb->insert_id;    
            return array('valid' => true, 'gallery_id' => $gallery_id);            
        }else{
            return array('valid' => false, 'gallery_id' => 0);
        }
    }
    
    /*
     * Permet de mettre à jour les données
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function updateGallery($datas,$where)
    {
        global $wpdb;
        $check = true;
        
        // Update gallery 
        if($wpdb->update(self::$table, $datas, $where) !== false)
        {
            return true;
        }        
        return false;
    }
    
    /*
     * Permet de mettre à jour les données
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function updateGalleryAttachement($id,$medias)
    {
        global $wpdb;    
        
        $delete = array(
            'gallery_id' => $id
        );
        
        $wpdb->delete(static::$table_gallery_attachement,$delete);
        
        $count = 0;        
        $check = true;
        
        if(is_array($medias) && !empty($medias))
        {
            foreach($medias as $media)
            {
                $insert = array(
                    'gallery_id' => $id,
                    'attachment_id' => $media,
                    'type_attachment' => 'post',
                    'sort' => $count,
                    'updated_at' => date('Y-m-d h:i:s'),
                    'created_at' => date('Y-m-d h:i:s')
                );
                
                if($wpdb->insert(static::$table_gallery_attachement,$insert) === false)
                {
                    $check = false;
                }
                
                $count++;
            }
        }
        
        return $check;
        
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function deleteGallery($id)
    {
        global $wpdb;
        $check = true;
        
        $where = array('gallery_id' => $id);
        if($wpdb->delete(static::$table_gallery_attachement, $where) !== false)
        {
            $where = array('id' => $id);
            if($wpdb->delete(static::$table, $where) !== false)
            {
                return true;
            }
        } 
        return false;
    }
    
}