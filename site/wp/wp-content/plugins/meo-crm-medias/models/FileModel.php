<?php 

class FileModel
{
    private static $table = 'wp_posts';
    private static $table_linked = 'wp_postmeta';
    private static $table_spinner_attachment = 'wp_meo_crm_spinner_attachement';
    private static $table_gallery_attachement = 'wp_meo_crm_gallery_attachement';
    
    public static function selectAllMimeType()
    {
        global $wpdb;
        
        $query  = 'SELECT post_mime_type '; 
        $query .= 'FROM '.static::$table.' ';
        $query .= 'WHERE post_type LIKE "%attachment%" ';
        $query .= 'GROUP BY post_mime_type ';
        //$query .= 'GROUP BY "post_mime_type"';
        
        $results = $wpdb->get_results($query); 
        
        return $results;
    }
    
    public static function selectAllMedias($current_user,$project_list = array())
    {
        global $wpdb;
        $medias = array();
        
        $query  = 'SELECT * '; 
        $query .= 'FROM '.static::$table.' AS p ';
        $query .= 'LEFT JOIN '.static::$table_linked.' AS pm ON pm.post_id = p.id ';
        $query .= 'WHERE p.post_type LIKE "%attachment%" '; 
        $query .= 'AND p.post_excerpt != "spinner" '; 
        
        if($current_user->roles[0] <> 'administrator')
        {
            $query .= 'AND ( pm.meta_key == "project_id" ';
            foreach($project_list as $project)
            {
                $query .= 'AND pm.meta_value == '.$project->project_id.' ';
            }
            $query .= ') ';
        }
            
        $results = $wpdb->get_results($query); 
        
        foreach($results as $result)
        {
            $post_id = $result->ID;           
                        
            $medias[$post_id]['post_author'] = $result->post_author;
            $medias[$post_id]['post_date'] = $result->post_date;
            $medias[$post_id]['post_date_gmt'] = $result->post_date_gmt;
            $medias[$post_id]['post_title'] = $result->post_title;
            $medias[$post_id]['post_content'] = $result->post_content;
            $medias[$post_id]['post_name'] = $result->post_name;
            $medias[$post_id]['post_modified'] = $result->post_modified;
            $medias[$post_id]['post_modified_gmt'] = $result->post_modified_gmt;
            $medias[$post_id]['guid'] = $result->guid;
            $medias[$post_id]['post_mime_type'] = $result->post_mime_type;            
            
            // Select meta of post
            $query  = 'SELECT * '; 
            $query .= 'FROM '.static::$table_linked.' ';
            $query .= 'WHERE post_id = '.$post_id;  
            $metas = $wpdb->get_results($query); 
            
            $medias[$post_id]['post_metas'] = $metas; 
            
            if(is_array($metas) && !empty($metas))
            {
                foreach($metas as $key => $meta)
                {
                    $medias[$post_id]['post_mime_type'] = $result->post_mime_type; 
                }
            }           
        }
        
        return $medias;
    }
    
    public static function selectMediaById($id)
    {
        global $wpdb;
        $medias = array();
        
        $query  = 'SELECT * '; 
        $query .= 'FROM '.static::$table.' AS p ';
        $query .= 'WHERE post_type LIKE "%attachment%" ';
        $query .= 'AND p.ID='.$id.' ';
        $results = $wpdb->get_results($query); 
        
        foreach($results as $result)
        {
            $post_id = $result->ID;           
            $medias['post_id'] = $post_id;
            $medias['post_author'] = $result->post_author;
            $medias['post_date'] = $result->post_date;
            $medias['post_date_gmt'] = $result->post_date_gmt;
            $medias['post_title'] = $result->post_title;
            $medias['post_content'] = $result->post_content;
            $medias['post_name'] = $result->post_name;
            $medias['post_modified'] = $result->post_modified;
            $medias['post_modified_gmt'] = $result->post_modified_gmt;
            $medias['guid'] = $result->guid;
            $medias['post_mime_type'] = $result->post_mime_type;            
            
            // Select meta of post
            $query  = 'SELECT * '; 
            $query .= 'FROM '.static::$table_linked.' ';
            $query .= 'WHERE post_id = '.$post_id;  
            $metas = $wpdb->get_results($query); 
            
            $medias['post_metas'] = $metas; 
            
            if(is_array($metas) && !empty($metas))
            {
                foreach($metas as $key => $meta)
                {
                    $medias['post_mime_type'] = $result->post_mime_type; 
                }
            }           
        }
        
        return $medias;
    }
    
    public static function searchMedias($search, $author, $project, $mime_type, $date_start=null, $date_end=null)
    {
        global $wpdb;
                
        $medias = array();
        
        $query  = 'SELECT * '; 
        $query .= 'FROM '.static::$table.' AS p ';
        $query .= 'LEFT JOIN '.static::$table_linked.' AS pm ON p.ID = pm.post_id ';
        $query .= 'WHERE post_type LIKE "%attachment%" '; 
        $query .= 'AND post_excerpt != "spinner" '; 
        
        if(isset($search) && !empty($search))
        {
            $query .= 'AND (p.post_name LIKE "%'.$search.'%" OR p.post_title LIKE "%'.$search.'%" OR p.post_title LIKE "%'.$search.'%") ';
        }
        
        if(isset($author) && !empty($author))
        {
            $query .= 'AND p.post_author = '.$author.' ';
        }
        
        if(isset($mime_type) && !empty($mime_type))
        {
            $query .= 'AND p.post_mime_type = "'.$mime_type.'" ';    
        }
        
        if(isset($project) && !empty($project))
        {
            $query .= 'AND pm.meta_key = "project_id" AND pm.meta_value = '.$project.' ';
        }
        
        if((isset($date_start) && !empty($date_start)) && (isset($date_end) && !empty($date_end)) && ($date_start <= $date_end)){
            $query .= 'AND p.post_date BETWEEN "'.$date_start.'" AND "'.$date_end.'" ';
        }else if((isset($date_start) && !empty($date_start)) && (!isset($date_end) || empty($date_end))){
            $query .= 'AND p.post_date >= "'.$date_start.'" ';
        }else if((isset($date_end) && !empty($date_end)) && (!isset($date_start) || empty($date_start))){
            $query .= 'AND p.post_date <= "'.$date_end.'" ';            
        }
        
        $results = $wpdb->get_results($query); 
        
        foreach($results as $result)
        {
            $post_id = $result->ID;          
                        
            $medias[$post_id]['post_author'] = $result->post_author;
            $medias[$post_id]['post_title'] = $result->post_title;
            $medias[$post_id]['post_content'] = $result->post_content;
            $medias[$post_id]['post_name'] = $result->post_name;
            $medias[$post_id]['guid'] = $result->guid;
            $medias[$post_id]['post_mime_type'] = $result->post_mime_type;    
            
            // Select meta of post
            $query  = 'SELECT * '; 
            $query .= 'FROM '.static::$table_linked.' ';
            $query .= 'WHERE post_id = '.$post_id;  
            $metas = $wpdb->get_results($query); 
            
            $medias[$post_id]['post_metas'] = $metas; 
            
            if(is_array($metas) && !empty($metas))
            {
                foreach($metas as $key => $meta)
                {
                    $medias[$post_id]['post_mime_type'] = $result->post_mime_type; 
                }
            }    
        }
        
        return $medias;
    }
    
    public static function insertFile($filename,$full_url,$current_user,$mime_type,$project_id)
    {
        global $wpdb;
        $timestamp = date('Y-m-d H:i:s');
        $upload_dir = wp_upload_dir();
        
        $dataFile = array();
        $dataFile['post_author'] = $current_user->ID;
        $dataFile['post_date'] = $timestamp;
        $dataFile['post_date_gmt'] = $timestamp;
        $dataFile['post_title'] = $filename;
        $dataFile['comment_status'] = 'close';
        $dataFile['ping_status'] = 'close';
        $dataFile['post_name'] = $filename;
        $dataFile['post_modified'] = $timestamp;
        $dataFile['post_modified_gmt'] = $timestamp;
        $dataFile['guid'] = $full_url;
        $dataFile['post_type'] = 'attachment';
        $dataFile['post_mime_type'] = $mime_type;
        
        
        if($wpdb->insert(static::$table,$dataFile))
        {
            $post_id = $wpdb->insert_id;  
            $dataFileMeta = array(
                'post_id' => $post_id, 
                'meta_key' => 'project_id', 
                'meta_value' => $project_id
                /*array('post_id' => $post_id, 'meta_key' => 'path_file', 'meta_value' => $upload_dir['basedir'].'/projects/'.$project_id.'/'.$filename)*/
            );
            if($wpdb->insert(static::$table_linked,$dataFileMeta))
            {
                $dataFileMeta = array(
                    'post_id' => $post_id, 
                    'meta_key' => 'path_file', 
                    'meta_value' => $upload_dir['basedir'].'/projects/'.$project_id.'/'.$filename
                );
                if($wpdb->insert(static::$table_linked,$dataFileMeta))
                {
                    return array('valid' => true, 'post_id' => $post_id); 
                }
            }          
        }
        
        return array('valid' => false, 'post_id' => 0);
        
    }
    
     public static function insertFileSpinner($filename,$full_url,$current_user,$mime_type,$project_id,$spinner_post=false,$url_dir='')
    {
        global $wpdb;
        $timestamp = date('Y-m-d H:i:s');
        $upload_dir = wp_upload_dir();
        
        $dataFile = array();
        $dataFile['post_author'] = $current_user->ID;
        $dataFile['post_date'] = $timestamp;
        $dataFile['post_date_gmt'] = $timestamp;
        $dataFile['post_title'] = $filename;
        $dataFile['comment_status'] = 'close';
        $dataFile['ping_status'] = 'close';
        $dataFile['post_name'] = $filename;
        $dataFile['post_modified'] = $timestamp;
        $dataFile['post_modified_gmt'] = $timestamp;
        $dataFile['guid'] = $full_url;
        $dataFile['post_type'] = 'attachment';
        $dataFile['post_mime_type'] = $mime_type;
        $dataFile['post_excerpt'] = ($spinner_post) ? 'spinner' : '';
        
        
        if($wpdb->insert(static::$table,$dataFile))
        {
            $post_id = $wpdb->insert_id;  
            $dataFileMeta = array(
                'post_id' => $post_id, 
                'meta_key' => 'project_id', 
                'meta_value' => $project_id
                /*array('post_id' => $post_id, 'meta_key' => 'path_file', 'meta_value' => $upload_dir['basedir'].'/projects/'.$project_id.'/'.$filename)*/
            );
            if($wpdb->insert(static::$table_linked,$dataFileMeta))
            {
                $dataFileMeta = array(
                    'post_id' => $post_id, 
                    'meta_key' => 'path_file', 
                    'meta_value' => $url_dir
                );
                if($wpdb->insert(static::$table_linked,$dataFileMeta))
                {
                    return array('valid' => true, 'post_id' => $post_id); 
                }
            }          
        }
        
        return array('valid' => false, 'post_id' => 0);
        
    }
    
    /*
     * Permet de mettre à jour les données
     * 
     * @param $datas(Array), $where(Array)
     * @return $id (Integer)
     */
    public static function updateMedia($datas,$where)
    {
        global $wpdb;
        $check = true;
        
        // Update gallery        
        $check = ($wpdb->update(self::$table, $datas, $where))? true : false;
       
        return $check;
        
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function deleteMedia($id)
    {
        global $wpdb;
        
        // Init Value     
        $where = array('ID' => $id);
        $whereLinked = array('post_id' => $id);
        $whereAttachment = array('attachment_id' => $id);
        
        // Delete on attachment table
        $wpdb->delete(static::$table_spinner_attachment, $whereAttachment);
        $wpdb->delete(static::$table_gallery_attachement, $whereAttachment);
        
        // Check if a removed post is ok
        if($wpdb->delete(static::$table, $where))
        {
            if($wpdb->delete(static::$table_linked, $whereLinked))
            {
                return true; // is ok
            }
        }        
        return false; //isn't ok                     
    }
    
}