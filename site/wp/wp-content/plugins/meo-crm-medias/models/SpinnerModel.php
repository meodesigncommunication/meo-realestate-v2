<?php 

class SpinnerModel
{
    private static $table = 'wp_meo_crm_spinner';
    private static $table_posts = 'wp_posts';
    private static $table_postmeta = 'wp_postmeta';
    private static $table_spinner_attachment = 'wp_meo_crm_spinner_attachement';
    private static $table_gallery_attachement = 'wp_meo_crm_gallery_attachement';
    
    /*
     * Select all spinner in spinner table
     * 
     * @param 
     * @return $results (Array)
     */
    public static function selectSpinnersByProjectId($id)
    {
        global $wpdb;  
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table.' ';
        $query .= 'WHERE project_id = '.$id.' ';
        $query .= 'AND ( export = 0 OR export IS NULL)';
        
        $results = $wpdb->get_results($query);    
        
        return $results;
    }
    
    /*
     * Select all spinner in spinner table
     * 
     * @param 
     * @return $results (Array)
     */
    public static function selectAllSpinners()
    {
        global $wpdb;  
        $query  = 'SELECT g.id, g.name AS spinner_name, u.display_name, p.name AS project_name ';
        $query .= 'FROM '.self::$table.' AS g ';
        $query .= 'LEFT JOIN wp_users AS u ON g.author_id=u.ID ';
        $query .= 'LEFT JOIN wp_meo_crm_projects AS p ON g.project_id=p.id ';        
        $results = $wpdb->get_results($query,ARRAY_A);         
        return $results;
    }
    
    /*
     * Make a select by id on spinner table
     * 
     * @param $id(Int)
     * @return $result (Array)
     */
    public static function selectSpinnerById($id)
    {
        global $wpdb;  
        $query  = 'SELECT * FROM '.self::$table.' ';
        $query .= 'WHERE id='.$id;
        
        $results = $wpdb->get_results($query,OBJECT); 
        
        return $results;
    }
    
    /*
     * Make a select on spinner attachment by spinner id
     * 
     * @param $id(Int)
     * @return $result (Array)
     */
    public static function selectSpinnerMedia($id)
    {
        global $wpdb;  
        $count = 0;
        $spinner = array();
        $spinner_name = '';
                
        $query  = 'SELECT * FROM '.self::$table.' AS s ';
        $query .= 'INNER JOIN '.self::$table_spinner_attachment.' AS spa ON spa.spinner_id = s.id ';
        $query .= 'INNER JOIN '.self::$table_posts.' AS p ON p.ID = spa.attachment_id ';
        $query .= 'INNER JOIN '.self::$table_postmeta.' AS pm ON pm.post_id = p.ID ';
        $query .= 'WHERE s.id='.$id.' ';        
        $results = $wpdb->get_results($query,OBJECT);        
        return $results;
    }
    
    /*
     * Make a select on spinner attachment by spinner id
     * 
     * @param $id(Int)
     * @return $result (Array)
     */
    public static function selectExportSpinnerMedia($id)
    {
        global $wpdb;  
        $count = 0;
        $spinner = array();
        $spinner_name = '';
                
        $query  = 'SELECT * FROM '.self::$table.' AS s ';
        $query .= 'INNER JOIN '.self::$table_spinner_attachment.' AS spa ON spa.spinner_id = s.id ';
        $query .= 'INNER JOIN '.self::$table_posts.' AS p ON p.ID = spa.attachment_id ';
        $query .= 'INNER JOIN '.self::$table_postmeta.' AS pm ON pm.post_id = p.ID ';
        $query .= 'WHERE s.id='.$id.' ';        
        $results = $wpdb->get_results($query,OBJECT);    
        
        foreach($results as $result)
        {
            $post_id = 0;
            $count = 0;
            
            if($spinner_name != $result->name)
            {
                $spinner['spinner']['title'] = $result->name;
                $spinner['spinner']['description'] = $result->description;
                $spinner_name = $result->name;
            }
            
            // Post Meta data
            if($result->meta_key == 'path_file')
            {
                $spinner['posts'][$result->ID]['postmeta'][$count]['post_id'] = $result->ID;
                $spinner['posts'][$result->ID]['postmeta'][$count]['meta_key'] = $result->meta_key; 
                $spinner['posts'][$result->ID]['postmeta'][$count]['meta_value'] = $result->meta_value;
            }            
            $count++;
            
            if($result->ID != $post_id){
                // Create array post
                $spinner['posts'][$result->ID]['post_author'] = $result->post_author;
                $spinner['posts'][$result->ID]['post_date'] = $result->post_date;
                $spinner['posts'][$result->ID]['post_date_gmt'] = $result->post_date_gmt;
                $spinner['posts'][$result->ID]['post_title'] = $result->post_title;
                $spinner['posts'][$result->ID]['post_excerpt'] = $result->post_excerpt;
                $spinner['posts'][$result->ID]['post_status'] = $result->post_status;
                $spinner['posts'][$result->ID]['comment_status'] = $result->comment_status;
                $spinner['posts'][$result->ID]['ping_status'] = $result->ping_status;
                $spinner['posts'][$result->ID]['post_name'] = $result->post_name;
                $spinner['posts'][$result->ID]['post_modified'] = $result->post_modified;
                $spinner['posts'][$result->ID]['post_modified_gmt'] = $result->post_modified_gmt;
                $spinner['posts'][$result->ID]['post_parent'] = $result->post_parent;
                $spinner['posts'][$result->ID]['guid'] = $result->guid;
                $spinner['posts'][$result->ID]['menu_order'] =$result->menu_order;
                $spinner['posts'][$result->ID]['post_type'] = $result->post_type;
                $spinner['posts'][$result->ID]['post_mime_type'] = $result->post_mime_type;
                $spinner['posts'][$result->ID]['comment_count'] = $result->comment_count;
                
                $post_id = $result->ID;
            }
            
        }
        
        return $spinner;
    }
    
    /*
     * Make a select on spinner attachment by spinner id
     * 
     * @param $id(Int)
     * @return $result (Array)
     */
    public static function selectFullSpinnerById($id)
    {
        global $wpdb;  
        $query  = 'SELECT * FROM '.self::$table_spinner_attachment.' ';
        $query .= 'WHERE spinner_id='.$id.' ';
        $query .= 'ORDER BY sort ';
        
        $results = $wpdb->get_results($query,OBJECT); 
        
        return $results;
    }
    
    /*
     * Make a insert on spinner table
     * 
     * @param $datas(Array)
     * @return $array([valide true / false][spinner_id]) (Array)
     */
    public static function insertSpinner($datas)
    {
        global $wpdb;    
        
        $datas['created_at'] = date('Y-m-d h:i:s');
        $datas['updated_at'] = date('Y-m-d h:i:s');
        
        if($wpdb->insert(static::$table, $datas))
        {
            $spinner_id = $wpdb->insert_id;    
            return array('valid' => true, 'spinner_id' => $spinner_id);            
        }else{
            return array('valid' => false, 'spinner_id' => 0);
        }
    }
    
    /*
     * Make a insert on spinner table
     * 
     * @param $datas(Array)
     * @return $array([valide true / false][spinner_id]) (Array)
     */
    public static function insertSpinnerAttachment($datas)
    {
        global $wpdb;    
        
        $datas['created_at'] = date('Y-m-d h:i:s');
        $datas['updated_at'] = date('Y-m-d h:i:s');
        
        if($wpdb->insert(static::$table_spinner_attachment, $datas))
        {
            $spinner_id = $wpdb->insert_id;    
            return array('valid' => true, 'spinner_id' => $spinner_id);            
        }else{
            return array('valid' => false, 'spinner_id' => 0);
        }
    }
    
    /*
     * Permet de mettre à jour les données
     * 
     * @param $datas(Array)
     * @return $id (Integer)
     */
    public static function updateSpinner($datas,$where)
    {
        global $wpdb;
        $check = true;
        
        // Update spinner 
        if($wpdb->update(self::$table, $datas, $where) !== false)
        {
            return true;
        }        
        return false;
    }
    
    /*
     * Permet de mettre à jour les données
     * 
     * @param $medias(Array), $spinner_id(Integer)
     * @return $check(Boolean)
     */
    public static function updateSpinnerMedia($medias,$spinner_id)
    {
        global $wpdb;    
        $check = true;
        
        $where = array(
            'spinner_id' => $spinner_id
        );
        
        if($wpdb->delete(static::$table_spinner_attachment, $where))
        {
            foreach($medias as $key => $media)
            {
                $datas = array();
                $datas['spinner_id'] = $spinner_id;
                $datas['attachment_id'] = $media['id'];
                $datas['sort'] = $key;
                $datas['created_at'] = date('Y-m-d h:i:s');
                $datas['updated_at'] = date('Y-m-d h:i:s');

                if(!$wpdb->insert(static::$table_spinner_attachment, $datas))
                {
                    $check = false;           
                }

            } 
        }
        
        return $check;
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function selectAllAttachmentSpinner($id)
    {
        global $wpdb;

        $query  = 'SELECT sp.id AS spinner_id, spa.id AS spinner_attachment_id, p.ID AS post_id, pm.meta_id AS meta_id, p.guid AS url, pm.meta_key AS meta_key, pm.meta_value AS meta_value ';
        $query .= 'FROM `'.static::$table.'` sp ';
        $query .= 'INNER JOIN '.static::$table_spinner_attachment.' AS spa ON spa.spinner_id = sp.id ';
        $query .= 'INNER JOIN '.static::$table_posts.' AS p ON p.ID = spa.attachment_id ';
        $query .= 'INNER JOIN '.static::$table_postmeta.' AS pm ON pm.post_id = p.ID ';
        $query .= 'WHERE sp.id = '.$id;
        
        $results = $wpdb->get_results($query,OBJECT); 
        
        return $results;        
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function deleteSpinnerMeta($id,$attachment)
    {
        global $wpdb;
        
        $check = true;
        
        foreach($attachment[0]['metas'] as $meta)
        {
            $where = array('meta_id' => $meta);
            if(!$wpdb->delete(static::$table_postmeta, $where))
            {
                $check = false;
            }
        }
        
        foreach($attachment[0]['post_id'] as $post)
        {
            $where = array('ID' => $post);
            if(!$wpdb->delete(static::$table_posts, $where))
            {
                $check = false;
            }
        }
        
        foreach($attachment[0]['attachment_id'] as $attachment_id)
        {
            $where = array('ID' => $attachment_id);
            if(!$wpdb->delete(static::$table_spinner_attachment, $where))
            {
                $check = false;
            }
        }
        
        $where = array('id' => $id);
        if(!$wpdb->delete(static::$table, $where))
        {
            $check = false;
        }
        
        return $check;
    }
    
    /*
     * Permet de supprimer une entrée
     * 
     * @param $id(integer)
     * @return boolean
     */
    public static function deleteOnlySpinner($id)
    {
        global $wpdb;
        
        $check = true;
        
        $where = array('id' => $id);
        if(!$wpdb->delete(static::$table, $where))
        {
            $check = false;
        }
        
        return $check;
    }
    
}