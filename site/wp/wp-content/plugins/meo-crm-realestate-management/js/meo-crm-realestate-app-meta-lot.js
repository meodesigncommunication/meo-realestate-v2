/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-meta-lot').click(function(){
        resetAllMetaLot();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-meta-lot-list').css('display','block');
        loadTableMetaLot();
    });
    
    $('#back-meta-lot-dashboard').click(function(){
        resetAllMetaLot();       
        $('#meo-crm-realestate-meta-lot-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-meta-lot').click(function(){
        resetAllMetaLot();  
        $('#meo-crm-realestate-meta-lot-list').css('display','none');
        $('#meo-crm-realestate-meta-lot-form').css('display','block');
    });
    
    $('#back-meta-lot').click(function(){
        resetAllMetaLot(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-meta-lot-form').css('display','none');
        $('#meo-crm-realestate-meta-lot-list').css('display','block');
        loadTableMetaLot();
    });
    
    
    // Execute the submit form
    $('#form_meta_lot').on('submit',(function(e) {
        
        e.preventDefault();
        
        if($('#meta_lot_filter').prop('checked') == false)
        {
            var filter = 0;
        }else{
            var filter = 1;
        }
        
        if($('#meta_lot_type').val() != -1)
        {
            $.post(
                ajaxurl,
                {
                    'action': 'saveMetaLot',
                    'id': $('#meta_lot_id').val(),
                    'meta_key': $('#meta_lot_key').val(),
                    'meta_slug': $('#meta_lot_slug').val(),
                    'meta_type': $('#meta_lot_type').val(),
                    'meta_order': $('#meta_lot_order').val(),
                    'meta_filter': filter,
                    'project_id': $('#realestate_project_id').val()
                },
                function(response){
                    var obj = $.parseJSON(response);
                    $('#zone-message').html(obj.message);
                    $('#meta_lot_id').val(obj.id);
                }
            );
        }else{
            alert('Veuillez renseigner le type !');
        }
        
    }));
    
});

/* START FUNCTION */

function editMetaLot(id)
{
    resetAllMetaLot();  
    $('#meo-crm-waiting-bloc').css('display','block');
    $('#meo-crm-realestate-meta-lot-list').css('display','none');
    $('#meo-crm-realestate-meta-lot-form').css('display','block');
    $('#meta_lot_id').val(id);
    $.post(
        ajaxurl,
        {
            'action': 'getMetaLotById',
            'id': $('#meta_lot_id').val(),
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            var obj = $.parseJSON(response);
            $('#meta_lot_key').val(obj.meta_key);
            $('#meta_lot_slug').val(obj.meta_slug);
            $('#meta_lot_order').val(obj.meta_order);
            $('#meta_lot_type').val(obj.meta_type);
            if(obj.meta_filter != 0)
            {
                $('#meta_lot_filter').prop('checked',true);
            }
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function deleteMetaLot(id)
{
    if(confirm('Voulez-vous vraiment supprimer cette element ?')){
        $('#meo-crm-waiting-bloc').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'deleteMetaLot',
                'id': id,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                var obj = $.parseJSON(response);
                $('#zone-message').html(obj.message);
                loadTableMetaLot();
                $('#meo-crm-waiting-bloc').css('display','none');            
            }
        );
    }
}

function loadTableMetaLot()
{
    $.post(
        ajaxurl,
        {
            'action': 'getMetaLotList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-meta-lot').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllMetaLot()
{
    $('#zone-message').html('');
    $('#table-container-meta-lot').html('');    
    $('#meta_lot_id').val('');
    $('#meta_lot_key').val('');
    $('#meta_lot_slug').val('');
    $('#meta_lot_type').val('');
    $('#meta_lot_order').val('');
    $('#meta_lot_filter').prop('checked', false);
}
/* END FUNCTION */