/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    /*
     *  Check select list for default
     */
    if($('#realestate-list-project').val() <= -1)
    {
        $('#disabled_dashboard').css('display','block');
    }else{
        $('#disabled_dashboard').css('display','none');
    }
    
    /*
     * Permet de gerer le projet a modifier
     */
    $('#realestate-list-project').change(function(){  
        resetAllDeveloppement();
        //Init var
        var project = $(this).val();        
        // Attribute a new value for project_id input
        $('#realestate_project_id').val(project);        
        // If you edit a realestate project and you change a project closes all but the dashboard
        $('.meo-crm-realestate-window').each(function(){
            $(this).css('display','none');
        });        
        // Show dashboard bloc
        $('#meo-crm-realestate-dashboard').css('display','block');                
        // Check project_id value for enable or desable dashboard
        if(project <= -1)
        {
            $('#disabled_dashboard').css('display','block');
        }else{
            $('#disabled_dashboard').css('display','none');
        }
        $.ajax({
            url: $('#url_ajax').val(),
            type: "POST",
            data: {
                'action': 'getProjectUrl',
                'project_id': $(this).val()
            },
            success: function(response)
            {
                // TODO :: Integrer dans la db un URL a upload
                $('#url_upload').val(response+'/wp/wp-content/uploads');
            }
        });
    });
    
    
    /* ALL CLICK FUNCTION FOR DEVELOPPEMENT START */
    // Click on the dashicon for access to manage developpement
    $('#dashicon-developpement').click(function(){
        resetAllDeveloppement();
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-developpement-list').css('display','block');
        loadTableDeveloppement();
    });
    $('#back-developpement-dashboard').click(function(){
        resetAllDeveloppement();
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-developpement-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
        $('#meo-crm-waiting-bloc').css('display','none');
    });
    // Click on add new button on list developpement
    $('#new-developpement').click(function(){
        resetAllDeveloppement();
        $('#meo-crm-realestate-developpement-list').css('display','none');
        $('#meo-crm-realestate-developpement-form').css('display','block');
    });
    // Click on back button on form developpement
    $('#back-developpement').click(function(){
        resetAllDeveloppement();    
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-developpement-list').css('display','block');
        $('#meo-crm-realestate-developpement-form').css('display','none');
        loadTableDeveloppement();
        $('#meo-crm-waiting-bloc').css('display','none');
    });
    // Execute the submit form
    $("#form_developpement").on('submit',(function(e) {
        e.preventDefault();
        
        var ajaxData = new FormData(this);        
        ajaxData.append('action', 'saveDeveloppement');
        ajaxData.append('project_id', $('#realestate_project_id').val());
        
        if($('#developpement_id').val() != '')
        {
            ajaxData.append('developpement_id', $('#developpement_id').val());
        }
        
        if($('#plan_developpement_3d_old').val() != '')
        {
            ajaxData.append('plan_developpement_3d_old', $('#plan_developpement_3d_old').val());
        }
        
        if($('#plan_developpement_2d_old').val() != '')
        {
            ajaxData.append('plan_developpement_2d_old', $('#plan_developpement_2d_old').val());
        }
        
        $.ajax({
            url: $('#url_ajax').val(),
            type: "POST",
            data: ajaxData,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response)
            {
                var obj = $.parseJSON(response);
                $('#zone-message').html(obj.message);
                $('#developpement_id').val(obj.id);
                
                $('#balise_developpement_image_3d').attr('src', $('#url_upload').val()+'/'+obj.plan_developpement_3d);
                $('#plan_developpement_3d_old').val(obj.plan_developpement_3d);
                
                $('#balise_developpement_image_2d').attr('src', $('#url_upload').val()+'/'+obj.plan_developpement_2d);
                $('#plan_developpement_2d_old').val(obj.plan_developpement_2d);
            }
        });
    }));
    /* ALL CLICK FUNCTION FOR DEVELOPPEMENT END */
    
});

/* START FUNCTION */

function editDeveloppement(id)
{
    resetAllDeveloppement();
    $('#meo-crm-realestate-developpement-list').css('display','none');
    $('#meo-crm-realestate-developpement-form').css('display','block');
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'getDeveloppementById',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            var obj = $.parseJSON(response);
            $('#developpement_id').val(obj.id);
            $('#developpement_title').val(obj.title);
            $('#developpement_description').val(obj.description);
            $('#balise_developpement_image_3d').attr('src',$('#url_upload').val()+'/'+obj.plan_developpement_3d);
            $('#balise_developpement_image_2d').attr('src',$('#url_upload').val()+'/'+obj.plan_developpement_2d);
            $('#plan_developpement_3d_old').val(obj.plan_developpement_3d);
            $('#plan_developpement_2d_old').val(obj.plan_developpement_2d);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function deleteDeveloppement(id)
{
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'deleteDeveloppement',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            var obj = $.parseJSON(response);
            loadTableDeveloppement();            
            $('#zone-message').html(obj.message);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function loadTableDeveloppement()
{
    $.post(
        ajaxurl,
        {
            'action': 'getDeveloppementList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-developpement').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllDeveloppement()
{
    $('#table-container-developpement').html('');
    $('#developpement_description').html('');
    $('#developpement_description').val('');
    $('#developpement_description').empty();
    $('#form_developpement input').each(function(){
        $(this).val('');
    });
    $('#balise_developpement_image_3d').attr('src','');
    $('#balise_developpement_image_2d').attr('src','');
    $('#plan_developpement_3d_old').val('');
    $('#plan_developpement_2d_old').val('');
    $('#zone-message').html('');
}
/* END FUNCTION */