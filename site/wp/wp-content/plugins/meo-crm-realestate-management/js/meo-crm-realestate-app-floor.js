/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-floor').click(function(){
        resetAllFloor();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-floor-list').css('display','block');
        loadTableFloor();
    });
    
    $('#back-floor-dashboard').click(function(){
        resetAllFloor();       
        $('#meo-crm-realestate-floor-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-floor').click(function(){
        resetAllFloor();  
        $('#meo-crm-realestate-floor-list').css('display','none');
        $('#meo-crm-realestate-floor-form').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'getListBuilding',
                'id': 0,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                $('#floor_building_id').html(response);
            }
        );
    });
    
    $('#back-floor').click(function(){
        resetAllFloor(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-floor-form').css('display','none');
        $('#meo-crm-realestate-floor-list').css('display','block');
        loadTableFloor();
    });
    
    
    // Execute the submit form
    $("#form_floor").on('submit',(function(e) {
        
        e.preventDefault();        
               
        if($('#floor_building_id').val() >= 1)
        {   
            $('#meo-crm-waiting-bloc').css('display','block');
            var ajaxData = new FormData(this);        
            ajaxData.append('action', 'saveFloor');
            ajaxData.append('project_id', $('#realestate_project_id').val());
            

            $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                data: ajaxData,
                contentType: false,
                cache: false,
                processData:false,
                success: function(response)
                {
                    var obj = $.parseJSON(response);
                    $('#zone-message').html(obj.message);
                    $('#floor_id').val(obj.id);
                    
                    $.post(
                        ajaxurl,
                        {
                            'action': 'getListBuilding',
                            'id': obj.building_id,
                            'project_id': $('#realestate_project_id').val()
                        },
                        function(response){
                            $('#floor_building_id').html(response);
                            $('#meo-crm-waiting-bloc').css('display','none');
                        }
                    );
                    
                }
            });
        }else{
            alert('Veuillez sélectionner un batiment');
        }
    }));
    
});

/* START FUNCTION */

function editFloor(id)
{
    resetAllFloor();
    $('#meo-crm-realestate-floor-list').css('display','none');
    $('#meo-crm-realestate-floor-form').css('display','block');
    $('#meo-crm-waiting-bloc').css('display','block');
    $.post(
        ajaxurl,
        {
            'action': 'getFloorById',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            var obj = $.parseJSON(response);
            $('#floor_id').val(obj.id);
            $('#floor_title').val(obj.title);
            $('#floor_position').val(obj.position);            
            $.post(
                ajaxurl,
                {
                    'action': 'getListBuilding',
                    'id': obj.building_id,
                    'project_id': $('#realestate_project_id').val()
                },
                function(response){
                    $('#floor_building_id').html(response);
                    $('#meo-crm-waiting-bloc').css('display','none');
                }
            );
        }
    );
}

function deleteFloor(id)
{
    if(confirm('Voulez-vous supprimer cette étage ?'))
    {
        $('#meo-crm-waiting-bloc').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'deleteFloor',
                'id': id,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                loadTableFloor();  
                var obj = $.parseJSON(response);           
                $('#zone-message').html(obj.message);
                $('#meo-crm-waiting-bloc').css('display','none');
            }
        );
    }   
}

function loadTableFloor()
{
    $.post(
        ajaxurl,
        {
            'action': 'getFloorList',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            $('#table-container-floor').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllFloor()
{
    $('#zone-message').html('');
    $('#floor_id').val('');
    $('#table-container-floor').html('');
    $('#floor_title').val('');
    $('#floor_position').val('');
}
/* END FUNCTION */