window.$ = jQuery;

$(document).ready(function(){
    
    $('#project-list-lot-manager').change(function(){
        var project_id = $(this).val();
        refreshListLots(project_id);
    });
    
    $(document).on("dblclick","#lot-list-content table td.editable",function() {console.log('sales');
        
        $('td.updated_list').each(function(){
            if($(this).hasClass('editable'))
            {
                $(this).find('p').show();
                $(this).find('.form-input-list').hide();
            }else{
                $(this).addClass('editable');
                $(this).find('p').show();
                $(this).find('.form-input-list').hide();
            }
        });
        $(this).removeClass('editable');
        $(this).find('p').hide();
        $(this).find('.form-input-list').show();
        
    });
    
    $('#list-form-realestate .icon-item').click(function(){
        var form = $(this).find('.icon-form').attr('data-form');
        var project = $('select#projects').val();
        if(project > 0)
        {

            if(form == 'price-list-pdf')
            {
                window.location.href = 'meo-crm-realestate-'+form+'-form/?project='+project;
            }else{
                window.location.href = 'meo-crm-realestate-'+form+'-list/?project='+project;
            }

        }else{
            alert('aucun projet séléctionné !');
        }
    });
    
});

function refreshListLots(project_id)
{
    
    $.ajax({
        url  : $('input[name="ajax_url"]').val(),
        type : 'POST',
        data : {
            'action': 'meoCrmRealeastateListLotFrontendInterface',
            'project_id': project_id
        }, 
        success : function(response, statut){            
            console.log(response);            
            $('#lot-list-content').html(response);
        }
    });
}

function saveLotStatus(element, lot_id)
{
    var project_id = $('#project-list-lot-manager').val();
    var status = $(element).parent().find('select.list_status').val();    
    $.ajax({
        url  : $('input[name="ajax_url"]').val(),
        type : 'POST',
        data : {
            'action': 'meoCrmRealeastateSaveLotStatus',
            'project_id': project_id,
            'status': status,
            'lot_id': lot_id
        }, 
        success : function(response, statut){            
            refreshListLots(project_id);          
        }
    });
}

function saveLotPrice(element, lot_id)
{
    alert('test');
    var project_id = $('#project-list-lot-manager').val();
    var price = $(element).parent().find('input').val();
    $.ajax({
        url  : $('input[name="ajax_url"]').val(),
        type : 'POST',
        data : {
            'action': 'meoCrmRealeastateSaveLotPrice',
            'project_id': project_id,
            'price': price,
            'lot_id': lot_id
        }, 
        success : function(response, statut){            
            refreshListLots(project_id);         
        }
    });
}

function cancelEditableField(element)
{
    $(element).parents('.updated_list').addClass('editable');
    $(element).parents('.updated_list').find('p').show();
    $(element).parents('.updated_list').find('.form-input-list').hide();
}