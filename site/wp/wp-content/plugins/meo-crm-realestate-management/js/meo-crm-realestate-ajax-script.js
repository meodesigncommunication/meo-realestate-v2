/* 
 * Author: MEO | Kyle Mobilia
 */
jQuery(document).ready(function(){
    $('#create_external_table').click(function(){
        $.post(
            ajaxurl,
            {
                'action': 'createdExternalTableForRealestate',
                'access_project_id': $('#access_project_id').val()
            },
            function(response){
                 console.log(response);
             }
        );
    });
});