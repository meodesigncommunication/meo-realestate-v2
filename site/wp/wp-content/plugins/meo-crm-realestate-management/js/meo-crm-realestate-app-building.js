/*
 * ALL FUNCTION CLICK DECALRED HERE
 */

jQuery(document).ready(function(){
    
    // Click on the dashicon for access to manage developpement
    $('#dashicon-building').click(function(){
        resetAllBuilding();        
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-dashboard').css('display','none');
        $('#meo-crm-realestate-building-list').css('display','block');
        loadTableBuilding();
    });
    
    $('#back-building-dashboard').click(function(){
        resetAllBuilding();       
        $('#meo-crm-realestate-building-list').css('display','none');
        $('#meo-crm-realestate-dashboard').css('display','block');
    });
    
    $('#new-building').click(function(){
        resetAllBuilding();  
        $('#meo-crm-realestate-building-list').css('display','none');
        $('#meo-crm-realestate-building-form').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'getListSector', 
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                console.log(response);
                $('#building_sector_list').html(response);
            }
        );
    });
    
    $('#back-building').click(function(){
        resetAllBuilding(); 
        $('#meo-crm-waiting-bloc').css('display','block');
        $('#meo-crm-realestate-building-form').css('display','none');
        $('#meo-crm-realestate-building-list').css('display','block');
        loadTableBuilding();
    });
    
    
    // Execute the submit form
    $('#form_building').on('submit',(function(e) {
        
        e.preventDefault();
        
        $('#meo-crm-realestate-building-form').css('display','none');
        $('#meo-crm-waiting-bloc').css('display','block');
        var ajaxData = new FormData(this);        
        ajaxData.append('action', 'saveBuilding');
        ajaxData.append('project_id', $('#realestate_project_id').val());        
        ajaxData.append('building_sector_image_3d_hover', $('#input_building_sector_image_3d_hover').val()); 
        ajaxData.append('building_sector_image_2d_hover', $('#input_building_sector_image_2d_hover').val()); 
        ajaxData.append('building_front_face', $('#input_building_front_face').val()); 
        ajaxData.append('building_back_face', $('#input_building_back_face').val()); 
        ajaxData.append('building_preview', $('#input_building_preview').val()); 
        ajaxData.append('building_sector', $('#input_building_sector').val());
        
        if($('#building_title').val() != '' && $('#building_sector_list').val() != '-1' &&  $('#coordinate_building_sector_3d').val() != '' &&  $('#coordinate_building_sector_2d').val() != '' && ( $('#building_sector_image_3d_hover').val() != '' || $('#input_building_sector_image_3d_hover').val() != '' ) && ( $('#building_sector_image_2d_hover').val() != '' || $('#input_building_sector_image_2d_hover').val() != '' ) && ( $('#building_front_face').val() != '' || $('#input_building_front_face').val()
                != '' ) && ( $('#building_preview').val() != '' || $('#input_building_preview').val() != '' ) && ( $('#building_sector').val() != '' || $('#input_building_sector').val() != '' )){
            $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                data: ajaxData,
                contentType: false,
                cache: false,
                processData:false,
                success: function(response)
                {
                    console.log(response);
                    
                    $('#meo-crm-realestate-building-form').css('display','block');
                    $('#meo-crm-waiting-bloc').css('display','none');
                    
                    var obj = $.parseJSON(response);                    
                    $('#zone-message').html(obj.message);                   
                    $('#building_id').val(obj.id);              
                    
                    $('#img_building_sector_image_3d_hover').attr('src', $('#url_upload').val()+'/'+obj.images.image_hover_plan_sector_3d);  
                    $('#input_building_sector_image_3d_hover').val(obj.images.image_hover_plan_sector_3d); 
                    
                    $('#img_building_sector_image_2d_hover').attr('src', $('#url_upload').val()+'/'+obj.images.image_hover_plan_sector_2d); 
                    $('#input_building_sector_image_2d_hover').val(obj.images.image_hover_plan_sector_3d); 
                    
                    $('#img_building_front_face').attr('src', $('#url_upload').val()+'/'+obj.images.image_front_face);       
                    $('#input_building_front_face').val(obj.images.image_hover_plan_sector_3d); 
                    
                    $('#img_building_back_face').attr('src', $('#url_upload').val()+'/'+obj.images.image_back_face);    
                    $('#input_building_back_face').val(obj.images.image_hover_plan_sector_3d); 
                    
                    $('#img_building_preview').attr('src', $('#url_upload').val()+'/'+obj.images.image_preview);   
                    $('#input_building_preview').val(obj.images.image_hover_plan_sector_3d); 
                    
                    $('#img_building_sector').attr('src', $('#url_upload').val()+'/'+obj.images.image_building_sector);
                    $('#input_building_sector').val(obj.images.image_hover_plan_sector_3d);    
                }
            });
        }else{
            alert('Veuillez renseigner les champs obligatoires !');
        }
        
    }));
    
});

/* START FUNCTION */

function editBuilding(id)
{
    resetAllBuilding();  
    $('#meo-crm-waiting-bloc').css('display','block');
    $('#meo-crm-realestate-building-list').css('display','none');
    $('#meo-crm-realestate-building-form').css('display','block');
    $('#status_id').val(id);
    $.post(
        ajaxurl,
        {
            'action': 'getBuildingById',
            'id': id,
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            
            var obj = $.parseJSON(response);
            
            $('#building_id').val(obj.id); 
            
            $('#building_title').val(obj.title);
            
            $('#building_description').val(obj.description);
            $('#building_description').html(obj.description);
            
            $('#building_indoor_parc').val(obj.park_place_indoor);
            
            $('#building_outdoor_parc').val(obj.park_place_outdoor);
            
            $('#coordinate_building_sector_3d').val(obj.coordinate_plan_sector_3d);
            
            $('#coordinate_building_sector_2d').val(obj.coordinate_plan_sector_2d);
                    
            $('#img_building_sector_image_3d_hover').attr('src', $('#url_upload').val()+'/'+obj.image_hover_plan_sector_3d);  
            $('#input_building_sector_image_3d_hover').val(obj.image_hover_plan_sector_3d); 

            $('#img_building_sector_image_2d_hover').attr('src', $('#url_upload').val()+'/'+obj.image_hover_plan_sector_2d); 
            $('#input_building_sector_image_2d_hover').val(obj.image_hover_plan_sector_2d); 

            $('#img_building_front_face').attr('src', $('#url_upload').val()+'/'+obj.image_front_face);       
            $('#input_building_front_face').val(obj.image_front_face); 

            $('#img_building_back_face').attr('src', $('#url_upload').val()+'/'+obj.image_back_face);    
            $('#input_building_back_face').val(obj.image_back_face); 

            $('#img_building_preview').attr('src', $('#url_upload').val()+'/'+obj.image_preview);   
            $('#input_building_preview').val(obj.image_preview); 

            $('#img_building_sector').attr('src', $('#url_upload').val()+'/'+obj.image_building_sector);
            $('#input_building_sector').val(obj.image_building_sector);             
            
            $.post(
                ajaxurl,
                {
                    'action': 'getListSector', 
                    'project_id': $('#realestate_project_id').val()
                },
                function(list){
                    console.log(list);
                    $('#building_sector_list').html(list);
                    $('#building_sector_list option[value=' + obj.sector_id + ']').prop('selected',true);
                }
            ); 
            
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function deleteBuilding(id)
{
    if(confirm('Voulez-vous vraiment supprimer cette element ?')){
        $('#meo-crm-waiting-bloc').css('display','block');
        $.post(
            ajaxurl,
            {
                'action': 'deleteBuilding',
                'id': id,
                'project_id': $('#realestate_project_id').val()
            },
            function(response){
                var obj = $.parseJSON(response);
                $('#zone-message').html(obj.message);
                loadTableBuilding();
                $('#meo-crm-waiting-bloc').css('display','none');            
            }
        );
    }
}

function loadTableBuilding()
{
    $.post(
        ajaxurl,
        {
            'action': 'getAllBuilding',
            'project_id': $('#realestate_project_id').val()
        },
        function(response){
            console.log(response);
            $('#table-container-building').html(response);
            $('#meo-crm-waiting-bloc').css('display','none');
        }
    );
}

function resetAllBuilding()
{
    $('#zone-message').html('');
    $('#table-container-building').html('');    
    $('#building_id').val('');
    $('#building_id').val('');             
    $('#building_title').val('');  
    $('#building_description').val(''); 
    $('#building_description').html('');            
    $('#building_indoor_parc').val('');            
    $('#building_outdoor_parc').val('');            
    $('#coordinate_building_sector_3d').val('');            
    $('#coordinate_building_sector_2d').val(''); 
    
    $('#building_sector_image_3d_hover').val('');
    $('#img_building_sector_image_3d_hover').attr('src', '');  
    $('#input_building_sector_image_3d_hover').val(''); 
    
    $('#building_sector_image_2d_hover').val('');
    $('#img_building_sector_image_2d_hover').attr('src', ''); 
    $('#input_building_sector_image_2d_hover').val(''); 
    
    $('#building_front_face').val('');
    $('#img_building_front_face').attr('src', '');       
    $('#input_building_front_face').val(''); 
    
    $('#building_back_face').val('');
    $('#img_building_back_face').attr('src', '');    
    $('#input_building_back_face').val(''); 
    
    $('#building_preview').val('');
    $('#img_building_preview').attr('src', '');   
    $('#input_building_preview').val(''); 
    
    $('#building_sector').val('');
    $('#img_building_sector').attr('src', '');
    $('#input_building_sector').val(''); 
}
/* END FUNCTION */