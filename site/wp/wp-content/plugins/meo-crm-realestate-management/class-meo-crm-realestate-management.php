<?php

class MeoCrmRealestateManagement
{
    ######################
    ##  Access Project  ##
    ######################
    
    // Select all access project
    public static function selectAccessProjectById($id)
    {
        global $wpdb;
        $query  = ' SELECT *
                    FROM ' . getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE . ' 
                    WHERE id='.$id.' ';
        return $wpdb->get_results($query);
    }
    // Select access project by project id
    public static function selectAccessProjectByProjectId($id)
    {
        global $wpdb;
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE.' 
                    WHERE project_id='.$id.' ';
        $results = $wpdb->get_results($query);
        return (isset($results[0]) && isset($results)) ? $results[0] : $results ;
    }
    // Insert access project
    public static function insertAccessProject($datas)
    {
        global $wpdb;
        if($wpdb->insert(getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE,$datas))
        {
            return $wpdb->insert_id;
        }
        return false;
    }
    // Update access project
    public static function updateAccessProject($datas,$where)
    {
        global $wpdb;
        if($wpdb->update(getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE,$datas,$where))
        {
            return true;
        }
        return false;
    }


    ###################
    ## Developpement ##
    ###################

    // Select All developpement in selected project DB
    public static function getDeveloppements($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Select developpement by ID in selected project DB
    public static function getDeveloppementById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE.'
                    WHERE id='.$id;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Insert developpement in selected project DB
    public static function insertDeveloppement($external_wpdb, $data)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE;
        return MeoCrmCoreExternalDbQuery::insert($external_wpdb, $table, $data);
    }
    // update developpement in selected project DB
    public static function updateDeveloppement($external_wpdb, $data, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE;
        return MeoCrmCoreExternalDbQuery::update($external_wpdb, $table, $data, $where);
    }
    // delete developpement in selected project DB
    public static function deleteDeveloppement($external_wpdb, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE;
        return MeoCrmCoreExternalDbQuery::delete($external_wpdb, $table, $where);
    }

    #################
    ##    Sector   ##
    #################

    // Select All Sector in selected project DB
    public static function getSectors($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SECTORS_TABLE;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Select Sector by ID in selected project DB
    public static function getSectorById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SECTORS_TABLE.'
                    WHERE id='.$id;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Insert Sector in selected project DB
    public static function insertSector($external_wpdb, $data)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_SECTORS_TABLE;
        return MeoCrmCoreExternalDbQuery::insert($external_wpdb, $table, $data);
    }
    // update Sector in selected project DB
    public static function updateSector($external_wpdb, $data, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_SECTORS_TABLE;
        return MeoCrmCoreExternalDbQuery::update($external_wpdb, $table, $data, $where);
    }
    // delete Sector in selected project DB
    public static function deleteSector($external_wpdb, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_SECTORS_TABLE;
        return MeoCrmCoreExternalDbQuery::delete($external_wpdb, $table, $where);
    }

    #################
    ##   BUILDING  ##
    #################

    // Select All Status in selected project DB
    public static function getBuildings($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Select Status by ID in selected project DB
    public static function getBuildingById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE.'
                    WHERE id='.$id;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Insert Status in selected project DB
    public static function insertBuilding($external_wpdb, $data)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE;
        return MeoCrmCoreExternalDbQuery::insert($external_wpdb, $table, $data);
    }
    // update Status in selected project DB
    public static function updateBuilding($external_wpdb, $data, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE;
        return MeoCrmCoreExternalDbQuery::update($external_wpdb, $table, $data, $where);
    }
    // delete Status in selected project DB
    public static function deleteBuilding($external_wpdb, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE;
        return MeoCrmCoreExternalDbQuery::delete($external_wpdb, $table, $where);
    }

    #################
    ##    FLOORS   ##
    #################

    public static function getFloors($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.'
                    ORDER BY position, building_id';
        return $external_wpdb->get_results( $query );
    }

    public static function getAllFloorJoinBuilding($external_wpdb)
    {
        $query  = ' SELECT f.id, b.title AS building, f.title AS floor 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' AS f 
                    INNER JOIN '.getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE.' AS b ON f.building_id = b.id 
                    ORDER BY f.position, f.building_id';
        return $external_wpdb->get_results( $query );
    }

    public static function getFloorById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' 
                    WHERE id='.$id.'
                    ORDER BY position ASC';
        return $external_wpdb->get_results( $query );
    }

    public static function getFloorByBuildingId($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' 
                    WHERE building_id='.$id.'
                    ORDER BY position ASC';
        return $external_wpdb->get_results( $query );
    }

    public static function insertFloor($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE,$datas) === false)
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }

    public static function updateFloor($external_wpdb,$datas,$where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE,$datas,$where) === false)
        {
            return true;
        }
        return false;
    }

    public static function deleteFloor($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE,$where) === false)
        {
            return true;
        }
        return false;
    }

    #################
    ##  FLOOR LOT  ##
    #################

    public static function getAllFloorLot($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    public static function getFloorLotByLotId($external_wpdb,$id)
    {
        $query  = 'SELECT fl.id AS floor_lot_id, fl.lot_entry, fl.lot_id, fl.floor_id, fl.plan_floor, f.title AS floor_title , cplb.title AS coordinate_title 
                   FROM ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE . ' AS fl  
                   LEFT JOIN ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE . ' AS f ON fl.floor_id = f.id
                   LEFT JOIN ' . getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE . ' AS cplb ON fl.status_plan_floor_id = cplb.id
                   WHERE lot_id='.$id;
        
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    public static function getFloorLotByLotIdAndFloorId($external_wpdb,$lot_id,$floor_id)
    {
        $query  = 'SELECT * 
                   FROM ' . getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE . ' AS fl  
                   WHERE lot_id='.$lot_id.'
                   AND floor_id = '.$floor_id;
        
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    public static function insertFloorLot($external_wpdb,$data)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE;
        return MeoCrmCoreExternalDbQuery::insert($external_wpdb, $table, $data);
    }
    public static function updateFloorLot($external_wpdb,$data,$where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE;
        return MeoCrmCoreExternalDbQuery::update($external_wpdb, $table, $data, $where);
    }
    public static function deleteFloorLot($external_wpdb,$where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_LOT_TABLE;
        return MeoCrmCoreExternalDbQuery::delete($external_wpdb, $table, $where);
    }

    ###########################
    ##  Building Plan Floor  ##
    ###########################

    /*public static function getFullDataLotById($external_wpdb)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE.' ';
        $results = $external_wpdb->get_results( $query );
        return $results;
    }*/
    public static function getAllBuildingFloorPlan($external_wpdb)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE.' ';       
        return $external_wpdb->get_results( $query );
    }
    public static function getBuildinFloorPlanById($external_wpdb, $id_coordinate)
    {
        $query = '  SELECT c.id AS coordinates_id, c.title, c.coordinates_front, c.coordinates_back, pc.id AS image_id, pc.status_id, pc.image, pc.front_face
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE . ' AS c
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_LINK_COORDINATES_PLAN_FLOOR_LOT_BUILDING_TABLE . ' AS lc ON lc.coordinates_id = c.id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE . ' AS pc ON lc.plan_lot_id = pc.id
                    WHERE c.id = ' . $id_coordinate ;
        return $external_wpdb->get_results( $query );
    }
    public static function insertCoordinateFloorPlan($external_wpdb, $datas)
    {
        if ($external_wpdb->insert(getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE, $datas)) {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function updateCoordinateFloorPlan($external_wpdb, $datas, $where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE,$datas,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteCoordinateFloorPlan($external_wpdb, $where = array())
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE, $where))
        {
            return true;
        }
        return false;
    }
    public static function insertImageFloorPlan($external_wpdb, $datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE, $datas))
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function updateImageFloorPlan($external_wpdb, $datas, $where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_PLAN_LOT_BUILDING_TABLE, $datas, $where))
        {
            return true;
        }
        return flase;
    }
    public static function insertLinkFloorPlan($external_wpdb, $datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LINK_COORDINATES_PLAN_FLOOR_LOT_BUILDING_TABLE, $datas))
        {
            return true;
        }
        return false;
    }

    #################
    ##     Lot     ##
    #################

    public static function getAllLot($external_wpdb)
    {
        $query  = '	SELECT *
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE ;
        return $external_wpdb->get_results( $query );
    }
    public static function getAllDocumentPost($external_wpdb)
    {
        global $wpdb;
        $query  = ' SELECT *
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_POSTS_TABLE . '     
                    WHERE post_mime_type LIKE "%application/pdf%"';
        return $external_wpdb->get_results( $query );
    }
    public static function getLotById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE . ' AS l   
                    WHERE l.id='.$id;
        return $external_wpdb->get_results( $query );
    }
    public static function getFullDataLotById($external_wpdb,$id)
    {
        $query  = ' SELECT l.id AS lot_id, l.title AS lot_title, l.description AS lot_description, l.rooms AS lot_rooms, l.type_lot AS lot_type, l.surface AS lot_surface, l.price AS lot_price, l.status_id, l.document_id, l.plan_id, pbl.title AS plan_building_status_title, pbl.coordinates_front AS front_coordinate, pbl.coordinates_back AS back_coordinate, f.title AS floor_title, fl.status_plan_floor_id, f.id AS floor_id, fl.lot_entry, fl.plan_floor 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE . ' AS l    
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_LOT_TABLE .' AS fl ON fl.lot_id = l.id    
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE .' AS pbl ON pbl.id = fl.status_plan_floor_id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_TABLE .' AS f ON fl.floor_id = f.id
                    WHERE l.id=' . $id;
        $results = $external_wpdb->get_results( $query );
        return (isset($results)) ? $results : array();
    }
    public static function insertLot($external_wpdb,$data)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LOTS_TABLE,$data))
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function updateLot($external_wpdb,$datas,$where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_LOTS_TABLE,$datas,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteLot($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_LOTS_TABLE,$where))
        {
            return true;
        }
        return false;
    }


    #################
    ##   Meta Lot  ##
    #################
    public static function getAllMetaLot($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE.'
                    ORDER BY meta_order ASC ';
        return $external_wpdb->get_results( $query );
    }
    public static function getMetaLotById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE.' 
                    WHERE id='.$id;
        return $external_wpdb->get_results( $query );
    }
    public static function getValueMetaLotByLotId($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE.' 
                    WHERE lot_id='.$id;
        return $external_wpdb->get_results( $query );
    }
    public static function insertMetaLot($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE,$datas))
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function updateMetaLot($external_wpdb,$datas,$where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE,$datas,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteMetaLot($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_LOT_METAS_TABLE,$where))
        {
            return true;
        }
        return false;
    }
    public static function getMetaValueLotById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE.'
                    WHERE lot_id='.$id;
        $results = $external_wpdb->get_results( $query );
        return $results[0];
    }
    public static function getMetaValueLotByLotIdAndMetaId($external_wpdb,$lot_id,$meta_id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE.'
                    WHERE lot_id='.$lot_id.' AND meta_id='.$meta_id;
        return $external_wpdb->get_results( $query );
    }
    public static function insertMetaValueLot($external_wpdb,$data)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE,$data))
        {
            return true;
        }
        return false;
    }
    public static function updateMetaValueLot($external_wpdb,$datas,$where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE,$datas,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteMetaValueLot($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE,$where))
        {
            return true;
        }
        return false;
    }


    #################
    ##    Plan     ##
    #################

    public static function getAllPlan($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE;
        return $external_wpdb->get_results( $query );
    }
    public static function getPlanById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE.'
                    WHERE id='.$id;
        $results = $external_wpdb->get_results( $query );
        return $results[0];
    }
    public static function insertPlan($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE,$datas))
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function updatePlan($external_wpdb,$datas,$where)
    {
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE,$datas,$where))
        {
            return true;
        }
        return false;
    }
    public static function deletePlan($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_PLANS_TABLE,$where))
        {
            return true;
        }
        return false;
    }

    #################
    ##  PriceList  ##
    #################
    public static function getPriceListParamByProjectId($project_id)
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OPTIONS_TABLE . ' 
                    WHERE project_id = '.$project_id;
        return $query;
    }
    public static function getPriceListOtherPriceByTemplateId()
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES_TABLE ;
        return $query;
    }
    public static function getPriceListHeaderByTemplateId()
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_HEADER_VALUE_TABLE ;
        return $query;
    }

    ###################
    ##  SpinnerImmo  ##
    ###################

    public static function getAllSpinner($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE.' ';
        return $external_wpdb->get_results( $query );
    }
    public static function getSpinnerById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE.' 
                    WHERE id='.$id.' ';
        return $external_wpdb->get_results( $query );
    }
    public static function getAttachmentBySpinnerId($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_POST_TABLE.' 
                    WHERE spinner_id='.$id.' ';
        return $external_wpdb->get_results( $query );
    }
    public static function insertSpinner($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE,$datas) === false)
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function insertSpinnerAttachment($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_POST_TABLE,$datas) === false)
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function insertSpinnerPost($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_POSTS_TABLE,$datas))
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function insertSpinnerPostMeta($external_wpdb,$datas)
    {
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_POSTMETA_TABLE,$datas))
        {
            return $external_wpdb->insert_id;
        }
        return false;
    }
    public static function deleteSpinner($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteSpinnerAttachment($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_POST_TABLE,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteSpinnerPost($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_POSTS_TABLE,$where))
        {
            return true;
        }
        return false;
    }
    public static function deleteSpinnerPostMeta($external_wpdb,$where)
    {
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_POSTMETA_TABLE,$where))
        {
            return true;
        }
        return false;
    }

    #################
    ##    Status   ##
    #################

    // Select All Status in selected project DB
    public static function getStatus($external_wpdb)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_STATUS_TABLE;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Select Status by ID in selected project DB
    public static function getStatusById($external_wpdb,$id)
    {
        $query  = ' SELECT * 
                    FROM '.getExternalPrefix().MEO_CRM_REALESTATE_STATUS_TABLE.'
                    WHERE id='.$id;
        return MeoCrmCoreExternalDbQuery::select($external_wpdb, $query);
    }
    // Insert Status in selected project DB
    public static function insertStatus($external_wpdb, $data)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_STATUS_TABLE;
        return MeoCrmCoreExternalDbQuery::insert($external_wpdb, $table, $data);
    }
    // update Status in selected project DB
    public static function updateStatus($external_wpdb, $data, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_STATUS_TABLE;
        return MeoCrmCoreExternalDbQuery::update($external_wpdb, $table, $data, $where);
    }
    // delete Status in selected project DB
    public static function deleteStatus($external_wpdb, $where)
    {
        $table = getExternalPrefix().MEO_CRM_REALESTATE_STATUS_TABLE;
        return MeoCrmCoreExternalDbQuery::delete($external_wpdb, $table, $where);
    }


    #################
    ##   PDF LIST  ##
    #################

    public static function getAllLotsForPriceList($external_wpdb)
    {
        $count = 0;
        $array_to_return = array();

        $query = '  SELECT l.id AS lot_id, l.status_id, l.title AS lot_name, s.name AS status_name, s.color AS status_color, s.hide_price, b.title AS building_name, f.title AS floor_title, l.rooms, l.surface, l.price, m.id AS meta_id, m.meta_key, mv.value AS meta_value, m.meta_slug 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE . ' AS l
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_LOT_TABLE . ' AS fl ON fl.lot_id = l.id 
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE . ' AS pbl ON pbl.id = fl.status_plan_floor_id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_TABLE . ' AS f ON fl.floor_id = f.id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_BUILDING_TABLE . ' AS b ON f.building_id = b.id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOT_META_VALUE_TABLE . ' AS mv ON mv.lot_id = l.id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOT_METAS_TABLE . ' AS m ON mv.meta_id = m.id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_STATUS_TABLE . ' AS s ON l.status_id = s.id
                    ORDER BY l.title, m.meta_order ';

        $lots = MeoCrmCoreExternalDbQuery::select($external_wpdb,$query,ARRAY_A);
        foreach($lots as $lot)
        {
            foreach($lot as $key => $value)
            {
                if($key != 'meta_price_list' && $key != 'meta_value' && $key != 'meta_key' && $key != 'meta_slug')
                {
                    if(!isset($array_to_return[$lot['lot_id']][$key]))
                    {
                        $array_to_return[$lot['lot_id']][$key] = $value;
                    }
                }else if($key == 'meta_value' || $key == 'meta_key' || $key == 'meta_slug'){
                    $array_to_return[$lot['lot_id']]['metas'][$lot['meta_id']][$key] = $value;
                }
            }
        }
        return $array_to_return;
    }

    public static function getPriceListPdfParam($project_id,$external_wpdb)
    {
        $count = 0;
        $temp_array = array();
        $query_option = ' SELECT * FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OPTIONS_TABLE ;
        $query_other_price = ' SELECT * FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES_TABLE ;
        $query_header = ' SELECT * FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_PRICE_LIST_HEADER_VALUE_TABLE . ' ORDER BY position';
        $results_options = $external_wpdb->get_results($query_option);
        $results_other_price = $external_wpdb->get_results($query_other_price);
        $results_header = $external_wpdb->get_results($query_header);
        if(isset($results_options) && !empty($results_options))
        {
            $temp_array['pdf_template_landscape'] = $results_options[0]->pdf_template_landscape;
            $temp_array['pdf_template_portrait'] = $results_options[0]->pdf_template_portrait;
            $temp_array['color_theme'] = $results_options[0]->color_theme;
            $temp_array['color_info'] = $results_options[0]->color_info;
        }
        if(isset($results_other_price) && !empty($results_other_price))
        {
            foreach($results_other_price as $key => $value)
            {
                $temp_array['other_price'][$count]['label'] = $value->label;
                $temp_array['other_price'][$count]['price'] = $value->price;
                $count++;
            }
        }
        $count = 0;
        if(isset($results_header) && !empty($results_header))
        {
            foreach($results_header as $key => $value)
            {
                $temp_array['header'][$count]['header_key'] = $value->header_key;
                $temp_array['header'][$count]['label_1'] = $value->label_1;
                $temp_array['header'][$count]['label_2'] = $value->label_2;
                $count++;
            }
        }
        return $temp_array;
    }

    public static function getRealestateMetas($external_wpdb)
    {
        $query_temp = ' SELECT * 
                        FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOT_METAS_TABLE . ' AS m 
                        ORDER BY meta_order';
        return $query_temp;
    }

    public static function getPostAttachmentPriceList()
    {
        $query = '  SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_POSTS_TABLE . '
                    WHERE post_type = "attachment" AND post_name = "price-list" AND post_mime_type="application/pdf" ';
        return $query;
    }
}