<?php 

/*
 *  Get Spinner realestate by project
 */
add_action( 'wp_ajax_nopriv_getProjectSpinner', 'getProjectSpinner' );  
add_action( 'wp_ajax_getProjectSpinner', 'getProjectSpinner' );
function getProjectSpinner()
{       
    $datas = array();
    $return_html = array();
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $return_html = array();
    $helperList = new MeoCrmCoreListHelper();
    
    $results = SpinnerModel::selectSpinnersByProjectId($project_id);
    
    if(isset($results) && !empty($results))
    {
        foreach($results as $result)
        {
            $datas['app'][$result->id]['id'] = $result->id;
            $datas['app'][$result->id]['author_id'] = $result->author_id;
            $datas['app'][$result->id]['project_id'] = $result->project_id;
            $datas['app'][$result->id]['name'] = $result->name;
            $datas['app'][$result->id]['description'] = $result->description;
        } 
    }else{
        $datas['app'] = array();
    }
    
    $header_app = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'name', 'name' => 'name', 'type_data' => 'base', 'class' => ''),
    );  
    
    $list_action_app = array(
        array('click_action' => 'exportSpinner(@id)', 'icon' => 'fa fa-upload', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
    );  
    
    $return_html['list_app'] = $helperList->getList($datas['app'], $header_app, $list_action_app);
    
    
    $results = SpinnerImmoModel::getAllSpinner($external_wpdb);  
    if(isset($results) && !empty($results))
    {
        foreach($results as $result)
        {
            $datas['site'][$result->id]['id'] = $result->id;
            $datas['site'][$result->id]['title'] = $result->title;
            $datas['site'][$result->id]['description'] = $result->description;
        }  
    }else{
        $datas['site'] = array();
    }
    $header_site = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'title', 'name' => 'name', 'type_data' => 'base', 'class' => ''),
    );     
    $list_action_site = array(
        array('click_action' => 'deleteExportedSpinner(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
    );         
    $return_html['list_website'] = $helperList->getList($datas['site'], $header_site, $list_action_site);
    
    echo json_encode($return_html);
    
    die();
}

/*
 *  Get Spinner realestate by project
 */
add_action( 'wp_ajax_nopriv_exportSpinner', 'exportSpinner' );  
add_action( 'wp_ajax_exportSpinner', 'exportSpinner' );
function exportSpinner()
{   
    global $wpdb;
    
    // Init Variables
    $check = true;
    $errors = array();
    $datas = array();
    $spinner_id = $_POST['spinner_id'];
    $project_id = $_POST['project_id'];
    
    // Call static methods
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    
    // Path image spinner in upload
    $upload_path = $baseDirFTP.'uploads/';
    $upload_url = $project['url'].'wp-content/uploads/';    
    $base_path = $upload_path.'spinner/';
    $base_url = $upload_url.'spinner/';
    
    if(isset($spinner_id) && !empty($spinner_id))
    {        
        // Copy Spinner Table With ID = Spinner_id
        $spinner = SpinnerModel::selectExportSpinnerMedia($spinner_id);
        
        // Insert spinner in website immo
        $results = SpinnerImmoModel::insertSpinner($external_wpdb, $spinner['spinner']);
        $new_spinner_id = $results['id'];
        
        if($results['success'])
        {
            // Init Variables      
            $list_posts = array();
            $spinner_id = $results['id'];
            $posts = $spinner['posts'];  
            
            // Insert Post Attachment
            foreach($posts as $post)
            {                
                $datas = array();  
                $datas['post_author'] = $post['post_author'];
                $datas['post_date'] = $post['post_date'];
                $datas['post_date_gmt'] = $post['post_date_gmt'];
                $datas['post_title'] = $post['post_title'];
                $datas['post_excerpt'] = $post['post_excerpt'];   
                $datas['post_status'] = $post['post_status'];
                $datas['comment_status'] = $post['comment_status'];                        
                $datas['ping_status'] = $post['ping_status'];
                $datas['post_name'] = $post['post_name'];
                $datas['post_modified'] = $post['post_modified'];
                $datas['post_modified_gmt'] = $post['post_modified_gmt'];
                $datas['post_parent'] = $post['post_parent'];
                $datas['guid'] = $base_url.$spinner_id.'/'.$post['post_title'];
                $datas['menu_order'] = $post['menu_order'];
                $datas['post_type'] = $post['post_type'];
                $datas['post_mime_type'] = $post['post_mime_type'];
                $datas['comment_count'] = $post['comment_count'];
                
                $results = SpinnerImmoModel::insertSpinnerPost($external_wpdb, $datas);
                
                $list_post = array(
                    'post_id' => $results['id'],
                    'spinner_id' => $spinner_id
                );
                
                $resultsAttachment = SpinnerImmoModel::insertSpinnerAttachment($external_wpdb, $list_post);
                
                if(!$resultsAttachment['success'])
                {
                    $errors[] = 'LINKED SPINNER POST ERROR INSERT';
                    $check = false;
                }
                
                if($results['success'])
                {     
                    $datas = array(); 
                    foreach($post['postmeta'] as $key => $postmeta)
                    {
                        $datas['postmeta'] = array();

                        $path_file = $base_path.$spinner_id.'/'.$post['post_title'];
                        
                        if($postmeta['meta_key'] == 'path_file')
                        {
                            $path_realestate =  $postmeta['meta_value'];
                            $datas['postmeta'] = array(
                                'post_id' => $results['id'],
                                'meta_key' => 'path_file',
                                'meta_value' =>  $path_file
                            );
                        }
                        
                        // Save Meta
                        $results = SpinnerImmoModel::insertSpinnerPostMeta($external_wpdb, $datas['postmeta']);
                        if(!$results['success'])
                        {
                            $errors[] = 'POST META ERROR INSERT';
                            $check = false;
                        }
                    }
                }else{
                    $errors[] = 'POST ERROR INSERT';
                    $check = false;
                }
                
                // Save spinner attachmenet
                foreach($list_posts as $list_post)
                {
                    $results = SpinnerImmoModel::insertSpinnerAttachment($external_wpdb, $list_post);
                    if(!$results['success'])
                    {
                        $errors[] = 'LINKED SPINNER POST ERROR INSERT';
                        $check = false;
                    }
                }
                
                // Upload spinner file and create folder 
                if($check)
                {
                    // Create Folder if not exist on external FTP
                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path, 'spinner'))
                    {
                        if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path.'spinner/', $new_spinner_id))
                        {
                            $folder_created = true;
                        }
                    }

                    // Upload all images spinner on external FTP
                    if($folder_created)
                    {
                        if(!ftp_put($ftp, $path_file, $path_realestate, FTP_BINARY))
                        {
                            $check = false;
                        }
                    }
                }               
            }
        }else{
            $errors[] = 'SPINNER ERROR INSERT';
            $check = false;
        }
        
        if($check)
        {
            // Change export value
            $datas = array();
            $datas['export'] = 1;
            $where = array('id' => $_POST['spinner_id']);            
            if(SpinnerModel::updateSpinner($datas, $where))
            {
                $check = true;
                $message = $helperCore->getFlashMessageAdmin('Spinner export success', 1);
            }else{
                $check = true;
                $message = $helperCore->getFlashMessageAdmin('Spinner export error', 0);
            }
        }else{
            $check = true;
            $message = $helperCore->getFlashMessageAdmin('Spinner export error', 0);
        }
        
    } 
    
    // Ajax array return to JS
    $result = array(
        'success' => $check,
        'message' => $message
    );    
    echo json_encode($result);
    
    // Close external FTP connection
    ftp_close($ftp);
    
    die();
}

/*
 *  Delete Spinner
 */
add_action( 'wp_ajax_nopriv_deleteExportedSpinner', 'deleteExportedSpinner' );  
add_action( 'wp_ajax_deleteExportedSpinner', 'deleteExportedSpinner' );
function deleteExportedSpinner()
{   
    // Init Variables
    $check = true;
    $datas = array();
    $errors = array();
    $spinner_id = $_POST['spinner_id'];
    $project_id = $_POST['project_id'];
    
    // Call static methods
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Path image spinner in upload
    $upload_path = $baseDirFTP.'uploads/';
    $upload_url = $project['url'].'wp-content/uploads/';    
    $base_path = $upload_path.'spinner/';
    $base_url = $upload_url.'spinner/';    
    $full_path = $base_path.$spinner_id.'/';
    
    // List post link to spinner
    $spinner = SpinnerImmoModel::getSpinnerById($external_wpdb,$spinner_id);
    $list_posts = SpinnerImmoModel::getAttachmentBySpinnerId($external_wpdb,$spinner_id);
    
    // List the image
    $images = ftp_nlist($ftp, $full_path);  
    
    // Delete image list
    foreach($images as $image)
    {
        list($path, $filename) = explode('/'.$spinner_id.'/', $image);
        if($filename != '.' && $filename != '..' )
        {
            if(!ftp_delete ($ftp, $image))
            {
                $errors[] = $image;
                $check = false;
            }
        }
    }
    
    // If all files deleted => delete folder
    if($check)
    {
        if(!ftp_rmdir ($ftp, $full_path))
        {
            $errors[] = $full_path;
            $check = false;
        }else{
            $where = array('id' => $spinner_id);
            if(SpinnerImmoModel::deleteSpinner($external_wpdb,$where))
            {
                $where = array('spinner_id' => $spinner_id);
                if(SpinnerImmoModel::deleteSpinnerAttachment($external_wpdb,$where))
                {
                    // SUPPRIMER LES POST ET POST META
                    foreach($list_posts as $list_post)
                    {
                        $where = array('post_id' => $list_post->post_id);
                        if(SpinnerImmoModel::deleteSpinnerPostMeta($external_wpdb,$where))
                        {
                            $where = array('ID' => $list_post->post_id);
                            if(!SpinnerImmoModel::deleteSpinnerPost($external_wpdb,$where))
                            {
                                $errors[] = 'Deleted spinner post error !';
                                $check = false;
                            }
                        }else{
                            $errors[] = 'Deleted spinner postmeta error !';
                            $check = false;
                        }
                    }
                }else{
                    $errors[] = 'Deleted spinner attachment error !';
                    $check = false;
                }
            }else{
                $errors[] = 'Deleted spinner error !';
                $check = false;
            }
        }    
    }
    
    //Defined return to JS
    if($check)
    {
        // Change export value
            $datas = array('export' => '0');
            $where = array('name' => $spinner[0]->title, 'description' => $spinner[0]->description); 
            
            if(SpinnerModel::updateSpinner($datas, $where))
            {
                $message = $helperCore->getFlashMessageAdmin('Spinner delete success', 1);
            }else{
                $errors[] = 'Change value export error';
                $message_content = 'Spinner delete error:';
                foreach($errors as $error)
                {
                    $message_content .= '<br/>- '.$error;
                }
                $message = $helperCore->getFlashMessageAdmin($message_content, 0);
            }
    }else{
        $message_content = 'Spinner delete error:';
        foreach($errors as $error)
        {
            $message_content .= '<br/>- '.$error;
        }
        $message = $helperCore->getFlashMessageAdmin($message_content, 0);
    }
    
    // Ajax array return to JS
    $result = array(
        'success' => $check,
        'message' => $message
    );    
    echo json_encode($result);
    
    // Close external FTP connection
    ftp_close($ftp);
    
    die();
}