<?php

/*
 *  Show Developpement List
 */
add_action( 'wp_ajax_nopriv_getDeveloppementList', 'getDeveloppementList' );  
add_action( 'wp_ajax_getDeveloppementList', 'getDeveloppementList' );
function getDeveloppementList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = DeveloppementModel::getAllDeveloppement($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['title'] = $result->title;
        $datas[$result->id]['description'] = $result->description;
        $datas[$result->id]['plan_developpement_3d'] = $result->plan_developpement_3d;
        $datas[$result->id]['plan_developpement_2d'] = $result->plan_developpement_2d;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'Description', 'type_data' => 'base', 'class' => ''),
        array('key' => 'plan_developpement_3d', 'name' => 'Plan Developpement 3d', 'type_data' => 'base', 'class' => ''),
        array('key' => 'plan_developpement_2d', 'name' => 'Plan Developpement 2d', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editDeveloppement(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deleteDeveloppement(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_getDeveloppementById', 'getDeveloppementById' );  
add_action( 'wp_ajax_getDeveloppementById', 'getDeveloppementById' );
function getDeveloppementById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $developpement = DeveloppementModel::getDeveloppementById($external_wpdb,$id);
    
    echo json_encode($developpement);
    
    die();
}

/*
 *  Save a new developpement or update one 
 */
add_action( 'wp_ajax_nopriv_saveDeveloppement', 'saveDeveloppement' );  
add_action( 'wp_ajax_saveDeveloppement', 'saveDeveloppement' );
function saveDeveloppement()
{        
    // Init variable
    $check = true;
    $datas = array();
    $actionDB = '';
    $folder_created = false;
    $deleted_picture = array();    
    $id = $_POST['developpement_id'];
    $project_id = $_POST['project_id'];
    $title = $_POST['developpement_title'];
    $description = $_POST['developpement_description'];
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);    
    
    // Check if title and description are informs
    if(!empty($title) && !empty($description))
    {    
        // Upate or create
        if(isset($id) && !empty($id))
        {
            // Start Update
            $datas['title'] = $title;
            $datas['description'] = $description;  
            
            // Url and path utility
            $content_dir_url = 'developpement/'.$id.'/';
            $content_dir_path = $baseDirFTP.'uploads/developpement/'.$id.'/';
            
            // Check image as change or not
            if(empty($_FILES['developpement_image_3d']['name']) && empty($_FILES['developpement_image_2d']['name']))
            {  
                $datas['plan_developpement_3d'] = $_POST['plan_developpement_3d_old'];
                $datas['plan_developpement_2d'] = $_POST['plan_developpement_2d_old'];                
            }else{
                if(!empty($_FILES['developpement_image_3d']['name']))
                {
                    $datas['plan_developpement_3d'] = $content_dir_url.$_FILES['developpement_image_3d']['name'];
                    $deleted_picture['plan_developpement_3d'] = $_POST['plan_developpement_3d_old'];
                }else{
                    $datas['plan_developpement_3d'] = $_POST['plan_developpement_3d_old'];
                }
                if(!empty($_FILES['developpement_image_2d']['name']))
                {
                    $datas['plan_developpement_2d'] = $content_dir_url.$_FILES['developpement_image_2d']['name'];
                    $deleted_picture['plan_developpement_2d'] = $_POST['plan_developpement_2d_old'];
                }else{
                    $datas['plan_developpement_2d'] = $_POST['plan_developpement_2d_old'];
                }
            }  
            
            // Image as change delete this and add new
            if(is_array($deleted_picture) && !empty($deleted_picture))
            {
                foreach($deleted_picture as $key => $picture)
                {
                    list($url, $filename) = explode('/'.$id.'/', $picture);                    
                    if(!ftp_delete($ftp, $content_dir_path.$filename))
                    {  
                        $check = false;
                    }else{
                        
                        if($key == 'plan_developpement_3d')
                        {
                            $tmp_file = $_FILES['developpement_image_3d']['tmp_name'];
                            $type_file = $_FILES['developpement_image_3d']['type'];
                            $name_file = $_FILES['developpement_image_3d']['name'];
                        }else if($key == 'plan_developpement_2d'){
                            $tmp_file = $_FILES['developpement_image_2d']['tmp_name'];
                            $type_file = $_FILES['developpement_image_2d']['type'];
                            $name_file = $_FILES['developpement_image_2d']['name'];
                        }  
                        
                        // Check if uploaded
                        if( !is_uploaded_file($tmp_file) )
                        {
                            $check = false;
                        }
                        
                        // on vérifie maintenant l'extension
                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                        {
                            $check = false;
                        }
                        
                        // on copie le fichier dans le dossier de destination
                        if( ftp_put($ftp, $content_dir_path.$name_file, $tmp_file, FTP_BINARY) === false )
                        {
                            $check = false;
                        }
                    }
                }
            }     
            
            // if upload picture is ok you can update informations
            if($check)
            {
                $where = array('id' => $id);            
                $result = DeveloppementModel::updateDeveloppment($external_wpdb, $datas, $where);            
                $actionDB = 'update'; 
                
                if($result['success'])
                {
                    $check = true;
                    $message = $helperCore->getFlashMessageAdmin('Developpement edit success', 1);
                }else{
                    $check = false;
                    $message = $helperCore->getFlashMessageAdmin('Developpement edit error', 0);
                }
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Developpement edit error', 0);
            }                        
            
        }else{
            // Create a new
            $datas['title'] = $title;
            $datas['description'] = $description;
            $result = DeveloppementModel::insertDeveloppment($external_wpdb, $datas);
            $id = $result['id'];
            $actionDB = 'insert';
            if($result['success'])
            {
                // upload media
                $content_dir_path = $baseDirFTP.'uploads/';       

                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, 'developpement'))
                {
                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.'developpement/', $id))
                    {
                        $folder_created = true;
                    }
                }

                if($folder_created)
                {

                    $content_dir_url = 'developpement/'.$id.'/';
                    $content_dir_path = $baseDirFTP.'uploads/developpement/'.$id.'/';

                    if($actionDB == 'insert')
                    {
                        $datas['plan_developpement_3d'] = $content_dir_url.$_FILES['developpement_image_3d']['name'];
                        $datas['plan_developpement_2d'] = $content_dir_url.$_FILES['developpement_image_2d']['name'];

                        $where = array(
                            'id' => $id
                        );

                        $result = DeveloppementModel::updateDeveloppment($external_wpdb, $datas, $where);
                    }

                    // Check if file is uploaded
                    $tmp_file_3d = $_FILES['developpement_image_3d']['tmp_name'];
                    $tmp_file_2d = $_FILES['developpement_image_2d']['tmp_name'];
                    if( !is_uploaded_file($tmp_file_3d) && !is_uploaded_file($tmp_file_2d) )
                    {
                        $check = false;
                    }

                    // on vérifie maintenant l'extension de l'image 3d
                    $type_file = $_FILES['developpement_image_3d']['type'];
                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        $check = false;
                    }

                    // on vérifie maintenant l'extension de l'image 2d
                    $type_file = $_FILES['developpement_image_2d']['type'];
                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        $check = false;
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file = $_FILES['developpement_image_3d']['name'];
                    if( ftp_put($ftp, $content_dir_path.$_FILES['developpement_image_3d']['name'], $tmp_file_3d, FTP_BINARY) === false )
                    {
                        $check = false;
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file = $_FILES['developpement_image_2d']['name'];
                    if( ftp_put($ftp, $content_dir_path.$_FILES['developpement_image_2d']['name'], $tmp_file_2d, FTP_BINARY) === false )
                    {
                        $check = false;
                    }

                    $check = true;
                    $message = $helperCore->getFlashMessageAdmin('Developpement save success', 1);
                }
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Developpement save error', 0);
            }
        }
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Developpement save error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
        'plan_developpement_3d' => $datas['plan_developpement_3d'],
        'plan_developpement_2d' => $datas['plan_developpement_2d']
    );
            
    echo json_encode($result);
    
    die();    
}

/*
 *  Delete a developpement
 */
add_action( 'wp_ajax_nopriv_deleteDeveloppement', 'deleteDeveloppement' );  
add_action( 'wp_ajax_deleteDeveloppement', 'deleteDeveloppement' );
function deleteDeveloppement()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Get current developpement for take a two image link
    $developpement = DeveloppementModel::getDeveloppementById($external_wpdb, $id);
    $plan_developpement_3d = $developpement->plan_developpement_3d;
    $plan_developpement_2d = $developpement->plan_developpement_2d;
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // URL for image
    $content_dir_path = $baseDirFTP.'uploads/developpement/'.$id.'/';
    
    // Delete image on FTP
    list($url_3d, $filename_3d) = explode('/'.$id.'/', $plan_developpement_3d);  
    list($url_2d, $filename_2d) = explode('/'.$id.'/', $plan_developpement_2d);  
    
    if(!ftp_delete($ftp, $content_dir_path.$filename_3d))
    {
        $check = false;
    }      
    
    if(!ftp_delete($ftp, $content_dir_path.$filename_2d))
    {
        $check = false;
    } 
    
    // Delete developpement on DB
    if($check)
    {
        $where = array( 'id' => $id );    
        $result = DeveloppementModel::deleteDeveloppment($external_wpdb,$where);
    }else{
        $result = array();
        $result['success'] = false;
    }
    
    // Check if all steps are OK for message return 
    if($result['success'])
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Developpement delete success', 1);
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Developpement delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id
    );
    
    echo json_encode($result);
    
    die();  
}