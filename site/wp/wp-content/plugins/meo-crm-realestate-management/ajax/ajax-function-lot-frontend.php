<?php

add_action( 'wp_ajax_nopriv_getAllFloorFromDatabaseToJSON', 'getAllFloorFromDatabaseToJSON' );  
add_action( 'wp_ajax_getAllFloorFromDatabaseToJSON', 'getAllFloorFromDatabaseToJSON' );
function getAllFloorFromDatabaseToJSON()
{
    $count = 0;
    $data = array();
    $project_id = $_POST['project'];

    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);

    $project = meo_crm_projects_getProjectInfo($project_id);
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);

    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);

    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $floors = FloorModel::getAllFloorJoinBuilding($external_wpdb);
    
    foreach($floors as $floor)
    {
        $data[$count]['value'] = $floor->id;
        $data[$count]['label'] = $floor->building.' - '.$floor->floor;
        
        $count++;
    }
    
    echo json_encode($data);
    die();
}
add_action( 'wp_ajax_nopriv_meoCrmRealeastateDeleteFloorLot', 'meoCrmRealeastateDeleteFloorLot' );  
add_action( 'wp_ajax_meoCrmRealeastateDeleteFloorLot', 'meoCrmRealeastateDeleteFloorLot' );
function meoCrmRealeastateDeleteFloorLot()
{ 
    // Recuperation des variables POST
    $nextStep = true;
    $lot_id = (isset($_POST['lot_id']) && !empty($_POST['lot_id'])) ? $_POST['lot_id'] : 0;
    $floor_id = (isset($_POST['floor_id']) && !empty($_POST['floor_id'])) ? $_POST['floor_id'] : 0;
    $project_id = (isset($_POST['project']) && !empty($_POST['project'])) ? $_POST['project'] : 0;
    $url_site = (isset($_POST['url_site']) && !empty($_POST['url_site'])) ? $_POST['url_site'] : '';
    
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);

    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);

    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    $floor_lot = FloorLotModel::getFloorLotByLotIdAndFloorId($external_wpdb,$lot_id,$floor_id);
    
    if(!empty($floor_lot->plan_floor))
    {
        if(!ftp_delete($ftp, $url_site.'/wp/wp-content/uploads/'.$floor_lot->plan_floor))
        {
            $nextStep = false;
        }
    }
    
    if($nextStep)
    {
        $where = array(
            'floor_id' => $floor_id,
            'lot_id' => $lot_id
        );
        $resultDeleteSQL = FloorLotModel::deleteFloorLot($external_wpdb,$where);
        if($resultDeleteSQL['success'])
        {
            echo 'success';
        }else{
            echo 'erreur delete SQL';
        }
    }else{
        echo 'erreur delete image';
    }
    
    die();
    
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateLotInsertFloor', 'meoCrmRealeastateLotInsertFloor' );  
add_action( 'wp_ajax_meoCrmRealeastateLotInsertFloor', 'meoCrmRealeastateLotInsertFloor' );
function meoCrmRealeastateLotInsertFloor()
{    
    // Recuperation des variables POST
    $lot_id = (isset($_POST['lot_id']) && !empty($_POST['lot_id'])) ? $_POST['lot_id'] : 0;
    $floor_id = (isset($_POST['floor_id']) && !empty($_POST['floor_id'])) ? $_POST['floor_id'] : 0;
    $project_id = (isset($_POST['project_id']) && !empty($_POST['project_id'])) ? $_POST['project_id'] : 0;
    $floor_entry = (isset($_POST['lot_floor_entry']) && !empty($_POST['lot_floor_entry'])) ? $_POST['lot_floor_entry'] : 0;   
    $id_lot_plan_building = (isset($_POST['lot_plan_building']) && !empty($_POST['lot_plan_building'])) ? $_POST['lot_plan_building'] : 0;  
    
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    $project = meo_crm_projects_getProjectInfo($project_id);
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);

    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);

    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Enregistrement etage du lot
    if(!empty($lot_id) && !empty($floor_id))
    {
        $datas_floor_lot = array();
        $url_site = $_POST['url_site'];
        $datas_floor_lot['floor_id'] = $floor_id;
        $datas_floor_lot['lot_id'] = $lot_id;
        $datas_floor_lot['status_plan_floor_id'] = $id_lot_plan_building;
        $datas_floor_lot['lot_entry'] = $floor_entry;
        $resultSQL = FloorLotModel::insertFloorLot($external_wpdb, $datas_floor_lot);
        if($resultSQL['success'])
        {
            
            $content_dir_path = $baseDirFTP.'uploads/';
            
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, 'floor-lot'))
            {
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.'floor-lot/', $resultSQL['id']))
                {
                    $folder_created = true;
                }
            }
            
            $content_dir_url = 'floor-lot/'.$resultSQL['id'].'/';     
            $content_dir_path = $baseDirFTP.'uploads/floor-lot/'.$resultSQL['id'].'/';            
            
            if($folder_created)
            {
                // ************************************************************
                // Upload plan
                // ************************************************************
                if(!empty($_FILES['lot_floor_position']['name']))
                {
                    $dataUpdateFloorLot = array();
                    $tmp_file = $_FILES['lot_floor_position']['tmp_name'];
                    $type_file = $_FILES['lot_floor_position']['type'];
                    $name_file = $_FILES['lot_floor_position']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['lot_floor_position']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $whereUpdateFloorLot = array(
                                'id' => $resultSQL['id']
                            );
                            $dataUpdateFloorLot['plan_floor'] = $content_dir_url.$_FILES['lot_floor_position']['name'];
                            $resultUpdateSQL = FloorLotModel::updateFloorLot($external_wpdb, $dataUpdateFloorLot, $whereUpdateFloorLot);
                            if(!$resultUpdateSQL['success'])
                            {
                                echo 'erreur update';
                                echo '<pre>';
                                print_r($dataUpdateFloorLot);
                                echo '</pre>';
                            }
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier !!';
                    }
                }
            }
            
            $html = '';
            $floors_lot = FloorLotModel::getFloorLotByLotId($external_wpdb,$lot_id);              
            foreach($floors_lot as $floor_lot)
            {                
                $lot_entry = ($floor_lot->lot_entry) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>';
                $html .= '  <tr>
                                <td>'.$floor_lot->floor_title.'</td>
                                <td><img src="'.$url_site.'/wp/wp-content/uploads/'.$floor_lot->plan_floor.'" alt="" width="200" /></td>
                                <td>'.$lot_entry.'</td>
                                <td>'.$floor_lot->coordinate_title.'</td>
                                <td><button onclick="deleteFloorLot(this)" class="btn-remove-floor-lot" data-lot-id="'.$floor_lot->lot_id.'" data-floor-id="'.$floor_lot->floor_id.'"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                            </tr>';
            }
            echo $html;            
        }
    }    
    die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateAddPriceListObject', 'meoCrmRealeastateAddPriceListObject' );
add_action( 'wp_ajax_meoCrmRealeastateAddPriceListObject', 'meoCrmRealeastateAddPriceListObject' );
function meoCrmRealeastateAddPriceListObject()
{
    $project_id = $_POST['project_id'];
    $project = meo_crm_projects_getProjectInfo($project_id);
    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

    $table = getExternalPrefix().MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES;

    $data = array();
    $data['label'] = $_POST['label'];
    $data['price'] = $_POST['price'];

    if(!empty($data['label']) && !empty($data['price']))
    {
        if($external_wpdb->insert($table,$data))
        {
            echo true;
            wp_die();
        }
    }

    echo false;
    wp_die();
}

add_action( 'wp_ajax_nopriv_meoCrmRealeastateRemovePriceListObject', 'meoCrmRealeastateRemovePriceListObject' );
add_action( 'wp_ajax_meoCrmRealeastateRemovePriceListObject', 'meoCrmRealeastateRemovePriceListObject' );
function meoCrmRealeastateRemovePriceListObject()
{
    $project_id = $_POST['project_id'];
    $project = meo_crm_projects_getProjectInfo($project_id);
    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

    $table = getExternalPrefix().MEO_CRM_REALESTATE_PRICE_LIST_OTHER_PRICES;
    $where = array('id' => $_POST['id']);

    if(!empty($_POST['id']))
    {
        if($external_wpdb->delete($table,$where))
        {
            echo true;
            wp_die();
        }
    }

    echo false;
    wp_die();
}