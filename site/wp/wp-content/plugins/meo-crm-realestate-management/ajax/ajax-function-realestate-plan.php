<?php 

/*
 *  Show Plan List
 */
add_action( 'wp_ajax_nopriv_getPlanList', 'getPlanList' );  
add_action( 'wp_ajax_getPlanList', 'getPlanList' );
function getPlanList()
{
    // INIT VARIABLE
    $html = '';    
    global $wpdb;
    $check = true;
    $datas = array();
    $project_id = $_POST['project_id'];
    $charset_collate = $wpdb->get_charset_collate();
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    $results = PlanModel::getAllPlan($external_wpdb);
    
    foreach($results as $result)
    {
        $datas[$result->id]['id'] = $result->id;
        $datas[$result->id]['title'] = $result->title;
        $datas[$result->id]['description'] = $result->description;
        $datas[$result->id]['document'] = $result->document;
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'title', 'name' => 'title', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'description', 'type_data' => 'base', 'class' => ''),
        array('key' => 'document', 'name' => 'document', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('click_action' => 'editPlan(@id)', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('click_action' => 'deletePlan(@id)', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );    
    
    echo $helperList->getList($datas, $header, $list_action);
    
    die();
}

/*
 *  Get list spinner for select list form plan
 */
add_action( 'wp_ajax_nopriv_getSpinnerList', 'getSpinnerList' );  
add_action( 'wp_ajax_getSpinnerList', 'getSpinnerList' );
function getSpinnerList()
{
    // INIT VARIABLE
    $html = '<option value="0">Choose a spinner</option>';    
    $spinner_id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Get List Spinner
    $spinners = SpinnerImmoModel::getAllSpinner($external_wpdb);
    
    // Create html for select list
    foreach($spinners as $spinner)
    {
        $selected = ($spinner_id == $spinner->id) ? 'selected=true' : '';
        $html .= '<option value="'.$spinner->id.'" '.$selected.'>'.$spinner->title.'</option>';
    }
    
    echo $html;
    
    die();
}

/*
 *  Create and update Plan
 */
add_action( 'wp_ajax_nopriv_savePlan', 'savePlan' );  
add_action( 'wp_ajax_savePlan', 'savePlan' );
function savePlan()
{
    // Init From Value
    $id = $_POST['plan_id'];
    $spinner_id = $_POST['plan_spinner'];
    $title = $_POST['plan_title'];
    $description = $_POST['plan_description'];
    $document = $_POST['plan_document'];
    $image_2d = $_POST['plan_image_2d'];
    $image_3d = $_POST['plan_image_3d'];
    $project_id = $_POST['project_id'];
    
    // Init variable
    $check = true;
    $datas = array();
    $errors = array();  
    
    // DB Request
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Base URL for upload images
    $upload_path = $baseDirFTP.'uploads/';
    $base_url = $project['url'].'wp-content/uploads';
    $base_path = $upload_path.'plans/';
    
    if(!empty($title) && !empty($description))
    {    
        if(isset($id) && !empty($id))
        {
            // Init Variables
            $count = 0;
            $files = array();
            $uploads = array();
            $deletes = array();
            $content_dir_url = 'plans/'.$id.'/';
            $content_dir_path = $baseDirFTP.'uploads/plans/'.$id.'/';
            
            // Update current plan            
            $datas['spinner_id'] = $spinner_id;
            $datas['title'] = $title;
            $datas['description'] = $description;
                
            $files['document']['old'] = $_POST['input_plan_document'];
            $files['plan_2d']['old'] = $_POST['input_plan_image_2d'];
            $files['plan_3d']['old'] = $_POST['input_plan_image_3d'];

            $files['document']['new'] = $_FILES['plan_document'];
            $files['plan_2d']['new'] = $_FILES['plan_image_2d'];
            $files['plan_3d']['new'] = $_FILES['plan_image_3d'];
            
            foreach($files as $key => $file)
            {
                if(!empty($file['new']['name']))
                {
                    list($path,$filename) = explode('/'.$id.'/', $file['old']);
                    
                    $deletes[] = $content_dir_path.$filename;
                    
                    $uploads[$count]['file'] = $content_dir_path.$file['new']['name'];
                    $uploads[$count]['tmp_file'] = $file['new']['tmp_name'];
                    $uploads[$count]['type'] = $file['new']['type'];
                    
                    $datas[$key] = $content_dir_url.$file['new']['name'];
                }else{
                    $datas[$key] = $file['old'];
                }
                $count++;
            }
            
            $where = array('id' => $id);
            
            $result = PlanModel::updatePlan($external_wpdb, $datas, $where);
            
            if($result['success'])
            {
                if(is_array($deletes) && !empty($deletes))
                {
                    foreach($deletes as $delete)
                    {
                        if(!ftp_delete($ftp, $delete))
                        {
                            $errors[] = 'ERROR : '.$delete.' not deleted !!';
                            $check = false;
                        }
                    }
                }
                
                if(is_array($uploads) && !empty($uploads))
                {
                    foreach($uploads as $upload)
                    {
                        if(is_array($upload) && !empty($upload))
                        {
                            if(!is_uploaded_file($upload['tmp_file']))
                            {
                                $errors[] = 'ERROR : file not uploaded !!';
                                $check = false;
                            }
                            if(!strstr($upload['type'], 'jpeg') && !strstr($upload['type'], 'jpg') && !strstr($upload['type'], 'png') && !strstr($upload['type'], 'pdf'))
                            {
                                $check = false;
                                $errors[] = $key.' isn\'t a image or PDF(for plane document) file';
                            }
                            if(ftp_put($ftp, $upload['file'], $upload['tmp_file'], FTP_BINARY) === false )
                            {
                                $errors[] = $key.' is not upload on the external server';
                                $check = false;
                            }
                        }
                    }
                }
            }else{
                $check = false;
            }
            
            if($check)
            {
                $check = true;
                $message = $helperCore->getFlashMessageAdmin('Plan update success', 1);
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Plan update error', 0);
            }
            
        }else{
            
            // Insert new plan
            $datas['spinner_id'] = $spinner_id;
            $datas['title'] = $title;
            $datas['description'] = $description;
            
            $result = PlanModel::insertPlan($external_wpdb, $datas);
            
            if($result['success'])
            {
             
                // Get insert id
                $id = $result['id'];
                
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path, 'plans'))
                {
                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $upload_path.'plans/', $id))
                    {
                        $folder_created = true;
                    }
                }
                
                if($folder_created)
                {
                    // Init variables
                    $datas = array();
                    $content_dir_url = 'plans/'.$id.'/';
                    $content_dir_path = $baseDirFTP.'uploads/plans/'.$id.'/';
                    
                    // Save Url Images
                    $datas['document'] = $content_dir_url.$_FILES['plan_document']['name'];
                    $datas['plan_2d'] = $content_dir_url.$_FILES['plan_image_2d']['name'];
                    $datas['plan_3d'] = $content_dir_url.$_FILES['plan_image_3d']['name'];  
                    
                    $where = array('id' => $id);
                    
                    $result = PlanModel::updatePlan($external_wpdb, $datas, $where);
                    
                    if($result['success'])
                    {
                        $files['document'] = $_FILES['plan_document'];
                        $files['images_2d'] = $_FILES['plan_image_2d'];
                        $files['images_3d'] = $_FILES['plan_image_3d'];
                        
                        // Check tmp file is uploaded
                        foreach($files as $key => $file)
                        {
                            if(!is_uploaded_file($file['tmp_name']))
                            {
                                $check = false;
                                $errors[] = $key.' isn\'t uploaded';
                            }
                            
                            if($key == 'document')
                            {
                                if(!strstr($file['type'], 'pdf'))
                                {
                                    $check = false;
                                    $errors[] = $key.' isn\'t a pdf file';
                                }
                            }else{
                                if(!strstr($file['type'], 'jpeg') && !strstr($file['type'], 'jpg') && !strstr($file['type'], 'png') && !strstr($file['type'], 'gif'))
                                {
                                    $check = false;
                                    $errors[] = $key.' isn\'t a image file';
                                }
                            }
                            
                            if($check)
                            {
                                if( ftp_put($ftp, $content_dir_path.$file['name'], $file['tmp_name'], FTP_BINARY) === false )
                                {
                                    $errors[] = $key.' is not upload on the external server';
                                    $check = false;
                                }
                            }                            
                        }              
                    }else{
                        $errors[] = 'Image can not update DB';
                        $check = false;
                    }  
                    
                    if(!$check)
                    {
                        $errors[] = 'Image can not upload';
                        $check = false;
                    }
                    
                }else{
                    $check = false;
                }
                
            }else{
                $check = false;
            }
            
            if($check)
            {
                $check = true;
                $message = $helperCore->getFlashMessageAdmin('Plan create success', 1);
                $id = $result['id'];
            }else{
                $check = false;
                $message = $helperCore->getFlashMessageAdmin('Plan create error', 0);
                $id = 0;
            }
        }
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Renseigner titre ou description', 0);
        $id = 0;
    }
    
    $result = array(
        'success' => $check,
        'message' => $message,
        'id' => $id,
        'document' => $datas['document'],
        'plan_2d' => $datas['plan_2d'],
        'plan_3d' => $datas['plan_3d']
    );
    
    echo json_encode($result);
    
    die();
}

// Get Sector entry by id
add_action( 'wp_ajax_nopriv_getPlanById', 'getPlanById' );  
add_action( 'wp_ajax_getPlanById', 'getPlanById' );
function getPlanById()
{
    // Init Variables
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // REQUEST DATABASE
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    $project = meo_crm_projects_getProjectInfo($project_id);
    
    // INIT CLASS
    $helperList = new MeoCrmCoreListHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // INIT VARIABLE TO CONNECT TO EXTERNAL DB
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // CONNECT TO THE EXTERNAL DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    $plan = PlanModel::getPlanById($external_wpdb,$id);
    
    echo json_encode($plan);
    
    die();
}

/*
 * 
 */
add_action( 'wp_ajax_nopriv_deletePlan', 'deletePlan' );  
add_action( 'wp_ajax_deletePlan', 'deletePlan' );
function deletePlan()
{
    // Init variable
    $check = true;
    
    // Get post request datas
    $id = $_POST['id'];
    $project_id = $_POST['project_id'];
    
    // Get data in DB
    $project = meo_crm_projects_getProjectInfo($project_id);
    $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
    
    // Init Class
    $helperCore = new MeoCrmCoreHelper();
    $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);
    
    // Init variable to connect to external db
    $hostDB = $meoCrmCoreCryptData->decode($access->host_db);
    $nameDB = $meoCrmCoreCryptData->decode($access->name_db);
    $loginDB = $meoCrmCoreCryptData->decode($access->login_db);
    $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
    
    // Init variable to connect to FTP
    $hostFTP = $meoCrmCoreCryptData->decode($access->host_ftp);
    $baseDirFTP = $meoCrmCoreCryptData->decode($access->base_dir_ftp);
    $loginFTP = $meoCrmCoreCryptData->decode($access->login_ftp);
    $passwordFTP = $meoCrmCoreCryptData->decode($access->password_ftp);
    
    // Create a new WPDB link to the other DB
    $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);
    
    // Create a connection with external FTP
    $ftp = ftp_connect($hostFTP, 21);
    ftp_login($ftp, $loginFTP, $passwordFTP);
    
    // Base URL for upload images
    $upload_path = $baseDirFTP.'uploads/';
    $base_url = $project['url'].'wp-content/uploads';
    $base_path = $upload_path.'plans/'.$id.'/';
    
    // Get Plan For image link
    $result = PlanModel::getPlanById($external_wpdb, $id);
    
    // Create array with all file want delete
    $files = array();
    list($url, $files['document']) = explode('/'.$id.'/',$result->document);
    list($url, $files['plan_2d']) = explode('/'.$id.'/',$result->plan_2d);
    list($url, $files['plan_3d']) = explode('/'.$id.'/',$result->plan_3d);
    
    foreach($files as $file)
    {
        if(file_exists($base_path.$file))
        {
            if(!ftp_delete($ftp, $base_path.$file))
            {
                $errors[] = 'ERROR : '.$file.' not deleted !!';
                $check = false;
            }
        }
    }
    
    if($check)
    {
        // Delete Plan by id
        $where = array( 'id' => $id );      
        $result = PlanModel::deletePlan($external_wpdb,$where);
        $check = $result['success'];
    }
    
    // Check if all steps are OK for message return 
    if($check)
    {
        $check = true;
        $message = $helperCore->getFlashMessageAdmin('Plan delete success', 1);
        
    }else{
        $check = false;
        $message = $helperCore->getFlashMessageAdmin('Plan delete error', 0);
    }
    
    $result = array(
        'success' => $check,
        'message' => $message
    );
    
    echo json_encode($result);
    
    die();
}