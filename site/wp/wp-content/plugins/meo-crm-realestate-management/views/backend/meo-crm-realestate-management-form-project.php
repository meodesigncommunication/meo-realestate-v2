<?php 

// Check if we already have a record to update, but check project id first
if(isset($_GET['id']) && !empty($_GET['id'])){
    
    $data = array();

    // Get project datas
    $project = meo_crm_projects_getProjectInfo($_GET['id']);
    $site_url = $project['url'];
    // TODO::Test if project url exists?

    // Get access project
    $results = AccessProjectModel::selectAccessProjectByProjectId($_GET['id']);

    // Decrypt access datas 
    $MeoCrmCoreCryptData = new MeoCrmCoreCryptData($site_url);  
    $data['id'] = (isset($results->id) && !empty($results->id)) ? $results->id : 0;
    $data['host_ftp'] = (isset($results->host_ftp) && !empty($results->host_ftp)) ? $MeoCrmCoreCryptData->decode($results->host_ftp) : '';
    $data['login_ftp'] = (isset($results->login_ftp) && !empty($results->login_ftp)) ? $MeoCrmCoreCryptData->decode($results->login_ftp) : '';
    $data['password_ftp'] = (isset($results->password_ftp) && !empty($results->password_ftp)) ? $MeoCrmCoreCryptData->decode($results->password_ftp) : '';
    $data['base_dir_ftp'] = (isset($results->base_dir_ftp) && !empty($results->base_dir_ftp)) ? $MeoCrmCoreCryptData->decode($results->base_dir_ftp) : '';
    $data['host_db'] = (isset($results->host_db) && !empty($results->host_db)) ? $MeoCrmCoreCryptData->decode($results->host_db) : '';
    $data['name_db'] = (isset($results->name_db) && !empty($results->name_db)) ? $MeoCrmCoreCryptData->decode($results->name_db) : '';
    $data['login_db'] = (isset($results->login_db) && !empty($results->login_db)) ? $MeoCrmCoreCryptData->decode($results->login_db) : '';
    $data['password_db'] = (isset($results->password_db) && !empty($results->password_db)) ? $MeoCrmCoreCryptData->decode($results->password_db) : '';
}else{
    
    // Return value set before post form (if don't insert data) 
    $data['host_ftp'] = $_POST['host_ftp'];
    $data['login_ftp'] = $_POST['login_ftp'];
    $data['password_ftp'] = $_POST['password_ftp'];
    $data['base_dir_ftp'] = $_POST['base_dir_ftp'];
    $data['host_db'] = $_POST['host_db'];
    $data['name_db'] = $_POST['name_db'];
    $data['login_db'] = $_POST['login_db'];
    $data['password_db'] = $_POST['password_db'];
    
}

?>
<h2>Donn&eacute;es FTP Site du projet</h2>
<input type="hidden" id="access_project_id" name="access_project_id" value="<?php echo (isset($data['id']) && !empty($data['id']))? $data['id'] : ''; ?>">
<table class="form-table meo-form-table">
    <tbody>
        <tr class="form-field">
            <th><label>Host FTP</label></th>
            <td><input type="text" id="host_ftp" name="host_ftp" value="<?php echo (isset($data['host_ftp']) && !empty($data['host_ftp']))? $data['host_ftp'] : ''; ?>" placeholder=""></td>
        </tr>
        <tr class="form-field">
            <th><label>Login FTP</label></th>
            <td><input type="text" id="login_ftp" name="login_ftp" value="<?php echo (isset($data['login_ftp']) && !empty($data['login_ftp']))? $data['login_ftp'] : ''; ?>" placeholder=""></td>
        </tr>
        <tr class="form-field">
            <th><label>Mot de passe FTP</label></th>
            <td><input type="password" id="password_ftp" name="password_ftp" value="<?php echo (isset($data['password_ftp']) && !empty($data['password_ftp']))? $data['password_ftp'] : ''; ?>" placeholder=""></td>
        </tr>
        <tr class="form-field">
            <th><label>Chemin de base FTP</label></th>
            <td><input type="text" id="base_dir_ftp" name="base_dir_ftp" value="<?php echo (isset($data['base_dir_ftp']) && !empty($data['base_dir_ftp']))? $data['base_dir_ftp'] : ''; ?>" placeholder=""></td>
        </tr>
    </tbody>
</table>
<br/><br/>
<h2>Infos sur la base de donn&eacute;es</h2>
<table class="form-table meo-form-table">
    <tbody>
        <tr class="form-field">
            <th><label>Host DB</label></th>
            <td><input type="text" id="host_db" name="host_db" value="<?php echo (isset($data['host_db']) && !empty($data['host_db']))? $data['host_db'] : ''; ?>" placeholder=""></td>
        </tr>
        <tr class="form-field">
            <th><label>Nom DB</label></th>
            <td><input type="text" id="name_db" name="name_db" value="<?php echo (isset($data['name_db']) && !empty($data['name_db']))? $data['name_db'] : ''; ?>" placeholder=""></td>
        </tr>
        <tr class="form-field">
            <th><label>Login DB</label></th>
            <td><input type="text" id="login_db" name="login_db" value="<?php echo (isset($data['login_db']) && !empty($data['login_db']))? $data['login_db'] : ''; ?>" placeholder=""></td>
        </tr>
        <tr class="form-field">
            <th><label>Mot de passe DB</label></th>
            <td><input type="password" id="password_db" name="password_db" value="<?php echo (isset($data['password_db']) && !empty($data['password_db']))? $data['password_db'] : ''; ?>" placeholder=""></td>
        </tr>
    </tbody>
</table>
<table class="form-table meo-form-table">
    <tbody>
        <tr class="form-field">
            <th></th>
            <td><button type="button" name="create_external_table" id="create_external_table">Create externale table for realestate</button></td>
        </tr>
    </tbody>
</table>