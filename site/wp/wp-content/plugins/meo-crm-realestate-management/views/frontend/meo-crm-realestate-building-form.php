<?php
global $current_user;
$check_data = true;
$errors = array();
$input_class_title = '';
$input_class_desc = '';
$building_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = (isset($_GET['project'])) ? $_GET['project'] : 0;

$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

$sectors = MeoCrmRealestateManagement::getSectors($external_wpdb);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $folder_created = false;
    
    if(!empty($building_id))
    {
        
        $datas = array();
        $content_dir_path = $baseDirFTP.'uploads/';
        
        if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_BUILDING))
        {
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_BUILDING.'/', $building_id))
            {
                $folder_created = true;
            }
        }

        $url_length = strlen($project['url']);
        $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

        $content_dir_url = FOLDER_BUILDING.'/'.$building_id.'/';
        $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_BUILDING.'/'.$building_id.'/';

        if($folder_created)
        {            
            // ************************************************************
            // Upload images b&acirc;timent 3D
            // ************************************************************
            if(!empty($_FILES['image_hover_plan_sector_3d']['name']))
            {
                $nextStep = true;      
                if(!empty($old_image_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_3d))
                    {
                        $errors[] = 'Ancienne image 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_hover_plan_sector_3d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_sector_3d']['type'];
                    $name_file = $_FILES['image_hover_plan_sector_3d']['name']; 

                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_sector_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_sector_3d'] = $content_dir_url.$_FILES['image_hover_plan_sector_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
            }
            // ************************************************************
            // Upload images b&acirc;timent 2D
            // ************************************************************
            if(!empty($_FILES['image_hover_plan_sector_2d']['name']))
            {
                $nextStep = true;      
                if(!empty($old_image_2d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_2d))
                    {
                        $errors[] = 'Ancienne image 2D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_hover_plan_sector_2d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_sector_2d']['type'];
                    $name_file = $_FILES['image_hover_plan_sector_2d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_sector_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_sector_2d'] = $content_dir_url.$_FILES['image_hover_plan_sector_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }
                }
            }
            // ************************************************************
            // Upload images b&acirc;timent Mobile
            // ************************************************************
            if(!empty($_FILES['image_hover_plan_sector_mobile']['name']))
            {
                $nextStep = true;      
                if(!empty($old_image_mobile))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_mobile))
                    {
                        $errors[] = 'Ancienne image mobile pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_hover_plan_sector_mobile']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_sector_mobile']['type'];
                    $name_file = $_FILES['image_hover_plan_sector_mobile']['name']; 
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_sector_mobile']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_sector_mobile'] = $content_dir_url.$_FILES['image_hover_plan_sector_mobile']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
            }
            // ************************************************************
            // Upload images hover sector 2D
            // ************************************************************
            if(!empty($_FILES['image_front_face']['name']))
            {
                $nextStep = true; 
                if(!empty($old_image_hover_plan_sector_2d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_hover_plan_sector_2d))
                    {
                        $errors[] = 'Ancienne image hover 2D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_front_face']['tmp_name'];
                    $type_file = $_FILES['image_front_face']['type'];
                    $name_file = $_FILES['image_front_face']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_front_face']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_front_face'] = $content_dir_url.$_FILES['image_front_face']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }
                }
            }
            // ************************************************************
            // Upload images hover sector 2D
            // ************************************************************
            if(!empty($_FILES['image_back_face']['name']))
            {
                $nextStep = true;              
                if(!empty($old_image_hover_plan_sector_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_hover_plan_sector_3d))
                    {
                        $errors[] = 'Ancienne image hover 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_back_face']['tmp_name'];
                    $type_file = $_FILES['image_back_face']['type'];
                    $name_file = $_FILES['image_back_face']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_back_face']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_back_face'] = $content_dir_url.$_FILES['image_back_face']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
            }
            // ************************************************************
            // Upload images hover sector 2D
            // ************************************************************
            if(!empty($_FILES['image_preview']['name']))
            {
                $nextStep = true;              
                if(!empty($old_image_hover_plan_sector_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_hover_plan_sector_3d))
                    {
                        $errors[] = 'Ancienne image hover 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_preview']['tmp_name'];
                    $type_file = $_FILES['image_preview']['type'];
                    $name_file = $_FILES['image_preview']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_preview']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_preview'] = $content_dir_url.$_FILES['image_preview']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
            }
            // ************************************************************
            // Upload images hover sector 2D
            // ************************************************************
            if(!empty($_FILES['image_building_sector']['name']))
            {
                $nextStep = true;              
                if(!empty($old_image_hover_plan_sector_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_hover_plan_sector_3d))
                    {
                        $errors[] = 'Ancienne image hover 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_building_sector']['tmp_name'];
                    $type_file = $_FILES['image_building_sector']['type'];
                    $name_file = $_FILES['image_building_sector']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_building_sector']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_building_sector'] = $content_dir_url.$_FILES['image_building_sector']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
            }
                        
            if(empty($_POST['sector_id'])){ $check_data = false; $input_class_sector_id = 'warningField'; }
            if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
            if(empty($_POST['description'])){ $check_data = false; $input_class_desc = 'warningField'; }        
            
            if($check_data)
            {
                $datas['sector_id'] = $_POST['sector_id'];
                $datas['title'] = $_POST['title'];
                $datas['description'] = $_POST['description'];        
                $datas['park_place_indoor'] = $_POST['park_place_indoor'];        
                $datas['park_place_outdoor'] = $_POST['park_place_outdoor'];        
                $datas['coordinate_plan_sector_2d'] = $_POST['coordinate_plan_sector_2d'];
                $datas['coordinate_plan_sector_3d'] = $_POST['coordinate_plan_sector_3d'];
                $datas['coordinate_plan_sector_mobile'] = $_POST['coordinate_plan_sector_mobile'];

                if(!empty($datas))
                {
                    $where = array('id' => $building_id);
                    $result = MeoCrmRealestateManagement::updateBuilding($external_wpdb,$datas,$where);

                    if($result['success'])
                    {
                        header('Location:/meo-crm-realestate-form-building/?project='.$project_id.'&id='.$building_id);
                    }
                }
            }
            
        }
        
    }else{
       
        // Enregistrer les datas dans la DB
        $datas = array();
        $datas['sector_id'] = $_POST['sector_id'];
        $datas['title'] = $_POST['title'];
        $datas['description'] = $_POST['description'];        
        $datas['park_place_indoor'] = $_POST['park_place_indoor'];        
        $datas['park_place_outdoor'] = $_POST['park_place_outdoor'];        
        $datas['coordinate_plan_sector_2d'] = $_POST['coordinate_plan_sector_2d'];
        $datas['coordinate_plan_sector_3d'] = $_POST['coordinate_plan_sector_3d'];
        $datas['coordinate_plan_sector_mobile'] = $_POST['coordinate_plan_sector_mobile'];
        
        if(empty($datas['sector_id'])){ $check_data = false; $input_class_sector_id = 'warningField'; }
        if(empty($datas['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($datas['description'])){ $check_data = false; $input_class_desc = 'warningField'; }        
        
        if($check_data)
        {
            $resultInsert = MeoCrmRealestateManagement::insertBuilding($external_wpdb, $datas);
        }else{
            $resultInsert = array(
                'success' => 0
            );
        }
        
        if($resultInsert['success'])
        {
            $datas = array();
            
            $insert_id = $resultInsert['id'];
            
            $content_dir_path = $baseDirFTP.'uploads/';
            
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_BUILDING))
            {
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_BUILDING.'/', $insert_id))
                {
                    $folder_created = true;
                }
            }
            
            
            $url_length = strlen($project['url']);
            $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));
            
            $content_dir_url = FOLDER_BUILDING.'/'.$insert_id.'/';
            $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_BUILDING.'/'.$insert_id.'/';
            
            if($folder_created)
            {
                // ************************************************************
                // Upload images b&acirc;timent 3D
                // ************************************************************
                if(!empty($_FILES['image_hover_plan_sector_3d']['name']))
                {
                    $tmp_file = $_FILES['image_hover_plan_sector_3d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_sector_3d']['type'];
                    $name_file = $_FILES['image_hover_plan_sector_3d']['name']; 
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_sector_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_sector_3d'] = $content_dir_url.$_FILES['image_hover_plan_sector_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier!!';
                        } 
                    }                   
                }
                // ************************************************************
                // Upload images b&acirc;timent 2D
                // ************************************************************
                if(!empty($_FILES['image_hover_plan_sector_2d']['name']))
                {
                    $tmp_file = $_FILES['image_hover_plan_sector_2d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_sector_2d']['type'];
                    $name_file = $_FILES['image_hover_plan_sector_2d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_sector_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_sector_2d'] = $content_dir_url.$_FILES['image_hover_plan_sector_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier!!';
                        } 
                    }
                }
                // ************************************************************
                // Upload images b&acirc;timent Mobile
                // ************************************************************
                if(!empty($_FILES['image_hover_plan_sector_mobile']['name']))
                {
                    $tmp_file = $_FILES['image_hover_plan_sector_mobile']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_sector_mobile']['type'];
                    $name_file = $_FILES['image_hover_plan_sector_mobile']['name']; 
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_sector_mobile']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_sector_mobile'] = $content_dir_url.$_FILES['image_hover_plan_sector_mobile']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }                   
                }
                // ************************************************************
                // Upload images hover sector 2D
                // ************************************************************
                if(!empty($_FILES['image_front_face']['name']))
                {
                    $tmp_file = $_FILES['image_front_face']['tmp_name'];
                    $type_file = $_FILES['image_front_face']['type'];
                    $name_file = $_FILES['image_front_face']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_front_face']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_front_face'] = $content_dir_url.$_FILES['image_front_face']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }
                }
                // ************************************************************
                // Upload images hover sector 2D
                // ************************************************************
                if(!empty($_FILES['image_back_face']['name']))
                {
                    $tmp_file = $_FILES['image_back_face']['tmp_name'];
                    $type_file = $_FILES['image_back_face']['type'];
                    $name_file = $_FILES['image_back_face']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_back_face']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_back_face'] = $content_dir_url.$_FILES['image_back_face']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
                // ************************************************************
                // Upload images hover sector 2D
                // ************************************************************
                if(!empty($_FILES['image_preview']['name']))
                {
                    $tmp_file = $_FILES['image_preview']['tmp_name'];
                    $type_file = $_FILES['image_preview']['type'];
                    $name_file = $_FILES['image_preview']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_preview']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_preview'] = $content_dir_url.$_FILES['image_preview']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
                // ************************************************************
                // Upload images hover sector 2D
                // ************************************************************
                if(!empty($_FILES['image_building_sector']['name']))
                {
                    $tmp_file = $_FILES['image_building_sector']['tmp_name'];
                    $type_file = $_FILES['image_building_sector']['type'];
                    $name_file = $_FILES['image_building_sector']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_building_sector']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_building_sector'] = $content_dir_url.$_FILES['image_building_sector']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }
                }
                
                if(!empty($datas))
                {
                    $where = array('id' => $insert_id);
                    $result = MeoCrmRealestateManagement::updateBuilding($external_wpdb,$datas,$where);
                }
                
                if(isset($insert_id) && !empty($insert_id))
                {
                    header('Location:/meo-crm-realestate-form-building/?project='.$project_id.'&id='.$insert_id);
                }
                
            }else{
                $errors[] = 'Erreur durant la création des dossiers !!';
            } 
            
        }else{
            $errors[] = 'Le titre ou la description ou les coordonn&eacute;e du b&acirc;timent ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        //MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form building " . print_r($errors));
    }    
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($building_id))
{
    $building = MeoCrmRealestateManagement::getBuildingById($external_wpdb, $building_id);
    $old_sector_id = (!empty($building[0]->sector_id)) ? $building[0]->sector_id : '';
    $old_title = (!empty($building[0]->title)) ? $building[0]->title : '';
    $old_description = (!empty($building[0]->description)) ? $building[0]->description : '';
    $old_park_place_indoor = (!empty($building[0]->park_place_indoor)) ? $building[0]->park_place_indoor : '';
    $old_park_place_outdoor = (!empty($building[0]->park_place_outdoor)) ? $building[0]->park_place_outdoor : '';
    $old_coordinate_plan_sector_3d = (!empty($building[0]->coordinate_plan_sector_3d)) ? $building[0]->coordinate_plan_sector_3d : '';
    $old_coordinate_plan_sector_2d = (!empty($building[0]->coordinate_plan_sector_2d)) ? $building[0]->coordinate_plan_sector_2d : '';
    $old_coordinate_plan_sector_mobile = (!empty($building[0]->coordinate_plan_sector_mobile)) ? $building[0]->coordinate_plan_sector_mobile : '';
    $old_image_3d = (!empty($building[0]->image_hover_plan_sector_3d)) ? $building[0]->image_hover_plan_sector_3d : '';
    $old_image_2d = (!empty($building[0]->image_hover_plan_sector_2d)) ? $building[0]->image_hover_plan_sector_2d : '';
    $old_image_mobile = (!empty($building[0]->image_hover_plan_sector_mobile)) ? $building[0]->image_hover_plan_sector_mobile : '';
    $old_image_front_face = (!empty($building[0]->image_front_face)) ? $building[0]->image_front_face : '';
    $old_image_back_face = (!empty($building[0]->image_back_face)) ? $building[0]->image_back_face : '';
    $old_image_preview = (!empty($building[0]->image_preview)) ? $building[0]->image_preview : '';
    $old_image_building_sector = (!empty($building[0]->image_building_sector)) ? $building[0]->image_building_sector : '';
    $floors = MeoCrmRealestateManagement::getFloorByBuildingId($external_wpdb, $building_id);
}else{
    $old_sector_id = 0;
    $old_title = '';
    $old_description = '';
    $old_park_place_indoor = '';
    $old_park_place_outdoor = '';
    $old_coordinate_plan_sector_3d = '';
    $old_coordinate_plan_sector_2d = '';    
    $old_coordinate_plan_sector_mobile = '';    
    $old_image_3d = '';
    $old_image_2d = '';
    $old_image_mobile = '';    
    $old_image_front_face  = '';
    $old_image_back_face  = '';
    $old_image_preview = '';
    $old_image_building_sector = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    
    $url_length = strlen($projects['url']);
    $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
?>
    <form method="post" name="add_building" id="form_sector" enctype="multipart/form-data">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <input type="hidden" name="building_id" value="<?php echo $building_id ?>" />
        <h2>B&acirc;timent</h2>
        
        <div class="input-group">
            <label>Secteur li&eacute;</label>
            <select name="sector_id" id="sector_id" class="<?php echo $input_class_sector_id; ?>">
                <option value="0">Choisir un secteur</option>
                <?php 
                    // METTRE LA LISTE DES DEVELOPPEMENT LIE AU PROJET
                    foreach($sectors as $sector)
                    {
                        $selected = (isset($old_sector_id) && !empty($old_sector_id)) ? 'selected' : '';
                        echo '<option value="'.$sector->id.'" '.$selected.'>'.$sector->title.'</option>';
                    }
                ?>
            </select>
        </div>
        
        <div class="input-group">
            <label>Titre du b&acirc;timent</label>
            <input type="text" name="title" id="sector_title" value="<?php echo $old_title ?>" placeholder="Titre du b&acirc;timent" class="<?php echo $input_class_title ?>" />
        </div>
        
        <div class="input-group">
            <label>Description du b&acirc;timent</label>
            <textarea type="text" name="description" id="sector_description" class="<?php echo $input_class_desc ?>"><?php echo $old_description ?></textarea>
        </div>
        
        <div class="input-group">
            <label>Place de parc int&eacute;rieur</label>
            <input type="text" name="park_place_indoor" id="park_place_indoor" value="<?php echo $old_park_place_indoor ?>" placeholder="Place de parc int&eacute;rieur" class="<?php //echo $input_class_park_place_indoor ?>" />
        </div>
        
        <div class="input-group">
            <label>Place de parc ext&eacute;rieur</label>
            <input type="text" name="park_place_outdoor" id="park_place_outdoor" value="<?php echo $old_park_place_outdoor ?>" placeholder="Place de parc ext&eacute;rieur" class="<?php //echo $input_class_park_place_outdoor ?>" />
        </div>
        
        <div class="input-group">
            <label>Coordonn&eacute;e plan secteur 3D</label>
            <input type="text" name="coordinate_plan_sector_3d" id="coordinate_plan_sector_3d" value="<?php echo $old_coordinate_plan_sector_3d ?>" placeholder="Coordonn&eacute;e plan de d&eacute;veloppement 3D" class="<?php //echo $input_class_coordonne_plan_sector_3d ?>" />
        </div>
        
        <div class="input-group">
            <label>Coordonn&eacute;e plan secteur 2D</label>
            <input type="text" name="coordinate_plan_sector_2d" id="coordinate_plan_sector_2d" value="<?php echo $old_coordinate_plan_sector_2d ?>" placeholder="Coordonn&eacute;e plan de d&eacute;veloppement 2D" class="<?php //echo $input_class_coordonne_plan_sector_2d ?>" />
        </div>
        
        <div class="input-group">
            <label>Coordonn&eacute;e plan secteur mobile</label>
            <input type="text" name="coordinate_plan_sector_mobile" id="coordinate_plan_sector_mobile" value="<?php echo $old_coordinate_plan_sector_mobile ?>" placeholder="Coordonn&eacute;e plan de secteur mobile" class="<?php //echo $input_class_coordinate_plan_sector_mobile ?>" />
        </div>
        
        <div class="input-group">
            <label>Plan du b&acirc;timent 3D</label>
            <input type="file" name="image_hover_plan_sector_3d" id="image_hover_plan_sector_3d" />
            <?php 
                if(!empty($old_image_3d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_3d.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Plan du b&acirc;timent 2D</label>
            <input type="file" name="image_hover_plan_sector_2d" id="image_hover_plan_sector_2d" />
            <?php 
                if(!empty($old_image_2d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_2d.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Plan du b&acirc;timent Mobile</label>
            <input type="file" name="image_hover_plan_sector_mobile" id="image_hover_plan_sector_mobile" />
            <?php 
                if(!empty($old_image_mobile))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_mobile.'" />';
                }
            ?>
        </div>
        
        <div class="row-group">
            <div class="input-group">
                <label>Face avant du b&acirc;timent</label>
                <input type="file" name="image_front_face" id="image_front_face" />
                <?php 
                    if(!empty($old_image_front_face))
                    {
                        echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_front_face.'" />';
                    }
                ?>
            </div>        
            <div class="input-group">
                <label>Face arri&egrave;re du b&acirc;timent</label>
                <input type="file" name="image_back_face" id="image_back_face" />
                <?php 
                    if(!empty($old_image_back_face))
                    {
                        echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_back_face.'" />';
                    }
                ?>
            </div>
        </div>
        
        <div class="input-group">
            <label>Pr&eacute;visualisation du b&acirc;timent</label>
            <input type="file" name="image_preview" id="image_preview" />
            <?php 
                if(!empty($old_image_preview))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_preview.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Plan du b&acirc;timent dans le secteur</label>
            <input type="file" name="image_building_sector" id="image_building_sector" />
            <?php 
                if(!empty($old_image_building_sector))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_building_sector.'" />';
                }
            ?>
        </div>
        
        <?php if(!empty($building_id)): ?>
            <div class="floor-container">
                <span>&Eacute;tages</span>
                <table id="sortable-floors" class="table table-striped">
                    <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="text" name="new_floor_title" id="new_floor_title" placeholder="Nom de l'&eacute;tage" /></td>
                            <td>
                                <button type="button" id="btn_submit_floor"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($floors) && !empty($floors)): ?>
                            <?php foreach($floors as $floor): ?>
                                <tr class="floor-exist">
                                    <td class="sortable-floor"><span><i class="fa fa-arrows" aria-hidden="true"></i></span></td>
                                    <td>
                                        <input type="hidden" name="floor_id" value="<?php echo $floor->id; ?>" />
                                        <input type="text" name="floor_title" placeholder="Nom de l'&eacute;tage" value="<?php echo $floor->title; ?>" />
                                    </td>
                                    <td class="floor_action">
                                        <button><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                        <button class="delete_floor"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
        
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-building-list/?project='.$project_id ?>" title=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
<?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?>

<script type="text/javascript">

    window.$ = jQuery;
    $(document).ready(function(){

        $('#sortable-floors tbody').sortable({
            axis: 'y',
            stop: function( ) {
                var list_floor = [];
                $('#sortable-floors tbody tr').each(function(key, value){
                    list_floor[key] = $(this).find('input[name="floor_id"]').val();
                });
                $.ajax({
                    url: '<?php echo admin_url('admin-ajax.php'); ?>',
                    type: "POST",
                    data: {
                        'action': 'meoCrmRealeastateSaveFloorBuildingOrder',
                        'project_id': $('input[name="project_id"]').val(),
                        'floors': list_floor
                    },
                    success: function(response)
                    {
                        console.log(response);
                    }
                });
            }
        });

        // Save a floor
        $('#btn_submit_floor').click(function(event){
            $.ajax({
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                type: "POST",
                data: {
                    'action': 'meoCrmRealeastateSaveFloorBuilding',
                    'building_id': $('input[name="building_id"]').val(),
                    'project_id': $('input[name="project_id"]').val(),
                    'floor_name': $('#new_floor_title').val()
                },
                success: function(response)
                {
                    $('#new_floor_title').val('');
                    location.reload();
                }
            });
        });

    });

</script>

<?php get_footer(); ?>