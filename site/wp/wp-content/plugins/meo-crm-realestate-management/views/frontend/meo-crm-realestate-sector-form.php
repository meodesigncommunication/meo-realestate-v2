<?php
global $current_user;
$check_data = true;
$errors = array();
$input_class_title = '';
$input_class_desc = '';
$sector_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
    
$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

$developpements = MeoCrmRealestateManagement::getDeveloppements($external_wpdb);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $folder_created = false;
    
    if(!empty($sector_id))
    {
        
        $datas = array();
        $content_dir_path = $baseDirFTP.'uploads/';
        
        if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_SECTOR))
        {
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_SECTOR.'/', $sector_id))
            {
                $folder_created = true;
            }
        }

        $url_length = strlen($project['url']);
        $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

        $content_dir_url = FOLDER_SECTOR.'/'.$sector_id.'/';
        $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_SECTOR.'/'.$sector_id.'/';

        if($folder_created)
        {            
            // ************************************************************
            // Upload images secteur 3D
            // ************************************************************
            if(!empty($_FILES['sector_image_3d']['name']))
            {
                $nextStep = true;      
                if(!empty($old_image_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_3d))
                    {
                        $errors[] = 'Ancienne image 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['sector_image_3d']['tmp_name'];
                    $type_file = $_FILES['sector_image_3d']['type'];
                    $name_file = $_FILES['sector_image_3d']['name']; 

                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['sector_image_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_sector_3d'] = $content_dir_url.$_FILES['sector_image_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }  
                }
            }
            // ************************************************************
            // Upload images secteur 2D
            // ************************************************************
            if(!empty($_FILES['sector_image_2d']['name']))
            {
                $nextStep = true;      
                if(!empty($old_image_2d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_2d))
                    {
                        $errors[] = 'Ancienne image 2D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['sector_image_2d']['tmp_name'];
                    $type_file = $_FILES['sector_image_2d']['type'];
                    $name_file = $_FILES['sector_image_2d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['sector_image_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_sector_2d'] = $content_dir_url.$_FILES['sector_image_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }
            }
            // ************************************************************
            // Upload images secteur Mobile
            // ************************************************************
            if(!empty($_FILES['sector_image_mobile']['name']))
            {
                $nextStep = true;      
                if(!empty($old_image_mobile))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_mobile))
                    {
                        $errors[] = 'Ancienne image mobile pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['sector_image_mobile']['tmp_name'];
                    $type_file = $_FILES['sector_image_mobile']['type'];
                    $name_file = $_FILES['sector_image_mobile']['name']; 
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['sector_image_mobile']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_sector_mobile'] = $content_dir_url.$_FILES['sector_image_mobile']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }  
                }
            }
            // ************************************************************
            // Upload images hover developpement 2D
            // ************************************************************
            if(!empty($_FILES['image_hover_plan_developpement_2d']['name']))
            {
                $nextStep = true; 
                if(!empty($old_image_hover_plan_developpement_2d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_hover_plan_developpement_2d))
                    {
                        $errors[] = 'Ancienne image hover 2D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_hover_plan_developpement_2d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_developpement_2d']['type'];
                    $name_file = $_FILES['image_hover_plan_developpement_2d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_developpement_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_developpement_2d'] = $content_dir_url.$_FILES['image_hover_plan_developpement_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }
            }
            // ************************************************************
            // Upload images hover developpement 2D
            // ************************************************************
            if(!empty($_FILES['image_hover_plan_developpement_3d']['name']))
            {
                $nextStep = true;              
                if(!empty($old_image_hover_plan_developpement_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_hover_plan_developpement_3d))
                    {
                        $errors[] = 'Ancienne image hover 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['image_hover_plan_developpement_3d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_developpement_3d']['type'];
                    $name_file = $_FILES['image_hover_plan_developpement_3d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_developpement_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_developpement_3d'] = $content_dir_url.$_FILES['image_hover_plan_developpement_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }
                }
            }
            // ************************************************************
                        
            if(empty($_POST['developpement_id'])){ $check_data = false; $input_class_developpement_id = 'warningField'; }
            if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
            if(empty($_POST['description'])){ $check_data = false; $input_class_desc = 'warningField'; }
            
            if($check_data)
            {
                $datas['developpement_id'] = $_POST['developpement_id'];
                $datas['title'] = $_POST['title'];
                $datas['description'] = $_POST['description'];        
                $datas['coordinate_plan_developpement_2d'] = $_POST['coordinate_plan_developpement_2d'];
                $datas['coordinate_plan_developpement_3d'] = $_POST['coordinate_plan_developpement_3d'];

                if(!empty($datas))
                {
                    $where = array('id' => $sector_id);
                    $result = MeoCrmRealestateManagement::updateSector($external_wpdb,$datas,$where);

                    if($result['success'])
                    {
                        header('Location:/meo-crm-realestate-form-sector/?project='.$project_id.'&id='.$sector_id);
                    }
                }
            }
            
        }
        
    }else{
       
        // Enregistrer les datas dans la DB
        $datas = array();
        $datas['developpement_id'] = $_POST['developpement_id'];
        $datas['title'] = $_POST['title'];
        $datas['description'] = $_POST['description'];        
        $datas['coordinate_plan_developpement_2d'] = $_POST['coordinate_plan_developpement_2d'];
        $datas['coordinate_plan_developpement_3d'] = $_POST['coordinate_plan_developpement_3d'];
        
        if(empty($datas['developpement_id'])){ $check_data = false; $input_class_developpement_id = 'warningField'; }
        if(empty($datas['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($datas['description'])){ $check_data = false; $input_class_desc = 'warningField'; }

        if($check_data)
        {
            $resultInsert = MeoCrmRealestateManagement::insertSector($external_wpdb, $datas);
        }else{
            $resultInsert = array(
                'success' => 0
            );
        }
        
        if($resultInsert['success'])
        {
            $datas = array();
            
            $insert_id = $resultInsert['id'];
            
            $content_dir_path = $baseDirFTP.'uploads/';
            
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_SECTOR))
            {
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_SECTOR.'/', $insert_id))
                {
                    $folder_created = true;
                }
            }
            
            
            $url_length = strlen($project['url']);
            $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));
            
            $content_dir_url = FOLDER_SECTOR.'/'.$insert_id.'/';
            $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_SECTOR.'/'.$insert_id.'/';

            if($folder_created)
            {
                // ************************************************************
                // Upload images secteur 3D
                // ************************************************************
                if(!empty($_FILES['sector_image_3d']['name']))
                {
                    $tmp_file = $_FILES['sector_image_3d']['tmp_name'];
                    $type_file = $_FILES['sector_image_3d']['type'];
                    $name_file = $_FILES['sector_image_3d']['name']; 
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['sector_image_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_sector_3d'] = $content_dir_url.$_FILES['sector_image_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }                    
                }
                // ************************************************************
                // Upload images secteur 2D
                // ************************************************************
                if(!empty($_FILES['sector_image_2d']['name']))
                {
                    $tmp_file = $_FILES['sector_image_2d']['tmp_name'];
                    $type_file = $_FILES['sector_image_2d']['type'];
                    $name_file = $_FILES['sector_image_2d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['sector_image_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_sector_2d'] = $content_dir_url.$_FILES['sector_image_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }
                // ************************************************************
                // Upload images secteur Mobile
                // ************************************************************
                if(!empty($_FILES['sector_image_mobile']['name']))
                {
                    $tmp_file = $_FILES['sector_image_mobile']['tmp_name'];
                    $type_file = $_FILES['sector_image_mobile']['type'];
                    $name_file = $_FILES['sector_image_mobile']['name']; 
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['sector_image_mobile']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_sector_mobile'] = $content_dir_url.$_FILES['sector_image_mobile']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }                    
                }
                // ************************************************************
                // Upload images hover developpement 2D
                // ************************************************************
                if(!empty($_FILES['image_hover_plan_developpement_2d']['name']))
                {
                    $tmp_file = $_FILES['image_hover_plan_developpement_2d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_developpement_2d']['type'];
                    $name_file = $_FILES['image_hover_plan_developpement_2d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_developpement_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_developpement_2d'] = $content_dir_url.$_FILES['image_hover_plan_developpement_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }
                // ************************************************************
                // Upload images hover developpement 2D
                // ************************************************************
                if(!empty($_FILES['image_hover_plan_developpement_3d']['name']))
                {
                    $tmp_file = $_FILES['image_hover_plan_developpement_3d']['tmp_name'];
                    $type_file = $_FILES['image_hover_plan_developpement_3d']['type'];
                    $name_file = $_FILES['image_hover_plan_developpement_3d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['image_hover_plan_developpement_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['image_hover_plan_developpement_3d'] = $content_dir_url.$_FILES['image_hover_plan_developpement_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }
                }
                
                if(!empty($datas))
                {
                    $where = array('id' => $insert_id);
                    $result = MeoCrmRealestateManagement::updateSector($external_wpdb,$datas,$where);
                }
                
                if(isset($insert_id) && !empty($insert_id))
                {
                    header('Location:/meo-crm-realestate-form-sector/?project='.$project_id.'&id='.$insert_id);
                }
                
            }else{
                $errors[] = 'Erreur durant la création des dossiers !!';
            } 
            
        }else{
            $errors[] = 'Le titre ou la description ou les coordonn&eacute;e du secteur ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }    
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($sector_id))
{
    $sector = MeoCrmRealestateManagement::getSectorById($external_wpdb, $sector_id);
    $old_developpement_id = (!empty($sector[0]->title)) ? $sector[0]->developpement_id : 0 ;
    $old_title = (!empty($sector[0]->title)) ? $sector[0]->title : '';
    $old_description = (!empty($sector[0]->description)) ? $sector[0]->description : '';
    $old_coordonne_plan_developpement_3d = (!empty($sector[0]->coordinate_plan_developpement_3d)) ? $sector[0]->coordinate_plan_developpement_3d : '';
    $old_coordonne_plan_developpement_2d = (!empty($sector[0]->coordinate_plan_developpement_2d)) ? $sector[0]->coordinate_plan_developpement_2d : '';
    $old_image_3d = (!empty($sector[0]->plan_sector_3d)) ? $sector[0]->plan_sector_3d : '';
    $old_image_2d = (!empty($sector[0]->plan_sector_2d)) ? $sector[0]->plan_sector_2d : '';
    $old_image_mobile = (!empty($sector[0]->plan_sector_mobile)) ? $sector[0]->plan_sector_mobile : '';
    $old_image_hover_plan_developpement_2d = (!empty($sector[0]->image_hover_plan_developpement_3d)) ? $sector[0]->image_hover_plan_developpement_3d : '';
    $old_image_hover_plan_developpement_3d = (!empty($sector[0]->image_hover_plan_developpement_2d)) ? $sector[0]->image_hover_plan_developpement_2d : '';
}else{
    $old_developpement_id = 0;
    $old_title = '';
    $old_description = '';
    $old_coordonne_plan_developpement_3d = '';
    $old_coordonne_plan_developpement_2d = '';    
    $old_image_3d = '';
    $old_image_2d = '';
    $old_image_mobile = '';    
    $old_image_hover_plan_developpement_2d = '';
    $old_image_hover_plan_developpement_3d = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    
    $url_length = strlen($projects['url']);
    $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
?>
    <form method="post" name="add_sector" id="form_developpement" enctype="multipart/form-data">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>Secteur</h2>
        
        <div class="input-group">
            <label>D&eacute;veloppement li&eacute;</label>
            <select name="developpement_id" id="developpement_id" class="<?php echo $input_class_developpement_id; ?>">
                <option value="0">Choisir un d&eacute;veloppement immobilier</option>
                <?php 
                    // METTRE LA LISTE DES DEVELOPPEMENT LIE AU PROJET
                    foreach($developpements as $developpement)
                    {
                        $selected = (isset($old_developpement_id) && !empty($old_developpement_id)) ? 'selected' : '';
                        echo '<option value="'.$developpement->id.'" '.$selected.'>'.$developpement->title.'</option>';
                    }
                ?>
            </select>
        </div>
        
        <div class="input-group">
            <label>Titre du secteur</label>
            <input type="text" name="title" id="sector_title" value="<?php echo $old_title ?>" placeholder="Titre du secteur" class="<?php echo $input_class_title ?>" />
        </div>
        
        <div class="input-group">
            <label>Description du secteur</label>
            <textarea type="text" name="description" id="sector_description" class="<?php echo $input_class_desc ?>"><?php echo $old_description ?></textarea>
        </div>
        
        <div class="input-group">
            <label>Coordonn&eacute;e plan d&eacute;veloppement 3D</label>
            <input type="text" name="coordinate_plan_developpement_3d" id="coordinate_plan_developpement_3d" value="<?php echo $old_coordonne_plan_developpement_3d ?>" placeholder="Coordonn&eacute;e plan de d&eacute;veloppement 3D" class="<?php //echo $input_class_coordonne_plan_developpement_3d ?>" />
        </div>
        
        <div class="input-group">
            <label>Coordonn&eacute;e plan d&eacute;veloppement 2D</label>
            <input type="text" name="coordinate_plan_developpement_2d" id="coordinate_plan_developpement_2d" value="<?php echo $old_coordonne_plan_developpement_2d ?>" placeholder="Coordonn&eacute;e plan de d&eacute;veloppement 2D" class="<?php //echo $input_class_coordonne_plan_developpement_2d ?>" />
        </div>
        
        <div class="input-group">
            <label>Plan de secteur 3D</label>
            <input type="file" name="sector_image_3d" id="sector_image_3d" />
            <?php 
                if(!empty($old_image_3d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_3d.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Plan de sector 2D</label>
            <input type="file" name="sector_image_2d" id="sector_image_2d" />
            <?php 
                if(!empty($old_image_2d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_2d.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Plan de secteur Mobile</label>
            <input type="file" name="sector_image_mobile" id="sector_image_mobile" />
            <?php 
                if(!empty($old_image_3d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_mobile.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Image hover developpement 3D</label>
            <input type="file" name="image_hover_plan_developpement_3d" id="image_hover_plan_developpement_3d" />
            <?php 
                if(!empty($old_image_hover_plan_developpement_3d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_hover_plan_developpement_3d.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Image hover developpement 2D</label>
            <input type="file" name="image_hover_plan_developpement_2d" id="image_hover_plan_developpement_2d" />
            <?php 
                if(!empty($old_image_hover_plan_developpement_2d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_hover_plan_developpement_2d.'" />';
                }
            ?>
        </div>
        
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-sector-list/?project='.$project_id ?>" title=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
<?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
    });
</script>
<?php get_footer(); ?>