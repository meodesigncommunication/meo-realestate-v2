<?php
global $current_user;
$data = array();
$project_id = $_GET['project'];
$delete_id = (isset($_GET['delete_id']) && !empty($_GET['delete_id'])) ? $_GET['delete_id'] : 0;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){

    $helperList = new MeoCrmCoreListHelper();

    $baseDirFTP = $projects['realestate']['ftp']['base_dir'];

    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($projects['realestate']['db']['login'],$projects['realestate']['db']['password'],$projects['realestate']['db']['name'],$projects['realestate']['db']['host']);

    $ftp = MeoCrmCoreHelper::getExternalFtpConnection($projects['realestate']['ftp']['login'], $projects['realestate']['ftp']['password'],$projects['realestate']['ftp']['host']);
    
    if(!empty($delete_id))
    {
        $nextStep = true;
        $url_length = strlen($projects['url']);
        $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
                
        $sector = MeoCrmRealestateManagement::getSectorById($external_wpdb, $delete_id);
        
        if(!empty($sector))
        {
            $image_3d = $baseDirFTP.'uploads/'.$sector->plan_sector_3d;
            $image_2d = $baseDirFTP.'uploads/'.$sector->plan_sector_2d;
            $image_mobile = $baseDirFTP.'uploads/'.$sector->plan_sector_mobile;
            $image_hover_2d = $baseDirFTP.'uploads/'.$sector->image_hover_plan_developpement_3d;
            $image_hover_3d = $baseDirFTP.'uploads/'.$sector->image_hover_plan_developpement_2d;
            
            if($image_2d != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $image_2d))
                {
                    $nextStep = false;
                }
            }
            if($image_3d != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $image_3d))
                {
                    $nextStep = false;
                }
            }
            if($image_mobile != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $image_mobile))
                {
                    $nextStep = false;
                }
            }
            if($image_hover_2d != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $image_hover_2d))
                {
                    $nextStep = false;
                }
            }
            if($image_hover_3d != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $image_hover_3d))
                {
                    $nextStep = false;
                }
            }
            
            if($nextStep)
            {
                $where = array('id' => $delete_id);
                $result = MeoCrmRealestateManagement::deleteSector($external_wpdb, $where);
                if(!$result['success'])
                {
                    echo 'erreur de suppression des données dans la DB !<br/>';
                }
            }  
        }else{
            echo 'Pas de développement avec cette ID<br/>';
        }
    }
    
    $results = MeoCrmRealestateManagement::getSectors($external_wpdb);
    if( isset($results) && !empty($results))
    {
        foreach($results as $result)
        {
            $developpement = MeoCrmRealestateManagement::getDeveloppementById($external_wpdb, $result->developpement_id);
            $data[$result->id]['id'] = $result->id;
            $data[$result->id]['developpement_id'] = $developpement[0]->title;
            $data[$result->id]['title'] = $result->title;
            $data[$result->id]['description'] = $result->description;
        }
    }

    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'developpement_id', 'name' => 'D&eacute;veloppement', 'type_data' => 'base', 'class' => ''),
        array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'Description', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('url_action' => site_url().'/meo-crm-realestate-form-sector?project='.$project_id.'&id=@id', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('url_action' => site_url().'/meo-crm-realestate-sector-list?project='.$project_id.'&delete_id=@id', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );
    
    ?>
    <div class="wrap">
        <h2 id="meo-crm-realestate-list-title">Secteur</h2>
        <div id="meo-crm-realestate-add">
            <a id="add-developpement" href="meo-crm-realestate-form-sector/?project=<?php echo $project_id ?>" title="Add Sector">Add Secteur</a>
            <a id="back-btn" href="meo-crm-realestate-management-tool/?project=<?php echo $project_id ?>" title="Back to menu">Back to menu</a>
        </div>
        <div class="clear-both"></div>
    </div>
    <?php echo $helperList->getList($data, $header, $list_action, false, 'id', true); ?>
<?php     
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
    });
</script>
<?php get_footer(); ?>