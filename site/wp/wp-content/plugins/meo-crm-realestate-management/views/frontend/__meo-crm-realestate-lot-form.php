<?php

global $current_user;
$check_data = true;
$errors = array();
$input_class_title = '';
$input_class_desc = '';
$lot_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
$url_ajax = get_site_url().'/wp-admin/admin-ajax.php';
    
$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);
$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

$url_length = strlen($project['url']);
$url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

$list_status = MeoCrmRealestateManagement::getStatus($external_wpdb);
$plans = MeoCrmRealestateManagement::getAllPlan($external_wpdb);
$documents = MeoCrmRealestateManagement::getAllDocumentPost($external_wpdb);
$metas = MeoCrmRealestateManagement::getAllMetaLot($external_wpdb);
$building_plans_floors = MeoCrmRealestateManagement::getAllBuildingFloorPlan($external_wpdb);

// IS METHOD POST SAVE DATA

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $folder_created = false;
    
    if(!empty($lot_id))
    {
        $data = array();
        $checkUpdate = true;
        $lots = MeoCrmRealestateManagement::getFullDataLotById($external_wpdb, $lot_id);
        if(empty($_POST['status_id'])){ $check_data = false; $input_class_status_id = 'warningField'; }
        if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($_POST['description'])){ $check_data = false; $input_class_desc = 'warningField'; }        
        if(empty($_POST['rooms'])){ $check_data = false; $input_class_rooms = 'warningField'; }        
        if(empty($_POST['surface'])){ $check_data = false; $input_class_surface = 'warningField'; }        
        if(empty($_POST['type_lot'])){ $check_data = false; $input_class_type_lot = 'warningField'; }        
        if(empty($_POST['price'])){ $check_data = false; $input_class_price = 'warningField'; }        

        if($check_data)
        {
            if($_POST['status_id'] != $lots[0]->status_id){ $data['status_id'] = $_POST['status_id']; }
            if($_POST['plan_id'] != $lots[0]->plan_id){ $data['plan_id'] = $_POST['plan_id']; }
            if($_POST['document_id'] != $lots[0]->document_id){ $data['document_id'] = $_POST['document_id']; }
            if($_POST['title'] != $lots[0]->lot_title){ $data['title'] = $_POST['title']; }
            if($_POST['description'] != $lots[0]->lot_description){ $data['description'] = $_POST['description']; }      
            if($_POST['rooms'] != $lots[0]->lot_rooms){ $data['rooms'] = $_POST['rooms']; }       
            if($_POST['surface'] != $lots[0]->lot_surface){ $data['surface'] = $_POST['surface']; }      
            if($_POST['type_lot'] != $lots[0]->lot_type){ $data['type_lot'] = $_POST['type_lot']; }      
            if($_POST['price'] != $lots[0]->lot_price){ $data['price'] = $_POST['price']; }
            
            if(!empty($data))
            {
                $where = array('id' => $lot_id);
                $result = MeoCrmRealestateManagement::updateLot($external_wpdb,$data,$where);  

                if(!$result)
                {
                    $checkUpdate = false;
                }
            }
            $dataMeta = array();
            foreach($metas as $meta)
            {
                if(!empty($_POST[$meta->meta_slug]) || $_POST[$meta->meta_slug] == 0)
                {
                    $dataMeta[$meta->meta_slug]['lot_id'] = $lot_id;
                    $dataMeta[$meta->meta_slug]['meta_id'] = $meta->id;
                    $dataMeta[$meta->meta_slug]['value'] = $_POST[$meta->meta_slug];
                    $meta_value = MeoCrmRealestateManagement::getMetaValueLotByLotIdAndMetaId($external_wpdb,$lot_id,$meta->id);
                    if(isset($meta_value) && !empty($meta_value))
                    {
                        if($meta_value[0]->value != $dataMeta[$meta->meta_slug]['value'])
                        {
                            // FAIRE UN UPDATE DE  LA META
                            $where = array(
                                'id' => $meta_value[0]->id
                            );
                            $resultInsertMetaLotValue  = MeoCrmRealestateManagement::updateMetaValueLot($external_wpdb,$dataMeta[$meta->meta_slug],$where);
                            if(!$resultInsertMetaLotValue)
                            {
                                $checkUpdate = false;
                            }
                        }
                    }else{
                        // FAIRE UN INSERT DES META
                        $resultInsertMetaLotValue  = MeoCrmRealestateManagement::insertMetaValueLot($external_wpdb,$dataMeta[$meta->meta_slug]);
                        if(!$resultInsertMetaLotValue)
                        {
                            $checkUpdate = false;
                        }
                    }
                }
            }
            if($checkUpdate)
            {
                do_action('meo_crm_realestate_create_price_list',$project_id);
                header('Location:/meo-crm-realestate-form-lot/?project='.$project_id.'&id='.$lot_id);
            }
        }
        
    }else{
       
        // Enregistrer les datas dans la DB
        $data = array();
        $data['status_id'] = $_POST['status_id'];
        $data['plan_id'] = $_POST['plan_id'];
        $data['document_id'] = $_POST['document_id'];
        $data['title'] = $_POST['title'];
        $data['description'] = $_POST['description'];        
        $data['rooms'] = $_POST['rooms'];        
        $data['surface'] = $_POST['surface'];        
        $data['type_lot'] = $_POST['type_lot'];        
        $data['price'] = $_POST['price'];           
        
        if(empty($_POST['status_id'])){ $check_data = false; $input_class_status_id = 'warningField'; }
        if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($_POST['description'])){ $check_data = false; $input_class_desc = 'warningField'; }        
        if(empty($_POST['rooms'])){ $check_data = false; $input_class_rooms = 'warningField'; }        
        if(empty($_POST['surface'])){ $check_data = false; $input_class_surface = 'warningField'; }        
        if(empty($_POST['type_lot'])){ $check_data = false; $input_class_type_lot = 'warningField'; }        
        if(empty($_POST['price'])){ $check_data = false; $input_class_price = 'warningField'; }

        if($check_data)
        {
            $resultInsert = MeoCrmRealestateManagement::insertLot($external_wpdb, $data);
        }else{
            $resultInsert = false;
        }
        
        if($resultInsert != false)
        {
            $data = array();
            $insert_id = $resultInsert['id'];       

            if(!empty($data))
            {
                $where = array('id' => $insert_id);
                $result = MeoCrmRealestateManagement::updateLot($external_wpdb,$data,$where);
            }

            if(isset($insert_id) && !empty($insert_id))
            {
                $dataMeta = array();
                $checkInsertMeta = true;
                foreach($metas as $meta)
                {
                    if(!empty($_POST[$meta->meta_slug]) || $_POST[$meta->meta_slug] == 0)
                    {
                        $dataMeta[$meta->meta_slug]['lot_id'] = $insert_id;
                        $dataMeta[$meta->meta_slug]['meta_id'] = $meta->id;
                        $dataMeta[$meta->meta_slug]['value'] = $_POST[$meta->meta_slug];

                        $resultInsertMetaLotValue  = MeoCrmRealestateManagement::insertMetaValueLot($external_wpdb,$dataMeta[$meta->meta_slug]);
                        if(!$resultInsertMetaLotValue)
                        {
                            $checkInsertMeta = false;
                        }
                    }
                }
                if($checkInsertMeta)
                {
                    do_action('meo_crm_realestate_create_price_list',$project_id);
                    header('Location:/meo-crm-realestate-form-lot/?project='.$project_id.'&id='.$insert_id);
                }
            }

        }else{
            $errors[] = 'Le titre ou la description ou les coordonn&eacute;e du secteur ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        //MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }    
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($lot_id))
{
    $lots = (isset($lots) && !empty($lots)) ? $lots : MeoCrmRealestateManagement::getFullDataLotById($external_wpdb, $lot_id);    
    $lot = (isset($lots) && !empty($lots)) ? $lots[0] : array();
    $metas_value = MeoCrmRealestateManagement::getValueMetaLotByLotId($external_wpdb, $lot_id);
    $old_status_id = (isset($lot->status_id) && !empty($lot->status_id)) ? $lot->status_id : 0;
    $old_plan_id = (isset($lot->plan_id) && !empty($lot->plan_id)) ? $lot->plan_id : 0;
    $old_document_id = (isset($lot->document_id) && !empty($lot->document_id)) ?  $lot->document_id : 0;
    $old_title = (!empty($lot->lot_title)) ? $lot->lot_title : '';
    $old_description = (!empty($lot->lot_description)) ? $lot->lot_description : '';
    $old_rooms = (!empty($lot->lot_rooms)) ? $lot->lot_rooms : '';
    $old_surface = (!empty($lot->lot_surface)) ? $lot->lot_surface : '';
    $old_type_lot = (!empty($lot->lot_type)) ? $lot->lot_type : '';
    $old_price = (!empty($lot->lot_price)) ? $lot->lot_price : '';
    $old_status_plan_floor_id = (!empty($lot->status_plan_floor_id)) ? $lot->status_plan_floor_id : 0;
}else{
	$lots = array();
    $old_status_id = 0;
    $old_plan_id = 0;
    $old_document_id = 0;
    $old_title = '';
    $old_description = '';
    $old_rooms = '';
    $old_surface = '';
    $old_type_lot = '';
    $old_price = '';
	$old_status_plan_floor_id = 0;
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    $url_length = strlen($projects['url']);
    $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
?>
    <form method="post" name="add_lot" id="form_developpement" enctype="multipart/form-data">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <input type="hidden" disabled="true" name="url_ajax" id="url_ajax" value="<?php echo $url_ajax ?>" />
        <h2>Lot</h2> 
        
        <div class="input-group">
            <label>Status li&eacute;</label>
            <select name="status_id" id="status_id" class="<?php /*echo $input_class_spinner_id;*/ ?>">
                <option value="0">Choisir un status</option>
                <?php 
                    // METTRE LA LISTE DES DEVELOPPEMENT LIE AU PROJET
                    foreach($list_status as $status)
                    {
                        $selected = (isset($old_status_id) && !empty($old_status_id) && $status->id == $old_status_id) ? 'selected' : '';
                        echo '<option value="'.$status->id.'" '.$selected.'>'.$status->name.'</option>';
                    }
                ?>
            </select>
        </div>
        
        <div class="input-group">
            <label>Plan li&eacute;</label>
            <select name="plan_id" id="plan_id" class="<?php /*echo $input_class_spinner_id;*/ ?>">
                <option value="0">Choisir un plan</option>
                <?php 
                    // METTRE LA LISTE DES DEVELOPPEMENT LIE AU PROJET
                    foreach($plans as $plan)
                    {
                        $selected = (isset($old_plan_id) && !empty($old_plan_id) && $old_plan_id == $plan->id) ? 'selected' : '';
                        echo '<option value="'.$plan->id.'" '.$selected.'>'.$plan->title.'</option>';
                    }
                ?>
            </select>
        </div>
        
        <div class="input-group">
            <label>Document li&eacute;</label>
            <select name="document_id" id="document_id" class="<?php /*echo $input_class_spinner_id;*/ ?>">
                <option value="0">Choisir un document PDF</option>
                <?php 
                    // METTRE LA LISTE DES DEVELOPPEMENT LIE AU PROJET
                    foreach($documents as $document)
                    {
                        $selected = (isset($old_document_id) && !empty($old_document_id) && $document->ID == $old_document_id) ? 'selected' : '';
                        echo '<option value="'.$document->ID.'" '.$selected.'>'.$document->post_title.'</option>';
                    }
                ?>
            </select>
        </div>
        
        <div class="input-group">
            <label>Type de lot</label>
            <select name="type_lot" id="type_lot" class="<?php /*echo $input_class_spinner_id;*/ ?>">
                <option value="0">Choisir type de lot</option>
                <option value="appartement" <?php echo ($old_type_lot == 'appartement') ? 'selected="true"' : ''; ?>>Appartement</option>
                <option value="maison" <?php echo ($old_type_lot == 'maison') ? 'selected="true"' : ''; ?>>Maison</option>
            </select>
        </div>
        
        <div class="input-group">
            <label>Nom du lot</label>
            <input type="text" name="title" id="title" value="<?php echo $old_title ?>" placeholder="Nom du lot" class="<?php echo $input_class_title ?>" />
        </div>
        
        <div class="input-group">
            <label>Description</label>
            <textarea type="text" name="description" id="description" class="<?php echo $input_class_desc ?>"><?php echo $old_description ?></textarea>
        </div>
        
        <div class="input-group">
            <label>Nombre de pi&egrave;ces</label>
            <input type="text" name="rooms" id="rooms" value="<?php echo $old_rooms ?>" placeholder="Nombre de pi&egrave;ces" class="<?php echo $input_class_rooms ?>" />
        </div>
        
        <div class="input-group">
            <label>Surface du lot</label>
            <input type="text" name="surface" id="surface" value="<?php echo $old_surface ?>" placeholder="Surface du lot" class="<?php echo $input_class_surface ?>" />
        </div>
        
        <div class="input-group">
            <label>Prix du lot</label>
            <input type="text" name="price" id="price" value="<?php echo $old_price ?>" placeholder="Prix du lot" class="<?php echo $input_class_prix ?>" />
        </div>
        
        <div class="meta-container">
            <?php
            $meta_to_show = array();
            foreach($metas as $meta){ 
                if(!empty($lot_id)){
                    foreach($metas_value as $meta_value){ 
                        if($meta_value->meta_id == $meta->id)
                        {
                            $meta_to_show[] = $meta->id;
                        ?> 
                            <div class="input-group">
                                 <label><?php echo $meta->meta_key ?></label>
                                 <input type="text" name="<?php echo $meta->meta_slug ?>" id="<?php echo $meta->meta_slug ?>" value="<?php echo $meta_value->value ?>" placeholder="<?php echo $meta->meta_key ?>" class="<?php echo $input_class_prix ?>" />
                             </div> 
                        <?php 
                        }
                    }
                }else{
                    ?>
                        <div class="input-group">
                            <label><?php echo $meta->meta_key ?></label>
                            <input type="text" name="<?php echo $meta->meta_slug ?>" id="<?php echo $meta->meta_slug ?>" value="" placeholder="<?php echo $meta->meta_key ?>" class="<?php echo $input_class_prix ?>" />
                        </div> 
                    <?php
                }
                
            }
            foreach($metas as $meta)
            {
                $checkExist = false;
                foreach($meta_to_show as $meta_id)
                {
                    if($meta->id == $meta_id)
                    {
                        $checkExist = true;
                    }
                }
                if(!$checkExist)
                {
                    ?>
                    <div class="input-group">
                        <label><?php echo $meta->meta_key ?></label>
                        <input type="text" name="<?php echo $meta->meta_slug ?>" id="<?php echo $meta->meta_slug ?>" value="" placeholder="<?php echo $meta->meta_key ?>" class="<?php echo $input_class_prix ?>" />
                    </div>
                    <?php
                }
            }

            ?> 
        </div>
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="project" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-lot-list/?project='.$project_id ?>" title=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
    <form name="add_lot_floor_form" id="add_lot_floor_form" enctype="multipart/form-data">  
        <?php if(!empty($lot_id)): ?>
        <div id="floor-container" class="floor-container">
                <h2>Ajouter un nouvel &eacute;tage</h2>
                
                <section>
                    <input type="hidden" name="lot_id" id="lot_id" value="<?php echo $lot_id ?>" />                         
                    <input type="hidden" name="floor_id" id="floor_id" value="" />                         
                    <label for="floor">&Eacute;tage</label>
                    <input type="text" name="floor" id="floor" /> 
                </section>

                <section>
                    <label for="lot_floor_position">Plan par &eacute;tage</label>
                    <input type="file" name="lot_floor_position" id="lot_floor_position" /> 
                </section>

                <section>
                    <label for="lot_floor_entry">Entr&eacute;e</label>
                    <input type="checkbox" name="lot_floor_entry" id="lot_floor_entry" value="1" />  
                </section>

                <section>
                    <label for="lot_plan_building">Image Lot status dans Bâtiment</label>
                    <select name="lot_plan_building" id="lot_plan_building"> 
                        <option value="0">Plan de lot par &eacute;tage typologie</option>
                        <?php foreach($building_plans_floors as $building_plan_floor): ?>
                            <option value="<?php echo $building_plan_floor->id; ?>"><?php echo $building_plan_floor->title; ?></option>
                        <?php endforeach; ?>
                    </select> 
                </section>  
                
                <div class="btn-action-form-floor">
                    <button type="submit" id="saveLotFloorForm">Sauver &eacute;tage</button>
                </div> 
                
            </div>
        </form>
        <div>
            <table id="list_lot_floor" class="table table-striped">
                <thead>
                    <tr>
                        <th>&Eacute;tage</th>
                        <th>Position lot &eacute;tage</th>
                        <th>Lot entr&eacute;e</th>
                        <th>Typologie plan sur b&acirc;timent par &eacute;tage</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach($lots as $lot): 
                        if(isset($lot->floor_title) && !empty($lot->floor_title)):
                    ?>
                        <tr>
                            <td><?php echo $lot->floor_title ?></td>
                            <td><img src="<?php echo $url_site.'/wp/wp-content/uploads/'.$lot->plan_floor ?>" alt="" width="200" /></td>
                            <td><?php echo ($lot->lot_entry) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>'; ?></td>
                            <td><?php echo $lot->plan_building_status_title ?></td>
                            <td><button class="btn-remove-floor-lot" data-lot-id="<?php echo $lot->lot_id ?>" data-floor-id="<?php echo $lot->floor_id ?>"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                        </tr>
                    <?php
                        endif;
                    endforeach; 
                    ?>
                </tbody>
            </table>
        </div>
    <?php endif; 
        
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
        
        // Ajout d'un etage a un lot
        $("#add_lot_floor_form").on('submit',(function(e) {
            
            e.preventDefault();
            
            var formData = new FormData(this);
            formData.append('action','meoCrmRealeastateLotInsertFloor');
            formData.append('project_id',$('input[name="project_id"]').val());
            formData.append('lot_id',$('input[name="lot_id"]').val());
            formData.append('url_site','<?php echo $url_site ?>'); // !! Modifier !!
            
            $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                success: function(response)
                {
                    $('#list_lot_floor tbody').html(response);
                }
            }); 
        }));
        
        $('.btn-remove-floor-lot').click(function(){
            deleteFloorLot($(this));            
        });
        
        $.ajax({
            url: $('#url_ajax').val(),
            type: "POST",
            data:{
                'action': 'getAllFloorFromDatabaseToJSON',
                'project': $('input[name="project_id"]').val()
            },
            success: function(response)
            {
                var projects = $.parseJSON(response);
                $( "#floor" ).autocomplete({
                    minLength: 0,
                    source: projects,
                    focus: function( event, ui ) {
                      $( "#floor" ).val( ui.item.label );
                      return false;
                    },
                    select: function( event, ui ) {
                      $( "#floor" ).val( ui.item.label );
                      $( "#floor_id" ).val( ui.item.value );
                      return false;
                    }
                })                
                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                    .append( "<div>" + item.label + "</div>" )
                    .appendTo( ul );
                };
            }
        });
		
        $('#lot_plan_building').change(function(){
            var value = $(this).val();
            if(value > 0)
            {
                $('#floor-container .content-form-lot-status').hide();
            }else{
                $('#floor-container .content-form-lot-status').show();
            }
        });
		
		$('.btn-remove-floor-lot')

    });
    
    function deleteFloorLot(element)
        {
            var lot_id = $(element).attr('data-lot-id');
            var floor_id = $(element).attr('data-floor-id');
                $.ajax({
                url: $('#url_ajax').val(),
                type: "POST",
                data:{
                    'action': 'meoCrmRealeastateDeleteFloorLot',
                    'project': $('input[name="project_id"]').val(),
                    'lot_id': lot_id,
                    'floor_id': floor_id,
                    'url_site': '<?php echo $url_site ?>'
                },
                success: function(response)
                {
                    console.log(response);
                }
            });
        }
</script>
<?php get_footer(); ?>