<?php
global $current_user;
$check_data = true;
$errors = array();
$input_class_title = '';
$input_class_desc = '';
$developpement_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
$project = meo_crm_projects_getProjectInfo($project_id);
$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection(
    $project['realestate']['db']['login'],
    $project['realestate']['db']['password'],
    $project['realestate']['db']['name'],
    $project['realestate']['db']['host']
);

$ftp = MeoCrmCoreHelper::getExternalFtpConnection(
    $project['realestate']['ftp']['login'],
    $project['realestate']['ftp']['password'],
    $project['realestate']['ftp']['host']
);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $folder_created = false;
    
    if(!empty($developpement_id))
    {
        
        $datas = array();
        $content_dir_path = $baseDirFTP.'uploads/';
        
        if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_DEVELOPPEMENT))
        {
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_DEVELOPPEMENT.'/', $developpement_id))
            {
                $folder_created = true;
            }
        }

        $url_length = strlen($project['url']);
        $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

        $content_dir_url = FOLDER_DEVELOPPEMENT.'/'.$developpement_id.'/';
        $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_DEVELOPPEMENT.'/'.$developpement_id.'/';

        if($folder_created)
        {            
            
            // Upload images developpement 2D
            if(!empty($_FILES['developpement_image_2d']['name']))
            {
                $nextStep = true; 
                if(!empty($old_image_2d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_2d))
                    {
                        $errors[] = 'Ancienne image 2D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['developpement_image_2d']['tmp_name'];
                    $type_file = $_FILES['developpement_image_2d']['type'];
                    $name_file = $_FILES['developpement_image_2d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['developpement_image_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_developpement_2d'] = $content_dir_url.$_FILES['developpement_image_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }
            }

            // Upload images developpement 3D
            if(!empty($_FILES['developpement_image_3d']['name']))
            {
                $nextStep = true;        
                if(!empty($old_image_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_image_3d))
                    {
                        $errors[] = 'Ancienne image 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['developpement_image_3d']['tmp_name'];
                    $type_file = $_FILES['developpement_image_3d']['type'];
                    $name_file = $_FILES['developpement_image_3d']['name']; 
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['developpement_image_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_developpement_3d'] = $content_dir_url.$_FILES['developpement_image_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }   
                }
            } 
            
            if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
            if(empty($_POST['description'])){ $check_data = false; $input_class_desc = 'warningField'; }
            
            if($check_data)
            {
                $datas['title'] = $_POST['title'];
                $datas['description'] = $_POST['description'];

                if(!empty($datas))
                {
                    $where = array('id' => $developpement_id);
                    $result = MeoCrmRealestateManagement::updateDeveloppement($external_wpdb,$datas,$where);
                }
            }            
        }
        
    }else{
       
        // Enregistrer les datas dans la DB
        $datas = array();
        $datas['title'] = $_POST['title'];
        $datas['description'] = $_POST['description'];
        
        if(empty($datas['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($datas['description'])){ $check_data = false; $input_class_desc = 'warningField'; }
        
        if($check_data){
            $resultInsert = MeoCrmRealestateManagement::insertDeveloppement($external_wpdb,$datas);
        }else $resultInsert = false;
        
        if($resultInsert)
        {
            $datas = array();
            
            $insert_id = $resultInsert['id'];
            
            $content_dir_path = $baseDirFTP.'uploads/';
            
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, 'developpement'))
            {
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.'developpement/', $insert_id))
                {
                    $folder_created = true;
                }
            }
            
            
            $url_length = strlen($project['url']);
            $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));
            
            $content_dir_url = FOLDER_DEVELOPPEMENT.'/'.$insert_id.'/';
            $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_DEVELOPPEMENT.'/'.$insert_id.'/';
            
            if($folder_created)
            {
                // Upload images developpement 2D
                if(!empty($_FILES['developpement_image_2d']['name']))
                {
                    $tmp_file = $_FILES['developpement_image_2d']['tmp_name'];
                    $type_file = $_FILES['developpement_image_2d']['type'];
                    $name_file = $_FILES['developpement_image_2d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['developpement_image_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_developpement_2d'] = $content_dir_url.$_FILES['developpement_image_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }else{
                    $errors[] = 'Image 2D not exists';
                }
                
                // Upload images developpement 3D
                if(!empty($_FILES['developpement_image_3d']['name']))
                {
                    $tmp_file = $_FILES['developpement_image_3d']['tmp_name'];
                    $type_file = $_FILES['developpement_image_3d']['type'];
                    $name_file = $_FILES['developpement_image_3d']['name']; 
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['developpement_image_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_developpement_3d'] = $content_dir_url.$_FILES['developpement_image_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }                    
                } else{
                    $errors[] = 'Image 3D not exists';
                } 
                
                if(!empty($datas))
                {
                    $where = array('id' => $insert_id);
                    $result = MeoCrmRealestateManagement::updateDeveloppment($external_wpdb,$datas,$where);  
                }
                
                if(isset($insert_id) && !empty($insert_id))
                {
                    header('Location:/meo-crm-realestate-form-developpement/?project='.$project_id.'&id='.$insert_id);
                }
                
            }else{
                $errors[] = 'Erreur durant la création des dossiers !!';
            } 
            
        }else{
            $errors[] = 'Le titre ou la description du d&eacute;veloppement ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form developpement " . print_r($errors));
    }    
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($developpement_id))
{
    $developpement = MeoCrmRealestateManagement::getDeveloppementById($external_wpdb, $developpement_id);
    echo 'QUERY = '.$external_wpdb->last_query;
    $old_title = (!empty($developpement[0]->title)) ? $developpement[0]->title : '';
    $old_description = (!empty($developpement[0]->description)) ? $developpement[0]->description : '';
    $old_image_3d = (!empty($developpement[0]->plan_developpement_3d)) ? $developpement[0]->plan_developpement_3d : '';
    $old_image_2d = (!empty($developpement[0]->plan_developpement_2d)) ? $developpement[0]->plan_developpement_2d : '';
}else{
    $old_title = '';
    $old_description = '';
    $old_image_3d = '';
    $old_image_2d = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    
    $url_length = strlen($projects['url']);
    $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
?>
    <form method="post" name="add_developpement" id="form_developpement" enctype="multipart/form-data">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>D&eacute;veloppement</h2>
        <div class="input-group">
            <label>Titre du d&eacute;veloppement</label>
            <input type="text" name="title" id="developpement_title" value="<?php echo $old_title ?>" placeholder="Titre du d&eacute;veloppement" class="<?php echo $input_class_title ?>" />
        </div>
        <div class="input-group">
            <label>Description du d&eacute;veloppement</label>
            <textarea type="text" name="description" id="developpement_description" class="<?php echo $input_class_desc ?>"><?php echo $old_description ?></textarea>
        </div>
        <div class="input-group">
            <label>Plan de d&eacute;veloppement 3D</label>
            <input type="file" name="developpement_image_3d" id="developpement_image_3d" />
            <?php 
                if(!empty($old_image_3d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_3d.'" />';
                }
            ?>
        </div>
        <div class="input-group">
            <label>Plan de d&eacute;veloppement 2D</label>
            <input type="file" name="developpement_image_2d" id="developpement_image_2d" />
            <?php 
                if(!empty($old_image_2d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_2d.'" />';
                }
            ?>
        </div>
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-developpement-list/?project='.$project_id ?>" title=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
<?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
    });
</script>
<?php get_footer(); ?>