<?php
//echo 'test';exit();
global $current_user;
$check_data = true;
$errors = array();
$plan_lot_floor = array();
$input_class_name = '';
$input_class_desc = '';
$coordinate_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
    
$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

$url_length = strlen($project['url']);
$url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

$list_status = MeoCrmRealestateManagement::getStatus($external_wpdb);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $folder_created = false;
    
    if(!empty($coordinate_id))
    {
        $checkUpdate = true;
        $plan_lot_floor = MeoCrmRealestateManagement::getBuildinFloorPlanById($external_wpdb, $coordinate_id); 
        
        echo '<pre>';
        print_r($plan_lot_floor);
        echo '</pre>';        
        
        if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($_POST['coordinates_front'])){ $check_data = false; $input_class_coordinates_front = 'warningField'; } 
        
        if($check_data)
        {
            $data_coordinates_plan = array();
            
            if($_POST['title'] != $plan_lot_floor[0]->title){ $data_coordinates_plan['title'] = $_POST['title']; }
            if($_POST['coordinates_front'] != $plan_lot_floor[0]->coordinates_front){ $data_coordinates_plan['coordinates_front'] = $_POST['coordinates_front']; }
            if($_POST['coordinates_back'] != $plan_lot_floor[0]->coordinates_back){ $data_coordinates_plan['coordinates_back'] = $_POST['coordinates_back']; }
            
            if(!empty($data_coordinates_plan))
            {
                $whereUpdateCoordinate = array(
                    'id' => $coordinate_id
                );                
                $resultUpdateCoordinate = MeoCrmRealestateManagement::updateCoordinateFloorPlan($external_wpdb,$data_coordinates_plan,$whereUpdateCoordinate); 
                // IF: Update don't work
                if(!$resultUpdateCoordinate)
                {
                    $checkUpdate = false;
                }                
            }
            
            $content_dir_path = $baseDirFTP.'uploads/';
        
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_TYPO_STATUS_FLOOR_LOT))
            {
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_TYPO_STATUS_FLOOR_LOT.'/', $coordinate_id))
                {
                    $folder_created = true;
                }
            }
            
            $content_dir_url = FOLDER_TYPO_STATUS_FLOOR_LOT.'/'.$coordinate_id.'/';     
            $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_TYPO_STATUS_FLOOR_LOT.'/'.$coordinate_id.'/';  

            if($folder_created)
            {
                foreach($list_status as $status)
                {
                    $data_floor_lot_image_front = array();                        
                    $data_floor_lot_image_front_hover = array();

                    $data_floor_lot_image_back = array();
                    $data_floor_lot_image_back_hover = array();

                    // ************************************************************
                    // Upload images front
                    // ************************************************************
                    if(!empty($_FILES['image_front_'.$status->id]['name']))
                    {      
                        $nextStep = true;
                        $image_front_id = 0;                        
                        foreach($plan_lot_floor as $value)
                        {
                            if($value->status_id == $status->id && $value->front_face && !empty($value->image))
                            {                                
                                $image_front_id = (!empty($value->image_id)) ? $value->image_id : 0;
                                if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$value->image))
                                {
                                    $errors[] = 'Ancienne image pas supprimé !!';
                                    $nextStep = false;
                                }
                            }
                        }                        
                        if($nextStep)
                        {
                            $tmp_file = $_FILES['image_front_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_front_'.$status->id]['type'];
                            $name_file = $_FILES['image_front_'.$status->id]['name'];
                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_front_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    if(!empty($image_front_id))
                                    {
                                        $wherePlanLotImageBack = array(
                                            'id' => $image_front_id
                                        );
                                        $data_floor_lot_image_front['image'] = $content_dir_url.$_FILES['image_front_'.$status->id]['name'];
                                        $resultImageFront = MeoCrmRealestateManagement::updateImageFloorPlan($external_wpdb,$data_floor_lot_image_front,$wherePlanLotImageBack);
                                    }else{
                                        
                                        $data_floor_lot_image_front['status_id'] = $status->id;
                                        $data_floor_lot_image_front['image'] = $content_dir_url.$_FILES['image_front_'.$status->id]['name'];
                                        $data_floor_lot_image_front['front_face'] = 1;
                                        $resultImageFront = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_front);
                                        
                                        if($resultImageFront != false)
                                        {
                                            $data_floor_lot_link_front = array();
                                            $data_floor_lot_link_front['coordinates_id'] = $coordinate_id;
                                            $data_floor_lot_link_front['plan_lot_id'] = $resultImageFront;
                                            $resultLinkImageCoordinateFront = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_link_front);    
                                            if(!$resultLinkImageCoordinateFront)
                                            {
                                                $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                            }
                                        }else{
                                            $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                        }
                                    }
                                    
                                    if(!$resultImageFront)
                                    {
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }
                                    
                                }else{
                                    $errors[] = 'Erreur durant l\'upload !!';
                                } 
                            }else{
                                $errors[] = 'Erreur de type !!';
                            }
                        }
                    }

                    // ************************************************************
                    // Upload images front HOVER
                    // ************************************************************
                    if(!empty($_FILES['image_front_hover_'.$status->id]['name']))
                    {
                        $nextStep = true;
                        $image_front_id = 0;
                        foreach($plan_lot_floor as $value)
                        {
                            if($value->status_id == $status->id && $value->front_face && !empty($value->image))
                            {
                                $image_front_id = (!empty($value->image_id)) ? $value->image_id : 0;
                                if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$value->image))
                                {
                                    $errors[] = 'Ancienne image pas supprimé !!';
                                    $nextStep = false;
                                }
                            }
                        }
                        if($nextStep)
                        {
                            $tmp_file = $_FILES['image_front_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_front_'.$status->id]['type'];
                            $name_file = $_FILES['image_front_'.$status->id]['name'];
                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_front_hover_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    if(!empty($image_front_id))
                                    {
                                        $wherePlanLotImageBack = array(
                                            'id' => $image_front_id
                                        );
                                        $data_floor_lot_image_front_hover['image'] = $content_dir_url.$_FILES['image_front_hover_'.$status->id]['name'];
                                        $resultImageFront = MeoCrmRealestateManagement::updateImageFloorPlan($external_wpdb,$data_floor_lot_image_front_hover,$wherePlanLotImageBack);
                                    }else{

                                        $data_floor_lot_image_front_hover['status_id'] = $status->id;
                                        $data_floor_lot_image_front_hover['image'] = $content_dir_url.$_FILES['image_front_hover_'.$status->id]['name'];
                                        $data_floor_lot_image_front_hover['front_face'] = 1;
                                        $data_floor_lot_image_front_hover['hover'] = 1;
                                        $resultImageFront = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_front_hover);

                                        if($resultImageFront != false)
                                        {
                                            $data_floor_lot_image_front_hover = array();
                                            $data_floor_lot_image_front_hover['coordinates_id'] = $coordinate_id;
                                            $data_floor_lot_image_front_hover['plan_lot_id'] = $resultImageFront;
                                            $resultLinkImageCoordinateFront = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_image_front_hover);
                                            if(!$resultLinkImageCoordinateFront)
                                            {
                                                $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                            }
                                        }else{
                                            $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                        }
                                    }

                                    if(!$resultImageFront)
                                    {
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }

                                }else{
                                    $errors[] = 'Erreur durant l\'upload !!';
                                }
                            }else{
                                $errors[] = 'Erreur de type !!';
                            }
                        }
                    }

                    // ************************************************************
                    // Upload images back
                    // ************************************************************
                    if(!empty($_FILES['image_back_'.$status->id]['name']))
                    { 
                        $nextStep = true;
                        $image_back_id = 0;                        
                        foreach($plan_lot_floor as $value)
                        {
                            if($value->status_id == $status->id && !$value->front_face && !empty($value->image))
                            {
                                $image_back_id = (!empty($value->image_id)) ? $value->image_id : 0;
                                if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$value->image))
                                {
                                    $errors[] = 'Ancienne image pas supprimé !!';
                                    $nextStep = false;
                                }
                            }
                        }
                        
                        if($nextStep)
                        {
                            $tmp_file = $_FILES['image_back_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_back_'.$status->id]['type'];
                            $name_file = $_FILES['image_back_'.$status->id]['name'];
                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_back_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    if(!empty($image_back_id))
                                    {
                                        $wherePlanLotImageBack = array(
                                            'id' => $image_back_id
                                        );
                                        $data_floor_lot_image_back['image'] = $content_dir_url.$_FILES['image_back_'.$status->id]['name'];
                                        $resultImageBack = MeoCrmRealestateManagement::updateImageFloorPlan($external_wpdb,$data_floor_lot_image_back,$wherePlanLotImageBack); 
                                    }else{
                                        
                                        $data_floor_lot_image_back['status_id'] = $status->id;
                                        $data_floor_lot_image_back['image'] = $content_dir_url.$_FILES['image_back_'.$status->id]['name'];
                                        $data_floor_lot_image_back['front_face'] = 0;
                                        $resultImageBack = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_back);
                                        if($resultImageBack != false)
                                        {
                                            $data_floor_lot_link_back = array();
                                            $data_floor_lot_link_back['coordinates_id'] = $coordinate_id;
                                            $data_floor_lot_link_back['plan_lot_id'] = $resultImageBack;
                                            $resultLinkImageCoordinateBack = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_link_back);
                                            if(!$resultLinkImageCoordinateBack)
                                            {
                                                $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                            }
                                        }else{
                                            $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                        }                                        
                                    }
                                    
                                    if(!$resultImageBack)
                                    {
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }
                                    
                                }else{
                                    $errors[] = 'Erreur durant l\'upload !!';
                                } 
                            }else{
                                $errors[] = 'Erreur de type !!';
                            }
                        }
                    }

                    // ************************************************************
                    // Upload images back HOVER
                    // ************************************************************
                    if(!empty($_FILES['image_back_hover_'.$status->id]['name']))
                    {
                        $nextStep = true;
                        $image_back_id = 0;
                        foreach($plan_lot_floor as $value)
                        {
                            if($value->status_id == $status->id && !$value->front_face && !empty($value->image))
                            {
                                $image_back_id = (!empty($value->image_id)) ? $value->image_id : 0;
                                if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$value->image))
                                {
                                    $errors[] = 'Ancienne image pas supprimé !!';
                                    $nextStep = false;
                                }
                            }
                        }

                        if($nextStep)
                        {
                            $tmp_file = $_FILES['image_back_hover_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_back_hover_'.$status->id]['type'];
                            $name_file = $_FILES['image_back_hover_'.$status->id]['name'];
                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_back_hover_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    if(!empty($image_back_id))
                                    {
                                        $wherePlanLotImageBack = array(
                                            'id' => $image_back_id
                                        );
                                        $data_floor_lot_image_back_hover['image'] = $content_dir_url.$_FILES['image_back_hover_'.$status->id]['name'];
                                        $resultImageBack = MeoCrmRealestateManagement::updateImageFloorPlan($external_wpdb,$data_floor_lot_image_back_hover,$wherePlanLotImageBack);
                                    }else{

                                        $data_floor_lot_image_back_hover['status_id'] = $status->id;
                                        $data_floor_lot_image_back_hover['image'] = $content_dir_url.$_FILES['image_back_hover_'.$status->id]['name'];
                                        $data_floor_lot_image_back_hover['front_face'] = 0;
                                        $resultImageBack = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_back_hover);
                                        if($resultImageBack != false)
                                        {
                                            $data_floor_lot_link_back = array();
                                            $data_floor_lot_link_back['coordinates_id'] = $coordinate_id;
                                            $data_floor_lot_link_back['plan_lot_id'] = $resultImageBack;
                                            $resultLinkImageCoordinateBack = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_link_back);
                                            if(!$resultLinkImageCoordinateBack)
                                            {
                                                $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                            }
                                        }else{
                                            $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                        }
                                    }

                                    if(!$resultImageBack)
                                    {
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }

                                }else{
                                    $errors[] = 'Erreur durant l\'upload !!';
                                }
                            }else{
                                $errors[] = 'Erreur de type !!';
                            }
                        }
                    }
                    
                    if($checkUpdate)
                    {            
                        if(isset($coordinate_id) && !empty($coordinate_id))
                        {
                            header('Location:/meo-crm-realestate-form-plan-floor-lot/?project='.$project_id.'&id='.$coordinate_id);
                        }            
                    }else{
                        $errors[] = 'Error dans le proccessus de update';
                    }                    
                }
            }               
        }
        
    }else{
        
        $checkInsert = true;
        $data_coordinates_plan = array();
        $data_coordinates_plan['title'] = $_POST['title'];
        $data_coordinates_plan['coordinates_front'] = $_POST['coordinates_front'];        
        $data_coordinates_plan['coordinates_back'] = $_POST['coordinates_back'];        
        
        if(empty($data_coordinates_plan['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($data_coordinates_plan['coordinates_front'])){ $check_data = false; $input_class_coordinates_front = 'warningField'; } 
        
        if($check_data)
        {
            $resultInsertCoordinates = MeoCrmRealestateManagement::insertCoordinateFloorPlan($external_wpdb,$data_coordinates_plan);
            
            if($resultInsertCoordinates != false)
            {
                $content_dir_path = $baseDirFTP.'uploads/';
                $insert_id = $resultInsertCoordinates;
                        
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, FOLDER_TYPO_STATUS_FLOOR_LOT))
                {
                    if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.FOLDER_TYPO_STATUS_FLOOR_LOT.'/', $insert_id))
                    {
                        $folder_created = true;
                    }
                }
                
                $content_dir_url = FOLDER_TYPO_STATUS_FLOOR_LOT.'/'.$insert_id.'/';     
                $content_dir_path = $baseDirFTP.'uploads/'.FOLDER_TYPO_STATUS_FLOOR_LOT.'/'.$insert_id.'/';            

                if($folder_created)
                {
                    foreach($list_status as $status)
                    {
                        $data_floor_lot_image_front = array();                        
                        $data_floor_lot_image_back = array();

                        // ************************************************************
                        // Upload Image Front
                        // ************************************************************
                        if(!empty($_FILES['image_front_'.$status->id]['name']))
                        {
                            $tmp_file = $_FILES['image_front_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_front_'.$status->id]['type'];
                            $name_file = $_FILES['image_front_'.$status->id]['name'];

                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_front_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    // Insert data in External DB
                                    $data_floor_lot_image_front['status_id'] = $status->id;
                                    $data_floor_lot_image_front['image'] = $content_dir_url.$_FILES['image_front_'.$status->id]['name'];
                                    $data_floor_lot_image_front['front_face'] = 1;
                                    $resultImageFront = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_front);
                                    
                                    if($resultImageFront != false)
                                    {
                                        $data_floor_lot_link_front = array();
                                        $data_floor_lot_link_front['coordinates_id'] = $insert_id;
                                        $data_floor_lot_link_front['plan_lot_id'] = $resultImageFront;
                                        $resultLinkImageCoordinateFront = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_link_front); 
                                        
                                        if(!$resultLinkImageCoordinateFront)
                                        {
                                            $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                        }
                                    }else{
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }
                                }else{
                                    $errors[] = 'Erreur durant l\'upload image front !!';
                                } 
                            }else{
                                $errors[] = 'Erreur de type de fichier !!';
                            }
                        }

                        // ************************************************************
                        // Upload Image Front HOVER
                        // ************************************************************
                        if(!empty($_FILES['image_front_hover_'.$status->id]['name']))
                        {
                            $tmp_file = $_FILES['image_front_hover_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_front_hover_'.$status->id]['type'];
                            $name_file = $_FILES['image_front_hover_'.$status->id]['name'];

                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_front_hover_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    // Insert data in External DB
                                    $data_floor_lot_image_front_hover['status_id'] = $status->id;
                                    $data_floor_lot_image_front_hover['image'] = $content_dir_url.$_FILES['image_front_hover_'.$status->id]['name'];
                                    $data_floor_lot_image_front_hover['front_face'] = 1;
                                    $data_floor_lot_image_front_hover['hover'] = 1;
                                    $resultImageFront = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_front_hover);

                                    if($resultImageFront != false)
                                    {
                                        $data_floor_lot_image_front_hover = array();
                                        $data_floor_lot_image_front_hover['coordinates_id'] = $insert_id;
                                        $data_floor_lot_image_front_hover['plan_lot_id'] = $resultImageFront;
                                        $resultLinkImageCoordinateFront = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_image_front_hover);

                                        if(!$resultLinkImageCoordinateFront)
                                        {
                                            $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                        }
                                    }else{
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }
                                }else{
                                    $errors[] = 'Erreur durant l\'upload image front !!';
                                }
                            }else{
                                $errors[] = 'Erreur de type de fichier !!';
                            }
                        }

                        // ************************************************************
                        // Upload Image Back
                        // ************************************************************
                        if(!empty($_FILES['image_back_'.$status->id]['name']))
                        {
                            $tmp_file = $_FILES['image_back_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_back_'.$status->id]['type'];
                            $name_file = $_FILES['image_back_'.$status->id]['name'];

                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_back_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    $data_floor_lot_image_back['status_id'] = $status->id;
                                    $data_floor_lot_image_back['image'] = $content_dir_url.$_FILES['image_back_'.$status->id]['name'];
                                    $data_floor_lot_image_back['front_face'] = 0;
                                    $resultImageBack = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_back);
                                    if($resultImageBack != false)
                                    {
                                        $data_floor_lot_link_back = array();
                                        $data_floor_lot_link_back['coordinates_id'] = $insert_id;
                                        $data_floor_lot_link_back['plan_lot_id'] = $resultImageBack;
                                        $resultLinkImageCoordinateBack = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_link_back);
                                        if(!$resultLinkImageCoordinateBack)
                                        {
                                            $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                        }
                                    }else{
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }
                                }else{
                                    $errors[] = 'Erreur durant l\'upload image back !!';
                                } 
                            }else{
                                $errors[] = 'Erreur de type de fichier !!';
                            }
                        }

                        // ************************************************************
                        // Upload Image Back Hover
                        // ************************************************************
                        if(!empty($_FILES['image_back_hover_'.$status->id]['name']))
                        {
                            $tmp_file = $_FILES['image_back_hover_'.$status->id]['tmp_name'];
                            $type_file = $_FILES['image_back_hover_'.$status->id]['type'];
                            $name_file = $_FILES['image_back_hover_'.$status->id]['name'];

                            if($type_file == 'image/png')
                            {
                                if(ftp_put($ftp, $content_dir_path.$_FILES['image_back_hover_'.$status->id]['name'], $tmp_file, FTP_BINARY) === true)
                                {
                                    $data_floor_lot_image_back_hover['status_id'] = $status->id;
                                    $data_floor_lot_image_back_hover['image'] = $content_dir_url.$_FILES['image_back_hover_'.$status->id]['name'];
                                    $data_floor_lot_image_back_hover['front_face'] = 0;
                                    $data_floor_lot_image_back_hover['hover'] = 1;
                                    $resultImageBack = MeoCrmRealestateManagement::insertImageFloorPlan($external_wpdb,$data_floor_lot_image_back_hover);
                                    if($resultImageBack != false)
                                    {
                                        $data_floor_lot_image_back_hover = array();
                                        $data_floor_lot_image_back_hover['coordinates_id'] = $insert_id;
                                        $data_floor_lot_image_back_hover['plan_lot_id'] = $resultImageBack;
                                        $resultLinkImageCoordinateBack = MeoCrmRealestateManagement::insertLinkFloorPlan($external_wpdb,$data_floor_lot_image_back_hover);
                                        if(!$resultLinkImageCoordinateBack)
                                        {
                                            $errors[] = 'MeoCrmRealestateManagement::insertImageFloorPlan error !!';
                                        }
                                    }else{
                                        $errors[] = 'Erreur enregistrement dans DB sur la table "meo_crm_realestate_plan_lot_building" !!';
                                    }
                                }else{
                                    $errors[] = 'Erreur durant l\'upload image back !!';
                                }
                            }else{
                                $errors[] = 'Erreur de type de fichier !!';
                            }
                        }
                    }
                }else{
                    $checkInsert = false;
                }            
            }else{
                $checkInsert = false;
            }         
        }else{
            $checkInsert = false;
        }
        
        if($checkInsert)
        {            
            if(isset($insert_id) && !empty($insert_id))
            {
                header('Location:/meo-crm-realestate-form-plan-floor-lot/?project='.$project_id.'&id='.$insert_id);
            }            
        }else{
            $errors[] = 'Error dans le proccessus de insert';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        //MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }  
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

$old_title = '';
$old_coordinates_front = '';
$old_coordinates_back = '';
$old_image_front = array();
$old_image_back = array();
$old_image_front_hover = array();
$old_image_back_hover = array();

if(!empty($coordinate_id))
{
    $plan_lot_floor = (!empty($plan_lot_floor))? $plan_lot_floor : MeoCrmRealestateManagement::getBuildinFloorPlanById($external_wpdb, $coordinate_id);  
    
    if(!empty($plan_lot_floor))
    {
        $old_title = $plan_lot_floor[0]->title;
        $old_coordinates_front = $plan_lot_floor[0]->coordinates_front;
        $old_coordinates_back = $plan_lot_floor[0]->coordinates_back;      
        foreach($plan_lot_floor as $value)
        {
            if($value->front_face)
            {
                if(isset($value->hover) && $value->hover)
                {
                    $old_image_front_hover[$value->status_id] = $value->image;
                }else{
                    $old_image_front[$value->status_id] = $value->image;
                }
            }else{
                if(isset($value->hover) && $value->hover)
                {
                    $old_image_back_hover[$value->status_id] = $value->image;
                }else{
                    $old_image_back[$value->status_id] = $value->image;
                }
            }
        }
    }
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
?>
    <form method="post" name="add_typo_plan_lot_floor" id="typo_plan_lot_floor" enctype="multipart/form-data">
        <h2>Typologie plan par &eacute;tage</h2>          
        <div class="input-group">
            <label>Titre</label>
            <input type="text" name="title" id="status" value="<?php echo $old_title ?>" placeholder="Titre" class="<?php echo $input_class_title ?>" />
        </div>        
        <div class="input-group">
            <label>Coordonnée face avant</label>
            <input type="text" name="coordinates_front" id="coordinate_front" value="<?php echo $old_coordinates_front ?>" placeholder="Coordonnée avant" class="<?php echo $input_class_coordinates_front ?>" />
        </div>        
        <div class="input-group">
            <label>Coordonnée face arri&egrave;re</label>
            <input type="text" name="coordinates_back" id="coordinate_back" value="<?php echo $old_coordinates_back ?>" placeholder="Coordonnée arri&egrave;re" class="" />
        </div>        
        <div id="container-image-lot-status">
            <?php foreach($list_status as $status): ?>
                <div class="container-status-image">
                    <label for="">Image face avant <?php echo $status->name ?></label>
                    <input type="file" name="image_front_<?php echo $status->id ?>" id="image_front_<?php echo $status->id ?>" />
                    <?php 
                        if(isset($old_image_front[$status->id]) && !empty($old_image_front[$status->id]))
                        {
                            echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_front[$status->id].'" />';
                        }
                    ?>
                </div>
                <div class="container-status-image">
                    <label for="">Image hover face avant <?php echo $status->name ?></label>
                    <input type="file" name="image_front_hover_<?php echo $status->id ?>" id="image_front_hover_<?php echo $status->id ?>" />
                    <?php
                    if(isset($old_image_front_hover[$status->id]) && !empty($old_image_front_hover[$status->id]))
                    {
                        echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_front_hover[$status->id].'" />';
                    }
                    ?>
                </div>
                <div class="container-status-image">
                    <label for="">Image face arri&egrave;re <?php echo $status->name ?></label>
                    <input type="file" name="image_back_<?php echo $status->id ?>" id="image_back_<?php echo $status->id ?>" />
                    <?php 
                        if(isset($old_image_back[$status->id]) && !empty($old_image_back[$status->id]))
                        {
                            echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_back[$status->id].'" />';
                        }
                    ?>
                </div>
                <div class="container-status-image">
                    <label for="">Image hover face arri&egrave;re <?php echo $status->name ?></label>
                    <input type="file" name="image_back_hover_<?php echo $status->id ?>" id="image_back_hover_<?php echo $status->id ?>" />
                    <?php
                    if(isset($old_image_back_hover[$status->id]) && !empty($old_image_back_hover[$status->id]))
                    {
                        echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_image_back_hover[$status->id].'" />';
                    }
                    ?>
                </div>
            <?php endforeach; ?>
        </div>        
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-plan-floor-lot-list/?project='.$project_id ?>" name=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
<?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} 

get_footer(); ?>