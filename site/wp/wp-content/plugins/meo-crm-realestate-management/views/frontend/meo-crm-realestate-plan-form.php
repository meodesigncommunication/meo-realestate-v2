<?php
global $current_user;
$check_data = true;
$errors = array();
$input_class_title = '';
$input_class_desc = '';
$plan_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$project_id = $_GET['project'];
    
$project = meo_crm_projects_getProjectInfo($project_id);

$baseDirFTP = $project['realestate']['ftp']['base_dir'];

$external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($project['realestate']['db']['login'],$project['realestate']['db']['password'],$project['realestate']['db']['name'],$project['realestate']['db']['host']);

$ftp = MeoCrmCoreHelper::getExternalFtpConnection($project['realestate']['ftp']['login'], $project['realestate']['ftp']['password'],$project['realestate']['ftp']['host']);

$spinners = MeoCrmRealestateManagement::getAllSpinner($external_wpdb);

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $folder_created = false;
    
    if(!empty($plan_id))
    {
        
        $datas = array();
        $content_dir_path = $baseDirFTP.'uploads/';
        
        if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, 'plans'))
        {
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.'plans/', $plan_id))
            {
                $folder_created = true;
            }
        }

        $url_length = strlen($project['url']);
        $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));

        $content_dir_url = 'plans/'.$plan_id.'/';     
        $content_dir_path = $baseDirFTP.'uploads/plans/'.$plan_id.'/';  

        if($folder_created)
        {     
            // ************************************************************
            // Upload images hover developpement 2D
            // ************************************************************
            if(!empty($_FILES['plan_3d']['name']))
            {
                $nextStep = true; 
                if(!empty($old_plan_3d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_plan_3d))
                    {
                        $errors[] = 'Ancienne image hover 2D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['plan_3d']['tmp_name'];
                    $type_file = $_FILES['plan_3d']['type'];
                    $name_file = $_FILES['plan_3d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['plan_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_3d'] = $content_dir_url.$_FILES['plan_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 2D !!';
                    }
                }
            }
            // ************************************************************
            // Upload images hover developpement 2D
            // ************************************************************
            if(!empty($_FILES['plan_2d']['name']))
            {
                $nextStep = true;              
                if(!empty($old_plan_2d))
                {
                    if(!ftp_delete($ftp,$baseDirFTP.'uploads/'.$old_plan_2d))
                    {
                        $errors[] = 'Ancienne image hover 3D pas supprimé !!';
                        $nextStep = false;
                    }
                }
                if($nextStep)
                {
                    $tmp_file = $_FILES['plan_2d']['tmp_name'];
                    $type_file = $_FILES['plan_2d']['type'];
                    $name_file = $_FILES['plan_2d']['name'];
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['plan_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_2d'] = $content_dir_url.$_FILES['plan_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 3D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }
                }
            }
                        
            if(empty($_POST['title'])){ $check_data = false; $input_class_title = 'warningField'; }
            if(empty($_POST['description'])){ $check_data = false; $input_class_desc = 'warningField'; }        
            
            if($check_data)
            {
                $datas['spinner_id'] = $_POST['spinner_id'];
                $datas['title'] = $_POST['title'];
                $datas['description'] = $_POST['description'];        

                if(!empty($datas))
                {
                    $where = array('id' => $plan_id);
                    $result = MeoCrmRealestateManagement::updatePlan($external_wpdb,$datas,$where);  

                    if($result['success'])
                    {
                        header('Location:/meo-crm-realestate-form-plan/?project='.$project_id.'&id='.$plan_id);
                    }
                }
            }
            
        }
        
    }else{
       
        // Enregistrer les datas dans la DB
        $datas = array();
        $datas['spinner_id'] = $_POST['spinner_id'];
        $datas['title'] = $_POST['title'];
        $datas['description'] = $_POST['description'];        
        
        if(empty($datas['title'])){ $check_data = false; $input_class_title = 'warningField'; }
        if(empty($datas['description'])){ $check_data = false; $input_class_desc = 'warningField'; }        
        
        if($check_data)
        {
            $resultInsert = MeoCrmRealestateManagement::insertPlan($external_wpdb, $datas);
        }else{
            $resultInsert = array(
                'success' => 0
            );
        }
        
        if($resultInsert['success'])
        {
            $datas = array();
            
            $insert_id = $resultInsert['id'];
            
            $content_dir_path = $baseDirFTP.'uploads/';
            
            if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path, 'plans'))
            {
                if(MeoCrmCoreFileManager::externalFTPcheckAndCreateFolder($ftp, $content_dir_path.'plans/', $insert_id))
                {
                    $folder_created = true;
                }
            }
            
            
            $url_length = strlen($project['url']);
            $url_site = (substr($project['url'], -1, 1) != '/')? $project['url'] : substr($project['url'], 0, ($url_length-1));
            
            $content_dir_url = 'plans/'.$insert_id.'/';     
            $content_dir_path = $baseDirFTP.'uploads/plans/'.$insert_id.'/';            
            
            if($folder_created)
            {
                // ************************************************************
                // Upload plan 3D
                // ************************************************************
                if(!empty($_FILES['plan_3d']['name']))
                {
                    $tmp_file = $_FILES['plan_3d']['tmp_name'];
                    $type_file = $_FILES['plan_3d']['type'];
                    $name_file = $_FILES['plan_3d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['plan_3d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_3d'] = $content_dir_url.$_FILES['plan_3d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }else{
                        $errors[] = 'Erreur de type de fichier 3D !!';
                    }
                }
                // ************************************************************
                // Upload plan 2D
                // ************************************************************
                if(!empty($_FILES['plan_2d']['name']))
                {
                    $tmp_file = $_FILES['plan_2d']['tmp_name'];
                    $type_file = $_FILES['plan_2d']['type'];
                    $name_file = $_FILES['plan_2d']['name'];
                    
                    if($type_file == 'image/jpg' || $type_file == 'image/jpeg' || $type_file == 'image/png')
                    {
                        if(ftp_put($ftp, $content_dir_path.$_FILES['plan_2d']['name'], $tmp_file, FTP_BINARY) === true)
                        {
                            $datas['plan_2d'] = $content_dir_url.$_FILES['plan_2d']['name'];
                        }else{
                            $errors[] = 'Erreur durant l\'upload de fichier 2D !!';
                        } 
                    }
                }
                
                if(!empty($datas))
                {
                    $where = array('id' => $insert_id);
                    $result = MeoCrmRealestateManagement::updatePlan($external_wpdb,$datas,$where);  
                }
                
                if(isset($insert_id) && !empty($insert_id))
                {
                    header('Location:/meo-crm-realestate-form-plan/?project='.$project_id.'&id='.$insert_id);
                }
                
            }else{
                $errors[] = 'Erreur durant la création des dossiers !!';
            } 
            
        }else{
            $errors[] = 'Le titre ou la description ou les coordonn&eacute;e du secteur ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
        }        
    }
    
    if(is_array($errors) && !empty($errors))
    {
        //MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }    
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($plan_id))
{
    $plan = MeoCrmRealestateManagement::getPlanById($external_wpdb, $plan_id);
    $old_spinner_id = $plan->spinner_id;
    $old_title = (!empty($plan->title)) ? $plan->title : '';
    $old_description = (!empty($plan->description)) ? $plan->description : '';
    $old_plan_3d = (!empty($plan->plan_3d)) ? $plan->plan_3d : '';
    $old_plan_2d = (!empty($plan->plan_2d)) ? $plan->plan_2d : '';
}else{
    $old_spinner_id = 0;
    $old_title = '';
    $old_description = '';
    $old_plan_3d = '';
    $old_plan_2d = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    
    $url_length = strlen($projects['url']);
    $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
?>
    <form method="post" name="add_plan" id="form_developpement" enctype="multipart/form-data">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>Plan</h2>        
        <div class="input-group">
            <label>Spinner li&eacute;</label>
            <select name="spinner_id" id="spinner_id" class="<?php /*echo $input_class_spinner_id;*/ ?>">
                <option value="0">Choisir un spinner</option>
                <?php 
                    // METTRE LA LISTE DES DEVELOPPEMENT LIE AU PROJET
                    foreach($spinners as $spinner)
                    {
                        $selected = (isset($old_spinner_id) && !empty($old_spinner_id)) ? 'selected' : '';
                        echo '<option value="'.$spinner->id.'" '.$selected.'>'.$spinner->title.'</option>';
                    }
                ?>
            </select>
        </div>
        
        <div class="input-group">
            <label>Typologie plan</label>
            <input type="text" name="title" id="title" value="<?php echo $old_title ?>" placeholder="Typologie plan" class="<?php echo $input_class_title ?>" />
        </div>
        
        <div class="input-group">
            <label>Description</label>
            <textarea type="text" name="description" id="description" class="<?php echo $input_class_desc ?>"><?php echo $old_description ?></textarea>
        </div>
        
        <div class="input-group">
            <label>Plan 3D</label>
            <input type="file" name="plan_3d" id="plan_3d" />
            <?php 
                if(!empty($old_plan_3d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_plan_3d.'" />';
                }
            ?>
        </div>
        
        <div class="input-group">
            <label>Plan 2D</label>
            <input type="file" name="plan_2d" id="plan_2d" />
            <?php 
                if(!empty($old_plan_2d))
                {
                    echo '<img width="200" src="'.$url_site.'/wp/wp-content/uploads/'.$old_plan_2d.'" />';
                }
            ?>
        </div>
        
        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-plan-list/?project='.$project_id ?>" title=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
<?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
    });
</script>
<?php get_footer(); ?>