<?php
global $current_user;
$data = array();
$project_id = $_GET['project'];
$delete_id = (isset($_GET['delete_id']) && !empty($_GET['delete_id'])) ? $_GET['delete_id'] : 0;

if(!isset($current_user) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){

    $helperList = new MeoCrmCoreListHelper();

    $baseDirFTP = $projects['realestate']['ftp']['base_dir'];

    $external_wpdb = MeoCrmCoreHelper::getExternalDbConnection($projects['realestate']['db']['login'],$projects['realestate']['db']['password'],$projects['realestate']['db']['name'],$projects['realestate']['db']['host']);

    $ftp = MeoCrmCoreHelper::getExternalFtpConnection($projects['realestate']['ftp']['login'], $projects['realestate']['ftp']['password'],$projects['realestate']['ftp']['host']);
    
    if(!empty($delete_id))
    {
        $nextStep = true;
        $url_length = strlen($projects['url']);
        $url_site = (substr($projects['url'], -1, 1) != '/')? $projects['url'] : substr($projects['url'], 0, ($url_length-1));
                
        $plan = MeoCrmRealestateManagement::getPlanById($external_wpdb, $delete_id);
        
        if(!empty($plan))
        {
            $plan_3d = $baseDirFTP.'uploads/'.$plan->plan_3d;
            $plan_2d = $baseDirFTP.'uploads/'.$plan->plan_2d;
            
            if($plan_3d != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $plan_3d))
                {
                    $nextStep = false;
                }
            }
            if($plan_2d != $baseDirFTP.'uploads/')
            {
                if(!ftp_delete($ftp, $plan_2d))
                {
                    $nextStep = false;
                }
            }
            
            if($nextStep)
            {
                $where = array('id' => $delete_id);
                $result = MeoCrmRealestateManagement::deletePlan($external_wpdb, $where);
                if(!$result['success'])
                {
                    echo 'erreur de suppression des données dans la DB !<br/>';
                }
            }  
        }else{
            echo 'Pas de développement avec cette ID<br/>';
        }
    }

    $resultsGetPlan = MeoCrmRealestateManagement::getAllPlan($external_wpdb);
    
    if(isset($resultsGetPlan) && !empty($resultsGetPlan))
    {
        foreach($resultsGetPlan as $result)
        {
            $data[$result->id]['id'] = $result->id;
            $data[$result->id]['title'] = $result->title;
            $data[$result->id]['description'] = $result->description;
        }
    }
    
    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => ''),
        array('key' => 'description', 'name' => 'Description', 'type_data' => 'base', 'class' => ''),
    ); 
    
    $list_action = array(
        array('url_action' => site_url().'/meo-crm-realestate-form-plan?project='.$project_id.'&id=@id', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id'),
        array('url_action' => site_url().'/meo-crm-realestate-plan-list?project='.$project_id.'&delete_id=@id', 'icon' => 'fa fa-trash', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );
    
    ?>
    <div class="wrap">
        <h2 id="meo-crm-realestate-list-title">Plans</h2>
        <div id="meo-crm-realestate-add">
            <a id="add-developpement" href="meo-crm-realestate-form-plan/?project=<?php echo $project_id ?>" title="Add plan">Ajouter un plan</a>
            <a id="back-btn" href="meo-crm-realestate-management-tool/?project=<?php echo $project_id ?>" title="Back to menu">Retour au menu</a>
        </div>
        <div class="clear-both"></div>
    </div>

    <?php echo $helperList->getList($data, $header, $list_action, false, 'id', true); ?>
<?php     
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?> 
<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){
        
    });
</script>
<?php get_footer(); ?>