<?php

class FloorModel
{
    public static function getAllFloor($external_wpdb)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' ';
        $query .= 'ORDER BY position ASC, building_id ASC';
        $results = $external_wpdb->get_results( $query );
        return $results;
    }

    public static function getAllFloorJoinBuilding($external_wpdb)
    {
        $query  = 'SELECT f.id, b.title AS building, f.title AS floor ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' AS f ';
        $query .= 'INNER JOIN '.getExternalPrefix().MEO_CRM_REALESTATE_BUILDING_TABLE.' AS b ON f.building_id = b.id ';
        $query .= 'ORDER BY f.position, f.building_id ASC';
        $results = $external_wpdb->get_results( $query );
        return $results;
    }

    public static function getFloorById($external_wpdb,$id)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' ';
        $query .= 'WHERE id='.$id;
        $results = $external_wpdb->get_results( $query );
        return $results;
    }

    public static function getFloorByBuildingId($external_wpdb,$id)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE.' ';
        $query .= 'WHERE building_id='.$id;
        $results = $external_wpdb->get_results( $query );
        return $results;
    }

    public static function insertFloor($external_wpdb,$datas)
    {
        $check = true;
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE,$datas) === false)
        {
            $check = false;
        }
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }

    public static function updateFloor($external_wpdb,$datas,$where)
    {
        $check = true;
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE,$datas,$where) === false)
        {
            $check = false;
        }
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }

    public static function deleteFloor($external_wpdb,$where)
    {
        $check = true;
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_FLOORS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
}
