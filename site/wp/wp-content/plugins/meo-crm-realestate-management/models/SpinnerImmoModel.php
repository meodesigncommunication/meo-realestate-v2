<?php

class SpinnerImmoModel
{
    
    public static function getAllSpinner($external_wpdb)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE.' ';
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getSpinnerById($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE.' ';
        $query .= 'WHERE id='.$id.' '; 
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getAttachmentBySpinnerId($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_POST_TABLE.' ';
        $query .= 'WHERE spinner_id='.$id.' '; 
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    /* START INSERT METHOD */
    public static function insertSpinner($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function insertSpinnerAttachment($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_POST_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function insertSpinnerPost($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_POSTS_TABLE,$datas) === false)
        {
            $check = false;
        } 
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function insertSpinnerPostMeta($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_POSTMETA_TABLE,$datas) === false)
        {
            $check = false;
        } 
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    /* START DELETE METHOD */
    public static function deleteSpinner($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return $check;
    }
    
    public static function deleteSpinnerAttachment($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_SPINNERS_POST_TABLE,$where) === false)
        {
            $check = false;
        }        
        return $check;
    }
    
    public static function deleteSpinnerPost($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_POSTS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return $check;
    }
    
    public static function deleteSpinnerPostMeta($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_POSTMETA_TABLE,$where) === false)
        {
            $check = false;
        }        
        return $check;
    }
}
