<?php

class AccessProjectModel
{
    /*
     *  Select project acces by id 
     */
    public static function selectAccessProjectById($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM ' . getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE . ' ';
        $query .= 'WHERE id='.$id.' ';
        
        $results = $wpdb->get_results($query); 
        
        return $results;
    }
    
    /*
     *  Select project acces by project_id 
     */
    public static function selectAccessProjectByProjectId($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE.' ';
        $query .= 'WHERE project_id='.$id.' ';
        
        $results = $wpdb->get_results($query); 
        
        if(isset($results[0]) && isset($results)){
            return $results[0];
        }else{
            return $results;
        }
    }
    
    /*
     * Insert datas
     */
    public static function insertAccessProject($datas)
    {
        global $wpdb;
        $check = true;
        
        if($wpdb->insert(getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE,$datas) === false)
        {
            $check = false;
        }
        
        if($check)
        {
            return array(
                'success' => true,
                'access_project_id' => $wpdb->insert_id
            );
        }else{
            return array(
                'success' => false,
                'access_project_id' => 0
            );
        }
        
    }
    
    /*
     * Update datas
     */
    public static function updateAccessProject($datas,$where)
    {
        global $wpdb;
        $check = true;
        
        if($wpdb->update(getExternalPrefix().MEO_CRM_ACCESS_PROJECT_TABLE,$datas,$where) === false)
        {
            $check = false;
        }
        
        if($check)
        {
            return array(
                'success' => true,
                'access_project_id' => $where['id']
            );
        }else{
            return array(
                'success' => false,
                'access_project_id' => 0
            );
        }
        
    }
}