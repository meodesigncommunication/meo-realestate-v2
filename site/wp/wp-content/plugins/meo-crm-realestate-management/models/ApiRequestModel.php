<?php

class ApiRequestModel
{
    // Init table variables
    public static $table_post = 'wp_posts';

    public static $table_lot = 'wp_meo_crm_realestate_lots';
    public static $table_plan = 'wp_meo_crm_realestate_plans';
    public static $table_floor = 'wp_meo_crm_realestate_floors';
    public static $table_status = 'wp_meo_crm_realestate_status';
    public static $table_sector = 'wp_meo_crm_realestate_sectors';
    public static $table_meta = 'wp_meo_crm_realestate_lot_metas';
    public static $table_spinner = 'wp_meo_crm_realestate_spinners';
    public static $table_building = 'wp_meo_crm_realestate_buildings';
    public static $table_floor_lot = 'wp_meo_crm_realestate_floor_lot';
    public static $table_meta_value = 'wp_meo_crm_realestate_lot_meta_value';
    public static $table_post_spinner = 'wp_meo_crm_realestate_spinner_post';
    public static $table_developpement = 'wp_meo_crm_realestate_developpements';
    public static $table_plan_lot_building = 'wp_meo_crm_realestate_plan_lot_building';
    public static $table_coordinates_plan_lot_building = 'wp_meo_crm_realestate_coordinates_plan_lot_building';
    public static $table_link_coordinates_plan_floor_lot_building = 'wp_meo_crm_realestate_link_coordinates_plan_floor_lot_building';

    /*
     * DEVELOPPEMENT QUERY METHOD
     */
    public static function selectDeveloppement($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_developpement.' ';
        return $external_wpdb->get_results($query);
    }
    public static function selectDeveloppementWhere($external_wpdb,$wheres = array())
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_developpement.' ';
        if(isset($where) && !empty($where))
        {
            $query .= 'WHERE ';
            foreach($wheres as $where)
            {
                $query .= $where['before'].' '.$where['key'].' '.$where['comparator'].' '.$where['value'].' ';
            }
        }
        return $external_wpdb->get_results($query);
    }

    /*
     * SECTOR QUERY METHOD
     */
    public static function selectSector($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_sector.' ';
        return $external_wpdb->get_results($query);
    }
    public static function selectSectorWhere($external_wpdb,$wheres = array())
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_sector.' ';
        if(isset($where) && !empty($where))
        {
            $query .= 'WHERE ';
            foreach($wheres as $where)
            {
                $query .= $where['before'].' '.$where['key'].' '.$where['comparator'].' '.$where['value'].' ';
            }
        }
        return $external_wpdb->get_results($query);
    }

    /*
     * BUILDING QUERY METHOD
     */
    public static function selectBuilding($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_building.' ';
        $query  .= 'ORDER BY title ASC ';
        return $external_wpdb->get_results($query);
    }
    public static function selectBuildingById($external_wpdb,$id)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_building.' ';
        $query  .= 'WHERE id='.$id.' ';
        return $external_wpdb->get_results($query);
    }
    public static function selectBuildingWhere($external_wpdb,$wheres = array())
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_building.' ';
        if(isset($where) && !empty($where))
        {
            $query .= 'WHERE ';
            foreach($wheres as $where)
            {
                $query .= $where['before'].' '.$where['key'].' '.$where['comparator'].' '.$where['value'].' ';
            }
        }
        $query  .= 'ORDER BY title ASC ';
        return $external_wpdb->get_results($query);
    }

    /*
     * SELECT ALL DATAS
     */
    public static function selectLot($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_lot.' ';
        $query  .= 'ORDER BY title ASC ';
        return $external_wpdb->get_results($query);
    }


    public static function selectLotRooms($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT l.rooms ';
        $query  .= 'FROM '.self::$table_lot.' AS l ';
        $query  .= 'GROUP BY l.rooms ';
        $query  .= 'ORDER BY l.rooms ASC ';
        return $external_wpdb->get_results($query);
    }

    public static function selectLotSurface($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT l.surface ';
        $query  .= 'FROM '.self::$table_lot.' AS l ';
        $query  .= 'GROUP BY l.surface ';
        $query  .= 'ORDER BY l.surface ASC ';
        return $external_wpdb->get_results($query);
    }

    public static function selectMeta($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_meta.' ';
        return $external_wpdb->get_results($query);
    }

    public static function selectMetaValue($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_meta_value.' ';
        return $external_wpdb->get_results($query);
    }

    public static function selectFloor($external_wpdb,$groupBy = false)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table_floor.' ';
        if($groupBy)
        {
            $query .= 'GROUP BY position ';
        }
        $query .= 'ORDER BY position ASC ';
        return $external_wpdb->get_results($query);
    }

    public static function selectFloorById($external_wpdb,$id)
    {
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table_floor.' ';
        $query .= 'WHERE id = '.$id.' ';
        return $external_wpdb->get_results($query);
    }

    public static function selectPlan($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_plan.' ';
        return $external_wpdb->get_results($query);
    }

    public static function selectStatus($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * 
                    FROM '.self::$table_status.' 
                    ORDER BY name';
        return $external_wpdb->get_results($query);
    }

    public static function selectFloorLot($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_floor_lot.' ';
        return $external_wpdb->get_results($query);
    }

    public static function selectCoordinatePlanLotBuilding($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_coordinates_plan_lot_building.' ';
        return $external_wpdb->get_results($query);
    }
    public static function selectLinkCoordinatePlanFloorLotBuilding($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_link_coordinates_plan_floor_lot_building.' ';
        return $external_wpdb->get_results($query);
    }
    public static function selectPlanFloorLotBuilding($external_wpdb)
    {
        global $wpdb;
        $query   = 'SELECT * ';
        $query  .= 'FROM '.self::$table_plan_lot_building.' ';
        return $external_wpdb->get_results($query);
    }
    
    /*
     * BUILDINGS + FULL LOTS QUERY METHOD
     * 
     * Generate a big array with all developpement informations
     * 
     */
    public static function selectBuildingsWithLot($external_wpdb)
    {
        $count = 0;
        $datas = array();

        $temp_lot = 0;
        $temp_status = 0;
        $temp_lot_total = 0;
        $sectors = self::selectSector($external_wpdb);
        $buildings = self::selectBuilding($external_wpdb);
        $lots = self::selectLot($external_wpdb);
        $status = self::selectStatus($external_wpdb);
        $metas = self::selectMeta($external_wpdb);
        $metas_value = self::selectMetaValue($external_wpdb);
        $plans = self::selectPlan($external_wpdb);
        $floors = self::selectFloor($external_wpdb);
        $floors_lot = self::selectFloorLot($external_wpdb);
        $coordinate_plan_lot_buildings = self::selectCoordinatePlanLotBuilding($external_wpdb);
        $link_coordinate_plan_lot_buildings = self::selectLinkCoordinatePlanFloorLotBuilding($external_wpdb);
        $plan_lot_buildings = self::selectPlanFloorLotBuilding($external_wpdb);

        foreach($buildings as $building)
        {
            foreach($status as $value)
            {
                $datas['buildings'][$building->id]['status_lot'][$value->name]['value'] = 0;
                $datas['buildings'][$building->id]['status_lot'][$value->name]['color'] = $value->color;
            }

            $datas['buildings'][$building->id]['count_lots'] = 0;
            $datas['buildings'][$building->id]['title'] = $building->title;
            $datas['buildings'][$building->id]['description'] = $building->description;
            $datas['buildings'][$building->id]['coordinate_plan_sector_2d'] = $building->coordinate_plan_sector_2d;
            $datas['buildings'][$building->id]['image_hover_plan_sector_2d'] = $building->image_hover_plan_sector_2d;
            $datas['buildings'][$building->id]['image_front_face'] = $building->image_front_face;
            $datas['buildings'][$building->id]['image_back_face'] = $building->image_back_face;
            $datas['buildings'][$building->id]['image_preview'] = $building->image_preview;
            $datas['buildings'][$building->id]['image_building_sector'] = $building->image_building_sector;


            foreach($sectors as $sector)
            {
                if($sector->id == $building->sector_id)
                {
                    $datas['buildings'][$building->id]['sector'] = $sector->title;
                    $datas['buildings'][$building->id]['plan_sector_2d'] = $sector->plan_sector_2d;
                }
            }

            foreach($floors as $floor)
            {
                if($building->id == $floor->building_id)
                {
                    $datas['buildings'][$building->id]['floors'][]['title'] = $floor->title;

                    foreach($floors_lot as $floor_lot)
                    {
                        if($floor_lot->floor_id == $floor->id)
                        {
                            foreach($lots as $lot)
                            {
                                if($floor_lot->lot_id == $lot->id)
                                {
                                    if($temp_lot_total != $lot->id)
                                    {
                                        $datas['buildings'][$building->id]['count_lots']++;
                                        $temp_lot_total = $lot->id;
                                    }
                                    //* LOTS *//
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['document_id'] = MeoCrmCoreCryptData::encodeAttachmentId($lot->document_id);
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['title'] = $lot->title;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['description'] = $lot->description;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['room'] = $lot->rooms;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['surface'] = $lot->surface;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['type_lot'] = $lot->type_lot;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['price'] = $lot->price;

                                    foreach($metas_value as $key_meta_value => $meta_value)
                                    {
                                        if($lot->id == $meta_value->lot_id)
                                        {
                                            foreach($metas as $meta)
                                            {
                                                if($meta->id == $meta_value->meta_id)
                                                {
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['value'] = $meta_value->value;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['key'] = $meta->meta_key;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['type'] = $meta->meta_type;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['slug'] = $meta->meta_slug;
                                                    $datas['buildings'][$building->id]['lots'][$lot->id]['meta_values'][$key_meta_value]['unit'] = (!empty($meta->meta_unit)) ? $meta->meta_unit : '';
                                                }
                                            }
                                        }
                                    }

                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['floor'] = $floor->title;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['entry'] = $floor_lot->lot_entry;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['position'] = $floor->position;
                                    $datas['buildings'][$building->id]['lots'][$lot->id]['floor_lot'][$floor->id]['plan'] = $floor_lot->plan_floor;

                                    foreach($plans as $plan)
                                    {
                                        if($plan->id == $lot->plan_id)
                                        {
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['title'] = $plan->title;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['description'] = $plan->description;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['document'] = (!empty($plan->document)) ? $plan->document : '';
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['plan_2d'] = $plan->plan_2d;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['plan_3d'] = $plan->plan_3d;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan']['spinner_id'] = $plan->spinner_id;
                                        }
                                    }

                                    $count = 0;
                                    foreach($coordinate_plan_lot_buildings as $coordinate_plan)
                                    {
                                        if($coordinate_plan->id == $floor_lot->status_plan_floor_id)
                                        {
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['title'] = $coordinate_plan->title;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['coordinate_plan_face'] = $coordinate_plan->coordinates_front;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['coordinate_plan_back'] = $coordinate_plan->coordinates_back;

                                            foreach($link_coordinate_plan_lot_buildings as $link_coordinate_plan)
                                            {

                                                if($link_coordinate_plan->coordinates_id == $coordinate_plan->id)
                                                {
                                                    foreach($plan_lot_buildings as $plan_lot_building)
                                                    {
                                                        $count_image = 0;
                                                        if($plan_lot_building->id == $link_coordinate_plan->plan_lot_id)
                                                        {
                                                            if($lot->status_id == $plan_lot_building->status_id)
                                                            {
                                                                if(!empty($plan_lot_building->front_face))
                                                                {
                                                                    if(!empty($plan_lot_building->hover))
                                                                    {
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['front']['hover'] = $plan_lot_building->image;
                                                                    }else{
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['front']['base'] = $plan_lot_building->image;
                                                                    }
                                                                }else{
                                                                    if(!empty($plan_lot_building->hover))
                                                                    {
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['back']['hover'] = $plan_lot_building->image;
                                                                    }else{
                                                                        $datas['buildings'][$building->id]['lots'][$lot->id]['plan_building_status'][$count]['images_plans'][$count_image]['back']['base'] = $plan_lot_building->image;
                                                                    }
                                                                }
                                                            }

                                                        }
                                                        $count_image++;
                                                    }
                                                }
                                            }

                                        }
                                        $count++;

                                    }

                                    /* 
                                     *  $coordinate_plan_lot_buildings = self::selectCoordinatePlanLotBuilding();
                                     *  $link_coordinate_plan_lot_buildings = self::selectLinkCoordinatePlanFloorLotBuilding();                                     *
                                     *  $coordinate_plan_lot_buildings = self::selectPlanFloorLotBuilding();
                                     */
                                    $lot_status_count = 1;
                                    foreach($status as $value)
                                    {
                                        if($value->id == $lot->status_id)
                                        {
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['status'] = $value->name;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['status_color'] = $value->color;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['hide_price'] = $value->hide_price;
                                            $datas['buildings'][$building->id]['lots'][$lot->id]['sold'] = $value->sold;
                                            $datas['buildings'][$building->id]['status_lot'][$value->name]['value']++;
                                            $temp_lot = $lot->id;
                                            $temp_status = $lot->status_id;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $datas;
    }
}