<?php

class LotModel
{
    public static function getAllLot($external_wpdb)
    {  
        $query  = '	SELECT *
                        FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE . ' ';
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getAllDocumentPost($external_wpdb)
    {    
		global $wpdb;
		    
        $query  = ' SELECT *
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_POSTS_TABLE . '     
                    WHERE post_mime_type LIKE "%application/pdf%"';        
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getLotById($external_wpdb,$id)
    {  	
        $query  = ' SELECT * 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE . ' AS l   
                    WHERE l.id='.$id;
        
        $results = $external_wpdb->get_results( $query );
        return (isset($results)) ? $results : array();
    }

    // LEFT JOIN CAR SI EX: PAS D ETAGE LIE LOT N EXISTE PAS
    public static function getFullDataLotById($external_wpdb,$id)
    {    		
        $query  = ' SELECT l.id AS lot_id, l.title AS lot_title, l.description AS lot_description, l.rooms AS lot_rooms, l.type_lot AS lot_type, l.surface AS lot_surface, l.price AS lot_price, l.status_id, l.document_id, l.plan_id, pbl.title AS plan_building_status_title, pbl.coordinates_front AS front_coordinate, pbl.coordinates_back AS back_coordinate, f.title AS floor_title, fl.status_plan_floor_id, f.id AS floor_id, fl.lot_entry, fl.plan_floor 
                    FROM ' . getExternalPrefix() . MEO_CRM_REALESTATE_LOTS_TABLE . ' AS l    
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_LOT_TABLE .' AS fl ON fl.lot_id = l.id    
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_COORDINATES_PLAN_LOT_BUILDING_TABLE .' AS pbl ON pbl.id = fl.status_plan_floor_id
                    LEFT JOIN ' . getExternalPrefix() . MEO_CRM_REALESTATE_FLOORS_TABLE .' AS f ON fl.floor_id = f.id
                    WHERE l.id=' . $id;
        
        $results = $external_wpdb->get_results( $query ); 		
        return (isset($results)) ? $results : array();
    }
    
    public static function insertLot($external_wpdb,$data)
    {
		global $wpdb;
		
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_LOTS_TABLE,$data) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateLot($external_wpdb,$datas,$where)
    {
		global $wpdb;
		
        $check = true;        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_LOTS_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
    
    public static function deleteLot($external_wpdb,$where)
    {
		global $wpdb;
		
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_LOTS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
}
