<?php

class DeveloppementModel
{
    
    public static function getAllDeveloppement($external_wpdb)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE.' ';        
        $results = $external_wpdb->get_results( $query );        
        return $results;
    }
    
    public static function getDeveloppementById($external_wpdb,$id)
    {                
        $query  = 'SELECT * ';
        $query .= 'FROM '.getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE.' ';       
        $query .= 'WHERE id='.$id;
        $results = $external_wpdb->get_results( $query );        
        return (isset($results[0]) && !empty($results[0]))? $results[0] : null ;
    }
    
    public static function insertDeveloppment($external_wpdb,$datas)
    {
        $check = true;        
        if($external_wpdb->insert(getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE,$datas) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $external_wpdb->insert_id
        ];
    }
    
    public static function updateDeveloppment($external_wpdb,$datas,$where)
    {
        $check = true;        
        if($external_wpdb->update(getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE,$datas,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
    
    public static function deleteDeveloppment($external_wpdb,$where)
    {
        $check = true;        
        if($external_wpdb->delete(getExternalPrefix().MEO_CRM_REALESTATE_DEVELOPPEMENTS_TABLE,$where) === false)
        {
            $check = false;
        }        
        return [
            'success' => $check,
            'id' => $where['id']
        ];
    }
}
