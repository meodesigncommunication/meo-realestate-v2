<?php

class MeoCrmCoreExternalDbQuery
{
    public static function select($external_wpdb, $query, $type_array = OBJECT)
    {
        $resultQuery = $external_wpdb->get_results( $query, $type_array );

        return $resultQuery;
    }

    public static function insert($external_wpdb, $table, $data)
    {
        $check = true;
        if($external_wpdb->insert($table,$data) === false)
        {
            $check = false;
        }
        return [
            'success' => $check,
            'id' =>  ($check) ? $external_wpdb->insert_id : 0
        ];
    }

    public static function update($external_wpdb,$table,$data,$where)
    {
        $check = true;
        if($external_wpdb->update($table,$data,$where) === false)
        {
            $check = false;
        }
        return [
            'success' => $check
        ];
    }

    public static function delete($external_wpdb,$table,$where)
    {
        $check = true;
        if($external_wpdb->delete($table,$where) === false)
        {
            $check = false;
        }
        return [
            'success' => $check
        ];
    }
}