/* 
 * Name:       MEO CRM REALESTATE SALE LIST
 * Author:     MEO
 * Author URI: http://www.meo-realestate.com/   
 */

window.$ = jQuery;

$(document).ready(function(){
    
    $('#project-list-sale').change(function(){
        refreshSaleList($(this).val());
    });
        
    $(document).on("dblclick","#sale-list-content table td.editable",function() {console.log('sales');
        $('td.updated_list').each(function(){
            if($(this).hasClass('editable'))
            {
                $(this).find('p').show();
                $(this).find('ul.show-data').show();
                $(this).find('.form-input-list').hide();
            }else{
                $(this).addClass('editable');
                $(this).find('p').show();
                $(this).find('ul.show-data').show();
                $(this).find('.form-input-list').hide();
            }
        });
        $(this).removeClass('editable');
        $(this).find('p').hide();
        $(this).find('ul.show-data').hide();
        $(this).find('.form-input-list').show();
    });
    
    // Pour ayant besoin de la fonction principalement page contact !!
    if($('#sale-container-contact').length)
    {
        var project_id = $('input[name="project_id_sale"]').val();
        var contact_id = $('input[name="contact_id_sale"]').val();
        
        refreshSaleListContact(project_id,contact_id);
    }
    
});

/*
 * 
 */
function refreshSaleListContact(project_id,contact_id)
{
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'getSaleListByProjectId',
            'project_id': project_id,
            'contact_id': contact_id,
            'show_contact': true
        },
        function(response){
            $('#sale-container-contact').html(response);
        }
    );
}

/*
 * 
 */
function refreshSaleList(project_id)
{
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'getSaleListByProjectId',
            'project_id': project_id
        },
        function(response){
            $('#sale-list-content').html(response);
        }
    );
}

function addObjectList(element)
{
    var objects_value = [];
    var project_id = $('input[name="project_id_sale"]').val();
    
    $('.select-object-list').each(function(){
        objects_value.push($(this).val());
    });
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'selectListObject',
            'object_id': objects_value,
            'project_id': project_id
        },
        function(response){
            $('#bloc-object select:last-child').after('<br/>'+response);
        }
    );
}

// Add object select in edit a existing row
function addObjectUpdateList(element)
{    
    var html_option = $('#add_new_entry #bloc-object .select-object-list').html();
    var html_button = '<input type="button" class="btn-remove" name="remove-objects" value="remove" onclick="removeObjectList(this)" />';
    var select = '<li><select class="list_objects" onchange="changeSelectOption(this)" name="list_objects">'+html_option+'</select>'+html_button+'</li>';
    console.log($(element).parent().find('ul').html());
    if($(element).parent().find('ul').html() == '')
    {
        console.log('HTML <UL> IS EMPTY');
        $(element).parent().find('ul').html(select);
    }else{
        console.log('HTML <UL> IS\'NT EMPTY');
        $(element).parent().find('ul li:last-child').after(select);
    }    
}

function changeSelectOption(element)
{
    var html = '';
    var tabOption = {};    

    $(element).find('option').each(function(){         
        var value = $(this).attr('value');
        var content = $(this).html();        
        if($(element).val() != value){
            if(value != 0)
            {
                tabOption[value] = content;
            }
        }
    });  
    
    $('.list_objects').each(function(){
        var select = this;
        var selected = $(this).val();
        $(this).find('option').each(function(){
            if($(this).is(':selected'))
            {
            }else{
                $(this).remove();
            }
        });
        $.each(tabOption,function(key,value){
            $(select).append($("<option></option>")
                    .attr("value",key)
                    .text(value));
        });
    });
}

/*
 * Create a new entry
 */
function saveSale()
{    
    var project_id = $('input[name="project_id_sale"]').val();
    var contact_id = $('input[name="contact_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
        contact_id = $('.contact_list').val();
    }
    
    var objects_value = [];    
    $('.select-object-list').each(function(){
        objects_value.push($(this).val());
    });    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'saveSale',
            'project_id': project_id,
            'contact_id': contact_id,
            'lot_id': $('.list_lot').val(),
            'status_id': $('.list_status').val(),
            'objects_id': objects_value,
        },
        function(response){
            if(response)
            {
                if($('#sale-container-contact').length)
                {
                    var project_id = $('input[name="project_id_sale"]').val();
                    var contact_id = $('input[name="contact_id_sale"]').val();

                    refreshSaleListContact(project_id,contact_id);
                }else{
                    refreshSaleList($('#project-list-sale').val());
                }                
            }
        }
    );
}

/*
 * 
 */
function removeObjectList(element)
{
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    var sale_id = $(element).parents('tr').find('#sale_id').val();
    var object_id = $(element).parent().find('select option:first-child').attr('value');    
    if(object_id != 0)
    {
        $.post(
            meo_crm_object_js_object.ajax_url,
            {
                'action': 'deleteObjectBySaleId',
                'sale_id': sale_id,
                'object_id': object_id
            },
            function(response){
                if(response)
                {
                    if($('#sale-container-contact').length)
                    {
                        var project_id = $('input[name="project_id_sale"]').val();
                        var contact_id = $('input[name="contact_id_sale"]').val();

                        refreshSaleListContact(project_id,contact_id);
                    }else{
                        refreshSaleList($('#project-list-sale').val());
                    }   
                }else{
                    alert('une erreur s\'est produite durant la suppression de l\'objet' );
                }
            }
        );
    }else{
        $(element).parent().remove();
    }
}

/*
 * 
 */
function updateSaleContact(element)
{
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    var value = $(element).parent().find('.contact_list').val();    
    var sale_id = $(element).parents('tr.update_entry').find('#sale_id').val();
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'updateSale',
            'sale_id': sale_id,
            'dataUpdate': 'contact_id',
            'value': value
        },
        function(response){
            if(response)
            {
                if($('#sale-container-contact').length)
                {
                    var project_id = $('input[name="project_id_sale"]').val();
                    var contact_id = $('input[name="contact_id_sale"]').val();

                    refreshSaleListContact(project_id,contact_id);
                }else{
                    refreshSaleList($('#project-list-sale').val());
                }   
            }else{
                alert('une erreur s\'est produite durant l\'update de la vente' );
            }
        }
    );
}

/*
 * 
 */
function updateSaleLot(element)
{
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    var value = $(element).parent().find('.list_lot').val();    
    var sale_id = $(element).parents('tr.update_entry').find('#sale_id').val();
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'updateSale',
            'sale_id': sale_id,
            'dataUpdate': 'lot_id',
            'value': value
        },
        function(response){
            if(response)
            {
                if($('#sale-container-contact').length)
                {
                    var project_id = $('input[name="project_id_sale"]').val();
                    var contact_id = $('input[name="contact_id_sale"]').val();

                    refreshSaleListContact(project_id,contact_id);
                }else{
                    refreshSaleList($('#project-list-sale').val());
                }   
            }else{
                alert('une erreur s\'est produite durant l\'update de la vente' );
            }
        }
    );
}

/*
 * 
 */
function updateSaleLotStatus(element)
{
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    var lot_id = $(element).parents('tr.update_entry').find('select.list_lot').val();
    var status_id = $(element).parents('tr.update_entry').find('select#sale_lot_status').val();
    
    console.log('lot = '+lot_id+' | status = '+status_id)
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'updateSaleLotStatus',
            'project_id': project_id,
            'lot_id': lot_id,
            'status_id': status_id
        },
        function(response){
            if(response)
            {
                if($('#sale-container-contact').length)
                {
                    var project_id = $('input[name="project_id_sale"]').val();
                    var contact_id = $('input[name="contact_id_sale"]').val();

                    refreshSaleListContact(project_id,contact_id);
                }else{
                    refreshSaleList($('#project-list-sale').val());
                }   
            }else{
                alert('une erreur s\'est produite durant l\'update de la vente' );
            }
        }
    );
}

/*
 * UPDATE THE OBJECT LIST OF SALE
 */
function updateObjects(element)
{
    var values = [];
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    var sale_id = $(element).parents('tr.update_entry').find('#sale_id').val();
    
    $(element).parent().find('ul li').each(function(){
        values.push($(this).find('.list_objects').val());
    });
    
    console.log(sale_id);
    console.log(values);
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'updateObjectsBySaleId',
            'sale_id': sale_id,
            'objects': values
        },
        function(response){
            if(response)
            {
                if($('#sale-container-contact').length)
                {
                    var project_id = $('input[name="project_id_sale"]').val();
                    var contact_id = $('input[name="contact_id_sale"]').val();

                    refreshSaleListContact(project_id,contact_id);
                }else{
                    refreshSaleList($('#project-list-sale').val());
                }   
            }else{
                alert('une erreur s\'est produite durant l\'update des objets' );
            }
        }
    );
}

/*
 * DELETING SALE
 */
function deleteSale(sale_id)
{

    var project_id = $('#primary').attr('data-project');

    alert(' !! DELETE SALE !! Project ID => '+project_id);
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'deleteSaleBySaleId',
            'sale_id': sale_id,
            'project_id': project_id
        },
        function(response){
            console.log(response);
            if(response == 1)
            {
                if($('#sale-container-contact').length)
                {
                    console.log('if');
                    var project_id = $('input[name="project_id_sale"]').val();
                    var contact_id = $('input[name="contact_id_sale"]').val();
                    refreshSaleListContact(project_id,contact_id);
                }else{
                    var project_id = $('#project-list-sale').val();
                    console.log('else project = '+project_id);
                    refreshSaleList(project_id);
                }  
            }else{
                alert('une erreur s\'est produite durant la suppression de la vente' );
            }
        }
    );
}

/*
 * 
 */
function change_status_add(element)
{
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    var lot_id = $(element).val();
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'statusLot',
            'project_id': project_id,
            'lot_id': lot_id
        },
        function(response){
            $(element).parents('#add_new_entry').find('.list_status option[value="'+response+'"]').prop('selected', true);
        }
    );
}

/*
 * 
 */
function change_status_update_lot(element)
{
    var project_id = $('input[name="project_id_sale"]').val();
    
    if($('#project-list-sale').length)
    {
        project_id = $('#project-list-sale').val();
    }
    
    console.log(project_id);
    
    var lot_id = $(element).val();
    
    $.post(
        meo_crm_object_js_object.ajax_url,
        {
            'action': 'statusLot',
            'project_id': project_id,
            'lot_id': lot_id
        },
        function(response){
            $(element).parents('.update_entry').find('.list_status option[value="'+response+'"]').prop('selected', true);
            var txt = $(element).parents('.update_entry').find('.list_status option[value="'+response+'"]').html();
            $(element).parents('.update_entry').find('.list_status').parents('.updated_list').find('p').html(txt);
        }
    );
}