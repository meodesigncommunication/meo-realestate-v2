/* 
 * Name:       MEO CRM REALESTATE OBJECT
 * Author:     MEO
 * Author URI: http://www.meo-realestate.com/   
 */

window.$ = jQuery;

$(document).ready(function(){
    $('select[name="object_type_id"]').change(function(){
        $.post(
            meo_crm_object_js_object.ajax_url,
            {
                'action': 'getMetaFormObject',
                'id_object': $('#id_object').val(),
                'type_id': $(this).val()
            },
            function(response){
                console.log(response);
                $('#table-meta-form-object').html(response);
            }
        );
    });
});

