<?php 

/*
Plugin Name: Meo Crm Realestate Sale Managment
Description: Plugin to manage contact sale
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/   
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/
# Globals

error_reporting(E_ALL);
ini_set('display_errors', 1);

global $meo_crm_realestate_management_db_version;

//Init variables
$plugin_root = plugin_dir_path( __FILE__ );

// Define
define('MEO_REALESTATE_SALE_MANAGEMENT_PLUGIN_ROOT', $plugin_root);

define('MEO_REALESTATE_SALE_MANAGEMENT_SLUG', 'meo-crm-realestate-sale-management');
define('MEO_CRM_REALESTATE_SALE_MANAGER_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-sale-list-front.php');

define('MEO_CRM_REALESTATE_OBJECT_LIST_MANAGEMENT_SLUG', 'meo-crm-realestate-object-list');
define('MEO_CRM_REALESTATE_OBJECT_LIST_MANAGEMENT_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-object-list.php');

define('MEO_CRM_REALESTATE_OBJECT_FORM_MANAGEMENT_SLUG', 'meo-crm-realestate-object-form');
define('MEO_CRM_REALESTATE_OBJECT_FORM_MANAGEMENT_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-object-form.php');

define('MEO_CRM_REALESTATE_OBJECT_TYPE_LIST_MANAGEMENT_SLUG', 'meo-crm-realestate-object-type-list');
define('MEO_CRM_REALESTATE_OBJECT_TYPE_LIST_MANAGEMENT_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-object-type-list.php');

define('MEO_CRM_REALESTATE_OBJECT_TYPE_FORM_MANAGEMENT_SLUG', 'meo-crm-realestate-object-type-form');
define('MEO_CRM_REALESTATE_OBJECT_TYPE_FORM_MANAGEMENT_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-object-type-form.php');

define('MEO_CRM_REALESTATE_OBJECT_META_LIST_MANAGEMENT_SLUG', 'meo-crm-realestate-object-meta-list');
define('MEO_CRM_REALESTATE_OBJECT_META_LIST_MANAGEMENT_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-object-meta-list.php');

define('MEO_CRM_REALESTATE_OBJECT_META_FORM_MANAGEMENT_SLUG', 'meo-crm-realestate-object-meta-form');
define('MEO_CRM_REALESTATE_OBJECT_META_FORM_MANAGEMENT_TPL', '../../meo-crm-realestate-sale-management/views/frontend/meo-crm-realestate-object-meta-form.php');

// Table Defined
define('MEO_REALESTATE_SALE_OBJECT_TABLE', 				'meo_crm_realestate_sale_objects');
define('MEO_REALESTATE_SALE_MANAGEMENT_TABLE', 			'meo_crm_realestate_sale_management');
define('MEO_REALESTATE_SALE_TYPE_OBJECT_TABLE', 		'meo_crm_realestate_sale_type_object');
define('MEO_REALESTATE_SALE_META_OBJECT_TABLE', 		'meo_crm_realestate_sale_object_metas');
define('MEO_REALESTATE_SALE_MANAGEMENT_OBJECT_TABLE', 	'meo_crm_realestate_sale_management_object');
define('MEO_REALESTATE_SALE_META_VALUE_OBJECT_TABLE', 	'meo_crm_realestate_sale_object_meta_values');

// AJAX FILES FUNCTIONS
require_once 'ajax/meo-crm-realestate-sale-ajax.php';
require_once 'ajax/meo-crm-realestate-object-ajax.php';

// MODELS
require_once 'models/meo-crm-realestate-sale-model.php';
require_once 'models/meo-crm-realestate-object-model.php';
require_once 'models/meo-crm-realestate-type-object-model.php';
require_once 'models/meo-crm-realestate-meta-object-model.php';

$meo_crm_realestate_sale_management_db_version = '1.0';

# Plugin activation
function meo_crm_realestate_sale_management_activate () {
   global $wpdb;
   global $meo_crm_realestate_sale_management_db_version;
   
    $installed_dependencies = false;
    if ( is_plugin_active( 'meo-crm-core/meo-crm-core.php' )) {
            $installed_dependencies = true;
    }
    
    if(!$installed_dependencies) {
        
        // WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
        // and this will prevent WordPress to activate the plugin.
        echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Core plugins before', 'meo-realestate').'</h3></div>';
        
        //Adding @ before will prevent XDebug output
        @trigger_error(__('Please install and activate the MEO CRM Core plugins before.', 'meo-realestate'), E_USER_ERROR);
        exit;
        
    }else{

    	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	
        $charset_collate = $wpdb->get_charset_collate();
        
        # Realestate Sale       
        $table_name = $wpdb->prefix . MEO_REALESTATE_SALE_MANAGEMENT_TABLE;        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            project_id INTEGER(11) NOT NULL,
            contact_id INTEGER(11) NOT NULL,
            lot_id INTEGER(11) NOT NULL,
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";        
        dbDelta( $sql );  
        
        # Realestate Sale Object       
        $table_name = $wpdb->prefix . MEO_REALESTATE_SALE_TYPE_OBJECT_TABLE;        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            type VARCHAR(255) NOT NULL,
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";        
        dbDelta( $sql ); 
        
        # Realestate Sale Object       
        $table_name = $wpdb->prefix . MEO_REALESTATE_SALE_OBJECT_TABLE;        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            project_id INTEGER(11) NOT NULL,
            object_type_id INTEGER(11) NOT NULL,
            title VARCHAR(255) NOT NULL,
            description TEXT() NOT NULL,
            price FLOAT(8,2) NOT NULL,
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";        
        dbDelta( $sql );  
        
        # Realestate Sale Meta Object        
        $table_name = $wpdb->prefix . MEO_REALESTATE_SALE_META_OBJECT_TABLE;        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            object_type_id INTEGER(11) NOT NULL,
            meta_key VARCHAR(255) NOT NULL,
            meta_slug VARCHAR(255) NOT NULL,
            meta_order INTEGER(11) NULL,
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";        
        dbDelta( $sql ); 
        
        # Realestate Sale Meta Object Value     
        $table_name = $wpdb->prefix . MEO_REALESTATE_SALE_META_VALUE_OBJECT_TABLE;        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            object_id INTEGER(11) NOT NULL,
            meta_id INTEGER(11) NOT NULL,
            value VARCHAR(255) NOT NULL,
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";        
        dbDelta( $sql ); 
        
        # Realestate Sale       
        $table_name = $wpdb->prefix . MEO_REALESTATE_SALE_MANAGEMENT_OBJECT_TABLE;        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            id INTEGER(11) NOT NULL AUTO_INCREMENT,
            sale_id INTEGER(11) NOT NULL,
            object_id INTEGER(11) NOT NULL,            
            updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at DATETIME DEFAULT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";        
        dbDelta( $sql ); 
        
        add_option( 'meo_crm_realestate_sale_management_db_version', $meo_crm_realestate_sale_management_db_version );
    }
}
register_activation_hook( __FILE__, 'meo_crm_realestate_sale_management_activate' );

# Add Scripts and Styles
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_script_style_sale' );
add_action( 'wp_enqueue_scripts', 'load_custom_wp_admin_script_style_sale' );
function load_custom_wp_admin_script_style_sale() {
    # JS
    wp_register_script( 'meo_crm_sale_management_js','/wp-content/plugins/meo-crm-realestate-sale-management/js/meo-crm-realestate-sale-list.js',false,'1.0.0',true);
    wp_enqueue_script( 'meo_crm_sale_management_js' );
    
    wp_register_script( 'meo_crm_object_js',  '/wp-content/plugins/meo-crm-realestate-sale-management/js/meo-crm-realestate-object.js', false, '1.0.0', true );
    wp_enqueue_script( 'meo_crm_object_js' );
    
    wp_localize_script( 'meo_crm_object_js', 'meo_crm_object_js_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    
    # CSS
    wp_register_style( 'meo_crm_sale_management_css',  '/wp-content/plugins/meo-crm-realestate-sale-management/css/meo-crm-realestate-sale.css', false, '1.0.0' );
    wp_enqueue_style( 'meo_crm_sale_management_css' );
    
    wp_register_style( 'meo_crm_object_management_css',  '/wp-content/plugins/meo-crm-realestate-sale-management/css/meo-crm-realestate-object.css', false, '1.0.0' );
    wp_enqueue_style( 'meo_crm_object_management_css' );
}

# Add plugin menu in admin navigation
add_action( 'admin_menu', 'meo_crm_realestate_sale_admin_menu' );
function meo_crm_realestate_sale_admin_menu() {
    $page_title = 'Sale';
    $menu_title = 'Sale';
    $capability = 'manage_options';
    $menu_slug = 'manage_sale';
    $function = 'page_admin_manage_sale';
    $icon_url = 'dashicons-cart';
    $position = 8;
    add_menu_page ( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}
    // Views admin page for sale management
    function page_admin_manage_sale() {   
        include_once 'views/backend/meo-crm-realestate-sale-list.php';
    }
    
# Add plugin menu in admin navigation
add_action( 'admin_menu', 'meo_crm_realestate_object_admin_menu' );
function meo_crm_realestate_object_admin_menu() {
    $page_title = 'Object';
    $menu_title = 'Object';
    $capability = 'manage_options';
    $menu_slug = 'manage_object';
    $function = 'page_admin_manage_object';
    $icon_url = 'dashicons-pressthis';
    $position = 7;
    add_menu_page ( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );    
    add_submenu_page($menu_slug,'Ajouter objet','Ajouter objet',$capability,'add_object','page_admin_add_object');
    add_submenu_page($menu_slug,'Meta objet','Meta objet',$capability,'manage_meta_object','page_admin_manage_meta_object');
    add_submenu_page($menu_slug,'Ajouter meta objet','Ajouter meta objet',$capability,'add_meta_object','page_admin_add_meta_object');
    add_submenu_page($menu_slug,'Type objet','Type objet',$capability,'manage_type_object','page_admin_manage_type_object');
    add_submenu_page($menu_slug,'Ajouter type objet','Ajouter type objet',$capability,'add_type_object','page_admin_add_type_object');
}

// Views admin page for object list
function page_admin_manage_object() {    
    include_once 'views/backend/meo-crm-realestate-object-list.php';
}
// Views admin page form add object
function page_admin_add_object() {    
    include_once 'views/backend/meo-crm-realestate-object-form.php';
}
// Views admin page for type object
function page_admin_manage_meta_object() {    
    include_once 'views/backend/meo-crm-realestate-object-meta-list.php';
}
// Views admin page for type object
function page_admin_add_meta_object() {    
    include_once 'views/backend/meo-crm-realestate-object-meta-form.php';
}
// Views admin page for type object
function page_admin_manage_type_object() {    
    include_once 'views/backend/meo-crm-realestate-type-object-list.php';
}
// Views admin page for type object
function page_admin_add_type_object() {    
    include_once 'views/backend/meo-crm-realestate-type-object-form.php';
}

# Register Plugin templates

    add_filter('meo_crm_core_templates_collector', 'meo_crm_realestate_sale_templates_register', 1, 1);
    function meo_crm_realestate_sale_templates_register($pluginsTemplates){
        $pluginsTemplates[MEO_CRM_REALESTATE_SALE_MANAGER_TPL] = 'MEO CRM Sale Management';
        $pluginsTemplates[MEO_CRM_REALESTATE_OBJECT_LIST_MANAGEMENT_TPL] = 'MEO CRM Sale Management List object';
        $pluginsTemplates[MEO_CRM_REALESTATE_OBJECT_FORM_MANAGEMENT_TPL] = 'MEO CRM Sale Management Form object';
        $pluginsTemplates[MEO_CRM_REALESTATE_OBJECT_TYPE_LIST_MANAGEMENT_TPL] = 'MEO CRM Sale Management List Type Object';
        $pluginsTemplates[MEO_CRM_REALESTATE_OBJECT_TYPE_FORM_MANAGEMENT_TPL] = 'MEO CRM Sale Management Form Type Object';
        $pluginsTemplates[MEO_CRM_REALESTATE_OBJECT_META_LIST_MANAGEMENT_TPL] = 'MEO CRM Sale Management List Meta Object';
        $pluginsTemplates[MEO_CRM_REALESTATE_OBJECT_META_FORM_MANAGEMENT_TPL] = 'MEO CRM Sale Management Form Meta Object';

        return $pluginsTemplates;
    }

    add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_realestate_sale_pages_register' );
    function meo_crm_realestate_sale_pages_register($pluginsPages) {
        # Create Page for Users Availability
        $pluginsPages[MEO_REALESTATE_SALE_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_SALE_MANAGER_TPL);
        $pluginsPages[MEO_CRM_REALESTATE_OBJECT_LIST_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_OBJECT_LIST_MANAGEMENT_TPL);
        $pluginsPages[MEO_CRM_REALESTATE_OBJECT_FORM_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_OBJECT_FORM_MANAGEMENT_TPL);
        $pluginsPages[MEO_CRM_REALESTATE_OBJECT_TYPE_LIST_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_OBJECT_TYPE_LIST_MANAGEMENT_TPL);
        $pluginsPages[MEO_CRM_REALESTATE_OBJECT_TYPE_FORM_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_OBJECT_TYPE_FORM_MANAGEMENT_TPL);
        $pluginsPages[MEO_CRM_REALESTATE_OBJECT_META_LIST_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_OBJECT_META_LIST_MANAGEMENT_TPL);
        $pluginsPages[MEO_CRM_REALESTATE_OBJECT_META_FORM_MANAGEMENT_SLUG] = array(
            'post_title' => 'MEO CRM REALESTATE Sale Management',
            'post_content' => 'Page to be used for manage the contact lot sale ',
            '_wp_page_template' => MEO_CRM_REALESTATE_OBJECT_META_FORM_MANAGEMENT_TPL);


        return $pluginsPages;
    }
    
    /*
     * $all_contacts = MeoCrmContacts::getContacts('WHERE id="'.$_GET['id'].'"', ARRAY_A);
     * $contact = meo_crm_contacts_prepareContactCellsFrontIndividual($contact);
     */    
	if(!array_key_exists('HTTP_REFERER', $_SERVER) || (array_key_exists('HTTP_REFERER', $_SERVER) && $_SERVER['HTTP_REFERER'] != home_url().'/'.MEO_CONTACTS_SLUG.'/')){
    	add_filter('meocrmcontacts_getContactData', 'meo_crm_sale_contact_getContactData');
	}

    function meo_crm_sale_contact_getContactData($contact){
        
        // INIT VARIABLE
        $project_id = $contact['project_id'];
        
        // GET ACCESS PROJECT
        $access = AccessProjectModel::selectAccessProjectByProjectId($project_id);
        $project = meo_crm_projects_getProjectInfo($project_id);
        
        if($project){
        
            // INIT CLASS
            $meoCrmCoreCryptData = new MeoCrmCoreCryptData($project['url']);

            // INIT VARIABLE TO CONNECT TO EXTERNAL DB
            if(isset($access->host_db) && isset($access->name_db) && isset($access->login_db) && isset($access->password_db))
            {
                $hostDB     = $meoCrmCoreCryptData->decode($access->host_db);
                $nameDB     = $meoCrmCoreCryptData->decode($access->name_db);
                $loginDB    = $meoCrmCoreCryptData->decode($access->login_db);
                $passwordDB = $meoCrmCoreCryptData->decode($access->password_db);
            }else{
                $hostDB     = '';
                $nameDB     = '';
                $loginDB    = '';
                $passwordDB = '';
            }

            // CONNECT TO THE EXTERNAL DB
            $external_wpdb = new wpdb($loginDB,$passwordDB,$nameDB,$hostDB);

            $where = array(
                array(
                    'index' => 's.contact_id',
                    'compare' => '=',
                    'value' => $contact['id'],
                )
            );

            $result = SaleManagementModel::selectSaleContactLotObject($external_wpdb,$where);

            $contact['sales'] = $result;
        }
        
        return $contact;
    }


/*
 *  ACTION ADD ITEM IN TOOL REALESTATE
 */
add_action( 'list_item_realestate_tool', 'meo_crm_realestate_sale_object_item' );
function meo_crm_realestate_sale_object_item()
{
    $html  = '  <div class="icon-item">
                    <span class="icon-form" data-form="object">
                        <i class="fa fa-cubes" aria-hidden="true"></i>
                        <p>Objets</p>
                    </span>
                </div>';

    $html .= '  <div class="icon-item">
                    <span class="icon-form" data-form="object-type">
                        <i class="fa fa-object-group" aria-hidden="true"></i>
                        <p>Types d\'objet</p>
                    </span>
                </div>';

    $html .= '  <div class="icon-item">
                    <span class="icon-form" data-form="object-meta">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <!--<i class="fa fa-cubes" aria-hidden="true"></i>-->
                        <p>Metadonn&eacutee d\'objets</p>
                    </span>
                </div>';

    echo $html;
}