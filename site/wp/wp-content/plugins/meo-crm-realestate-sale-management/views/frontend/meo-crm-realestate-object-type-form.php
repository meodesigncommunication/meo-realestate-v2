<?php
//echo 'test';exit();
global $current_user;
$check_data = true;
$errors = array();
$input_class_type = '';
$project_id = $_GET['project'];
$project = meo_crm_projects_getProjectInfo($project_id);
$object_type_id = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : 0 ;

// IS METHOD POST SAVE DATA
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(!empty($object_type_id))
    {
        $data = array();
        $data['type'] = $_POST['type'];

        if(empty($data['type'])){ $check_data = false; $input_class_type = 'warningField'; }

        if(!empty($data))
        {
            $where = array('id' => $object_type_id);
            $resultUpdate = TypeObjectModel::updateType($where,$data);

            if($resultUpdate['valid'])
            {
                header('Location:/meo-crm-realestate-object-type-form/?project='.$project_id.'&id='.$object_type_id);
            }
        }

    }else{

        // Enregistrer les datas dans la DB
        $data = array();
        $data['type'] = $_POST['type'];

        if(empty($data['type'])){ $check_data = false; $input_class_type = 'warningField'; }

        if($check_data)
        {
            $resultInsert = TypeObjectModel::insertType($data);
        }else{
            $resultInsert = array(
                'valid' => 0
            );
        }

        if($resultInsert['valid'])
        {
            $insert_id = $resultInsert['id'];

            if(isset($insert_id) && !empty($insert_id))
            {
                header('Location:/meo-crm-realestate-object-type-form/?project='.$project_id.'&id='.$insert_id);
            }

        }else{
            $errors[] = 'Le titre ou la description ou les coordonn&eacute;e du secteur ne se sont pas enregistr&eacute; correctement !!';
            $input_class = 'warningField';
            echo 'error';
        }
    }

    if(is_array($errors) && !empty($errors))
    {
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error form sector " . print_r($errors));
    }
}

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

if(!empty($object_type_id))
{
    $object_type = TypeObjectModel::getTypeObjectById($object_type_id);
    $old_name = (!empty($object_type[0]->type)) ? $object_type[0]->type : '';
}else{
    $old_name = '';
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){
    ?>
    <form method="post" name="add_status" id="form_status" action="<?php //echo site_url('/meo-crm-realestate-form-status/').'?project='.$project_id.'&id='.$object_type_id ?>">
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
        <h2>Type d'objet</h2>

        <div class="input-group">
            <label>Nom du type</label>
            <input type="text" name="type" id="type" value="<?php echo $old_name ?>" placeholder="Nom du type" class="<?php echo $input_class_type ?>" />
        </div>

        <div class="group-btn-form">
            <input type="submit" name="send_form" id="send_form" value="enregistrer" />
            <a href="<?php echo site_url().'/meo-crm-realestate-object-type-list/?project='.$project_id ?>" name=""><input type="button" name="cancel_form" id="cancel_form" value="retour" /></a>
        </div>
    </form>
    <?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?>
    <script type="text/javascript">
        window.$ = jQuery;
        $(document).ready(function(){
        });
    </script>
<?php get_footer(); ?>