<?php
global $current_user;
$data = array();
$project_id = $_GET['project'];
$delete_id = (isset($_GET['delete_id']) && !empty($_GET['delete_id'])) ? $_GET['delete_id'] : 0;

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects) {

    $helperList = new MeoCrmCoreListHelper();

    $metas = MetaObjectModel::getListMetaTypeObjects();

    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'type', 'name' => 'Type d\'objet', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'meta_key', 'name' => 'Metadonnées', 'type_data' => 'base', 'class' => ''),
        array('key' => 'meta_slug', 'name' => 'Slug', 'type_data' => 'base', 'class' => ''),
    );

    $list_action = array(
        array('url_action' => get_admin_url().'meo-crm-realestate-object-meta-form/?project='.$project_id.'&id=@id', 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );

    ?>

    <div class="wrap">
        <h2 id="meo-crm-realestate-list-title">Gestion des objets</h2>
        <div id="meo-crm-realestate-add">
            <a id="add-developpement" href="meo-crm-realestate-object-meta-form/?project=<?php echo $project_id ?>" title="Add plan">Ajouter un objet</a>
            <a id="back-btn" href="meo-crm-realestate-management-tool/?project=<?php echo $project_id ?>" title="Back to menu">Retour au menu</a>
        </div>
        <div class="clear-both"></div>
    </div>
    <?php echo $helperList->getList($metas, $header, $list_action, false, 'id', true); ?>

<?php
}
get_footer();
?>