<?php 
   
 get_header();  
 
 ?>
<?php 
    global $wpdb;
    $projects = array();
    $current_user = wp_get_current_user();
    
    if(isset($current_user->projects))
    {
        foreach($current_user->projects as $pid => $pname)
        {
            $project_selected = ProjectModel::projectById($pid);  

            if(isset($project_selected[0])){
                $projects[$project_selected[0]->id] = $project_selected[0]->name;
            }
        }
    }
?>
<h1>Gestion des ventes</h1>
<div class="redband">
    <label>Project List</label>
    <select id="project-list-sale" name="project-list-sale" class="project-list-sale">
        <option value="0">Choose a project</option>
        <?php foreach($projects as $key => $project): ?>
            <option value="<?php echo $key ?>"><?php echo $project ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div id="sale-list-content">
    
</div>
<?php 

get_footer(); 

?>