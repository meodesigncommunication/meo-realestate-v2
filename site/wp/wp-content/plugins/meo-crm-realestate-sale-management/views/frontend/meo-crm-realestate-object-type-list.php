<?php
global $current_user;
$data = array();
$project_id = $_GET['project'];

if(!isset($current_user) || !$current_user){
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contacts list data with no connection");
    MeoCrmCoreTools::meo_crm_core_403();
    die();
}

$projects = meo_crm_projects_getProjectInfo($project_id);

get_header();

if($projects){

    $helperList = new MeoCrmCoreListHelper();

    $types = TypeObjectModel::getAllTypeObjects();

    $header = array(
        array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
        array('key' => 'type', 'name' => 'Type', 'type_data' => 'base', 'class' => ''),
    );

    $list_action = array(
        array('url_action' => site_url().'/meo-crm-realestate-object-type-form?project='.$project_id.'&id=@id', 'icon' => 'fa fa-edit', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id')
    );

    ?>
    <div class="wrap">
        <h2 id="meo-crm-realestate-list-name">Types d'objet</h2>
        <div id="meo-crm-realestate-add">
            <a id="add-status" href="meo-crm-realestate-object-type-form/?project=<?php echo $project_id ?>" name="Add status">Ajouter un type</a>
            <a id="back-btn" href="meo-crm-realestate-management-tool/?project=<?php echo $project_id ?>" name="Back to menu">Retour au menu</a>
        </div>
        <div class="clear-both"></div>
    </div>

    <?php echo $helperList->getList($types, $header, $list_action, false, 'id', true); ?>
    <?php
}else{
    MeoCrmCoreTools::meo_crm_core_403();
} ?>
    <script type="text/javascript">
        window.$ = jQuery;
        $(document).ready(function(){

        });
    </script>
<?php get_footer(); ?>