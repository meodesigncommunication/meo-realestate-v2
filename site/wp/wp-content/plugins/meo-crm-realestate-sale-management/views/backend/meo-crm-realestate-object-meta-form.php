<?php 

/*
 * TODO :: Si enregistrement (insert) est ok passer en mode update (ajouter ?id=x dans url)
 */

// Init variables
$datas = array();
$id = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id']  : 0 ;
$page_title = (isset($_GET['id']) && !empty($_GET['id'])) ? 'Modifier meta' : 'Ajouter meta';
$link_add_type = '';
$flash_message = '';

$helper = new MeoCrmCoreHelper();
$validation = new MeoCrmCoreValidationForm();

$types = TypeObjectModel::selectAllTypeObjects();

$link_admin = get_admin_url().'admin.php';
$link_list_type_object = $link_admin.'?page=manage_meta_object';

/*$type_selected = (isset($_GET['id']) && !empty($_GET['id'])) ? MetaObjectModel::selectMetaObjectById($id) : array() ;
$type_id = (isset($type_selected) && !empty($type_selected)) ? $type_selected[0]->type : '';*/

// Init classes
$helperList = new MeoCrmCoreListHelper();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // Init value
    $id = $_POST['id_meta'];
    
    // Declare a validation rules
    $rules = array(
        'object_type_id' => 'required|numeric',
        'meta_key' => 'required|min:5|max:100',
        'meta_slug' => 'required|slug',
        'meta_order' => 'required|numeric'
    );
    
    $results_validation = $validation->validationDatas($rules, $_POST);        
    $results = $validation->checkResultValidation($results_validation);
    $validate = $results['result'];
    
    if($validate){
        
        $datas['object_type_id'] = $_POST['object_type_id'];
        $datas['meta_key'] = $_POST['meta_key'];
        $datas['meta_slug'] = $_POST['meta_slug'];
        $datas['meta_order'] = $_POST['meta_order'];
        
        if(isset($id) && !empty($id)){ // Update type
            $results = MetaObjectModel::updateMeta($id, $datas);
            if($results['valid']){
                $message = 'Update success';
                $flash_message = $helper->getFlashMessageAdmin($message, 1);
            }else{
                $message = 'Update error';
                $flash_message = $helper->getFlashMessageAdmin($message, 0);
            }
        }else{ // Insert type
            $results = MetaObjectModel::insertMeta($datas);
            if($results['valid']){
                $message = 'Insert success';
                $flash_message = $helper->getFlashMessageAdmin($message, 1);
            }else{
                $message = 'Insert error';
                $flash_message = $helper->getFlashMessageAdmin($message, 0);
            }
        }
    }
    
    $type_id = $datas['object_type_id'];
    $meta_key = $datas['meta_key'];
    $meta_slug = $datas['meta_slug'];
    $meta_order = $datas['meta_order'];
    
}else{
    // Update data in get methode 
    // Select data to show in form    
    $meta = (isset($_GET['id']) && !empty($_GET['id'])) ? MetaObjectModel::selectMetaObjectById($_GET['id']) : array();
    $type_id = (isset($meta[0]->object_type_id) && !empty($meta[0]->object_type_id)) ? $meta[0]->object_type_id : 0;
    $meta_key = (isset($meta[0]->meta_key) && !empty($meta[0]->meta_key)) ? $meta[0]->meta_key : '';
    $meta_slug = (isset($meta[0]->meta_slug) && !empty($meta[0]->meta_slug)) ? $meta[0]->meta_slug : '';
    $meta_order = (isset($meta[0]->meta_order) && ($meta[0]->meta_order < 0 || $meta[0]->meta_order <> '') ) ? $meta[0]->meta_order : '';
}

?>
<div class="wrap meo-crm-object-list">
    <?php 
        if(!empty($flash_message))
        {
            echo $flash_message;    
        }      
    ?>
    <h1><?php echo $page_title; ?><a class="page-title-action" href="<?php echo $link_list_type_object; ?>">Retour</a></h1>
    <form id="from_object" name="from_meta_object" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_meta" id="id_meta" value="<?php echo $id ?>" />
        <table class="form-table meo-form-table">
            <tr class="form-field form-required">
                <th>
                    <label for="type_object_id">
                        Type d'objet
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <select name="object_type_id" id="object_type_id" >
                        <option value="0">Choose object type</option>
                        <?php foreach($types as $type): ?>
                            <?php $selected = ($type_id == $type['id']) ? 'selected=true' : ''; ?>
                            <option value="<?php echo $type['id'] ?>" <?php echo $selected ?>><?php echo $type['type'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="meta_key">
                        Meta key
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="meta_key" id="meta_key" placeholder="Meta key" value="<?php echo $meta_key ?>" />
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="meta_slug">
                        Meta slug
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="meta_slug" id="meta_slug" placeholder="Meta slug" value="<?php echo $meta_slug ?>" />
                </td>
            </tr>
            <tr class="form-field form-required">
                <th>
                    <label for="meta_order">
                        Ordre
                        <span class="description">(obligatoire)</span>
                    </label>
                </th>
                <td>
                    <input type="text" name="meta_order" id="meta_order" placeholder="Meta order" value="<?php echo $meta_order ?>" />
                </td>
            </tr>
        </table>
        <p class="submit">
            <input id="createmeta" class="button button-primary" type="submit" name="createmeta" value="<?php echo $page_title; ?>">
        </p>
    </form>
</div>