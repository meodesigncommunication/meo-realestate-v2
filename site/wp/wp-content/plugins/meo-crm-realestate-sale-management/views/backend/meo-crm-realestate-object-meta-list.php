<?php 

// Init variables
$link_add_type = '';
$edit_url = $link_add_type.'&id=@id';
$link_admin = get_admin_url().'admin.php';
$link_add_type = $link_admin.'?page=add_meta_object';
$header = MetaObjectModel::getHeaderTable();
$list_action = MetaObjectModel::getActionTable();
$types = MetaObjectModel::selectAllMetaObjects();

// Init classes
$helperList = new MeoCrmCoreListHelper();


?>
<div class="wrap meo-crm-type-object-list">
    <h1>
        Gestion des metas d'objet
        <a class="page-title-action" href="<?php echo $link_add_type; ?>">Ajouter</a>
    </h1>
    <div class="list-object-type">
        <?php echo $helperList->getList($types, $header, $list_action, true) ?>
    </div>
</div>