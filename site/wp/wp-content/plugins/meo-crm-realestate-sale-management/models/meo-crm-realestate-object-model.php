<?php

class ObjectModel
{
    public static $table = 'wp_meo_crm_realestate_sale_objects';
    public static $table_metas = 'wp_meo_crm_realestate_sale_object_metas';
    public static $table_metas_values = 'wp_meo_crm_realestate_sale_object_meta_values';    
    
    /*
     * Make header of type object list
     */
    public static function getHeaderTable()
    {
        return array(
            array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'title', 'name' => 'Titre', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'description', 'name' => 'Description', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'price', 'name' => 'Prix', 'type_data' => 'base', 'class' => 'w40px center-text')
        );
    }  
    
    /*
     * Make action cell of type object list
     */
    public static function getActionTable()
    {
        $link_admin = get_admin_url().'admin.php';
        $link_add_project = $link_admin.'?page=add_object';
        $edit_url = $link_add_project.'&id=@id';

        return array(
            array('url_action' => $edit_url, 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id') 
        );
    }
    
    public static function selectAllObjects()
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table.' AS t ';
        $query .= 'ORDER BY id ASC ';
        
        return $wpdb->get_results($query, ARRAY_A);
    }
    
    public static function selectObjectById($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table.' AS t ';
        $query .= 'WHERE id = '.$id.' ';
        
        return $wpdb->get_results($query);
    }
    
    public static function selectObjectByProjectId($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table.' AS t ';
        $query .= 'WHERE project_id = '.$id.' ';
        
        return $wpdb->get_results($query);
    }
    
    public static function selectMetaValueByObjectId($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table_metas_values.' AS t ';
        $query .= 'WHERE object_id = '.$id.' ';
        
        return $wpdb->get_results($query);
    }
    
    public static function insertObject($datas,$metas)
    {
        global $wpdb; 
        $check = true;
        $errors = array();
        
        // Init date DB
        $datas['created_at'] = date('Y-m-d H:m:i');
        $datas['updated_at'] = date('Y-m-d H:m:i');
        
        if($wpdb->insert(static::$table, $datas))
        { 
            $id = $wpdb->insert_id;
            
            foreach($metas as $meta){
                $meta['object_id'] = $id;
                $meta['created_at'] = date('Y-m-d H:m:i');
                $meta['updated_at'] = date('Y-m-d H:m:i');
                if(!$wpdb->insert(static::$table_metas_values, $meta))
                {
                    $check = false;
                    $errors[] = $meta;
                }
            }
            if($check)
            {
                return array('valid' => true, 'id' => $id);   
            }else{
                $wpdb->delete(static::$table,array('id' => $id) );
                return array('valid' => false, 'id' => 0, 'errors' => $errors);
            }
        }else{
            return array('valid' => false, 'id' => 0);
        }
    }
    
    public static function updateObject($id,$datas,$metas)
    {
        global $wpdb; 
        $check = true;
        $errors = array();
        
        $where = array( 'id' => $id );    
        $datas['updated_at'] = date('Y-m-d H:m:i');
        
        if($wpdb->update( static::$table, $datas, $where ))
        { 
            if($wpdb->delete(static::$table_metas_values,array('object_id' => $id) ))
            {
                foreach($metas as $meta){
                    $meta['object_id'] = $id;
                    $meta['created_at'] = date('Y-m-d H:m:i');
                    $meta['updated_at'] = date('Y-m-d H:m:i');
                    if(!$wpdb->insert(static::$table_metas_values, $meta))
                    {
                        $check = false;
                        $errors[] = $meta;
                    }
                }
            }else{
                $errors[] = 'Error delted meta value for object_id '.$id;   
            }
            
            if($check)
            {
                return array('valid' => true, 'id' => $id);   
            }else{
                return array('valid' => false, 'id' => 0, 'errors' => $errors);
            }
            
        }else{
            return array('valid' => false, 'id' => $id);
        }
    }
}