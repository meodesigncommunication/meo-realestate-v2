<?php

class TypeObjectModel
{
    public static $table = 'wp_meo_crm_realestate_sale_type_object';
    
    
    /*
     * Make header of type object list
     */
    public static function getHeaderTable()
    {
        return array(
            array('key' => 'id', 'name' => '#id', 'type_data' => 'base', 'class' => 'w40px center-text'),
            array('key' => 'type', 'name' => 'Type', 'type_data' => 'base', 'class' => 'w40px center-text')
        ); 
    }  
    
    /*
     * Make action cell of type object list
     */
    public static function getActionTable()
    {
        $link_admin = get_admin_url().'admin.php';
        $link_add_project = $link_admin.'?page=add_type_object';
        $edit_url = $link_add_project.'&id=@id';

        return array(
            array('url_action' => $edit_url, 'icon' => 'fa fa-pencil', 'class' => 'button action mr5px', 'search' => '@id', 'replace' => 'id') 
        );
    }
    
    public static function getAllTypeObjects()
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table.' AS t ';
        $query .= 'ORDER BY id ASC ';
        
        return $wpdb->get_results($query, ARRAY_A);
    }
    
    public static function getTypeObjectById($id)
    {
        global $wpdb;
        
        $query  = 'SELECT * ';
        $query .= 'FROM '.self::$table.' AS t ';
        $query .= 'WHERE id = '.$id.' ';
        
        return $wpdb->get_results($query);
    }
    
    public static function insertType($data)
    {
        global $wpdb;   
        $data['created_at'] = date('Y-m-d H:m:i');
        if($wpdb->insert(static::$table, $data))
        { 
            $id = $wpdb->insert_id;
            return array('valid' => true, 'id' => $id);            
        }else{
            return array('valid' => false, 'id' => 0);
        }
    }
    
    public static function updateType($where, $data)
    {
        global $wpdb;
        $datas['updated_at'] = date('Y-m-d H:m:i');
        if($wpdb->update( static::$table, $data, $where ))
        { 
            return array('valid' => true);
        }else{
            return array('valid' => false);
        }
    }
}