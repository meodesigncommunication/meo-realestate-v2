/* Vars */

	// Will contain a string of the returned json result from ajax on search contacts list, to avoid to do the query again
	var SearchContact = '';
	// Will contain an array of contacts id who have been deleted/deactivated
	var NotDisplayContacts = [];

/* Functions */

	// Check email availability
	function checkEmailAvailability(jQueryObj){
		
		// Ajax code
		var data = {
			'action': 'check_email_availability',
			'email': jQuery(jQueryObj).siblings('input#email').val(),
			'project_id': jQuery('select#project_id option:selected').val(),
		};
		jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
			var htmlContent = '';
			
			if(response == 'false' || !response){
				htmlContent = ' <i class="fa fa-exclamation-triangle cursor-hand" aria-hidden="true" title="Incorrect email format"></i>';
				jQuery(jQueryObj).siblings('input#email').addClass('warningField');
			}
			else if(response == 'true') {
				htmlContent = ' <i class="fa fa-check" aria-hidden="true" title="email ok"></i>';
				jQuery(jQueryObj).siblings('input#email').removeClass('warningField');
			}
			else {
				htmlContent = ' <i class="fa fa-exclamation-triangle cursor-hand" aria-hidden="true" title="existing email for the selected project"></i> <a href="http://smartcapture.ch/contact/?id='+response+'" target="_blank"><i class="fa fa-user" aria-hidden="true"></i></a>';
				jQuery(jQueryObj).siblings('input#email').addClass('warningField');
			}
			
			jQuery(jQueryObj).siblings('.emailCheckResult').html(htmlContent);
		});
	}
	
	// To be available for new DOM object (created on the fly by jQuery)
	function clickToEditContact(jQueryObj){
		
		var that = jQueryObj;
		var tdClass = jQuery(jQueryObj).attr('class');
		
		if(tdClass != 'id' && tdClass != 'id editable' && tdClass.indexOf("editable") > -1 
			&& tdClass != 'analytics_id' && tdClass != 'analytics_id editable' 
			&& tdClass != 'email' && tdClass != 'email editable' 
			&& tdClass != 'pdf' && tdClass != 'pdf editable') {
			
			jQuery(jQueryObj).removeClass('editable');
			
			var trRecordId = jQuery(jQueryObj).parents('tr').attr('id');
			var tdFieldName = jQuery(jQueryObj).attr('class');
			var tdInitialValue = jQuery(jQueryObj).html();
			var tdValue = '';
			
			if(tdClass == 'country editable'){
				// Ajax code
				var data = {
					'action': 'meo_crm_contacts_countrieslist',
					'country': tdInitialValue,
				};
				jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
					tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'">'+response+'<input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\')" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
					jQuery(that).html(tdValue);
				});
			}
			else if(tdClass == 'language editable'){
				// Ajax code
				var data = {
					'action': 'meo_crm_contacts_languageslist',
					'language': tdInitialValue,
				};
				jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
					tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'">'+response+'<input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\')" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
					jQuery(that).html(tdValue);
				});
			}
			else if(tdClass == 'courtier editable'){
				// Ajax code
				var data = {
					'action': 'meo_crm_relationship_getusersbyprojectid',
					'project_id': jQuery(jQueryObj).parents('tr').attr("data-project"),
					'courtier_id': jQuery(jQueryObj).parents('tr').attr("data-user")
				};
				jQuery.ajax({
					  type: 'POST',
					  url:  meo_crm_contacts_ajax.ajax_url,
					  data: data,
					  async:false
					}).done(function(html_userlist) {
						tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'">'+html_userlist+'<input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\' )" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
						jQuery(that).html(tdValue);
					});
			}
			else {
				tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'"><input type="text" name="fieldToSave" value="'+tdInitialValue+'" class="fieldToSave" /><input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\')" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
				jQuery(jQueryObj).html(tdValue);
			}				
		}
		
	}
	
	function moveToTop(jQueryTrObject){
		// Table
		var tableContacts = jQuery(jQueryTrObject).parents('table#contactsTable');
		// Remove background color to all TRs
		jQuery(tableContacts).find('tbody tr').css('background-color', '#ffffff');
		// Change background color to selected
		jQuery(jQueryTrObject).css('background-color', '#ffcfcf');
		// Save the element
		var trObject = jQuery(jQueryTrObject);
		// Remove the line
		jQuery(jQueryTrObject).remove();
		// Add it to the main Table
		jQuery('#contactsTable').prepend(trObject);
		// Scroll to top
		// window.scrollTo(0, 0);
		jQuery('html, body').animate({ 
            scrollTop: (jQuery('#map').offset().top) - 80
        }, 1000);
		
	}
	
	// Update counter and pagination on list
	
	function updateContactsListCounterAndPagination(contact_id, action){
		
		// Test if we are on the Contacts List page
		if(jQuery('.page-template-meo-crm-contacts-list-view').length){
			
			// Update Project Contacts Counter
			var project_id = jQuery('tr#'+contact_id).attr('data-project');
			var regExp = /\(([^)]+)\)/;
			var counterText = jQuery('#meo-crm-contacts-projectSelector option.'+project_id).text();
			var matches = regExp.exec(counterText);
			var counter = parseInt(matches[1]);
			
			if(action == 'add'){
				counter++;
			}
			else counter--;
			
			jQuery('#meo-crm-contacts-projectSelector .'+project_id).text(counterText.replace(/\(([^)]+)\)/g, '('+counter+')'));
			
			// Update Pagination Counter
			jQuery('.paginator span').html(counter);
		}
		// Search page
		else if(jQuery('.page-template-meo-crm-contacts-search-view')){
			
			var counter = parseInt(jQuery('#search-results-counter').html());
			console.log(action);
			if(action == 'add'){
				counter++;
				var index = NotDisplayContacts.indexOf(contact_id);
				if (index > -1) {
					NotDisplayContacts.splice(index, 1);
				}
			}
			else {
				counter--;
				NotDisplayContacts.push(contact_id);
			}
			submitSearchContacts('true');
			
			jQuery('#search-results-counter').html(counter);
		}
	}
	
	
	// Mark as deleted
	
	function updateDeleteContact(contact_id, redirectUrl){
		
		jQuery('<div></div>').appendTo('body')
		    .html('<div><h6 id="dialog-content">Please confirm DELETE CONTACT ['+contact_id+']</h6></div>')
			    .dialog({
			        modal: true,
			        title: 'Delete Contact ['+contact_id+']',
			        zIndex: 10000,
			        autoOpen: true,
			        width: '300px',
			        resizable: false,
			        buttons: {
			            Yes: function () {
			            	
			            	var that = this;

			            	// https://www.sitepoint.com/jquery-todays-date-ddmmyyyy/
			            	// http://www.java2s.com/Code/JavaScript/Development/Getthecurrenttimeandthenextractthehoursminutesandseconds.htm
			        		var fullDate = new Date();
			        		var twoDigitMonth 	= (fullDate.getMonth()+1)+"";	if(twoDigitMonth.length==1)		twoDigitMonth="0" +twoDigitMonth;
			        		var twoDigitDate 	= fullDate.getDate()+"";		if(twoDigitDate.length==1)		twoDigitDate="0" +twoDigitDate;
			        		var twoDigitHours   = fullDate.getHours()+"";		if(twoDigitHours.length==1)		twoDigitHours="0" +twoDigitHours;
			        		var twoDigitMinutes = fullDate.getMinutes()+"";		if(twoDigitMinutes.length==1)	twoDigitMinutes="0" +twoDigitMinutes;
			        		var twoDigitSeconds = fullDate.getSeconds()+"";		if(twoDigitSeconds.length==1)	twoDigitSeconds="0" +twoDigitSeconds;
			        		
			        		var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + twoDigitDate + " " + twoDigitHours + ":" + twoDigitMinutes + ":" + twoDigitSeconds;

			        		var precision = jQuery( "#contactsTable").attr("data-precision");
			        		
							// Ajax code
							var data = {
								'action': 'update_contact_field',
								'fieldName': 'date_deleted',
								'fieldValue': currentDate,
								'precision': precision,
								'contactID': contact_id,
							};
							jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
								
								if(response == 'false' || !response){
									
									jQuery(that).find('h6#dialog-content').html( "<span class=\"warning-text\">Error Delete Contact ["+contact_id+"]</span>" );
								}
								else {
									if(redirectUrl != ''){
										
										if(redirectUrl == 'element'){
											
											updateContactsListCounterAndPagination(contact_id, 'substract');
											
											var role = jQuery('#primary').attr('data-role');
											if(role != 'administrator'){
												// Remove Contact element
												jQuery('tr#'+contact_id).remove();
											}
											else {
												jQuery('tr#'+contact_id).addClass('warning-text');
												var upActConBtn = '<em class=""><i class="fa fa-refresh cursor-hand" aria-hidden="true" onclick="updateActivateContact('+contact_id+', \'element\')"></i></em>';
												jQuery('tr#'+contact_id+' td.delete-activate-contact').html(upActConBtn);
											}
											jQuery(that).remove();
										}
										else {
											// similar behavior as an HTTP redirect
											window.location.replace(redirectUrl);
										}
									}
								}
								
							});
			                
			            },
			            No: function () {
			                jQuery(this).dialog("close");
			            }
			        },
			        close: function (event, ui) {
			            jQuery(this).remove();
			        }
			    });
						
	}
	
	
	// Activate Contact
	
	function updateActivateContact(contact_id, redirectUrl){
		
		jQuery('<div></div>').appendTo('body')
		    .html('<div><h6 id="dialog-content">Please confirm Re Activate CONTACT ['+contact_id+']</h6></div>')
			    .dialog({
			        modal: true,
			        title: 'Re Activate Contact ['+contact_id+']',
			        zIndex: 10000,
			        autoOpen: true,
			        width: '300px',
			        resizable: false,
			        buttons: {
			            Yes: function () {
			            	
			            	var that = this;
			            	var precision = jQuery( "#contactsTable").attr("data-precision");
			        		
							// Ajax code
							var data = {
								'action': 'update_contact_field',
								'fieldName': 'date_deleted',
								'fieldValue': 'NULL',
								'precision': precision,
								'contactID': contact_id,
							};
							jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
								
								if(response == 'false' || !response){
									
									jQuery(that).find('h6#dialog-content').html( "<span class=\"warning-text\">Error Re Activate Contact ["+contact_id+"]</span>" );
								}
								else {
									if(redirectUrl == 'element'){
										jQuery('tr#'+contact_id).removeClass('warning-text');
										var upDelConBtn = '<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact('+contact_id+', \'element\')"></i></em>';
										jQuery('tr#'+contact_id+' td.delete-activate-contact').html(upDelConBtn);
										
										updateContactsListCounterAndPagination(contact_id, 'add');
										
										jQuery(that).remove();
									}
									else {
										// Reload from the server side
										location.reload(true);
									}
								}
								
							});
			                
			            },
			            No: function () {
			                jQuery(this).dialog("close");
			            }
			        },
			        close: function (event, ui) {
			            jQuery(this).remove();
			        }
			    });
						
	}
	
	
	// Delete Contact
	function deleteContact(contact_id, redirectUrl){
		
		jQuery('<div></div>').appendTo('body')
		    .html('<div><h6 id="dialog-content">Please confirm DELETE CONTACT ['+contact_id+']</h6></div>')
			    .dialog({
			        modal: true,
			        title: 'Delete Contact ['+contact_id+']',
			        zIndex: 10000,
			        autoOpen: true,
			        width: '300px',
			        resizable: false,
			        buttons: {
			            Yes: function () {
			            	
			            	var that = this;
			                
							// Ajax code
							var data = {
								'action': 'delete_contact',
								'id': contact_id,
							};
							jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
								
								if(response == 'false' || !response){
									
									jQuery(that).find('h6#dialog-content').html( "<span class=\"warning-text\">Error Delete Contact ["+contact_id+"]</span>" );
								}
								else {
									if(redirectUrl != ''){
										
										if(redirectUrl == 'element'){
											
											updateContactsListCounterAndPagination(contact_id, 'substract');
											
											// Remove Contact element
											jQuery('tr#'+contact_id).remove();
											jQuery(that).remove();
										}
										else {
											// similar behavior as an HTTP redirect
											window.location.replace(redirectUrl);
										}
									}
								}
								
							});
			                
			            },
			            No: function () {
			                jQuery(this).dialog("close");
			            }
			        },
			        close: function (event, ui) {
			            jQuery(this).remove();
			        }
			    });
						
	}
	
	// For now, as reloading the contacts from JS var doesn't include the changes that have been made on the list (changing addresses etc.)
	// I will keep the code and behavior, but will change the params on the map search from 'true' to 'false', in order to always reload the map and markers
	
	function submitSearchContacts(mapSearch){
		
		// Manage display (mapSearch true means we clicked on the glode icon to reload the markers on the map)
		if(mapSearch == 'reloadTable'){ // mapSearch == 'false' || 
			jQuery('#search-results-counter').html('');
			jQuery('#redband-export').hide();
			jQuery("#google-map").hide();
		    jQuery("#contactsTable").hide();
		    jQuery("#search-loading").show();
		}
		jQuery('#map-loading').show();
	    
	    // Manage posted values
		var projects = '';
		var fields = '';
		var keyword = jQuery('input#contacts-search-term').val();
		
	    jQuery('.search-projects:checked').each(function() {
	        projects+= jQuery(this).val()+'-';
	    });
	    
	    jQuery('.search-fields:checked').each(function() {
	        fields+= jQuery(this).val()+'-';
	    });
        
        // Init var
        var contactsToParse = [];
        var contacts = [];
        var count = 0;
        var countDeleted = 0;
		
		if( SearchContact != '' && mapSearch == 'true' ){
			
			contactsToParse = JSON.parse(SearchContact);
		}
		else {
			jQuery.ajax({
				url:meo_crm_contacts_ajax.ajax_url, 
				data: {
					'action': 'meo_crm_contacts_getContactsSearch',
					'keyword': keyword,
					'projects': projects,
					'fields': fields,
				}, 
				method: 'POST',
				async: false, 
				success: function(response) {
					var json_response = jQuery.parseJSON( response );
					
					if(mapSearch == 'reloadTable') {
						jQuery("#contactsTable tbody.contactsSearchContainer").html(json_response.html);
			    		jQuery("#contactsTable.tablesorter").trigger("update");
		    		}
		    		SearchContact = JSON.stringify(json_response.contacts);
		    		
		    		contactsToParse = json_response.contacts;
				}
			});
		}

		jQuery("#google-map").show();
		
		// Prepare Map
		map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 8,
	        center: {lat: 46.6136509, lng: 7.0592593}
	    });
        
        map.addListener('mouseout', function() {
        	jQuery('#current_user_map').html('');
        });
        
        //Parsing JSON
        jQuery.each(contactsToParse, function(idx, obj) {
        	
        	var addData = true;
        	for(var i = NotDisplayContacts.length - 1; i >= 0; i--) {
        		if(NotDisplayContacts[i] == obj.id){
                	addData = false;
                }
            }
        	
        	if(addData){
        		myData = [];
	            myData['id'] = obj.id;
	            myData['title'] = "["+obj.id+"] "+obj.last_name+" "+obj.first_name;
	            myData['latitude'] = obj.latitude;
	            myData['longitude'] = obj.longitude;
	            myData['zindex'] = count;                  
	            contacts.push(myData);
	            if(obj.date_deleted == null){
	            	count++;
	            }
	            else countDeleted++;
        	}
	            
        });
        
        for (var i = 0; i < contacts.length; i++) {
            var contact = contacts[i];
            var myLatLng = {lat: parseFloat(contact['latitude']), lng: parseFloat(contact['longitude'])};
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: contact['title'],
                animation: google.maps.Animation.DROP
            });

            marker.set("id", contact['id']);
            marker.addListener('click', function() {
                var contact_id = this.get('id');
                if(jQuery('tr#'+contact_id).length){
	            	moveToTop(jQuery('tr#'+contact_id));
	            }
              });

            marker.addListener('mouseover', function() {
            	var contact_title = this.get('title');
            	jQuery('#current_user_map').html(contact_title);
              });
        }
		
		// Manage display
        if(mapSearch == 'reloadTable'){ // false
        	
        	if(countDeleted > 0){
        		var tempString = count + '<span><br/>+ ' + countDeleted + ' inactive</span>';
        		jQuery('#search-results-counter').show().html(tempString).delay(2000).fadeOut(1000, function(){jQuery('#search-results-counter').html(count);}).delay(1000).fadeIn(400);
        	}
        	else jQuery('#search-results-counter').html(count);
        	
        	jQuery("#search-loading").hide();
    	    jQuery("#contactsTable").show();
			jQuery('#redband-export').show();
		}
		jQuery('#map-loading').hide();
	}
	
	/* Document ready functions */

	jQuery(document).ready(function() {
		
		// Init table sorter library
		jQuery("#contactsTable.tablesorter").tablesorter();
	
		
		// To edit field
		jQuery("#contactsTable tr td.editable").dblclick(
			function(e){
				clickToEditContact(this);
			}
		);
		
		// Checking email / courtier's project, on project change
		
		jQuery("#new-contact-form select#project_id").change(
				function(e){
				var project_id = jQuery('option:selected', this).val();
				
				// Check email address
				var emailChecker = jQuery("i.email-checker");
				checkEmailAvailability(emailChecker);
				// Check courtier's project
				if(jQuery("#new-contact-form select#courtier").length){
					// Note the use of .prop instead of .attr
					jQuery("#new-contact-form select#courtier option").prop("selected", false);
					jQuery("#new-contact-form select#courtier optgroup."+project_id+" option:first-child").prop("selected", true);
				}
			});
		
		// Checking project, on courtier's project change
		
		jQuery("#new-contact-form select#courtier").change(
				function(e){
				
				var selected = jQuery('option:selected', this);
				var project_id = selected.parent('optgroup').attr('class');
				
				// Check project
				jQuery("#new-contact-form select#project_id option").prop("selected", false);
				jQuery("#new-contact-form select#project_id option."+project_id+"").prop("selected", true);
				
				// Check email address
				var emailChecker = jQuery("i.email-checker");
				checkEmailAvailability(emailChecker);
			});
		
		
		// Action on the project dropdown list
		
		jQuery("#meo-crm-contacts-projectSelector").change(
			function(e){
			
			var colspan = jQuery( "#contactsTable thead tr th").length;
			jQuery("#contactsTable tbody.contactsListContainer").html('<tr id="loading-row"><td colspan="'+colspan+'" style="text-align:center;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></td></tr>');
			jQuery('#csv-export').hide();
			
			var start = parseInt(jQuery('#meo-crm-contacts-projectSelector').attr("data-initstart"));
			var limit = parseInt(jQuery('#meo-crm-contacts-projectSelector').attr("data-initlimit"));
			NotDisplayContacts = [];
			// Re init in case we had a view All previously clicked
			jQuery("#load-more").attr("data-limit", limit);
			
		    jQuery( "#meo-crm-contacts-projectSelector option:selected" ).each(function() {
			    if(jQuery( this ).val() > 0) {
			    	
			    	// Hide load more button till we pull contacts
			    	jQuery('#pager').hide();
			    	
			    	var data = {
			    			'action': 'meo_crm_contacts_contactsListByProjectID',
			    			'project_id': jQuery( this ).val(),
			    			'precision': 'FRONT_LIST',
			    			'start': start,
			    			'limit': limit
			    		};
			    	jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
			    		jQuery("#contactsTable tbody.contactsListContainer").html(response);
			    		meo_crm_contacts_initMap(start, limit);
			    		jQuery("#contactsTable.tablesorter").trigger("update");
			    		jQuery('#csv-export').show();
			    		jQuery("#load-more").attr("data-start", parseInt(start + limit));
			    		
			    		var counterText = jQuery("#meo-crm-contacts-projectSelector option:selected").text();
			    		var regExp = /\(([^)]+)\)/;
			    		var matches = regExp.exec(counterText);
			    		var nbContacts = parseInt(matches[1]);
			    		jQuery('.paginator').html(limit+' of <span>'+nbContacts+'</span>');
			    		
			    		if(nbContacts > limit){
			    			jQuery('#pager').show();
			    		}
					});
					
			    }
			    else {
			    	jQuery('#pager').hide();
			    	jQuery("#contactsTable tbody.contactsListContainer").html('<tr id="loading-row"><td colspan="'+colspan+'" style="text-align:center;" class="warning-text"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Please select a Project</td></tr>');
			    }
		    });
		});
		
		
		// On export page
		
		jQuery("#csv-export").click(
			function(e){
				e.preventDefault();
				var project_id = jQuery( "select#meo-crm-contacts-projectSelector option:selected" ).val();
				var precision = jQuery(this).attr('data-precision');
				
				if(project_id && project_id > 0){
					location.href = meo_crm_contacts_ajax.meo_crm_contacts_export_url + '?project_id=' + project_id + '&precision=' + precision;
				}
		});
		
		jQuery("#csv-export-search").click(
				function(e){
					e.preventDefault();
					
					// Manage posted values
					var projects = '';
					var fields = '';
					var keyword = jQuery('input#contacts-search-term').val();
					
				    jQuery('.search-projects:checked').each(function() {
				        projects+= jQuery(this).val()+'-';
				    });
				    
				    jQuery('.search-fields:checked').each(function() {
				        fields+= jQuery(this).val()+'-';
				    });
				    
					location.href = meo_crm_contacts_ajax.meo_crm_contacts_export_url + '?keyword=' + keyword + '&projects=' + projects + '&fields=' + fields;
			});
		
		// Visualize Interactions on Contacts List
		
		jQuery('body').tooltip({
			items:"i.interactions-details",
			content: function () {
				var text = jQuery(this).attr('title');
	              return text;
	          }
		});
		
	});

/* Action functions */
	
	function contactsListLoadMore(Obj){
		
		// Hide pager while loading
		jQuery('#pager').hide();
		
		var colspan = jQuery( "#contactsTable thead tr th").length;
		jQuery("#contactsTable tbody.contactsListContainer").append('<tr id="loading-row"><td colspan="'+colspan+'" style="text-align:center;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></td></tr>');
		
		// Collect fix data
		var project_id = jQuery("#meo-crm-contacts-projectSelector option:selected").val();
		var counterText = jQuery("#meo-crm-contacts-projectSelector option:selected").text();
		var regExp = /\(([^)]+)\)/;
		var matches = regExp.exec(counterText);
		var nbContacts = parseInt(matches[1]);
		
		// Init values
		var start = parseInt(jQuery(Obj).attr("data-start"));
		var limit = parseInt(jQuery(Obj).attr("data-limit"));
		var newlimit = parseInt(start+limit);
		
		if(start <= nbContacts){
			
			// Query
			var data = {
	    			'action': 'meo_crm_contacts_contactsListByProjectID',
	    			'project_id': project_id,
	    			'precision': 'FRONT_LIST',
	    			'start': start,
	    			'limit': limit
	    		};
	    	jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
	    		
	    		jQuery('#loading-row').remove();
	    		jQuery("#contactsTable tbody.contactsListContainer").append(response);
	    		
	    	    meo_crm_contacts_setMarkers(map, start, limit);
	    	    
	    		jQuery("#contactsTable.tablesorter").trigger("update");
	    		jQuery(Obj).attr("data-start", newlimit);
	    		jQuery('.paginator').html(newlimit+' of <span>'+nbContacts+'</span>');
	    		if(newlimit < nbContacts){
	    			jQuery('#pager').show();
				}
			});
		}
	}
	
	function contactsListAll(Obj){
		
		// Hide pager
		jQuery('#pager').hide();
		
		var colspan = jQuery( "#contactsTable thead tr th").length;
		jQuery("#contactsTable tbody.contactsListContainer").html('<tr id="loading-row"><td colspan="'+colspan+'" style="text-align:center;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></td></tr>');
		
		// Collect fix data
		var project_id = jQuery("#meo-crm-contacts-projectSelector option:selected").val();
		var counterText = jQuery("#meo-crm-contacts-projectSelector option:selected").text();
		var regExp = /\(([^)]+)\)/;
		var matches = regExp.exec(counterText);
		var nbContacts = parseInt(matches[1]);
		
		// Update for View all contacts on the map function
		jQuery('#load-more').attr("data-start", 0);
		jQuery('#load-more').attr("data-limit", nbContacts);
		
		// Query
		var data = {
    			'action': 'meo_crm_contacts_contactsListByProjectID',
    			'project_id': project_id,
    			'precision': 'FRONT_LIST'
    		};
    	jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
    		
    		jQuery('#loading-row').remove();
    		jQuery("#contactsTable tbody.contactsListContainer").html(response);
    		
    		map = new google.maps.Map(document.getElementById('map'), {
    	        zoom: 8,
    	        center: {lat: 46.6136509, lng: 7.0592593}
    	    });
    		meo_crm_contacts_setMarkers(map, 0, nbContacts);
    	    
    		jQuery("#contactsTable.tablesorter").trigger("update");
		});
	}

	// Cancel the form and revert to initial content
	function cancelField(container, initialValue){
		jQuery(container).parents('td').addClass('editable');
		jQuery(container).parents('td').attr('data-locked', '');
		jQuery(container).parents('td').html(initialValue);
	}
	
	// Update the contact edited field
	function updateField(field, id){
		var formId = field+"-"+id;
		var contentToSave = '';
		var contentToPrint = '';
		var precision = jQuery( "#contactsTable").attr("data-precision");
		
		// Contact fields behavior
		if(field == 'country'){
			contentToSave = jQuery("form."+formId+" .fieldToSave").find('option:selected').text();
		}
		else if(field == 'language'){
			contentToSave = jQuery("form."+formId+" .fieldToSave").find('option:selected').val();
		}
		else contentToSave = jQuery("form."+formId+" .fieldToSave").val();
		
		contentToPrint = contentToSave;
		
		var data = {
			'action': 'update_contact_field',
			'contactID': id,
			'fieldName': field,
			'fieldValue': contentToSave,
			'precision': precision
		};
		
		// User field behavior (Exception)
		if(field == 'courtier'){
			contentToSave = jQuery("form."+formId+" .fieldToSave").find('option:selected').val();
			contentToPrint = jQuery("form."+formId+" .fieldToSave").find('option:selected').text();
			
			var user_id_old = '';
			if(jQuery("form."+formId+" input.user_id_old").length){
				user_id_old = jQuery("form."+formId+" .user_id_old").val();
			}
			
			// Redefine data
			var data = {
				'action': 'meo_crm_relationship_reassign',
				'contact_id': id,
				'user_id': contentToSave,
				'user_name': contentToPrint,
				'user_id_old': user_id_old
			};
			
			jQuery("form."+formId+" .fieldToSave").parents('tr').attr("data-user", contentToSave);
		}
		
		jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(record_id) {
			if(field == 'courtier' && jQuery("#edit-contact").length){
				meoInsertReassignState(id, contentToSave, record_id, contentToPrint);
			}
		});
		// Add class
		jQuery("tr#"+id+" td."+field).html(contentToPrint);
		jQuery("tr#"+id+" td."+field).addClass('editable');
	}

	// EDIT CONTACT PAGE: Update latitude and longitude for the contact and reset the map
	function updateContactCoordinates(that){
		// Get Contact fields: address, city, postcode, country
		if(!jQuery('.address.editable').length || !jQuery('.city.editable').length || !jQuery('.postcode.editable').length || !jQuery('.country.editable').length){
			alert('Please save address changes first');
			return false;
		}
		var address 	= jQuery('.address.editable').html();
		var city 		= jQuery('.city.editable').html();
		var postcode 	= jQuery('.postcode.editable').html();
		var country 	= jQuery('.country.editable').html();
		var contact_id	= jQuery("div.wrap h2").attr("id");
		
		return updateContactCoordinatesAjax(contact_id, address, city, postcode, country);
	}
	
	// CONTACTS LIST PAGE: Update latitude and longitude for the contact and reset the map
	function updateContactsCoordinates(that){
		// Get Contact fields: address, city, postcode, country
		if(!jQuery(that).parents('tr').find('.address.editable').length 
				|| !jQuery(that).parents('tr').find('.city.editable').length 
				|| !jQuery(that).parents('tr').find('.postcode.editable').length 
				|| !jQuery(that).parents('tr').find('.country.editable').length){
			alert('Please save address changes first');
			return false;
		}
		var address 	= jQuery(that).parents('tr').find('.address.editable').html();
		var city 		= jQuery(that).parents('tr').find('.city.editable').html();
		var postcode 	= jQuery(that).parents('tr').find('.postcode.editable').html();
		var country 	= jQuery(that).parents('tr').find('.country.editable').html();
		var contact_id	= jQuery(that).parents('tr').attr("id");
		
		return updateContactCoordinatesAjax(contact_id, address, city, postcode, country);
	}
	
	function updateContactCoordinatesAjax(contact_id, address, city, postcode, country){
		// Update Contact fields: latitude, longitude
		jQuery.ajax({
	        url: meo_crm_contacts_ajax.ajax_url,
	        data: {
	            'action':'meo_crm_contact_setcoordinates',
	            'contact_id' : contact_id,
	            'address' : address,
		    	'city' : city,
		    	'postcode' : postcode,
		    	'country' : country
	        },
	        method: 'POST',
	        success:function(datas) {
	            
	        	if(datas != ''){
	        		//Parsing JSON
		            var obj = jQuery.parseJSON(datas);
		            
		            if(obj.latitude && obj.longitude){
		            	var Lat = parseFloat(obj.latitude);
			            var Lon = parseFloat(obj.longitude);
			            
			            meo_crm_contacts_setMap(Lat, Lon, ''+contact_id+'');
			            
			            if(jQuery('tr#'+contact_id).length){
			            	jQuery('tr#'+contact_id).find('.see_map').html('<i class="fa fa-globe cursor-hand" aria-hidden="true" title="See on the map" onclick="meo_crm_contacts_setMap('+Lat+', '+Lon+', \''+contact_id+'\');moveToTop(jQuery(this).parents(\'tr\'));"></i>');
			            }
		            }
	        	}
		            
	        },
	        error: function(errorThrown){
	            console.log(errorThrown);
	        }
	    });
		return false;
	}

////////////////
// GOOGLE MAP //
////////////////

	/* Common */
	
	function meo_crm_contacts_setMap(latitude, longitude, contact_title){
		var map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 11,
	        center: {lat: latitude, lng: longitude}
	    });
		var marker = new google.maps.Marker({
	        position: {lat: latitude, lng: longitude},
	        map: map,
	        title: contact_title
	    });
	}
	
	/* Single View */
	
	function meo_crm_contact_initMap() {
	    var map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 11,
	        center: {lat: 46.6136509, lng: 7.0592593}
	    });   
	
	    meo_crm_contact_setMarkers(map);
	}
	
	function meo_crm_contact_setMarkers(map)
	{    
	    jQuery.ajax({
	        url: meo_crm_contacts_ajax.ajax_url,
	        data: {
	            'action':'get_contact_coordinates',
	            'contact_id' : jQuery("tr.tr_contact").attr("id")
	        },
	        method: 'POST',
	        success:function(datas) { 
	            var contacts = [];
	            var count = 0;   
	            
	            //Parsing JSON
	            jQuery.each(jQuery.parseJSON(datas), function(idx, obj) {
	                myData = [];
	                myData['title'] = obj.last_name+" "+obj.first_name;
	                myData['latitude'] = obj.latitude;
	                myData['longitude'] = obj.longitude;
	                myData['zindex'] = count;                    
	                contacts.push(myData); 
	                count++;
	            }); 
	            for (var i = 0; i < contacts.length; i++) {
	                var contact = contacts[i];
	                var myLatLng = {lat: parseFloat(contact['latitude']), lng: parseFloat(contact['longitude'])};
	                var marker = new google.maps.Marker({
	                    position: myLatLng,
	                    map: map,
	                    title: contact['title'],
	                    animation: google.maps.Animation.DROP
	                });
	            }
	        },
	        error: function(errorThrown){
	            console.log(errorThrown);
	        }
	    });
	}
	
	/* List View */
	
	function meo_crm_contacts_initMap() {
	    map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 8,
	        center: {lat: 46.6136509, lng: 7.0592593}
	    });
	    
	    var start = parseInt(jQuery('#meo-crm-contacts-projectSelector').attr("data-initstart"));
		var limit = parseInt(jQuery('#meo-crm-contacts-projectSelector').attr("data-initlimit"));
	
	    meo_crm_contacts_setMarkers(map, start, limit);
	}
	
	function meo_crm_contacts_initMapAllDisplayed() {
	    map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 8,
	        center: {lat: 46.6136509, lng: 7.0592593}
	    });
	    
	    var start = 0;
		var limit = parseInt(jQuery('#load-more').attr("data-start"));
		if(limit == 0){
			limit = parseInt(jQuery('#load-more').attr("data-limit"));
		}
	    meo_crm_contacts_setMarkers(map, start, limit);
	}
	
	function meo_crm_contacts_setMarkers(map, start, limit)
	{
		
		jQuery('#map-loading').show();
		
	    var project_id = 0;
	    jQuery( "#meo-crm-contacts-projectSelector option:selected" ).each(function() {
	    	project_id = jQuery( this ).val();
	    });
	    
	    jQuery.ajax({
	        url: meo_crm_contacts_ajax.ajax_url,
	        data: {
	            'action':'get_contacts_coordinates',
	            'project_id' : project_id,
	            'start' : start,
	            'limit' : limit
	        },
	        method: 'POST',
	        success:function(data) { 
	            var contacts = [];
	            var count = 0;
	            
	            map.addListener('mouseout', function() {
	            	jQuery('#current_user_map').html('');
	              });

	            
	            //Parsing JSON
	            jQuery.each(jQuery.parseJSON(data), function(idx, obj) {
	                myData = [];
	                myData['id'] = obj.id;
	                myData['title'] = "["+obj.id+"] "+obj.last_name+" "+obj.first_name;
	                myData['latitude'] = obj.latitude;
	                myData['longitude'] = obj.longitude;
	                myData['zindex'] = count;                    
	                contacts.push(myData); 
	                count++;
	            }); 
	            for (var i = 0; i < contacts.length; i++) {
	                var contact = contacts[i];
	                var myLatLng = {lat: parseFloat(contact['latitude']), lng: parseFloat(contact['longitude'])};
	                var marker = new google.maps.Marker({
	                    position: myLatLng,
	                    map: map,
	                    title: contact['title'],
	                    animation: google.maps.Animation.DROP
	                });
	                marker.set("id", contact['id']);
	                marker.addListener('click', function() {
	                    var contact_id = this.get('id');
	                    if(jQuery('tr#'+contact_id).length){
	    	            	moveToTop(jQuery('tr#'+contact_id));
	    	            }
	                  });

	                marker.addListener('mouseover', function() {
	                	var contact_title = this.get('title');
	                	jQuery('#current_user_map').html(contact_title);
	                  });
	            }
	            jQuery('#map-loading').hide();
	        },
	        error: function(errorThrown){
	            console.log(errorThrown);
	        }
	    });
	}