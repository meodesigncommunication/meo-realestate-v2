/* Functions */

	// To be available for new DOM object (created on the fly by jQuery)
	function clickToEditContact(jQueryObj){
		
		var that = jQueryObj;
		var tdClass = jQuery(jQueryObj).attr('class');
		
		if(tdClass != 'id' && tdClass != 'id editable' && tdClass.indexOf("editable") > -1 
			&& tdClass != 'analytics_id' && tdClass != 'analytics_id editable' 
			&& tdClass != 'email' && tdClass != 'email editable' 
			&& tdClass != 'pdf' && tdClass != 'pdf editable') {
			
			jQuery(jQueryObj).removeClass('editable');
			
			var trRecordId = jQuery(jQueryObj).parents('tr').attr('id');
			var tdFieldName = jQuery(jQueryObj).attr('class');
			var tdInitialValue = jQuery(jQueryObj).html();
			var tdValue = '';
			
			if(tdClass == 'country editable'){
				// Ajax code
				var data = {
					'action': 'meo_crm_contacts_countrieslist',
					'country': tdInitialValue,
				};
				jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
					tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'">'+response+'<input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\')" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
					jQuery(that).html(tdValue);
				});
			}
			else if(tdClass == 'courtier editable'){
				// Ajax code
				var data = {
					'action': 'meo_crm_relationship_getusersbyprojectid',
					'project_id': jQuery(jQueryObj).parents('tr').attr("data-project"),
					'courtier_id': jQuery(jQueryObj).parents('tr').attr("data-user")
				};
				jQuery.ajax({
					  type: 'POST',
					  url:  meo_crm_contacts_ajax.ajax_url,
					  data: data,
					  async:false
					}).done(function(html_userlist) {
						tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'">'+html_userlist+'<input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\', )" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
						jQuery(that).html(tdValue);
					});
			}
			else {
				tdValue = '<form class="'+tdFieldName+'-'+trRecordId+'"><input type="text" name="fieldToSave" value="'+tdInitialValue+'" class="fieldToSave" /><input type="hidden" name="idToSave" value="'+trRecordId+'" class="idToSave" /><i data-id="'+tdFieldName+'-'+trRecordId+'" class="cursor-hand fa fa-check-square save-state fieldUpdater" onclick="updateField(\''+tdFieldName+'\', \''+trRecordId+'\')" aria-hidden="true"></i><i class="cursor-hand fa fa-times-circle cancel-state fieldCanceler" onclick="cancelField(this, \''+tdInitialValue+'\')" aria-hidden="true"></i></form>';
				jQuery(jQueryObj).html(tdValue);
			}
			// jQuery('#start-date').datepicker({dateFormat : 'yy-mm-dd'});
		}
		
	}
	
	function moveToTop(jQueryTrObject){
		// Table
		var tableContacts = jQuery(jQueryTrObject).parents('table#contactsTable');
		// Remove background color to all TRs
		jQuery(tableContacts).find('tbody tr').css('background-color', '#ffffff');
		// Change background color to selected
		jQuery(jQueryTrObject).css('background-color', '#ffcfcf');
		// Save the element
		var trObject = jQuery(jQueryTrObject);
		// Remove the line
		jQuery(jQueryTrObject).remove();
		// Add it to the main Table
		jQuery('#contactsTable').prepend(trObject);
		// Scroll to top
		window.scrollTo(0, 0);
	}
	
	/* Document ready functions */

	jQuery(document).ready(function() {
		// Init table sorter library
		jQuery("#contactsTable.tablesorter").tablesorter();
		
		// To edit field
		jQuery("#contactsTable tr td.editable").dblclick(
			function(e){
				clickToEditContact(this);
			}
		);
		
		// Action on the project dropdown list
		
		jQuery("#meo-crm-contacts-projectSelector").change(
				function(e){
				
				var colspan = jQuery( "#contactsTable").attr("data-colspan");
				jQuery( "#contactsTable tbody.contactsListContainer").html('<tr><td colspan="'+colspan+'" style="text-align:center;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></td></tr>');
				jQuery('#csv-export').hide();
				
			    jQuery( "#meo-crm-contacts-projectSelector option:selected" ).each(function() {
				    if(jQuery( this ).val() > 0) {
				    	
				    	var data = {
				    			'action': 'meo_crm_contacts_contactsListByProjectID',
				    			'project_id': jQuery( this ).val(),
				    			'precision': 'ADMIN_LIST'
				    		};
				    	jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
				    		jQuery( "#contactsTable tbody.contactsListContainer").html(response);
				    		jQuery('#csv-export').show();
				    		meo_crm_contacts_initMap();
						});
				    }
			    });
			});
		

		
		// On export page
		
		jQuery("#csv-export").click(
				function(e){
					e.preventDefault();
					var project_id = jQuery( "select#meo-crm-contacts-projectSelector option:selected" ).val();
					var precision = jQuery(this).attr('data-precision');
					
					if(project_id && project_id > 0){
						location.href = meo_crm_contacts_ajax.meo_crm_contacts_export_url + '?project_id=' + project_id + '&precision=' + precision;
					}
			});
		
	});


/* Action functions */

	// Cancel the form and revert to initial content
	function cancelField(container, initialValue){
		jQuery(container).parents('td').addClass('editable');
		jQuery(container).parents('td').html(initialValue);
	}
	
	// Update the contact edited field
	function updateField(field, id){
		var formId = field+"-"+id;
		var contentToSave = '';
		var contentToPrint = '';
		var precision = jQuery( "#contactsTable").attr("data-precision");
		
		// Contact fields behavior
		if(field == 'country'){
			contentToSave = jQuery("form."+formId+" .fieldToSave").find('option:selected').text();
		}
		else contentToSave = jQuery("form."+formId+" .fieldToSave").val();
		
		contentToPrint = contentToSave;
		
		// Ajax code
		var data = {
			'action': 'update_contact_field',
			'contactID': id,
			'fieldName': field,
			'fieldValue': contentToSave,
			'precision': precision
		};
		
		// User field behavior (Exception)
		if(field == 'courtier'){
			contentToSave = jQuery("form."+formId+" .fieldToSave").find('option:selected').val();
			contentToPrint = jQuery("form."+formId+" .fieldToSave").find('option:selected').text();
			
			var user_id_old = '';
			if(jQuery("form."+formId+" input.user_id_old").length){
				user_id_old = jQuery("form."+formId+" .user_id_old").val();
			}
			
			// Redefine data
			var data = {
				'action': 'meo_crm_relationship_reassign',
				'contact_id': id,
				'user_id': contentToSave,
				'user_name': contentToPrint,
				'user_id_old': user_id_old
			};
			
			jQuery("form."+formId+" .fieldToSave").parents('tr').attr("data-user", contentToSave);
		}
		
		jQuery.post(meo_crm_contacts_ajax.ajax_url, data, function(response) {
			// alert('Got this from the server: ' + response);
		});
		// Add class
		jQuery("tr#"+id+" td."+field).html(contentToPrint);
		jQuery("tr#"+id+" td."+field).addClass('editable');
	}
	
	// Update latitude and longitude for the contact and reset the map
	function updateContactCoordinatesBackend(that){
		// Get Contact fields: address, city, postcode, country
		if(!jQuery(that).parents('tr').find('.address.editable').length 
				|| !jQuery(that).parents('tr').find('.city.editable').length 
				|| !jQuery(that).parents('tr').find('.postcode.editable').length 
				|| !jQuery(that).parents('tr').find('.country.editable').length){
			alert('Please save address changes first');
			return false;
		}
		var address 	= jQuery(that).parents('tr').find('.address.editable').html();
		var city 		= jQuery(that).parents('tr').find('.city.editable').html();
		var postcode 	= jQuery(that).parents('tr').find('.postcode.editable').html();
		var country 	= jQuery(that).parents('tr').find('.country.editable').html();
		var contact_id	= jQuery(that).parents('tr').attr("id");
		
		// Update Contact fields: latitude, longitude
		jQuery.ajax({
	        url: meo_crm_contacts_ajax.ajax_url,
	        data: {
	            'action':'meo_crm_contact_setcoordinates',
	            'contact_id' : contact_id,
	            'address' : address,
		    	'city' : city,
		    	'postcode' : postcode,
		    	'country' : country
	        },
	        method: 'POST',
	        success:function(datas) {
	            console.log(JSON.stringify(datas));
	            //Parsing JSON
	            var obj = jQuery.parseJSON(datas);
	            var Lat = parseFloat(obj.latitude);
	            var Lon = parseFloat(obj.longitude);
	            console.log('lat :'+Lat+' - Lon :'+Lon);
	            meo_crm_contacts_setMap(Lat, Lon, ''+contact_id+'');
	        },
	        error: function(errorThrown){
	            console.log(errorThrown);
	        }
	    });
		return false;
	}

////////////////
// GOOGLE MAP //
////////////////

	function meo_crm_contacts_initMap() {
	    var map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 8,
	        center: {lat: 46.6136509, lng: 7.0592593}
	    });   
	
	    meo_crm_contacts_setMarkers(map);
	}
	function meo_crm_contacts_setMap(latitude, longitude, contact_title){
		var map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 11,
	        center: {lat: latitude, lng: longitude}
	    });
		var marker = new google.maps.Marker({
	        position: {lat: latitude, lng: longitude},
	        map: map,
	        title: contact_title
	    });
	}
	
	function meo_crm_contacts_setMarkers(map)
	{
	
	    var project_id = 0;
	    jQuery( "#meo-crm-contacts-projectSelector option:selected" ).each(function() {
	    	project_id = jQuery( this ).val();
	    });
	    
	    jQuery.ajax({
	        url: meo_crm_contacts_ajax.ajax_url,
	        data: {
	            'action':'get_contacts_coordinates',
	            'project_id' : project_id
	        },
	        success:function(datas) { 
	            var contacts = [];
	            var count = 0;   
	            
	            //Parsing JSON
	            jQuery.each(jQuery.parseJSON(datas), function(idx, obj) {
	                myData = [];
	                myData['title'] = obj.last_name+" "+obj.first_name;
	                myData['latitude'] = obj.latitude;
	                myData['longitude'] = obj.longitude;
	                myData['zindex'] = count;                    
	                contacts.push(myData); 
	                count++;
	            }); 
	            for (var i = 0; i < contacts.length; i++) {
	                var contact = contacts[i];
	                var myLatLng = {lat: parseFloat(contact['latitude']), lng: parseFloat(contact['longitude'])};
	                var marker = new google.maps.Marker({
	                    position: myLatLng,
	                    map: map,
	                    title: contact['title']
	                });
	            }
	        },
	        error: function(errorThrown){
	            console.log(errorThrown);
	        }
	    });
	}