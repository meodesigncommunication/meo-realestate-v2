<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoCrmContacts {
	
	# Contact Attributes
	
		private $id;
		private $client_id;
		private $project_id;
		private $last_name;
		private $first_name;
		private $type;
		private $email;
		private $phone;
		private $address;
		private $postcode;
		private $city;
		private $country;
		private $latitude;
		private $longitude;
		private $note;
		private $language;
		private $date_added;
		private $date_updated;
		private $date_deleted;
		private $deleting_user_id;
	
		public function __construct() { }
		
	# Get allowed fields (const or private static array, when PHP5.6)
		
		public static function getAllowedFields ($precision = ''){
			// AllowedFields to be printed on the contacts table (Front and Admin)
			
			// FRONT INDIVIDUAL
			if($precision == 'FRONT_INDIVIDUAL'){
				$allowedFields = array(	'last_name', 	'address',
										'first_name', 	'city', 
										'type', 		'postcode',
										'email', 		'country', 
										'phone', 		'language', 
										'note', 		'analytics_id',
										'referer');
			}
			// FRONT LIST
			else if($precision == 'FRONT_LIST'){
				$allowedFields = array('id', 'date_added', 'last_name', 'first_name', 'type', 'email', 'phone', 'city', 'country');
				if(current_user_can('administrator')) {
					$allowedFields = array(	'id', 'date_added', 'last_name', 'first_name', 'type',
							'email', 'phone', 'address', 'postcode', 'city', 'country');
				}
			}
			// CSV EXPORT
			else if($precision == 'CSV_EXPORT'){
				$allowedFields = array('id', 'last_name', 'first_name', 'type', 'phone', 'address', 'postcode', 'city', 'country', 'date_added', 'note');
			}
			// ADMIN
			else if(current_user_can('administrator') || $precision == 'ADMIN_LIST') {
				// 'client_id', 
				$allowedFields = array(	'id', 'date_added', 'last_name', 'first_name', 'type',
						'email', 'phone', 'address', 'postcode',
						'city', 'country', 'analytics_id');
			}
			// SAME AS FRONT LIST
			else {
				$allowedFields = array('id', 'date_added', 'last_name', 'first_name', 'type', 'phone', 'city', 'country');
			}
			
			
			$fields = array();
			foreach($allowedFields as $field) {
				$fields[ucwords(str_replace('_', ' ', $field))] = $field;
			}
			
			return $fields;
		}
		
	# Get editable fields (const or private static array, when PHP5.6)
		
		public static function getEditableFields ($precision = ''){
			// AllowedFields to be printed on the contacts table (Front and Admin)

			// ADMIN
			if(is_admin() || current_user_can('administrator')) {
				$editableFields = array(	'id', 'client_id', 'project_id', 'last_name', 'first_name', 'type',
						'email', 'phone', 'address', 'postcode',
						'city', 'country', 'language', 'date_added', 'analytics_id', 'date_deleted');
			}
			// FRONT INDIVIDUAL
			else if($precision == 'FRONT_INDIVIDUAL'){
				$editableFields = array(	'last_name', 	'address', 
											'first_name', 	'city', 
											'type', 'postcode', 
											'country',		'phone', 
											'language', 	'note');
				
			}
			// FRONT LIST
			else if($precision == 'FRONT_LIST') {
				$editableFields = array('last_name', 'first_name', 'phone');
			}
			// SAME AS FRONT LIST
			else {
				$editableFields = array('last_name', 'first_name', 'phone');
			}
			return apply_filters('meocrmcontacts_editableFields', $editableFields, $precision );
		}
		
	# Get addContact fields (const or private static array, when PHP5.6)
		
		public static function getAddContactFields ($precision = '', $filtered = true){
			// AllowedFields to be printed on the contacts table (Front and Admin)
				
			// ADD CONTACT
			if($precision == 'ADD_CONTACT'){
				$addContactsFields = array(	'last_name', 	'address',
											'first_name', 	'city',
											'type', 'postcode',
											'email',		'country',
											'phone',	 	'note',
											'language',		'project_id'
				);
			}
			// ADMIN
			else if(is_admin()) {
				$addContactsFields = array(	'id', 'client_id', 'project_id', 'last_name', 'first_name', 'type',
											'email', 'phone', 'address', 'postcode',
											'city', 'country', 'language', 'date_added', 'analytics_id');
			}
			
			if($filtered){
				return apply_filters('meocrmcontacts_addContactFields', $addContactsFields, $precision );
			}
			else return $addContactsFields;
		}
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
	
	# Manage Contacts in the Database
	
		public static function addContact ($data){
						
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_CONTACTS_TABLE;
			
			if($wpdb->insert( $table, $data )) {
				return $wpdb->insert_id;
			}
			return false;
		}
		
		public static function updateContact ($data, $where){
			
			global $wpdb;
			
			// $data['date_updated'] = date('Y-m-d H:i:s');
			$table = $wpdb->prefix . MEO_CONTACTS_TABLE;
			
			return $wpdb->update( $table, $data, $where );
		}
		
		public static function deleteContactById ($contactID){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_CONTACTS_TABLE;
			
			// Will return 1 or false
			return $wpdb->delete( $table, array( 'id' => $contactID ) );
		}
	
	################
	# Get Contacts #
	################
	
		
		# getContacts
		
		public static function getContacts ($where = '', $limitStart = false, $limitQty = false, $returnType = ARRAY_A){
			
			global $wpdb;

			$limit = '';
			if($limitStart !== false && $limitQty !== false && $limitStart !== 'false' && $limitQty !== 'false'){
				$limit = ' LIMIT '.$limitStart.', '.$limitQty;
			}
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_TABLE . " " . $where . " ORDER BY id DESC". $limit;
			
			return $wpdb->get_results($query, $returnType);
		}
		
		
		# getContactsByContactID
		
		public static function getContactsByContactID ($contactID, $returnType = ARRAY_A){
			
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_TABLE . " WHERE id = " . $contactID . " ORDER BY id DESC";
			
			$contact = $wpdb->get_results($query, $returnType);
			
			if($contact && is_array($contact) && count($contact) > 0) return reset($contact);
			
			return false;
		}
		
		
		# getContactsByEmail
		
		public static function getContactsByEmailAndProjectID ($contactEmail, $project_ID, $returnType = ARRAY_A){
				
			global $wpdb;
			
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_CONTACTS_TABLE . " 
						WHERE email = '" . $contactEmail . "' 
							AND project_id = ". $project_ID ." 
								ORDER BY id DESC";
			
			$contact = $wpdb->get_results($query, $returnType);
				
			if($contact && is_array($contact) && count($contact) > 0) return reset($contact);
				
			return false;
		}
		
		
		# getContactsByUserID
		
		public static function getContactsByUserID ($userID, $limitStart = false, $limitQty = false, $returnType = ARRAY_A){
			
			global $wpdb;

			$limit = '';
			if($limitStart !== false && $limitQty != false){
				$limit = ' LIMIT '.$limitStart.', '.$limitQty;
			}
			
			$query = "SELECT DISTINCT c.* FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE." as c
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE." as cur ON cur.contact_id = c.id
						WHERE cur.user_id = ".$userID."
								ORDER BY c.id DESC"
								. $limit;
			return $wpdb->get_results($query, $returnType);
		}
		
		
		# getContactsByUserIDandProjectID
		
		public static function getContactsByUserIDandProjectID ($userID, $projectID, $limitStart = false, $limitQty = false, $returnType = ARRAY_A){
			
			global $wpdb;

			$limit = '';
			if($limitStart !== false && $limitQty !== false){
				$limit = ' LIMIT '.$limitStart.', '.$limitQty;
			}
			
			$query = "SELECT DISTINCT c.* FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE." as c
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE." as cur ON cur.contact_id = c.id
						WHERE cur.user_id = ".$userID." AND c.project_id = ".$projectID." 
								ORDER BY c.id DESC"
								. $limit;
			
			return $wpdb->get_results($query, $returnType);
		}
		
		
		# getContactsByProjectID
		
		public static function getContactsByProjectID ($projectID, $limitStart = false, $limitQty = false, $returnType = ARRAY_A){
			
			global $wpdb;
			
			$limit = '';
			if($limitStart !== false && $limitQty !== false){
				$limit = ' LIMIT '.$limitStart.', '.$limitQty;
			}
			
			$query = "SELECT * FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE." 
						WHERE project_id = ".$projectID."
								ORDER BY id DESC"
								. $limit;
			
			return $wpdb->get_results($query, $returnType);
		}
		
		# getContactsSearch
		
		public static function getContactsSearch ($keyword, $projects, $fields, $returnType = ARRAY_A){
			
			global $wpdb;
			
			$fieldNames = explode('-', $fields);
				
			$query = "SELECT * FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE."
						WHERE project_id IN (".str_replace('-', ',', $projects).") AND (";

			$queryFields = '';
			foreach($fieldNames as $fname){
				$queryFields.= "".$fname." LIKE '%".$keyword."%' OR ";
			}
			
			if($queryFields != ''){
				$query.= substr($queryFields, 0, -4);
			}
			
			$query.= ") ORDER BY id DESC";
			
			return $wpdb->get_results($query, $returnType);
		}
		
		
		# countContactsByProjectID
		
		public static function countContactsByProjectID ($projectID, $date1 = '', $date2 = '', $returnType = ARRAY_A){
			global $wpdb;
			
			$query = "SELECT COUNT(DISTINCT(c.ID)) AS number_of_contacts FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE." as c
						LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE." as cur ON cur.contact_id = c.id
						WHERE c.project_id = ".$projectID." 
							AND c.date_deleted IS NULL";
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$count = $wpdb->get_results($query, $returnType);
			return reset($count);
		}
		
		
		# countContactsByUserIDandProjectID
		
		public static function countContactsByUserIDandProjectID ($userID, $projectID, $date1 = '', $date2 = '', $returnType = ARRAY_A){
			global $wpdb;
			
			$query = "SELECT COUNT(DISTINCT(c.ID)) AS number_of_contacts FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE." as c ";
			
			if(user_can($userID, CLIENT_ROLE)){
				$query.= "WHERE c.client_id = ".$userID."";
			}
			else if(user_can($userID, COURTIER_ROLE)){
				$query.= "LEFT JOIN ".$wpdb->prefix . MEO_CONTACTS_USERS_RELATIONSHIP_TABLE." as cur ON cur.contact_id = c.id 
							WHERE cur.user_id = ".$userID."";
			}
			
			$query.= " AND c.project_id = ".$projectID." AND c.date_deleted IS NULL";
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND c.date_added > \''.$date1.'\' AND c.date_added < \''.$date2.'\'';
			}
			
			$count = $wpdb->get_results($query, $returnType);
			return reset($count);
		}
	
	##############
	# Statistics #
	##############
	
		public static function countContactPerDistinctRefererByProjectID($projectID, $date1 = '', $date2 = ''){
		
			global $wpdb;
			
			// Get Records
			$query = "SELECT IFNULL(referer, 'Direct') as referer, COUNT(id) as nbcontacts 
						FROM ".$wpdb->prefix . MEO_CONTACTS_TABLE." 
							WHERE project_id = ".$projectID." 
								AND date_deleted IS NULL";
			
			if($date1 != '' && $date2 != ''){
				$query.= ' AND date_added > \''.$date1.'\' AND date_added < \''.$date2.'\'';
			}
			
			$query.= "			GROUP BY referer
						ORDER BY nbcontacts DESC";
			
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) return $records;
			
			return false;
		}
}
