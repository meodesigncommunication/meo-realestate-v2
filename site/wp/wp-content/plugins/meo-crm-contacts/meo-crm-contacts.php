<?php

/*
Plugin Name: MEO CRM Contacts
Description: Plugin to administrate Contacts
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );


# Defines / Constants
define('MEO_CONTACTS_PLUGIN_ROOT', 				$plugin_root);
define('MEO_CONTACTS_PLUGIN_SLUG', 				'meo-crm-contacts');
define('MEO_CONTACTS_TABLE', 					'meo_crm_contacts');	// Contacts db table
define('MEO_CONTACTS_FOLDER', 					'contacts');			// Contacts folder name under the corresponding project id
define('MEO_CONTACTS_LIST_LIMIT', 				200);					// Number of contacts per pagination

// Contacts Generate PDF
define('MEO_CONTACTS_GENERATE_PDF_SLUG', 					'meo-crm-contacts-generate-pdf');
define('MEO_CRM_CONTACTS_GENERATE_PDF_TPL_FRONT', 			'../../'.MEO_CONTACTS_PLUGIN_SLUG.'/views/frontend/meo-crm-contacts-generate-pdf.php');
// Contacts List page
define('MEO_CONTACTS_SLUG', 					'meo-crm-contacts');
define('MEO_CRM_CONTACTS_TPL_FRONT', 			'../../'.MEO_CONTACTS_PLUGIN_SLUG.'/views/frontend/meo-crm-contacts-list-view.php');
// Contact page
define('MEO_CONTACT_SLUG', 						'contact');
define('MEO_CRM_CONTACT_TPL_FRONT', 			'../../'.MEO_CONTACTS_PLUGIN_SLUG.'/views/frontend/meo-crm-contacts-contact-view.php');
// Add Contact page
define('MEO_ADD_CONTACT_SLUG', 					'add-contact');
define('MEO_CRM_ADD_CONTACT_TPL_FRONT', 		'../../'.MEO_CONTACTS_PLUGIN_SLUG.'/views/frontend/meo-crm-contacts-add-contact.php');
// Export page
define('MEO_CONTACTS_EXPORTS_SLUG', 			'meo-crm-contacts-exports');
define('MEO_CRM_CONTACTS_EXPORTS_TPL_FRONT', 	'../../'.MEO_CONTACTS_PLUGIN_SLUG.'/views/frontend/meo-crm-contacts-export-view.php');
// Search Contact page
define('MEO_CONTACTS_SEARCH_SLUG', 				'search-contacts');
define('MEO_CRM_CONTACTS_SEARCH_TPL_FRONT', 	'../../'.MEO_CONTACTS_PLUGIN_SLUG.'/views/frontend/meo-crm-contacts-search-view.php');



// Contacts status
define('MEO_CONTACTS_STATUS_MANUAL', 	'manual');
define('MEO_CONTACTS_STATUS_FILE', 		'file');
define('MEO_CONTACTS_STATUS_CONTACT', 	'contact');
define('MEO_CONTACTS_STATUS_ALERT', 	'alert');
define('MEO_CONTACTS_STATUS_INFO', 		'day_info');
define('MEO_CONTACTS_STATUS_NEWS', 		'newsletter');
define('MEO_CONTACTS_STATUS_BUY', 		'achat');

define('MEO_CONTACTS_STATUS_DEFAULT', 	MEO_CONTACTS_STATUS_MANUAL);

# Required Files
require_once( $plugin_root . 'class-meo-crm-contacts.php');
require_once( $plugin_root . 'classes/meo-crm-contacts-pdf.php');
require_once( $plugin_root . 'ajax/meo-crm-contacts-ajax.php');
require_once( $plugin_root . 'views/backend/meo-crm-contacts-view.php');


# Globals
global $meocrmcontacts, $meo_crm_contacts_db_version, $contactsTypes, $contactsSearchFields;

$meocrmcontacts = new MeoCrmContacts();
$meo_crm_contacts_db_version = '1.0';
$contactsTypes = array(
			MEO_CONTACTS_STATUS_MANUAL 		=> 'fa-hand-paper-o',
			MEO_CONTACTS_STATUS_FILE 		=> 'fa-file-o',
			MEO_CONTACTS_STATUS_CONTACT 	=> 'fa-comment-o',
			MEO_CONTACTS_STATUS_ALERT 		=> 'fa-bell-o',
			MEO_CONTACTS_STATUS_INFO 		=> 'fa-users',
			MEO_CONTACTS_STATUS_NEWS 		=> 'fa-envelope-o',
			MEO_CONTACTS_STATUS_BUY 		=> 'fa-shopping-cart'
	);
$contactsSearchFields = array('last_name', 'first_name', 'email', 'phone', 'city', 'postcode', 'country');

/*
 * Check for Dependencies :: MEO CRM Project
 */
function meo_crm_contacts_activate() {

	$installed_dependencies = false;
	if ( is_plugin_active( 'meo-crm-projects/meo-crm-projects.php' ) ) {
		$installed_dependencies = true;
	}

	if(!$installed_dependencies) {

		// WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
		// and this will prevent WordPress to activate the plugin.
		echo '<div class="notice notice-error"><h3>'.__('Please install and activate the MEO CRM Project plugin before', 'meo-realestate').'</h3></div>';

		//Adding @ before will prevent XDebug output
		@trigger_error(__('Please install and activate the MEO CRM Project plugin before.', 'meo-realestate'), E_USER_ERROR);
		exit;


	}else{

		// Everything is fine

		global $wpdb, $meo_crm_contacts_db_version;
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$table_name = $wpdb->prefix . MEO_CONTACTS_TABLE;
		$charset_collate = $wpdb->get_charset_collate();

		// Create plugin table
		$sql = "CREATE TABLE " . $wpdb->prefix . $table_name . " (
				id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				client_id bigint(20) unsigned NOT NULL,
				project_id bigint(20) unsigned NOT NULL,
				last_name varchar(100) NOT NULL,
				first_name varchar(100) NULL,
				type ENUM('".MEO_CONTACTS_STATUS_MANUAL."', '".MEO_CONTACTS_STATUS_FILE."', '".MEO_CONTACTS_STATUS_CONTACT."', '".MEO_CONTACTS_STATUS_ALERT."', '".MEO_CONTACTS_STATUS_INFO."', '".MEO_CONTACTS_STATUS_NEWS."', '".MEO_CONTACTS_STATUS_BUY."') DEFAULT '".MEO_CONTACTS_STATUS_DEFAULT."',
				email varchar(100) NULL,
				phone varchar(100) NULL,
				address varchar(100) NULL,
				postcode varchar(100) NULL,
				city varchar(100) NULL,
				country varchar(100) NULL,
				latitude float NOT NULL,
				longitude float NOT NULL,
				note text DEFAULT NULL,
				language varchar(100) NOT NULL,
				date_added datetime DEFAULT NULL,
				date_deleted datetime DEFAULT NULL,
				deleting_user_id bigint(20) unsigned DEFAULT NULL,
				analytics_id varchar(100) NULL,
				referer varchar(128) NULL,
				PRIMARY KEY (id),
				UNIQUE KEY project_email (project_id, email)
				) $charset_collate;";
		
		dbDelta( $sql );

		// Update db with the version info
		add_option( 'meo_crm_contacts_db_version', $meo_crm_contacts_db_version );
		
		// Call project function to create 'contacts' folders under the existing projects
        createFolderAllProject('contacts');
                
        // Create action for generate page
        do_action('plugins_meo_crm_active_create_page');
	}

}
register_activation_hook(__FILE__, 'meo_crm_contacts_activate');

// Installation and uninstallation hooks
//register_deactivation_hook(__FILE__, array('MeoCrmContacts', 'deactivate'));

# Add Scripts and Styles

	# Admin
	
	add_action( 'admin_enqueue_scripts', 'meo_crm_contacts_scripts_styles' );
	
	function meo_crm_contacts_scripts_styles() {
		
		# JS
		
		wp_register_script( 'meo_crm_contacts_js',  plugins_url('js/meo-crm-contacts.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_contacts_js' );
		
		wp_localize_script( 'meo_crm_contacts_js', 'meo_crm_contacts_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'meo_crm_contacts_export_url' => esc_url( home_url( '/' ) ).MEO_CONTACTS_EXPORTS_SLUG ) );
		
		# CSS
		
		wp_register_style( 'meo_crm_contacts_css',  plugins_url('css/meo-crm-contacts.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_contacts_css' );
		
	}
	
	# Front
	
	add_action( 'wp_enqueue_scripts', 'meo_crm_contacts_scripts_styles_front' ); // TODO::Check if admin stuff is printed onto frontend
	
	function meo_crm_contacts_scripts_styles_front() {
	
		# JS
		wp_register_script( 'meo_crm_contacts_js',  plugins_url('js/front/meo-crm-contacts.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_contacts_js' );
	
		wp_localize_script( 'meo_crm_contacts_js', 'meo_crm_contacts_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'meo_crm_contacts_export_url' => esc_url( home_url( '/' ) ).MEO_CONTACTS_EXPORTS_SLUG ) );
	
		# CSS
		wp_register_style( 'meo_crm_contacts_css',  plugins_url('css/meo-crm-contacts.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_contacts_css' );
	
	}




#########
# ADMIN #
#########


	# Admin Menu
	add_action( 'admin_menu', 'meo_crm_contacts_admin_menu' );
	
	function meo_crm_contacts_admin_menu() {
		add_menu_page( 'MEO CRM Contacts',
				'Contacts',
				'manage_options',
				'meo-crm-contacts/views/backend/meo-crm-contacts-view.php',
				'meo_crm_contacts_admin_page',
				'dashicons-admin-users',
				6  );
	}
	
	# Admin Page
	
		# Table Head : Get Plugin Class allowed fields and more
		function meo_crm_contacts_getAllowedFields($precision = ''){
		
			$allowedFields = MeoCrmContacts::getAllowedFields($precision);
		
			// Call to compatible plugins
			$allowedFields = apply_filters('meocrmcontacts_allowedFields', $allowedFields, $precision);
		
			return $allowedFields;
		}
		
		# Get Contact editable fields list
		function meo_crm_contacts_getEditableFields($precision = ''){
		
			$editableFields = MeoCrmContacts::getEditableFields($precision);
		
			// Call to compatible plugins
			$editableFields = apply_filters('meocrmcontacts_editablefields', $editableFields, $precision);
		
			return $editableFields;
		}
		
		# Get Contact add contact fields list
		function meo_crm_contacts_getAddContactFields($precision = '', $filtered = true){
		
			return MeoCrmContacts::getAddContactFields($precision, $filtered);
		}
		
		
		# Table Cells : Call to compatible plugins
		function meo_crm_contacts_prepareContactCells($contact){
				
			$contact = apply_filters('meocrmcontacts_getContactData', $contact);
				
			return $contact;
		}
		
		# Table Cells : Call to compatible plugins FRONT
		function meo_crm_contacts_prepareContactCellsFront($contact){
		
			// TODO::Code specific behavior for front
		
			return meo_crm_contacts_prepareContactCells($contact);
		}
		
		# Table Cells : Call to compatible plugins FRONT INDIVIDUAL page
		function meo_crm_contacts_prepareContactCellsFrontIndividual($contact){
		
			// TODO::Code specific behavior for front
		
			return meo_crm_contacts_prepareContactCells($contact);
		}
		
		# getContactByContactID
		
		function meo_crm_contacts_getContactByContactID($contactID){
		
			$contact = MeoCrmContacts::getContactsByContactID($contactID);
				
			if($contact){
					
				$contact = apply_filters('meocrmcontacts_getContactData', $contact);
					
				return $contact;
			}
				
			return false;
		}
		
		# getContactsByUserID

		function meo_crm_contacts_getContactsByUserID($userID, $limitStart = false, $limitQty = false){
		
			$contacts = MeoCrmContacts::getContactsByUserID($userID, $limitStart, $limitQty);
			
			if($contacts){
				 
				$returnedContacts = array();
				 
				foreach($contacts as $contact){
					$contact = apply_filters('meocrmcontacts_getContactData', $contact);
					if($contact){
						$returnedContacts[] = $contact;
					}
				}
				 
				return $returnedContacts;
			}
			
			return false;
		}
		
		# getContactsByUserIDandProjectID

		function meo_crm_contacts_getContactsByUserIDandProjectID($userID, $projectID, $limitStart = false, $limitQty = false){
			
			$contacts = MeoCrmContacts::getContactsByUserIDandProjectID($userID, $projectID, $limitStart, $limitQty);
				
			if($contacts){
					
				$returnedContacts = array();
					
				foreach($contacts as $contact){
					$contact = apply_filters('meocrmcontacts_getContactData', $contact);
					if($contact){
						$returnedContacts[] = $contact;
					}
				}
					
				return $returnedContacts;
			}
				
			return false;
		}
		
		# getContactsByProjectID
		
		function meo_crm_contacts_getContactsByProjectID($projectID, $limitStart = false, $limitQty = false){
				
			$contacts = MeoCrmContacts::getContactsByProjectID($projectID, $limitStart, $limitQty);
		
			if($contacts){
					
				$returnedContacts = array();
					
				foreach($contacts as $contact){
					$contact = apply_filters('meocrmcontacts_getContactData', $contact);
					if($contact){
						$returnedContacts[] = $contact;
					}
				}
					
				return $returnedContacts;
			}
		
			return false;
		}
		
		# countContactsByProjectID
		
		function meo_crm_contacts_countContactsByProjectID($projectID, $date1 = '', $date2 = ''){
		
			$contacts = MeoCrmContacts::countContactsByProjectID($projectID, $date1, $date2);
				
			return $contacts['number_of_contacts'];
		}
		
		# countContactsByUserIDandProjectID
		
		function meo_crm_contacts_countContactsByUserIDandProjectID($userID, $projectID, $date1 = '', $date2 = ''){
			
			# Check if user is admin
			if(user_can($userID, 'administrator')){
				$contacts = MeoCrmContacts::countContactsByProjectID($projectID, $date1, $date2);
			}
			else $contacts = MeoCrmContacts::countContactsByUserIDandProjectID($userID, $projectID, $date1, $date2);
					
			return $contacts['number_of_contacts'];
		}
		
		# countContactPerDistinctRefererByProjectID
		function meo_crm_contacts_countContactPerDistinctRefererByProjectID($projectID, $date1 = '', $date2 = ''){

			$referers = MeoCrmContacts::countContactPerDistinctRefererByProjectID($projectID, $date1, $date2);
			
			return $referers;
		}

#########
# FRONT #
#########


	# PAGE CREATION
	
		# Register Plugin templates
		
		add_filter('meo_crm_core_templates_collector', 'meo_crm_contacts_templates_register', 1, 1);
		
		function meo_crm_contacts_templates_register($pluginsTemplates){
			
			$pluginsTemplates[MEO_CRM_CONTACTS_GENERATE_PDF_TPL_FRONT]  = 'Contacts Generate PDF';
            $pluginsTemplates[MEO_CRM_CONTACTS_TPL_FRONT]   		    = 'Contacts List';
			$pluginsTemplates[MEO_CRM_CONTACT_TPL_FRONT]   			    = 'Contact';
			$pluginsTemplates[MEO_CRM_ADD_CONTACT_TPL_FRONT]   		    = 'Add Contact';
			$pluginsTemplates[MEO_CRM_CONTACTS_EXPORTS_TPL_FRONT] 	    = 'Contacts Export';
			$pluginsTemplates[MEO_CRM_CONTACTS_SEARCH_TPL_FRONT] 	    = 'Search Contacts';
				
			return $pluginsTemplates;
		}
		
		# Register Plugin Pages
		
		add_filter( 'meo_crm_core_front_pages_collector', 'meo_crm_contacts_pages_register' );
		
		function meo_crm_contacts_pages_register($pluginsPages) {
		
			# Create Page for Contacts
			$pluginsPages[MEO_CONTACTS_GENERATE_PDF_SLUG] = array(
			                                                    'post_title' 	=> 'Contacts Generate PDF',
																'post_content' 		=> 'Page to be used for generate contact PDF file',
																'_wp_page_template' => MEO_CRM_CONTACTS_GENERATE_PDF_TPL_FRONT);
			
			# Create Page for Individual Contact
			$pluginsPages[MEO_CONTACT_SLUG] = array(			'post_title' 		=> 'Contact',
																'post_content' 		=> 'Page to be used for individual contact.',
																'_wp_page_template' => MEO_CRM_CONTACT_TPL_FRONT);
			
			# Create Page to Add Contact
			$pluginsPages[MEO_ADD_CONTACT_SLUG] = array(		'post_title' 		=> 'Add Contact',
																'post_content' 		=> 'Page to be used to add a contact.',
																'_wp_page_template' => MEO_CRM_ADD_CONTACT_TPL_FRONT);
			
			# Create Page for Export
			$pluginsPages[MEO_CONTACTS_EXPORTS_SLUG] = array(	'post_title' 		=> 'Contacts Export',
																'post_content' 		=> 'Page to be used for exports (CSV, XLS etc).',
																'_wp_page_template' => MEO_CRM_CONTACTS_EXPORTS_TPL_FRONT);
			
			# Create Page for Search
			$pluginsPages[MEO_CONTACTS_SEARCH_SLUG] = array(	'post_title' 		=> 'Search Contacts',
																'post_content' 		=> 'Page to be used to search Contacts',
																'_wp_page_template' => MEO_CRM_CONTACTS_SEARCH_TPL_FRONT);
				
			return $pluginsPages;
		}


########################
# EXTEND OTHER PLUGINS #
########################
		
		# MEO CRM Projects

		
		# Add projects fields Validation
		add_filter('projectsFormValidate', 'meo_crm_contacts_projectsformvalidate');
		
		function meo_crm_contacts_projectsformvalidate($projectID){
			
			# Create Project Folder
			createProjectUnderFolder($projectID, 'contacts');
			
			# Get Project's Client ID
			$project = meo_crm_projects_getProjectInfo($projectID);
			
			# Change, if any, contacts client id
			if($project && array_key_exists('clients', $project) && is_array($project['clients']) && count($project['clients']) > 0){
				
				MeoCrmContacts::updateContact( array('client_id' => reset( $project['clients']) ), array('project_id' => $projectID ) );
			}
			
			return $projectID;
		}
		
		