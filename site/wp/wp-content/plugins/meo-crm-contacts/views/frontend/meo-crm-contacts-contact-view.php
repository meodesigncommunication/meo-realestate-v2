<?php 
/**
 * Edit Contact
 */

global $current_user;

if(!isset($_GET) || !array_key_exists('id', $_GET) || !$current_user){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access contact data with no Contact ID");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}

get_header(); 

# Precision will be used to get Allowed/Editable fields, and passed to jQuery thanks to data-precision
$precision = 'FRONT_INDIVIDUAL';

$allowedFields = meo_crm_contacts_getAllowedFields($precision);
$editableFields = meo_crm_contacts_getEditableFields($precision);

# Get Contact info
$contact = meo_crm_contacts_getContactByContactID($_GET['id']);

if(!$contact){
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No contact data found for contact ID" . $_GET['id']);
	MeoCrmCoreTools::meo_crm_core_403();
}
else {

	# Get Project info
	$project = meo_crm_projects_getProjectInfo($contact['project_id']);
	
	# Build address for Google Map
	$tempAddress = $contact['address'].','.$contact['postcode'].','.$contact['city'].','.$contact['country'];
	$builtAddress = str_replace(" ", "+", $tempAddress);
	
	# Building Piwik Link
	$piwikVisitorUrl = "http://".MEO_ANALYTICS_SERVER."/index.php?
								date=".date('Y-m-d')."
								&module=Widgetize
								&action=iframe
								&visitorId=".$contact['analytics_id']."
								&idSite=".$project['piwik_id']."
								&period=day
								&moduleToWidgetize=Live
								&actionToWidgetize=getVisitorProfilePopup"."
								&token_auth=".$project['piwik_token'];
	
	
	// Marked as deleted
	$classTitle = '';
	if(!is_null($contact['date_deleted'])){
		$classTitle = ' class="warning-text"';
	}
	
	// Tracking Error
	$trackingError = '';
	if($contact['note'] == 'TRACKING ERROR'){
		$trackingError = ' - Tracking Error';
	}
	?>
	<div id="primary" class="content-area meo-crm-contacts-front-container individual-contact" data-project="<?php echo $contact['project_id']; ?>">
		<main id="main" class="project-main" role="main">
			
				<div class="wrap" id="edit-contact">
					<h3><?php echo $project['name']; ?></h3>
					<h2 id="<?php echo $contact['id']; ?>"<?php echo $classTitle; ?>><?php echo "Contact: ".$contact['last_name']." ". $contact['first_name'].$trackingError; ?></h2>
					<em class="em-left"><?php echo __('Double click to edit editable fields on the page'); ?></em>
					<?php 
					
					# Admins will be able to truely delete contacts from the system, triggering delete action on other plugins
					# Clients will only set them as deleted (it's an update), the contact won't be shown to the client
					# Courtier can not delete contacts
					
					$maskDeleteBtnTitle = 'Delete Contact';
					
					if(current_user_can('administrator')){ 
						$maskDeleteBtnTitle = 'Mark as Deleted';
						?>
						<em class="em-right warning-text">Delete Contact <i class="fa fa-trash-o cursor-hand" aria-hidden="true" onclick="deleteContact(<?php echo $contact['id'].', \''.home_url().'/'.MEO_CONTACTS_SLUG.'\''; ?>)"></i></em>
						<?php 
						if(is_null($contact['date_deleted'])){
							?>
							<em class="em-right warning-text"><?php echo $maskDeleteBtnTitle; ?> <i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact(<?php echo $contact['id'].', \''.home_url().'/'.MEO_CONTACTS_SLUG.'\''; ?>)"></i></em>
						<?php 
						}
						else {
						?>
							<em class="em-right">Reactivate Contact <i class="fa fa-refresh cursor-hand" aria-hidden="true" onclick="updateActivateContact(<?php echo $contact['id'].', \'\''; ?>)"></i></em>
						<?php 
						}
					}
					
					if(current_user_can(CLIENT_ROLE) && is_null($contact['date_deleted'])){
						?>
							<em class="em-right warning-text"><?php echo $maskDeleteBtnTitle; ?> <i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact(<?php echo $contact['id'].', \''.home_url().'/'.MEO_CONTACTS_SLUG.'\''; ?>)"></i></em>
						<?php 
					}
					?>
					<div class="clear"></div>
				</div>
				
				<?php do_action('meo_crm_contacts_editContact_top', $contact); ?>
				
				<div class="portlet details contact-crm-template clear">
			
					<div class="portlet-body">
			
						<h4><span class="first-contact"></span></h4>
						<table class="table individual-contact" id="contactsTable" data-precision="<?php echo $precision; ?>">
							<tbody>
								<?php $field_index = 0;
								foreach ($allowedFields as $kField => $field) {
									
									$value = '';
									if(array_key_exists($field, $contact)){
										$value = $contact[$field];
									}
									
									$editable = '';
									if(in_array($field, $editableFields)) $editable = ' editable';
									
									if ($field_index % 2 == 0) {
										?><tr class="tr_contact" id="<?php echo $contact['id']; ?>" data-project="<?php echo $contact['project_id']; ?>" data-user="<?php echo $contact['courtier_id']; ?>"><?php
									} ?>
									<th style="width: 16.6%"><?php echo $kField; ?></th>
									<td style="width: 33.3%" class="<?php echo $field.$editable; ?>"><?php echo $value; ?></td>
									<?php if ($field_index % 2 != 0) {
										?></tr><?php
									}
									$field_index++;
								}
								
								if ($field_index % 2 != 0) { ?>
										<th style="width: 16.6%">&nbsp;</th>
										<td style="width: 33.3%">&nbsp;</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						
					</div>
				</div>
				
				<div class="meo_crm_analytics_piwikvisitor">
					<a href="#google-map-anchor" class="" onclick="updateContactCoordinates();"><?php echo __('Recalculate Map Coordinates'); ?></a>
				</div>
				
				<a name="google-map-anchor"></a>
				
				<div id="google-map">
				    <div id="map"></div>
				</div>
				
				<?php /* 
				<a class="map-wrapper" href="http://maps.google.com/maps?q=<?php echo $builtAddress; ?>&amp;t=m&amp;z=12" target="_blank">
					<img src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $builtAddress; ?>&amp;markers=color:red%7Clabel:C%7C<?php echo $contact['latitude'].','.$contact['longitude'] ;?>&amp;zoom=13&amp;size=1140x150&amp;maptype=roadmap&amp;scale=2">
				</a>
				*/ ?>
				
				<?php do_action('meo_crm_contacts_editContact_bottom', $contact); ?>
				
				<?php 
				
				if($contact['analytics_id'] != '' && $trackingError == ''){
					
					# It will make a 'Ajax' call to corresponding immo project
					$dataAnalytics = MeoCrmAnalyticsPiwik::getContactDataByAnalyticsId($project, $contact['analytics_id']);
					
					/*
					if(current_user_can('administrator')){
						echo $project['url'] . $project['api_uri'] . '?action=get_contact&api_key=' . $project['api_key'] . '&analytics_id=' . $contact['analytics_id'].'<br/>';
						print_r($dataAnalytics);
						echo '<br/>';
						print_r($contact);
						echo '<br/>';
						print_r($project);
					}
					*/
					?>
					<div class="redband center">Visits</div>
					
					<table class="table analytics">
						<tbody>
							<tr>
								<th style="width: 50%">Lots viewed</th>
								<th style="width: 50%">Level of interest</th>
							</tr>
							<tr>
								<td><span class="lots-viewed"><?php
									if (!empty($dataAnalytics['lots_viewed']) && count($dataAnalytics['lots_viewed']) > 0) {
										$first_width = null;
										foreach ($dataAnalytics['lots_viewed'] as $lot) {
											// if ($lot && is_object($lot) &&  (!isset($lot->label) || ( isset($lot->label) && !isset($seen[$lot->label]) ) ) ) {
											if ($lot && is_object($lot) && isset($lot->label)) {
												if ($first_width === null) {
													$first_width = empty($lot->sum_time_spent) ? 1 : $lot->sum_time_spent;
												}
												echo '<div class="page-name-wrapper"><span class="page-name">' . $lot->label . ' - ' . $lot->nb_rooms . '</span> <span class="bar" style="width: ' . round( $lot->sum_time_spent / $first_width * 100 / 2.5) . '%;">&nbsp;</span>&nbsp;' . gmdate("i:s", $lot->sum_time_spent) . '</div>';
											}
		
										}
									}
								?></span></td>
								<td><span class="sizes-viewed"><?php
									if (!empty($dataAnalytics['lot_sizes']) && count($dataAnalytics['lot_sizes']) > 0) {
										
										$first_width = null;
										foreach ($dataAnalytics['lot_sizes'] as $size) {
											
											if ($size && is_object($size) && isset($size->label)) {
												if ($first_width === null) {
													$first_width = empty($size->sum_time_spent) ? 1 : $size->sum_time_spent;
												}
												echo '<div class="page-name-wrapper"><span class="page-name">' . $size->label . 'p</span> <span class="bar" style="width: ' . round( $size->sum_time_spent / $first_width * 100 / 2.5) . '%;">&nbsp;</span>&nbsp;' . gmdate("i:s", $size->sum_time_spent) . '</div>';
											}
										
										}
									}
								?></span></td>
							</tr>
		
							<tr>
								<th>Items downloaded</th>
								<th>Pages viewed</th>
							</tr>
							<tr>
								<td><span class="lots-downloaded"><?php
									if (!empty($dataAnalytics['downloads']) && count($dataAnalytics['downloads']) > 0) {
										
										foreach ($dataAnalytics['downloads'] as $download) {
											
											if ($download && is_object($download) && isset($download->label)) {
												echo '<div class="page-name-wrapper"><span class="page-name">' . $download->label . '</span></div>';
											}
									
										}
									}
								?></span></td>
								<td><span class="pages-viewed"><?php
									if (!empty($dataAnalytics['pages_viewed']) && count($dataAnalytics['pages_viewed']) > 0) {
										$first_width = null;
										foreach ($dataAnalytics['pages_viewed'] as $page) {
											if ($page && isset($page->label)) {
												if ($first_width === null) {
													$first_width = empty($page->sum_time_spent) ? 1 : $page->sum_time_spent;
												}
		
												echo '<div class="page-name-wrapper"><span class="page-name">' . $page->label . '</span> <span class="bar" style="width: ' . round( $page->sum_time_spent / $first_width * 100 / 2.5) . '%;">&nbsp;</span>&nbsp;' . gmdate("i:s", $page->sum_time_spent) . '</div>';
											}
										}
									}
								?></span></td>
							</tr>
						</tbody>
					</table>                                
					<div class="meo_crm_analytics_piwikvisitor">
						<a href="<?php echo $piwikVisitorUrl; ?>" class="fancybox fancybox.iframe">Voir le profil des visites</a>
					</div>
				<?php 
				} // /if $contact['analytics_id']
				?>
	                                <input type="hidden" name="project_id_sale" value="<?php echo $contact['project_id'] ?>" />
	                                <input type="hidden" name="contact_id_sale" value="<?php echo $contact['id'] ?>" />
	                                <div id="sale-container-contact">
	                                    
	                                </div>
				<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJUW5npc6oqm-919njldenzPwnscUTLE&callback=meo_crm_contact_initMap"></script>
				<?php 
			?>
		</main><!-- .project-main -->
	
	
	</div><!-- .content-area -->
	
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('.fancybox').fancybox();
		});
	</script>
<?php 
}

get_footer(); ?>