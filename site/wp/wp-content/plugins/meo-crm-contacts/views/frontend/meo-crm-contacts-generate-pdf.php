<?php
/*
 * Template de page pour generer des fiche contact PDF a partir du contact_id
 * http://smartcapture.ch/meo-crm-contacts-generate-pdf/?cron_key=e769c5776c5fbb7c&id=[ID_CONTACT]
 */
global $wpdb;
$pdf_email_cron_key = get_option('mca_cron_key');

// Shouldn't really matter, but we don't want this URL to be hit without permission
if (!array_key_exists('cron_key', $_GET) || $_GET['cron_key'] != $pdf_email_cron_key) {
    header("HTTP/1.0 403 Forbidden"); ?>
    <h1>Access Forbidden!</h1>
    <?php exit;
}

# Check ID contact
if(!isset($_GET['id']) || empty($_GET['id'])){
    echo 'Error generating PDF <br>';
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error generating PDF : contact_id (id) est vide");
}else $id = $_GET['id'];

# Get Contact info and Project info
$contact = meo_crm_contacts_getContactByContactID($id);

if(!empty($contact)) {

    # Get data for generate PDF
    $project = meo_crm_projects_getProjectInfo($contact['project_id']);

    if(!empty($contact['analytics_id'])) {
        $dataAnalytics = MeoCrmAnalyticsPiwik::getContactDataByAnalyticsId($project, $contact['analytics_id']);
    }else{
       $dataAnalytics = $contact;
    }

    $dataAnalytics['project'] = $project;

    # Define path for save the PDF file
    $PDFpath = MEO_CRM_PROJECTS_FOLDER_DIR . $contact['project_id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER;
    $PDFfile = $PDFpath . '/' . meo_crm_analytics_building_contact_pdf($contact);

    $url_show_pdf = $contact['project_id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER . '/' . meo_crm_analytics_building_contact_pdf($contact);

    # Check if file exist and if exist delete this
    if (file_exists($PDFfile)) {
        unlink($PDFfile);
    }
    try {
        # Get the PDF Template to write on it
        $PDFtemplate = MEO_CRM_PROJECTS_FOLDER_DIR . $contact['project_id'] . '/' . MEO_CRM_ANALYTICS_PDF_DIRNAME . '/' . $project['pdf_template'];

        # Create and save the contavt PDF file
        $pdf = new MeoCrmContactPdf('en', $PDFtemplate);
        $pdf->generate($dataAnalytics, 0, 0);
        $pdf->Output($PDFfile, 'F');

        // Il sera nommé downloaded.pdf
        header('Content-Type: application/pdf');
        readfile($PDFfile);

    }
    catch (Exception $e) {

        echo 'Error generating PDF for ' . $potential_contact['analytics_id'] . '<br>';
        MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error generating PDF for " . print_r($dataContact, true));
    }

}else{

    echo 'Error generating PDF <br>';
    MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error generating PDF : Contact is empty -> " . print_r($contact, true));

}