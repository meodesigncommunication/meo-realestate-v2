<?php 
/*
 * CSV Export Page for Contacts
 * 
 * 
 * 
 * We will use this page to export project contacts and search results
 */

global $wpdb;

$isSearch = false;

####################
# PROJECT CONTACTS #
####################

	if(array_key_exists('project_id', $_GET) && array_key_exists('precision', $_GET)){
		
		# Get Project ID from Ajax call
		$project_id = $_GET['project_id'];
		$precision 	= $_GET['precision'];
		
		# Test if Project ID is correct
		if(!isset($project_id) || empty($project_id) || $project_id == '0' || empty($precision) || $precision == '') {
			
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to export contacts with Project ID (" . $project_id . ' precision '.$precision.')');
			die('Forbidden Access');
			
		}
		
		# Get Project Info
		$project = meo_crm_projects_getProjectInfo($project_id);
		
		# Test if project exists
		if(!$project || (is_array($project) && count($project) == 0)) {
		
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to export contacts with unexisting or unauthorized Project ID (" . $project_id . ')');
			die('Forbidden Access');
		
		}
		
		# Get corresponding Project Contacts
		$projectContacts = meo_crm_contacts_getContactsByProjectID($project_id);
		
		# Do we have contacts for this project?
		if(!$projectContacts || !is_array($projectContacts) || count($projectContacts) == 0){
			
			die('No Contacts available');
		}
		
		$filename = str_replace(' ', '-', $project['name']).'_contacts.csv';
	}

##################
# SEARCH RESULTS #		
##################

	else if(array_key_exists('keyword', $_GET) && array_key_exists('keyword', $_GET) && array_key_exists('fields', $_GET)){
		
		$keyword = $_GET['keyword'];
		$projects = $_GET['projects'];
		$fields = $_GET['fields'];
		
		$precision = 'CSV_EXPORT';
		
		$projectContacts = MeoCrmContacts::getContactsSearch($keyword, substr($projects, 0, -1), substr($fields, 0, -1));
		
		$filename = 'search_'.str_replace(' ', '-', $keyword).'_contacts.csv';
		
		$isSearch = true;
	}

	else {
		
		MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to export contacts with wrong parameters");
		die('Forbidden Access');
		
	}
		

# We have Contacts
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

$output = fopen('php://output', 'w');

# Get allowed fields to show
$allowedFields = meo_crm_contacts_getAllowedFields($precision);


# Building Headers
$headers = array();

if($isSearch){
	$headers[] = 'Project';	// Remove the isSearch condition everywhere if you want to allways priint the project
}
foreach($allowedFields as $field => $fieldName){
	$headers[] = $fieldName;
}

fputcsv($output, $headers);


foreach ($projectContacts as $contact) {
	
	# Building Data
	$contactData = array();
	
	if($isSearch){
		$project = meo_crm_projects_getProjectInfo($contact['project_id'], false);
		
		$contactData[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $project['name']);
	}
	
	foreach($allowedFields as $field){
		$value = array_key_exists($field, $contact) ? $contact[$field] : '';
		$contactData[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $value);
	}
	
	# Write Data Line into CSV file
	fputcsv($output, $contactData);

}

// Read what we have written.
@rewind($output);

echo stream_get_contents($output);
?>