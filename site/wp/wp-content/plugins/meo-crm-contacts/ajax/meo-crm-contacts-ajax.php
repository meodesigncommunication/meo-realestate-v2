<?php 

# Add Ajax calls and functions


	# Check email availability
	
	add_action('wp_ajax_check_email_availability', 'check_email_availability');
	add_action('wp_ajax_nopriv_check_email_availability', 'check_email_availability');
	
		function check_email_availability() {
			
			if(isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['project_id']) && $_POST['project_id'] != ''){
				# Verify email syntax
				if(is_email( $_POST['email'] )){
					# Verify if email already exists
					$potentialContact = MeoCrmContacts::getContactsByEmailAndProjectID($_POST['email'], $_POST['project_id']);
					if(!$potentialContact){
						echo 'true';
						
						wp_die();
					}
					else if(is_array($potentialContact)){
						echo $potentialContact['id'];
						
						wp_die();
					}
				}
			}
			echo 'false';
			
			wp_die();
		}
		
		
	# Delete Contact
		
		add_action('wp_ajax_delete_contact', 'delete_contact');
		add_action('wp_ajax_nopriv_delete_contact', 'delete_contact');
		
		function delete_contact() {
			
			if(isset($_POST['id']) && $_POST['id'] != ''){
				
				// Placed here as to get contact info in certain plugins (ie. PDFfile in analytics)
				do_action('meo_crm_contacts_delete_contact_by_id', $_POST['id']);
				
				# Delete the Contact
				if(MeoCrmContacts::deleteContactById($_POST['id'])){
					
					// do_action('meo_crm_contacts_delete_contact_by_id', $_POST['id']);
					
					echo 'true';
					wp_die();
				}
				
			}
			echo 'false';
				
			wp_die();
		}
		
		
	# Update Contact Field
	
	add_action( 'wp_ajax_update_contact_field', 'update_contact_field_callback' );
	add_action( 'wp_ajax_nopriv_update_contact_field', 'update_contact_field_callback' );
	
		function update_contact_field_callback() {
			
			global $wpdb, $meocrmcontacts;
			
			# Get Editable fields list to see if it's a valid field
			$precision 	= $_POST['precision'];
			$editableFields = meo_crm_contacts_getEditableFields($precision);
			
			$contactID 	= intval( $_POST['contactID'] );
			$fieldName 	= $_POST['fieldName'];
			$fieldValue = $_POST['fieldValue'];
			
			if($fieldValue == 'NULL') {
				$fieldValue = NULL;
			}
			
			# Field is valid/authorized
			if(in_array($fieldName, $editableFields)){
				$data = array($fieldName => $fieldValue);
				$where = array('id' => $contactID);
				
				if(MeoCrmContacts::updateContact($data, $where) === false){
					echo 'false';
				}
				else echo 'true';
			}
			# Field is not valid or authorized
			else MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to edit a non valid/unauthorized field " . print_r($_POST, true));
			
			wp_die();
		}

		
	# Get Contact coordinates for Gmaps
	
	add_action( 'wp_ajax_get_contacts_coordinates', 'get_contacts_coordinates' );
	add_action( 'wp_ajax_nopriv_get_contacts_coordinates', 'get_contacts_coordinates' );
	
		function get_contacts_coordinates()
		{
			global $wpdb;
			
			$start = (isset($_POST['start'])) ? $_POST['start'] : false;
			$limit = (isset($_POST['limit'])) ? $_POST['limit'] : false;
			
			$project_id = $_POST['project_id'];
			$where = 'WHERE project_id='.$project_id.'';
			
			if($project_id == '0') {
				$where = '';
			}
			
			$contacts = MeoCrmContacts::getContacts($where, $start, $limit);
			
			if($contacts){
					
				$returnedContacts = array();
					
				foreach($contacts as $contact){
					$contact = apply_filters('meocrmcontacts_getContactData', $contact);
					if($contact){
						$returnedContacts[] = $contact;
					}
				}
					
				echo json_encode($returnedContacts);
			}
			
			wp_die();
		}
	
		
	# Get Contact Coordinates
	
	add_action( 'wp_ajax_get_contact_coordinates', 'get_contact_coordinates' );
	add_action( 'wp_ajax_nopriv_get_contact_coordinates', 'get_contact_coordinates' );
		
		function get_contact_coordinates(){
			
			global $wpdb;
			
			$contact_id = $_POST['contact_id'];
			$where = 'WHERE id='.$contact_id.'';
							
			$contacts = MeoCrmContacts::getContacts($where);
			
			if($contacts){
					
				$returnedContacts = array();
					
				foreach($contacts as $contact){
					$contact = apply_filters('meocrmcontacts_getContactData', $contact);
					if($contact){
						$returnedContacts[] = $contact;
					}
				}
					
				echo json_encode($returnedContacts);
			}
			wp_die();
		}
	
		
	# Core Locator Countries List
	
	add_action('wp_ajax_meo_crm_contacts_countrieslist', 'get_contact_countrieslist');
	add_action('wp_ajax_nopriv_meo_crm_contacts_countrieslist', 'get_contact_countrieslist');
	
		function get_contact_countrieslist(){
			
			global $wpdb;
		
			$country = $_POST['country'];
		
			echo do_shortcode('[meo_crm_core_countrieslist selectedcountry="'.$country.'" class="fieldToSave" name="fieldToSave"]');
		
			wp_die();
		}
	
		
	# Core Locator Languages List
		
		add_action('wp_ajax_meo_crm_contacts_languageslist', 'get_contact_languageslist');
		add_action('wp_ajax_nopriv_meo_crm_contacts_languageslist', 'get_contact_languageslist');
		
		function get_contact_languageslist(){
			
			global $wpdb;
		
			$language = $_POST['language'];
		
			echo MeoCrmCoreLocator::generateLanguagesListHTMLDropDown($language, 'language', 'fieldToSave');
		
			wp_die();
		}
		
		
	# Core Locator get Google Map Coordinates
	
	add_action('wp_ajax_meo_crm_contact_setcoordinates', 'set_contact_coordinates');
	add_action('wp_ajax_nopriv_meo_crm_contact_setcoordinates', 'set_contact_coordinates');
		
		function set_contact_coordinates(){
			
			$address 	= $_POST['address'];
			$city 		= $_POST['city'];
			$postcode 	= $_POST['postcode'];
			$country 	= $_POST['country'];
			$contact_id = $_POST['contact_id'];
				
			if($address == '' || $city == ''|| $postcode == '' || $country == '' || $contact_id == '') wp_die();
				
			# Updating existing states
			if($address != '' && $city != '' && $postcode != '' && $country != '' && $contact_id != ''){
				$dataAddress = array(
										'address' 	=> $address,
										'city' 		=> $city,
										'postcode' 	=> $postcode,
										'country' 	=> $country
								);
				
				# Request Google Maps API to get latitude and longitude
				$dataContact = MeoCrmCoreLocator::getGoogleCoordinates($dataAddress);
				
				if(array_key_exists('latitude', $dataContact) && array_key_exists('longitude', $dataContact)){
					$data = array(
										'latitude' => $dataContact['latitude'],
										'longitude' => $dataContact['longitude']
					 		);
					$where = array('id' => $contact_id);
					
					MeoCrmContacts::updateContact ($data, $where);
					
					echo json_encode($data);
				}
			}
			
			wp_die();
		}
		
		function formatPeriod($endtime, $starttime)
		{
		
			$duration = $endtime - $starttime;
		
			$hours = (int) ($duration / 60 / 60);
		
			$minutes = (int) ($duration / 60) - $hours * 60;
		
			$seconds = (int) $duration - $hours * 60 * 60 - $minutes * 60;
		
			return ($hours == 0 ? "00":$hours) . ":" . ($minutes == 0 ? "00":($minutes < 10? "0".$minutes:$minutes)) . ":" . ($seconds == 0 ? "00":($seconds < 10? "0".$seconds:$seconds));
		}
		
		
	# Contacts list
	
	add_action('wp_ajax_meo_crm_contacts_contactsListByProjectID', 'get_contact_contactsListByProjectID');
	add_action('wp_ajax_nopriv_meo_crm_contacts_contactsListByProjectID', 'get_contact_contactsListByProjectID');
	
	function get_contact_contactsListByProjectID()
	{
		global $wpdb, $contactsTypes;
		
		$project_id = $_POST['project_id'];
		$precision 	= $_POST['precision'];
		$start 		= (isset($_POST['start'])) ? $_POST['start'] : false;
		$limit	 	= (isset($_POST['limit'])) ? $_POST['limit'] : false;
	
		$projectContacts 	= meo_crm_contacts_getContactsByProjectID($project_id, $start, $limit);
		$allowedFields 		= meo_crm_contacts_getAllowedFields($precision);
		$editableFields 	= meo_crm_contacts_getEditableFields($precision);
		
		if($projectContacts && is_array($projectContacts) && count($projectContacts) > 0){
			
			foreach($projectContacts as $contact) {
				
				if($contact){
					
					$deletedUserClass = '';
					if(!is_null($contact['date_deleted'])){
						$deletedUserClass = ' warning-text';
					}
					?>
					<tr id="<?php echo $contact['id']; ?>" class="project-<?php echo $contact['project_id']; ?> tr_contact<?php echo $deletedUserClass; ?>" data-project="<?php echo $contact['project_id']; ?>" data-user="<?php if(array_key_exists('courtier_id', $contact)) echo $contact['courtier_id']; ?>">
						<?php 
							// To see the contact on the map, only if there is a latitude and longitude
							$contactFullName = $contact['last_name'].' '.$contact['first_name'];
							$lat = $contact['latitude'];
							$lon = $contact['longitude'];
							
							foreach($allowedFields as $kField => $field_name){
								$extraClass = '';
								$editable = '';
								$onclick = '';
								if(in_array($field_name, $editableFields)) {
									$editable = ' editable';
									$onclick = '  ondblclick="clickToEditContact(this);"';
								}
								
								$field_value = array_key_exists($field_name, $contact) ? $contact[$field_name] : '';
									
								if($field_name == 'project_id') {
									$projectInfo = meo_crm_projects_getProjectInfo($field_value);
									$field_value = $projectInfo['name'];
									$editable = '';
								}
								else if($field_name == 'client_id') {
									// User Info
									$currentUser = get_user_by('id', $record['user_id']);
									$field_value = $currentUser->data->display_name;
									$editable = '';
								}
								else if($field_name == 'date_added') {
									$field_value = substr($field_value, 0, -9);
								}
								else if($field_name == 'type' && $field_value != ''){
									$field_value = '<i class="fa '.$contactsTypes[$field_value].'" aria-hidden="true" title="'.$field_value.'"></i>';
								}
								else if($field_name == 'last_interaction' && $field_value != ''){
									$extraClass = ' center';
								}
								
									
								echo '<td class="'.$field_name.$editable.$extraClass.'"'.$onclick.'>'.$field_value.'</td>';
							}
							
							echo '<td class="see_map center">';
							
							if($lat > 0 && $lon > 0) {
								echo '<i class="fa fa-globe cursor-hand" aria-hidden="true" title="See on the map" onclick="meo_crm_contacts_setMap('.$lat.', '.$lon.', \''.$contactFullName.'\');moveToTop(jQuery(this).parents(\'tr\'));"></i>';
							}
							
							echo '</td>';
							
							// if((is_admin() || $precision == 'ADMIN_LIST') && $precision != 'FRONT_LIST'){
							if(current_user_can('administrator')){
								echo '<td class="calc_coordinates center">
											<a href="#" class="" title="Recalculate position/coordinates" onclick="updateContactsCoordinates(this);moveToTop(jQuery(this).parents(\'tr\'));"><i class="fa fa-compass" aria-hidden="true"></i></a>
										</td>';
							}
						?>
						<td><a class="edit-contact" href="<?php echo home_url('/').MEO_CONTACT_SLUG; ?>/?id=<?php echo $contact['id']; ?>" target="_blank"><i class="fa fa-pencil"></i></a></td>
						<td class="delete-activate-contact">
							<?php 
							if(current_user_can('administrator')){
								if(is_null($contact['date_deleted'])){
								?>
									<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em>
								<?php 
								}
								else {
								?>
									<em class=""><i class="fa fa-refresh cursor-hand" aria-hidden="true" onclick="updateActivateContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em>
								<?php 
								}
							}
							else if(current_user_can(CLIENT_ROLE) && is_null($contact['date_deleted'])){
								?>
									<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em>
								<?php 
							}
							?>
						</td>
						<?php 
						if(current_user_can('administrator')){
						?>
						<td><em class="warning-text"><i class="fa fa-trash-o cursor-hand" aria-hidden="true" onclick="deleteContact(<?php echo $contact['id'].', \'element\''; ?>)"></i></em></td>
						<?php 
						}
						?>
					</tr>
				<?php }
				} 
		}
		
		wp_die();
	}
	
	add_action('wp_ajax_meo_crm_contacts_getContactsSearch', 'meo_crm_contacts_getContactsSearch');
	add_action( 'wp_ajax_nopriv_meo_crm_contacts_getContactsSearch', 'meo_crm_contacts_getContactsSearch');

	# getContactsSearch
	
	function meo_crm_contacts_getContactsSearch(){
	
		global $wpdb, $contactsTypes, $contactsSearchFields;
		
		$returnedContacts = array(); 	// Will contain resulting contacts
		$html = '';						// Will contain built html content
		
		$keyword = $_POST['keyword'];
		$projects = $_POST['projects'];
		$fields = $_POST['fields'];
		
		$precision = 'FRONT_LIST';
		
		$contacts = MeoCrmContacts::getContactsSearch($keyword, substr($projects, 0, -1), substr($fields, 0, -1));
		
		if($contacts){
			
			$allowedFields 		= meo_crm_contacts_getAllowedFields($precision);
			$editableFields 	= meo_crm_contacts_getEditableFields($precision);
				
			foreach($contacts as $contact){
				
				$contact = apply_filters('meocrmcontacts_getContactData', $contact);
				
				if($contact){
					
					$returnedContacts[] = $contact;
					
					$deletedUserClass = '';
					if(!is_null($contact['date_deleted'])){
						$deletedUserClass = ' warning-text';
					}
					
					$html.= '<tr id="'.$contact['id'].'" class="project-'.$contact['project_id'].' tr_contact'.$deletedUserClass.'" data-project="'.$contact['project_id'].'" data-user="';
								if(array_key_exists('courtier_id', $contact)) $html.= $contact['courtier_id'];
					$html.= '">';
						
						$project = meo_crm_projects_getProjectInfo($contact['project_id'], false);
						
						$html.= '<td>'.$project['name'].'</td>';
						
						// To see the contact on the map, only if there is a latitude and longitude
						$contactFullName = $contact['last_name'].' '.$contact['first_name'];
						$lat = $contact['latitude'];
						$lon = $contact['longitude'];
						
						foreach($allowedFields as $kField => $field_name){
							$editable = '';
							$extraClass = '';
							$onclick = '';
							if(in_array($field_name, $editableFields)) {
								$editable = ' editable';
								$onclick = '  ondblclick="clickToEditContact(this);"';
							}
							
							$field_value = array_key_exists($field_name, $contact) ? $contact[$field_name] : '';
								
							if($field_name == 'project_id') {
								$projectInfo = meo_crm_projects_getProjectInfo($field_value);
								$field_value = $projectInfo['name'];
								$editable = '';
							}
							else if($field_name == 'client_id') {
								// User Info
								$currentUser = get_user_by('id', $record['user_id']);
								$field_value = $currentUser->data->display_name;
								$editable = '';
							}
							else if($field_name == 'date_added') {
								$field_value = substr($field_value, 0, -9);
							}
							else if($field_name == 'type' && $field_value != ''){
								$field_value = '<i class="fa '.$contactsTypes[$field_value].'" aria-hidden="true" title="'.$field_value.'"></i>';
							}
							else if($field_name == 'last_interaction' && $field_value != ''){
								$extraClass = ' center';
							}
								
							$html.= '<td class="'.$field_name.$editable.$extraClass.'"'.$onclick.'>'.$field_value.'</td>';
						}
						
						$html.= '<td class="see_map center">';
						
						if($lat > 0 && $lon > 0) {
							$html.= '<i class="fa fa-globe cursor-hand" aria-hidden="true" title="See on the map" onclick="meo_crm_contacts_setMap('.$lat.', '.$lon.', \''.$contactFullName.'\');moveToTop(jQuery(this).parents(\'tr\'));"></i>';
						}
						
						$html.= '</td>';
						
						// if((is_admin() || $precision == 'ADMIN_LIST') && $precision != 'FRONT_LIST'){
						if(current_user_can('administrator')){
							$html.= '<td class="calc_coordinates center">
										<a href="#" class="" title="Recalculate position/coordinates" onclick="updateContactsCoordinates(this);moveToTop(jQuery(this).parents(\'tr\'));"><i class="fa fa-compass" aria-hidden="true"></i></a>
									</td>';
						}
					
					$html.= '<td><a class="edit-contact" href="'.home_url('/').MEO_CONTACT_SLUG.'/?id='.$contact['id'].'" target="_blank"><i class="fa fa-pencil"></i></a></td>
								<td class="delete-activate-contact">';
						
						if(current_user_can('administrator')){
							if(is_null($contact['date_deleted'])){
								$html.= '<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact('.$contact['id'].', \'element\''.')"></i></em>';
							}
							else {
								$html.= '<em class=""><i class="fa fa-refresh cursor-hand" aria-hidden="true" onclick="updateActivateContact('.$contact['id'].', \'element\''.')"></i></em>';
							}
						}
						else if(current_user_can(CLIENT_ROLE) && is_null($contact['date_deleted'])){
							$html.= '<em class="warning-text"><i class="fa fa-user-times cursor-hand" aria-hidden="true" onclick="updateDeleteContact('.$contact['id'].', \'element\''.')"></i></em>';
						}
						
					$html.= '</td>';
					
					if(current_user_can('administrator')){
						$html.= '<td><em class="warning-text"><i class="fa fa-trash-o cursor-hand" aria-hidden="true" onclick="deleteContact('.$contact['id'].', \'element\''.')"></i></em></td>';
					}
					
					$html.= '</tr>';
									
				}
			}
		}
		
		echo json_encode( array( 'html' => $html, 'contacts' => $returnedContacts ) );
		
		wp_die();
	}
?>