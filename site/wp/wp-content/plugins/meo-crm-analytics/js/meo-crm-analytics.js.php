<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));

</script>
<?php if(isset($googleAnalyticsMasterArray) && is_array($googleAnalyticsMasterArray)){ ?>
<script>
gapi.analytics.ready(function() {

  // Step 3: Authorize the user.

  var CLIENT_ID = '<?php echo GOOGLE_CORE_CLIENT_ID; ?>';

  gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
  });

  // Step 4: Create the view selector.

  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector'
  });

  // Step 5: Create the charts.
	<?php meo_crm_analytics_drawChart($googleAnalyticsMasterArray); ?>
	
  // Step 6: Hook up the components to work together.

  gapi.analytics.auth.on('success', function(response) {
    viewSelector.execute();
  });

  viewSelector.on('change', function(ids) {
    var newIds = {
      query: {
        ids: ids
      }
    }
    
    // Keep current id in a hidden field, for dates change
    jQuery('#property-id').val(ids);
    
    refreshStats(newIds);
  });

  jQuery('#submit').click(function(){

	  var ids = jQuery('#property-id').val();
	  var startDate = jQuery('#start-date').val();
	  var endDate = jQuery('#end-date').val();
	  
	  // TODO: test is ids
		 var newIds = {
			      query: {
			        ids: ids,
			        'start-date': startDate,
			        'end-date': endDate,
			      }
			    }
		 refreshStats(newIds);
  });


	function refreshStats(newIds){
	    <?php 
	    foreach($googleAnalyticsMasterArray as $chart => $chartOptions) {
	    	echo $chart.".set(newIds).execute();";
	    }
	    ?>
	}
});
</script>
<?php } ?>
<script>
function piwikAnalytics_updateInfo(){
	var siteId = jQuery("select#siteId option:selected").val();
	var startDate = jQuery('#start-date').val();
	var endDate = jQuery('#end-date').val();
	var daterange = startDate+','+endDate;
	
	if(siteId != '0'){
		// Ajax code
		var data = {
			'action': 'update_piwik_analytics',
			'siteID': siteId,
			'dateRange': daterange
		};
		jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function(response) {
			// alert('Got this from the server: ' + response);
			jQuery("#piwik-charts-main").html(response);
		});

		// Update iframe url for dashboard
		var piwikUrl = jQuery('.meo_crm_analytics_piwikvisitor').attr('data-piwikurl');
		var piwikIframeUrl = 'http://'+piwikUrl+'/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite='+siteId+'&period=week&date=today';

		jQuery('.meo_crm_analytics_piwikvisitor a').attr('href', piwikIframeUrl);
		jQuery('.meo_crm_analytics_piwikvisitor a').show();
	}
	else {
		jQuery("#piwik-charts-main").html('Incorrect Site ID');
		jQuery('.meo_crm_analytics_piwikvisitor a').hide();
	}
	
}

function get_analytics_comparator(action, index){
	
	var startDate = jQuery('#start-date-'+index).val();
	var endDate = jQuery('#end-date-'+index).val();
	var project_id = jQuery("select#siteId-"+index+" option:selected").val();

	jQuery("#stats-container-"+index).html('<table><tr><td style="text-align:center;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></td></tr></table>');
	
	// Ajax code
	var data = {
		'action': action,
		'startDate': startDate,
		'endDate': endDate,
		'project_id': project_id
	};
	jQuery.post('<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function(response) {
		
		jQuery("#stats-container-"+index).html(response);
	});
}

function togglePercentages(element){
	jQuery(element).parents('table').find('.percentages').toggle();
}

jQuery( document ).ready(function() {

	if(jQuery('#start-date').length && jQuery('#end-date').length){
		jQuery('#start-date').datepicker({dateFormat : 'yy-mm-dd'});
		jQuery('#end-date').datepicker({dateFormat : 'yy-mm-dd'});
	}

	if(jQuery('.datepicker').length){
		jQuery('.datepicker').datepicker({dateFormat : 'yy-mm-dd'});
	}
	
	jQuery('#submitpiwik').click(function(){ piwikAnalytics_updateInfo(); });
	jQuery('.status-toggler').click(function(){ jQuery('.percentages').toggle(); });
	jQuery('#submit_proconsrc-1').click(function(){ get_analytics_comparator('get_analytics_proconsrc', '1'); });
	jQuery('#submit_proconsrc-2').click(function(){ get_analytics_comparator('get_analytics_proconsrc', '2'); });
	jQuery('#submit_proconsta-1').click(function(){ get_analytics_comparator('get_analytics_proconsta', '1'); });
	jQuery('#submit_proconsta-2').click(function(){ get_analytics_comparator('get_analytics_proconsta', '2'); });
	
});
	
</script>