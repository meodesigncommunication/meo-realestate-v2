<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoCrmAnalytics {
	
	
	/* Smart Capture
	
	$table_name = $wpdb->prefix . MEO_ANALYTICS_SCHEDULE_TABLE;
	
	// Create Email Schedule table
	$sql = "CREATE TABLE " . $table_name . " (
	id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	analytics_id varchar(100) NOT NULL,
	site_id bigint(20) unsigned NOT NULL,
	creation_date datetime NOT NULL,
	update_date datetime NOT NULL,
	sent_date datetime,
	in_error enum('yes','no') default 'no',
	PRIMARY KEY (id),
	UNIQUE KEY analytics_and_site (analytics_id, site_id)
	) $charset_collate;";
	
	dbDelta( $sql ); */
	
	# Analytics Attributes
	
		private $project_id;
		private $google_id;
		private $piwik_id;
		private $piwik_token;
		private $api_key;
		private $api_uri;
		private $email;
		private $email_content;
	
		public function __construct() {
			
		}
		
	# Corresponding Database Table
	
		public static function activate() {
			
		}
		
		public static function deactivate() {
			// Do nothing
		}
	
	# Manage Analytics in the Database
		
		public static function addAnalytics ($data){
			// TODO :: $args containing fields
			//         Custom query
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_ANALYTICS_TABLE;
			
			$wpdb->insert( $table, $data );
		}
		
		public static function updateAnalytics ($data, $where){
			// TODO :: $args containing fields and values (associative array)
			//         Custom query
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_ANALYTICS_TABLE;
				
			return $wpdb->update( $table, $data, $where );
		}
		
		public static function deleteAnalyticsByProjectId ($projectID){
			// TODO :: Check if IDs exist?
			//         Custom query
		}
		
	# Get Records
		
		#getAnalytics
		
		public static function getAnalytics ($where = '1=1'){
				
			global $wpdb;
				
			// Get Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ANALYTICS_TABLE . " WHERE ".$where." ORDER BY project_id ASC";
				
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) return $records;
			
			return false;
		}
		
		#getAnalyticsByProjectID
		
		public static function getAnalyticsByProjectID ($ProjectID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ANALYTICS_TABLE . " WHERE project_id=".$ProjectID;
			
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return $records;
		
			return false;
		}
		
		#getRecordsByApiKey
		
		public static function getRecordsByApiKey ($ApiKey){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ANALYTICS_TABLE . " WHERE api_key='".$ApiKey."'";
				
			$records = $wpdb->get_results($query, ARRAY_A);
			
			if($records) return $records;
		
			return false;
		}
	
	##################
	# Schedule Table #
	##################
	
		public static function getScheduledEmails (){

			global $wpdb;
			
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ANALYTICS_SCHEDULE_TABLE . " 
						WHERE update_date <= date_add(now(), interval -30 minute)
						AND sent_date is null
						AND in_error = 'no'";
			
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) return $records;
			
			return false;
		}
		
		public static function updateScheduledEmails ($data, $where){
			
			global $wpdb;
		
			$table = $wpdb->prefix . MEO_ANALYTICS_SCHEDULE_TABLE;
		
			$wpdb->update( $table, $data, $where );
		}
}
