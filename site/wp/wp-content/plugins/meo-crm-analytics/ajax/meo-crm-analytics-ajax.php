<?php 

########
# AJAX #
########


	# Update Piwik Analytics when changing project and/or dates
	
	add_action( 'wp_ajax_update_piwik_analytics', 'update_piwik_analytics_callback' );
	add_action( 'wp_ajax_nopriv_update_piwik_analytics', 'update_piwik_analytics_callback' );
	
		function update_piwik_analytics_callback() {
			global $wpdb;
		
			$siteID = intval( $_POST['siteID'] );
			$dateRange = $_POST['dateRange'];
		
			$piwikAnalyticsMasterArray = meo_crm_analytics_getPiwikChartsConfig($siteID, 'range', $dateRange);
		
			$html = '';
		
			# Build chart containers
			foreach($piwikAnalyticsMasterArray as $chart => $chartOptions) {
		
				$html.= "<div class='piwik-chart-container'>
						<h3>".$chartOptions['info']['title']."</h3>
						<section id='".$chart."' class='google-chart'>
						".meo_crm_analytics_piwik_widget($chartOptions['moduleToWidgetize'], $chartOptions['actionToWidgetize'], $chartOptions['idSite'], $chartOptions['period'], $chartOptions['date'], $chartOptions['viewDataTable'])."
						</section>
					 </div>";
		
			}
		
			echo $html;
		
			wp_die();
		}
		
	# Update Analytics Project Field
	
	add_action( 'wp_ajax_meo_crm_analytics_saveProjectField', 'meo_crm_analytics_saveProjectField');
	add_action( 'wp_ajax_nopriv_meo_crm_analytics_saveProjectField', 'meo_crm_analytics_saveProjectField');
	
		function meo_crm_analytics_saveProjectField(){
			
			global $analyticsProjectFields;
			
			$projectID = $_POST['project_id'];
			$field = $_POST['field'];
			$value = $_POST['value'];
			$validField = true;
			
			if($projectID > 0 && array_key_exists($field, $analyticsProjectFields) && $value != ''){
				
				# Check email syntax
				if($field == 'email'){
					
					# Check if comma separated, even if not, use array
					if(strpos($value, ',') !== false){
						
						$emailAddresses = explode(',', $value);
						
						foreach($emailAddresses as $email){
							$validField = is_email($email);
							
							if(!$validField) break;
						}
					}
					else $validField = is_email($value);
				}
				
				# We expect at this time that the analytics project data exists
				if($validField && MeoCrmAnalytics::updateAnalytics(array($field => $value), array('project_id' => $projectID))){
					echo '<i class="fa fa-check" aria-hidden="true"></i> Saved';
				}
				else echo '<span class="warning-text"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Saving Error</span>';
			}
			else echo '<span class="warning-text"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Error in data</span>';
			
			wp_die();
		}


#################
# SMART CAPTURE #
#################


	# Get Scheduled emails/contacts, Called from Immo site thanks to ajax / curl
	
	add_action( 'wp_ajax_nopriv_schedule_pdf_email', 'meo_crm_analytics_schedulePdfEmail' );
	add_action( 'wp_ajax_schedule_pdf_email', 'meo_crm_analytics_schedulePdfEmail' );
	
		function meo_crm_analytics_schedulePdfEmail() {
		
			global $wpdb;
		
			header('Content-Type: application/json');
		
			if (!array_key_exists('analytics_id', $_GET) or empty($_GET['analytics_id']) or
					!array_key_exists('api_key', $_GET) or empty($_GET['api_key'])) {
						echo json_encode(array(	'success' => 0,
								'result' => __('Required parameter missing', 'meo_real_estate_admin')
						));
						MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Required parameter missing " . print_r($_GET, true));
						exit;
			}
	
			// Check if we have a record, meaning valid api key
			$record = MeoCrmAnalytics::getRecordsByApiKey($_GET['api_key']);
	
			if (!$record) {
				echo json_encode(array(	'success' => 0,
						'result' => __('Invalid API key', 'meo_real_estate_admin')
				));
				MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Invalid API key " . print_r($_GET, true));
				exit;
			}
			
			$resetRecord = reset($record);
	
			$result = $wpdb->query( $wpdb->prepare( "
				INSERT INTO ".$wpdb->prefix . MEO_ANALYTICS_SCHEDULE_TABLE." (analytics_id, project_id, creation_date, update_date)
				VALUES (%s, %d, now(), now())
				ON DUPLICATE KEY UPDATE update_date = now()",
					$_GET['analytics_id'],
					$resetRecord['project_id']
			) );
	
			if (!empty($wpdb->last_error)) {
				echo json_encode(array(	'success' => 0,
						'message' => __('Insert failed: ', 'meo_real_estate_admin') . $wpdb->last_error
				));
				MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Insert failed: " . print_r($_GET, true));
				exit;
			}
	
			echo json_encode(array(	'success' => 1,
					'message' => __('PDF email scheduled', 'meo_real_estate_admin')
			));
			exit;
		}


##############
# STATISTICS #
##############
	
	
	# Project / Contact / Source
	
	add_action('wp_ajax_get_analytics_proconsrc', 'get_analytics_proconsrc');
	add_action('wp_ajax_nopriv_get_analytics_proconsrc', 'get_analytics_proconsrc');
	
		function get_analytics_proconsrc(){
			
			$date1 = $_POST['startDate'];
			$date2 = $_POST['endDate'];
			$postedProjectID = $_POST['project_id'];
			$forceProjectID = false;
			
			if($postedProjectID != '' && $postedProjectID != '0'){
				$forceProjectID = true;
			}
			
			if($date1 != '' && $date2 != ''){
				echo '<div class="redband">Statistics for Contacts recorded between '.$date1.' and '.$date2.'</div>';
			}
			
			# Get current user (client)
			
			$clients = meo_crm_users_getClients();
			
			# Will be use to change project background color
			$indexColor = 0;
			
			if($clients && is_array($clients)){
				
				echo '<table id="table-client">';
								
				foreach($clients as $c => $client){
					
					# CLIENT row
					echo '<tr>
							<td>';
					
					# Get List of his projects
					$clientProjects = meo_crm_projects_getProjectsByUserID($client['ID']);
					
					if($clientProjects && is_array($clientProjects)){
						
						echo '<table id="table-project">';
						
						foreach($clientProjects as $p => $project){
							
							if($project && !$forceProjectID || ($forceProjectID && $project['id'] == $postedProjectID)){
								# GET Project's Contacts
								$nbOfContactsTotalProj = meo_crm_contacts_countContactsByProjectID($project['id'], $date1, $date2);
								
								$projectHasStatus = false;
								
								# Get list of Status for the current project
								if(array_key_exists('rel_status', $project) && is_array($project['rel_status']) && count($project['rel_status']) > 1){
								
									$projectHasStatus = true;


									# Add EXIT state at the end
									
									$project['rel_status'][] = array (
											'id' => MEO_RELATIONSHIP_STATE_EXIT_ID,
											'project_id' => 28,
											'label' => 'EXIT',
											'status_order' => MEO_RELATIONSHIP_STATE_EXIT_ID,
											'color' =>  MEO_RELATIONSHIP_STATE_EXIT_COLOR);
									
									
									$nbStatus = count($project['rel_status']) + 1; 	// Number of statuses + record one, to evaluate td with
									$tdWidth = round(99/$nbStatus, 4); 				// 99 instead of 100 to not reach a too high percentage of width
										
									# Build table headers
									$projectStatusHeaders = '<tr>
																<th style="width:'.$tdWidth.'%">Record</th>';
									foreach($project['rel_status'] as $ps => $projectStatus){
										$projectStatusHeaders.= '<th style="width:'.$tdWidth.'%">'.$projectStatus['label'].'</th>';
									}
									$projectStatusHeaders.= '</tr>';
				
									$nbContactsPerRefererPerStatus = MeoCrmContactsUsersRelationship::countContactsPerRefererPerStatusbyProjectID($project['id'], $date1, $date2);
								}
								
								$indexColor++;
								
								$percentBtn = '';
								if($nbOfContactsTotalProj > 0){
									$percentBtn = '<br/><div id="stats-referer-status-filter"><i class="fa fa-percent status-toggler cursor-hand" aria-hidden="true" onclick="togglePercentages(this);"></i></div>';
								}
								
								# Piwik Visits
								$Visits = MeoCrmAnalyticsPiwik::getNbVisits($project, $date1.','.$date2, 'range');
								$nbVisits = '';
								$nbVisitsText = '';
								if($Visits && is_array($Visits)){
									$nbVisits = $Visits['nb_visits'];
									$nbVisitsText = '<br/><span class="nbvisits">'. $nbVisits . ' visits</span>';
								}
								
								# PROJECT row
								echo '<tr class="project-color-'.($indexColor%2 == 0 ? '1' : '0').'">
										<th>'.$client['display_name'].'<br/>'.$project['name'].'<br/><em>'.$nbOfContactsTotalProj.' contacts</em>'.$nbVisitsText.$percentBtn.'</th>
										<td>';
								
								# countContactPerDistinctRefererByProjectID
								$referers = meo_crm_contacts_countContactPerDistinctRefererByProjectID($project['id'], $date1, $date2);
								
								if($referers){
									
									echo '<table id="table-referer">';
									
									if($projectHasStatus){
											
										echo '<tr>
												<th class="th-referer">Referer</th>
												<td class="stat-referer-td">Contacts</td>
												<td><table id="table-status-referer-headers">'.$projectStatusHeaders.'</table></td>
											</tr>';
									
									} // /if projecthasstatus
									else {
										echo '<tr>
												<th class="th-referer">Referer</th>
												<td class="stat-referer-td">Contacts</td>
											</tr>';
									} // /else projecthasstatus
									
									foreach($referers as $uc => $referer){
										
										$nbOfContactsRef = $referer['nbcontacts'];
										
										$percentageRefText = '';
										if($nbOfContactsTotalProj > 0){
											$percentageRef = round(($nbOfContactsRef*100) / $nbOfContactsTotalProj);
											$percentageRefText = '<br/><span class="stat-perc-src percentages">'.$percentageRef.'%</span>';
										}
											
										$styleText = '';
										if($percentageRef > 90){
											$styleText = ' style="position: absolute; right: 10px; top: 17px;"';
										}
										
										# Piwik Visits Per Referer
										$refererVisits = MeoCrmAnalyticsPiwik::getNbVisitsPerReferer($project, '2010-01-01,today', 'range');
										$nbVisitsRefText = '';
										$percentageRefVis = '';
										if($refererVisits && is_array($refererVisits) && array_key_exists($referer['referer'], $refererVisits)){
											$nbVisitsRef = $refererVisits[$referer['referer']]['nb_visits']*1;
											$nbVisitsRefText = '<br/><span class="nbvisitsperreferer">'. $nbVisitsRef . ' visits</span>';
											$percentageRefVis = "<span class='nbvisitsperreferer percentages'> - ".round(($nbVisitsRef*100) / $nbVisits)."%</span>"; // Visits
										}
											
											# REFERER row
											echo '<tr>
													<th>'.$referer['referer'].$nbVisitsRefText.''.$percentageRefVis.'</th>
													<td class="stat-referer-td"><span class="stat-referer" style="background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.'; width:'.$percentageRef.'%;"></span><span class="stat-text-src"'.$styleText.'>'.$nbOfContactsRef.'<br/><span class="stat-perc-src percentages">'.$percentageRef.'%</span></span></td>';
													
											if($projectHasStatus){
												
												echo '<td><table id="table-status-referer"><tr>';
												
												$nbContacts = 0;
												$percent = 0;
												if(array_key_exists($referer['referer'], $nbContactsPerRefererPerStatus) && array_key_exists(0,$nbContactsPerRefererPerStatus[$referer['referer']] )){
													$nbContacts = $nbContactsPerRefererPerStatus[$referer['referer']][0]['nbcontacts'];
													$percent = round(($nbContacts * 100)/$nbOfContactsRef); // $nbOfContactsRef;$nbOfContactsTotalProj
												}
												echo '<td class="nbcontacts-src-status" style="width:'.$tdWidth.'%"><span class="stat-text-ref-status">'.$nbContacts.'</span><span class="stat-bar-ref-status" style="width:'.$percent.'%;background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.';"></span><span class="stat-perc-ref-status percentages">'.$percent.'%</span></td>';
												
												foreach($project['rel_status'] as $ps => $projectStatus){
													// Current Status
													$indexStatus = $projectStatus['id'];
													$nbContacts = 0;
													$percent = 0;
													if(array_key_exists($referer['referer'],$nbContactsPerRefererPerStatus) && array_key_exists($indexStatus,$nbContactsPerRefererPerStatus[$referer['referer']] )){
														$nbContacts = $nbContactsPerRefererPerStatus[$referer['referer']][$indexStatus]['nbcontacts'];
														$percent = round(($nbContacts * 100)/$nbOfContactsRef); // $nbOfContactsRef;$nbOfContactsTotalProj
													}
													echo '<td class="nbcontacts-src-status" style="width:'.$tdWidth.'%"><span class="stat-text-ref-status">'.$nbContacts.'</span><span class="stat-bar-ref-status" style="width:'.$percent.'%;background-color:'.$projectStatus['color'].';"></span><span class="stat-perc-ref-status percentages">'.$percent.'%</span></td>';
													
												} // /foreach projects
													
												echo '</tr></table></td>';
													
											} // /if projecthasstatus
												
											echo '</tr>';
											
										} // /foreach referers
									
									echo '</table>';
									
								} // /if referers
								
								echo '	</td>
									  </tr>';
							
							} // /if(!$forceProjectID)
								
							
						} // /foreach clientProjects
						
						echo '</table>';
						
					} // /if clientProjects
					
					echo '	</td>
						  </tr>';
					
				} // /foreach clients
				
				echo '</table>';
								
			} // /if clients
			wp_die();
		}
		
		
		# Project / Contact / Status
		
		add_action('wp_ajax_get_analytics_proconsta', 'get_analytics_proconsta');
		add_action('wp_ajax_nopriv_get_analytics_proconsta', 'get_analytics_proconsta');
		
		function get_analytics_proconsta(){
			
			$date1 = $_POST['startDate'];
			$date2 = $_POST['endDate'];
			$postedProjectID = $_POST['project_id'];
			$forceProjectID = false;
				
			if($postedProjectID != '' && $postedProjectID != '0'){
				$forceProjectID = true;
			}
				
			if($date1 != '' && $date2 != ''){
				echo '<div class="redband">Statistics for Contacts recorded between '.$date1.' and '.$date2.'</div>';
			}
				
			# Get current user (client)
			
			$clients = meo_crm_users_getClients();
			
			# Will be use to change project background color
			$indexColor = 0;
			
			if($clients && is_array($clients)){
			
				# Init
				$htmlContent = '<table id="table-client">';
			
				foreach($clients as $c => $client){
			
					# CLIENT row
					$htmlContent.= '<tr>
						<td>';
					
					# Get List of his projects
					$clientProjects = meo_crm_projects_getProjectsByUserID($client['ID']);
			
					if($clientProjects && is_array($clientProjects)){
							
						$htmlContent.= '<table id="table-project">';
							
						foreach($clientProjects as $p => $project){
			
							if($project && !$forceProjectID || ($forceProjectID && $project['id'] == $postedProjectID)){
								
								# GET Project's Contacts
								$nbOfContactsTotalProj = meo_crm_contacts_countContactsByProjectID($project['id'], $date1, $date2);
									
								$projectHasStatus = false;
									
								# Get list of Status for the current project
								if(array_key_exists('rel_status', $project) && is_array($project['rel_status']) && count($project['rel_status']) > 1){
										
									$projectHasStatus = true;
			
									# Add EXIT state at the end
			
									$project['rel_status'][] = array (
												'id' => MEO_RELATIONSHIP_STATE_EXIT_ID,
												'project_id' => $project['id'],
												'label' => 'EXIT',
												'status_order' => MEO_RELATIONSHIP_STATE_EXIT_ID,
												'color' =>  MEO_RELATIONSHIP_STATE_EXIT_COLOR
											);
			
			
									# Build table headers
									$projectStatusHeaders = '<tr>
													<th>Record</th>';
									foreach($project['rel_status'] as $ps => $projectStatus){
										$projectStatusHeaders.= '<th>'.$projectStatus['label'].'</th>';
									}
									$projectStatusHeaders.= '</tr>';
			
								}
									
								$indexColor++;
								
								$percentBtn = '';
								if($nbOfContactsTotalProj > 0){
									$percentBtn = '<br/><div id="stats-referer-status-filter"><i class="fa fa-percent status-toggler cursor-hand" aria-hidden="true" onclick="togglePercentages(this);"></i></div>';
								}
								
								# PROJECT row
								$htmlContent.= '<tr class="project-color-'.($indexColor%2 == 0 ? '1' : '0').'">
													<th>'.$client['display_name'].'<br/>'.$project['name'].'<br/>'.$nbOfContactsTotalProj.' contacts'.$percentBtn.'</th>
													<td>';
									
								# Get list of users (Courtier) per Project
								$courtiers = meo_crm_users_getCourtiersByProjectID($project['id']);
									
								if($courtiers){
			
									$htmlContent.= '<table id="table-courtier">';
			
									foreach($courtiers as $uc => $courtier){
											
										# GET Courtier's Contacts
										$nbOfContactsTotalCourtier = meo_crm_contacts_countContactsByUserIDandProjectID($courtier['ID'], $project['id'], $date1, $date2);
										
										$percentageCourtierText = '';
										if($nbOfContactsTotalProj > 0){
											$percentageCourtier = round(($nbOfContactsTotalCourtier*100) / $nbOfContactsTotalProj);
											$percentageCourtierText = '<br/><span class="stat-perc-src percentages">'.$percentageCourtier.'%</span>';
										}
										
										# COURTIER row
										$htmlContent.= '<tr>
											<th>'.$courtier['display_name'].'<br/>'.$nbOfContactsTotalCourtier.' contacts'.$percentageCourtierText.'</th>
											<td>';
											
										if($projectHasStatus && $nbOfContactsTotalCourtier > 0){
											
											$htmlContent.= '<table id="table-status">'.$projectStatusHeaders.'<tr>';
			
			
											# Get Courtier stats in one query for all status (except 0)
											$courtierLastStats = MeoCrmContactsUsersRelationship::countContactsPerStatusIDbyCourtierIDProjectID($courtier['ID'], $project['id'], $date1, $date2);
											// $courtierSecondLastStats = MeoCrmContactsUsersRelationship::countContactsPerSecondLastStatusIDbyCourtierIDProjectID($courtier['ID'], $project['id'], $date1, $date2);
											$courtierSecondLastStats = MeoCrmContactsUsersRelationship::countContactsPerSecondLastStatusIDExitDetailsbyCourtierIDProjectID($courtier['ID'], $project['id']);
												
											$exitbackgroundColor = end($project['rel_status'])['color'];
			
											# Get stats for state 0 (recording a new user)
											$courtierStatsZero = MeoCrmContactsUsersRelationship::countContactsByStatusIDbyCourtierIDProjectID(0, $courtier['ID'], $project['id'], $date1, $date2);
											
											$htmlExit = '';
											
											if(is_array($courtierSecondLastStats) && array_key_exists(0, $courtierSecondLastStats)) {

												$htmlExit = '<span class="exit-details">
													<ul><li><strong>EXIT Details</strong></li>';
													
												foreach($courtierSecondLastStats[0] as $note => $nb){
													if($note != 'total'){
														$htmlExit.= '<li>'.$note.' : '.$nb.'</li>';
													}
												}
													
												$htmlExit.= '</ul>
												</span>';
												
												$nbexits = $courtierSecondLastStats[0]['total'];
												$percentageExit = round(($nbexits*100) / $nbOfContactsTotalCourtier);
												$nbexitshtml = '<span class="nbexits" style="background-color:'.$exitbackgroundColor.'; height:'.$percentageExit.'%;"><span class="stat-text">'.$nbexits.'</span></span>';
											}
											else $nbexitshtml ='';
			
											$percentage = round(($courtierStatsZero*100/$nbOfContactsTotalCourtier));
											$htmlContent.= '<td>'.$htmlExit.'<span class="nbcontacts" style="background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.'; height:'.$percentage.'%;"><span class="stat-text">'.$courtierStatsZero.'</span></span>'.$nbexitshtml.'</td>';
			
											foreach($project['rel_status'] as $ps => $projectStatus){
												
												# Add EXIT details on all status
												$htmlExit = '';
												
												// Current Status
												if(is_array($courtierLastStats) && array_key_exists($projectStatus['id'], $courtierLastStats)) {$nbcontacts = $courtierLastStats[$projectStatus['id']];}
												else $nbcontacts = 0;
																									
												// Current Status was last before exit (supposingly the last status)
												if(is_array($courtierSecondLastStats) && array_key_exists($projectStatus['id'], $courtierSecondLastStats)) {
													
													$htmlExit = '<span class="exit-details">
														<ul><li><strong>EXIT Details</strong></li>';
													
													foreach($courtierSecondLastStats[$projectStatus['id']] as $note => $nb){
														if($note != 'total'){
															$htmlExit.= '<li>'.$note.' : '.$nb.'</li>';
														}
													}
													
													$htmlExit.= '</ul>
													</span>';
													
													$nbexits = $courtierSecondLastStats[$projectStatus['id']]['total'];
													$percentageExit = round(($nbexits*100) / $nbOfContactsTotalCourtier);
													$nbexitshtml = '<span class="nbexits" style="background-color:'.$exitbackgroundColor.'; height:'.$percentageExit.'%;"><span class="stat-text">'.$nbexits.'</span></span>';
												}
												else $nbexitshtml ='';
												
												$percentage = round(($nbcontacts*100/$nbOfContactsTotalCourtier));
												
												
												if($projectStatus['id'] == MEO_RELATIONSHIP_STATE_EXIT_ID){
													# Exit details
													$exitDetails = MeoCrmContactsUsersRelationship::countContactsPerExitStateByProjectIDByUserID($project['id'], $courtier['ID'], $date1, $date2);
														
													if($exitDetails && is_array($exitDetails)){
															
														$htmlExit = '<span class="exit-details">
																<ul><li><strong>TOTAL EXIT</strong></li>';
															
														foreach($exitDetails as $text => $nb){
															$htmlExit.= '<li>'.$text.' : '.$nb.'</li>';
														}
															
														$htmlExit.= '</ul>
															</span>';
													}
												}
												
												
												$htmlContent.= '<td>'.$htmlExit.'<span class="nbcontacts" style="background-color:'.$projectStatus['color'].'; height:'.$percentage.'%;"><span class="stat-text">'.$nbcontacts.'</span></span>'.$nbexitshtml.'</td>';
												
											} // /foreach projects											
			
											$htmlContent.= '</tr></table>';
											
										} // /if projecthasstatus
											
										$htmlContent.= '</td></tr>';										
											
									} // /foreach courtiers
			
									$htmlContent.= '</table>';
			
								} // /if courtiers
									
								else {
									$htmlContent.= meo_crm_contacts_countContactsByUserIDandProjectID($client['ID'], $project['id']);
								}
									
								$htmlContent.= '	</td>
						  </tr>';
							} // /if project
			
						} // /foreach clientProjects
							
						$htmlContent.= '</table>';
							
					} // /if clientProjects
			
					$htmlContent.= '	</td>
			  </tr>';
			
				} // /foreach clients
			
				$htmlContent.= '</table>';
				echo $htmlContent;
			}
			
			wp_die();
		}
?>