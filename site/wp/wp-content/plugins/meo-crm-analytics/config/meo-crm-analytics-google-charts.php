<?php 

/*
 * Docs
 * 
 * https://developers.google.com/analytics/devguides/reporting/core/dimsmets
 * https://developers.google.com/chart/interactive/docs/
 * https://ga-dev-tools.appspot.com/query-explorer/
 * 
 */
function meo_crm_analytics_getGoogleChartsConfig() {
	
	$googleAnalyticsMasterArray = array (
		
			
			# GOALS # Reached Goals per Type
			
			"goalCompletion" => array(
						
					"info" => array(
							"title" => 'Nb Objectifs par Type',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:goalCompletionsAll",
							"dimensions" => "ga:goalCompletionLocation",
							"sort" => '-ga:goalCompletionsAll',
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 10,
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'goalCompletion',
							"options" => array(
									'legend' => 'none',
									'series' => array(
											0 => '#e2431e',
											1 => '#e7711b'
									)
							)
					)
			),
			
			# GOALS / ADWORDS # Reached Goals and Cost Per Source/Medium
					
			"goalPerSource" => array(
			
					"info" => array(
							"title" => 'Nb Objectifs &amp; Co&ucirc;t adWords par Source',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:goalCompletionsAll,ga:sessions,ga:adCost",
							"dimensions" => "ga:sourceMedium",
							"sort" => '-ga:goalCompletionsAll,-ga:sessions',
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 12,
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'goalPerSource',
							"options" => array(
									'legend' => 'none',
									'series' => array(
											0 => '#e2431e',
											1 => '#e7711b'
									)
							)
					)
			),
			
			# Events
			"eventsList" => array(
			
					"info" => array(
							"title" => 'Events counting',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:totalEvents",
							"dimensions" => "ga:eventCategory,ga:eventAction", // ,ga:eventLabel
							"sort" => '-ga:totalEvents',
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'eventsList',
				)
			),
			/*
			# ADWORDS # Visits and Cost per Source/Medium
			
			"costPerMedium" => array(
			
					"info" => array(
							"title" => 'Nb Visites &amp; Co&ucirc;t par Source',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions,ga:adCost",
							"dimensions" => "ga:sourceMedium",
							"filters" => 'ga:adCost>0',
							"sort" => '-ga:sessions',
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 10,
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'costPerMedium',
							"options" => array(
									'legend' => 'none',
									'series' => array(
											0 => '#e2431e',
											1 => '#e7711b'
									)
							)
					)
			),
			*/	
			# GEOGRAPHY # Country / Continent Table
			"sessionsPerCountryTable" => array(
			
				"info" => array(
						"title" => 'Visites par Pays',
						"description" => '',
				),
				"query" => array (	"metrics" => "ga:sessions",
						"dimensions" => "ga:country",
						"start-date" => 'today',
						"end-date" => 'today',
						"sort" => '-ga:sessions',
						"max-results" => '8',
				),
				"chart" => array ( 	"type" => 'TABLE',
						"container" => 'sessionsPerCountryTable',
						"options" => array(
								'legend' => 'none',
								'region' => '150',
						)
				)
			),
			
			# GEOGRAPHY # City / Country
			"sessionsPerCity" => array(
			
					"info" => array(
							"title" => 'Visites par Ville',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:city,ga:country",
							/* "filters" => 'ga:countryIsoCode==CH', */
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 12,
							"sort" => "-ga:sessions",
					),
					"chart" => array ( 	"type" => 'TABLE', /* GEO */
							"container" => 'sessionsPerCity',
							"options" => array(
									'legend' => 'none',
									'region' => 'CH',
									'colors' => '["#e2431e", "#e7711b"]',
									'displayMode' => 'markers',
							)
					)
			),
			
			# CONTENT # Keyword
			"keywordsPerSession" => array(
			
					"info" => array(
							"title" => 'Keywords per visit',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:keyword",
							"sort" => '-ga:sessions',
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 12,
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'keywordsPerSession',
					)
			),
			
			/*
			"sessionsPerDates" => array(
					
					"info" => array(
							"title" => 'Visits per day',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:day",
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'LINE',
							"container" => 'sessionsPerDates',
							"options" => array(
									'legend' => 'none',
									'series' => array(
											0 => '#e2431e',
											1 => '#e7711b'
									)
							)
					)
			),
			
			"sessionsPerHours" => array(
			
					"info" => array(
							"title" => 'Visits per hour of the day',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:hour",
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'LINE',
							"container" => 'sessionsPerHours',
							"options" => array(
									'legend' => 'none',
									'series' => array(
											0 => '#e2431e',
											1 => '#e7711b'
									)
							)
					)
			),
			
			"newUsers" => array(
						
					"info" => array(
							"title" => 'Users VS New Users',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:users,ga:newUsers",
							"dimensions" => "ga:day",
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'LINE',
							"container" => 'newUsers',
							"options" => array(
									'legend' => 'none',
									'series' => array(
											0 => '#e2431e',
											1 => '#e7711b'
									)
							)
					)
			),
			*/
			
			
			/*
			# Browser
			"sessionsPerBrowser" => array(
			
					"info" => array(
							"title" => 'Visits per device and browsers',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:deviceCategory, ga:browser",
							"sort" => 'ga:deviceCategory,-ga:sessions,ga:browser',
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'sessionsPerBrowser',
					)
			),
			*/
			
			/*
			# Country / Continent Map
			"sessionsPerCountryMap" => array(
	
					"info" => array(
							"title" => 'Visits per country',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:country",
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'GEO',
							"container" => 'sessionsPerCountryMap',
							"options" => array(
									'legend' => 'none',
									'region' => '150',
									'colors' => '["#e2431e", "#e7711b"]',
							)
					)
			),
			*/
			
			
			##############
			# TECHNOLOGY #
			##############
			/*
			# Device
			"sessionsPerDeviceCat" => array(
			
					"info" => array(
							"title" => 'Visits per device types',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:deviceCategory",
							"sort" => '-ga:sessions,ga:deviceCategory',
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'PIE',
							"container" => 'sessionsPerDeviceCat',
					)
			),
			*/
			/*
			# Browser Total
			"sessionsPerBrowserTotal" => array(
				
					"info" => array(
							"title" => 'Visits per device types',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:browser",
							"sort" => '-ga:sessions',
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'PIE',
							"container" => 'sessionsPerBrowserTotal',
					)
			),
				
			# Browser
			"sessionsPerBrowser" => array(
	
					"info" => array(
							"title" => 'Visits per device and browsers',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:deviceCategory, ga:browser",
							"sort" => 'ga:deviceCategory,-ga:sessions,ga:browser',
							"start-date" => 'today',
							"end-date" => 'today',
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'sessionsPerBrowser',
					)
			),
			*/
			###########
			# CONTENT #
			###########
			/*
			# Page
			"pageviewsPerSession" => array(
			
					"info" => array(
							"title" => 'Pageviews per visit',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:pageviewsPerSession",
							"dimensions" => "ga:pageTitle",
							"sort" => '-ga:pageviewsPerSession',
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 10,
							"filters" => 'ga:pageviewsPerSession>0'
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'pageviewsPerSession',
					)
			),
			*/
			
			/*
			# Event
			"evenTracking" => array(
			
					"info" => array(
							"title" => 'Event tracking',
							"description" => '',
					),
					"query" => array (	"metrics" => "ga:sessions",
							"dimensions" => "ga:eventLabel",
							"sort" => '-ga:sessions',
							"start-date" => 'today',
							"end-date" => 'today',
							"max-results" => 10,
					),
					"chart" => array ( 	"type" => 'TABLE',
							"container" => 'evenTracking',
					)
			),
			*/
	);
	
	return $googleAnalyticsMasterArray;
}

?>