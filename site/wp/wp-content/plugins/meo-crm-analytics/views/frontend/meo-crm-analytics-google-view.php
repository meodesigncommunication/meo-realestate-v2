<?php 
/**
 * Google Report
 */

get_header(); 

global $current_user;

if($current_user) {
	?>
	<div id="primary" class="content-area meo-crm-contacts-front-container">
		<main id="main" class="project-main" role="main">
			<div class="clear"></div>
			<?php 
				// Init
				$googleAnalyticsMasterArray = meo_crm_analytics_getGoogleChartsConfig();
			?>
			
			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
			<script type="text/javascript" src="https://www.google.com/jsapi"></script>
				
			<!-- Step 1: Create the containing elements. -->
			<div class="selectors-container redband">
				<section id="auth-button"></section>
				<section id="view-selector" class="view-selector"></section>
				<section id="date-selector" class="date-selector">
					<div>From: <input id="start-date" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" /></div>
					<div>To: <input id="end-date" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" /></div>
					<div><input id="property-id" name="property-id" type="hidden" /><input id="submit" name="submit" type="submit" /></div>
				</section>
				<div class="clear"></div>
			</div>
			
			<div class="google-charts-main">
			<?php 
				# Build chart containers
				foreach($googleAnalyticsMasterArray as $chart => $chartOptions) {
					
					echo "<div class='google-chart-container'>
							<h3>".$chartOptions['info']['title']."</h3>
							<section id='".$chart."' class='google-chart'></section>
						 </div>";
					
				}
			?>
			</div>
			<?php 
				# Load the library
				require_once(MEO_ANALYTICS_PLUGIN_ROOT.'/js/meo-crm-analytics.js.php'); 
			?>
		</main>
	</div>
	<?php 
}
else {
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access Google Report data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}
get_footer(); 
?>