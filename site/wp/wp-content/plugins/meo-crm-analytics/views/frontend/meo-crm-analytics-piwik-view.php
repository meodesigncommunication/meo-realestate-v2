<?php 
/**
 * Piwik Report
 */

get_header(); 

global $current_user;

if($current_user) {
	?>
	<div id="primary" class="content-area meo-crm-contacts-front-container">
		<main id="main" class="project-main align-filters" role="main">
			<div class="clear"></div>
			<?php 
				
				// Init
				$piwikAnalyticsMasterArray = false;
				
				// Get Projects
				$userProjects = meo_crm_projects_getProjects();
				
				$selectedProject = false;
				
				?>
				<!-- Step 1: Create the containing elements. -->
				<div class="selectors-container redband">
					<section id="auth-button"></section>
					<section id="view-selector" class="view-selector">
					<?php if($userProjects){ ?>
						<select id="siteId" name="siteId">
							<option value="0">Select a Project</option>
							<?php 
							$piwikInitiated = false;
							foreach($userProjects as $p => $project){
							
								if($project){
									if(array_key_exists('piwik_id', $project)){
										
										# Init piwikAnalytics
										$selected = '';
										if($project['piwik_id'] > 0 && !$piwikInitiated){
											$piwikAnalyticsMasterArray = meo_crm_analytics_getPiwikChartsConfig($project['piwik_id']);
											$piwikInitiated = true;
											$selected = ' selected';
											$selectedProject = $project['piwik_id'];
										}
										$disabled = '';
										if($project['piwik_id'] == 0){
											$disabled = ' disabled';
										}
										
										echo '<option value="'.$project['piwik_id'].'"'.$selected.''.$disabled.'>'.$project['name'].'</option>';
										
										
									}
								}
								
							}?>
						</select>
					<?php } ?>
					</section>
					<section id="date-selector" class="date-selector">
						<div>From: <input id="start-date" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" /></div>
						<div>To: <input id="end-date" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" /></div>
						<div><input id="property-id" name="property-id" type="hidden" /><input id="submitpiwik" name="submitpiwik" type="submit" /></div>
					</section>
					<div class="clear"></div>
				</div>
				<div id="piwik-charts-main">
				<?php 
				# Does the user have project(s)?
				if($piwikAnalyticsMasterArray && is_array($piwikAnalyticsMasterArray)){
					
					# Build chart containers
					foreach($piwikAnalyticsMasterArray as $chart => $chartOptions) {
					
						echo "<div class='piwik-chart-container'>
									<h3>".$chartOptions['info']['title']."</h3>
									<section id='".$chart."' class='google-chart'>
									".meo_crm_analytics_piwik_widget($chartOptions['moduleToWidgetize'], $chartOptions['actionToWidgetize'], $chartOptions['idSite'], $chartOptions['period'], $chartOptions['date'], $chartOptions['viewDataTable'])."
									</section>
								 </div>";
						
					}
					
				}
				?>
				</div>
				<?php 
				
					if($selectedProject){
						?>
						<div class="meo_crm_analytics_piwikvisitor" data-piwikurl="<?php echo MEO_ANALYTICS_SERVER; ?>">
							<a href="http://<?php echo MEO_ANALYTICS_SERVER; ?>/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=<?php echo $selectedProject; ?>&period=week&date=today" class="fancybox fancybox.iframe">Voir le tableau de bord</a>
						</div>
						<?php 
					}
					
					# Load the library
					require_once(MEO_ANALYTICS_PLUGIN_ROOT.'/js/meo-crm-analytics.js.php');
				?>
				<script type="text/javascript">
					jQuery(document).ready(function (){
						jQuery('.fancybox').fancybox({width:1200});
					});
				</script>
		</main>
	</div>
	<?php 
}
else {
	MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Trying to access Piwik Report data with no connection");
	MeoCrmCoreTools::meo_crm_core_403();
	die();
}
get_footer(); ?>