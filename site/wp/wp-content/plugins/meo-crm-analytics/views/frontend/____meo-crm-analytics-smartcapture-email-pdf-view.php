<?php
/**
 * Template name: MEO CRM Analytics Email PDF
 *
 * @package MEO real estate admin
 */

/* --------------------------------------------------------------------------------------------- */

global $wpdb, $expectedAnalyticsFields, $contactsTypes;

$pdf_email_cron_key = get_option('mca_cron_key');

// Shouldn't really matter, but we don't want this URL to be hit without permission
if (!array_key_exists('cron_key', $_GET) || $_GET['cron_key'] != $pdf_email_cron_key) {
	header("HTTP/1.0 403 Forbidden"); ?>
	<h1>Access Forbidden!</h1>
	<?php exit;
}

date_default_timezone_set("UTC"); // piwik uses UTC, so we need to do likewise

// Fin d'activite 30min
$activity_cutoff = time() - 30 * 60; // Half an hour ago

date_default_timezone_set('Europe/Zurich');  // TODO::Recheck server time


# Get potential emails
$potential_contacts = MeoCrmAnalytics::getScheduledEmails();


if (!empty($potential_contacts)) {
	
	# Get all projects
	$projects = meo_crm_projects_getProjects();
	
	foreach ($potential_contacts as $potential_contact) {
		
		# Set Current Time of Action
		$happeningNow = date('Y-m-d H:i:s');
		
		# Get Corresponding project
		if(array_key_exists($potential_contact['project_id'], $projects)){
			$project = $projects[$potential_contact['project_id']];
		}
		else {
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No project [".$potential_contact['project_id']."] found for " . print_r($potential_contact, true));
				
			continue;
		}
		
		# No Project : Error
		if (empty($project)) {
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No project found for " . print_r($potential_contact, true));
			
			continue;
		}		
		
		# Get list of project's email addresses that we need to send emails to
		if(array_key_exists('email', $project) && !empty($project['email'])) {
			$recipients = MeoCrmCoreTools::meo_crm_core_emailStringToArray($project['email']);
		}
		else {
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No project email for " . print_r($potential_contact, true));
				
			continue;
		}
		
		# No Recipient OR No PDF template : Error
		if (empty($recipients) || empty($project['pdf_template'])) {
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No recipient or PDF template for " . print_r($potential_contact, true));
			
			continue;
		}
		
		/*
		# It will make a Curl call to Piwik
		$last_visit_data = MeoCrmAnalyticsPiwik::getLastVisitData($project, $potential_contact['analytics_id']);
		
		if (empty($last_visit_data)) {
			# Update potential email record in the schedule
			$data = array( 'in_error' => 'yes' );
			$where = array( 'id' => $potential_contact['id'] );
			
			MeoCrmAnalytics::updateScheduledEmails($data, $where);
			
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Empty Last Visit Data for " . print_r($potential_contact, true));
			
			continue;
		}
		
		$last_visit_timestamp = (int) $last_visit_data[0]->lastActionTimestamp;
		*/
		
		#####################################################################################################
		# /!\ Now we are going to save the contact into the system, even if we don't have any analytics /!\ #
		#####################################################################################################
		
		$missingAnalyticsData = false;
		
		# Prepare potential contact for record
		
		$dataContact = array();

		$dataContact['analytics_id'] 	= $potential_contact['analytics_id']; // We suppose analytics_id exists on Piwik
		$dataContact['project_id'] 		= $potential_contact['project_id'];
		$dataContact['note'] 			= '';
		$dataContact['date_added'] 		= $happeningNow;
		
		# Get Client Id, normally one
		if(array_key_exists('clients', $project) && is_array($project['clients']) && reset($project['clients']) > 0){
			$clientId = reset($project['clients']);
		}
		else $clientId = 1; // Admin
		$dataContact['client_id'] 		= $clientId;
		
		#####################################################################################################
		
		# Trying to get data from Piwik
		
		$visitor_profile_data = MeoCrmAnalyticsPiwik::getVisitorProfileData($project, $potential_contact['analytics_id']);
		
		
		if (empty($visitor_profile_data)) {
			# Update potential email record in the schedule
			$data = array( 'in_error' => 'yes' );
			$where = array( 'id' => $potential_contact['id'] );
				
			MeoCrmAnalytics::updateScheduledEmails($data, $where);
				
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Empty Visitor Profile Data for " . print_r($potential_contact, true));
			
			// continue;
			$missingAnalyticsData = true;
		}
		
		
		# It will make a 'Ajax' call to corresponding immo project to get Contact Form Db data
		
		$dataAnalytics = MeoCrmAnalyticsPiwik::getContactDataByAnalyticsId($project, $potential_contact['analytics_id']);
			
		if (empty($dataAnalytics)) {
				
			echo 'No Analytics data found for ' . $potential_contact['analytics_id'] . '<br>';
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No Analytics data found for " . print_r($potential_contact, true));
			
			# We don't have any record or data on the immo site
			continue;
		}
		
		
		# We have data form imoo site contact form db
		
		
		# Get Analytics Fields (from submitted form) to add to Contact data
		foreach($expectedAnalyticsFields as $expectedAnalyticsField){
			$dataContact[$expectedAnalyticsField] = $dataAnalytics[$expectedAnalyticsField];
		}
		if(array_key_exists('submit_time', $dataContact)){unset($dataContact['submit_time']);}
			
		# Contact type
		$contact_type = 'manual';
		if (array_key_exists('contact_type', $dataAnalytics) && array_key_exists($dataAnalytics['contact_type'], $contactsTypes)){
			$contact_type = $dataAnalytics['contact_type'];
		}
		$dataContact['type'] = $contact_type;
		if(array_key_exists('contact_type', $dataContact)) { unset($dataContact['contact_type']); }
			
		# Capture Referer if exists
		if(array_key_exists('referer', $dataAnalytics) && $dataAnalytics['referer'] != ''){
			$dataContact['referer'] = $dataAnalytics['referer'];
		}
		
		# We have analytics data
		if(!$missingAnalyticsData){
			
			$last_visit_timestamp = (int) $visitor_profile_data->lastVisit->date + (int) $visitor_profile_data->totalVisitDuration;
			
			# Contact hasn't been active for the last 30minutes
			if ( $last_visit_timestamp <= $activity_cutoff ) {
				
			}
			# Contact is still active (/if ( $last_visit_timestamp <= $activity_cutoff ))
			else {
				echo $potential_contact['analytics_id'] . ' has been active in the last half hour, not sending<br/>';
				
				# If we can get analytics data, then we can retry later to record the contact with all the data
				continue;
			}
			
		} // if(!$missingAnalyticsData)
		else {
			$dataContact['note'] = 'TRACKING ERROR';
		}
			
			
		# Add data to contact from plugins adding this filter
		// If the project is activated for Sonnimmo, this filter will send the contact to it, check the plugin
		$dataContact = apply_filters( 'meo_crm_contact_record_dataContact', $dataContact );
			
		# Save Contact and get Id
		$newContactID = MeoCrmContacts::addContact($dataContact);
		
		if($newContactID == false){
			
			echo 'Couldn\'t record Contact ('.$wpdb->last_error.') for ' . $potential_contact['analytics_id'] . '<br/>';
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Couldn't record Contact (".$wpdb->last_error.") for " . print_r($dataContact, true));
			
			# Update potential email record in the schedule, in order to not retry to store it (the issue mainly happens as the contact already exists)
			$data = array( 'in_error' => 'yes' );
			$where = array( 'id' => $potential_contact['id'] );
				
			MeoCrmAnalytics::updateScheduledEmails($data, $where);
			
			continue;
		}
		
		$dataContact['id'] = $newContactID;
			
			
		# Building the PDF file
		
		
		$PDFpath = MEO_CRM_PROJECTS_FOLDER_DIR . $potential_contact['project_id'] . '/' . MEO_CONTACTS_FOLDER . '/' . MEO_ANALYTICS_CONTACTPDF_FOLDER;
		$PDFfile = $PDFpath . '/' . meo_crm_analytics_building_contact_pdf($dataContact);
		
		if (file_exists($PDFfile)) {
			unlink($PDFfile);
		}
		try {
			# Get the PDF Template to write on it
			$PDFtemplate = MEO_CRM_PROJECTS_FOLDER_DIR . $dataContact['project_id'] . '/' . MEO_CRM_ANALYTICS_PDF_DIRNAME . '/' . $project['pdf_template'];
							
			$pdf = new MeoCrmContactPdf('en', $PDFtemplate);
			$pdf->generate($dataAnalytics, 0, 0);
			$pdf->Output($PDFfile, 'F');
			
		}
		catch (Exception $e) {
			
			echo 'Error generating PDF for ' . $potential_contact['analytics_id'] . '<br>';
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Error generating PDF for " . print_r($dataContact, true));
			
			continue;
		}
			
			
		# Availability
		
		
		// New: param to add extra content to the email (Courtier info so far)
		$extracontent = "";
		$emailCourtier = "";
		
		# Check if project allows Availability option
		if(array_key_exists('availability', $project) && $project['availability'] == 1){
			
			# Select available courtier, to get the email address // check availabitlity
			$potential_courtier = MeoCrmUsersAvailability::getAvailableUsersByProjectID($project['id'], 1);
			
				# We have an available Courtier
				if($potential_courtier) {
					
					$courtier = reset($potential_courtier);
					# Get Courtier's email address
					$emailCourtier = $courtier->data->user_email;
					
				}
				# We don't have any available Courtier
				else {
					
					# Make them all available
					MeoCrmUsersAvailability::resetUsersAvailabilityByProjectID($project['id']);
					# Pick one
					$potential_courtier = MeoCrmUsersAvailability::getAvailableUsersByProjectID($project['id'], 1);
					
					if($potential_courtier){
						$courtier = reset($potential_courtier);
						# Get Courtier's email address
						$emailCourtier = $courtier->data->user_email;
					}
					else {
						// If there is no Courtier, just report it, email will be send to the client anyway
						MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "No Courtier for Project " . print_r($project, true));
						echo "/!\ No Courtier for Project " . $project['id'].'<br/>';
					}
					
				}
				
				# Send email to the selected one
				if($emailCourtier != ""){
					
					# Prepare Email settings
					$headers = array( 'From: ' . MEO_CORE_MAIL_NAME . ' <' . MEO_CORE_MAIL_USER_FROM . '>', 'Content-type: text/html; charset=UTF-8' );
					$subject = $project['name'] . ' :: ' . __( 'new contact', 'meo_real_estate_admin' );
					
					$replacements = array( 	'{CLIENT_FIRST_NAME}' => $dataAnalytics['first_name'],
											'{CLIENT_SURNAME}'    => $dataAnalytics['last_name'],
											'{CLIENT_ADMIN_URL}'  => home_url('/').MEO_CONTACT_SLUG.'/?id='.$newContactID
					);
				
					$content = str_replace(array_keys($replacements), array_values($replacements), $project['email_content']);
					
					# Send email to Courtier
					$sentCourtier = wp_mail( $emailCourtier, $subject, '<html><body>'.$content.'</body></html>', $headers, array($PDFfile) );
					
					if($sentCourtier){
						
						# Mark the Courtier as unavailable
						$data = array('availability' => 0, 'last_send' => $happeningNow);
						$where = array('user_id' => $courtier->ID, 'project_id' => $project['id']);
						MeoCrmUsersAvailability::updateUserAvailability ($data, $where);
							
						# Add Courtier's email address to the default project email content that will be send below
						$extracontent = '<br/>Courtier '.$emailCourtier;
						
						# Check if project is active for emails history
						if(array_key_exists('email_history', $project) && $project['email_history'] == 1){
							
							# Record Courtier Emails History
							MeoCrmUserEmailsHistory::addEmailHistory(array('user_id' => $courtier->ID, 'contact_id' => $newContactID, 'date_sent' => $happeningNow));
						}
						
						# TODO::Update Contact/User relationship table and check if relationship (eventually if feature is not enabled by default)
						$relationData = array('contact_id' => $newContactID, 'user_id' => $courtier->ID, 'status_id' => 0, 'date_updated' => $happeningNow);
						MeoCrmContactsUsersRelationship::addContactUserRelationship($relationData);
					}
					else {
						MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Failed to send to Courtier (".$emailCourtier.")" . basename($PDFfile) . ' for ' . print_r($potential_contact, true));
						echo 'Failed to send to Courtier ('.$emailCourtier.') ' . basename($PDFfile) . '<br/>';
					}
				} // /if $emailCourtier
				
		} // /if availability
			
				
		# We need to send the email to the project default email address too (including the Courtier info, if we have it)
		$subject = $project['name'] . ' :: ' . __( 'new contact', 'meo_real_estate_admin' );
		$headers = array( 'From: ' . MEO_CORE_MAIL_NAME . ' <' . MEO_CORE_MAIL_USER_FROM . '>', 'Content-type: text/html; charset=UTF-8' );
		
		$replacements = array(
				'{CLIENT_FIRST_NAME}' => $dataAnalytics['first_name'],
				'{CLIENT_SURNAME}'    => $dataAnalytics['last_name'],
				'{CLIENT_ADMIN_URL}'  => home_url('/').MEO_CONTACT_SLUG.'/?id='.$newContactID
		);
		
		$content = str_replace(array_keys($replacements), array_values($replacements), $project['email_content']);
		$content.= $extracontent;
		
		# Send email to the selected email address
		$sent = wp_mail( $project['email'], $subject, '<html><body>'.$content.'</body></html>', $headers, array($PDFfile) );
		
		if ($sent) {
			echo basename($PDFfile) . ' has been generated and sent<br/>';
			
			# Update potential email record in the schedule
			$data = array( 'sent_date' => $happeningNow ); // current_time('mysql', 1)
			$where = array( 'id' => $potential_contact['id'] );
			
			MeoCrmAnalytics::updateScheduledEmails($data, $where);
		}
		# Failed to send
		else {
			MeoCrmCoreTools::meo_crm_core_report_error(__FILE__, __LINE__, "Failed to send " . basename($PDFfile) . ' for ' . print_r($potential_contact, true));
			
			echo 'Failed to send ' . basename($PDFfile) . '<br>';
		}
		
		# Cleaning PHP Memory
		unset($dataAnalytics);
		unset($dataContact);
		unset($potential_courtier);
		
		
	}
}

echo 'Done<br/>';
