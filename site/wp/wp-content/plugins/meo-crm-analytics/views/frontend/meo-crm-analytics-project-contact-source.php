<?php 
/**
 * The template for displaying individual Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

global $current_user;

get_header(); 

?>
<div id="primary" class="content-area meo-crm-contacts-front-container">
	<main id="main" class="project-main align-filters" role="main">
		<div class="clear"></div>
		<div class="meo-crm-analytics-contacts-projects-referer">
<?php 
# Get current user (client)
// if(current_user_can('administrator') || current_user_can(CLIENT_ROLE)){
	$clients = meo_crm_users_getClients();
/*}
else if(current_user_can(COURTIER_ROLE)){
	$clients = array(meo_crm_users_getClientByCourtierID($current_user->ID));
	print_r($clients);
}*/


# Will be use to change project background color
$indexColor = 0;

if($clients && is_array($clients)){
	
	# Init
	$htmlContent = '';
	$htmlFilter = '';
	## $masterArray = array();
	
	$htmlContent.= '<div id="stats-container-1">
 					<table id="table-client">';
	
	foreach($clients as $c => $client){
		
		# CLIENT row
		$htmlContent.= '<tr>
				<td>';
		
		$htmlFilter.= '<optgroup label="'.$client['display_name'].'" data-client="'.$client['ID'].'">';
		
		# Get List of his projects
		$clientProjects = meo_crm_projects_getProjectsByUserID($client['ID']);
		
		if($clientProjects && is_array($clientProjects)){
			
			$htmlContent.= '<table id="table-project">';
			
			foreach($clientProjects as $p => $project){
				
				if($project){
					# HTML
					$htmlFilter.= '<option value="'.$project['id'].'">'.$project['name'].'</option>';
					
					# GET Project's Contacts
					$nbOfContactsTotalProj = meo_crm_contacts_countContactsByProjectID($project['id']);

					## $masterArray[$client['display_name']]['projects'][$project['name']]['total'] = $nbOfContactsTotalProj;
					
					$projectHasStatus = false;
					
					# Get list of Status for the current project
					if(array_key_exists('rel_status', $project) && is_array($project['rel_status']) && count($project['rel_status']) > 0){ // 1
					
						$projectHasStatus = true;

						# Add EXIT state at the end
						
						$project['rel_status'][] = array (
								'id' => MEO_RELATIONSHIP_STATE_EXIT_ID,
								'project_id' => 28,
								'label' => 'EXIT',
								'status_order' => MEO_RELATIONSHIP_STATE_EXIT_ID,
								'color' =>  MEO_RELATIONSHIP_STATE_EXIT_COLOR);
						
						
						$nbStatus = count($project['rel_status']) + 1; 	// Number of statuses + record one, to evaluate td with
						$tdWidth = round(99/$nbStatus, 4); 				// 99 instead of 100 to not reach a too high percentage of width
							
						# Build table headers
						$projectStatusHeaders = '<tr>
													<th style="width:'.$tdWidth.'%">Record</th>';
						foreach($project['rel_status'] as $ps => $projectStatus){
							$projectStatusHeaders.= '<th style="width:'.$tdWidth.'%">'.$projectStatus['label'].'</th>';
						}
						$projectStatusHeaders.= '</tr>';
	
						$nbContactsPerRefererPerStatus = MeoCrmContactsUsersRelationship::countContactsPerRefererPerStatusbyProjectID($project['id']);
					}
					
					$indexColor++;
					
					$percentBtn = '';
					if($nbOfContactsTotalProj > 0){
						$percentBtn = '<br/><div id="stats-referer-status-filter"><i class="fa fa-percent status-toggler cursor-hand" aria-hidden="true"></i></div>';
					}
					
					# Piwik Visits
					$Visits = MeoCrmAnalyticsPiwik::getNbVisits($project, '2010-01-01,today', 'range');
					$nbVisits = 0;
					$nbVisitsText = '';
					if($Visits && is_array($Visits)){
						$nbVisits = $Visits['nb_visits'];
						$nbVisitsText = '<br/><span class="nbvisits">'. $nbVisits . ' visits</span>';
					}
					
					# PROJECT row
					$htmlContent.= '<tr class="project-color-'.($indexColor%2 == 0 ? '1' : '0').'">
							<th>'.$client['display_name'].'<br/>'.$project['name'].'<br/><em>'.$nbOfContactsTotalProj.' contacts</em>'.$nbVisitsText.$percentBtn.'</th>
							<td>';
					
					# countContactPerDistinctRefererByProjectID
					$referers = meo_crm_contacts_countContactPerDistinctRefererByProjectID($project['id']);
					
					if($referers){
						
						$htmlContent.= '<table id="table-referer">';
						
						if($projectHasStatus){
								
							$htmlContent.= '<tr>
									<th class="th-referer">Referer</th>
									<td class="stat-referer-td">Contacts</td>
									<td><table id="table-status-referer-headers">'.$projectStatusHeaders.'</table></td>
								</tr>';
						
						} // /if projecthasstatus
						else {
							$htmlContent.= '<tr>
									<th class="th-referer">Referer</th>
									<td class="stat-referer-td">Contacts</td>
									<td></td>
								</tr>';
						} // /else projecthasstatus
						
						$refererVisits = MeoCrmAnalyticsPiwik::getNbVisitsPerReferer($project, '2010-01-01,today', 'range');
						
						foreach($referers as $uc => $referer){
							
							$nbOfContactsRef = $referer['nbcontacts'];
							
							$percentageRefText = '';
							if($nbOfContactsTotalProj > 0){
								$percentageRef = round(($nbOfContactsRef*100) / $nbOfContactsTotalProj);
								$percentageRefText = '<br/><span class="stat-perc-src percentages">'.$percentageRef.'%</span>';
							}
							
							$styleText = '';
							if($percentageRef > 90){
								$styleText = ' style="position: absolute; right: 10px; top: 17px;"';
							}
							
							# Piwik Visits Per Referer
							$nbVisitsRefText = '';
							$percentageRefVis = '';
							if($nbVisits > 0 && $refererVisits && is_array($refererVisits) && array_key_exists($referer['referer'], $refererVisits)){
								$nbVisitsRef = $refererVisits[$referer['referer']]['nb_visits']*1;
								$nbVisitsRefText = '<br/><span class="nbvisitsperreferer">'. $nbVisitsRef . ' visits</span>';
								$percentageRefVis = "<span class='nbvisitsperreferer percentages'> - ".round(($nbVisitsRef*100) / $nbVisits)."%</span>"; // Visits
							}
							
							# REFERER row
							$htmlContent.= '<tr>
									<th>'.$referer['referer'].$nbVisitsRefText.''.$percentageRefVis.'</th>
									<td class="stat-referer-td"><span class="stat-referer" style="background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.'; width:'.$percentageRef.'%;"></span><span class="stat-text-src"'.$styleText.'>'.$nbOfContactsRef.''.$percentageRefText.'</span></td>';

							## $masterArray[$client['display_name']]['projects'][$project['name']]['referers']['total'] = $nbOfContactsRef;
							
							if($projectHasStatus){
								
								$htmlContent.= '<td><table id="table-status-referer"><tr>';
								
								## $masterArray[$client['display_name']]['projects'][$project['name']]['status'] = array();
								
								$nbContacts = 0;
								$percent = 0;
								if(array_key_exists($referer['referer'], $nbContactsPerRefererPerStatus) && array_key_exists(0,$nbContactsPerRefererPerStatus[$referer['referer']] )){
									$nbContacts = $nbContactsPerRefererPerStatus[$referer['referer']][0]['nbcontacts'];
									$percent = round(($nbContacts * 100)/$nbOfContactsRef); // $nbOfContactsRef;$nbOfContactsTotalProj
								}
								$htmlContent.= '<td class="nbcontacts-src-status" style="width:'.$tdWidth.'%"><span class="stat-text-ref-status">'.$nbContacts.'</span><span class="stat-bar-ref-status" style="width:'.$percent.'%;background-color:'.MEO_RELATIONSHIP_STATE_DEFAULT_COLOR.';"></span><span class="stat-perc-ref-status percentages">'.$percent.'%</span></td>';
								
								foreach($project['rel_status'] as $ps => $projectStatus){
									// Current Status
									$indexStatus = $projectStatus['id'];
									$nbContacts = 0;
									$percent = 0;
									if(array_key_exists($referer['referer'],$nbContactsPerRefererPerStatus) && array_key_exists($indexStatus,$nbContactsPerRefererPerStatus[$referer['referer']] )){
										$nbContacts = $nbContactsPerRefererPerStatus[$referer['referer']][$indexStatus]['nbcontacts'];
										$percent = round(($nbContacts * 100)/$nbOfContactsRef); // $nbOfContactsRef;$nbOfContactsTotalProj
									}
									$htmlContent.= '<td class="nbcontacts-src-status" style="width:'.$tdWidth.'%"><span class="stat-text-ref-status">'.$nbContacts.'</span><span class="stat-bar-ref-status" style="width:'.$percent.'%;background-color:'.$projectStatus['color'].';"></span><span class="stat-perc-ref-status percentages">'.$percent.'%</span></td>';
									
									## if(!array_key_exists($projectStatus['label'], $masterArray[$client['display_name']]['projects'][$project['name']]['status'])){
									##	$masterArray[$client['display_name']]['projects'][$project['name']]['status'][] = $projectStatus['label'];
									## }
									## $masterArray[$client['display_name']]['projects'][$project['name']]['referers']['list'][$referer['referer']]['status'][$projectStatus['label']] = $nbContacts;
									
								} // /foreach projects
									
								$htmlContent.= '</tr></table></td>';
									
							} // /if projecthasstatus
							else $htmlContent.= '<td></td>';
								
							$htmlContent.= '</tr>';
							
						} // /foreach referers
						
						$htmlContent.= '</table>';
						
					} // /if referers
					
					$htmlContent.= '	</td>
						  </tr>';
				} // /if project
					
				
			} // /foreach clientProjects
			
			$htmlContent.= '</table>';
			
		} // /if clientProjects
		
		$htmlContent.= '	</td>
			  </tr>';
		
	} // /foreach clients
	
	$htmlContent.= '</table>
					</div>
					<div id="stats-container-2"></div>';
	
	$htmlFilter.= '</optgroup>';
	
	?>
	<div class="selectors-container redband">
		<section id="view-selector-1" class="view-selector">
			<select id="siteId-1" name="siteId">
				<option value="0">Select a Project</option>
				<?php echo $htmlFilter; ?>
			</select>
		</section>
		<section id="date-selector" class="date-selector">
			<div>From: <input id="start-date-1" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div>To: <input id="end-date-1" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div><input id="submit_proconsrc-1" name="submit" type="submit" /></div>
		</section>
		<div class="compare">VS</div>
		<section id="view-selector-2" class="view-selector">
			<select id="siteId-2" name="siteId-2">
				<option value="0">Select a Project</option>
				<?php echo $htmlFilter; ?>
			</select>
		</section>
		<section id="date-selector-2" class="date-selector">
			<div>From: <input id="start-date-2" name="start-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div>To: <input id="end-date-2" name="end-date" size="8" value="<?php echo date('Y-m-d'); ?>" class="datepicker" /></div>
			<div><input id="submit_proconsrc-2" name="submit" type="submit" /></div>
		</section>
		<div class="clear"></div>
	</div>
	<?php 
	
	echo $htmlContent;
	
	
} // /if clients
else MeoCrmCoreTools::meo_crm_core_403();
?>
	</div>
	
	<?php /* ## 
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

		function drawChart() {
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      
      var options = {
    	                                                    width: 600,
    	                                                    height: 400,
    	                                                    legend: { position: 'top', maxLines: 3 },
    	                                                    bar: { groupWidth: '75%' },
    	                                                    isStacked: false
    	                                                  };
      <?php 
      
      $i = 0;
      
		$jsArray = '';
			
			foreach($masterArray as $client => $clientData){
			
				foreach($clientData as $p => $projects){
					
					foreach($projects as $projectName => $projectData){
						
						$jsArray = "[";
						
						if(array_key_exists('status', $projectData)){
							$jsArray.= "[";
							foreach($projectData['status'] as $s => $status){
								$jsArray.= "'".$status."', ";
							}
							$jsArray.= "{ role: 'annotation' } ], ";
						}
						
						if(array_key_exists('referers', $projectData) && array_key_exists('list', $projectData['referers'])){
							
							foreach($projectData['referers']['list'] as $refererName => $refererData){
								$jsArray.= "['".$refererName."', ";
								
								foreach($refererData['status'] as $statusName => $statusNbContacts){
									$jsArray.= "".$statusNbContacts.",";
								}
								
								$jsArray.= "], ";
							}
							
						}
						
						$jsArray.= "]";
						
						echo "var data".$i." = google.visualization.arrayToDataTable(" . $jsArray . ");

    	                                                  

				        // Instantiate and draw our chart, passing in some options.
				        var chart".$i." = new google.visualization.BarChart(document.getElementById('chart_div".$i."'));
				        chart".$i.".draw(data".$i.", options);";
						
						$i++;
					}
					
				}
				
			}
	?>
      }
    </script>
    <!--Div that will hold the pie chart-->
    <div id="chart_div0"></div>
    <div id="chart_div1"></div>
    <div id="chart_div2"></div>
    <div id="chart_div3"></div>
    <div id="chart_div4"></div>
    <div id="chart_div5"></div>
    <div id="chart_div6"></div>
    <div id="chart_div7"></div>
    <div id="chart_div8"></div>
    <div id="chart_div9"></div>
    <div id="chart_div10"></div>
    <div id="chart_div11"></div>
    */
	?>
    
	</main><!-- .project-main -->
</div><!-- .content-area -->
<?php 
# Load the library
require_once(MEO_ANALYTICS_PLUGIN_ROOT.'/js/meo-crm-analytics.js.php');
get_footer(); 
?>