<?php 

/*
 Plugin Name: MEO CRM Access Management
 Description: Plugin to manage client access to plugins
 Version: 1.0
 Author: MEO
 Author URI: http://www.meo-realestate.com/
 */

/*
 Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential

 info@meomeo.ch
 */

$plugin_root = plugin_dir_path( __FILE__ );

# Defines / Constants
define('MEO_ACCESS_MANAGEMENT_TABLE', 'meo_crm_access_management');	// Access Management db table


# Required Files
require_once( $plugin_root . 'class-meo-crm-access-management.php');
require_once( $plugin_root . 'views/backend/meo-crm-access-management-view.php');


# Globals
global $meo_crm_access_management_db_version;

$meo_crm_access_management_db_version = '1.0';


# Plugin activation
function meo_crm_access_management_activate() {

	global $wpdb, $meo_crm_access_management_db_version;
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$table_name = $wpdb->prefix . MEO_ACCESS_MANAGEMENT_TABLE;
	$charset_collate = $wpdb->get_charset_collate();

	// Create plugin table
	$sql = "CREATE TABLE $table_name (
				user_id int(11) NOT NULL,
				plugin varchar(64) NOT NULL,
				access int(11) NOT NULL DEFAULT '0',
				updated datetime NOT NULL
				) $charset_collate;";

	dbDelta( $sql );
	
	$sql2 = "ALTER TABLE $table_name
				ADD CONSTRAINT uq_".$table_name." UNIQUE(user_id, plugin);";
	
	dbDelta( $sql2 );

	// Update db with the version info
	add_option( 'meo_crm_access_management_db_version', $meo_crm_access_management_db_version );

}
register_activation_hook( __FILE__, 'meo_crm_access_management_activate' );



# Add Scripts and Styles

	add_action( 'admin_enqueue_scripts', 'meo_crm_access_management_scripts_styles' );
	
	function meo_crm_access_management_scripts_styles() {
		
		# JS
		wp_register_script( 'meo_crm_access_management_js',  plugins_url('js/meo-crm-access-management.js', __FILE__), false, '1.0.0' );
		wp_enqueue_script( 'meo_crm_access_management_js' );
	
		wp_localize_script( 'meo_crm_access_management_js', 'meo_crm_access_management_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
	
		# CSS
		wp_register_style( 'meo_crm_access_management_css',  plugins_url('css/meo-crm-access-management.css', __FILE__), false, '1.0.0' );
		wp_enqueue_style( 'meo_crm_access_management_css' );
	
	}

# Add Ajax calls and functions

	# Update Contact Field
	
	add_action( 'wp_ajax_update_access_management', 'update_access_management_callback' );
	add_action( 'wp_ajax_nopriv_update_access_management', 'update_access_management_callback' );
	
	function update_access_management_callback() {
		global $wpdb;
		
		$userID = intval( $_POST['userID'] );
		$pluginToHTML =  $_POST['pluginToHTML'];
		$access = intval( $_POST['access'] );
		
		$pluginPath = str_replace('_', '-', $pluginToHTML.'/'.$pluginToHTML.'.php');
	
		$data = array('access' => $access, 'updated' => date('Y-m-d H:i:s'));
		$where = array('user_id' => $userID, 'plugin' => $pluginPath);
		$whereSelect = "user_id = '".$userID."' AND plugin = '".$pluginPath."'";
	
		$existingRecord = MeoCrmAccessManagement::getAccessManagementRecords($whereSelect);
		
		if($existingRecord){MeoCrmAccessManagement::updateAccessManagement($data, $where);}
		else MeoCrmAccessManagement::addAccessManagement(array_merge($data, $where));
		
		echo 'saved';
		
		wp_die();
	}


#########
# ADMIN #
#########


	# Admin Menu
	
	add_action( 'admin_menu', 'meo_crm_access_management_admin_menu' );
	
	function meo_crm_access_management_admin_menu() {
		add_menu_page( 'MEO CRM Access',
				'Access',
				'manage_options',
				'meo-crm-access-management/views/backend/meo-crm-access-management-view.php',
				'meo_crm_access_management_admin_page',
				'dashicons-unlock',
				6  );
	}
	
	
/* Filters */
	
	
	# Content Access Filter
	
	add_filter('meo_crm_content_access_filter_front', 'meo_crm_content_access_filter_front_callback', 10, 3);
	
	function meo_crm_content_access_filter_front_callback($HTMLcontent, $plugin, $current_user_id) {
		
		# Rebuild PluginPath
		$pluginPath = $plugin.'/'.$plugin.'.php';
		
		$whereSelect = "user_id = '".$current_user_id."' AND plugin = '".$pluginPath."' AND access = 1";
		
		$existingRecord = MeoCrmAccessManagement::getAccessManagementRecords($whereSelect);
		
		if($existingRecord || current_user_can( 'manage_options' ) ){
			return $HTMLcontent.$plugin.$current_user_id;
		}
		else return 'Access Forbidden, you should upgrade your account to use this feature.';
	}
	
	
#########################
#    EXTENDS PLUGINS    #
#########################
	
	
	# Contacts Data Access
	
	add_filter('meocrmcontacts_getContactData', 'meo_crm_content_access_getContactData', 100, 1);
	
	function meo_crm_content_access_getContactData($contact){
		
		global $current_user;
		
		# Exception for the Cron
		$pdf_email_cron_key = get_option('mca_cron_key');
		
		if($contact){
			
			# Administrator
			if( current_user_can('administrator') || ( isset($_GET['cron_key'] ) &&  $_GET['cron_key'] == $pdf_email_cron_key ) ){
				return $contact;
			}
			# Client
			else if( current_user_can(CLIENT_ROLE) && is_null($contact['date_deleted']) ){
				# Contact's Client matches Current Client
				if( $contact['client_id'] == $current_user->ID ){
					return $contact;
				}
				# Contact's Project matches one of Current Client ones
				if( isset($current_user->projects) && is_array($current_user->projects) ){
					$clientProjects = $current_user->projects;
					
					foreach($clientProjects as $cpid => $cpname){
						if($cpid == $contact['project_id']){
							return $contact;
						}
					}
				}
			}
			# Courtier
			else if( current_user_can(COURTIER_ROLE)  && is_null($contact['date_deleted']) ){
				# Contact's Courtier matches Current Courtier
				if( array_key_exists('courtier_id', $contact) && $contact['courtier_id'] == $current_user->ID ){
					return $contact;
				}
			}
		}
		return false;
	}
	
	
	# Users Data Access : Clients
	
	add_filter('meocrmusers_getClientData', 'meo_crm_content_access_getClientData', 100, 1);
	
	function meo_crm_content_access_getClientData($client){
		
		global $current_user;

		# Exception for the Cron
		$pdf_email_cron_key = get_option('mca_cron_key');
		
		# Administrator
		if( current_user_can('administrator') || ( isset($_GET['cron_key'] ) &&  $_GET['cron_key'] == $pdf_email_cron_key ) ){
			return $client;
		}
		# Client matches Current Client
		else if( current_user_can(CLIENT_ROLE) && $client['ID'] == $current_user->ID ){
			return $client;
		}
		
		return false;
	}
	
	
	# Users Data Access : Courtiers
	
	add_filter('meocrmusers_getCourtierData', 'meo_crm_content_access_getCourtierData', 100, 1);
	
	function meo_crm_content_access_getCourtierData($courtier){
	
		global $current_user;

		# Exception for the Cron
		$pdf_email_cron_key = get_option('mca_cron_key');
		
		# Administrator
		if( current_user_can('administrator') || ( isset($_GET['cron_key'] ) &&  $_GET['cron_key'] == $pdf_email_cron_key ) ){
			return $courtier;
		}
		# Client
		else if( current_user_can(CLIENT_ROLE) ){
			# Courtier's Client matches Current Client
			if( $courtier['client_id'] == $current_user->ID ){
				return $courtier;
			}
			/* Commented as a Project can have one or more clients, so if we base on project, we can get some other client's courtiers
			# Contact's Project matches one of Current Client ones
			if( isset($current_user->projects) && is_array($current_user->projects) ){
				$clientProjects = $current_user->projects;
				
				foreach($clientProjects as $cpid => $cpname){
					if(array_key_exists('projects', $courtier) && is_array($courtier['projects']) && array_key_exists($cpid, $courtier['projects'])){
						echo $cpid;
						print_r($courtier);
						return $courtier;
					}
				}
			}
			*/
		}
		# Courtier
		else if( current_user_can(COURTIER_ROLE) ){
			# Courtier matches Current Courtier
			if( $courtier['ID'] == $current_user->ID ){
				return $courtier;
			}
		}
		return false;
	}
	
	
	# Projects Data Access
	
	add_filter('meocrmprojects_getProjectData', 'meo_crm_content_access_getProjectData', 100, 1);
	
	function meo_crm_content_access_getProjectData($project){
			
		global $current_user;

		# Exception for the Cron
		$pdf_email_cron_key = get_option('mca_cron_key');
		
		if($project){
			
			$returnProject = array();
			
			# Administrator
			if( current_user_can('administrator') || ( isset($_GET['cron_key'] ) &&  $_GET['cron_key'] == $pdf_email_cron_key ) ){
				return $project;
			}
			# Client
			else if( current_user_can(CLIENT_ROLE) ){
				# Project's Client matches Current Client
				if( array_key_exists('clients', $project) && in_array($current_user->ID, $project['clients']) ){
					return $project;
				}
			}
			# Courtier
			else if( current_user_can(COURTIER_ROLE) ){
				# Projectt's Courtier matches Current Courtier
				if( array_key_exists('courtiers', $project) && in_array($current_user->ID, $project['courtiers']) ){
					return $project;
				}
			}
			return false;
			
		}
		return false;
	}
	
	/**
	 * Filter the login redirect URL.
	 *
	 * @since 3.0.0
	 *
	 * @param string           $redirect_to           The redirect destination URL.
	 * @param string           $requested_redirect_to The requested redirect destination URL passed as a parameter.
	 * @param WP_User|WP_Error $user                  WP_User object if login was successful, WP_Error object otherwise.
	 * 
	 */
	add_filter('login_redirect', 'meo_crm_access_management_login_redirect', 100, 3);
	
	function meo_crm_access_management_login_redirect($redirect_to, $requested_redirect_to, $user){
		
		if($user && is_object($user) && isset($user->data) && isset($user->data->ID) && $user->data->ID > 0){
			return $redirect_to.'?connect_via_google=true';
		}
		return $redirect_to;
	}
	
	add_filter('admin_init', 'meo_crm_access_management_login');
	
	function meo_crm_access_management_login(){
		
		global $current_user;
		
		
		if(is_user_logged_in() && array_key_exists('connect_via_google', $_GET) && $_GET['connect_via_google'] == 'true'){
			
			wp_redirect(home_url('/'));
			exit;
		}
	}
?>