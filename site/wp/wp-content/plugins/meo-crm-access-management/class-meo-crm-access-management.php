<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoCrmAccessManagement {
	
	# Access Management Attributes
	
		private $user_id;
		private $plugin;
		private $access;
		private $updated;
	
		public function __construct() { }
		
	# Corresponding Database Table
	
		public static function activate() { }
		
		public static function deactivate() { }
	
	# Manage Access Management in the Database
		
		public static function addAccessManagement ($data){
			
			global $wpdb;
			
			$table = $wpdb->prefix . MEO_ACCESS_MANAGEMENT_TABLE;
			
			$wpdb->insert( $table, $data );
		}
		
		public static function updateAccessManagement ($data, $where){
			
			global $wpdb;
				
			$table = $wpdb->prefix . MEO_ACCESS_MANAGEMENT_TABLE;
				
			$wpdb->update( $table, $data, $where );
		}
		
		public static function deleteAccessManagementByIds ($userID, $plugin){
			// TODO :: Check if IDs exist?
			//         Custom query
		}
		
	# Get Records
		
		#getRecords
		
		public static function getAccessManagementRecords ($where = '1=1'){
				
			global $wpdb;
				
			// Get Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ACCESS_MANAGEMENT_TABLE . " WHERE ".$where." ORDER BY user_id ASC";
				
			$records = $wpdb->get_results($query, ARRAY_A);
				
			if($records) return $records;
			
			return false;
		}
		
		#getRecordsBySiteID
		
		public static function getManagementByProjectID ($ProjectID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ACCESS_MANAGEMENT_TABLE . " WHERE project_id=".ProjectID." ORDER BY user_id ASC";
		
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return $records;
		
			return false;
		}
		
		#getManagementByUserID
		
		public static function getManagementByUserID ($UserID){
		
			global $wpdb;
		
			// Get Records
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ACCESS_MANAGEMENT_TABLE . " WHERE user_id=".$UserID." ORDER BY project_id ASC";
		
			$records = $wpdb->get_results($query, ARRAY_A);
		
			if($records) return $records;
		
			return false;
		}
		
	# Get Users
	
		public static function getUsersByProjectID ($Project_ID){
			
			global $wpdb;
			
			// Get Users
			$query = "SELECT user_id FROM " . $wpdb->prefix.MEO_ACCESS_MANAGEMENT_TABLE . " WHERE project_id=" . $Project_ID . " ORDER BY user_id ASC";
			
			$userIDs = $wpdb->get_results($query, ARRAY_A);
			
			if($userIDs){
				$include = array();
	        	foreach($userIDs as $k => $u){
	        		$include[] = $u['user_id'];
	        	}
	        	$users = get_users(array('include' => $include));
	        
	        	return $users;
			}
			
			return false;
		}
		
		public static function getUsers ($where = '', $returnType = OBJECT){
			
			global $wpdb;
			
			// Get all Users
			$query = "SELECT * FROM " . $wpdb->prefix.MEO_ACCESS_MANAGEMENT_TABLE . " " . $where . " ORDER BY id DESC";
			
			$all_users = $wpdb->get_results($query, $returnType);
			
			return $all_users;
		}
		
	
}
